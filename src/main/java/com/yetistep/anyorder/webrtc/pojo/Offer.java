package com.yetistep.anyorder.webrtc.pojo;

/**
 * Created by sagar on 9/2/2016.
 */
public class Offer {
    private String type;
    private String sdp;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSdp() {
        return sdp;
    }

    public void setSdp(String sdp) {
        this.sdp = sdp;
    }
}
