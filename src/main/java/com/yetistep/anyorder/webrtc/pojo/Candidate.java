package com.yetistep.anyorder.webrtc.pojo;

/**
 * Created by sagar on 9/2/2016.
 */
public class Candidate {
    private String candidate;
    private String sdpMid;
    private String sdpMLineIndex;

    public String getCandidate() {
        return candidate;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public String getSdpMid() {
        return sdpMid;
    }

    public void setSdpMid(String sdpMid) {
        this.sdpMid = sdpMid;
    }

    public String getSdpMLineIndex() {
        return sdpMLineIndex;
    }

    public void setSdpMLineIndex(String sdpMLineIndex) {
        this.sdpMLineIndex = sdpMLineIndex;
    }
}
