package com.yetistep.anyorder.webrtc.pojo;

import com.yetistep.anyorder.enums.OrderStatus;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 10/3/16
 * Time: 12:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResponseOrderDto {

    private String orderId;
    private OrderStatus orderStatus;
    private String driverId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }
}
