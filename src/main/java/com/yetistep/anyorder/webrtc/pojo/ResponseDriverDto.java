package com.yetistep.anyorder.webrtc.pojo;

import com.yetistep.anyorder.enums.AvailabilityStatus;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 10/3/16
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResponseDriverDto {

    private String driverId;
    private AvailabilityStatus availabilityStatus;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public AvailabilityStatus getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(AvailabilityStatus availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }
}
