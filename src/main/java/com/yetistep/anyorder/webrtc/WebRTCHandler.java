package com.yetistep.anyorder.webrtc;

import com.yetistep.anyorder.service.inf.WebRTCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Created by sagar on 8/27/2016.
 */
public class WebRTCHandler  extends TextWebSocketHandler {

    @Autowired
    WebRTCService webRTCService;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        webRTCService.registerOpenConnection(session);
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        webRTCService.processMessage(session, message.getPayload().toString());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        webRTCService.handleTransportError(session, exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        webRTCService.registerCloseConnection(session, status);
    }

}
