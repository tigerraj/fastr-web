package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public enum CustomerAreaType {

    IN_SERVING_AREA,
    OUT_OF_SERVING_AREA,
    UNKNOWN_AREA
}
