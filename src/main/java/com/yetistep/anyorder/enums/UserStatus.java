package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 12/10/14
 * Time: 10:39 AM
 * To change this template use File | Settings | File Templates.
 */
public enum UserStatus {
    //Do not Order Change [ 1, 2, 3, 4]
    UNVERIFIED(1), ACTIVE(2), INACTIVE(3), BLACK_LISTED(4);

    private final int status;

    private UserStatus(int status) {
        this.status = status;
    }

    public Integer toInt() {
        return this.ordinal();
    }

    public static UserStatus fromInt(Integer arg) {
        switch (arg) {
            case 1:
                return UNVERIFIED;
            case 2:
                return ACTIVE;
            case 3:
                return INACTIVE;
            case 4:
                return BLACK_LISTED;
            default:
                return UNVERIFIED;
        }
    }

    public static UserStatus fromString(String arg) {
        switch (arg) {
            case "UNVERIFIED":
                return UNVERIFIED;
            case "ACTIVE":
                return ACTIVE;
            case "INACTIVE":
                return INACTIVE;
            case "BLACK_LISTED":
                return BLACK_LISTED;
            default:
                return UNVERIFIED;
        }
    }
}
