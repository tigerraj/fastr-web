package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 2/17/16
 * Time: 10:45 AM
 * To change this template use File | Settings | File Templates.
 */
public enum PreferenceType {
    /* Country And currency */
    CURRENCY,
    COUNTRY,
    SMS_PROVIDER,     //Sparrow SMS 1, Twilio SMS 2
    SMS_COUNTRY_CODE, //Country code for sms to be sent

    /* Support configuration */
    HELPLINE_NUMBER,      //system helpline number
    ADMIN_EMAIL,        //system administration email
    SUPPORT_EMAIL,      //
    SERVER_URL,

    /* Company Information */
    COMPANY_NAME,     // Name of the company
    COMPANY_LOGO,      //logo of the company
    LOGO_FOR_PDF_EMAIL, //logo to be used in the pdf and email
    COMPANY_ADDRESS,   //address of the company
    CONTACT_NO,        //contact number of the company
    COMPANY_EMAIL,     //email of the company
    COMPANY_WEBSITE,   //website of the company
    REGISTRATION_NO,   //company registration number
    APPLICATION_NAME,   //Name of the application
    DEFAULT_IMG_SEARCH,

    /* Version Configuration */
    ANDROID_APP_VER_NO,
    WEB_APP_VER_NO,
    IOS_APP_VER_NO,

    /*driver assign algorithm*/
    GOOGLE_TRAFFIC_MODEL,     //options are: best guess, optimistic, pessimistic
    GOOGLE_TRAVEL_MODE,      //options are: driving, bicycling
    ASSIGN_DRIVER_AUTOMATICALLY, //values true and false
    DEVIATION_TIME_IN_MIN,
    MAX_NUM_ORDER_FOR_DRIVER,
    //Driver location update time
    DRIVER_LOCATION_UPDATE_TIME,
    ORDER_COMPLETION_MAXIMUM_TIME,
    DRIVERS_TIME_AT_STORE,
    DRIVERS_TIME_AT_CUSTOMER,
    DRIVING_TIME_PER_KM,
    DRIVER_OFF_DUTY_STATUS_UPDATE_TIME,
    DRIVER_OFFSET_TIME,
    AIR_TO_ROUTE_DISTANCE_TIME_FACTOR_PER_KM,
    AIR_OR_ACTUAL_DISTANCE_SWITCH

}
