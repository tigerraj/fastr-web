package com.yetistep.anyorder.enums;

/**
 * Created by root on 9/23/16.
 */
public enum OrderPlan {
    SERVICE_AREA_PLAN,
    OUTSIDE_SERVICE_AREA_PLAN,
    DISTANCE_BASED_PLAN
}
