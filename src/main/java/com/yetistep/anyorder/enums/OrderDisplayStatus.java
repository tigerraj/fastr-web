package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 8/1/16
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */
public enum OrderDisplayStatus {

    WAITING(0), /* order placed */
    PROCESSING(1), /* order started, in route to pick up, at store */
    DISPATCHED(2), /* in route to delivery, at customer */
    DELIVERED(3), /* when delivery boy declare the order has been delivered */
    CANCELLED(4), /* when order has been cancelled or failed */
    RUNNING(5),
    PAST(6),
    ALL(7);

    private final int orderDisplayStatus;

    private OrderDisplayStatus(int orderDisplayStatus) {
        this.orderDisplayStatus = orderDisplayStatus;
    }

    public static OrderDisplayStatus fromInt(int arg) {
        switch (arg) {
            case 0:
                return WAITING;
            case 1:
                return PROCESSING;
            case 2:
                return DISPATCHED;
            case 3:
                return DELIVERED;
            case 4:
                return CANCELLED;
            default:
                return WAITING;
        }
    }

}
