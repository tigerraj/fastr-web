package com.yetistep.anyorder.enums;

public enum DistanceType {
    AIR_DISTANCE, ACTUAL_DISTANCE
}
