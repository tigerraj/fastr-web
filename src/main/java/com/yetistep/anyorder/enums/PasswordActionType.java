package com.yetistep.anyorder.enums;

/**
 * Created by BinaySingh on 5/11/2016.
 */
public enum PasswordActionType {
    FORGOT, RESET, RESEND, NEW
}
