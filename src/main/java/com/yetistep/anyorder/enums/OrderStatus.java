package com.yetistep.anyorder.enums;

public enum OrderStatus {
    ORDER_PLACED(1), /* when checkout is successful */
    ORDER_STARTED(2), /* Driver start the job */
    EN_ROUTE_TO_PICK_UP(3),/* Driver in route to pick up order item from store */
    AT_STORE(4), /* driver reached the store */
    EN_ROUTE_TO_DELIVER(5), /* driver pick up order item and route to delivery */
    AT_CUSTOMER(6),  /* driver reached to customer */
    DELIVERED(7), /* when delivery boy declare the order has been delivered */
    CANCELLED(8); /* when order has been cancelled or failed */

    private final int status;

    private OrderStatus(int status) {
        this.status = status;
    }

    public static OrderStatus fromInt(int arg) {
        switch (arg) {
            case 0:
                return ORDER_PLACED;
            case 1:
                return ORDER_STARTED;
            case 2:
                return EN_ROUTE_TO_PICK_UP;
            case 3:
                return AT_STORE;
            case 4:
                return EN_ROUTE_TO_DELIVER;
            case 5:
                return AT_CUSTOMER;
            case 6:
                return DELIVERED;
            default:
                return CANCELLED;
        }
    }
}
