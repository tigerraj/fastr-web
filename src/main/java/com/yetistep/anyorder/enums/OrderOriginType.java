package com.yetistep.anyorder.enums;

/**
 * Created by root on 9/23/16.
 */
public enum OrderOriginType {
    FASTR,
    RESTAURANT,
    THIRD_PARTY
}
