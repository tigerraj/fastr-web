package com.yetistep.anyorder.enums;

/**
 * Created by Sagar on 6/13/2016.
 */
public enum DurationType {
    WEEKLY,MONTHLY,YEARLY;
}
