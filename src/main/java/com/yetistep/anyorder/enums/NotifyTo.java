package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 2/10/15
 * Time: 11:54 AM
 * To change this template use File | Settings | File Templates.
 */
public enum NotifyTo {
    STORE_MANAGER(1), FASTR_DRIVER(2), RESTAURANT_DRIVER(3), DRIVER(4), MERCHANT(5);

    private final int notifyto;

    NotifyTo(int notifyto) {
        this.notifyto = notifyto;
    }
}
