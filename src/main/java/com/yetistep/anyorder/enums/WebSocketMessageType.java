package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 9/16/16
 * Time: 2:13 PM
 * To change this template use File | Settings | File Templates.
 */
public enum  WebSocketMessageType {
    ORDER_STATUS,
    NEW_ORDER,
    DRIVER_LOCATION_UPDATE,
    DRIVER_AVAILABILITY_STATUS;

}
