package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
public enum AvailabilityStatus {

    AVAILABLE,
    UNAVAILABLE,
    BUSY
}
