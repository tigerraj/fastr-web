package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 11/21/14
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
public enum UserRole {

    ROLE_ADMIN(1), ROLE_MANAGER(2), ROLE_CSR(3), ROLE_MERCHANT(4), ROLE_STORE_MANAGER(5), ROLE_CUSTOMER(6), ROLE_DRIVER(7);

    private final int role;

    private UserRole(int role) {
        this.role = role;
    }

    public Integer toInt() {
        return role;
    }

    public String toStr() {
        return String.valueOf(role);
    }

    public static UserRole fromInt(Integer arg) {
        switch (arg) {
            case 1:
                return ROLE_ADMIN;
            case 2:
                return ROLE_MANAGER;
            case 3:
                return ROLE_CSR;
            case 4:
                return ROLE_MERCHANT;
            case 5:
                return ROLE_STORE_MANAGER;
            case 6 :
                return ROLE_CUSTOMER;
            case 7 :
                return ROLE_DRIVER;
            default:
                return ROLE_ADMIN;

        }
    }
}
