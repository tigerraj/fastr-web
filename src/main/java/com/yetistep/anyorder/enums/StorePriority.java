package com.yetistep.anyorder.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/18/16
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public enum StorePriority {

    PRIORITIZED,
    NON_PRIORITIZED;

}
