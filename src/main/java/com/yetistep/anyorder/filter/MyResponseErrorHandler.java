package com.yetistep.anyorder.filter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.MapperWrapper;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.xml.transform.Result;

/**
 * Created by suroz on 1/20/2017.
 */

@ControllerAdvice
public class MyResponseErrorHandler extends XStream {
    XStream xstream = new XStream() {
        @Override
        protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                @Override
                public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                    if (definedIn == Object.class) {
                        return false;
                    }
                    return super.shouldSerializeMember(definedIn, fieldName);
                }
            };
        }
    };
}
