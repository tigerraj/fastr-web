package com.yetistep.anyorder.filter;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

/**
 * Created by suroz on 1/20/2017.
 */
@XStreamAlias("errors")
public class ErrorMessage {

    private String error;
    private String errorDetail;
    private List<String> param;

    public ErrorMessage(List<String> param){
        this.param = param;
    }

    public ErrorMessage(String param1){
        this.error = param1;
    }
    public ErrorMessage(String param1, String param2){
        this.error = param1;
        this.errorDetail = param2;
    }
}
