package com.yetistep.anyorder.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yetistep.anyorder.enums.DriverType;
import com.yetistep.anyorder.model.DriverEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 8/4/16
 * Time: 12:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResponseJsonDto {

    private String orderId;
    private List<DriverEntity> drivers;
    private Boolean assignDriverAutomatically;
    private DriverType driverType;
    private String driverId;
    private String message;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public List<DriverEntity> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<DriverEntity> drivers) {
        this.drivers = drivers;
    }

    public Boolean getAssignDriverAutomatically() {
        return assignDriverAutomatically;
    }

    public void setAssignDriverAutomatically(Boolean assignDriverAutomatically) {
        this.assignDriverAutomatically = assignDriverAutomatically;
    }

    public DriverType getDriverType() {
        return driverType;
    }

    public void setDriverType(DriverType driverType) {
        this.driverType = driverType;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
