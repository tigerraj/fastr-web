package com.yetistep.anyorder.dto;

import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 1/13/16
 * Time: 12:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class RequestJsonDto {

    private Integer id;
    private List<PreferencesEntity> preferences;
    private PasswordActionType actionType;   // for password assist
    private UserEntity user;
    private MerchantEntity merchant;
    private String password;
    private StoreManagerEntity storeManager;

    /*order properties*/
    private OrderEntity order;
    private Boolean saveAndAssign;
    private String driverId;
    private OrderStatus orderStatus;
    private OrderDisplayStatus orderDisplayStatus;

    private String mobileNumber;
    private DriverEntity driver;
    private StoreEntity store;

    private Page page;
    private StoreBrandEntity storeBrand;


    //response properties
    private String waitingOrderNumber;
    private String processingOrderNumber;
    private String dispatchedOrderNumber;
    private String deliveredOrderNumber;
    private String cancelledOrderNumber;
    private String runningOrderNumber;
    private String pastOrderNumber;
    private StatementEntity statement;
    //Notification
    private NotificationEntity notification;
    private List<String> userIds;
    private List<NotifyTo> notifyTos;
    private UserDeviceType userDeviceType;
    private List<NotificationEntity> notifications;
    private Integer notViewedCount;
    //drivers
    private List<DriverEntity> offDutyDrivers;
    private List<DriverEntity> onDutyDrivers;
    private String prefKey;
    private String imageString;
    private String searchString;
    private List<String> driverIds;
    private Date fromDate;
    private Date toDate;
    private List<UnknownAreaRatePlanEntity> unknownAreaRatePlans;
    private List<RatePlanEntity> ratePlans;
    private AreaEntity area;
    private String orderCancelReason;
    private List<OrderStatus> orderStatuses;
    private List<String> merchantIds;
    private List<String> storeBrandIds;
    private List<String> storeIds;

    public String getRunningOrderNumber() {
        return runningOrderNumber;
    }

    public void setRunningOrderNumber(String runningOrderNumber) {
        this.runningOrderNumber = runningOrderNumber;
    }

    public String getPastOrderNumber() {
        return pastOrderNumber;
    }

    public void setPastOrderNumber(String pastOrderNumber) {
        this.pastOrderNumber = pastOrderNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<PreferencesEntity> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<PreferencesEntity> preferences) {
        this.preferences = preferences;
    }

    public PasswordActionType getActionType() {
        return actionType;
    }

    public void setActionType(PasswordActionType actionType) {
        this.actionType = actionType;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public MerchantEntity getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantEntity merchant) {
        this.merchant = merchant;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public StoreManagerEntity getStoreManager() {
        return storeManager;
    }

    public void setStoreManager(StoreManagerEntity storeManager) {
        this.storeManager = storeManager;
    }

    public Boolean getSaveAndAssign() {
        return saveAndAssign;
    }

    public void setSaveAndAssign(Boolean saveAndAssign) {
        this.saveAndAssign = saveAndAssign;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public OrderDisplayStatus getOrderDisplayStatus() {
        return orderDisplayStatus;
    }

    public void setOrderDisplayStatus(OrderDisplayStatus orderDisplayStatus) {
        this.orderDisplayStatus = orderDisplayStatus;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    public StoreBrandEntity getStoreBrand() {
        return storeBrand;
    }

    public void setStoreBrand(StoreBrandEntity storeBrand) {
        this.storeBrand = storeBrand;
    }

    public String getWaitingOrderNumber() {
        return waitingOrderNumber;
    }

    public void setWaitingOrderNumber(String waitingOrderNumber) {
        this.waitingOrderNumber = waitingOrderNumber;
    }

    public String getProcessingOrderNumber() {
        return processingOrderNumber;
    }

    public void setProcessingOrderNumber(String processingOrderNumber) {
        this.processingOrderNumber = processingOrderNumber;
    }

    public String getDispatchedOrderNumber() {
        return dispatchedOrderNumber;
    }

    public void setDispatchedOrderNumber(String dispatchedOrderNumber) {
        this.dispatchedOrderNumber = dispatchedOrderNumber;
    }

    public String getDeliveredOrderNumber() {
        return deliveredOrderNumber;
    }

    public void setDeliveredOrderNumber(String deliveredOrderNumber) {
        this.deliveredOrderNumber = deliveredOrderNumber;
    }

    public String getCancelledOrderNumber() {
        return cancelledOrderNumber;
    }

    public void setCancelledOrderNumber(String cancelledOrderNumber) {
        this.cancelledOrderNumber = cancelledOrderNumber;
    }

    public StatementEntity getStatement() {
        return statement;
    }

    public void setStatement(StatementEntity statement) {
        this.statement = statement;
    }

    public NotificationEntity getNotification() {
        return notification;
    }

    public void setNotification(NotificationEntity notification) {
        this.notification = notification;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public void setUsers(List<String> userIds) {
        this.userIds = userIds;
    }

    public List<NotifyTo> getNotifyTos() {
        return notifyTos;
    }

    public void setNotifyTos(List<NotifyTo> notifyTos) {
        this.notifyTos = notifyTos;
    }

    public UserDeviceType getUserDeviceType() {
        return userDeviceType;
    }

    public void setUserDeviceType(UserDeviceType userDeviceType) {
        this.userDeviceType = userDeviceType;
    }

    public List<NotificationEntity> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationEntity> notifications) {
        this.notifications = notifications;
    }

    public Integer getNotViewedCount() {
        return notViewedCount;
    }

    public void setNotViewedCount(Integer notViewedCount) {
        this.notViewedCount = notViewedCount;
    }

    public List<DriverEntity> getOffDutyDrivers() {
        return offDutyDrivers;
    }

    public void setOffDutyDrivers(List<DriverEntity> offDutyDrivers) {
        this.offDutyDrivers = offDutyDrivers;
    }

    public List<DriverEntity> getOnDutyDrivers() {
        return onDutyDrivers;
    }

    public void setOnDutyDrivers(List<DriverEntity> onDutyDrivers) {
        this.onDutyDrivers = onDutyDrivers;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getPrefKey() {
        return prefKey;
    }

    public void setPrefKey(String prefKey) {
        this.prefKey = prefKey;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public List<String> getDriverIds() {
        return driverIds;
    }

    public void setDriverIds(List<String> driverIds) {
        this.driverIds = driverIds;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public AreaEntity getArea() {
        return area;
    }

    public void setArea(AreaEntity area) {
        this.area = area;
    }

    public String getOrderCancelReason() {
        return orderCancelReason;
    }

    public void setOrderCancelReason(String orderCancelReason) {
        this.orderCancelReason = orderCancelReason;
    }

    public List<UnknownAreaRatePlanEntity> getUnknownAreaRatePlans() {
        return unknownAreaRatePlans;
    }

    public void setUnknownAreaRatePlans(List<UnknownAreaRatePlanEntity> unknownAreaRatePlans) {
        this.unknownAreaRatePlans = unknownAreaRatePlans;
    }

    public List<RatePlanEntity> getRatePlans() {
        return ratePlans;
    }

    public void setRatePlans(List<RatePlanEntity> ratePlans) {
        this.ratePlans = ratePlans;
    }

    public List<OrderStatus> getOrderStatuses() {
        return orderStatuses;
    }

    public void setOrderStatuses(List<OrderStatus> orderStatuses) {
        this.orderStatuses = orderStatuses;
    }

    public List<String> getMerchantIds() {
        return merchantIds;
    }

    public void setMerchantIds(List<String> merchantIds) {
        this.merchantIds = merchantIds;
    }

    public List<String> getStoreBrandIds() {
        return storeBrandIds;
    }

    public void setStoreBrandIds(List<String> storeBrandIds) {
        this.storeBrandIds = storeBrandIds;
    }

    public List<String> getStoreIds() {
        return storeIds;
    }

    public void setStoreIds(List<String> storeIds) {
        this.storeIds = storeIds;
    }
}

