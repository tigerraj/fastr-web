package com.yetistep.anyorder.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/3/16
 * Time: 9:48 AM
 * To change this template use File | Settings | File Templates.
 */
@Aspect
@Component
public class LoggerAspect {

    private static final Logger log = Logger.getLogger(LoggerAspect.class);

    @Before("execution(* com.yetistep.anyorder.controller.*.*(..))")
    public void allControllerCalled(JoinPoint joinPoint) {
        //log.info("arguments " + Arrays.toString(joinPoint.getArgs()));
        log.info("Method " + joinPoint.getSignature().getName() + " called in controller");
    }

    @After("execution(* com.yetistep.anyorder.controller.*.*(..))")
    public void allControllerExited(JoinPoint joinPoint) {
        log.info("Exited from method "+joinPoint.getSignature().getName());
    }


}
