package com.yetistep.anyorder.aspect;

import com.yetistep.anyorder.dao.inf.UserDeviceDao;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.UserDeviceEntity;
import com.yetistep.anyorder.util.MD5Hash;
import com.yetistep.anyorder.util.YSException;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/17/16
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */
@Aspect
@Component
public class ValidationAspect {

    @Autowired
    UserDeviceDao userDeviceDao;

    private static final Logger log = Logger.getLogger(LoggerAspect.class);

    @Before("execution(* com.yetistep.anyorder.service.impl.*MobileServiceImpl.*(..))")
    //@Before("@annotation(com.yetistep.anyorder.aspect.annotations.MobileService)")
    public void validateAccessToken(JoinPoint joinPoint) throws Exception{
        log.info("arguments " + Arrays.toString(joinPoint.getArgs()));
        if(joinPoint.getArgs()[0].getClass().getSimpleName().equals("HeaderDto")) {
            HeaderDto headerDto = (HeaderDto) joinPoint.getArgs()[0];

            if(headerDto.getAccessToken()!=null){
                UserDeviceEntity userDevice = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        
                if(userDevice==null)
                    throw new YSException("SEC001");
            }
        }
    }


    @Before("@annotation(com.yetistep.anyorder.aspect.annotations.CreateOrderValidate)")
    public void  validateCreateOrder(JoinPoint joinPoint) throws Exception {
        log.info("arguments " + Arrays.toString(joinPoint.getArgs()));
        RequestJsonDto requestJsonDto = (RequestJsonDto) joinPoint.getArgs()[1];
        OrderEntity order = requestJsonDto.getOrder();

        if(order==null)
            throw new YSException("COM001");

        if(order.getCustomer()==null)
            throw new YSException("ORD001");

        /*if(order.getCustomer().getCustomerId()==null && order.getCustomer().getUser()==null)
            throw new YSException("ORD002");*/

        if(order.getCustomer().getCustomersArea()==null || order.getCustomer().getCustomersArea().size()<1)
            throw new YSException("ORD003");

        if(order.getCustomer().getCustomersArea().get(0).getCustomersAreaId()==null &&
                (order.getCustomer().getCustomersArea().get(0).getArea()== null || order.getCustomer().getCustomersArea().get(0).getArea().getAreaId()==null)
                && (order.getCustomer().getCustomersArea().get(0).getLatitude()==null || order.getCustomer().getCustomersArea().get(0).getLongitude()==null))
            throw new YSException("ORD003");

    }

}
