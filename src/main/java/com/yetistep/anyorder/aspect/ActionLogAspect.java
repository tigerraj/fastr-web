package com.yetistep.anyorder.aspect;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/3/16
 * Time: 10:16 AM
 * To change this template use File | Settings | File Templates.
 */

import com.yetistep.anyorder.dao.inf.ActionLogDao;
import com.yetistep.anyorder.dao.inf.PreferencesDao;
import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.model.ActionLogEntity;
import com.yetistep.anyorder.model.PreferencesEntity;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.util.ActionLogUtil;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Aspect
@Component
public class ActionLogAspect {

    private static final Logger log = Logger.getLogger(ActionLogAspect.class);

    @Autowired
    ActionLogDao actionLogDaoService;

    @Autowired
    UserDao userDaoService;

    @Autowired
    PreferencesDao preferencesDaoService;

    @After("@annotation(com.yetistep.anyorder.aspect.annotations.ActionLogSupport)")
    public void saveActionLog(JoinPoint joinPoint) throws Exception{
        List<PreferencesEntity> systemPreferences = (List<PreferencesEntity>) joinPoint.getArgs()[0];

        List<ActionLogEntity> actionLogs = new ArrayList<>();
        UserEntity userEntity = userDaoService.find(1/*SessionManager.getUserIds()*/);
        for (PreferencesEntity preference : systemPreferences) {
            PreferencesEntity dbPreference = preferencesDaoService.findByKey(preference.getPrefKey());
            ActionLogEntity actionLog = ActionLogUtil.createActionLog(dbPreference, preference, new String[]{"value"}, userEntity);
            if (actionLog != null) {
                actionLogs.add(actionLog);
            }
        }

        log.info("saving action log");
        actionLogDaoService.saveAll(actionLogs);
    }

}
