package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.CustomerEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CustomerDao extends GenericDaoService<Long, CustomerEntity> {

    public CustomerEntity findByCustomerId(String customerId) throws Exception;

}
