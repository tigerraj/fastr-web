package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.enums.StatementGenerationType;
import com.yetistep.anyorder.model.StoreEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface StoreDao extends GenericDaoService<Integer, StoreEntity> {

    public StoreEntity findById(String storeId) throws Exception;

    public List<StoreEntity> findAllActive() throws Exception;

    public List<StoreEntity> findActiveStoreByStatementGenerationType(StatementGenerationType statementGenerationType) throws Exception;

    public List<StoreEntity> findActive() throws Exception;

}
