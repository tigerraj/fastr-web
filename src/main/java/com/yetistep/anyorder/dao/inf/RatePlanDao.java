package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.RatePlanEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:00 AM
 * To change this template use File | Settings | File Templates.
 */
public interface RatePlanDao extends GenericDaoService<Integer, RatePlanEntity> {

    public RatePlanEntity findById(String ratePlanId) throws Exception;
}
