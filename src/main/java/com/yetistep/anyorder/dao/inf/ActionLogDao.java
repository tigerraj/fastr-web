package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.ActionLogEntity;
import com.yetistep.anyorder.model.Page;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ActionLogDao extends GenericDaoService<Long, ActionLogEntity> {
     public List<ActionLogEntity> findActionLogPaginated(Page page) throws Exception;

     public void saveAll(List<ActionLogEntity> values) throws Exception;

     public Integer getTotalNumberOfActionLogs() throws Exception;

}
