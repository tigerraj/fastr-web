package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.StatementEntity;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:00 AM
 * To change this template use File | Settings | File Templates.
 */
public interface StatementDao extends GenericDaoService<Long, StatementEntity> {
    Integer getNumberOfMerchantStatements(String merchantId, Page page, Date fromDate, Date toDate) throws Exception;

    List<StatementEntity> getMerchantStatements(String merchantId, Page page, Date fromDate, Date toDate) throws Exception;

    Integer getNumberOfStoreStatements(String storeId, Page page, Date fromDate, Date toDate) throws Exception;

    List<StatementEntity> getStoreStatements(String storeId, Page page, Date fromDate, Date toDate) throws Exception;

    Integer getNumberOfAllStatements(Page page, Date fromDate, Date toDate) throws Exception;

    List<StatementEntity> getAllStatements(Page page, Date fromDate, Date toDate) throws Exception;

    StatementEntity findByStatementId(String statementId) throws Exception;
}
