package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.StoresNotDriverEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
public interface StoresNotDriverDao extends GenericDaoService<Integer, StoresNotDriverEntity> {

    public StoresNotDriverEntity findById(String driverId) throws Exception;

    public StoresNotDriverEntity findByDriverAndStoreId(Integer driverId, Integer storeId) throws Exception;
}
