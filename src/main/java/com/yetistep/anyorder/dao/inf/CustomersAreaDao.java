package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.CustomersAreaEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/20/16
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CustomersAreaDao extends GenericDaoService<Integer, CustomersAreaEntity> {

    public CustomersAreaEntity findByCustomersAreaId(String  customersAreaId) throws Exception;

}
