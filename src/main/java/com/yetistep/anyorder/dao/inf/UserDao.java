package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.util.YSException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao extends GenericDaoService<Integer, UserEntity> {

    //Spring Authentication Only
    public UserEntity findByUserName(String userName);

    public UserEntity findStoreManagerByUsernameMobile(String userName) throws Exception;

    UserEntity findStoreManagerByUsernameMobileNo(String userName) throws Exception;

    public UserEntity findByUsernameMobile(String userName) throws Exception;

    public UserEntity findCustomerByNameAndMobileNumber(String firstName, String lastName, String mobileNumber) throws Exception;

    public UserEntity findByUserId(String userId) throws Exception;

    public UserEntity findByMobileNumberAndRole(String mobileNumber, UserRole userRole);

    public Boolean checkIfEmailExistsWithSameRole(String email, UserRole userRole) throws YSException;

    public Boolean checkIfEmailExistsWithSameRole(String emailAddress, UserRole userRole, Integer userId) throws YSException;

    public Boolean checkIfEmailExists(String email) throws YSException;

    public Boolean checkIfMobileNumberExists(String mobileNumber, UserRole userRole) throws YSException;

    public Boolean checkIfMobileNumberExists(String mobileNumber, UserRole userRole, Integer userId) throws YSException;

    public UserEntity getUserByMobileNumber(String mobileNumber, UserRole userRole) throws YSException;

    public UserEntity findByVerificationCode(String code) throws Exception;

    public UserEntity findById(Integer userId) throws Exception;

    public Boolean checkUserNameExist(String username) throws Exception;

    public List<UserEntity> getAllManager() throws Exception;

    public UserEntity findStoreManagerByUserName(String userName) throws Exception;

    public List<UserEntity> findCustomersByMobileNumber(String mobileNumber) throws Exception;

    public UserEntity findByDriverId(String driverId) throws Exception;

    public Integer getNumberOfUsers(Page page) throws Exception;

    public List<UserEntity> getUsers(Page page) throws Exception;

    public List<UserEntity> findActiveMerchant() throws Exception;

    public UserEntity findDriverByUserName(String username) throws Exception;
}
