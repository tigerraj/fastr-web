package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.enums.NotifyTo;
import com.yetistep.anyorder.model.NotificationEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 9:59 AM
 * To change this template use File | Settings | File Templates.
 */
public interface NotificationDao extends GenericDaoService<Long, NotificationEntity> {

    public NotificationEntity findByNotificationId(String notificationId) throws Exception;

    public List<NotificationEntity> findDriverNotification(NotifyTo notifyTo) throws Exception;

    public List<NotificationEntity> findMerchantNotification(Integer merchantId) throws Exception;

    public List<NotificationEntity> findWebNotification() throws Exception;

    public List<NotificationEntity> findMobileNotification() throws Exception;
}
