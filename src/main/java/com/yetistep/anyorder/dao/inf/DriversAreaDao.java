package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.DriversAreaEntity;
import com.yetistep.anyorder.model.StoresAreaEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DriversAreaDao extends GenericDaoService<Integer, DriversAreaEntity> {

    public DriversAreaEntity findById(String driversAreaId) throws Exception;

    public DriversAreaEntity findByDriverAndAreaId(Integer driverId, Integer areaId) throws Exception;

    public void flush() throws Exception;

    public void deleteAll(Integer driverId) throws Exception;

    public void updateAll(List<DriversAreaEntity> driversAreas) throws Exception;

}
