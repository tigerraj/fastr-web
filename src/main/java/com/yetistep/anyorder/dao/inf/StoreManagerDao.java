package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.StoreManagerEntity;

/**
 * Created by BinaySingh on 5/18/2016.
 */
public interface StoreManagerDao extends GenericDaoService<Integer, StoreManagerEntity> {

    public StoreManagerEntity findByFakeId (String fakeId) throws Exception;
    public StoreManagerEntity findById(Integer id, String fakeId) throws Exception;
}
