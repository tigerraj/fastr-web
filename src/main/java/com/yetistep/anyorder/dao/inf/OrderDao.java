package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.OrderDisplayStatus;
import com.yetistep.anyorder.enums.OrderStatus;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.xml.APIData;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */
public interface OrderDao extends GenericDaoService<Integer, OrderEntity> {

    public OrderEntity findById(String id) throws Exception;

    public void flush() throws Exception;

    public List<OrderEntity> getLowerPriorityOrders(Integer driverId) throws Exception;

    public List<OrderEntity> findActiveOrdersOfDriver(String driverId) throws Exception;

    List<OrderEntity> findActiveOrdersOfDriverIds(List<Integer> ids) throws Exception;

    public Integer getNoOfCurrentOrdersByDriverId(String driverId) throws Exception;

    public Integer getNoOfPastOrdersByDriverId(String driverId) throws Exception;

    public Integer getNoOfDeliveredOrdersByDriverId(String driverId) throws Exception;

    public List<OrderEntity> getPastOrdersByDriverId(String driverId, Page page) throws Exception;

    public BigDecimal getRatingOfDriver(String driverId) throws Exception;

    public RequestJsonDto getNumberOfClassifiedOrders(Integer csrId) throws Exception;

    public RequestJsonDto getNumberOfClassifiedOrdersWeb(Integer storeId) throws Exception;

    public List<OrderEntity> getOrdersByStatus(List<Integer> storeId, Page page, OrderDisplayStatus orderStatus) throws Exception;

    public List<OrderEntity> getOrdersByStatusWeb(List<Integer> storeId, Page page, OrderDisplayStatus orderStatus) throws Exception;

    public Integer getTotalNumberOfOrdersByStatus(List<Integer> storeId, OrderDisplayStatus orderStatus) throws Exception;

    public Integer getTotalNumberOfOrdersByStatusWeb(List<Integer> storeId, OrderDisplayStatus orderStatus) throws Exception;

    public List<OrderEntity> getStoreOrders(Integer storeId, String fromDate, String toDate) throws Exception;

    public Integer getNumberOfSearchedOrder(Page page, OrderDisplayStatus orderDisplayStatus) throws Exception;

    public List<OrderEntity> getSearchedOrder(Page page, OrderDisplayStatus orderDisplayStatus) throws Exception;

    public Integer getNumberOfMerchantSearchedOrder(Page page, String merchantId, OrderDisplayStatus orderDisplayStatus) throws Exception;

    public List<OrderEntity> getMerchantSearchedOrder(Page page, String merchantId, OrderDisplayStatus orderDisplayStatus) throws Exception;

    public Integer getNumberOfStoreManagerSearchedOrder(Page page, String storeManagerId, OrderDisplayStatus orderDisplayStatus) throws Exception;

    public List<OrderEntity> getStoreManagerSearchedOrder(Page page, String storeManagerId, OrderDisplayStatus orderDisplayStatus) throws Exception;

    public Integer getTotalNumbersOfSearchedOrders(String searchString) throws Exception;

    public List<OrderEntity> searchOrder(Page page, String searchString) throws Exception;

    public BigDecimal findRatingOfDriver(Integer driverId) throws Exception;

    public Integer getNumberOfStoreManagerOrder(Page page, String storeId, List<OrderStatus> orderStatuses) throws Exception;

    public List<OrderEntity> getStoreManagerOrder(Page page, String storeId, List<OrderStatus> orderStatus) throws Exception;

    public Integer getNumberOfMerchantOrder(Page page, String userId, List<OrderStatus> orderStatuses) throws Exception;

    public List<OrderEntity> getMerchantOrder(Page page, String userId, List<OrderStatus> orderStatuses) throws Exception;

    public Integer getNumberOfAllOrder(Page page, List<OrderStatus> orderStatuses) throws Exception;

    public List<OrderEntity> getAllOrder(Page page, List<OrderStatus> orderStatuses) throws Exception;

    public Integer getNumberOfStoreOrder(Page page, String storeId, List<OrderStatus> orderStatuses) throws Exception;

    public List<OrderEntity> getStoreOrder(Page page, String storeId, List<OrderStatus> orderStatuses) throws Exception;

    public Integer getNumberOfStoreBrandOrder(Page page, String storeBrandId, List<OrderStatus> orderStatuses) throws Exception;

    public List<OrderEntity> getStoreBrandOrder(Page page, String storeBrandId, List<OrderStatus> orderStatuses) throws Exception;

    public Integer getNumberOfStoreManagerOrder(String storeId, OrderStatus orderPlaced) throws Exception;

    public Integer getNumberOfMerchantOrder(String userId, OrderStatus orderPlaced) throws Exception;

    public Integer getNumberOfAllOrder(OrderStatus orderPlaced) throws Exception;

    public Integer getNumberOfStoreOrder(String storeId, OrderStatus orderStatus) throws Exception;

    public Integer getNumberOfStoreBrandOrder(String storeBrandId, OrderStatus orderStatus) throws Exception;

    public Integer getNumberOfOrdersByDrivers(List<String> driverIds, Date fromDate, Date toDate, List<OrderStatus> orderStatuses, List<String> storeIds) throws Exception;

    public List<OrderEntity> getOrdersByDrivers(List<String> driverIds, Date fromDate, Date toDate, Page page, List<OrderStatus> orderStatuses, List<String> storeIds) throws Exception;

    public List<OrderEntity> getOrderListByAPIKey(APIData xmlData, Integer merchantId, String storeId) throws Exception;

    public APIData getDetailById(String id) throws Exception;

    Integer getStoreOrderCountByDate(String storeId, RequestJsonDto requestJsonDto, OrderDisplayStatus orderDisplayStatus) throws Exception;

    Integer getMerchantOrderCountByDate(String merchantId, RequestJsonDto requestJsonDto, OrderDisplayStatus orderDisplayStatus) throws Exception;

    Integer getAllOrderCountByDate(RequestJsonDto requestJsonDto, OrderDisplayStatus orderDisplayStatus) throws Exception;
}
