package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.UserDeviceEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/17/16
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */
public interface UserDeviceDao extends GenericDaoService<Integer, UserDeviceEntity> {

    public UserDeviceEntity findByAccessToken(String accessToken) throws Exception;

    public UserDeviceEntity findByEntityId(String entityId) throws Exception;

    public List<String> getAllDeviceTokensForFamilyAndRole(UserRole role, String family) throws Exception;

    public List<String> findDeviceTokenByUserId(Integer userId) throws Exception;

    public List<String> findDeviceTokenByStoreId(Integer storeId) throws Exception;

    public UserDeviceEntity findByUserId(Integer userId) throws Exception;

}
