package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.PreferenceTypeEntity;
import com.yetistep.anyorder.model.PreferencesEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PreferencesDao extends GenericDaoService<Integer, PreferencesEntity> {
    public PreferencesEntity findByKey(String key) throws Exception;

    public List<PreferenceTypeEntity> findAllPreferenceType() throws Exception;

    public PreferenceTypeEntity findPreferenceType(Integer typeId) throws Exception;

    public void updatePreferences(List<PreferencesEntity> preferencesEntities) throws Exception;

    public void updatePreferenceType(PreferenceTypeEntity preferenceType) throws Exception;
}
