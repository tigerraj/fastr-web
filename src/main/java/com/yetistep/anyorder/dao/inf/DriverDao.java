package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.enums.AvailabilityStatus;
import com.yetistep.anyorder.enums.StorePriority;
import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.Page;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DriverDao  extends GenericDaoService<Integer, DriverEntity> {

    public DriverEntity findById(String driverId) throws Exception;

    public List<DriverEntity> getSelectedDriver(Integer storeId, Integer areaId, StorePriority priority, String maxCount, Integer driverLocationUpdateTimeInMinute, Integer driverOffsetTime) throws Exception;

    public Integer getDriversOrderCount(String driverId) throws Exception;

    public OrderEntity getDriversLastOrder (String driverId) throws Exception;

    public BigDecimal getDriversAvgRating (String driverId) throws Exception;

    public BigDecimal getDriversPreviousOrderTotalEtd (String driverId, Integer id) throws Exception;

    public BigDecimal getDriversTotalEtd (String driverId) throws Exception;

    public Boolean mobileNumberExists(String mobileNumber) throws Exception;

    public Boolean licenseNumberExists(String licenseNumber) throws Exception;

    public Integer getTotalNumberOfStoreDriver(String storeId, Page page) throws Exception;

    public Integer getTotalNumberOfStoreDriverByStatus(String storeId, Page page, AvailabilityStatus availabilityStatus) throws Exception;

    public Integer getTotalNumberOfAnyOrderDriverByStatus(Page page, AvailabilityStatus availabilityStatus) throws Exception;

    public Integer getTotalNumberOfStoreBrandDriver(String storeBrandId, Page page) throws Exception;

    public Integer getTotalNumberOfMerchantDriver(String merchantId, Page page) throws Exception;

    public Integer getTotalNumberOfMerchantDriverByStatus(String merchantId, Page page, AvailabilityStatus status) throws Exception;

    public Integer getTotalNumberOfAllDriver(Page page) throws Exception;

    public List<DriverEntity> findStoreDriver(String storeId, Page page) throws Exception;

    public List<DriverEntity> findStoreDriverByStatus(String storeId, Page page, AvailabilityStatus availabilityStatus) throws Exception;

    public List<DriverEntity> findAnyOrderDriverByStatus(Page page, AvailabilityStatus availabilityStatus) throws Exception;

    public List<DriverEntity> findStoreBrandDriver(String storeBrandId, Page page) throws Exception;

    public List<DriverEntity> findMerchantDriver(String merchantId, Page page) throws Exception;

    public List<DriverEntity> findMerchantDriverByStatus(String merchantId, Page page, AvailabilityStatus status) throws Exception;

    public List<DriverEntity> findAll(Page page) throws Exception;

    public List<DriverEntity> getAnyOrderDrivers() throws Exception;

    public List<DriverEntity> findActiveAnyOrderDrivers() throws Exception;

    public List<DriverEntity> findStoreDriverForOrder(String storeId) throws Exception;

    public List<DriverEntity> findAvailableDriver() throws Exception;

    public Integer getTotalNumberOfAnyOrderDriver(Page page) throws Exception;

    public List<DriverEntity> findAnyOrderDriver(Page page) throws Exception;

    public List<OrderEntity> getDriversRunningOrderList(String driverId) throws Exception;

    public Integer getNumberOfStoreBrandsDrivers(List<String> storeBrandIds) throws Exception;

    public List<DriverEntity> getStoreBrandsDrivers(List<String> storeBrandIds, Page page) throws Exception;
}
