package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.AreaEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AreaDao extends GenericDaoService<Integer, AreaEntity> {

    public AreaEntity findById(String areaId) throws Exception;

    public List<AreaEntity> findAreaIds() throws Exception;

    public List<AreaEntity> findParentAreas() throws Exception;

    public List<AreaEntity> findActiveParentAreas() throws Exception;

    public Boolean checkAreaName(String areaName) throws Exception;

    public AreaEntity findByAreaId(String areaId) throws Exception;

    public List<AreaEntity> findParentAreasByAreaId(String areaId) throws Exception;

}
