package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.AreaEntity;
import com.yetistep.anyorder.model.StoresAreaEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface StoreAreaDao extends GenericDaoService<Integer, StoresAreaEntity> {

    public StoresAreaEntity findById(String storesAreaId) throws Exception;

    public StoresAreaEntity findByAreaId(Integer storeId, String areaId) throws Exception;

    public void flush() throws Exception;

    public Boolean saveAll(List<StoresAreaEntity> stores) throws Exception;

    public void deleteAll(Integer storeId) throws Exception;

    public void updateAll(List<StoresAreaEntity> storeAreas) throws Exception;

}
