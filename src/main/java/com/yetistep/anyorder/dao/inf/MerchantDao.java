package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.MerchantEntity;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.util.YSException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MerchantDao extends GenericDaoService<Integer, MerchantEntity> {

    public MerchantEntity findById(String merchantId) throws Exception;

    public Boolean emailExist(String email) throws Exception;

    public Boolean businessTitleExist(String businessTitle) throws Exception;

    public Integer getTrueId(String fakeId) throws Exception;

    public Integer getNumberOfMerchant(Page page) throws Exception;

    public List<MerchantEntity> getMerchantList(Page page) throws Exception;
}
