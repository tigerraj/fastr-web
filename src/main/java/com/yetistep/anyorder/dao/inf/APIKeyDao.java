package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.APIKeyEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface APIKeyDao extends GenericDaoService<Integer, APIKeyEntity> {

    boolean isUniqueKey(String apiKey) throws Exception;

    List<APIKeyEntity> findActiveAPIKeys(Integer merchantId) throws Exception;

    APIKeyEntity findByAPIKey(String apiKey) throws Exception;

}
