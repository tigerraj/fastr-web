package com.yetistep.anyorder.dao.inf;


import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.StoreBrandEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface StoreBrandDao extends GenericDaoService<Integer, StoreBrandEntity> {

    public StoreBrandEntity findById(String storeBrandId) throws Exception;

    public Boolean checkBrandNameExist(String storeBrandName) throws Exception;

    public Integer getNumberOfMerchantStoreBrand(String merchantId, Page page) throws Exception;

    public List<StoreBrandEntity> getMerchantStoreBrandList(String merchantId, Page page) throws Exception;

    public Integer getTotalNumberOfStoreByStatus(String merchantId, Status status) throws Exception;

    public List<StoreBrandEntity> getStoreBrandListByStatus(String merchantId, Page page) throws Exception;

    public Integer getNumberOfMerchantsStoreBrands(List<String> merchantIds) throws Exception;

    public List<StoreBrandEntity> getMerchantsStoreBrands(List<String> merchantIds, Page page) throws Exception;
}
