package com.yetistep.anyorder.dao.inf;

import com.yetistep.anyorder.abs.GenericDaoService;
import com.yetistep.anyorder.model.DriversWorkLogEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:02 AM
 * To change this template use File | Settings | File Templates.
 */
public interface DriversWorkLogDao extends GenericDaoService<Integer, DriversWorkLogEntity> {

    public DriversWorkLogEntity findById(String driversWorkLogId) throws Exception;

    public DriversWorkLogEntity findByDriverWithNoToDate(Integer driverId) throws Exception;

    public DriversWorkLogEntity findLastLoginOfDriver(String driverId) throws Exception;
}
