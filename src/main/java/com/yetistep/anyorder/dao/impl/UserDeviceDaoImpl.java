package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.UserDeviceDao;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.UserDeviceEntity;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/17/16
 * Time: 9:50 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class UserDeviceDaoImpl implements UserDeviceDao {


    private static final Logger log = Logger.getLogger(UserDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public UserDeviceEntity find(Integer id) throws Exception {
        return (UserDeviceEntity) getCurrentSession().get(UserDeviceEntity.class, id);
    }

    @Override
    public List<UserDeviceEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(UserDeviceEntity.class).list();
    }

    @Override
    public Boolean save(UserDeviceEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(UserDeviceEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(UserDeviceEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public UserDeviceEntity findByEntityId(String entityId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(UserDeviceEntity.class);
        criteria.add(Restrictions.eq("userDeviceId", entityId));
        return (UserDeviceEntity) criteria.uniqueResult();
    }

    @Override
    public UserDeviceEntity findByAccessToken(String accessToken) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(UserDeviceEntity.class);
        criteria.add(Restrictions.eq("accessToken", accessToken));
        return (UserDeviceEntity) criteria.uniqueResult();
    }

    @Override
    public List<String> getAllDeviceTokensForFamilyAndRole(UserRole role, String family) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(UserDeviceEntity.class);
        criteria.createAlias("user","user");
        criteria.add(Restrictions.eq("user.userRole", role));
        if(family.equals("IOS")) {
            criteria.add(Restrictions.or(Restrictions.ilike("family", "%"+family+"%"), Restrictions.ilike("family", "%UNKNOWN%"), Restrictions.ilike("family", "%MAC_OS_X%")));
        }else{
            criteria.add(Restrictions.ilike("family", "%"+family+"%"));
        }
        criteria.add(Restrictions.isNotNull("deviceToken"));
        criteria.setProjection(Projections.distinct(Projections.property("deviceToken")));
        return (List<String>) criteria.list();
    }

    @Override
    public List<String> findDeviceTokenByUserId(Integer userId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(UserDeviceEntity.class)
                .createAlias("user","user")
                .add(Restrictions.eq("user.id", userId))
                .add(Restrictions.isNotNull("deviceToken"))
                .add(Restrictions.isNotNull("accessToken"))
                .setProjection(Projections.distinct(Projections.property("deviceToken")));
        return (List<String>) criteria.list();
    }

    @Override
    public List<String> findDeviceTokenByStoreId(Integer storeId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(UserDeviceEntity.class)
                .createAlias("user", "user")
                .createAlias("user.storeManager", "sm")
                .createAlias("sm.store", "store")
                .add(Restrictions.eq("user.userRole", UserRole.ROLE_STORE_MANAGER))
                .add(Restrictions.eq("store.id", storeId))
                .add(Restrictions.isNotNull("deviceToken"))
                .setProjection(Projections.distinct(Projections.property("deviceToken")));
        return (List<String>) criteria.list();
    }

    public UserDeviceEntity findByUserId(Integer userId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(UserDeviceEntity.class)
                .createAlias("user", "user")
                .add(Restrictions.eq("user.id", userId))
                .add(Restrictions.isNotNull("deviceToken"))
                .add(Restrictions.isNotNull("accessToken"));
        return (UserDeviceEntity) criteria.uniqueResult();
    }

}
