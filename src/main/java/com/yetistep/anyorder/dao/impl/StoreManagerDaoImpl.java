package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.StoreManagerDao;
import com.yetistep.anyorder.model.StoreManagerEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by BinaySingh on 5/18/2016.
 */
@Repository
public class StoreManagerDaoImpl implements StoreManagerDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public StoreManagerEntity find(Integer id) throws Exception {
        return (StoreManagerEntity) getCurrentSession().get(StoreManagerEntity.class, id);
    }

    @Override
    public List<StoreManagerEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(StoreManagerEntity.class).list();
    }

    @Override
    public Boolean save(StoreManagerEntity value) throws Exception {
        getCurrentSession().persist(value);
        return Boolean.TRUE;
    }

    @Override
    public Boolean update(StoreManagerEntity value) throws Exception {
        getCurrentSession().persist(value);
        return Boolean.TRUE;
    }

    @Override
    public Boolean delete(StoreManagerEntity value) throws Exception {
        return null;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public StoreManagerEntity findByFakeId(String fakeId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreManagerEntity.class);
        criteria.add(Restrictions.eq("storeManagerId", fakeId));
        return (StoreManagerEntity) criteria.uniqueResult();
    }

    @Override
    public StoreManagerEntity findById(Integer id, String fakeId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreManagerEntity.class);
        criteria.add(Restrictions.eq("id", id));
        criteria.add(Restrictions.eq("storeManagerId", fakeId));
        return (StoreManagerEntity) criteria.uniqueResult();
    }
}
