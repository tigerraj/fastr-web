package com.yetistep.anyorder.dao.impl;

//import com.yetistep.anyorder.HibernateSessionFactory;
import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.enums.UserDeviceType;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.enums.UserStatus;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.util.HibernateUtil;
import com.yetistep.anyorder.util.YSException;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserDaoImpl implements UserDao {

    private static final Logger log = Logger.getLogger(UserDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;
    
    @Override
    public UserEntity findByVerificationCode(String code) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("verificationCode", code));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity find(Integer id) throws Exception {
        return (UserEntity) getCurrentSession().get(UserEntity.class, id);
    }

    @Override
    public List<UserEntity> findAll() throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean save(UserEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }


    @Override
    public Boolean update(UserEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(UserEntity value) throws Exception {
        return true;

    }

    @Override
    public Session getCurrentSession() throws Exception {
        return getSessionFactory().getCurrentSession();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public UserEntity findByUserName(String userName) {
        UserEntity userEntity=null;
        try {
//            final Session session= HibernateSessionFactory.getInstance().getSessionFactory().getCurrentSession();
//            final Transaction tx=session.beginTransaction();
            Criteria criteria = getCurrentSession().createCriteria(UserEntity.class);
            criteria.add(Restrictions.and(Restrictions.or(Restrictions.eq("username", userName), Restrictions.eq("mobileNumber", userName)), Restrictions.eq("deviceType", UserDeviceType.WEB)));
            userEntity = (UserEntity) criteria.uniqueResult();
//            tx.commit();
//            session.close();
        } catch (Exception e) {
            try {
                throw e;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return userEntity;
    }

    @Override
    public UserEntity findStoreManagerByUsernameMobile(String userName) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.and(Restrictions.or(Restrictions.eq("username", userName), Restrictions.eq("mobileNumber", userName)), Restrictions.eq("deviceType", UserDeviceType.WEB)));
        criteria.add(Restrictions.eq("userRole", UserRole.ROLE_STORE_MANAGER));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity findStoreManagerByUsernameMobileNo(String userName) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.and(Restrictions.or(Restrictions.eq("username", userName), Restrictions.eq("mobileNumber", userName))));
        criteria.add(Restrictions.eq("userRole", UserRole.ROLE_STORE_MANAGER));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity findByUsernameMobile(String userName) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.and(Restrictions.or(Restrictions.eq("username", userName), Restrictions.eq("mobileNumber", userName)), Restrictions.eq("deviceType", UserDeviceType.MOBILE)));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity findCustomerByNameAndMobileNumber(String firstName, String lastName, String mobileNumber) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.and(Restrictions.eq("firstName", firstName), Restrictions.eq("lastName", lastName), Restrictions.eq("mobileNumber", mobileNumber), Restrictions.eq("userRole", UserRole.ROLE_CUSTOMER)));
        return  (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity findByUserId(String userId) throws Exception{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("userId", userId));
        return  (UserEntity) criteria.uniqueResult();
    }


    @Override
    public UserEntity findByMobileNumberAndRole(String mobileNumber, UserRole userRole) {
        List<UserEntity> usersList = new ArrayList<>();
        try {
            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
            criteria.add(Restrictions.eq("mobileNumber", mobileNumber));
            criteria.add(Restrictions.eq("userRole", userRole));
            usersList = criteria.list();
        } catch (Exception e) {
            throw e;
        }

        return usersList.size() > 0 ? usersList.get(0) : null;
    }

    @Override
    public Boolean checkIfEmailExistsWithSameRole(String emailAddress, UserRole userRole) throws YSException{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("emailAddress", emailAddress));
        criteria.add(Restrictions.eq("userRole", userRole));
        return (criteria.list().size()>0) ? true : false;
    }

    @Override
    public Boolean checkIfEmailExistsWithSameRole(String emailAddress, UserRole userRole, Integer userId) throws YSException{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("emailAddress", emailAddress));
        criteria.add(Restrictions.eq("userRole", userRole));
        criteria.add(Restrictions.ne("id", userId));
        return (criteria.list().size()>0) ? true : false;
    }

    @Override
    public Boolean checkIfEmailExists(String emailAddress) throws YSException{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("emailAddress", emailAddress));
        return (criteria.list().size()>0) ? true : false;
    }

    @Override
    public Boolean checkIfMobileNumberExists(String mobileNumber, UserRole userRole) throws YSException{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("mobileNumber", mobileNumber));
        criteria.add(Restrictions.eq("userRole", userRole));
        return (criteria.list().size()>0) ? true : false;
    }

    @Override
    public Boolean checkIfMobileNumberExists(String mobileNumber, UserRole userRole, Integer userId) throws YSException {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("mobileNumber", mobileNumber));
        criteria.add(Restrictions.eq("userRole", userRole));
        criteria.add(Restrictions.ne("id", userId));
        return (criteria.list().size()>0) ? true : false;
    }

    @Override
    public UserEntity getUserByMobileNumber(String mobileNumber, UserRole userRole) throws YSException{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("mobileNumber", mobileNumber));
        criteria.add(Restrictions.eq("userRole", userRole));
        return (UserEntity)((criteria.list().size()>0) ? criteria.list().get(0) : null);
    }

    @Override
    public UserEntity findById(Integer userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("id", userId));
        return (UserEntity)criteria.uniqueResult();
    }

    @Override
    public Boolean checkUserNameExist(String username) throws Exception{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("username", username));
        return (criteria.list().size()>0) ? true : false;
    }

    @Override
    public List<UserEntity> getAllManager() throws Exception{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("userRole", UserRole.ROLE_MANAGER));
        return (List<UserEntity>) criteria.list();
    }

    @Override
    public UserEntity findStoreManagerByUserName(String userName) throws Exception{
        List<UserEntity> usersList = new ArrayList<>();
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.or(Restrictions.eq("username", userName),Restrictions.eq("mobileNumber",userName)));
        criteria.add(Restrictions.eq("role", UserRole.ROLE_STORE_MANAGER));
        usersList = criteria.list();
        return usersList.size() > 0 ? usersList.get(0) : null;
    }

    //currently not used
    @Override
    public List<UserEntity> findCustomersByMobileNumber(String mobileNumber) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class, "user");
        criteria.add(Restrictions.eq("mobileNumber", mobileNumber));
        criteria.add(Restrictions.eq("userRole", UserRole.ROLE_CUSTOMER));

        return (List<UserEntity>)criteria.list();
    }

    @Override
    public UserEntity findByDriverId(String driverId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.createAlias("driver","driver");
        criteria.add(Restrictions.eq("driver.driverId", driverId));
        criteria.add(Restrictions.eq("userRole", UserRole.ROLE_DRIVER));
        return (UserEntity)criteria.uniqueResult();
    }

    @Override
    public Integer getNumberOfUsers(Page page) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.or(Restrictions.eq("userRole", UserRole.ROLE_MANAGER), Restrictions.eq("userRole", UserRole.ROLE_CSR)));
        HibernateUtil.fillPaginationCriteria(criteria, page, UserEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<UserEntity> getUsers(Page page) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.or(Restrictions.eq("userRole", UserRole.ROLE_MANAGER), Restrictions.eq("userRole", UserRole.ROLE_CSR)));
        HibernateUtil.fillPaginationCriteria(criteria, page, UserEntity.class);
        return criteria.list();
    }

    @Override
    public List<UserEntity> findActiveMerchant() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class)
                .add(Restrictions.eq("userRole", UserRole.ROLE_MERCHANT))
                .add(Restrictions.eq("userStatus", UserStatus.ACTIVE));
        return criteria.list();
    }

    @Override
    public UserEntity findDriverByUserName(String userName) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
        criteria.add((Restrictions.or(Restrictions.eq("username", userName), Restrictions.eq("mobileNumber", userName), Restrictions.eq("emailAddress", userName))));
        criteria.add(Restrictions.eq("userRole", UserRole.ROLE_DRIVER));
        return (UserEntity) criteria.uniqueResult();
    }
}
