package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.RatePlanDao;
import com.yetistep.anyorder.dao.inf.UnknownAreaRatePlanDao;
import com.yetistep.anyorder.model.RatePlanEntity;
import com.yetistep.anyorder.model.UnknownAreaRatePlanEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class UnknownAreaRatePlanDaoImpl implements UnknownAreaRatePlanDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public UnknownAreaRatePlanEntity find(Integer id) throws Exception {
        return (UnknownAreaRatePlanEntity) getCurrentSession().get(UnknownAreaRatePlanEntity.class, id);
    }

    @Override
    public List<UnknownAreaRatePlanEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(UnknownAreaRatePlanEntity.class).list();
    }

    @Override
    public Boolean save(UnknownAreaRatePlanEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(UnknownAreaRatePlanEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(UnknownAreaRatePlanEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return getSessionFactory().getCurrentSession();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public UnknownAreaRatePlanEntity findById(String planId) throws Exception {
        return (UnknownAreaRatePlanEntity) getCurrentSession().createCriteria(UnknownAreaRatePlanEntity.class)
                .add(Restrictions.eq("unknownAreaRatePlanId",planId)).uniqueResult();
    }
}
