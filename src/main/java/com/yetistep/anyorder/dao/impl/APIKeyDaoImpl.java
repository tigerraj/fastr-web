package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.APIKeyDao;
import com.yetistep.anyorder.dao.inf.AreaDao;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.model.APIKeyEntity;
import com.yetistep.anyorder.model.AreaEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class APIKeyDaoImpl implements APIKeyDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public APIKeyEntity find(Integer id) throws Exception {
        return (APIKeyEntity) getCurrentSession().get(APIKeyEntity.class, id);
    }

    @Override
    public List<APIKeyEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(AreaEntity.class).list();
    }

    @Override
    public Boolean save(APIKeyEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(APIKeyEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(APIKeyEntity value) throws Exception {
        getCurrentSession().delete(value);
        return true;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public boolean isUniqueKey(String apiKey) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(APIKeyEntity.class).add(Restrictions.eq("apiKey", apiKey));
        return criteria.list().size() > 0 ? false : true;
    }

    @Override
    public List<APIKeyEntity> findActiveAPIKeys(Integer merchantId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(APIKeyEntity.class).
                add(Restrictions.eq("merchant.id", merchantId))
                .setProjection(Projections.projectionList()
                        .add(Projections.property("id"),"id")
                        .add(Projections.property("createdDate"),"createdDate")
                        .add(Projections.property("apiKey"),"apiKey")
                        .add(Projections.property("apiName"),"apiName")
                );
        criteria.setResultTransformer(Transformers.aliasToBean(APIKeyEntity.class));
        return criteria.list();
    }

    @Override
    public APIKeyEntity findByAPIKey(String apiKey) throws Exception {
        return (APIKeyEntity) getCurrentSession().createCriteria(APIKeyEntity.class).add(Restrictions.eq("apiKey",apiKey)).uniqueResult();

    }
}
