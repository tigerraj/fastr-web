package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.NotificationDao;
import com.yetistep.anyorder.enums.NotifyTo;
import com.yetistep.anyorder.model.NotificationEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class NotificationDaoImpl implements NotificationDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public NotificationEntity find(Long id) throws Exception {
        return (NotificationEntity) getCurrentSession().get(NotificationEntity.class,id);
    }

    @Override
    public List<NotificationEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(NotificationEntity.class).list();
    }

    @Override
    public Boolean save(NotificationEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(NotificationEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(NotificationEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public NotificationEntity findByNotificationId(String notificationId) throws Exception {
        return (NotificationEntity) getCurrentSession().createCriteria(NotificationEntity.class)
                .add(Restrictions.eq("notificationId", notificationId)).uniqueResult();
    }

    @Override
    public List<NotificationEntity> findDriverNotification(NotifyTo notifyTo) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date today = calendar.getTime();
        Criteria criteria = getCurrentSession().createCriteria(NotificationEntity.class)
                .add(Restrictions.eq("notifyTo", notifyTo))
                .add(Restrictions.ge("validTill", today));
        return  criteria.list();
    }

    @Override
    public List<NotificationEntity> findMerchantNotification(Integer userId) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date today = calendar.getTime();
        Criteria criteria = getCurrentSession().createCriteria(NotificationEntity.class)
                .add(Restrictions.eq("user.id", userId))
                .add(Restrictions.ge("validTill", today))
                .addOrder(Order.desc("createdDate"));
        return criteria.list();
    }

    @Override
    public List<NotificationEntity> findWebNotification() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date today = calendar.getTime();
        Criteria criteria = getCurrentSession().createCriteria(NotificationEntity.class)
                .add(Restrictions.isNotNull("user.id"))
                .add(Restrictions.ge("validTill", today))
                .addOrder(Order.desc("createdDate"));
        return criteria.list();
    }

    @Override
    public List<NotificationEntity> findMobileNotification() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date today = calendar.getTime();
        Criteria criteria = getCurrentSession().createCriteria(NotificationEntity.class)
                .add(Restrictions.isNull("user.id"))
                .add(Restrictions.ge("validTill", today))
                .addOrder(Order.desc("createdDate"));
        return criteria.list();
    }
}
