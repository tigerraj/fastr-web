package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.MerchantDao;
import com.yetistep.anyorder.model.MerchantEntity;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class MerchantDaoImpl implements MerchantDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public MerchantEntity find(Integer id) throws Exception {
        return (MerchantEntity) getCurrentSession().get(MerchantEntity.class,id);
    }

    @Override
    public List<MerchantEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(MerchantEntity.class).list();
    }

    @Override
    public Boolean save(MerchantEntity value) throws Exception {
        getCurrentSession().persist(value);
        return Boolean.TRUE;
    }

    @Override
    public Boolean update(MerchantEntity value) throws Exception {
        getCurrentSession().persist(value);
        return Boolean.TRUE;
    }

    @Override
    public Boolean delete(MerchantEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    public MerchantEntity findById(String merchantId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(MerchantEntity.class)
                .add(Restrictions.eq("merchantId",merchantId));
        return (MerchantEntity) criteria.uniqueResult();
    }

    @Override
    public Boolean emailExist(String email) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(MerchantEntity.class);
        criteria.createAlias("user", "u");
        criteria.add(Restrictions.eq("u.username", email));
        return criteria.list().size() > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Boolean businessTitleExist(String businessTitle) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(MerchantEntity.class);
        criteria.add(Restrictions.eq("businessTitle", businessTitle));
        return criteria.list().size() > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Integer getTrueId(String fakeId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(MerchantEntity.class);
        criteria.add(Restrictions.eq("merchantId", fakeId));
        criteria.setProjection(Projections.property("id"));
        return ((Integer) criteria.uniqueResult());
    }

    @Override
    public Integer getNumberOfMerchant(Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(MerchantEntity.class);
        HibernateUtil.fillPaginationCriteria(criteria, page, MerchantEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<MerchantEntity> getMerchantList(Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(MerchantEntity.class);
        HibernateUtil.fillPaginationCriteria(criteria, page, MerchantEntity.class);
        return criteria.list();
    }
}
