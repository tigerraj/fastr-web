package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.DriversAreaDao;
import com.yetistep.anyorder.dao.inf.StoreAreaDao;
import com.yetistep.anyorder.model.DriversAreaEntity;
import com.yetistep.anyorder.model.StoresAreaEntity;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class DriversAreaDaoImpl implements DriversAreaDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public DriversAreaEntity find(Integer id) throws Exception {
        return (DriversAreaEntity) getCurrentSession().get(DriversAreaEntity.class,id);
    }

    @Override
    public List<DriversAreaEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(DriversAreaEntity.class).list();
    }

    @Override
    public Boolean save(DriversAreaEntity value) throws Exception {
       getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(DriversAreaEntity value) throws Exception {
         getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(DriversAreaEntity value) throws Exception {
           return null;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public DriversAreaEntity findById(String driversAreaId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(DriversAreaEntity.class)
                .add(Restrictions.eq("driversAreaId",driversAreaId));
        return (DriversAreaEntity) criteria.uniqueResult();
    }

    @Override
    public DriversAreaEntity findByDriverAndAreaId(Integer driverId, Integer areaId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(DriversAreaEntity.class)
                .add(Restrictions.eq("id",driverId))
                .add(Restrictions.eq("area.id",areaId));
        return (DriversAreaEntity) criteria.uniqueResult();
    }


    public void flush() throws Exception{
        this.getCurrentSession().flush();
        this.getCurrentSession().clear();
    }

    public void deleteAll(Integer driverId) throws Exception {
        Query query=getCurrentSession().createQuery("delete from DriversAreaEntity where driver.id=:driverId");
        query.setParameter("driverId",driverId);
        query.executeUpdate();
    }

    public void updateAll(List<DriversAreaEntity> driversAreas) throws Exception {
        int i=0;
        for(DriversAreaEntity driversArea: driversAreas){
            getCurrentSession().save(driversArea);
            if(++i%20==0)
                getCurrentSession().flush();
        }
    }
}