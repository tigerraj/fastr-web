package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.AreaDao;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.model.AreaEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class AreaDaoImpl implements AreaDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public AreaEntity find(Integer id) throws Exception {
        return (AreaEntity) getCurrentSession().get(AreaEntity.class,id);
    }

    @Override
    public List<AreaEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(AreaEntity.class).list();
    }

    @Override
    public Boolean save(AreaEntity value) throws Exception {
       getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(AreaEntity value) throws Exception {
         getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(AreaEntity value) throws Exception {
        getCurrentSession().delete(value);
        return true;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public AreaEntity findById(String areaId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(AreaEntity.class)
                .add(Restrictions.eq("areaId",areaId));
        return (AreaEntity) criteria.uniqueResult();
    }

    public List<AreaEntity> findAreaIds() throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(AreaEntity.class)
                .add(Restrictions.eq("status",Status.ACTIVE));
        return criteria.list();
    }

    @Override
    public List<AreaEntity> findParentAreas() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(AreaEntity.class)
                .add(Restrictions.isNull("parent"));
        return criteria.list();
    }

    @Override
    public List<AreaEntity> findActiveParentAreas() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(AreaEntity.class)
                .add(Restrictions.isNull("parent"))
                .add(Restrictions.eq("status", Status.ACTIVE));
        return criteria.list();
    }

    @Override
    public List<AreaEntity> findParentAreasByAreaId(String areaId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(AreaEntity.class)
                .createAlias("parent","parent")
                .add(Restrictions.eq("parent.areaId", areaId));
        return criteria.list();
    }

    public Boolean checkAreaName(String areaName) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(AreaEntity.class)
                .add(Restrictions.eq("areaName", areaName));
        return (criteria.list().size() > 0 ? true : false);

    }

    @Override
    public AreaEntity findByAreaId(String areaId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(AreaEntity.class)
                .add(Restrictions.eq("areaId",areaId));
        return (AreaEntity) criteria.uniqueResult();
    }
}
