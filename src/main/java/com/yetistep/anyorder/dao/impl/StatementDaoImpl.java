package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.StatementDao;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.StatementEntity;
import com.yetistep.anyorder.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class StatementDaoImpl implements StatementDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public StatementEntity find(Long id) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<StatementEntity> findAll() throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean save(StatementEntity value) throws Exception {
        sessionFactory.getCurrentSession().persist(value);  //To change body of implemented methods use File | Settings | File Templates.
        return true;
    }

    @Override
    public Boolean update(StatementEntity value) throws Exception {
        sessionFactory.getCurrentSession().persist(value);
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean delete(StatementEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Integer getNumberOfMerchantStatements(String merchantId, Page page, Date fromDate, Date toDate) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StatementEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");

        criteria.add(Restrictions.eq("m.merchantId", merchantId));
        if (fromDate != null && toDate != null) {
            criteria.add(Restrictions.between("generatedDate", fromDate, toDate));
        }
        HibernateUtil.fillPaginationCriteria(criteria, page, StatementEntity.class);

        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<StatementEntity> getMerchantStatements(String merchantId, Page page, Date fromDate, Date toDate) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StatementEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");

        criteria.add(Restrictions.eq("m.merchantId", merchantId));
        if (fromDate != null && toDate != null) {
            criteria.add(Restrictions.between("generatedDate", fromDate, toDate));
        }
        HibernateUtil.fillPaginationCriteria(criteria, page, StatementEntity.class);

        return criteria.list();
    }

    @Override
    public Integer getNumberOfStoreStatements(String storeId, Page page, Date fromDate, Date toDate) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StatementEntity.class);
        criteria.createAlias("store", "s");

        criteria.add(Restrictions.eq("s.storeId", storeId));
        if (fromDate != null && toDate != null) {
            criteria.add(Restrictions.between("generatedDate", fromDate, toDate));
        }
        HibernateUtil.fillPaginationCriteria(criteria, page, StatementEntity.class);

        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<StatementEntity> getStoreStatements(String storeId, Page page, Date fromDate, Date toDate) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StatementEntity.class);
        criteria.createAlias("store", "s");

        criteria.add(Restrictions.eq("s.storeId", storeId));
        if (fromDate != null && toDate != null) {
            criteria.add(Restrictions.between("generatedDate", fromDate, toDate));
        }
        HibernateUtil.fillPaginationCriteria(criteria, page, StatementEntity.class);

        return criteria.list();
    }

    @Override
    public Integer getNumberOfAllStatements(Page page, Date fromDate, Date toDate) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StatementEntity.class);
        if (fromDate != null && toDate != null) {
            criteria.add(Restrictions.between("generatedDate", fromDate, toDate));
        }
        HibernateUtil.fillPaginationCriteria(criteria, page, StatementEntity.class);

        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<StatementEntity> getAllStatements(Page page, Date fromDate, Date toDate) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StatementEntity.class);
        if (fromDate != null && toDate != null) {
            criteria.add(Restrictions.between("generatedDate", fromDate, toDate));
        }
        HibernateUtil.fillPaginationCriteria(criteria, page, StatementEntity.class);

        return criteria.list();
    }

    @Override
    public StatementEntity findByStatementId(String statementId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StatementEntity.class);
        criteria.add(Restrictions.eq("statementId", statementId));
        return (StatementEntity) criteria.uniqueResult();
    }
}
