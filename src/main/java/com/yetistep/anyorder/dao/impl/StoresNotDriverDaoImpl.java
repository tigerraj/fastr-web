package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.DriversAreaDao;
import com.yetistep.anyorder.dao.inf.StoresNotDriverDao;
import com.yetistep.anyorder.model.DriversAreaEntity;
import com.yetistep.anyorder.model.StoresNotDriverEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class StoresNotDriverDaoImpl implements StoresNotDriverDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public StoresNotDriverEntity find(Integer id) throws Exception {
        return (StoresNotDriverEntity) getCurrentSession().get(StoresNotDriverEntity.class,id);
    }


    @Override
    public List<StoresNotDriverEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(StoresNotDriverEntity.class).list();
    }

    @Override
    public Boolean save(StoresNotDriverEntity value) throws Exception {
       getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(StoresNotDriverEntity value) throws Exception {
         getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(StoresNotDriverEntity value) throws Exception {
           return null;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public StoresNotDriverEntity findById(String driversAreaId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(StoresNotDriverEntity.class)
                .add(Restrictions.eq("driversAreaId",driversAreaId));
        return (StoresNotDriverEntity) criteria.uniqueResult();
    }

    @Override
    public StoresNotDriverEntity findByDriverAndStoreId(Integer driverId, Integer storeId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(StoresNotDriverEntity.class)
                .add(Restrictions.eq("driver.id",driverId))
                .add(Restrictions.eq("store.id",storeId));
        return (StoresNotDriverEntity) criteria.uniqueResult();
    }
}