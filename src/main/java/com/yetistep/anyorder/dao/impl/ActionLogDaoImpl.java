package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.ActionLogDao;
import com.yetistep.anyorder.model.ActionLogEntity;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ActionLogDaoImpl implements ActionLogDao {
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public ActionLogEntity find(Long id) throws Exception {
        return (ActionLogEntity) getCurrentSession().get(ActionLogEntity.class, id);
    }


    //just for testing purpose
    @Override
    public List<ActionLogEntity> findAll() throws Exception {
        return (List<ActionLogEntity>) getCurrentSession().createCriteria(ActionLogEntity.class).list();
    }

    @Override
    public Boolean save(ActionLogEntity value) throws Exception {
        getCurrentSession().persist(value);
        return null;
    }

    @Override
    public void saveAll(List<ActionLogEntity> values) throws Exception {
        Integer i = 0;
        for (ActionLogEntity value: values) {
            getCurrentSession().persist(value);
            if (i % 20 == 0 ) { //20, same as the JDBC batch size
                //flush a batch of inserts and release memory:
                getCurrentSession().flush();
                getCurrentSession().clear();
            }
            i++;
        }
    }

    @Override
    public Boolean update(ActionLogEntity value) throws Exception {
        // TODO
        return null;
    }

    @Override
    public Boolean delete(ActionLogEntity value) throws Exception {
        getCurrentSession().delete(value);
        return true;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<ActionLogEntity> findActionLogPaginated(Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(ActionLogEntity.class);
        HibernateUtil.fillPaginationCriteria(criteria, page, ActionLogEntity.class);
        return (List<ActionLogEntity>) criteria.list();
    }

    @Override
    public Integer getTotalNumberOfActionLogs() throws Exception {
        Criteria criteriaCount = getCurrentSession().createCriteria(ActionLogEntity.class);
        criteriaCount.setProjection(Projections.rowCount());
        Long count = (Long) criteriaCount.uniqueResult();
        return (count != null) ? count.intValue() : null;
    }
}
