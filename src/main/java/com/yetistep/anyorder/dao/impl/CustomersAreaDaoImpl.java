package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.CustomersAreaDao;
import com.yetistep.anyorder.model.CustomersAreaEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/20/16
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CustomersAreaDaoImpl implements CustomersAreaDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public CustomersAreaEntity find(Integer id) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public CustomersAreaEntity findByCustomersAreaId(String  customersAreaId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CustomersAreaEntity.class);
        criteria.add(Restrictions.eq("customersAreaId", customersAreaId));
        return  (CustomersAreaEntity) criteria.uniqueResult();
    }

    @Override
    public List<CustomersAreaEntity> findAll() throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean save(CustomersAreaEntity value) throws Exception {
        getCurrentSession().save(value);
        return true;
    }

    @Override
    public Boolean update(CustomersAreaEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean delete(CustomersAreaEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }
}
