package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.CustomerDao;
import com.yetistep.anyorder.model.CustomerEntity;
import com.yetistep.anyorder.model.UserEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public CustomerEntity find(Long id) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public CustomerEntity findByCustomerId(String customerId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CustomerEntity.class);
        criteria.add(Restrictions.eq("customerId", customerId));
        return  (CustomerEntity) criteria.uniqueResult();
    }

    @Override
    public List<CustomerEntity> findAll() throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean save(CustomerEntity value) throws Exception {
        getCurrentSession().persist(value);  //To change body of implemented methods use File | Settings | File Templates.
        return true;
    }

    @Override
    public Boolean update(CustomerEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean delete(CustomerEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
       return sessionFactory.getCurrentSession();
    }
}
