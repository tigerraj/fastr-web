package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.DriverDao;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class DriverDaoImpl implements DriverDao {

    private static final Logger log = Logger.getLogger(DriverDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public DriverEntity find(Integer id) throws Exception {
        return (DriverEntity) getCurrentSession().get(DriverEntity.class, id);
    }

    @Override
    public List<DriverEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(DriverEntity.class).list();
    }

    @Override
    public Boolean save(DriverEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(DriverEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(DriverEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public DriverEntity findById(String driverId) throws Exception {
        return (DriverEntity) getCurrentSession().createCriteria(DriverEntity.class)
                .add(Restrictions.eq("driverId",driverId)).uniqueResult();
    }

    @Override
    public List<DriverEntity> getSelectedDriver(Integer storeId, Integer areaId, StorePriority priority, String maxCount, Integer driverLocationUpdateTimeInMinute, Integer driverOffsetTime ) throws Exception {
        /*List<Integer> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_STARTED.ordinal());
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal());
        orderStatuses.add(OrderStatus.AT_STORE.ordinal());
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER.ordinal());
        orderStatuses.add(OrderStatus.AT_CUSTOMER.ordinal());
        String experienceQuery = "";
        String priorityQuery = "";

        if(priority.equals(StorePriority.PRIORITIZED)){
            experienceQuery = " and d.experience_level= :experienceLevel ";
            priorityQuery = " and o.prioritized= :orderPrioritized ";
        }
        String sqQuery =    "select d.id, d.experience_level as experienceLevel,  d.latitude, d.longitude, (select count(id) from orders o" +
                " where driver_id = d.id "+priorityQuery+" and o.order_status in(:orderStatus) ) as previousOrderCount, (select sum(etd) from orders o where driver_id = d.id and o.order_status in(:orderStatus)) as totalPreviousOrderEtd " +
                "from drivers d left join users u on (d.user_id = u.id) where u.user_status =:userStatus and d.available_status =:availableStatus and d.driver_type =:driverType and d.last_location_update>=(DATE_SUB(now(), INTERVAL :driverLocationUpdateTimeInMinute MINUTE )) "+experienceQuery +
                "and d.id not in (select driver_id from stores_not_drivers where store_id =:storeId) and d.id in (select driver_id from drivers_areas where area_id =:areaId) "+
                "and (select count(id) from orders o where driver_id = d.id "+priorityQuery+" and o.order_status in(:orderStatus) ) <= :maxCount";
        Query query = getCurrentSession().createSQLQuery(sqQuery)
                .addScalar("id", IntegerType.INSTANCE)
                .addScalar("experienceLevel", HibernateUtil.getCustomEnumType("DriverExperienceLevel"))
                .addScalar("latitude", StringType.INSTANCE)
                .addScalar("longitude", StringType.INSTANCE)
                .addScalar("previousOrderCount", IntegerType.INSTANCE)
                .addScalar("totalPreviousOrderEtd", BigDecimalType.INSTANCE);

        query.setParameter("availableStatus", AvailabilityStatus.AVAILABLE.ordinal());
        query.setParameter("storeId","storeId");
        query.setParameter("userStatus", UserStatus.ACTIVE.ordinal());
        query.setParameterList("orderStatus", orderStatuses);
        query.setParameter("driverType", DriverType.FASTR.ordinal());
        query.setParameter("areaId", areaId);
        query.setParameter("maxCount", Integer.parseInt(maxCount));
        query.setParameter("driverLocationUpdateTimeInMinute", driverLocationUpdateTimeInMinute);

        if(priority.equals(StorePriority.PRIORITIZED)){
            query.setParameter("experienceLevel", DriverExperienceLevel.EXPERIENCED.ordinal());
            query.setParameter("orderPrioritized", 1);
        }
        query.setResultTransformer(Transformers.aliasToBean(DriverEntity.class));
        return  (List< DriverEntity >) query.list();*/

        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, -(driverLocationUpdateTimeInMinute+ driverOffsetTime));
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("user", "user");
        criteria.add(Restrictions.eq("driverType", DriverType.FASTR));
        criteria.add(Restrictions.eq("user.userStatus", UserStatus.ACTIVE));
        criteria.add(Restrictions.eq("availabilityStatus", AvailabilityStatus.AVAILABLE));
        if (priority.equals(StorePriority.PRIORITIZED)) {
            criteria.add(Restrictions.eq("experienceLevel", DriverExperienceLevel.EXPERIENCED));
        }
        criteria.add(Restrictions.ge("lastLocationUpdate", c.getTime()));
        return criteria.list();
    }

    @Override
    public Integer getDriversOrderCount (String driverId) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_PLACED);
        orderStatuses.add(OrderStatus.ORDER_STARTED);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatuses.add(OrderStatus.AT_STORE);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatuses.add(OrderStatus.AT_CUSTOMER);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .createAlias("driver", "driver")
                .add(Restrictions.eq("driver.driverId", driverId))
                .add(Restrictions.in("orderStatus", orderStatuses))
                .setProjection(Projections.rowCount());
        /*String sqQuery =    "select  count(o.id) from orders o LEFT JOIN orders_drivers od on(o.id = od.driver_id) where o.order_status in(:orderStatus) AND od.driver_id =:driverId AND od.driver_status=:driverStatus";
        Query query = getCurrentSession().createSQLQuery(sqQuery);
        query.setParameterList("orderStatus", orderStatuses);
        query.setParameter("driverId", driverId);
        query.setParameter("driverStatus", OrdersDriverStatus.RUNNING);*/

        return ((Long) criteria.uniqueResult()).intValue();

        //return ((Long) criteria.uniqueResult()).intValue();
    }

    @Override
    public BigDecimal getDriversTotalEtd (String driverId) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_STARTED);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatuses.add(OrderStatus.AT_CUSTOMER);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .createAlias("driver", "driver")
                .add(Restrictions.eq("driver.driverId", driverId))
                .add(Restrictions.in("orderStatus", orderStatuses))
                .setProjection(Projections.sum("estimatedDeliveryTime"));

        BigDecimal etd =  (BigDecimal) criteria.uniqueResult();
        if (etd==null)
            return BigDecimal.ZERO;
        return etd;
    }

    @Override
    public BigDecimal getDriversPreviousOrderTotalEtd (String driverId, Integer id) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_STARTED);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatuses.add(OrderStatus.AT_CUSTOMER);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .createAlias("driver", "driver")
                .add(Restrictions.eq("driver.driverId", driverId))
                .add(Restrictions.in("orderStatus", orderStatuses))
                .add(Restrictions.lt("id", id))
                .setProjection(Projections.sum("estimatedDeliveryTime"));

        BigDecimal etd =  (BigDecimal) criteria.uniqueResult();
        if (etd==null)
            return BigDecimal.ZERO;
        return etd;
    }

    @Override
    public BigDecimal getDriversAvgRating (String driverId) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_STARTED);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatuses.add(OrderStatus.AT_CUSTOMER);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .createAlias("driver", "driver")
                .createAlias("orderRating", "rating")
                .add(Restrictions.eq("driver.driverId", driverId))
                .add(Restrictions.in("orderStatus", orderStatuses))
                .setProjection(Projections.avg("rating.ratingByCustomer"));
        Number rating = (Number) criteria.uniqueResult();
        if (rating != null)
            return BigDecimal.valueOf(rating.doubleValue());
        return BigDecimal.ZERO;
    }

    @Override
    public OrderEntity getDriversLastOrder (String driverId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "d");
        criteria.add(Restrictions.eq("d.driverId", driverId));
        criteria.addOrder(Order.desc("id"));
        criteria.setMaxResults(1);
        List<OrderEntity> orders = (List<OrderEntity>) criteria.list();
        if (orders.size()>0)
            return orders.get(0);
        else
            return null;

    }

    @Override
    public Boolean mobileNumberExists(String mobileNumber) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("user", "u");
        criteria.add(Restrictions.eq("u.username", mobileNumber));
        return criteria.uniqueResult() != null ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Boolean licenseNumberExists(String licenseNumber) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.add(Restrictions.eq("licenseNumber", licenseNumber));
        return criteria.uniqueResult() != null ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Integer getTotalNumberOfStoreDriver(String storeId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public Integer getTotalNumberOfStoreDriverByStatus(String storeId, Page page, AvailabilityStatus availabilityStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        criteria.add(Restrictions.eq("availabilityStatus", availabilityStatus));
        criteria.add(Restrictions.eq("s.allowStoresDriver", Boolean.TRUE));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public Integer getTotalNumberOfAnyOrderDriverByStatus(Page page, AvailabilityStatus availabilityStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.add(Restrictions.isNull("store"));
        criteria.add(Restrictions.eq("availabilityStatus", availabilityStatus));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<DriverEntity> findStoreDriver(String storeId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public List<DriverEntity> findStoreDriverByStatus(String storeId, Page page, AvailabilityStatus availabilityStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        criteria.add(Restrictions.eq("availabilityStatus", availabilityStatus));
        criteria.add(Restrictions.eq("s.allowStoresDriver", Boolean.TRUE));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public List<DriverEntity> findAnyOrderDriverByStatus(Page page, AvailabilityStatus availabilityStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.add(Restrictions.isNull("store"));
        criteria.add(Restrictions.eq("availabilityStatus", availabilityStatus));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getTotalNumberOfStoreBrandDriver(String storeBrandId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.add(Restrictions.eq("sb.storeBrandID", storeBrandId));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<DriverEntity> findStoreBrandDriver(String storeBrandId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.add(Restrictions.eq("sb.storeBrandId", storeBrandId));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getTotalNumberOfMerchantDriver(String merchantId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");
        criteria.add(Restrictions.eq("m.merchantId", merchantId));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public Integer getTotalNumberOfMerchantDriverByStatus(String merchantId, Page page, AvailabilityStatus status) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");
        criteria.add(Restrictions.and(Restrictions.eq("s.status", Status.ACTIVE), Restrictions.eq("sb.status", Status.ACTIVE), Restrictions.eq("m.merchantId", merchantId)));
        criteria.add(Restrictions.eq("s.allowStoresDriver", Boolean.TRUE));
        criteria.add(Restrictions.eq("availabilityStatus", status));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<DriverEntity> findMerchantDriver(String merchantId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");
        criteria.add(Restrictions.eq("m.merchantId", merchantId));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public List<DriverEntity> findMerchantDriverByStatus(String merchantId, Page page, AvailabilityStatus status) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");
        criteria.add(Restrictions.and(Restrictions.eq("s.status", Status.ACTIVE), Restrictions.eq("sb.status", Status.ACTIVE), Restrictions.eq("m.merchantId", merchantId)));
        criteria.add(Restrictions.eq("availabilityStatus", status));
        criteria.add(Restrictions.eq("s.allowStoresDriver", Boolean.TRUE));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getTotalNumberOfAllDriver(Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<DriverEntity> findAll(Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public List<DriverEntity> getAnyOrderDrivers() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class)
                .add(Restrictions.isNull("store.id"));
        return criteria.list();
    }

    @Override
    public List<DriverEntity> findActiveAnyOrderDrivers() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class)
                .createAlias("user", "user")
                .add(Restrictions.isNull("store.id"))
                .add(Restrictions.eq("user.userStatus", UserStatus.ACTIVE));
        return criteria.list();
    }

    @Override
    public List<DriverEntity> findStoreDriverForOrder(String storeId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("user", "u");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        criteria.add(Restrictions.eq("u.userStatus", UserStatus.ACTIVE));
        criteria.add(Restrictions.eq("availabilityStatus", AvailabilityStatus.AVAILABLE));
        return criteria.list();
    }

    @Override
    public List<DriverEntity> findAvailableDriver() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.add(Restrictions.eq("availabilityStatus", AvailabilityStatus.AVAILABLE));
        return criteria.list();
    }

    @Override
    public Integer getTotalNumberOfAnyOrderDriver(Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.add(Restrictions.eq("driverType", DriverType.FASTR));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<DriverEntity> findAnyOrderDriver(Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.add(Restrictions.eq("driverType", DriverType.FASTR));
        HibernateUtil.fillPaginationCriteria(criteria, page, DriverEntity.class);
        return criteria.list();
    }

    @Override
    public List<OrderEntity> getDriversRunningOrderList(String driverId) throws Exception {
        log.info("Getting running orders of driver");
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_PLACED);
        orderStatuses.add(OrderStatus.ORDER_STARTED);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatuses.add(OrderStatus.AT_STORE);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatuses.add(OrderStatus.AT_CUSTOMER);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .createAlias("driver", "driver")
                .add(Restrictions.eq("driver.driverId", driverId))
                .add(Restrictions.in("orderStatus", orderStatuses));
        return criteria.list();
    }

    @Override
    public Integer getNumberOfStoreBrandsDrivers(List<String> storeBrandIds) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.add(Restrictions.in("sb.storeBrandId", storeBrandIds));
        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<DriverEntity> getStoreBrandsDrivers(List<String> storeBrandIds, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(DriverEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.add(Restrictions.in("sb.storeBrandId", storeBrandIds));
        criteria.setFirstResult((page.getPageNumber() - 1) * page.getPageSize());
        criteria.setMaxResults(page.getPageSize());
        return criteria.list();
    }
}
