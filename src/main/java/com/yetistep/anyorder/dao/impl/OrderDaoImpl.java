package com.yetistep.anyorder.dao.impl;


import com.yetistep.anyorder.dao.inf.OrderDao;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.OrderDisplayStatus;
import com.yetistep.anyorder.enums.OrderStatus;
import com.yetistep.anyorder.enums.StorePriority;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.xml.APIData;
import com.yetistep.anyorder.util.HibernateUtil;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class OrderDaoImpl implements OrderDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public OrderEntity find(Integer id) throws Exception {
        return (OrderEntity) getCurrentSession().createCriteria(OrderEntity.class).add(Restrictions.eq("id",id));
    }

    @Override
    public List<OrderEntity> findAll() throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Boolean save(OrderEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(OrderEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(OrderEntity value) throws Exception {
        getCurrentSession().update(value);
        return null;
    }


    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    public void flush() throws Exception{
        this.getCurrentSession().flush();
    }

    @Override
    public OrderEntity findById(String id) throws Exception {
        return (OrderEntity) getCurrentSession().createCriteria(OrderEntity.class)
                .add(Restrictions.eq("orderId", id)).uniqueResult();
    }

    @Override
    public List<OrderEntity> getLowerPriorityOrders(Integer driverId) throws Exception {
            Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
            criteria.add(Restrictions.eq("driver.id", driverId));
            criteria.add(Restrictions.eq("store.priority", StorePriority.NON_PRIORITIZED));
            return (List<OrderEntity>) criteria.list();
    }


    @Override
    public List<OrderEntity> findActiveOrdersOfDriver(String driverId) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_PLACED);
        orderStatuses.add(OrderStatus.ORDER_STARTED);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatuses.add(OrderStatus.AT_STORE);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatuses.add(OrderStatus.AT_CUSTOMER);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "driver");
        criteria.add(Restrictions.eq("driver.driverId", driverId));
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        criteria.addOrder(Order.desc("id"));
        List<OrderEntity> orders = criteria.list();

        /*CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<OrderEntity> cq = cb.createQuery(OrderEntity.class);
        APIData<OrderEntity> root = cq.from(OrderEntity.class);
        cq.select(root);
        List<OrderEntity> orders = getCurrentSession().createQuery(cq).getResultList();*/
        /*Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .createAlias("driver","driver")
                .add(Restrictions.eq("driver.driverId", driverId))
                .add(Restrictions.in("orderStatus",orderStatusList));*/
        /*List<OrderEntity> list=criteria.list();
        session.getTransaction().commit();*/
        return  orders;
    }

    @Override
    public List<OrderEntity> findActiveOrdersOfDriverIds(List<Integer> driverId) throws Exception {
        List<OrderStatus> orderStatusList = new ArrayList<>();
        orderStatusList.add(OrderStatus.ORDER_PLACED);
        orderStatusList.add(OrderStatus.ORDER_STARTED);
        orderStatusList.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatusList.add(OrderStatus.AT_STORE);
        orderStatusList.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatusList.add(OrderStatus.AT_CUSTOMER);
        List<OrderEntity> orders = null;
        if(!driverId.isEmpty()) {
            Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
            criteria.createAlias("driver", "d");
            criteria.add(Restrictions.in("d.id", driverId));
            criteria.add(Restrictions.in("orderStatus", orderStatusList));
            orders = criteria.list();
        }
        return orders;
    }

    @Override
    public Integer getNoOfCurrentOrdersByDriverId(String driverId) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.ORDER_PLACED);
        orderStatuses.add(OrderStatus.ORDER_STARTED);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        orderStatuses.add(OrderStatus.AT_STORE);
        orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        orderStatuses.add(OrderStatus.AT_CUSTOMER);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "driver");
        criteria.add(Restrictions.eq("driver.driverId", driverId));
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        criteria.addOrder(Order.desc("id"));
        return criteria.list().size();
    }

    @Override
    public Integer getNoOfDeliveredOrdersByDriverId(String driverId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "driver");
        criteria.add(Restrictions.eq("driver.driverId", driverId));
        criteria.add(Restrictions.eq("orderStatus", OrderStatus.DELIVERED));
        return criteria.list().size();
    }

    @Override
    public Integer getNoOfPastOrdersByDriverId(String driverId) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.DELIVERED);
        orderStatuses.add(OrderStatus.CANCELLED);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "driver");
        criteria.add(Restrictions.eq("driver.driverId", driverId));
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        criteria.addOrder(Order.desc("id"));
        return criteria.list().size();
    }

    @Override
    public List<OrderEntity> getPastOrdersByDriverId(String driverId, Page page) throws Exception {
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.DELIVERED);
        orderStatuses.add(OrderStatus.CANCELLED);
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "d");
        criteria.add(Restrictions.eq("d.driverId", driverId));
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        criteria.addOrder(Order.desc("id"));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return (List<OrderEntity>) criteria.list();
    }

    @Override
    public BigDecimal getRatingOfDriver(String driverId) throws Exception {
        /*Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "driver");
        criteria.add(Restrictions.eq("driver.driverId",driverId));
        criteria.add(Restrictions.isNotNull("ratingForDriver"));
        criteria.setProjection(Projections.sum("ratingForDriver").as("ratingForDriver"));
        BigDecimal totalRating = (BigDecimal) criteria.uniqueResult();
        Integer count = getCurrentSession().createCriteria(OrderEntity.class)
                .createAlias("driver","driver")
                .add(Restrictions.eq("driver.driverId",driverId))
                .add(Restrictions.isNotNull("ratingForDriver")).list().size();
        if(count.equals(0))
            return BigDecimal.ZERO.setScale(1, RoundingMode.HALF_UP);*/
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "driver");
        criteria.add(Restrictions.eq("driver.driverId",driverId));
        criteria.add(Restrictions.isNotNull("ratingForDriver"));
        criteria.setProjection(Projections.max("duration"));
        Long rating = (Long) criteria.uniqueResult();
        return new BigDecimal(rating);
        //return BigDecimalUtil.checkNull(totalRating).divide(BigDecimal.valueOf(count)).setScale(1, RoundingMode.HALF_UP);
    }

    @Override
    public RequestJsonDto getNumberOfClassifiedOrders(Integer storeId) throws Exception {
       /* String sql = "SELECT waitingOrderNumber, processingOrderNumber, dispatchedOrderNumber "+
                "FROM (SELECT COUNT(id) as waitingOrderNumber FROM orders WHERE order_status = :orderPlaced AND driver_id is null and store_id =:storeId) o1 "+
        "CROSS JOIN (SELECT COUNT(id) as processingOrderNumber FROM orders WHERE order_status in(:processing) AND driver_id is not null and store_id =:storeId) o2 "+
        "CROSS JOIN (SELECT COUNT(id) as dispatchedOrderNumber FROM orders WHERE order_status in(:dispatched) AND driver_id is not null and store_id =:storeId) so";*/

        String sql = "SELECT runningOrderNumber, pastOrderNumber " +
                "FROM (SELECT COUNT(id) as runningOrderNumber FROM orders WHERE order_status in(:running) AND store_id =:storeId) o1 " +
                "CROSS JOIN (SELECT COUNT(id) as pastOrderNumber FROM orders WHERE order_status in(:past) AND store_id =:storeId) o2 ";
        List<Integer> running = new ArrayList<>();
        running.add(OrderStatus.ORDER_PLACED.ordinal());
        running.add(OrderStatus.ORDER_STARTED.ordinal());
        running.add(OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal());
        running.add(OrderStatus.AT_STORE.ordinal());
        running.add(OrderStatus.EN_ROUTE_TO_DELIVER.ordinal());
        running.add(OrderStatus.AT_CUSTOMER.ordinal());

        List<Integer> past = new ArrayList<>();
        past.add(OrderStatus.CANCELLED.ordinal());
        past.add(OrderStatus.DELIVERED.ordinal());

        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("runningOrderNumber", StringType.INSTANCE)
                .addScalar("pastOrderNumber", StringType.INSTANCE);
        query.setParameterList("running", running);
        query.setParameterList("past", past);
        query.setParameter("storeId", storeId);
        query.setResultTransformer(Transformers.aliasToBean(RequestJsonDto.class));
        return (RequestJsonDto)  query.uniqueResult();
    }

    @Override
    public RequestJsonDto getNumberOfClassifiedOrdersWeb(Integer storeId) throws Exception {
        String sql = "SELECT waitingOrderNumber, processingOrderNumber, dispatchedOrderNumber, deliveredOrderNumber, cancelledOrderNumber ";
        if(storeId!=null)
            sql = sql + "FROM (SELECT COUNT(id) as waitingOrderNumber FROM orders WHERE order_status = :orderPlaced AND driver_id is null and store_id =:storeId) o1 ";
        else
            sql = sql + "FROM (SELECT COUNT(id) as waitingOrderNumber FROM orders WHERE order_status = :orderPlaced AND driver_id is null) o1 ";
        if(storeId!=null)
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as processingOrderNumber FROM orders WHERE order_status= :orderProcessing AND driver_id is not null and store_id =:storeId) o2 ";
        else
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as processingOrderNumber FROM orders WHERE order_status= :orderProcessing AND driver_id is not null) o2 ";
        if(storeId!=null)
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as dispatchedOrderNumber FROM orders WHERE order_status= :orderDispatched AND driver_id is not null and store_id =:storeId) o3 ";
        else
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as dispatchedOrderNumber FROM orders WHERE order_status= :orderDispatched AND driver_id is not null) o3 ";
        if(storeId!=null)
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as deliveredOrderNumber FROM orders WHERE order_status= :orderDelivered AND driver_id is not null and store_id =:storeId) o4 ";
        else
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as deliveredOrderNumber FROM orders WHERE order_status= :orderDelivered AND driver_id is not null) o4 ";
        if(storeId!=null)
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as cancelledOrderNumber FROM orders WHERE order_status= :orderCancelled AND driver_id is not null and store_id =:storeId) so";
        else
            sql = sql + "CROSS JOIN (SELECT COUNT(id) as cancelledOrderNumber FROM orders WHERE order_status= :orderCancelled AND driver_id is not null) so";

        List<Integer> processing = new ArrayList<>();
        processing.add(OrderStatus.ORDER_STARTED.ordinal());
        processing.add(OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal());
        processing.add(OrderStatus.AT_STORE.ordinal());

        List<Integer> dispatched = new ArrayList<>();
        dispatched.add(OrderStatus.EN_ROUTE_TO_DELIVER.ordinal());
        dispatched.add(OrderStatus.AT_CUSTOMER.ordinal());

        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("waitingOrderNumber", StringType.INSTANCE)
                .addScalar("processingOrderNumber", StringType.INSTANCE)
                .addScalar("dispatchedOrderNumber", StringType.INSTANCE)
                .addScalar("deliveredOrderNumber", StringType.INSTANCE)
                .addScalar("cancelledOrderNumber", StringType.INSTANCE);
        query.setParameter("orderPlaced", OrderStatus.ORDER_PLACED.ordinal());
        query.setParameterList("orderProcessing", processing);
        query.setParameterList("orderDispatched", dispatched);
        query.setParameter("orderDelivered", OrderStatus.DELIVERED.ordinal());
        query.setParameter("orderCancelled", OrderStatus.CANCELLED.ordinal());
        if(storeId!=null)
            query.setParameter("storeId", storeId);
        query.setResultTransformer(Transformers.aliasToBean(RequestJsonDto.class));
        return (RequestJsonDto)  query.uniqueResult();
    }

    @Override
    public Integer getTotalNumberOfOrdersByStatus(List<Integer> storeId, OrderDisplayStatus orderStatus) throws Exception {

            Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
            /*List<OrderStatus> processing = new ArrayList<>();
            processing.add(OrderStatus.ORDER_STARTED);
        processing.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
            processing.add(OrderStatus.AT_STORE);

            List<OrderStatus> dispatched = new ArrayList<>();
        dispatched.add(OrderStatus.EN_ROUTE_TO_DELIVER);
            dispatched.add(OrderStatus.AT_CUSTOMER);

            List<OrderStatus> waiting = new ArrayList<>();
            waiting.add(OrderStatus.ORDER_PLACED);

            List<OrderStatus> delivered = new ArrayList<>();
            delivered.add(OrderStatus.DELIVERED);

            List<OrderStatus> canceled = new ArrayList<>();
            canceled.add(OrderStatus.CANCELLED);

            if(orderStatus.equals(OrderDisplayStatus.DISPATCHED))
                criteria.add( Restrictions.in("orderStatus", dispatched));
            else if(orderStatus.equals(OrderDisplayStatus.PROCESSING))
                criteria.add( Restrictions.in("orderStatus", processing));
            else if(orderStatus.equals(OrderDisplayStatus.WAITING))
                criteria.add( Restrictions.in("orderStatus", waiting));
            else if(orderStatus.equals(OrderDisplayStatus.DELIVERED))
                criteria.add( Restrictions.in("orderStatus", delivered));
            else if(orderStatus.equals(OrderDisplayStatus.CANCELLED))
                criteria.add( Restrictions.in("orderStatus", canceled));*/
        List<OrderStatus> running = new ArrayList<>();
        running.add(OrderStatus.ORDER_PLACED);
        running.add(OrderStatus.ORDER_STARTED);
        running.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        running.add(OrderStatus.AT_STORE);
        running.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        running.add(OrderStatus.AT_CUSTOMER);

        List<OrderStatus> past = new ArrayList<>();
        past.add(OrderStatus.DELIVERED);
        past.add(OrderStatus.CANCELLED);
        if (orderStatus.equals(OrderDisplayStatus.RUNNING))
            criteria.add(Restrictions.in("orderStatus", running));
        else if (orderStatus.equals(OrderDisplayStatus.PAST))
            criteria.add(Restrictions.in("orderStatus", past));
        if (storeId != null && storeId.size() > 0)
            criteria.add(Restrictions.in("store.id", storeId));
        return ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();

    }

    @Override
    public Integer getTotalNumberOfOrdersByStatusWeb(List<Integer> storeId, OrderDisplayStatus orderStatus) throws Exception {

        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        List<OrderStatus> processing = new ArrayList<>();
        processing.add(OrderStatus.ORDER_STARTED);
        processing.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        processing.add(OrderStatus.AT_STORE);

        processing.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        processing.add(OrderStatus.AT_CUSTOMER);

        List<OrderStatus> waiting = new ArrayList<>();
        waiting.add(OrderStatus.ORDER_PLACED);

        List<OrderStatus> delivered = new ArrayList<>();
        delivered.add(OrderStatus.DELIVERED);

        List<OrderStatus> canceled = new ArrayList<>();
        canceled.add(OrderStatus.CANCELLED);

        if(orderStatus.equals(OrderDisplayStatus.PROCESSING))
            criteria.add( Restrictions.in("orderStatus", processing));
        else if(orderStatus.equals(OrderDisplayStatus.WAITING))
            criteria.add( Restrictions.in("orderStatus", waiting));
        else if(orderStatus.equals(OrderDisplayStatus.DELIVERED))
            criteria.add( Restrictions.in("orderStatus", delivered));
        else if(orderStatus.equals(OrderDisplayStatus.CANCELLED))
            criteria.add( Restrictions.in("orderStatus", canceled));
        List<OrderStatus> running = new ArrayList<>();
        running.add(OrderStatus.ORDER_PLACED);
        running.add(OrderStatus.ORDER_STARTED);
        running.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        running.add(OrderStatus.AT_STORE);
        running.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        running.add(OrderStatus.AT_CUSTOMER);

        List<OrderStatus> past = new ArrayList<>();
        past.add(OrderStatus.DELIVERED);
        past.add(OrderStatus.CANCELLED);
        if (orderStatus.equals(OrderDisplayStatus.RUNNING))
            criteria.add(Restrictions.in("orderStatus", running));
        else if (orderStatus.equals(OrderDisplayStatus.PAST))
            criteria.add(Restrictions.in("orderStatus", past));
        if (storeId != null && storeId.size() > 0)
            criteria.add(Restrictions.in("store.id", storeId));
        return ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<OrderEntity> getOrdersByStatus(List<Integer> storeId, Page page, OrderDisplayStatus orderStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        /*List<OrderStatus> processing = new ArrayList<>();
        processing.add(OrderStatus.ORDER_STARTED);
        processing.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        processing.add(OrderStatus.AT_STORE);

        List<OrderStatus> dispatched = new ArrayList<>();
        dispatched.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        dispatched.add(OrderStatus.AT_CUSTOMER);

        List<OrderStatus> waiting = new ArrayList<>();
        waiting.add(OrderStatus.ORDER_PLACED);

        List<OrderStatus> delivered = new ArrayList<>();
        delivered.add(OrderStatus.DELIVERED);

        List<OrderStatus> canceled = new ArrayList<>();
        canceled.add(OrderStatus.CANCELLED);

        if(orderStatus.equals(OrderDisplayStatus.DISPATCHED))
            criteria.add( Restrictions.in("orderStatus", dispatched));
        else if(orderStatus.equals(OrderDisplayStatus.PROCESSING))
            criteria.add( Restrictions.in("orderStatus", processing));
        else if(orderStatus.equals(OrderDisplayStatus.WAITING))
            criteria.add( Restrictions.in("orderStatus", waiting));
        else if(orderStatus.equals(OrderDisplayStatus.DELIVERED))
            criteria.add( Restrictions.in("orderStatus", delivered));
        else if(orderStatus.equals(OrderDisplayStatus.CANCELLED))
            criteria.add( Restrictions.in("orderStatus", canceled));*/
        List<OrderStatus> running = new ArrayList<>();
        running.add(OrderStatus.ORDER_PLACED);
        running.add(OrderStatus.ORDER_STARTED);
        running.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        running.add(OrderStatus.AT_STORE);
        running.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        running.add(OrderStatus.AT_CUSTOMER);

        List<OrderStatus> past = new ArrayList<>();
        past.add(OrderStatus.DELIVERED);
        past.add(OrderStatus.CANCELLED);
        if (orderStatus.equals(OrderDisplayStatus.RUNNING))
            criteria.add(Restrictions.in("orderStatus", running));
        else if (orderStatus.equals(OrderDisplayStatus.PAST))
            criteria.add(Restrictions.in("orderStatus", past));
        if (storeId != null && storeId.size() > 0)
            criteria.add(Restrictions.in("store.id", storeId));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }

    @Override
    public List<OrderEntity> getOrdersByStatusWeb(List<Integer> storeId, Page page, OrderDisplayStatus orderStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        List<OrderStatus> processing = new ArrayList<>();
        processing.add(OrderStatus.ORDER_STARTED);
        processing.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
        processing.add(OrderStatus.AT_STORE);

        processing.add(OrderStatus.EN_ROUTE_TO_DELIVER);
        processing.add(OrderStatus.AT_CUSTOMER);

        List<OrderStatus> waiting = new ArrayList<>();
        waiting.add(OrderStatus.ORDER_PLACED);

        List<OrderStatus> delivered = new ArrayList<>();
        delivered.add(OrderStatus.DELIVERED);

        List<OrderStatus> canceled = new ArrayList<>();
        canceled.add(OrderStatus.CANCELLED);


        if(orderStatus.equals(OrderDisplayStatus.PROCESSING))
            criteria.add( Restrictions.in("orderStatus", processing));
        else if(orderStatus.equals(OrderDisplayStatus.WAITING))
            criteria.add( Restrictions.in("orderStatus", waiting));
        else if(orderStatus.equals(OrderDisplayStatus.DELIVERED))
            criteria.add( Restrictions.in("orderStatus", delivered));
        else if(orderStatus.equals(OrderDisplayStatus.CANCELLED))
            criteria.add( Restrictions.in("orderStatus", canceled));

        if (storeId != null && storeId.size() > 0)
            criteria.add(Restrictions.in("store.id", storeId));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }

    @Override
    public List<OrderEntity> getStoreOrders(Integer storeId, String fromDate, String toDate) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.add(Restrictions.and(Restrictions.or(Restrictions.eq("orderStatus", OrderStatus.DELIVERED), Restrictions.eq("orderStatus", OrderStatus.CANCELLED)),
                Restrictions.eq("store.id", storeId), Restrictions.isNull("statement.id"),
                Restrictions.gt("deliveredDate", new SimpleDateFormat("yyyy-MM-dd").parse(fromDate)), Restrictions.le("deliveredDate", new SimpleDateFormat("yyyy-MM-dd").parse(toDate))));
        return criteria.list();
    }

    @Override
    public Integer getNumberOfSearchedOrder(Page page, OrderDisplayStatus orderDisplayStatus) throws Exception {
        /*String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " WHERE" +
                " (O.order_id like :searchFor OR O.order_display_id like :searchFor OR O.receiver_name like :searchFor" +
                " OR S.given_location1 like :searchFor OR S.given_location2 like :searchFor OR S.address_note like :searchFor OR S.contact_no like :searchFor" +
                " OR SB.brand_name like :searchFor" +
                " OR U.first_name like :searchFor OR U.last_name like :searchFor OR U.email like :searchFor" +
                " OR U.mobile_number like :searchFor" +
                " OR DU.first_name like :searchFor OR DU.last_name like :searchFor OR DU.email like :searchFor" +
                " OR DU.mobile_number like :searchFor)";*/
        String filterByOrderStatus ="";
        if(orderDisplayStatus!=null) {
            if (orderDisplayStatus.equals(OrderDisplayStatus.DELIVERED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.DELIVERED.ordinal() + " AND";
            } else if (orderDisplayStatus.equals(OrderDisplayStatus.CANCELLED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.CANCELLED.ordinal() + " AND";
            } else {
                filterByOrderStatus = " O.order_status in ("
                        +OrderStatus.ORDER_PLACED.ordinal()+","
                        +OrderStatus.ORDER_STARTED.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal()+","
                        +OrderStatus.AT_STORE.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_DELIVER.ordinal()+","
                        +OrderStatus.AT_CUSTOMER.ordinal()+
                ") AND";
            }
        }
        String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " WHERE " +filterByOrderStatus+
                " (O.order_display_id like :searchFor" +
                " OR S.given_location1 like :searchFor" +
                " OR S.given_location2 like :searchFor" +
                " OR SB.brand_name like :searchFor)";
        SQLQuery query = getCurrentSession().createSQLQuery(hsql);
        query.setString("searchFor", "%" + page.getSearchFor() + "%");
        return query.list().size();
    }

    @Override
    public List<OrderEntity> getSearchedOrder(Page page, OrderDisplayStatus orderDisplayStatus) throws Exception {
        /*String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " WHERE" +
                " (O.order_id like :searchFor OR O.order_display_id like :searchFor OR O.receiver_name like :searchFor" +
                " OR S.given_location1 like :searchFor OR S.given_location2 like :searchFor OR S.address_note like :searchFor OR S.contact_no like :searchFor" +
                " OR SB.brand_name like :searchFor" +
                " OR U.first_name like :searchFor OR U.last_name like :searchFor OR U.email like :searchFor" +
                " OR U.mobile_number like :searchFor" +
                " OR DU.first_name like :searchFor OR DU.last_name like :searchFor OR DU.email like :searchFor" +
                " OR DU.mobile_number like :searchFor)";*/
        String filterByOrderStatus ="";
        if(orderDisplayStatus!=null) {
            if (orderDisplayStatus.equals(OrderDisplayStatus.DELIVERED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.DELIVERED.ordinal() + " AND";
            } else if (orderDisplayStatus.equals(OrderDisplayStatus.CANCELLED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.CANCELLED.ordinal() + " AND";
            } else {
                filterByOrderStatus = " O.order_status in ("
                        +OrderStatus.ORDER_PLACED.ordinal()+","
                        +OrderStatus.ORDER_STARTED.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal()+","
                        +OrderStatus.AT_STORE.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_DELIVER.ordinal()+","
                        +OrderStatus.AT_CUSTOMER.ordinal()+
                        ") AND";
            }
        }
        String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " WHERE " +filterByOrderStatus+
                " (O.order_display_id like :searchFor" +
                " OR S.given_location1 like :searchFor" +
                " OR S.given_location2 like :searchFor" +
                " OR SB.brand_name like :searchFor)";
        SQLQuery query = getCurrentSession().createSQLQuery(hsql);
        query.setString("searchFor", "%" + page.getSearchFor() + "%");
        query.setFirstResult((page.getPageNumber() - 1) * page.getPageSize());
        query.setMaxResults(page.getPageSize());
        query.addEntity(OrderEntity.class);
        return (List<OrderEntity>) query.list();
    }

    @Override
    public Integer getNumberOfMerchantSearchedOrder(Page page, String merchantId, OrderDisplayStatus orderDisplayStatus) throws Exception {
        /*String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN merchants M on SB.merchant_id = M.id" +
                " WHERE" +
                " M.merchant_id = :merchantId" +
                " AND (O.order_id like :searchFor OR O.order_display_id like :searchFor OR O.receiver_name like :searchFor" +
                " OR S.given_location1 like :searchFor OR S.given_location2 like :searchFor OR S.address_note like :searchFor OR S.contact_no like :searchFor" +
                " OR SB.brand_name like :searchFor" +
                " OR U.first_name like :searchFor OR U.last_name like :searchFor OR U.email like :searchFor" +
                " OR U.mobile_number like :searchFor" +
                " OR DU.first_name like :searchFor OR DU.last_name like :searchFor OR DU.email like :searchFor" +
                " OR DU.mobile_number like :searchFor)";*/
        String filterByOrderStatus ="";
        if(orderDisplayStatus!=null) {
            if (orderDisplayStatus.equals(OrderDisplayStatus.DELIVERED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.DELIVERED.ordinal() + " AND";
            } else if (orderDisplayStatus.equals(OrderDisplayStatus.CANCELLED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.CANCELLED.ordinal() + " AND";
            } else {
                filterByOrderStatus = " O.order_status in ("
                        +OrderStatus.ORDER_PLACED.ordinal()+","
                        +OrderStatus.ORDER_STARTED.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal()+","
                        +OrderStatus.AT_STORE.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_DELIVER.ordinal()+","
                        +OrderStatus.AT_CUSTOMER.ordinal()+
                        ") AND";
            }
        }
        String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN merchants M on SB.merchant_id = M.id" +
                " WHERE" +filterByOrderStatus+
                " M.merchant_id = :merchantId" +
                " AND (O.order_id like :searchFor" +
                " OR O.order_display_id like :searchFor" +
                " OR S.given_location1 like :searchFor" +
                " OR S.given_location2 like :searchFor" +
                " OR SB.brand_name like :searchFor";
        SQLQuery query = getCurrentSession().createSQLQuery(hsql);
        query.setString("merchantId", merchantId);
        query.setString("searchFor", "%" + page.getSearchFor() + "%");
        return query.list().size();
    }

    @Override
    public List<OrderEntity> getMerchantSearchedOrder(Page page, String merchantId, OrderDisplayStatus orderDisplayStatus) throws Exception {
        /*String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN merchants M on SB.merchant_id = M.id" +
                " WHERE" +
                " M.merchant_id = :merchantId" +
                " AND (O.order_id like :searchFor OR O.order_display_id like :searchFor OR O.receiver_name like :searchFor" +
                " OR S.given_location1 like :searchFor OR S.given_location2 like :searchFor OR S.address_note like :searchFor OR S.contact_no like :searchFor" +
                " OR SB.brand_name like :searchFor" +
                " OR U.first_name like :searchFor OR U.last_name like :searchFor OR U.email like :searchFor" +
                " OR U.mobile_number like :searchFor" +
                " OR DU.first_name like :searchFor OR DU.last_name like :searchFor OR DU.email like :searchFor" +
                " OR DU.mobile_number like :searchFor)";*/
        String filterByOrderStatus ="";
        if(orderDisplayStatus!=null) {
            if (orderDisplayStatus.equals(OrderDisplayStatus.DELIVERED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.DELIVERED.ordinal() + " AND";
            } else if (orderDisplayStatus.equals(OrderDisplayStatus.CANCELLED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.CANCELLED.ordinal() + " AND";
            } else {
                filterByOrderStatus = " O.order_status in ("
                        +OrderStatus.ORDER_PLACED.ordinal()+","
                        +OrderStatus.ORDER_STARTED.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal()+","
                        +OrderStatus.AT_STORE.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_DELIVER.ordinal()+","
                        +OrderStatus.AT_CUSTOMER.ordinal()+
                        ") AND";
            }
        }
        String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN merchants M on SB.merchant_id = M.id" +
                " WHERE" +filterByOrderStatus+
                " M.merchant_id = :merchantId" +
                " AND (O.order_id like :searchFor" +
                " OR O.order_display_id like :searchFor" +
                " OR S.given_location1 like :searchFor" +
                " OR S.given_location2 like :searchFor" +
                " OR SB.brand_name like :searchFor";
        SQLQuery query = getCurrentSession().createSQLQuery(hsql);
        query.setString("merchantId", merchantId);
        query.setString("searchFor", "%" + page.getSearchFor() + "%");
        query.setFirstResult((page.getPageNumber() - 1) * page.getPageSize());
        query.addEntity(OrderEntity.class);
        return (List<OrderEntity>) query.list();
    }

    @Override
    public Integer getNumberOfStoreManagerSearchedOrder(Page page, String storeManagerId, OrderDisplayStatus orderDisplayStatus) throws Exception {
        /*String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN store_managers SM on O.store_manager_id = SM.id" +
                " WHERE" +
                " SM.store_manager_id = :storeManagerId" +
                " AND (O.order_id like :searchFor OR O.order_display_id like :searchFor OR O.receiver_name like :searchFor" +
                " OR S.given_location1 like :searchFor OR S.given_location2 like :searchFor OR S.address_note like :searchFor OR S.contact_no like :searchFor" +
                " OR SB.brand_name like :searchFor" +
                " OR U.first_name like :searchFor OR U.last_name like :searchFor OR U.email like :searchFor" +
                " OR U.mobile_number like :searchFor" +
                " OR DU.first_name like :searchFor OR DU.last_name like :searchFor OR DU.email like :searchFor" +
                " OR DU.mobile_number like :searchFor)";*/
        String filterByOrderStatus ="";
        if(orderDisplayStatus!=null) {
            if (orderDisplayStatus.equals(OrderDisplayStatus.DELIVERED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.DELIVERED.ordinal() + " AND";
            } else if (orderDisplayStatus.equals(OrderDisplayStatus.CANCELLED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.CANCELLED.ordinal() + " AND";
            } else {
                filterByOrderStatus = " O.order_status in ("
                        +OrderStatus.ORDER_PLACED.ordinal()+","
                        +OrderStatus.ORDER_STARTED.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal()+","
                        +OrderStatus.AT_STORE.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_DELIVER.ordinal()+","
                        +OrderStatus.AT_CUSTOMER.ordinal()+
                        ") AND";
            }
        }
        String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN store_managers SM on O.store_manager_id = SM.id" +
                " WHERE" +filterByOrderStatus+
                " SM.store_manager_id = :storeManagerId" +
                " AND (O.order_id like :searchFor" +
                " OR O.order_display_id like :searchFor" +
                " OR S.given_location1 like :searchFor" +
                " OR S.given_location2 like :searchFor" +
                " OR SB.brand_name like :searchFor)";
        SQLQuery query = getCurrentSession().createSQLQuery(hsql);
        query.setString("storeManagerId", storeManagerId);
        query.setString("searchFor", "%" + page.getSearchFor() + "%");
        return query.list().size();
    }

    @Override
    public List<OrderEntity> getStoreManagerSearchedOrder(Page page, String storeManagerId, OrderDisplayStatus orderDisplayStatus) throws Exception {
        /*String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN store_managers SM on O.store_manager_id = SM.id" +
                " WHERE" +
                " SM.store_manager_id = :storeManagerId" +
                " AND (O.order_id like :searchFor OR O.order_display_id like :searchFor OR O.receiver_name like :searchFor" +
                " OR S.given_location1 like :searchFor OR S.given_location2 like :searchFor OR S.address_note like :searchFor OR S.contact_no like :searchFor" +
                " OR SB.brand_name like :searchFor" +
                " OR U.first_name like :searchFor OR U.last_name like :searchFor OR U.email like :searchFor" +
                " OR U.mobile_number like :searchFor" +
                " OR DU.first_name like :searchFor OR DU.last_name like :searchFor OR DU.email like :searchFor" +
                " OR DU.mobile_number like :searchFor)";*/
        String filterByOrderStatus ="";
        if(orderDisplayStatus!=null) {
            if (orderDisplayStatus.equals(OrderDisplayStatus.DELIVERED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.DELIVERED.ordinal() + " AND";
            } else if (orderDisplayStatus.equals(OrderDisplayStatus.CANCELLED)) {
                filterByOrderStatus = " O.order_status =" + OrderStatus.CANCELLED.ordinal() + " AND";
            } else {
                filterByOrderStatus = " O.order_status in ("
                        +OrderStatus.ORDER_PLACED.ordinal()+","
                        +OrderStatus.ORDER_STARTED.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_PICK_UP.ordinal()+","
                        +OrderStatus.AT_STORE.ordinal()+","
                        +OrderStatus.EN_ROUTE_TO_DELIVER.ordinal()+","
                        +OrderStatus.AT_CUSTOMER.ordinal()+
                        ") AND";
            }
        }
        String hsql = "SELECT O.* FROM orders O LEFT JOIN stores S ON O.store_id = S.id" +
                " LEFT JOIN store_brands SB on S.store_brand_id = SB.id" +
                " LEFT JOIN customer C on O.customer_id = C.id" +
                " LEFT JOIN users U on C.user_id = U.id" +
                " LEFT JOIN drivers D on O.driver_id = D.id" +
                " LEFT JOIN users DU on D.user_id = DU.id" +
                " LEFT JOIN store_managers SM on O.store_manager_id = SM.id" +
                " WHERE" +filterByOrderStatus+
                " SM.store_manager_id = :storeManagerId" +
                " AND (O.order_id like :searchFor" +
                " OR O.order_display_id like :searchFor" +
                " OR S.given_location1 like :searchFor" +
                " OR S.given_location2 like :searchFor" +
                " OR SB.brand_name like :searchFor)";
        SQLQuery query = getCurrentSession().createSQLQuery(hsql);
        query.setString("storeManagerId", storeManagerId);
        query.setString("searchFor", "%" + page.getSearchFor() + "%");
        query.addEntity(OrderEntity.class);
        return query.list();
    }

    public Integer getTotalNumbersOfSearchedOrders(String searchString) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("customer", "c");
        criteria.createAlias("c.user", "cu");
        criteria.add(Restrictions.or(Restrictions.like("orderDisplayId", searchString), Restrictions.like("cu.firstName", searchString), Restrictions.like("cu.lastName", searchString)));
        return criteria.list().size();
    }

    public List<OrderEntity> searchOrder(Page page, String searchString) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("customer", "c");
        criteria.createAlias("c.user", "cu");
        criteria.add(Restrictions.or(Restrictions.like("orderDisplayId", searchString), Restrictions.like("cu.firstName", searchString), Restrictions.like("cu.lastName", searchString)));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }


    @Deprecated
    @Override
    public BigDecimal findRatingOfDriver(Integer driverId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .add(Restrictions.isNotNull("ratingForDriver"))
                .add(Restrictions.eq("driver.id", driverId))
                .add(Restrictions.eq("orderStatus", OrderStatus.DELIVERED))
                .setProjection(Projections.rowCount());
        BigDecimal orderCount = BigDecimal.valueOf((Long) criteria.uniqueResult());
        Criteria criteria1 = getCurrentSession().createCriteria(OrderEntity.class)
                .add(Restrictions.isNotNull("ratingForDriver"))
                .add(Restrictions.eq("driver.id", driverId))
                .add(Restrictions.eq("orderStatus", OrderStatus.DELIVERED))
                .setProjection(Projections.sum("ratingForDriver"));
        BigDecimal totalRating = BigDecimal.ZERO;
        if (criteria1.uniqueResult() != null)
            totalRating = BigDecimal.valueOf((Long) criteria1.uniqueResult());
        if (orderCount.equals(BigDecimal.ZERO))
            return BigDecimal.ZERO;

        //Criteria criteria =  getCurrentSession().createCriteria(OrderEntity.class).projList.add(Projections.avg("price"));
        return totalRating.divide(orderCount).setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public Integer getNumberOfStoreManagerOrder(Page page, String storeId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "store");
        criteria.add(Restrictions.eq("store.storeId", storeId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public List<OrderEntity> getStoreManagerOrder(Page page, String storeId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "store");
        criteria.add(Restrictions.eq("store.storeId", storeId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getNumberOfMerchantOrder(Page page, String userId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");
        criteria.createAlias("m.user", "u");
        criteria.add(Restrictions.eq("u.userId", userId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public List<OrderEntity> getMerchantOrder(Page page, String userId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");
        criteria.createAlias("m.user", "u");
        criteria.add(Restrictions.eq("u.userId", userId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getNumberOfAllOrder(Page page, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public List<OrderEntity> getAllOrder(Page page, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getNumberOfStoreOrder(Page page, String storeId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public List<OrderEntity> getStoreOrder(Page page, String storeId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getNumberOfStoreBrandOrder(Page page, String storeBrandId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.add(Restrictions.eq("sb.storeBrandId", storeBrandId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public List<OrderEntity> getStoreBrandOrder(Page page, String storeBrandId, List<OrderStatus> orderStatuses) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.add(Restrictions.eq("sb.storeBrandId", storeBrandId));
        if (orderStatuses.contains(OrderStatus.EN_ROUTE_TO_DELIVER))
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        HibernateUtil.fillPaginationCriteria(criteria, page, OrderEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getNumberOfStoreManagerOrder(String storeId, OrderStatus orderStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "store");
        criteria.add(Restrictions.eq("store.storeId", storeId));
        if (orderStatus.equals(OrderStatus.AT_CUSTOMER) || orderStatus.equals(OrderStatus.EN_ROUTE_TO_DELIVER)) {
            criteria.add(Restrictions.or(Restrictions.eq("orderStatus", OrderStatus.AT_CUSTOMER), Restrictions.eq("orderStatus", OrderStatus.EN_ROUTE_TO_DELIVER)));
        } else {
            criteria.add(Restrictions.eq("orderStatus", orderStatus));
        }
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public Integer getNumberOfMerchantOrder(String userId, OrderStatus orderStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.createAlias("sb.merchant", "m");
        criteria.createAlias("m.user", "u");
        criteria.add(Restrictions.eq("u.userId", userId));
        if (orderStatus.equals(OrderStatus.AT_CUSTOMER) || orderStatus.equals(OrderStatus.EN_ROUTE_TO_DELIVER)) {
            criteria.add(Restrictions.or(Restrictions.eq("orderStatus", OrderStatus.AT_CUSTOMER), Restrictions.eq("orderStatus", OrderStatus.EN_ROUTE_TO_DELIVER)));
        } else {
            criteria.add(Restrictions.eq("orderStatus", orderStatus));
        }
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public Integer getNumberOfAllOrder(OrderStatus orderStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        if (orderStatus.equals(OrderStatus.AT_CUSTOMER) || orderStatus.equals(OrderStatus.EN_ROUTE_TO_DELIVER)) {
            criteria.add(Restrictions.or(Restrictions.eq("orderStatus", OrderStatus.AT_CUSTOMER), Restrictions.eq("orderStatus", OrderStatus.EN_ROUTE_TO_DELIVER)));
        } else {
            criteria.add(Restrictions.eq("orderStatus", orderStatus));
        }
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public Integer getNumberOfStoreOrder(String storeId, OrderStatus orderStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.add(Restrictions.eq("s.storeId", storeId));
        if (orderStatus.equals(OrderStatus.AT_CUSTOMER) || orderStatus.equals(OrderStatus.EN_ROUTE_TO_DELIVER)) {
            criteria.add(Restrictions.or(Restrictions.eq("orderStatus", OrderStatus.AT_CUSTOMER), Restrictions.eq("orderStatus", OrderStatus.EN_ROUTE_TO_DELIVER)));
        } else {
            criteria.add(Restrictions.eq("orderStatus", orderStatus));
        }
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public Integer getNumberOfStoreBrandOrder(String storeBrandId, OrderStatus orderStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand", "sb");
        criteria.add(Restrictions.eq("sb.storeBrandId", storeBrandId));
        if (orderStatus.equals(OrderStatus.AT_CUSTOMER) || orderStatus.equals(OrderStatus.EN_ROUTE_TO_DELIVER)) {
            criteria.add(Restrictions.or(Restrictions.eq("orderStatus", OrderStatus.AT_CUSTOMER), Restrictions.eq("orderStatus", OrderStatus.EN_ROUTE_TO_DELIVER)));
        } else {
            criteria.add(Restrictions.eq("orderStatus", orderStatus));
        }
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    @Override
    public Integer getNumberOfOrdersByDrivers(List<String> driverIds, Date fromDate, Date toDate, List<OrderStatus> orderStatuses, List<String> storeIds) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "d");
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand","sb");
        criteria.add(Restrictions.in("d.driverId", driverIds));
        criteria.add(Restrictions.between("createdDate", fromDate, toDate));
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        if(storeIds!=null)
            criteria.add(Restrictions.in("sb.storeBrandId",storeIds));
        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<OrderEntity> getOrdersByDrivers(List<String> driverIds, Date fromDate, Date toDate, Page page, List<OrderStatus> orderStatuses, List<String> storeIds) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("driver", "d");
        criteria.createAlias("store", "s");
        criteria.createAlias("s.storeBrand","sb");
        criteria.add(Restrictions.in("d.driverId", driverIds));
        criteria.add(Restrictions.between("createdDate", fromDate, toDate));
        criteria.add(Restrictions.in("orderStatus", orderStatuses));
        if(storeIds!=null)
            criteria.add(Restrictions.in("sb.storeBrandId",storeIds));
        if (page.getSortOrder() != null) {
            if (page.getSortOrder().equals("asc")) {
                criteria.addOrder(Order.asc(page.getSortBy()));
            } else {
                criteria.addOrder(Order.desc(page.getSortBy()));
            }
        }
        if(page.getPageNumber()!=null || page.getPageSize()!=null) {
            criteria.setFirstResult((page.getPageNumber() - 1) * page.getPageSize());
            criteria.setMaxResults(page.getPageSize());
        }
        return criteria.list();
    }

    public List<OrderEntity> getOrderListByAPIKey(APIData xmlData, Integer merchantId, String storeId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store","s")
                .createAlias("s.storeBrand","sb")
                .createAlias("sb.merchant","m");
        criteria.add(Restrictions.eq("m.id",merchantId));
        if(storeId!=null)
            criteria.add(Restrictions.eq("s.storeId",storeId));
        if(xmlData.getOrderDisplayStatus().equals(OrderDisplayStatus.DELIVERED)){
            criteria.add(Restrictions.eq("orderStatus",OrderStatus.DELIVERED));
        }else if(xmlData.getOrderDisplayStatus().equals(OrderDisplayStatus.CANCELLED)){
            criteria.add(Restrictions.eq("orderStatus",OrderStatus.CANCELLED));
        }else {
            List<OrderStatus> orderStatuses = new ArrayList<>();
            orderStatuses.add(OrderStatus.ORDER_PLACED);
            orderStatuses.add(OrderStatus.ORDER_STARTED);
            orderStatuses.add(OrderStatus.AT_STORE);
            orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
            criteria.add(Restrictions.in("orderStatus",orderStatuses));
        }
        return criteria.list();
    }

    @Override
    public APIData getDetailById(String orderId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
                .add(Restrictions.eq("orderId", orderId));
        criteria.createAlias("store","s")
                .createAlias("s.storeBrand","sb")
                /*.createAlias("driver","d")
                .createAlias("d.user","du")*/
                //.createAlias("customer","c")
                //.createAlias("c.user","uc")
                //.createAlias("customersArea","ca")
                //.createAlias("ca.area","area")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("id"),"id")
                        /*.add(Projections.property("orderId"),"orderId")
                        .add(Projections.property("uc.emailAddress"),"email")
                        .add(Projections.property("uc.firstName"),"customerName")
                        .add(Projections.property("uc.mobileNumber"),"mobileNumber")
                        .add(Projections.property("ca.givenLocation"),"customerAddress")
                        .add(Projections.property("area.latitude"),"customerLatitude")
                        .add(Projections.property("area.longitude"),"customerLongitude")
                        .add(Projections.property("ca.latitude"),"customerLatitude1")
                        .add(Projections.property("ca.longitude"),"customerLongitude1")
                        .add(Projections.property("orderDeliveryDate"),"orderTime")
                        .add(Projections.property("paymentMode"),"paymentMode")
                        .add(Projections.property("orderStatus"),"orderStatus")
                        .add(Projections.property("totalBillAmount"),"totalAmount")
                        .add(Projections.property("du.firstName"),"driverName")
                        .add(Projections.property("du.lastName"),"driverLastName")
                        .add(Projections.property("d.latitude"),"driverLatitude")
                        .add(Projections.property("d.longitude"),"driverLongitude")
                        .add(Projections.property("sb.brandName"),"storeName")
                        .add(Projections.property("s.givenLocation1"),"storeAddress")
                        *//*.add(Projections.property("sb.addressNote"),"addressNote")
                        .add(Projections.property("sb.givenLocation2"),"storeAddress1")*//*
                        .add(Projections.property("s.latitude"),"storeLatitude")
                        .add(Projections.property("s.longitude"),"storeLongitude")
                        .add(Projections.property("customersNoteToDriver"),"orderNote")*/
                );
        criteria.setResultTransformer(new AliasToBeanResultTransformer(APIData.class));
        return (APIData) (criteria.uniqueResult());
    }

    public Integer getStoreOrderCountByDate(String storeId, RequestJsonDto requestJsonDto, OrderDisplayStatus orderDisplayStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        criteria.createAlias("store", "store")
            .add(Restrictions.eq("store.storeId", storeId));
        if(requestJsonDto.getFromDate()!=null && requestJsonDto.getToDate()!=null)
            criteria.add(Restrictions.between("createdDate", requestJsonDto.getFromDate(), requestJsonDto.getToDate()));
        if(orderDisplayStatus.equals(OrderDisplayStatus.WAITING)){
            criteria.add(Restrictions.eq("orderStatus",OrderStatus.ORDER_PLACED));
        }else if(orderDisplayStatus.equals(OrderDisplayStatus.RUNNING)){
            List<OrderStatus> orderStatuses = new ArrayList<>();
            orderStatuses.add(OrderStatus.ORDER_STARTED);
            orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
            orderStatuses.add(OrderStatus.AT_STORE);
            orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
            criteria.add(Restrictions.in("orderStatus",orderStatuses));
        }
        return ((Integer) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    public Integer getMerchantOrderCountByDate(String merchantId, RequestJsonDto requestJsonDto, OrderDisplayStatus orderDisplayStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class)
            .createAlias("store", "s")
            .createAlias("s.storeBrand", "sb")
            .createAlias("sb.merchant", "m")
            .add(Restrictions.eq("m.merchantId", merchantId));
        if(requestJsonDto.getFromDate()!=null && requestJsonDto.getToDate()!=null)
            criteria.add(Restrictions.between("createdDate", requestJsonDto.getFromDate(), requestJsonDto.getToDate()));
        if(orderDisplayStatus.equals(OrderDisplayStatus.WAITING)){
            criteria.add(Restrictions.eq("orderStatus",OrderStatus.ORDER_PLACED));
        }else if(orderDisplayStatus.equals(OrderDisplayStatus.RUNNING)){
            List<OrderStatus> orderStatuses = new ArrayList<>();
            orderStatuses.add(OrderStatus.ORDER_STARTED);
            orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
            orderStatuses.add(OrderStatus.AT_STORE);
            orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
            criteria.add(Restrictions.in("orderStatus",orderStatuses));
        }
        return ((Integer) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }

    public Integer getAllOrderCountByDate(RequestJsonDto requestJsonDto, OrderDisplayStatus orderDisplayStatus) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(OrderEntity.class);
        if(requestJsonDto.getFromDate()!=null && requestJsonDto.getToDate()!=null)
            criteria.add(Restrictions.between("createdDate", requestJsonDto.getFromDate(), requestJsonDto.getToDate()));
        if(orderDisplayStatus.equals(OrderDisplayStatus.WAITING)){
            criteria.add(Restrictions.eq("orderStatus",OrderStatus.ORDER_PLACED));
        }else if(orderDisplayStatus.equals(OrderDisplayStatus.RUNNING)){
            List<OrderStatus> orderStatuses = new ArrayList<>();
            orderStatuses.add(OrderStatus.ORDER_STARTED);
            orderStatuses.add(OrderStatus.EN_ROUTE_TO_PICK_UP);
            orderStatuses.add(OrderStatus.AT_STORE);
            orderStatuses.add(OrderStatus.EN_ROUTE_TO_DELIVER);
            orderStatuses.add(OrderStatus.AT_CUSTOMER);
            criteria.add(Restrictions.in("orderStatus",orderStatuses));
        }
        return ((Long) (criteria.setProjection(Projections.rowCount()).uniqueResult())).intValue();
    }
}
