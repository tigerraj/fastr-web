package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.DriversWorkLogDao;
import com.yetistep.anyorder.model.DriversWorkLogEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class DriversWorkLogDaoImpl implements DriversWorkLogDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public DriversWorkLogEntity find(Integer id) throws Exception {
        return (DriversWorkLogEntity) getCurrentSession().get(DriversWorkLogEntity.class,id);
    }

    @Override
    public List<DriversWorkLogEntity> findAll() throws Exception {
        return (List<DriversWorkLogEntity>) getCurrentSession().createCriteria(DriversWorkLogEntity.class);
    }

    @Override
    public Boolean save(DriversWorkLogEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(DriversWorkLogEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(DriversWorkLogEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public DriversWorkLogEntity findById(String driversWorkLogId) throws Exception {
        return (DriversWorkLogEntity) getCurrentSession().createCriteria(DriversWorkLogEntity.class)
                .add(Restrictions.eq("driversWorkLogId",driversWorkLogId)).uniqueResult();
    }

    @Override
    public DriversWorkLogEntity findByDriverWithNoToDate(Integer driverId) throws Exception {
        return (DriversWorkLogEntity) getCurrentSession().createCriteria(DriversWorkLogEntity.class)
                .createAlias("driver","driver")
                .add(Restrictions.isNull("toDateTime"))
                .add(Restrictions.eq("driver.id",driverId)).uniqueResult();
    }

    @Override
    public DriversWorkLogEntity findLastLoginOfDriver(String driverId) throws Exception {
        return (DriversWorkLogEntity) getCurrentSession().createCriteria(DriversWorkLogEntity.class)
                .createAlias("driver","driver")
                .add(Restrictions.isNull("toDateTime"))
                .add(Restrictions.eq("driver.driverId",driverId)).uniqueResult();
    }


}
