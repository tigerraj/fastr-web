package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.StoreBrandDao;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.model.Page;
import com.yetistep.anyorder.model.StoreBrandEntity;
import com.yetistep.anyorder.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class StoreBrandDaoImpl implements StoreBrandDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public StoreBrandEntity find(Integer id) throws Exception {
        return (StoreBrandEntity) getCurrentSession().get(StoreBrandEntity.class,id);
    }

    @Override
    public List<StoreBrandEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(StoreBrandEntity.class).list();
    }

    @Override
    public Boolean save(StoreBrandEntity value) throws Exception {
       getCurrentSession().persist(value);
        return null;
    }

    @Override
    public Boolean update(StoreBrandEntity value) throws Exception {
         getCurrentSession().persist(value);
        return null;
    }

    @Override
    public Boolean delete(StoreBrandEntity value) throws Exception {
          return null;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    public StoreBrandEntity findById(String storeBrandId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class)
                .add(Restrictions.eq("storeBrandId",storeBrandId));
        return (StoreBrandEntity) criteria.uniqueResult();
    }

    public Boolean checkBrandNameExist(String storeBrandName) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class)
                .add(Restrictions.eq("brandName",storeBrandName));
        return (criteria.list().size()> 0 ? true : false);
    }

    @Override
    public Integer getNumberOfMerchantStoreBrand(String merchantId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class);
        criteria.createAlias("merchant", "m");
        criteria.add(Restrictions.eq("m.merchantId", merchantId));
        HibernateUtil.fillPaginationCriteria(criteria, page, StoreBrandEntity.class);
        return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<StoreBrandEntity> getMerchantStoreBrandList(String merchantId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class);
        criteria.createAlias("merchant", "m");
        criteria.add(Restrictions.eq("m.merchantId", merchantId));
        HibernateUtil.fillPaginationCriteria(criteria, page, StoreBrandEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getTotalNumberOfStoreByStatus(String merchantId, Status status) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class);
        if (merchantId != null) {
            criteria.createAlias("merchant", "m");
            criteria.add(Restrictions.eq("m.merchantId", merchantId));
        }
        criteria.add(Restrictions.eq("status", status));
        criteria.setProjection(Projections.rowCount());
        return ((Number) criteria.uniqueResult()).intValue();
    }

    @Override
    public List<StoreBrandEntity> getStoreBrandListByStatus(String merchantId, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class);
        if (merchantId != null) {
            criteria.createAlias("merchant", "m");
            criteria.add(Restrictions.eq("m.merchantId", merchantId));
        }
        criteria.add(Restrictions.eq("status", page.getStatus()));
        HibernateUtil.fillPaginationCriteria(criteria, page, StoreBrandEntity.class);
        return criteria.list();
    }

    @Override
    public Integer getNumberOfMerchantsStoreBrands(List<String> merchantIds) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class);
        criteria.createAlias("merchant", "m");
        criteria.add(Restrictions.in("m.merchantId", merchantIds));
        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<StoreBrandEntity> getMerchantsStoreBrands(List<String> merchantIds, Page page) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreBrandEntity.class);
        criteria.createAlias("merchant", "m");
        criteria.add(Restrictions.in("m.merchantId", merchantIds));
        criteria.setFirstResult((page.getPageNumber() - 1) * page.getPageSize());
        criteria.setMaxResults(page.getPageSize());
        return criteria.list();
    }
}
