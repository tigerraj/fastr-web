package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.RatePlanDao;
import com.yetistep.anyorder.model.RatePlanEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class RatePlanDaoImpl implements RatePlanDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public RatePlanEntity find(Integer id) throws Exception {
        return (RatePlanEntity) getCurrentSession().get(RatePlanEntity.class, id);
    }

    @Override
    public List<RatePlanEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(RatePlanEntity.class).list();
    }

    @Override
    public Boolean save(RatePlanEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(RatePlanEntity value) throws Exception {
        getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(RatePlanEntity value) throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return getSessionFactory().getCurrentSession();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public RatePlanEntity findById(String ratePlanId) throws Exception{
       return (RatePlanEntity) getCurrentSession().createCriteria(RatePlanEntity.class)
               .add(Restrictions.eq("ratePlanId", ratePlanId)).uniqueResult();
    }
}
