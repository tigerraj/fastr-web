package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.StoreAreaDao;
import com.yetistep.anyorder.model.StoresAreaEntity;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class StoreAreaDaoImpl implements StoreAreaDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public StoresAreaEntity find(Integer id) throws Exception {
        return (StoresAreaEntity) getCurrentSession().get(StoresAreaEntity.class,id);
    }

    @Override
    public List<StoresAreaEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(StoresAreaEntity.class).list();
    }

    @Override
    public Boolean save(StoresAreaEntity value) throws Exception {
        getCurrentSession().save(value);
        return true;
    }

    @Override
    public Boolean update(StoresAreaEntity value) throws Exception {
         getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean delete(StoresAreaEntity value) throws Exception {
        getCurrentSession().delete(value);
        return true;
    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    public StoresAreaEntity findById(String storesAreaId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(StoresAreaEntity.class)
                .add(Restrictions.eq("storesAreaId",storesAreaId));
        return (StoresAreaEntity) criteria.uniqueResult();
    }

    public StoresAreaEntity findByAreaId(Integer storeId, String areaId) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoresAreaEntity.class)
                .createAlias("area","area")
                .add(Restrictions.eq("store.id",storeId))
                .add(Restrictions.eq("area.areaId",areaId));
        return (StoresAreaEntity) criteria.uniqueResult();
    }

    @Override
    public void flush() throws Exception {
        this.getCurrentSession().flush();
        this.getCurrentSession().clear();
    }

    @Override
    public Boolean saveAll(List<StoresAreaEntity> stores) throws Exception {
        int i=0;
        for(StoresAreaEntity store : stores){
            getCurrentSession().save(store);
            if(++i % 20 == 0){
                //getCurrentSession().flush();
                //getCurrentSession().clear();
            }
        }
        return true;
    }

    public void deleteAll(Integer storeId) throws Exception {
        Query query=getCurrentSession().createQuery("delete from StoresAreaEntity where store.id=:storeId");
        query.setParameter("storeId",storeId);
        query.executeUpdate();

    }

    public void updateAll(List<StoresAreaEntity> storeAreas) throws Exception {
        int i=0;
        for(StoresAreaEntity storeArea: storeAreas){
            getCurrentSession().save(storeArea);
            if(++i%20==0)
                getCurrentSession().flush();
        }
    }
}
