package com.yetistep.anyorder.dao.impl;

import com.yetistep.anyorder.dao.inf.StoreDao;
import com.yetistep.anyorder.enums.StatementGenerationType;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.enums.UserStatus;
import com.yetistep.anyorder.model.StoreEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 11/6/14
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class StoreDaoImpl implements StoreDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public StoreEntity find(Integer id) throws Exception {
        return (StoreEntity) getCurrentSession().get(StoreEntity.class,id);
    }

    @Override
    public List<StoreEntity> findAll() throws Exception {
        return getCurrentSession().createCriteria(StoreEntity.class).list();
    }

    @Override
    public List<StoreEntity> findAllActive() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreEntity.class);
        criteria.createAlias("storeBrand","sb");
        criteria.createAlias("sb.merchant","m");
        criteria.createAlias("m.user","u");
        criteria.add(Restrictions.eq("status",Status.ACTIVE));
        criteria.add(Restrictions.eq("sb.status",Status.ACTIVE));
        criteria.add(Restrictions.eq("u.userStatus",UserStatus.ACTIVE));
        return criteria.list();
    }

    @Override
    public Boolean save(StoreEntity value) throws Exception {
       getCurrentSession().persist(value);
        return true;
    }

    @Override
    public Boolean update(StoreEntity value) throws Exception {
        getCurrentSession().persist(value);
//        getCurrentSession().flush();
        return true;
    }

    @Override
    public Boolean delete(StoreEntity value) throws Exception {
         return null;

    }

    @Override
    public Session getCurrentSession() throws Exception {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public StoreEntity findById(String storeId) throws Exception{
        Criteria criteria = getCurrentSession().createCriteria(StoreEntity.class)
                .add(Restrictions.eq("storeId",storeId));
        return (StoreEntity) criteria.uniqueResult();
    }

    @Override
    public List<StoreEntity> findActiveStoreByStatementGenerationType(StatementGenerationType statementGenerationType) throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreEntity.class);
        criteria.createAlias("storeBrand", "sb");
        criteria.add(Restrictions.eq("sb.statementGenerationType", statementGenerationType));
        return criteria.list();
    }

    @Override
    public List<StoreEntity> findActive() throws Exception {
        Criteria criteria = getCurrentSession().createCriteria(StoreEntity.class);
        criteria.createAlias("storeBrand","sb");
        criteria.createAlias("sb.merchant","m");
        criteria.createAlias("m.user","user");
        criteria.add(Restrictions.eq("status", Status.ACTIVE));
        criteria.add(Restrictions.eq("sb.status", Status.ACTIVE));
        criteria.add(Restrictions.eq("user.userStatus", UserStatus.ACTIVE));
        return criteria.list();
    }
}
