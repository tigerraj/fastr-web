package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.PreferenceTypeEntity;
import com.yetistep.anyorder.model.StoreEntity;
import com.yetistep.anyorder.service.inf.AdminService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/4/16
 * Time: 12:33 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    private static final Logger log = Logger.getLogger(AdminController.class);

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    AdminService adminService;

    @RequestMapping(value = "/update_preferences", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updatePreferences(@RequestBody RequestJsonDto requestJsonDto){
        try{
            systemPropertyService.updateSystemPreferences(requestJsonDto.getPreferences());

            ServiceResponse serviceResponse = new ServiceResponse("Preferences updated successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);

        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating system preferences", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);

        }
    }

    @RequestMapping(value = "/create_user", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> createUser(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USERNAME);

            String msg = adminService.createUser(headerDto.getUsername(), requestJsonDto.getUser());
            ServiceResponse serviceResponse = new ServiceResponse(msg + " An email has been sent to create the login credentials.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.CREATED);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while creating user", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_users", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getUsers(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto paginationDto = adminService.getUsers(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Users have been retrieved successfully.");
            serviceResponse.addParam("users", paginationDto);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while retrieving users.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_group_preferences", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getGroupPreferences(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            PreferenceTypeEntity preferences = systemPropertyService.getPreferencesType(Integer.parseInt(headerDto.getId()));

            ServiceResponse serviceResponse = new ServiceResponse("Preferences retrieved successfully");
            serviceResponse.addParam("preferences", preferences);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while getting system preferences", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/activate_store", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> activateStore(@RequestHeader HttpHeaders headers, @RequestBody StoreEntity store) {
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            adminService.activateStore(headerDto.getStoreId(), store);
            ServiceResponse serviceResponse = new ServiceResponse("Store has been activated successfully");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while activating store", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_default_images", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateDefaultImage(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            adminService.updateDefaultImage(requestJsonDto);

            ServiceResponse serviceResponse = new ServiceResponse("Default images updated successfully");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating default images", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);

        }
    }

    @RequestMapping(value = "/update_store_setting", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateStoreSetting(@RequestHeader HttpHeaders headers, @RequestBody StoreEntity store) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            adminService.updateStoreSetting(headerDto.getStoreId(), store);
            ServiceResponse serviceResponse = new ServiceResponse("Stores setting has been updated successfully");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating store setting", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);

        }
    }
}
