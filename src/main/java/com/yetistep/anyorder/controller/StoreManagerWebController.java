package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.abs.Constant;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.dto.ResponseJsonDto;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.DriverService;
import com.yetistep.anyorder.service.inf.StoreManagerWebService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import com.yetistep.anyorder.util.SessionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by BinaySingh on 5/24/2016.
 */
@Controller
@RequestMapping(value = "/smanager")
public class StoreManagerWebController {

    Logger log = Logger.getLogger(StoreManagerWebController.class);

    @Autowired
    StoreManagerWebService storeManagerWebService;

    @Autowired
    DriverService driverService;

    @RequestMapping(value = "/update_store_manager", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateStoreManager(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            storeManagerWebService.updateStoreManager(requestJsonDto.getStoreManager());
            ServiceResponse serviceResponse = new ServiceResponse("Store Manager has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.CREATED);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while creating store manager", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_area_list", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAreasList(){
        try{
            List<AreaEntity> area = storeManagerWebService.getAreas();
            ServiceResponse serviceResponse = new ServiceResponse("Areas list has been retrieved successfully");
            serviceResponse.addParam("areas",area);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving areas list", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_active_area_list", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getActiveAreasList() {
        try {
            List<AreaEntity> area = storeManagerWebService.getActiveAreas();
            ServiceResponse serviceResponse = new ServiceResponse("Active Areas list has been retrieved successfully");
            serviceResponse.addParam("areas", area);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving active areas list", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_area", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getArea(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            AreaEntity area = storeManagerWebService.getArea(headerDto.getId());
            ServiceResponse serviceResponse = new ServiceResponse("Area has been retrieved successfully");
            serviceResponse.addParam("area",area);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving area", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);

        }
    }

    @RequestMapping(value = "/create_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> createOrder(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            ResponseJsonDto responseJsonDto = storeManagerWebService.createOrder(headerDto, requestJsonDto);
            ServiceResponse serviceResponse;
            if(requestJsonDto.getDriverId()!=null)
                serviceResponse = new ServiceResponse("Order has been saved and driver has been assigned successfully");
            else if( responseJsonDto.getDrivers()!=null && responseJsonDto.getDrivers().size()>0)
                serviceResponse = new ServiceResponse("Order has been saved and drivers has been retrieved successfully");
            else if(!responseJsonDto.getAssignDriverAutomatically() &&
                    (responseJsonDto.getDrivers()==null ||  responseJsonDto.getDrivers().size()==0)
                    && requestJsonDto.getSaveAndAssign()!=null && requestJsonDto.getSaveAndAssign() && !SessionManager.getRole().equals(UserRole.ROLE_MERCHANT) && !SessionManager.getRole().equals(UserRole.ROLE_STORE_MANAGER))
                serviceResponse = new ServiceResponse("Order has been saved but drivers are not available. please try to assign driver later");
            else
                serviceResponse = new ServiceResponse("Order has been saved successfully");

            if(responseJsonDto.getMessage()!=null)
                serviceResponse = new ServiceResponse(responseJsonDto.getMessage());
            serviceResponse.addParam("data", responseJsonDto);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while creating orders", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_store_brand_quick_info", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> get_store_brand_quick_info(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            StoreEntity store = storeManagerWebService.getSpecialPrivilegeInfo(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Brand information has been retrieved successfully");
            serviceResponse.addParam("storeBrand", store);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while retrieving brand information", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_customers_info", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getCustomersInfo(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            CustomerEntity customer = storeManagerWebService.getCustomersInfo(headerDto.getStoreId(), requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("customers addresses has been retrieved successfully");
            serviceResponse.addParam("customer", customer);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while retrieving customers addresses", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_stores_drivers", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStoresDrivers(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto);

            List<DriverEntity> drivers = storeManagerWebService.getStoresDrivers(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("drivers has been retrieved successfully");
            serviceResponse.addParam("drivers",drivers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving drivers", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_order_detail", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getOrderDetail(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID);

            OrderEntity order = storeManagerWebService.getOrderDetail(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order detail has been retrieved successfully");
            serviceResponse.addParam("order", order);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving order detail", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_all_store_list", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAllStoreList(){
        try{
            List<StoreEntity> stores = storeManagerWebService.getAllStoreList();
            ServiceResponse serviceResponse = new ServiceResponse("Store list retrieved successfully");
            serviceResponse.addParam("stores", stores);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving store list", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/assign_driver_to_order", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> assignDriverToOrder(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.DRIVER_ID, GeneralUtil.ORDER_ID);
            ResponseJsonDto responseJsonDto = storeManagerWebService.assignDriverToOrder(headerDto);
            ServiceResponse serviceResponse;
            if(headerDto.getDriverId()!=null)
                serviceResponse = new ServiceResponse("Driver has been assigned successfully");
            else if( responseJsonDto.getDrivers()!=null && responseJsonDto.getDrivers().size()>0)
                serviceResponse = new ServiceResponse("Drivers has been retrieved successfully");
            else if(!responseJsonDto.getAssignDriverAutomatically() && (responseJsonDto.getDrivers()==null ||  responseJsonDto.getDrivers().size()==0))
                serviceResponse = new ServiceResponse("Drivers are not available now. Please try again later.");
            else
                serviceResponse = new ServiceResponse("Driver has been assigned successfully");
            if(responseJsonDto.getMessage()!=null)
                serviceResponse = new ServiceResponse(responseJsonDto.getMessage());

            serviceResponse.addParam("data", responseJsonDto);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while assigning driver to order", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_any_order_driver_list", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAnyOrderDriverList(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID);
            List<DriverEntity> drivers = storeManagerWebService.getAnyOrderDriverList(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver has been retrieved successfully");
            serviceResponse.addParam("drivers", drivers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving drivers", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_total_number_of_orders", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getTotalNumberOfOrders(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            RequestJsonDto responseJson = storeManagerWebService.getTotalNumberOfOrders(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Total number of orders has been retrieved successfully");
            serviceResponse.addParam("numberOfOrders", responseJson);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving total number of orders", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_orders_by_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getOrdersByStatus(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            PaginationDto orders = storeManagerWebService.getOrdersByStatus(headerDto, requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Orders has been retrieved successfully");
            serviceResponse.addParam("orders", orders);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving orders", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_stores_not_driver", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateStoresNotDriver (@RequestHeader HttpHeaders headers, @RequestBody List<DriverEntity> drivers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            storeManagerWebService.updateStoresNotDriver(headerDto, drivers);
            ServiceResponse serviceResponse = new ServiceResponse("Drivers has been restricted to store successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while restricting driver from stores", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_driver_not_stores", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getDriverNotStores(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers,headerDto, GeneralUtil.STORE_ID);
            List<DriverEntity> drivers = storeManagerWebService.getDriversNotStore(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Restricted drivers of store has been retrieved successfully.");
            serviceResponse.addParam("drivers",drivers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving restricted drivers of store", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_any_order_drivers", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAnyOrderDriver(){
        try{
            List<DriverEntity> drivers = storeManagerWebService.getAnyOrderDrivers();
            ServiceResponse serviceResponse = new ServiceResponse(Constant.APP_NAME + " drivers has been retrieved successfully.");
            serviceResponse.addParam("drivers",drivers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving " + Constant.APP_NAME + " drivers", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_driver_status", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> changeStoreManagerStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            driverService.changeDriverStatus(requestJsonDto.getDriver());
            ServiceResponse serviceResponse = new ServiceResponse("Driver's status has been changed successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while changing Driver's status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/assign_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> assignDriver(@RequestHeader HttpHeaders headers, @RequestBody OrderEntity order) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.DRIVER_ID);
            driverService.assignDriver(headerDto, order);
            ServiceResponse serviceResponse = new ServiceResponse("Driver has assigned to order successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while assigning driver to orders", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_store_list", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStoreList(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            List<StoreEntity> stores = storeManagerWebService.getStoreList(headerDto.getUserId());
            ServiceResponse serviceResponse = new ServiceResponse("Store list retrieved successfully");
            serviceResponse.addParam("stores", stores);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving store list", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/estimate_delivery_charge", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> calculateRatePlan(@RequestHeader HttpHeaders headers, @RequestBody CustomersAreaEntity customersArea) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            OrderEntity order = storeManagerWebService.estimateDeliveryCharge(headerDto.getStoreId(), customersArea);
            ServiceResponse serviceResponse = new ServiceResponse("Estimate delivery charge for customer to store calculated successfully");
            serviceResponse.addParam("data", order);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while calculating estimate delivery charge from store to customer", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/drivers_by_status_with_order_count", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getDriversWithOrderCount(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJson) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            Map<String, PaginationDto> drivers = storeManagerWebService.getDriversWithOrderCount(headerDto.getUserId(), requestJson);
            ServiceResponse serviceResponse = new ServiceResponse("Driver list by status retrieved successful");
            serviceResponse.addParam("data", drivers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving driver list by status", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/search_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> searchOrder(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto orderSearchResult = storeManagerWebService.searchOrder(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Search result retrieved successfully");
            serviceResponse.addParam("orderSearchResult", orderSearchResult);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while searching order", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_order_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateOrderStatus(@RequestHeader HttpHeaders headers, @RequestBody OrderEntity order) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID);
            storeManagerWebService.updateOrderStatus(headerDto, order);
            ServiceResponse serviceResponse = new ServiceResponse("Order status has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating order status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_store_serving_area_list", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAreasList(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            List<AreaEntity> area = storeManagerWebService.getStoreServingAreas(headerDto.getStoreId());
            ServiceResponse serviceResponse = new ServiceResponse("Serving Areas of store has been retrieved successfully");
            serviceResponse.addParam("areas", area);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving serving area of store", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_driver_performance", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getDriverPerformanceList(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            List<DriverEntity> driverPerformance = storeManagerWebService.getDriverPerformance(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver performance has been retrieved successfully");
            serviceResponse.addParam("driverPerformance", driverPerformance);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving driver performance", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_eligible_driver_list", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getDriverList(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            List<DriverEntity> driverList = storeManagerWebService.getDriverListForOrder(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver list has been retrieved successfully.");
            serviceResponse.addParam("driverList", driverList);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving driver list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/cancel_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> cancelOrder(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID);
            storeManagerWebService.cancelOrder(headerDto, requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order has been canceled successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while canceling order.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_store_allowing_driver", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStoreAllowDriverList(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            List<StoreEntity> stores = storeManagerWebService.getStoreAllowingDriver(headerDto.getUserId());
            ServiceResponse serviceResponse = new ServiceResponse("Store allowing driver list has been retrieved successfully.");
            serviceResponse.addParam("stores", stores);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving store allowing driver list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }


    @RequestMapping(value = "/get_store_list_by_user_role", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAllStoreListByUserRole(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            List<StoreEntity> stores = storeManagerWebService.getStoreListByUserRole(headerDto.getUserId());
            ServiceResponse serviceResponse = new ServiceResponse("Store list retrieved successfully");
            serviceResponse.addParam("stores", stores);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving store list", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/order_list_by_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getOrderListByStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto orderList = storeManagerWebService.getOrderByStatus(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order list has been retrieved successfully.");
            serviceResponse.addParam("orderList", orderList);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving order list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_order_num_by_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getNumberOfOrderByStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            Map<String, Integer> numberOfOrderByStatus = storeManagerWebService.getNumberOfOrderByStatus(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Number of order by status has been retrieved successfully.");
            serviceResponse.addParam("numberOfOrderByStatus", numberOfOrderByStatus);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving number of order by status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_current_orders_of_driver", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getNumberOfOrderByStatus(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.DRIVER_ID);
            List<OrderEntity> orders = storeManagerWebService.getCurrentOrderOfDrivers(headerDto.getDriverId());
            ServiceResponse serviceResponse = new ServiceResponse("Current orders of driver has been retrieved successfully.");
            serviceResponse.addParam("orders", orders);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving current order of driver.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateOrder(@RequestHeader HttpHeaders headers, @RequestBody OrderEntity order) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID);
            storeManagerWebService.updateOrder(headerDto.getOrderId(), order);
            ServiceResponse serviceResponse = new ServiceResponse("Order has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating order.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/order_count_by_date", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getOrderCountByDate(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            Map<String, Integer> orderCountByDate = storeManagerWebService.getOrderCountByDate(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order count by date has been retrieved successfully.");
            serviceResponse.addParam("orderCountByDate", orderCountByDate);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving order count by date.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_statement", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getStatementList(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto paginationDto = storeManagerWebService.getStatementList(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Statement list has been retrieved successfully.");
            serviceResponse.addParam("statementList", paginationDto);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving Statement list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_statement", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStatement(@RequestParam String statementId) {
        try {
            StatementEntity statement = storeManagerWebService.getStatement(statementId);
            ServiceResponse serviceResponse = new ServiceResponse("Statement has been retrieved successfully.");
            serviceResponse.addParam("statement", statement);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving Statement.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_statement", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateStatement(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            storeManagerWebService.updateStatement(requestJsonDto.getStatement());
            ServiceResponse serviceResponse = new ServiceResponse("Statement has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating Statement.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/generate_statement", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> generateStatement(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            storeManagerWebService.generateStatement(requestJsonDto.getStatement());
            ServiceResponse serviceResponse = new ServiceResponse("Statement has been generated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while generating Statement.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/order_report", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getOrdersByDrivers(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto orderList = storeManagerWebService.getOrderByDrivers(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("List of orders by driverIds has been retrieved successfully");
            serviceResponse.addParam("orderList", orderList);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving orders by driverIds", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_stores_by_merchants", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getStoresByMerchants(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto storeBrandList = storeManagerWebService.getStoreBrandListByMerchants(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("List of Stores by merchantIds has been retrieved successfully");
            serviceResponse.addParam("storeBrandList", storeBrandList);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving list of Stores by merchantIds", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_drivers_by_stores", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getDriversByStoreBrands(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto driverList = storeManagerWebService.getDriversByStoreBrands(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("List of Drivers by storeIds has been retrieved successfully");
            serviceResponse.addParam("driverList", driverList);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving list of Drivers by storeIds", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
