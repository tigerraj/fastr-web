package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.ManagerService;
import com.yetistep.anyorder.service.inf.MerchantService;
import com.yetistep.anyorder.service.inf.StoreManagerWebService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/organizer")
public class ManagerController {
    Logger log = Logger.getLogger(ManagerController.class);

    @Autowired
    ManagerService managerService;

    @Autowired
    MerchantService merchantService;

    @Autowired
    StoreManagerWebService storeManagerWebService;

    @RequestMapping(value="/save_area", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> saveArea(@RequestBody AreaEntity area){
        try{
            managerService.saveArea(area);
            ServiceResponse serviceResponse = new ServiceResponse("Area saved successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while saving area", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);

        }
    }

    @RequestMapping(value="/update_area", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateArea(@RequestHeader HttpHeaders headers, @RequestBody AreaEntity area){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            managerService.updateArea(headerDto.getId(), area);
            ServiceResponse serviceResponse = new ServiceResponse("Area updated successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating area", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);

        }
    }


    @RequestMapping(value="/delete_area", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> deleteArea(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            managerService.deleteArea(headerDto.getId());
            ServiceResponse serviceResponse = new ServiceResponse("Area has been deleted successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while deleting area", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_merchant_list", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getMerchantList(@RequestBody RequestJsonDto requestJsonDto){
        try{
            PaginationDto merchantList = managerService.getMerchantList(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Merchant list has been retrieved successfully.");
            serviceResponse.addParam("merchantList", merchantList);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving merchant list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/save_rate_plan", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> saveRatingPlan(@RequestHeader HttpHeaders headers, @RequestBody RatePlanEntity ratePlan){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            managerService.saveRatePlan(headerDto.getStoreId(), ratePlan);
            ServiceResponse serviceResponse = new ServiceResponse("Rate plan has been saved successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while saving rating plan.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_rate_plan", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateRatingPlan(@RequestHeader HttpHeaders headers, @RequestBody RatePlanEntity ratePlan){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            managerService.updateRatePlan(headerDto.getId(), ratePlan);
            ServiceResponse serviceResponse = new ServiceResponse("Rate plan has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating rating plan.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/save_unknown_area_rate_plan", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> saveUnknownAreaRatingPlan(@RequestHeader HttpHeaders headers, @RequestBody UnknownAreaRatePlanEntity unknownAreaRatePlan){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            managerService.saveUnknownAreaRatePlan(headerDto.getStoreId(), unknownAreaRatePlan);
            ServiceResponse serviceResponse = new ServiceResponse("Rate plan has been saved successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while saving rating plan.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_unknown_area_rate_plan", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateRatingPlan(@RequestHeader HttpHeaders headers, @RequestBody UnknownAreaRatePlanEntity unknownAreaRatePlan){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            managerService.updateUnknownAreaRatePlan(headerDto.getId(), unknownAreaRatePlan);
            ServiceResponse serviceResponse = new ServiceResponse("Rating plan has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating rating plan.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/approve_statement", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> approveStatement(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            managerService.approveStatement(requestJsonDto.getStatement());
            ServiceResponse serviceResponse = new ServiceResponse("Statement payment cleared.");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while clearing statement payment.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/notification", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getNotifications(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            List<NotificationEntity> notifications = managerService.getNotificationByDeviceType(requestJsonDto.getUserDeviceType());
            ServiceResponse serviceResponse = new ServiceResponse("Notification has been retrieved successfully.");
            serviceResponse.addParam("notifications", notifications);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving Notification.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/send_notification", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> sendPushNotification(@RequestBody RequestJsonDto requestJsonDto) {
        try{
            managerService.sendPushNotification(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Notification sent successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error occurred while sending push notification", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_active_merchant", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getActiveMerchant() {
        try {
            List<UserEntity> merchants = managerService.getActiveMerchant();
            ServiceResponse serviceResponse = new ServiceResponse("Active merchant has been retrieved successfully");
            serviceResponse.addParam("merchants", merchants);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving active merchant", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_stores_by_merchants", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getStoresByMerchants(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto storeBrandList = merchantService.getStoreBrandListByMerchants(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("List of Stores by merchantIds has been retrieved successfully");
            serviceResponse.addParam("storeBrandList", storeBrandList);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving list of Stores by merchantIds", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
