package com.yetistep.anyorder.controller.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.model.AreaEntity;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.StoreEntity;
import com.yetistep.anyorder.model.xml.APIData;
import com.yetistep.anyorder.service.inf.APIService;
import com.yetistep.anyorder.service.inf.StoreManagerWebService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.YSException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 1/25/17
 * Time: 12:33 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/api/json", produces = "application/json")
public class JSONApiController {

    private static final Logger log = Logger.getLogger(JSONApiController.class);

    @Autowired
    APIService apiService;

    @Autowired
    StoreManagerWebService storeManagerWebService;

    @RequestMapping(value = "/get_active_stores", method = RequestMethod.POST)
    @ResponseBody
    public APIData getActiveStores(@RequestBody APIData apiData, HttpServletResponse httpRes){
        APIData root = new APIData();
        try {
            List<StoreEntity> stores = apiService.getActiveStoresByAPIKey(apiData.getApiKey());
            root.setStores(stores);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while getting active stores", e);
            setErrorMessage(root, httpRes, e);
        }
        return root;
    }

    @RequestMapping(value = "/get_serving_area", method = RequestMethod.POST)
    @ResponseBody
    public APIData getActiveArea(@RequestBody APIData apiData, HttpServletResponse httpRes) throws Exception {
        APIData root = new APIData();
        try {
            List<AreaEntity> areas = apiService.storeServingAreas(apiData.getApiKey(), apiData.getStoreId());
            root.setAreas(areas);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while getting serving area", e);
            setErrorMessage(root, httpRes, e);
        }
        return root;
    }

    @RequestMapping(value = "/save_order", method = RequestMethod.POST)
    @ResponseBody
    public APIData saveOrder(@RequestBody APIData apiData, HttpServletResponse httpRes) throws Exception {
        APIData root = new APIData();
        try {
            String orderId = apiService.saveOrder(apiData);
            root.setOrderId(orderId);
            root.setMessage("Order has been saved successfully");
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while getting serving area", e);
            setErrorMessage(root, httpRes, e);
        }
        return root;
    }

    @RequestMapping(value = "/order_list", method = RequestMethod.POST)
    @ResponseBody
    public APIData APIOrderList(@RequestBody APIData apiData, HttpServletResponse httpRes) throws Exception {
        APIData root = new APIData();
        try {
            List<OrderEntity> orders = apiService.APIOrderList(apiData);
            root.setOrders(orders);
            ObjectMapper mapper=new ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            final String val=mapper.writeValueAsString(root);
            final APIData xmlDat=mapper.readValue(val,APIData.class);
            return xmlDat;
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while getting order list", e);
            setErrorMessage(root, httpRes, e);
        }
        return root;
    }

    @RequestMapping(value = "/order_detail", method = RequestMethod.POST)
    @ResponseBody
    public APIData orderDetail(@RequestBody APIData apiData, HttpServletResponse httpRes) throws Exception {
        APIData root = new APIData();
        try {
            HeaderDto headerDto = new HeaderDto();
            headerDto.setOrderId(apiData.getOrderId());
            OrderEntity order = storeManagerWebService.getOrderDetail(headerDto);
            ObjectMapper mapper=new ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            final String val=mapper.writeValueAsString(order);
            final OrderEntity orderNew=mapper.readValue(val,OrderEntity.class);
            root.setOrder(orderNew);
            return root;
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while getting order list", e);
            setErrorMessage(root, httpRes, e);
        }
        return null;
    }

    @RequestMapping(value = "/estimate_charge", method = RequestMethod.POST)
    @ResponseBody
    public APIData estimateCharge(@RequestBody APIData apiData, HttpServletResponse httpRes) throws Exception {
        APIData root = new APIData();
        try {
            BigDecimal estimateCharge = apiService.estimateCharge(apiData);
            root.setEstimateCharge(estimateCharge.setScale(2,BigDecimal.ROUND_HALF_UP));
            root.setMessage("Order estimate charge has been calculated");
        }catch (Exception e){
            setErrorMessage(root, httpRes, e);
        }
        return root;
    }

    public static void setErrorMessage(APIData apiData, HttpServletResponse httpStatus, Exception e){
        apiData.setErrorCode(((YSException) e).getErrorCode());
        apiData.setErrorMessage(e.getMessage());
        httpStatus.setStatus(HttpStatus.EXPECTATION_FAILED.value());
    }
}
