package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.PasswordActionType;
import com.yetistep.anyorder.model.MerchantEntity;
import com.yetistep.anyorder.service.inf.MerchantService;
import com.yetistep.anyorder.service.inf.UserService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import com.yetistep.anyorder.util.SessionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by BinaySingh on 5/11/2016.
 */
@Controller
@RequestMapping(value = "/anon")
public class AnonController {

    private static final Logger log = Logger.getLogger(AnonController.class);

    @Autowired
    UserService userService;

    @Autowired
    MerchantService merchantService;

    @RequestMapping(value = "/password_assist", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ServiceResponse> changePassword(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto actionType) {
        try {
            HeaderDto headerDto = new HeaderDto();
            if (actionType.getActionType().equals(PasswordActionType.NEW) || actionType.getActionType().equals(PasswordActionType.RESET)) {
                GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.PASSWORD, GeneralUtil.VERIFICATION_CODE);
            } else {
                GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USERNAME);
            }
            String msg = userService.performPasswordAction(headerDto, actionType.getActionType());

            ServiceResponse serviceResponse = new ServiceResponse(msg);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while assisting password", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/save_merchant", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> saveMerchant(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            MerchantEntity merchant = requestJsonDto.getMerchant();
            merchant.getUser().setPassword(requestJsonDto.getPassword());
            merchantService.saveMerchant(merchant);
            ServiceResponse serviceResponse = new ServiceResponse("Your account has been created successfully. A welcome email has been sent to you.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while creating merchant", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_password")
    public ResponseEntity<ServiceResponse> changePassword(@RequestHeader HttpHeaders httpHeaders) throws Exception {
        try {
            HeaderDto headerDto = new HeaderDto();
            List<String> hd = httpHeaders.get("id");
            if (hd == null || hd.size() == 0)
                httpHeaders.add("id", SessionManager.getUserId().toString());

            GeneralUtil.fillHeaderCredential(httpHeaders, headerDto, GeneralUtil.ID, GeneralUtil.PASSWORD, GeneralUtil.NEW_PASSWORD);
            userService.changePassword(headerDto);

            ServiceResponse serviceResponse = new ServiceResponse("User password changed successfully");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);

        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while changing password", e);
            HttpHeaders headers = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(headers, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
