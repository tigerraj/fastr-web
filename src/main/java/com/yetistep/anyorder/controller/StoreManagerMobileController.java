package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.PreferenceType;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.StoreManagerMobileService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(value = "/msmanager")
public class StoreManagerMobileController {

    private static Logger log = Logger.getLogger(StoreManagerMobileController.class);

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    StoreManagerMobileService storeManagerMobileService;

    @Autowired
    SystemPropertyService systemPropertyService;

    @RequestMapping(value="/login_store_manager", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> loginStoreManager(@RequestHeader HttpHeaders headers, @RequestBody UserDeviceEntity userDevice){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USERNAME, GeneralUtil.PASSWORD);
            String userAgent = httpServletRequest.getHeader("User-Agent");
            UserAgent ua = UserAgent.parseUserAgentString(userAgent);
            String family = ua.getOperatingSystem().name();
            userDevice.setFamily(family);
            UserEntity user = storeManagerMobileService.storeManagerLogin(headerDto, userDevice);
            StoreEntity store = null;
            try{
                store = storeManagerMobileService.getSpecialPrivilegeInfo(user.getAccessToken());
            } catch (Exception e){
                GeneralUtil.logError(log, "Error Occurred while logging in the driver", e);
                //do noting
            }

            ServiceResponse serviceResponse = new ServiceResponse("Driver has been logged in successfully");
            serviceResponse.addParam("userDetail", user);
            serviceResponse.addParam("setting", store);
            serviceResponse.addParam("currency", systemPropertyService.readPrefValue(PreferenceType.CURRENCY));
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);

        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while logging in the driver", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);

        }
    }

    @RequestMapping(value = "/create_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> createOrder(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN, GeneralUtil.STORE_MANAGER_ID);

            OrderEntity orderEntity = storeManagerMobileService.createOrder(headerDto, requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order has been created successfully");
            serviceResponse.addParam("order", orderEntity);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while creating orders", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    //not used
    @RequestMapping(value = "/find_customer_by_mobile_number", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> findCustomerByMobileNumber(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);

            List<UserEntity> customers = storeManagerMobileService.getCustomersByMobileNumber(headerDto, requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("customers has been retrieved successfully");
            serviceResponse.addParam("customers", customers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while retrieving customers", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_customers_info", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getCustomersInfo(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);

            CustomerEntity customer = storeManagerMobileService.getCustomersInfo(headerDto, requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("customers addresses has been retrieved successfully");
            serviceResponse.addParam("customer", customer);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while retrieving customers addresses", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_area", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAreas(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);

            List<AreaEntity> area = storeManagerMobileService.getArea(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Area has been retrieved successfully");
            serviceResponse.addParam("areas",area);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving area", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_stores_drivers", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStoresDrivers(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);

            List<DriverEntity> drivers = storeManagerMobileService.getStoresDrivers(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("drivers has been retrieved successfully");
            serviceResponse.addParam("drivers",drivers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving drivers", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_order_detail", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getOrderDetail(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID, GeneralUtil.ACCESS_TOKEN);

            OrderEntity order = storeManagerMobileService.getOrderDetail(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order detail has been retrieved successfully");
            serviceResponse.addParam("order", order);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving order detail", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_stores_settings", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStoresSetting(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);
            StoreEntity store = storeManagerMobileService.getSpecialPrivilegeInfo(headerDto.getAccessToken());
            ServiceResponse serviceResponse = new ServiceResponse("Stores settings has been retrieved successfully");
            serviceResponse.addParam("setting", store);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving stores settings", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/assign_driver_to_order", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> assignDriverToOrder(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.DRIVER_ID, GeneralUtil.ORDER_ID, GeneralUtil.ACCESS_TOKEN);
            OrderEntity orderEntity = storeManagerMobileService.assignDriverToOrder(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver has been assigned to order successfully");
            serviceResponse.addParam("order", orderEntity);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while assigning driver to order", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_total_number_of_orders", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getTotalNumberOfOrders(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);
            RequestJsonDto responseJson = storeManagerMobileService.getTotalNumberOfOrders(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Total number of orders has been retrieved successfully");
            serviceResponse.addParam("numberOfOrders", responseJson);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving total number of orders", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_stores_orders", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getStoresOrders(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);
            PaginationDto paginationDto = storeManagerMobileService.getStoresOrders(headerDto, requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Stores orders has been retrieved successfully");
            serviceResponse.addParam("orders", paginationDto);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving stores orders", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_order_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateOrderStatus(@RequestHeader HttpHeaders headers, @RequestBody OrderEntity order){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto ,GeneralUtil.ACCESS_TOKEN, GeneralUtil.ORDER_ID);
            OrderEntity orderEntity = storeManagerMobileService.updateOrderStatus(headerDto, order);
            ServiceResponse serviceResponse = new ServiceResponse("Order status has been updated successfully.");
            serviceResponse.addParam("order", orderEntity);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating order status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }


    @RequestMapping(value="/search_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateOrderStatus(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJson){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto ,GeneralUtil.ACCESS_TOKEN);
            PaginationDto orders = storeManagerMobileService.searchOrder(headerDto, requestJson);
            ServiceResponse serviceResponse = new ServiceResponse("Order status has been updated successfully.");
            serviceResponse.addParam("orders", orders);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating order status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_eligible_driver_list", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getEligibleDriverList(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN, GeneralUtil.ORDER_ID, GeneralUtil.AREA_ID, GeneralUtil.CUSTOMER_AREA_ID);
            List<DriverEntity> driverList = storeManagerMobileService.getEligibleDriverListForOrder(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Eligible Driver list has been retrieved successfully.");
            serviceResponse.addParam("drivers", driverList);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving eligible driver list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/estimate_delivery_charge", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> estimateDeliveryCharge(@RequestHeader HttpHeaders headers, @RequestBody CustomersAreaEntity customersArea) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);
            OrderEntity order = storeManagerMobileService.estimateDeliveryCharge(headerDto, customersArea);
            ServiceResponse serviceResponse = new ServiceResponse("Estimate delivery charge for customer to store calculated successfully");
            serviceResponse.addParam("data", order);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while calculating estimate delivery charge from store to customer", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_device_token", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateDeviceToken(@RequestHeader HttpHeaders headers, @RequestBody UserDeviceEntity userDevice) throws Exception {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);
            String userAgent = httpServletRequest.getHeader("User-Agent");
            UserAgent ua = UserAgent.parseUserAgentString(userAgent);
            String family = ua.getOperatingSystem().name();
            userDevice.setFamily(family);
            storeManagerMobileService.updateDeviceToken(headerDto, userDevice);
            ServiceResponse serviceResponse = new ServiceResponse("Device token has been updated successful");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while updating device token", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_store_serving_area", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getAreasList(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN);
            List<AreaEntity> area = storeManagerMobileService.getStoreServingAreas(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Serving Areas of store has been retrieved successfully");
            serviceResponse.addParam("areas", area);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving serving area of store", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/cancel_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> cancelOrder(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID, GeneralUtil.ACCESS_TOKEN);
            storeManagerMobileService.cancelOrder(headerDto, requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order has been canceled successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while canceling order.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

}
