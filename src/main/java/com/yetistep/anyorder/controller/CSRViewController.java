package com.yetistep.anyorder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Dell Inspiron 3847 on 5/31/2016.
 */
@Controller
@RequestMapping(value = "/csr")
public class CSRViewController {

    @RequestMapping(value = "/{page}/**", method = RequestMethod.GET)
    public ModelAndView merchantSignup(@PathVariable String page){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("csr/" + page);
        return modelAndView;
    }
}
