package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.service.inf.CustomerService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 7/8/16
 * Time: 10:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/customer")
public class CustomerController {

     Logger log = Logger.getLogger(CustomerController.class);

    @Autowired
    CustomerService customerService;

    @RequestMapping(value="/get_order_detail", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getOrderDetail(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ORDER_ID);

            OrderEntity order = customerService.getOrderDetail(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order detail has been retrieved successfully");
            serviceResponse.addParam("order", order);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving order detail", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/rate_order", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> rateOrder(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            customerService.rateOrder(requestJsonDto.getOrder());
            ServiceResponse serviceResponse = new ServiceResponse("Order has been rated successfully.");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while rating order.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
