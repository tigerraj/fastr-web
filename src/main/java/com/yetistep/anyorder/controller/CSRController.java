package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.abs.Constant;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.service.inf.CSRService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 9:51 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/csr")
public class CSRController{

    Logger log = Logger.getLogger(CSRController.class);

    @Autowired
    CSRService csrService;

    @RequestMapping(value = {"/dashboard"}, method = RequestMethod.GET)
    public ModelAndView defaultPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", Constant.TITLE+" dashboard");
        model.setViewName("/csr/dashboard");
        return model;
    }

    @RequestMapping(value = "/get_user/{userId}", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getUserDetail(@PathVariable String userId) {
        try{

            UserEntity userEntity = csrService.getUserDetail(userId);
            ServiceResponse serviceResponse = new ServiceResponse("User detail retrieved successfully.");
            serviceResponse.addParam("userDetail", userEntity);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving user detail.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_user", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateUser(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            UserEntity userEntity = csrService.updateUser(requestJsonDto.getUser());
            ServiceResponse serviceResponse = new ServiceResponse("User updated successfully.");
            serviceResponse.addParam("updatedUser", userEntity);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while updating user.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_user_status", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> changeUserStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            UserEntity userEntity = csrService.changeUserStatus(requestJsonDto.getUser());
            ServiceResponse serviceResponse = new ServiceResponse("User status changed successfully.");
            serviceResponse.addParam("statusChangedUser", userEntity);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while changing user status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
