package com.yetistep.anyorder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by BinaySingh on 5/24/2016.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminViewController {

    @RequestMapping(value = "/{page}/**", method = RequestMethod.GET)
    public ModelAndView merchantSignup(@PathVariable String page){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/" + page);
        return modelAndView;
    }
}
