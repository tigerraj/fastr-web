package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.MerchantService;
import com.yetistep.anyorder.service.inf.StoreManagerWebService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 4/29/16
 * Time: 3:21 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/merchant")
public class MerchantController {
    Logger log = Logger.getLogger(MerchantController.class);

    @Autowired
    MerchantService merchantService;

    @Autowired
    StoreManagerWebService storeManagerWebService;

    @RequestMapping(value="/save_stores_brand", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> saveStoresBrand(@RequestHeader HttpHeaders headers, @RequestBody StoreBrandEntity storeBrand){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.MERCHANT_ID);
            merchantService.saveStoresBrand(headerDto, storeBrand);
            ServiceResponse serviceResponse = new ServiceResponse("Stores Brand saved successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while saving stores brands", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_stores_brand", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateStoresBrand(@RequestHeader HttpHeaders headers, @RequestBody StoreBrandEntity storeBrand){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.MERCHANT_ID, GeneralUtil.BRAND_ID);
            merchantService.updateStoresBrand(headerDto, storeBrand);
            ServiceResponse serviceResponse = new ServiceResponse("Stores Brand updated successfully");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating stores brands", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_brand_detail", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getBrandDetails(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers,headerDto, GeneralUtil.BRAND_ID);
            StoreBrandEntity storeBrand =  merchantService.getBrandDetails(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Stores Brand detail retrieved successfully");
            serviceResponse.addParam("storeBrandDetail",storeBrand);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving stores brands detail", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/save_store_serving_area", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> saveStoreServingArea(@RequestHeader HttpHeaders headers, @RequestBody List<AreaEntity> areas){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers,headerDto, GeneralUtil.STORE_ID);
            merchantService.saveServingArea(headerDto, areas);
            ServiceResponse serviceResponse = new ServiceResponse("Serving Area of store has been added successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while adding serving area of store", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_store_serving_area", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateStoreServingArea(@RequestHeader HttpHeaders headers, @RequestBody List<AreaEntity> areas){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers,headerDto, GeneralUtil.STORE_ID);
            merchantService.updateServingArea(headerDto, areas);
            ServiceResponse serviceResponse = new ServiceResponse("Serving Area of store has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating serving area of store", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_store_serving_area", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStoreServingArea(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers,headerDto, GeneralUtil.STORE_ID);
            List<AreaEntity> areas = merchantService.getServingArea(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Serving Area of store has been retrieved successfully.");
            serviceResponse.addParam("servingArea",areas);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving serving area of store", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_merchant", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateMerchant(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.PASSWORD);
            merchantService.updateMerchant(headerDto, requestJsonDto.getMerchant());
            ServiceResponse serviceResponse = new ServiceResponse("Merchant updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating merchant", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/save_smanager", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> saveStoreManager(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            storeManagerWebService.saveStoreManager(requestJsonDto.getStoreManager());
            ServiceResponse serviceResponse = new ServiceResponse("Store Manager has been created successfully. An email has been sent to create the login credentials.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while creating merchant", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_merchant_detail", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getMerchantDetail(@RequestHeader HttpHeaders httpHeader){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(httpHeader, headerDto, GeneralUtil.MERCHANT_ID);
            MerchantEntity merchantEntity = merchantService.getMerchantDetail(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Merchant detail retrieved successfully.");
            serviceResponse.addParam("merchantDetail", merchantEntity);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving merchant detail.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/save_driver", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> saveDriver(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            String driverId = merchantService.saveDriver(requestJsonDto.getDriver());
            ServiceResponse serviceResponse = new ServiceResponse("Driver has been created successfully. An email has been sent to create the login credentials.");
            serviceResponse.addParam("driverId", driverId);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while creating driver", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_merchant_status", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> changeMerchantStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            merchantService.changeMerchantStatus(requestJsonDto.getMerchant());
            ServiceResponse serviceResponse = new ServiceResponse("Merchant's status has been changed successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while changing merchant's status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_store_brand_status", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> changeStoreBrandStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            merchantService.changeStoreBrandStatus(requestJsonDto.getStoreBrand());
            ServiceResponse serviceResponse = new ServiceResponse("StoreBrand's status has been changed successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while changing storeBrand's status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_store_status", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> changeStoreStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            merchantService.changeStoreStatus(requestJsonDto.getStore());
            ServiceResponse serviceResponse = new ServiceResponse("Store's status has been changed successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while changing store's status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_store_list", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getStoreBrandList(@RequestBody RequestJsonDto requestJsonDto) {
        try{
            PaginationDto storeBrandList = merchantService.getStoreList(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("StoreBrand list has been retrieved successfully.");
            serviceResponse.addParam("storeBrandList", storeBrandList);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving StoreBrand list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_store_manager_status", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> changeStoreManagerStatus(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            storeManagerWebService.changeStoreManagerStatus(requestJsonDto.getStoreManager());
            ServiceResponse serviceResponse = new ServiceResponse("StoreManager's status has been changed successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while changing Store Manager's status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_store_managers", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getStoreManagers(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            List<StoreManagerEntity> storeManagers = merchantService.getStoreManagers(headerDto.getStoreId());
            ServiceResponse serviceResponse = new ServiceResponse("Store Manager list has been retrieved successfully.");
            serviceResponse.addParam("storeManagers", storeManagers);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving store mangager list", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_rate_plan", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getRatePlan(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            RequestJsonDto ratePlans = merchantService.getRatePlanOfStore(headerDto.getStoreId());
            ServiceResponse serviceResponse = new ServiceResponse("Rate plan has been retrieved successfully.");
            serviceResponse.addParam("ratePlans",ratePlans);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving rating plan.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_unknown_area_rate_plan", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getUnknownAreaRatePlanOfStore(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.STORE_ID);
            List<UnknownAreaRatePlanEntity> ratePlans = merchantService.getUnknownAreaRatePlanOfStore(headerDto.getStoreId());
            ServiceResponse serviceResponse = new ServiceResponse("Rate plan has been retrieved successfully.");
            serviceResponse.addParam("ratePlans",ratePlans);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving rating plan.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_notification", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getNotifications(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            RequestJsonDto notifications = merchantService.getNotification(headerDto.getUserId());
            ServiceResponse serviceResponse = new ServiceResponse("Notification has been retrieved successfully.");
            serviceResponse.addParam("notifications", notifications);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving Notification.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_viewed_notification", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateNotifications(@RequestHeader HttpHeaders headers, @RequestBody List<String> notificationIds) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            merchantService.updateViewedNotification(headerDto.getUserId(), notificationIds);
            ServiceResponse serviceResponse = new ServiceResponse("Notification has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating Notification.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_stores_by_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> storeListByStatus(@RequestHeader HttpHeaders headers, @RequestBody Page page) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            PaginationDto paginationDto = merchantService.getStoreListByStatus(headerDto.getUserId(), page);
            ServiceResponse serviceResponse = new ServiceResponse("Store list has been retrieved successfully.");
            serviceResponse.addParam("storesBrand", paginationDto);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving store list by status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_statement", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateStatement(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            merchantService.updateStatement(requestJsonDto.getStatement());
            ServiceResponse serviceResponse = new ServiceResponse("Statement has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating Statement.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/generate_statement", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> generateStatement(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            merchantService.generateStatement(requestJsonDto.getStatement());
            ServiceResponse serviceResponse = new ServiceResponse("Statement has been generated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while generating Statement.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/generate_api_key", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> generateApiKey(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            String dbApiKey = merchantService.generateAPIKey(headerDto.getUserId());
            ServiceResponse serviceResponse = new ServiceResponse("API key has been generated successfully.");
            serviceResponse.addParam("apiKey", dbApiKey);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while generating API key.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/save_api_key", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> generateApiKey(@RequestHeader HttpHeaders headers, @RequestBody APIKeyEntity apiKey) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            merchantService.saveAPIKey(headerDto.getUserId(), apiKey);
            ServiceResponse serviceResponse = new ServiceResponse("API key has been saved successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while saving API key.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/delete_api_key", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> deleteAPIKey(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID, GeneralUtil.API_KEY_ID);
            merchantService.deleteAPIKey(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("API key has been deleted successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while deleting API key.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_active_api_keys", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getActiveAPIKeys(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USER_ID);
            List<APIKeyEntity> apiKeys = merchantService.getActiveAPIKeys(headerDto.getUserId());
            ServiceResponse serviceResponse = new ServiceResponse("API keys has been retrieved successfully.");
            serviceResponse.addParam("apiKeys",apiKeys);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while getting API keys of merchant.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
