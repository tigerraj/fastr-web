package com.yetistep.anyorder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 7/8/16
 * Time: 10:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/customer")
public class CustomerViewController {

    @RequestMapping(value = "/order/**", method = RequestMethod.GET)
    public ModelAndView merchantSignup(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("customer/order");
        return modelAndView;
    }

}
