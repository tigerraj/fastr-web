package com.yetistep.anyorder.controller;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.yetistep.anyorder.abs.Constant;
import com.yetistep.anyorder.enums.PreferenceType;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.AuthenticatedUser;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.service.inf.UserService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import com.yetistep.anyorder.util.SessionManager;
import com.yetistep.anyorder.util.YSException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

@Controller
public class MainController {

    private static Logger log = Logger.getLogger(MainController.class);
    @Autowired
    SystemPropertyService systemPropertyService;
    @Autowired
    UserService userService;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public ModelAndView defaultPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", Constant.TITLE);
        model.addObject("message", "Home Page!");
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null && !SessionManager.isAnonymousUser()) {
            UserRole userRole = SessionManager.getRole();
            if (userRole.toString().equals(UserRole.ROLE_ADMIN.toString())) {
                model.setViewName("smanager/order_list");
            } else if (userRole.toString().equals(UserRole.ROLE_MANAGER.toString())) {
                model.setViewName("smanager/order_list");
            } else if (userRole.toString().equals(UserRole.ROLE_CSR.toString())) {
                model.setViewName("smanager/order_list");
            } else if (userRole.toString().equals(UserRole.ROLE_MERCHANT.toString())) {
                model.setViewName("smanager/order_list");
            } else if (userRole.toString().equals(UserRole.ROLE_STORE_MANAGER.toString())) {
                model.setViewName("smanager/order_list");
            }
        } else {
            model.setViewName("login");
        }
        return model;
    }

    @RequestMapping(value = "/test_users/{facebookId}", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> testUsers(@PathVariable("facebookId") Long facebookId) {
        try {

            List<UserEntity> users = userService.getAllUsers();
            ServiceResponse serviceResponse = new ServiceResponse("Recommended items has been fetched successfully");
            serviceResponse.addParam("users", users);

            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while fetching recommended items", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/test_save_user", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> testSaveUser(@RequestBody UserEntity userEntity) {
        try {
            userService.saveUser(userEntity);
            ServiceResponse serviceResponse = new ServiceResponse("user has been save successfully");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while saving user", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = {"/welcome"}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ServiceResponse> loginDefaultPage() {
        try {
            String url = "";
            AuthenticatedUser userDetails = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null && !SessionManager.isAnonymousUser()) {
                UserRole userRole = SessionManager.getRole();
                if (userRole.toString().equals(UserRole.ROLE_ADMIN.toString())) {
                    url = "smanager/order_list";
                } else if (userRole.toString().equals(UserRole.ROLE_MANAGER.toString())) {
                    url = "smanager/order_list";
                } else if (userRole.toString().equals(UserRole.ROLE_CSR.toString())) {
                    url = "smanager/order_list";
                } else if (userRole.toString().equals(UserRole.ROLE_MERCHANT.toString())) {
                    //url = "merchant/" + SessionManager.getMerchantId() + "/store_list";
                    url = "smanager/order_list";
                } else if (userRole.toString().equals(UserRole.ROLE_STORE_MANAGER.toString())) {
                    url = "smanager/order_list";
                }
            }
            ServiceResponse serviceResponse = new ServiceResponse("User has been logged in successfully");
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            serviceResponse.addParam("url", url);
            serviceResponse.addParam("userDetails", userDetails);
            serviceResponse.addParam("currency", systemPropertyService.readPrefValue(PreferenceType.CURRENCY));
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.CREATED);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred getting welcome page", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }


    @RequestMapping(value = "/auth_failed", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> auth_failed(HttpServletRequest request) throws Exception{
        try {
            String key = "SPRING_SECURITY_LAST_EXCEPTION";
            Exception exception = (Exception) request.getSession().getAttribute(key);
            if (exception instanceof BadCredentialsException) {
                throw new YSException("USR007");
            } else if (exception instanceof InternalAuthenticationServiceException) {
                throw new YSException(((YSException)exception.getCause()).getErrorCode());
            } else {
                throw new YSException("USR007");
            }
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while login", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/access_denied")
    @ResponseBody
    public ModelAndView merchantStore(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/access_denied");
        return modelAndView;
    }

    @RequestMapping(value = "/signup")
    @ResponseBody
    public ModelAndView merchantSignup(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/signup");
        return modelAndView;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    @ResponseBody
    public ServiceResponse login(@RequestParam(value = "error", required = false) String error,
                                 @RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {
        String message = "";
        if (error != null) {
            message = getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION");
        }
        if (logout != null) {
            message = "You've been logged out successfully.";
        }
        return new ServiceResponse(message);
    }

    // customize the error message
    private String getErrorMessage(HttpServletRequest request, String key) {
        Exception exception = (Exception) request.getSession().getAttribute(key);
        String error = "";
        if (exception instanceof BadCredentialsException) {
            error = "Invalid username and password!";
        } else if (exception instanceof LockedException) {
            error = exception.getMessage();
        } else {
            error = "Invalid username and password!";
        }
        return error;
    }

    // for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {
        ModelAndView model = new ModelAndView();
        // check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            System.out.println(userDetail);
            model.addObject("username", userDetail.getUsername());
        }
        model.setViewName("403");
        return model;

    }

    @RequestMapping(value = "/assistance/**", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView forgotPassword(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("assistance");
        return modelAndView;
    }

    @RequestMapping(value = "/forgot_password", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView forgotPasswordMobile(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("assistance/forgot_password");
        return modelAndView;
    }

    @RequestMapping(value = "/test_ios_push_notification", method = RequestMethod.GET)
    @ResponseBody
    public boolean testIosPushNotification(){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("CertificatesDevelopment.p12");
        PayloadBuilder payloadBuilder = APNS.newPayload();
        payloadBuilder = payloadBuilder.sound("default");
        payloadBuilder = payloadBuilder.alertBody("new push notification generated");
        payloadBuilder = payloadBuilder.customField("customMessage","this is the custom message");
        String payload = payloadBuilder.build();

        ApnsService service = APNS.newService().withCert(inputStream, "password").withSandboxDestination().build();
        service.push(Collections.singletonList("43ac6d057b9fd41bc2097f859b82599a483f13780592c0328ad1c37f35364fda"), payload);
        return true;
    }

    @RequestMapping(value = "/test_android_push_notification", method = RequestMethod.GET)
    @ResponseBody
    public boolean testAndroidPushNotification(){

        Sender sender = new Sender("AIzaSyDSYcW-7jQH2D-d9K9c_FpLhRIE2v87Og8");
        Message message = new Message.Builder()
                .collapseKey("1")
                .timeToLive(3)
                .delayWhileIdle(false)
                .addData("message", "test push notification")
                .build();

        try{
            MulticastResult result = sender.send(message, Collections.singletonList("ex88O_9ahyI:APA91bEY52m5ZElMjOfYxCGCBSJXdBCFPpo360Lg_2_IVLoDEMLgYjl-tmWE5UtQBPmQUJAhpuMnsrYEhLe3FIO5ZdCJYPNyVn437j3HrQ8IHF7hkRGUql7_r8wind7AfLRGqV61FKkS"), 3);

            if (result.getResults() != null) {
                int canonicalRegId = result.getCanonicalIds();
                if (canonicalRegId != 0) {
                }
            } else {
                int error = result.getFailure();
                log.error("failed push notification ids" + error);
            }
        } catch (Exception e){
            log.info("error");
        }

        return true;
    }

    @RequestMapping(value = "/checkout_test_charge/{cardToken}", method = RequestMethod.GET)
    @ResponseBody
    public boolean checkoutTestCharge(@PathVariable String cardToken){

        try {
            //(String cardToken, String description, String amount, String currency, String email)
            //CheckoutClient.ChargeWithCardToken(cardToken, "test charge", new BigDecimal(1), "$", "test@yopmail.com");
            //CheckoutClient.addTestFund(null, 100);
        } catch (Exception e){
            log.info("error");
        }
        return true;
    }

    /**
     * Added By Pratik
     * This function is for making and testing the new views.
     * Will be deleted later if not nessesary
     * @return
     */
    @RequestMapping(value = {"/new_view"}, method = RequestMethod.GET)
    public ModelAndView new_view() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Food Discovery");
        model.addObject("message", "Home Page!");

        model.setViewName("maptest"); // view file name
        return model;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handle(NoHandlerFoundException ex) {
        return "error";
    }
}