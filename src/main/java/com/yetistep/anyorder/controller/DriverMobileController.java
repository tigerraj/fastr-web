package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.enums.PreferenceType;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.DriverMobileService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 5/16/16
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(value = "/mdriver")
public class DriverMobileController {

    private static final Logger log = Logger.getLogger(DriverMobileController.class);

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    DriverMobileService driverMobileService;

    @Autowired
    SystemPropertyService systemPropertyService;

    @RequestMapping(value="/login", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> driverLogin(@RequestHeader HttpHeaders headers, @RequestBody UserDeviceEntity userDevice){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USERNAME, GeneralUtil.PASSWORD);
            String userAgent = httpServletRequest.getHeader("User-Agent");
            UserAgent ua = UserAgent.parseUserAgentString(userAgent);
            String family = ua.getOperatingSystem().name();
            userDevice.setFamily(family);
            DriverEntity driver = driverMobileService.driverLogin(headerDto, userDevice);
            ServiceResponse serviceResponse = new ServiceResponse("Driver has been logged in successfully");
            serviceResponse.addParam("driver", driver);
            serviceResponse.addParam("currency", systemPropertyService.readPrefValue(PreferenceType.CURRENCY));
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);

        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while logging in the driver", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_current_order", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getDriveCurrentOrderList(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto ,GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            List<OrderEntity> orders = driverMobileService.getDriverCurrentOrderList(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver current order has been retrieved successfully.");
            serviceResponse.addParam("orders", orders);
            serviceResponse.addParam("updateLocationInMinute", Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVER_LOCATION_UPDATE_TIME)));
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving driver current order list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_past_order", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getDriverPastOrderList(@RequestHeader HttpHeaders headers, @RequestBody Page page){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto ,GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            PaginationDto orders = driverMobileService.driverPastOrderList(headerDto, page);
            ServiceResponse serviceResponse = new ServiceResponse("Driver past order has been retrieved successfully.");
            serviceResponse.addParam("orders", orders);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving driver past order list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/get_profile", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getDriveProfile(@RequestHeader HttpHeaders headers){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto ,GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            DriverEntity driverDetail = driverMobileService.getDriveProfile(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver detail has been retrieved successfully.");
            serviceResponse.addParam("driver", driverDetail);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while retrieving driver detail.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_order_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateOrderStatus(@RequestHeader HttpHeaders headers, @RequestBody OrderEntity order){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto ,GeneralUtil.ACCESS_TOKEN, GeneralUtil.ID);
            driverMobileService.updateOrderStatus(headerDto, order);
            ServiceResponse serviceResponse = new ServiceResponse("Order status has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating order status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value="/update_driver_status", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateDriverStatus(@RequestHeader HttpHeaders headers, @RequestBody DriverEntity driver){
        try{
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto ,GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            driverMobileService.updateDriverStatus(headerDto, driver);
            ServiceResponse serviceResponse = new ServiceResponse("Driver status has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }catch (Exception e){
            GeneralUtil.logError(log, "Error Occurred while updating driver status.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/change_password", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> changePassword(@RequestHeader HttpHeaders headers) throws Exception {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.DRIVER_ID, GeneralUtil.PASSWORD, GeneralUtil.NEW_PASSWORD, GeneralUtil.ACCESS_TOKEN);
            driverMobileService.changePassword(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Password changed successfully");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while changing password", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_notification", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getNotification(@RequestHeader HttpHeaders headers) throws Exception {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            List<NotificationEntity> notification = driverMobileService.getNotification(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Notification has been retrieved successful");
            serviceResponse.addParam("notifications", notification);
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while retrieving notification", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_device_token", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateDeviceToken(@RequestHeader HttpHeaders headers, @RequestBody UserDeviceEntity userDevice) throws Exception {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            String userAgent = httpServletRequest.getHeader("User-Agent");
            UserAgent ua = UserAgent.parseUserAgentString(userAgent);
            String family = ua.getOperatingSystem().name();
            userDevice.setFamily(family);
            driverMobileService.updateDeviceToken(headerDto,userDevice);
            ServiceResponse serviceResponse = new ServiceResponse("Device token has been updated successful");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while updating device token", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_driver_location", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateDriverLocation(@RequestHeader HttpHeaders headers, @RequestBody DriverEntity driver) throws Exception {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            driverMobileService.updateDriverLocation(headerDto, driver);
            ServiceResponse serviceResponse = new ServiceResponse("Driver Location has been updated successfully");
            return new ResponseEntity<ServiceResponse>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error occurred while updating driver location", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<ServiceResponse>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_order_detail", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getOrderDetail(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID, GeneralUtil.ID);
            OrderEntity order = driverMobileService.getOrderDetails(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Order detail has been retrieved successfully.");
            serviceResponse.addParam("order", order);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving order detail.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/forgot_password", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> forgotPassword(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.USERNAME);
            driverMobileService.forgotPassword(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Email has been sent to " + headerDto.getUsername() + " successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while sending email.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> logout(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ACCESS_TOKEN, GeneralUtil.DRIVER_ID);
            driverMobileService.logoutDriver(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver has been logout successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while logout the driver.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
