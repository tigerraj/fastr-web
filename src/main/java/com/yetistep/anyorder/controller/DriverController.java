package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.DriverService;
import com.yetistep.anyorder.util.GeneralUtil;
import com.yetistep.anyorder.util.ServiceResponse;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(value = "/driver")
public class DriverController {

    private static final Logger log = Logger.getLogger(DriverController.class);

    @Autowired
    DriverService driverService;

    @RequestMapping(value = "/get_driver", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getDriver(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            //DriverEntity driver = driverService.getDriver();

            ServiceResponse serviceResponse = new ServiceResponse("Driver has been retrieved successfully");
            //serviceResponse.addParam("driver", driver);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving the driver", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_driver_serving_area", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse> updateStoreServingArea(@RequestHeader HttpHeaders headers, @RequestBody List<AreaEntity> areas) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            driverService.updateServingArea(headerDto, areas);
            ServiceResponse serviceResponse = new ServiceResponse("Serving Area of driver has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating serving area of driver", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_serving_area", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getDriverServingArea(@RequestHeader HttpHeaders headers) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            List<AreaEntity> areas = driverService.getServingArea(headerDto);
            ServiceResponse serviceResponse = new ServiceResponse("Serving Area of driver has been retrieved successfully.");
            serviceResponse.addParam("servingArea", areas);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving serving area of driver", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update_driver", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> updateDriver(@RequestHeader HttpHeaders headers, @RequestBody RequestJsonDto requestJsonDto) {
        try {
            HeaderDto headerDto = new HeaderDto();
            GeneralUtil.fillHeaderCredential(headers, headerDto, GeneralUtil.ID);
            driverService.updateDriver(headerDto, requestJsonDto.getDriver());
            ServiceResponse serviceResponse = new ServiceResponse("Drivers has been updated successfully.");
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while updating driver.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_driver_detail", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getDriverDetail(@RequestParam(value = "driverId") String driverId) {
        try {
            DriverEntity driverEntity = driverService.getDriver(driverId);
            ServiceResponse serviceResponse = new ServiceResponse("Driver's detail has been retrieved successfully.");
            serviceResponse.addParam("driverDetail", driverEntity);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving driver detail.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/get_driver_list", method = RequestMethod.POST)
    public ResponseEntity<ServiceResponse> getDriverList(@RequestBody RequestJsonDto requestJsonDto) {
        try {
            PaginationDto driverList = driverService.getDriverList(requestJsonDto);
            ServiceResponse serviceResponse = new ServiceResponse("Driver list has been retrieved successfully.");
            serviceResponse.addParam("driverList", driverList);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (Exception e) {
            GeneralUtil.logError(log, "Error Occurred while retrieving driver list.", e);
            HttpHeaders httpHeaders = ServiceResponse.generateRuntimeErrors(e);
            return new ResponseEntity<>(httpHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
