package com.yetistep.anyorder.abs;

import com.yetistep.anyorder.util.MessageBundle;

/**
 * Created by suroz on 12/9/2016.
 */
public class Constant {
    //For Anyorder
    /*public static final String TITLE ="Any Order";
    public static final String APP_NAME ="Any Order";
    public static final String SUPPORT_NAME ="AnyOrder";*/

    public static final String TITLE ="Fastr";
    public static final String APP_NAME ="Fastr";
    public static final String SUPPORT_NAME ="Fastr";
    public static final String SUPPORT_NUMBER = MessageBundle.getSupportNumber();
}
