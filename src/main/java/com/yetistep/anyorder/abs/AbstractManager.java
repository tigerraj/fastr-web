package com.yetistep.anyorder.abs;

import com.yetistep.anyorder.enums.OrderStatus;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.enums.UserStatus;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.*;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 11/25/14
 * Time: 11:04 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractManager {

    Logger log = Logger.getLogger(AbstractManager.class);

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    SystemPropertyService systemPropertyService;



    public static Long timeRequiredToCompleteOrder(DriverEntity driver, Integer timePerKm) throws Exception {
        Long waitingTime = 0L;
        List<OrderEntity> orderEntities = driver.getOrders();
        List<OrderEntity> runningOrders = new ArrayList<>();
        for (OrderEntity order : orderEntities) {
            if (!(order.getOrderStatus().equals(OrderStatus.DELIVERED) || order.getOrderStatus().equals(OrderStatus.CANCELLED)))
                runningOrders.add(order);
        }
        if (runningOrders.size() == 0)
            return waitingTime;
        Collections.sort(runningOrders, new Comparator<OrderEntity>() {
            @Override
            public int compare(OrderEntity o1, OrderEntity o2) {
                if (o1.getOrderPriority() == null) {
                    return (o2.getOrderPriority() == null) ? 0 : -1;
                }
                if (o2.getOrderPriority() == null) {
                    return 1;
                }
                return o1.getOrderPriority().compareTo(o2.getOrderPriority());
            }
        });
        ListIterator<OrderEntity> orders = runningOrders.listIterator();
        for (ListIterator<OrderEntity> order = runningOrders.listIterator(); order.hasNext(); ) {
            OrderEntity orderEntity = order.next();
            Map<String, Long> driverData = GeoCodingUtil.calculateDistance(
                    orderEntity.getDriver().getLatitude(), orderEntity.getDriver().getLongitude(),
                    orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(), timePerKm);
            waitingTime += driverData.get("duration");
            if (orderEntity.getOrderStatus().equals(OrderStatus.ORDER_STARTED)
                    || orderEntity.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_PICK_UP)
                    || orderEntity.getOrderStatus().equals(OrderStatus.AT_STORE)
                    || orderEntity.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_DELIVER)
                    || orderEntity.getOrderStatus().equals(OrderStatus.AT_CUSTOMER)) {
                Map<String, Long> storeToCustomerData = new HashMap<>();
                if (orderEntity.getCustomersArea().getArea() != null) {
                    storeToCustomerData = GeoCodingUtil.calculateDistance(
                            orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                            orderEntity.getCustomersArea().getArea().getLatitude(), orderEntity.getCustomersArea().getArea().getLongitude(), timePerKm);
                    waitingTime += storeToCustomerData.get("duration");
                } else {
                    storeToCustomerData = GeoCodingUtil.calculateDistance(
                            orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                            orderEntity.getCustomersArea().getLatitude(), orderEntity.getCustomersArea().getLongitude(), timePerKm);
                    waitingTime += storeToCustomerData.get("duration");
                }
                Map<String, Long> prevOrderCustomerToNewOrderStoreData = new HashMap<>();
                if (orders.hasPrevious()) {
                    OrderEntity prevOrderEntity = orders.previous();
                    if (orderEntity.getCustomersArea().getArea() != null) {
                        prevOrderCustomerToNewOrderStoreData = GeoCodingUtil.calculateDistance(
                                orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                                prevOrderEntity.getCustomersArea().getArea().getLatitude(), prevOrderEntity.getCustomersArea().getArea().getLongitude(), timePerKm);
                        waitingTime += prevOrderCustomerToNewOrderStoreData.get("duration");
                    } else {
                        prevOrderCustomerToNewOrderStoreData = GeoCodingUtil.calculateDistance(
                                orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                                prevOrderEntity.getCustomersArea().getLatitude(), prevOrderEntity.getCustomersArea().getLongitude(), timePerKm);
                        waitingTime += prevOrderCustomerToNewOrderStoreData.get("duration");
                    }
                }

            }
        }
        return waitingTime;
    }

    public String getServerName() {
        //Requested Server Name
        return httpServletRequest.getServerName();
    }

    public String getServerUrl() {
        //Requested URL
        return httpServletRequest.getRequestURL().toString().replace(httpServletRequest.getServletPath(), "").trim();
    }

    public String getIpAddress() {
        //Client IP Address
        String ipAddress = httpServletRequest.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null)
            ipAddress = httpServletRequest.getRemoteAddr();

        return ipAddress;
    }

    protected boolean sendMail(String toAddress, String message, String subject) throws Exception {
        EmailUtil.Email mail = new EmailUtil.Email(toAddress, subject, message);
        return sendCommonMail(mail);
    }

    protected boolean sendAttachmentEmail(String toAddress, String message, String subject, String path) throws Exception {
        EmailUtil.Email mail = new EmailUtil.Email(toAddress, subject, message);
        EmailUtil.Attachment attachment = new EmailUtil.Attachment(path);
        return sendMailWithAttachments(mail, attachment);
    }

    private boolean sendCommonMail(EmailUtil.Email mail) throws Exception {
        EmailUtil email = new EmailUtil(mail);
        email.sendAsynchMail();
        return true;
    }

    private boolean sendMailWithAttachments(EmailUtil.Email mail, EmailUtil.Attachment attachment) throws Exception {
        EmailUtil email = new EmailUtil(mail);
        email.sendAsynchMail(attachment);
        return true;
    }

    public void validateMobileClient(String token) throws Exception {
        log.info("++++++++++++++ Validating mobile client +++++++++++++++");

        String userAgent = httpServletRequest.getHeader("User-Agent");
        UserAgent ua = UserAgent.parseUserAgentString(userAgent);
        String timeStr = null;
        String family = ua.getOperatingSystem().name();


        if (family.toUpperCase().indexOf("IOS") >= 0 || family.toUpperCase().indexOf("MAC") >= 0 || family.toUpperCase().indexOf("UNKNOWN") >= 0) {
            timeStr = RNCryptoEncDec.decryptAccessToken(token);
        } else if (family.toUpperCase().indexOf("ANDROID") >= 0) {
            timeStr = EncDecUtil.decryptAccessToken(token, MessageBundle.getSecretKey());
        }
        Long timeVal = Long.valueOf(timeStr).longValue();
        Long now = System.currentTimeMillis();
        log.info("+++++++ previous time " + timeStr + " Current Time " + now + " +++++++++");
        Long diff = (now - timeVal) / 1000;
        log.info("+++++++ Time Diff " + diff);
        if (diff > 240) //if diff more than 2 minutes
            throw  new YSException("SEC002", "total time is " + diff);

    }

    //Activate store
    public Boolean isVerifiableStore(StoreEntity storeEntity, Boolean throwError) {
        boolean hasStoresArea = false, hasRatePlan = false, hasKnownRatePlan = false, hasUnknownRatePlan = false, hasStoreManager = false;
        if (storeEntity.getStoresAreas().size() > 0) {
            for (StoresAreaEntity storesArea : storeEntity.getStoresAreas()) {
                if (storesArea.getArea().getStatus().equals(Status.ACTIVE)) {
                    hasStoresArea = true;
                    break;
                }
            }
        }
        if (!hasStoresArea && throwError)
            throw new YSException("STRAR001");
        //if (storeEntity.getIsDistanceBaseRatePlan() != null && !storeEntity.getIsDistanceBaseRatePlan()) {
        //area based case
        if(!storeEntity.getIsDistanceBaseRatePlan()){
            if (storeEntity.getRatePlans().size() > 0) {
                for (RatePlanEntity ratePlan : storeEntity.getRatePlans()) {
                    if (ratePlan.getStatus().equals(Status.ACTIVE)) {
                        hasRatePlan = true;
                        break;
                    }
                }
            }
            if (!hasRatePlan && throwError)
                throw new YSException("RPN001");
        } else {
            //distance based case
            if (storeEntity.getUnknownAreaRatePlans().size() > 0) {
                for (UnknownAreaRatePlanEntity unknownAreaRatePlan : storeEntity.getUnknownAreaRatePlans()) {
                    if (unknownAreaRatePlan.getStatus().equals(Status.ACTIVE) && (unknownAreaRatePlan.getIsKnownArea() != null && unknownAreaRatePlan.getIsKnownArea())) {
                        hasKnownRatePlan = true;
                        break;
                    }
                }
            }
            if (!hasKnownRatePlan && throwError)
                throw new YSException("RPN002");
        }
        //}
        if(storeEntity.getAllowUnknownAreaServing()!=null && storeEntity.getAllowUnknownAreaServing()) {
            if (storeEntity.getUnknownAreaRatePlans().size() > 0) {
                for (UnknownAreaRatePlanEntity unknownAreaRatePlan : storeEntity.getUnknownAreaRatePlans()) {
                    if (unknownAreaRatePlan.getStatus().equals(Status.ACTIVE) && (unknownAreaRatePlan.getIsKnownArea() == null || !unknownAreaRatePlan.getIsKnownArea())) {
                        hasUnknownRatePlan = true;
                        break;
                    }
                }
            }
            if (!hasUnknownRatePlan && throwError)
                throw new YSException("UNRPN001");
        }else{
            hasUnknownRatePlan = true;
        }
        if (storeEntity.getStoreManagers().size() > 0) {
            for (StoreManagerEntity storeManager : storeEntity.getStoreManagers()) {
                if (storeManager.getUser().getUserStatus().equals(UserStatus.ACTIVE)) {
                    hasStoreManager = true;
                    break;
                }
            }
            if (!hasStoreManager && throwError)
                throw new YSException("STRMGR004");
        } else {
            if (!hasStoreManager && throwError)
                throw new YSException("STRMGR001");
        }
        log.info("the verifiable status of the store is: "+(hasStoresArea && (hasRatePlan || hasKnownRatePlan) && hasUnknownRatePlan && hasStoreManager));
        return (hasStoresArea && (hasRatePlan || hasKnownRatePlan) && hasUnknownRatePlan && hasStoreManager);
    }
}
