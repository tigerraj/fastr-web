package com.yetistep.anyorder.abs;

import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: yetistep
 * Date: 1/9/14
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ThreadPoolManager {
    private static final Logger log = Logger.getLogger(ThreadPoolManager.class);
    private static ExecutorService pool;

    public static void init() {
        if (pool == null)
            pool = Executors.newFixedThreadPool(5);//fixed pool size 5

    }

    public static void runAsynchJob(Runnable job) {
        init();
        pool.execute(job);
    }

}
