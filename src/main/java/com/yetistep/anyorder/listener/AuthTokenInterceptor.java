package com.yetistep.anyorder.listener;

/**
 * Created by suroz on 1/2/2017.
 */

import com.yetistep.anyorder.util.YSException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.*;
import java.io.IOException;

public class AuthTokenInterceptor implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent event) {
        System.out.println("Session Created");
    }

    public void sessionDestroyed(HttpSessionEvent event) {
        //write your logic
        System.out.println("Session Destroyed");
        throw new YSException("USR001");
    }
}