package com.yetistep.anyorder.schedular;

import com.yetistep.anyorder.dao.inf.DriverDao;
import com.yetistep.anyorder.dao.inf.StoreDao;
import com.yetistep.anyorder.enums.AvailabilityStatus;
import com.yetistep.anyorder.enums.PreferenceType;
import com.yetistep.anyorder.enums.StatementGenerationType;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.DriversWorkLogEntity;
import com.yetistep.anyorder.model.StoreEntity;
import com.yetistep.anyorder.service.inf.ManagerService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.DateUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 12/12/14
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Component
public class TaskSchedule {
    private static final Logger log = Logger.getLogger(TaskSchedule.class);

    private final String CRON_WEEKLY = "0 0 0 * * 0"; //every sunday
    private final String CRON_FORTNIGTLY = "0 0 0 14 * *"; //every 14th day
    private final String CRON_MONTHLY = "0 0 0 1 * *"; //1st of every month at 00:00
    private final String CRON_DAILY = "0 10 16 * * *"; //Daily at 16:00
    private final String CRON_DRIVER_UPDATE = "0 0 * * * *";//second minute hour dayOfMonth month year (every hour)


    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    ManagerService managerService;

    @Autowired
    StoreDao storeDao;

    @Autowired
    DriverDao driverDao;

/*    @Scheduled(cron = CRON_WEEKLY) //minute hour dayOfMonth month dayOfWeek year
    @Transactional
    public void generateWeeklyInvoice() {
        try {
            log.info("Generating weekly invoice:");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Calendar calPrev = Calendar.getInstance();
            calPrev.add(Calendar.WEEK_OF_MONTH, -1);
            List<StoreEntity> stores = storeDao.findActiveStoreByStatementGenerationType(StatementGenerationType.WEEKLY);
            statementGeneration(stores, calPrev, cal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Scheduled(cron = CRON_FORTNIGTLY) //minute hour dayOfMonth month dayOfWeek year
    @Transactional
    public void generateFortnightlyInvoice() {
        try {
            log.info("Generating fortnightly invoice:");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Calendar calPrev = Calendar.getInstance();
            calPrev.add(Calendar.WEEK_OF_MONTH, -2);
            List<StoreEntity> stores = storeDao.findActiveStoreByStatementGenerationType(StatementGenerationType.FORTNIGHTLY);
            statementGeneration(stores, calPrev, cal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }*/

    @Scheduled(cron = CRON_MONTHLY) //minute hour dayOfMonth month dayOfWeek year
    @Transactional
    public void generateMonthlyInvoice() {
        try {
            log.info("Generating monthly invoice:");

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Calendar calPrev = Calendar.getInstance();
            calPrev.add(Calendar.MONTH, -1);
            List<StoreEntity> stores = storeDao.findActiveStoreByStatementGenerationType(StatementGenerationType.MONTHLY);
            List<StoreEntity> eligibleForStatement = new ArrayList<>();
            for (StoreEntity storeEntity : stores) {
                if (((storeEntity.getStatus().equals(Status.ACTIVE) && storeEntity.getIsVerified() == Boolean.TRUE)
                        && storeEntity.getRatePlans() != null && storeEntity.getRatePlans().size() > 0)
                        || ((storeEntity.getStatus().equals(Status.ACTIVE) && storeEntity.getIsVerified() == Boolean.TRUE)
                        && storeEntity.getIsDistanceBaseRatePlan() != null && storeEntity.getIsDistanceBaseRatePlan())
                        || (storeEntity.getStatus().equals(Status.INACTIVE) && storeEntity.getOrders() != null && storeEntity.getOrders().size() > 0)) {
                    eligibleForStatement.add(storeEntity);
                }
            }
            statementGeneration(eligibleForStatement, calPrev, cal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void statementGeneration(List<StoreEntity> stores, Calendar calPrev, Calendar cal) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info("Count Store: " + stores.size());
        if (stores.size() > 0) {
            for (StoreEntity store : stores) {
                log.info("Generating Statements for storeId: " + store.getStoreId());
                managerService.generateStatement(store, dateFormat.format(calPrev.getTime()), dateFormat.format(cal.getTime()));
            }
        } else {
            log.info("No stores found.");
        }
    }

    /*@Scheduled(cron = CRON_DAILY) //minute hour dayOfMonth month dayOfWeek year
    @Transactional
    public void testInvoiceGenerator() {
        try {
            log.info("Generating daily invoice:");

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Calendar calPrev = Calendar.getInstance();
            calPrev.add(Calendar.DAY_OF_MONTH, -1);
            List<StoreEntity> stores = storeDao.findActiveStoreByStatementGenerationType(StatementGenerationType.MONTHLY);
            List<StoreEntity> eligibleForStatement = new ArrayList<>();
            for (StoreEntity storeEntity : stores) {
                if (((storeEntity.getStatus().equals(Status.ACTIVE) && storeEntity.getIsVerified() == Boolean.TRUE)
                        && storeEntity.getRatePlans() != null && storeEntity.getRatePlans().size() > 0)
                        || ((storeEntity.getStatus().equals(Status.ACTIVE) && storeEntity.getIsVerified() == Boolean.TRUE)
                        && storeEntity.getIsDistanceBaseRatePlan() != null && storeEntity.getIsDistanceBaseRatePlan())
                        || (storeEntity.getStatus().equals(Status.INACTIVE) && storeEntity.getOrders() != null && storeEntity.getOrders().size() > 0)) {
                    eligibleForStatement.add(storeEntity);
                }
            }
            statementGeneration(eligibleForStatement, calPrev, cal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }*/

    @Scheduled(cron = CRON_DRIVER_UPDATE) //minute hour dayOfMonth month dayOfWeek year
    @Transactional
    public void updateDriverAvailability() {
        try {
            log.info("Updating driver availability status.");
            Long driverLocationUpdateTimeInMin = Long.parseLong(systemPropertyService.readPrefValue(PreferenceType.DRIVER_OFF_DUTY_STATUS_UPDATE_TIME).trim());
            List<DriverEntity> driverEntities = driverDao.findAvailableDriver();
            for (DriverEntity driverEntity : driverEntities) {
                Date lastUpdateLocation = driverEntity.getLastLocationUpdate();
                Double elapsedTimeSecond = DateUtil.getMinDiff(DateUtil.getCurrentDate().getTime(), lastUpdateLocation.getTime());
                if (driverLocationUpdateTimeInMin < elapsedTimeSecond) {
                    driverEntity.setAvailabilityStatus(AvailabilityStatus.UNAVAILABLE);
                    List<DriversWorkLogEntity> driversWorkLogEntities = driverEntity.getWorkLogs();
                    for (DriversWorkLogEntity driversWorkLogEntity : driversWorkLogEntities) {
                        if (driversWorkLogEntity.getToDateTime() == null) {
                            driversWorkLogEntity.setToDateTime(DateUtil.getCurrentDate());
                            break;
                        }
                    }
                }
                driverDao.update(driverEntity);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
