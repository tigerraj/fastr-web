package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.yetistep.anyorder.enums.ActionType;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.util.JsonDateSerializer;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity(name = "ActionLogEntity")
@Table(name = "action_log")
@XmlTransient
public class ActionLogEntity implements Serializable {

    private static final long serialVersionUID = 5288685306703826329L;

    private Long id;
    private String actionLogId;
    private UserRole userRole;
    private ActionType actionType;
    private String description;
    private String userIP;
    private Timestamp createdDate;

    private UserEntity user;

    public ActionLogEntity(UserRole userRole, UserEntity user, ActionType actionType, String description, String userIP) {
        this.userRole = userRole;
        this.user = user;
        this.actionType = actionType;
        this.description = description;
        this.userIP = userIP;
    }

    public ActionLogEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "action_log_id")
    public String getActionLogId() {
        return actionLogId;
    }

    public void setActionLogId(String actionLogId) {
        this.actionLogId = actionLogId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "user_id")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Column(name = "action")
    @Enumerated(EnumType.STRING)
    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    @Column(name = "description", columnDefinition = "longtext")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "user_ip")
    public String getUserIP() {
        return userIP;
    }

    public void setUserIP(String userIP) {
        this.userIP = userIP;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "created_date", columnDefinition="TIMESTAMP NULL DEFAULT NULL")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
