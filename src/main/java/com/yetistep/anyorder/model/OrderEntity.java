package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.util.DateConverterUtil;
import com.yetistep.anyorder.util.JsonDateDeserializer;
import com.yetistep.anyorder.util.JsonDateSerializer;
import com.yetistep.anyorder.util.TimeConverterUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "orders")
@XStreamAlias("order")
public class OrderEntity implements Serializable {

    private static final long serialVersionUID = -5495190594958857103L;

    private Integer id;
    private String orderId;
    private String orderDisplayId;
    private String orderDescription;
    private DriverType driverType;
    private OrderStatus orderStatus;
    private PaymentMode paymentMode;
    private CustomerAreaType customersAreaType;
    private OrderOriginType orderOriginType;
    private BigDecimal deliveryCharge;
    private BigDecimal totalBillAmount;
    private BigDecimal grandTotal;
    @XStreamConverter(TimeConverterUtil.class)
    private Timestamp createdDate;
    private Timestamp deliveredDate;
    private Timestamp pickupDate;
    private Timestamp dispatchDate;
    private BigDecimal estimatedDeliveryTime;
    private Integer prioritized;
    private Integer orderPriority;   //live orders should have priority and will be served based on priority
    private String commentOnDriver;
    private BigDecimal ratingForDriver;
    private String commentOnFood;
    private BigDecimal ratingOnFood;
    @XStreamConverter(DateConverterUtil.class)
    private Date orderDeliveryDate;
    private String customersNoteToDriver;
    private Boolean assignDriver;
    private String customerSignImage;
    private String receiverName;
    private BigDecimal estimatedDeliveryCharge;  //Transient
    private BigDecimal distanceInKm;             //Transient
    private Timestamp estimatedTimeOfDelivery;        //Transient
    private Integer elapsedTimeInMin;  //Transient
    private OrderPlan orderPlan;
    private BigDecimal distanceTravelInKM;
    private DistanceType distanceType;

    private StoreEntity store;
    private List<OrdersDriverEntity> ordersDrivers;
    private CustomerEntity customer;
    private CustomersAreaEntity customersArea;
    private StatementEntity statement;
    private DriverEntity driver;
    private StoreManagerEntity storeManager;
    private UserEntity csr;
    private OrderRatingEntity orderRating;
    private OrderCancelEntity orderCancel;
    private APIKeyEntity apiKey;

    private String customerName;
    private String mobileNumber;
    private String email;
    private String areaId;
    private String customerLatitude;
    private String customerLongitude;
    private String streetAddress;
    private String orderNote;
    private BigDecimal totalAmount;
    @XStreamConverter(DateConverterUtil.class)
    private Date orderTime;

    public OrderEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "order_id")
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Column(name = "order_display_id")
    public String getOrderDisplayId() {
        return orderDisplayId;
    }

    public void setOrderDisplayId(String orderDisplayId) {
        this.orderDisplayId = orderDisplayId;
    }

    @Column(name = "order_description")
    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }

    @Column(name = "driver_type")
    public DriverType getDriverType() {
        return driverType;
    }

    public void setDriverType(DriverType driverType) {
        this.driverType = driverType;
    }

    @Column(name = "order_status")
    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Column(name = "payment_mode")
    public PaymentMode getPaymentMode() {
        return paymentMode == null ? PaymentMode.COD : paymentMode;
    }

    public void setPaymentMode(PaymentMode paymentMode) {
        this.paymentMode = paymentMode;
    }

    @Column(name = "customers_area_type")
    public CustomerAreaType getCustomersAreaType() {
        return customersAreaType;
    }

    public void setCustomersAreaType(CustomerAreaType customersAreaType) {
        this.customersAreaType = customersAreaType;
    }

    @Column(name = "order_origin_type")
    public OrderOriginType getOrderOriginType() {
        return orderOriginType;
    }

    public void setOrderOriginType(OrderOriginType orderOriginType) {
        this.orderOriginType = orderOriginType;
    }

    @Column(name = "delivery_charge")
    public BigDecimal getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(BigDecimal deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    @Column(name = "total_bill_amount")
    public BigDecimal getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(BigDecimal totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    @Column(name = "grand_total")
    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    @Column(name = "created_date", columnDefinition="TIMESTAMP NULL DEFAULT NULL")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "delivered_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(Timestamp deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    @Column(name = "pickup_date")
    public Timestamp getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Timestamp pickupDate) {
        this.pickupDate = pickupDate;
    }

    @Column(name = "delivery_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public Date getOrderDeliveryDate() {
        return orderDeliveryDate;
    }

    public void setOrderDeliveryDate(Date orderDeliveryDate) {
        this.orderDeliveryDate = orderDeliveryDate;
    }

    @Column(name = "customers_note_to_driver")
    public String getCustomersNoteToDriver() {
        return customersNoteToDriver;
    }

    public void setCustomersNoteToDriver(String customersNoteToDriver) {
        this.customersNoteToDriver = customersNoteToDriver;
    }

    @Column(name = "dispatch_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(Timestamp dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    @Column(name = "orderPriority")
    public Integer getOrderPriority() {
        return orderPriority;
    }

    public void setOrderPriority(Integer orderPriority) {
        this.orderPriority = orderPriority;
    }

    @Column(name = "prioritized")
    public Integer getPrioritized() {
        return prioritized;
    }

    public void setPrioritized(Integer prioritized) {
        this.prioritized = prioritized;
    }

    @Column(name = "etd", columnDefinition="Decimal(10,2) default '0'")
    public BigDecimal getEstimatedDeliveryTime() {
        return estimatedDeliveryTime==null?BigDecimal.ZERO:estimatedDeliveryTime;
    }

    public void setEstimatedDeliveryTime(BigDecimal estimatedDeliveryTime) {
        this.estimatedDeliveryTime = estimatedDeliveryTime;
    }

    @Column(name = "comment_on_driver")
    public String getCommentOnDriver() {
        return commentOnDriver;
    }

    public void setCommentOnDriver(String commentOnDriver) {
        this.commentOnDriver = commentOnDriver;
    }

    @Column(name = "rating_for_driver")
    public BigDecimal getRatingForDriver() {
        return ratingForDriver;
    }

    public void setRatingForDriver(BigDecimal ratingForDriver) {
        this.ratingForDriver = ratingForDriver;
    }

    @Column(name = "comment_on_food")
    public String getCommentOnFood() {
        return commentOnFood;
    }

    public void setCommentOnFood(String commentOnFood) {
        this.commentOnFood = commentOnFood;
    }

    @Column(name = "rating_on_food")
    public BigDecimal getRatingOnFood() {
        return ratingOnFood;
    }

    public void setRatingOnFood(BigDecimal ratingOnFood) {
        this.ratingOnFood = ratingOnFood;
    }

    @Column(name = "assign_driver")
    public Boolean getAssignDriver() {
        return assignDriver;
    }

    public void setAssignDriver(Boolean assignDriver) {
        this.assignDriver = assignDriver;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    @OneToMany(mappedBy = "order", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<OrdersDriverEntity> getOrdersDrivers() {
        return ordersDrivers;
    }

    public void setOrdersDrivers(List<OrdersDriverEntity> ordersDrivers) {
        this.ordersDrivers = ordersDrivers;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public CustomerEntity getCustomer() {
        return customer;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customers_area_id")
    @JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
    public CustomersAreaEntity getCustomersArea() {
        return customersArea;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public void setCustomersArea(CustomersAreaEntity customersArea) {
        this.customersArea = customersArea;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StatementEntity getStatement() {
        return statement;
    }

    public void setStatement(StatementEntity statement) {
        this.statement = statement;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_manager_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreManagerEntity getStoreManager() {
        return storeManager;
    }

    public void setStoreManager(StoreManagerEntity storeManager) {
        this.storeManager = storeManager;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public UserEntity getCsr() {
        return csr;
    }

    public void setCsr(UserEntity csr) {
        this.csr = csr;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "driver_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    @Column(name = "signature_image")
    public String getCustomerSignImage() {
        return customerSignImage;
    }

    public void setCustomerSignImage(String customerSignImage) {
        this.customerSignImage = customerSignImage;
    }

    @Column(name = "receiver_name")
    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    @OneToOne(mappedBy = "order", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    public OrderRatingEntity getOrderRating() {
        return orderRating;
    }

    public void setOrderRating(OrderRatingEntity orderRating) {
        this.orderRating = orderRating;
    }

    @OneToOne(mappedBy = "order", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    public OrderCancelEntity getOrderCancel() {
        return orderCancel;
    }

    public void setOrderCancel(OrderCancelEntity orderCancel) {
        this.orderCancel = orderCancel;
    }

    @Transient
    public BigDecimal getEstimatedDeliveryCharge() {
        return estimatedDeliveryCharge;
    }

    public void setEstimatedDeliveryCharge(BigDecimal estimatedDeliveryCharge) {
        this.estimatedDeliveryCharge = estimatedDeliveryCharge;
    }

    @Transient
    public BigDecimal getDistanceInKm() {
        return distanceInKm;
    }

    public void setDistanceInKm(BigDecimal distanceInKm) {
        this.distanceInKm = distanceInKm;
    }

    @Transient
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getEstimatedTimeOfDelivery() {
        return estimatedTimeOfDelivery;
    }

    public void setEstimatedTimeOfDelivery(Timestamp estimatedTimeOfDelivery) {
        this.estimatedTimeOfDelivery = estimatedTimeOfDelivery;
    }

    @Transient
    public Integer getElapsedTimeInMin() {
        return elapsedTimeInMin;
    }

    public void setElapsedTimeInMin(Integer elapsedTimeInMin) {
        this.elapsedTimeInMin = elapsedTimeInMin;
    }

    public OrderPlan getOrderPlan() {
        return orderPlan;
    }

    public void setOrderPlan(OrderPlan orderPlan) {
        this.orderPlan = orderPlan;
    }

    @Column(name ="distance_travel_in_km")
    public BigDecimal getDistanceTravelInKM() {
        return distanceTravelInKM;
    }

    public void setDistanceTravelInKM(BigDecimal distanceTravelInKM) {
        this.distanceTravelInKM = distanceTravelInKM;
    }

    @Column(name = "distance_type")
    public DistanceType getDistanceType() {
        return distanceType;
    }

    public void setDistanceType(DistanceType distanceType) {
        this.distanceType = distanceType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_key_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public APIKeyEntity getApiKey() {
        return apiKey;
    }

    public void setApiKey(APIKeyEntity apiKey) {
        this.apiKey = apiKey;
    }

    @Transient
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Transient
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Transient
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Transient
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Transient
    public String getCustomerLatitude() {
        return customerLatitude;
    }

    public void setCustomerLatitude(String customerLatitude) {
        this.customerLatitude = customerLatitude;
    }

    @Transient
    public String getCustomerLongitude() {
        return customerLongitude;
    }

    public void setCustomerLongitude(String customerLongitude) {
        this.customerLongitude = customerLongitude;
    }

    @Transient
    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    @Transient
    public String getOrderNote() {
        return orderNote;
    }

    public void setOrderNote(String orderNote) {
        this.orderNote = orderNote;
    }

    @Transient
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Transient
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

}
