package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yetistep.anyorder.enums.OrdersDriverStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "orders_drivers")
public class OrdersDriverEntity implements Serializable {

    private static final long serialVersionUID = -8991717498656314120L;

    private Integer id;
    private String ordersDriverId;
    private OrdersDriverStatus driverStatus;
    private Date assignedDate;

    private DriverEntity driver;
    private OrderEntity order;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "orders_driver_id")
    public String getOrdersDriverId() {
        return ordersDriverId;
    }

    public void setOrdersDriverId(String ordersDriverId) {
        this.ordersDriverId = ordersDriverId;
    }

    @Column(name = "driver_status")
    public OrdersDriverStatus getDriverStatus() {
        return driverStatus;
    }

    public void setDriverStatus(OrdersDriverStatus driverStatus) {
        this.driverStatus = driverStatus;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    @Column(name = "assigned_date")
    public Date getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
    }
}
