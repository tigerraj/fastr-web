package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.yetistep.anyorder.enums.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:27 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "areas")
@XStreamAlias("area")
public class AreaEntity implements Serializable {

    private static final long serialVersionUID = 6558549818523449495L;

    private Integer id;
    private String areaId;
    private String areaName;
    private String latitude;
    private String longitude;
    private String street;
    private Status status;

    private AreaEntity parent;
    private List<AreaEntity> child;
    private List<CustomersAreaEntity> customersArea;
    private List<DriversAreaEntity> driversArea;
    private List<StoresAreaEntity> storesArea;
    private List<StoreEntity> stores;

    //Transient
    private Boolean selected;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "area_id")
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Column(name = "area_name")
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public AreaEntity getParent() {
        return parent;
    }

    public void setParent(AreaEntity parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public List<AreaEntity> getChild() {
        return child;
    }

    public void setChild(List<AreaEntity> child) {
        this.child = child;
    }

    @OneToMany(mappedBy = "area", cascade = {CascadeType.PERSIST})
    public List<CustomersAreaEntity> getCustomersArea() {
        return customersArea;
    }

    public void setCustomersArea(List<CustomersAreaEntity> customersArea) {
        this.customersArea = customersArea;
    }

    @OneToMany(mappedBy = "area", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<DriversAreaEntity> getDriversArea() {
        return driversArea;
    }

    public void setDriversArea(List<DriversAreaEntity> driversArea) {
        this.driversArea = driversArea;
    }

    @OneToMany(mappedBy = "area", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<StoresAreaEntity> getStoresArea() {
        return storesArea;
    }

    public void setStoresArea(List<StoresAreaEntity> storesArea) {
        this.storesArea = storesArea;
    }

    @OneToMany(mappedBy = "area", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<StoreEntity> getStores() {
        return stores;
    }

    public void setStores(List<StoreEntity> stores) {
        this.stores = stores;
    }

    @Transient
    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    @Column(name="status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {

        return this.areaId.equalsIgnoreCase(((AreaEntity)obj).getAreaId());
    }
}
