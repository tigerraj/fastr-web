package com.yetistep.anyorder.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 2/11/16
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "order_ratings")
public class OrderRatingEntity implements Serializable {

    private static final long serialVersionUID = -2770593563394438271L;

    private Integer id;
    private String orderRatingId;
    private Integer ratingByCustomer;
    private String commentByCustomer;

    private OrderEntity order;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "order_rating_id")
    public String getOrderRatingId() {
        return orderRatingId;
    }

    public void setOrderRatingId(String orderRatingId) {
        this.orderRatingId = orderRatingId;
    }

    @Column(name = "rating_by_customer")
    public Integer getRatingByCustomer() {
        return ratingByCustomer;
    }

    public void setRatingByCustomer(Integer ratingByCustomer) {
        this.ratingByCustomer = ratingByCustomer;
    }

    @Column(name = "comment_by_customer")
    public String getCommentByCustomer() {
        return commentByCustomer;
    }

    public void setCommentByCustomer(String commentByCustomer) {
        this.commentByCustomer = commentByCustomer;
    }

    @OneToOne(fetch = FetchType.LAZY)
    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }
}
