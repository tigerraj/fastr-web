package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yetistep.anyorder.enums.OrderPlan;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "statements")
public class StatementEntity implements Serializable {

    private static final long serialVersionUID = -178604591734441575L;

    private Integer id;
    private String statementId;
    private Boolean paidStatus;
    private Date fromDate;
    private Date toDate;
    private Timestamp generatedDate;
    private Timestamp paidDate;
    private String statementUrl;
    private BigDecimal statementAmount;
    private BigDecimal subscriptionRate;
    private Integer extraOrderCount;
    private BigDecimal extraOrderCharge;
    private Integer outOfServingAreaOrderCount;
    private BigDecimal outOfServingAreaOrderCharge;
    private BigDecimal totalPayableAmount;
    private String subscriptionRatePlan;
    private Integer distanceBasedOrderCount;
    private BigDecimal distanceBasedOrderAmount;
    private BigDecimal otherCharge;
    private Integer subscriptionRatePlanOrderCount;
    private OrderPlan orderPlan;

    private List<OrderEntity> order;

    private StoreEntity store;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "statement_id")
    public String getStatementId() {
        return statementId;
    }

    public void setStatementId(String statementId) {
        this.statementId = statementId;
    }

    @Column(name = "paid_status")
    public Boolean getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(Boolean paidStatus) {
        this.paidStatus = paidStatus;
    }

    @Column(name = "from_date")
    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Column(name = "to_date")
    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Column(name = "generated_date")
    public Timestamp getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(Timestamp generatedDate) {
        this.generatedDate = generatedDate;
    }

    @Column(name = "paid_date")
    public Timestamp getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Timestamp paidDate) {
        this.paidDate = paidDate;
    }

    @Column(name = "statement_url")
    public String getStatementUrl() {
        return statementUrl;
    }

    public void setStatementUrl(String statementUrl) {
        this.statementUrl = statementUrl;
    }

    @Column(name = "statement_amount")
    public BigDecimal getStatementAmount() {
        return statementAmount;
    }

    public void setStatementAmount(BigDecimal statementAmount) {
        this.statementAmount = statementAmount;
    }

    @OneToMany(mappedBy = "statement")
    public List<OrderEntity> getOrder() {
        return order;
    }

    public void setOrder(List<OrderEntity> order) {
        this.order = order;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    public BigDecimal getSubscriptionRate() {
        return subscriptionRate;
    }

    public void setSubscriptionRate(BigDecimal subscriptionRate) {
        this.subscriptionRate = subscriptionRate;
    }

    public Integer getExtraOrderCount() {
        return extraOrderCount;
    }

    public void setExtraOrderCount(Integer extraOrderCount) {
        this.extraOrderCount = extraOrderCount;
    }

    public BigDecimal getExtraOrderCharge() {
        return extraOrderCharge;
    }

    public void setExtraOrderCharge(BigDecimal extraOrderCharge) {
        this.extraOrderCharge = extraOrderCharge;
    }

    public Integer getOutOfServingAreaOrderCount() {
        return outOfServingAreaOrderCount;
    }

    public void setOutOfServingAreaOrderCount(Integer outOfServingAreaOrderCount) {
        this.outOfServingAreaOrderCount = outOfServingAreaOrderCount;
    }

    public BigDecimal getOutOfServingAreaOrderCharge() {
        return outOfServingAreaOrderCharge;
    }

    public void setOutOfServingAreaOrderCharge(BigDecimal outOfServingAreaOrderCharge) {
        this.outOfServingAreaOrderCharge = outOfServingAreaOrderCharge;
    }

    public BigDecimal getTotalPayableAmount() {
        return totalPayableAmount;
    }

    public void setTotalPayableAmount(BigDecimal totalPayableAmount) {
        this.totalPayableAmount = totalPayableAmount;
    }

    public String getSubscriptionRatePlan() {
        return subscriptionRatePlan;
    }

    public void setSubscriptionRatePlan(String subscriptionRatePlan) {
        this.subscriptionRatePlan = subscriptionRatePlan;
    }

    public Integer getDistanceBasedOrderCount() {
        return distanceBasedOrderCount;
    }

    public void setDistanceBasedOrderCount(Integer distanceBasedOrderCount) {
        this.distanceBasedOrderCount = distanceBasedOrderCount;
    }

    public BigDecimal getDistanceBasedOrderAmount() {
        return distanceBasedOrderAmount;
    }

    public void setDistanceBasedOrderAmount(BigDecimal distanceBasedOrderAmount) {
        this.distanceBasedOrderAmount = distanceBasedOrderAmount;
    }

    public BigDecimal getOtherCharge() {
        return otherCharge;
    }

    public void setOtherCharge(BigDecimal otherCharge) {
        this.otherCharge = otherCharge;
    }

    public Integer getSubscriptionRatePlanOrderCount() {
        return subscriptionRatePlanOrderCount;
    }

    public void setSubscriptionRatePlanOrderCount(Integer subscriptionRatePlanOrderCount) {
        this.subscriptionRatePlanOrderCount = subscriptionRatePlanOrderCount;
    }

    public OrderPlan getOrderPlan() {
        return orderPlan;
    }

    public void setOrderPlan(OrderPlan orderPlan) {
        this.orderPlan = orderPlan;
    }
}
