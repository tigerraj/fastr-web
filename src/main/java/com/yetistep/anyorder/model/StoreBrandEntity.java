package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yetistep.anyorder.enums.StatementGenerationType;
import com.yetistep.anyorder.enums.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "store_brands")
public class StoreBrandEntity implements Serializable {

    private static final long serialVersionUID = 7174003196101106778L;

    private Integer id;
    private String storeBrandId;
    private String brandName;
    private String brandLogo;
    private StatementGenerationType statementGenerationType;
    private Status status;

    private MerchantEntity merchant;
    private List<StoreEntity> stores;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "store_brand_id")
    public String getStoreBrandId() {
        return storeBrandId;
    }

    public void setStoreBrandId(String storeBrandId) {
        this.storeBrandId = storeBrandId;
    }

    @Column(name = "brand_name")
    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    @Column(name = "brand_logo")
    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    @Column(name = "statement_generation_type")
    public StatementGenerationType getStatementGenerationType() {
        return statementGenerationType;
    }

    public void setStatementGenerationType(StatementGenerationType statementGenerationType) {
        this.statementGenerationType = statementGenerationType;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public MerchantEntity getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantEntity merchant) {
        this.merchant = merchant;
    }

    @OneToMany(mappedBy = "storeBrand", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<StoreEntity> getStores() {
        return stores;
    }

    public void setStores(List<StoreEntity> stores) {
        this.stores = stores;
    }

    @Column(name="status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
