package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:04 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "drivers_work_logs")
public class DriversWorkLogEntity  implements Serializable {

    private static final long serialVersionUID = -33091811830056931L;

    private Integer id;
    private String driversWorkLogId;
    private Date fromDateTime;
    private Date toDateTime;

    private DriverEntity driver;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "drivers_work_log_id")
    public String getDriversWorkLogId() {
        return driversWorkLogId;
    }

    public void setDriversWorkLogId(String driversWorkLogId) {
        this.driversWorkLogId = driversWorkLogId;
    }

    @Column(name = "from_date_time")
    public Date getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(Date fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    @Column(name = "to_date_time")
    public Date getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(Date toDateTime) {
        this.toDateTime = toDateTime;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }
}
