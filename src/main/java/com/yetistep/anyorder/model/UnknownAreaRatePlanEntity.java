package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.util.JsonDateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by Sagar on 6/13/2016.
 */
@Entity
@Table(name="unknown_area_rate_plans")
public class UnknownAreaRatePlanEntity implements Serializable {

    private static final long serialVersionUID = -5556278134400242098L;

    private Integer id;
    private String unknownAreaRatePlanId;
    private String name;
    private Status status;
    private Timestamp createdDate;
    private Timestamp lastModified;
    private Integer startDistance;
    private Integer endDistance;
    private BigDecimal baseCharge;
    private BigDecimal additionalChargePerKM;
    private Boolean isKnownArea;

    private StoreEntity store;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "rate_plan_id")
    public String getUnknownAreaRatePlanId() {
        return unknownAreaRatePlanId;
    }

    public void setUnknownAreaRatePlanId(String unknownAreaRatePlanId) {
        this.unknownAreaRatePlanId = unknownAreaRatePlanId;
    }

    @Column(name = "status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Column(name="created_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name="modified_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getLastModified() {
        return lastModified;
    }

    public void setLastModified(Timestamp lastModified) {
        this.lastModified = lastModified;
    }

    @Column(name = "additional_charge_per_km")
    public BigDecimal getAdditionalChargePerKM() {
        return additionalChargePerKM;
    }

    public void setAdditionalChargePerKM(BigDecimal additionalChargePerKM) {
        this.additionalChargePerKM = additionalChargePerKM;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    @Column(name = "start_distance")
    public Integer getStartDistance() {
        return startDistance;
    }

    public void setStartDistance(Integer startDistance) {
        this.startDistance = startDistance;
    }

    @Column(name="end_distance")
    public Integer getEndDistance() {
        return endDistance;
    }

    public void setEndDistance(Integer endDistance) {
        this.endDistance = endDistance;
    }

    @Column(name="base_charge")
    public BigDecimal getBaseCharge() {
        return baseCharge;
    }

    public void setBaseCharge(BigDecimal baseCharge) {
        this.baseCharge = baseCharge;
    }

    @Column(name="rate_plan_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "is_known_area")
    public Boolean getIsKnownArea() {
        return isKnownArea;
    }

    public void setIsKnownArea(Boolean isKnownArea) {
        this.isKnownArea = isKnownArea;
    }
}
