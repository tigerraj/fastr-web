package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.enums.StorePriority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:12 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "stores")
@XStreamAlias("store")
public class StoreEntity implements Serializable {

    private static final long serialVersionUID = -5614281496140364005L;

    public StoreEntity() {
    }

    private Integer id;
    private String storeId;
    @XStreamAlias("street")
    private String givenLocation1;
    private String givenLocation2;
    private String addressNote;
    private String latitude;
    private String longitude;
    private Status status;
    private Boolean isVerified;
    private String contactNo;
    private StorePriority priority;
    private Boolean allowStoresDriver;
    private Boolean allowUnknownAreaServing;
    private Boolean canActivate;    //Transient
    private Boolean isDistanceBaseRatePlan;
    private String storeName;  //Transient

    private StoreBrandEntity storeBrand;
    private List<StoreManagerEntity> storeManagers;
    private List<RatePlanEntity> ratePlans;
    private List<DriverEntity> drivers;
    private AreaEntity area;
    private List<StoresAreaEntity> storesAreas;
    private List<StoresNotDriverEntity> storesNotDrivers;
    private List<OrderEntity> orders;
    private List<StatementEntity> statements;
    private List<UnknownAreaRatePlanEntity> unknownAreaRatePlans;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "store_id")
    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    @Column(name = "given_location1")
    public String getGivenLocation1() {
        return givenLocation1;
    }

    public void setGivenLocation1(String givenLocation1) {
        this.givenLocation1 = givenLocation1;
    }

    @Column(name = "given_location2")
    public String getGivenLocation2() {
        return givenLocation2;
    }

    public void setGivenLocation2(String givenLocation2) {
        this.givenLocation2 = givenLocation2;
    }

    @Column(name = "address_note")
    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name="contact_no")
    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    @Column(name = "priority")
    public StorePriority getPriority() {
        return priority;
    }

    public void setPriority(StorePriority priority) {
        this.priority = priority;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "store_brand_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreBrandEntity getStoreBrand() {
        return storeBrand;
    }

    public void setStoreBrand(StoreBrandEntity storeBrand) {
        this.storeBrand = storeBrand;
    }

    @OneToMany(mappedBy = "store")
    public List<StoreManagerEntity> getStoreManagers() {
        return storeManagers;
    }

    public void setStoreManagers(List<StoreManagerEntity> storeManagers) {
        this.storeManagers = storeManagers;
    }

    @OneToMany(mappedBy = "store")
    public List<RatePlanEntity> getRatePlans() {
        return ratePlans;
    }

    public void setRatePlans(List<RatePlanEntity> ratePlans) {
        this.ratePlans = ratePlans;
    }

    @OneToMany(mappedBy = "store")
    public List<DriverEntity> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<DriverEntity> drivers) {
        this.drivers = drivers;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public AreaEntity getArea() {
        return area;
    }

    public void setArea(AreaEntity area) {
        this.area = area;
    }

    @OneToMany(mappedBy = "store", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<StoresAreaEntity> getStoresAreas() {
        return storesAreas;
    }

    public void setStoresAreas(List<StoresAreaEntity> storesAreas) {
        this.storesAreas = storesAreas;
    }

    @OneToMany(mappedBy = "store", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<StoresNotDriverEntity> getStoresNotDrivers() {
        return storesNotDrivers;
    }

    public void setStoresNotDrivers(List<StoresNotDriverEntity> storesNotDrivers) {
        this.storesNotDrivers = storesNotDrivers;
    }

    @OneToMany(mappedBy = "store")
    public List<OrderEntity> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderEntity> orders) {
        this.orders = orders;
    }

    @OneToMany(mappedBy = "store")
    public List<StatementEntity> getStatements() {
        return statements;
    }

    public void setStatements(List<StatementEntity> statements) {
        this.statements = statements;
    }

    @Column(name="status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @OneToMany(mappedBy = "store")
    public List<UnknownAreaRatePlanEntity> getUnknownAreaRatePlans() {
        return unknownAreaRatePlans;
    }

    public void setUnknownAreaRatePlans(List<UnknownAreaRatePlanEntity> unknownAreaRatePlans) {
        this.unknownAreaRatePlans = unknownAreaRatePlans;
    }

    @Column(name="is_verified", columnDefinition = "boolean default false")
    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    @Transient
    public Boolean getCanActivate() {
        return canActivate;
    }

    public void setCanActivate(Boolean canActivate) {
        this.canActivate = canActivate;
    }

    @Column(name = "allow_stores_driver")
    public Boolean getAllowStoresDriver() {
        return allowStoresDriver;
    }

    public void setAllowStoresDriver(Boolean allowStoresDriver) {
        this.allowStoresDriver = allowStoresDriver;
    }

    @Column(name = "allow_unknown_area_serving")
    public Boolean getAllowUnknownAreaServing() {
        return allowUnknownAreaServing;
    }

    public void setAllowUnknownAreaServing(Boolean allowUnknownAreaServing) {
        this.allowUnknownAreaServing = allowUnknownAreaServing;
    }

    @Column(name = "is_distance_base_rate_plan")
    public Boolean getIsDistanceBaseRatePlan() {
        return isDistanceBaseRatePlan;
    }

    public void setIsDistanceBaseRatePlan(Boolean isDistanceBaseRatePlan) {
        this.isDistanceBaseRatePlan = isDistanceBaseRatePlan;
    }

    @Transient
    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
}
