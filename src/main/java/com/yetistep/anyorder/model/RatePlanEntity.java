package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yetistep.anyorder.enums.DriverType;
import com.yetistep.anyorder.enums.DurationType;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.util.JsonDateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 4:47 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "rate_plans")
public class RatePlanEntity implements Serializable {

    private static final long serialVersionUID = 8055954773218114362L;

    private Integer id;
    private String ratePlanId;
    private String name;
    private DriverType driverType;
    private Status status;
    private Timestamp createdDate;
    private Timestamp lastModified;
    private Integer orderCount;
    private BigDecimal amount;
    private BigDecimal additionalChargePerOrder;
    private DurationType durationType;

    private StoreEntity store;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "rate_plan_id")
    public String getRatePlanId() {
        return ratePlanId;
    }

    public void setRatePlanId(String ratePlanId) {
        this.ratePlanId = ratePlanId;
    }

    @Column(name = "driver_type")
    public DriverType getDriverType() {
        return driverType;
    }

    public void setDriverType(DriverType driverType) {
        this.driverType = driverType;
    }

    @Column(name = "status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Column(name = "created_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "last_modified")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getLastModified() {
        return lastModified;
    }

    public void setLastModified(Timestamp lastModified) {
        this.lastModified = lastModified;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    @Column(name="order_count")
    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    @Column(name ="amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name="additional_charge_per_order")
    public BigDecimal getAdditionalChargePerOrder() {
        return additionalChargePerOrder;
    }

    public void setAdditionalChargePerOrder(BigDecimal additionalChargePerOrder) {
        this.additionalChargePerOrder = additionalChargePerOrder;
    }

    @Column(name="duration_type")
    public DurationType getDuration() {
        return durationType;
    }

    public void setDuration(DurationType durationType) {
        this.durationType = durationType;
    }

    @Column(name="rate_plan_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
