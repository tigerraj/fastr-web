package com.yetistep.anyorder.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.yetistep.anyorder.enums.OrderDisplayStatus;
import com.yetistep.anyorder.model.AreaEntity;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.StoreEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by suroz on 1/16/2017.
 */
@XStreamAlias("root")
public class APIData implements Serializable {
    private String storeId;
    @XStreamAlias("APIKey")
    private String apiKey;
    private String orderId;
    private String errorMessage;
    private String message;
    private String errorCode;
    private String latitude;
    private String longitude;
    private BigDecimal estimateCharge;
    @XStreamAlias("status")
    private OrderDisplayStatus orderDisplayStatus;
    @XStreamAlias("order")
    private OrderEntity order;
    @XStreamAlias("orders")
    private List<OrderEntity> orders;
    @XStreamAlias("areas")
    private List<AreaEntity> areas;
    @XStreamAlias("stores")
    private List<StoreEntity> stores;


    public List<AreaEntity> getAreas() {
        return areas;
    }

    public void setAreas(List<AreaEntity> areas) {
        this.areas = areas;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public List<StoreEntity> getStores() {
        return stores;
    }

    public void setStores(List<StoreEntity> stores) {
        this.stores = stores;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public List<OrderEntity> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderEntity> orders) {
        this.orders = orders;
    }

    public OrderDisplayStatus getOrderDisplayStatus() {
        return orderDisplayStatus;
    }

    public void setOrderDisplayStatus(OrderDisplayStatus orderDisplayStatus) {
        this.orderDisplayStatus = orderDisplayStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getEstimateCharge() {
        return estimateCharge;
    }

    public void setEstimateCharge(BigDecimal estimateCharge) {
        this.estimateCharge = estimateCharge;
    }
}

