package com.yetistep.anyorder.model;

import com.yetistep.anyorder.enums.Status;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "drivers_areas")
public class DriversAreaEntity implements Serializable {

    private static final long serialVersionUID = -219314403809417456L;

    private Integer id;
    private String driversAreaId;

    private DriverEntity driver;
    private AreaEntity area;
    private Status status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "drivers_area_id")
    public String getDriversAreaId() {
        return driversAreaId;
    }

    public void setDriversAreaId(String driversAreaId) {
        this.driversAreaId = driversAreaId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public AreaEntity getArea() {
        return area;
    }

    public void setArea(AreaEntity area) {
        this.area = area;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
