package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "merchants")
public class MerchantEntity implements Serializable {

    private static final long serialVersionUID = 446221923803616597L;

    private Integer id;
    private String merchantId;
    private String businessTitle;  //should be unique
    private String checkoutCardId;
    private String cardLast4;
    private String cardType;

    private UserEntity user;
    private List<StoreBrandEntity> storeBrand;
    private List<APIKeyEntity> apiKeys;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "merchant_id")
    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Column(name = "business_title")
    public String getBusinessTitle() {
        return businessTitle;
    }

    public void setBusinessTitle(String businessTitle) {
        this.businessTitle = businessTitle;
    }

    @Column(name = "checkout_card_id")
    public String getCheckoutCardId() {
        return checkoutCardId;
    }

    public void setCheckoutCardId(String checkoutCardId) {
        this.checkoutCardId = checkoutCardId;
    }

    @Column(name = "card_last_4")
    public String getCardLast4() {
        return cardLast4;
    }

    public void setCardLast4(String cardLast4) {
        this.cardLast4 = cardLast4;
    }

    @Column(name = "card_type")
    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @OneToMany(mappedBy = "merchant")
    public List<StoreBrandEntity> getStoreBrand() {
        return storeBrand;
    }

    public void setStoreBrand(List<StoreBrandEntity> storeBrand) {
        this.storeBrand = storeBrand;
    }

    @OneToMany(mappedBy = "merchant")
    public List<APIKeyEntity> getApiKeys() {
        return apiKeys;
    }

    public void setApiKeys(List<APIKeyEntity> apiKeys) {
        this.apiKeys = apiKeys;
    }
}
