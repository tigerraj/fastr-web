package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yetistep.anyorder.enums.NotifyTo;
import com.yetistep.anyorder.util.JsonDateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 10:29 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "notifications")
public class NotificationEntity  implements Serializable {

    private static final long serialVersionUID = 1011346717170591087L;

    private Integer id;
    private String notificationId;
    private NotifyTo notifyTo;
    private String content;
    private Boolean viewed;
    private Timestamp validTill;
    private Timestamp createdDate;

    private UserEntity user;
    private List<UserEntity> users;      //Transient
    private List<NotifyTo> notifyTos;      //Transient

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "notification_id")
    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    @Column(name = "notify_to")
    public NotifyTo getNotifyTo() {
        return notifyTo;
    }

    public void setNotifyTo(NotifyTo notifyTo) {
        this.notifyTo = notifyTo;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "viewed")
    public Boolean getViewed() {
        return viewed;
    }

    public void setViewed(Boolean viewed) {
        this.viewed = viewed;
    }

    @Column(name = "valid_till")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getValidTill() {
        return validTill;
    }

    public void setValidTill(Timestamp validTill) {
        this.validTill = validTill;
    }

    @Column(name = "created_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @Transient
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    @Transient
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public List<NotifyTo> getNotifyTos() {
        return notifyTos;
    }

    public void setNotifyTos(List<NotifyTo> notifyTos) {
        this.notifyTos = notifyTos;
    }
}
