package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yetistep.anyorder.enums.AvailabilityStatus;
import com.yetistep.anyorder.enums.DriverExperienceLevel;
import com.yetistep.anyorder.enums.DriverType;
import com.yetistep.anyorder.enums.VehicleType;
import com.yetistep.anyorder.util.JsonDateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "drivers")
public class DriverEntity implements Serializable {

    private static final long serialVersionUID = -1544454911285700882L;

    private Integer id;
    private String driverId;
    private DriverType driverType;
    private AvailabilityStatus availabilityStatus;
    private VehicleType vehicleType;
    private String vehicleNumber;
    private String licenseNumber;
    private String latitude;
    private String longitude;
    private DriverExperienceLevel experienceLevel;
    private String streetAddress;
    private Integer previousOrderCount;      //transient variable
    private Integer currentOrderCount;      //transient variable
    private BigDecimal totalPreviousOrderEtd;//transient variable
    private BigDecimal currentOrderEtd;   //transient variable
    private BigDecimal totalOrderEtd;
    private BigDecimal rating;
    private String accessToken; //Transient Variable
    private Date lastOrderDelivered;
    private Timestamp lastLocationUpdate;
    private Timestamp createdDate;
    private Integer updateLocationInMinute; //Transient
    private Long timeRequiredToCompleteOrders; //Transient
    //Transients for driver performance
    private Date date;
    private BigDecimal dutyHour;
    private BigDecimal busyHour;
    private BigDecimal distanceCovered;
    private Integer orderAssigned;
    private Integer orderCompleted;

    private UserEntity user;
    private List<OrdersDriverEntity> ordersDrivers;
    private List<DriversAreaEntity> driversArea;
    private List<StoresNotDriverEntity> storesNotDriver;
    private StoreEntity store;
    private List<DriversWorkLogEntity> workLogs;
    private List<OrderEntity> orders;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "driver_id")
    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    @Column(name = "available_status")
    public AvailabilityStatus getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(AvailabilityStatus availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    @Column(name = "vehicle_type")
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Column(name = "vehicle_number")
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    @Column(name = "license_number")
    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "experience_level")
    public DriverExperienceLevel getExperienceLevel() {
        return experienceLevel;
    }

    public void setExperienceLevel(DriverExperienceLevel experienceLevel) {
        this.experienceLevel = experienceLevel;
    }

    @Transient
    public Integer getPreviousOrderCount() {
        return previousOrderCount;
    }

    public void setPreviousOrderCount(Integer previousOrderCount) {
        this.previousOrderCount = previousOrderCount;
    }

    @Transient
    public BigDecimal getTotalPreviousOrderEtd() {
        return totalPreviousOrderEtd;
    }

    public void setTotalPreviousOrderEtd(BigDecimal totalPreviousOrderEtd) {
        this.totalPreviousOrderEtd = totalPreviousOrderEtd;
    }

    @Transient
    public BigDecimal getCurrentOrderEtd() {
        return currentOrderEtd;
    }

    public void setCurrentOrderEtd(BigDecimal currentOrderEtd) {
        this.currentOrderEtd = currentOrderEtd;
    }

    @Transient
    public BigDecimal getTotalOrderEtd() {
        return totalOrderEtd;
    }

    public void setTotalOrderEtd(BigDecimal totalOrderEtd) {
        this.totalOrderEtd = totalOrderEtd;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @OneToMany(mappedBy = "driver", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<OrdersDriverEntity> getOrdersDrivers() {
        return ordersDrivers;
    }

    public void setOrdersDrivers(List<OrdersDriverEntity> ordersDrivers) {
        this.ordersDrivers = ordersDrivers;
    }

    @OneToMany(mappedBy = "driver", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<DriversAreaEntity> getDriversArea() {
        return driversArea;
    }

    public void setDriversArea(List<DriversAreaEntity> driversArea) {
        this.driversArea = driversArea;
    }

    @OneToMany(mappedBy = "driver", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<StoresNotDriverEntity> getStoresNotDriver() {
        return storesNotDriver;
    }

    public void setStoresNotDriver(List<StoresNotDriverEntity> storesNotDriver) {
        this.storesNotDriver = storesNotDriver;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    @OneToMany(mappedBy = "driver", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<DriversWorkLogEntity> getWorkLogs() {
        return workLogs;
    }

    public void setWorkLogs(List<DriversWorkLogEntity> workLogs) {
        this.workLogs = workLogs;
    }

    @Column (name = "driver_type")
    public DriverType getDriverType() {
        return driverType;
    }

    public void setDriverType(DriverType driverType) {
        this.driverType = driverType;
    }

    @Transient
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date getLastOrderDelivered() {
        return lastOrderDelivered;
    }

    public void setLastOrderDelivered(Date lastOrderDelivered) {
        this.lastOrderDelivered = lastOrderDelivered;
    }

    @Column(name = "created_date", columnDefinition="TIMESTAMP NULL DEFAULT NULL")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @OneToMany(mappedBy = "driver", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<OrderEntity> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderEntity> orders) {
        this.orders = orders;
    }

    @Transient
    public Integer getCurrentOrderCount() {
        return currentOrderCount;
    }

    public void setCurrentOrderCount(Integer currentOrderCount) {
        this.currentOrderCount = currentOrderCount;
    }

    @Column(name ="rating")
    public BigDecimal getRating() {
        return rating;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    @Transient
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Transient
    public Integer getUpdateLocationInMinute() {
        return updateLocationInMinute;
    }

    public void setUpdateLocationInMinute(Integer updateLocationInMinute) {
        this.updateLocationInMinute = updateLocationInMinute;
    }

    @Column(name = "last_location_update", columnDefinition = "TIMESTAMP NULL DEFAULT NULL")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getLastLocationUpdate() {
        return lastLocationUpdate;
    }

    public void setLastLocationUpdate(Timestamp lastLocationUpdate) {
        this.lastLocationUpdate = lastLocationUpdate;
    }

    @Transient
    public BigDecimal getDutyHour() {
        return dutyHour;
    }

    public void setDutyHour(BigDecimal dutyHour) {
        this.dutyHour = dutyHour;
    }

    @Transient
    public BigDecimal getBusyHour() {
        return busyHour;
    }

    public void setBusyHour(BigDecimal busyHour) {
        this.busyHour = busyHour;
    }

    @Transient
    public BigDecimal getDistanceCovered() {
        return distanceCovered;
    }

    public void setDistanceCovered(BigDecimal distanceCovered) {
        this.distanceCovered = distanceCovered;
    }

    @Column(name = "street_address")
    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    @Transient
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Transient
    public Integer getOrderAssigned() {
        return orderAssigned;
    }

    public void setOrderAssigned(Integer orderAssigned) {
        this.orderAssigned = orderAssigned;
    }

    @Transient
    public Integer getOrderCompleted() {
        return orderCompleted;
    }

    public void setOrderCompleted(Integer orderCompleted) {
        this.orderCompleted = orderCompleted;
    }

    @Transient
    public Long getTimeRequiredToCompleteOrders() {
        return timeRequiredToCompleteOrders;
    }

    public void setTimeRequiredToCompleteOrders(Long timeRequiredToCompleteOrders) {
        this.timeRequiredToCompleteOrders = timeRequiredToCompleteOrders;
    }
}
