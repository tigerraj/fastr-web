package com.yetistep.anyorder.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yetistep.anyorder.enums.Gender;
import com.yetistep.anyorder.enums.UserDeviceType;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.enums.UserStatus;
import com.yetistep.anyorder.util.JsonDateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 11/4/14
 * Time: 4:55 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity(name="UserEntity")
@Table(name = "users")
public class  UserEntity implements Serializable {

    private static final long serialVersionUID = 2435103313947009972L;

    private Integer id;
    private String userId;
    private UserRole userRole;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String mobileNumber;
    private Boolean mobileVerificationStatus;
    private String emailAddress;
    private String profileImage;
    private Timestamp lastActivityDate;
    private Timestamp createdDate;
    private Boolean blacklistStatus;
    private Boolean verifiedStatus;
    private String verificationCode;
    private UserDeviceType deviceType;
    private UserStatus userStatus;
    private String lastAddressMobile;
    private String accessToken;   //used to validate access token (res api security)

    private List<ActionLogEntity> actionLogEntities;
    private CustomerEntity customer;
    private MerchantEntity merchant;
    private DriverEntity driver;
    private StoreManagerEntity storeManager;
    private List<UserDeviceEntity> userDevices;
    private List<OrderEntity> orders;
    private List<NotificationEntity> notifications;
    private List<OrderCancelEntity> orderCancels;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "userRole")
    @Enumerated(EnumType.STRING)
    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column (name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column (name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name="gender")
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Column(name = "mobile_number")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Column(name = "mobile_verification_status", columnDefinition = "TINYINT(1)")
    public Boolean getMobileVerificationStatus() {
        return mobileVerificationStatus;
    }

    public void setMobileVerificationStatus(Boolean mobileVerificationStatus) {
        this.mobileVerificationStatus = mobileVerificationStatus;
    }

    @Column(name = "email")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Column(name = "profile_image")
    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "last_activity_date", columnDefinition="TIMESTAMP NULL DEFAULT NULL")
    public Timestamp getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(Timestamp lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "created_date", columnDefinition="TIMESTAMP NULL DEFAULT NULL")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "black_list_status", columnDefinition = "TINYINT(1)")
    public Boolean getBlacklistStatus() {
        return blacklistStatus;
    }

    public void setBlacklistStatus(Boolean blacklistStatus) {
        this.blacklistStatus = blacklistStatus;
    }

    @Column(name = "verification_status", columnDefinition = "TINYINT(1)")
    public Boolean getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(Boolean verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    @JsonIgnore
    @Column(name = "verification_code")
    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public List<ActionLogEntity> getActionLogEntities() {
        return actionLogEntities;
    }

    public void setActionLogEntities(List<ActionLogEntity> actionLogEntities) {
        this.actionLogEntities = actionLogEntities;
    }

    @Column(name = "user_status")
    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    @Column(name = "last_address_mobile")
    public String getLastAddressMobile() {
        return lastAddressMobile;
    }

    public void setLastAddressMobile(String lastAddressMobile) {
        this.lastAddressMobile = lastAddressMobile;
    }

    @Column(name = "access_token")
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Column(name = "device_type")
    public UserDeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(UserDeviceType deviceType) {
        this.deviceType = deviceType;
    }

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    @OneToOne(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public MerchantEntity getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantEntity merchant) {
        this.merchant = merchant;
    }

    @OneToOne(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    @OneToOne(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreManagerEntity getStoreManager() {
        return storeManager;
    }

    public void setStoreManager(StoreManagerEntity storeManager) {
        this.storeManager = storeManager;
    }

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<UserDeviceEntity> getUserDevices() {
        return userDevices;
    }

    public void setUserDevices(List<UserDeviceEntity> userDevices) {
        this.userDevices = userDevices;
    }

    @OneToMany(mappedBy = "csr")
    public List<OrderEntity> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderEntity> orders) {
        this.orders = orders;
    }

    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public List<NotificationEntity> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationEntity> notifications) {
        this.notifications = notifications;
    }

    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public List<OrderCancelEntity> getOrderCancels() {
        return orderCancels;
    }

    public void setOrderCancels(List<OrderCancelEntity> orderCancels) {
        this.orderCancels = orderCancels;
    }
}
