package com.yetistep.anyorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 3:15 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "stores_not_drivers")
public class StoresNotDriverEntity implements Serializable {


    private static final long serialVersionUID = 8147359277611464653L;

    private Integer id;
    private String storesNotDriverId;

    private DriverEntity driver;
    private StoreEntity store;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "stores_not_driver_id")
    public String getStoresNotDriverId() {
        return storesNotDriverId;
    }

    public void setStoresNotDriverId(String storesNotDriverId) {
        this.storesNotDriverId = storesNotDriverId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }
}
