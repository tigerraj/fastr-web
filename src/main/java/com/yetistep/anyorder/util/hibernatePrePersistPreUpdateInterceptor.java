package com.yetistep.anyorder.util;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 11:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class hibernatePrePersistPreUpdateInterceptor extends EmptyInterceptor {

    @Override
    public boolean onFlushDirty(Object entity,
                                Serializable id,
                                Object[] currentState,
                                Object[] previousState,
                                String[] propertyNames,
                                Type[] types) {

            Boolean status = false;
            for ( int i=0; i<propertyNames.length; i++ ) {
                if( "lastActivityDate".equals( propertyNames[i] ) || "lastModifiedDate".equals( propertyNames[i] ) ) {
                    currentState[i] = new Timestamp(System.currentTimeMillis());
                    status = true;
                }
            }
            return status;
    }

    @Override
    public boolean onSave(Object entity,
                          Serializable id,
                          Object[] state,
                          String[] propertyNames,
                          Type[] types) {

        Boolean status = false;
        for ( int i=0; i<propertyNames.length; i++ ) {
            String entityName = entity.getClass().getSimpleName();
            String entityId = entityName.replaceAll("Entity", "")+"Id";
            entityId = Character.toLowerCase(entityId.charAt(0)) + entityId.substring(1);
            if ( entityId.equals(propertyNames[i]) ) {
                //state[i] = String.valueOf(OrderIDAllocator.getID());
                state[i] = UUID.randomUUID().toString();
                status = true;
            }
            if( "createdDate".equals( propertyNames[i] ) ) {
                state[i] = new Timestamp(System.currentTimeMillis());
                status = true;
            }
            if( "orderDisplayId".equals( propertyNames[i] ) ) {
                state[i] = OrderIDAllocator.getID();
                status = true;
            }
        }

        return status;
    }


}
