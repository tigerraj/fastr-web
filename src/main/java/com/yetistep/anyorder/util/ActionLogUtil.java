package com.yetistep.anyorder.util;

import com.yetistep.anyorder.enums.ActionType;
import com.yetistep.anyorder.model.ActionLogEntity;
import com.yetistep.anyorder.model.UserEntity;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 8/6/15
 * Time: 5:49 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ActionLogUtil {

    private static final Logger log = Logger.getLogger(ActionLogUtil.class);

    public static ActionLogEntity createActionLog(Object firstInstance, Object secondInstance, String[] fields, UserEntity userEntity) throws Exception {

        log.info("----- Creating Action Log -----");

        if (firstInstance != null && secondInstance != null && (firstInstance.getClass().equals(secondInstance.getClass()))) {

            //String userIp = request.getHeader("X-FORWARDED-FOR") == null ? request.getRemoteAddr() : request.getHeader("X-FORWARDED-FOR");

            WebAuthenticationDetails details = (WebAuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
            String userIp = details.getRemoteAddress();

            if (userEntity != null) {

                Map<Object, Object[]> methodValueDifference = getMethodValueDifference(firstInstance, secondInstance, fields);

                if (methodValueDifference != null) {

                    Object id = PropertyUtils.getProperty(firstInstance, "id");

                    String description = createChangedValuesStr(id, methodValueDifference, firstInstance.getClass().toString());

                    ActionLogEntity actionLog = new ActionLogEntity();

                    actionLog.setUser(userEntity);
                    actionLog.setUserRole(userEntity.getUserRole());
                    actionLog.setActionType(ActionType.UPDATE);
                    actionLog.setDescription(description);
                    //actionLog.setUserIP(userIp);
                    actionLog.setUserIP(userIp);
                    actionLog.setCreatedDate(DateUtil.getCurrentTimestampSQL());

                    return actionLog;
                } else {
                    log.error("----- There is no difference in the property values of the objects -----");
                }
            } else {
                log.error("----- Unidentified user -----");
            }
        } else {
            log.error("----- Either objects are null or objects are not of same type -----");
        }
        return null;
    }

    //Object firstInstance and Object secondInstance must of same type
    //Object firstInstance is the original object which is being modified
    //Object secondInstance is the modified object instance
    //String[] methods is the list of methods name from the objects
    public static Map<Object, Object[]> getMethodValueDifference(Object firstInstance, Object secondInstance, String[] fields) throws Exception {

        log.info("----- Getting methods along with difference in the values of the properties of the provided objects -----");

        //Map<Object(methodName), Object[](returnedValueOfMethod, original, changed)>
        Map<Object, Object[]> methodDifferenceValue = new HashMap<>();

       /* Method[] classMethods = firstInstance.getClass().getMethods();

        for (Method method : classMethods) {
            for (String methodName : methods) {
                if (method.getName().equals(methodName)) {
                    Object firstValue = method.invoke(firstInstance, null);
                    Object secondValue = method.invoke(secondInstance, null);
                    if (!firstValue.equals(secondValue)) {
                        methodDifferenceValue.put(methodName, new Object[]{firstValue, secondValue});
                    }
                }
            }
        }*/
        for (String field : fields) {
            Object firstValue = PropertyUtils.getProperty(firstInstance, field.trim());
            Object secondValue = PropertyUtils.getProperty(secondInstance, field.trim());
            if ((firstValue == null && secondValue != null)
                    || (firstValue != null && secondValue == null)) {
                methodDifferenceValue.put(field, new Object[]{firstValue == null ? "null" : firstValue, secondValue == null ? "null" : secondValue});
            } else if (firstValue != null && secondValue != null && !firstValue.equals(secondValue)) {
                if (firstValue instanceof BigDecimal) {
                    if (!BigDecimalUtil.isEqualTo((BigDecimal)firstValue, (BigDecimal)secondValue)) {
                        methodDifferenceValue.put(field, new Object[]{firstValue, secondValue});
                    }
                } else if (firstValue instanceof List) {
                    if (((List) firstValue).size() != ((List)secondValue).size()) {
                        methodDifferenceValue.put(field, new Object[]{firstValue, secondValue});
                    } else {
                        Boolean isDifferent = Boolean.FALSE;
                        for (Object obj : (List)firstValue) {
                            for (Object obj1 : (List)secondValue) {
                                if (obj != obj1) {
                                    methodDifferenceValue.put(field, new Object[]{firstValue, secondValue});
                                    isDifferent = Boolean.TRUE;
                                    break;
                                }
                            }
                            if (isDifferent) {
                                break;
                            }
                        }
                    }
                } else {
                    methodDifferenceValue.put(field, new Object[]{firstValue, secondValue});
                }
            }
        }

        return methodDifferenceValue.size() > 0 ? methodDifferenceValue : null;
    }

    //Map<Object(methodName), Object[](returnedValueOfMethod, original, changed)>
    public static String createChangedValuesStr(Object id, Map<Object, Object[]> methodDifferenceValue, String className) throws Exception {

        StringBuilder sbChangedValuesStr = new StringBuilder();
        sbChangedValuesStr.append("[" + extractClassName(className) + " - " + id + "] ");

        for (Map.Entry<Object, Object[]> entry : methodDifferenceValue.entrySet()) {
            String field = entry.getKey().toString();
            Object[] differenceValue = entry.getValue();
            sbChangedValuesStr.append(field + " " + " previous: ");
            sbChangedValuesStr.append(differenceValue[0].toString() + " after: " + differenceValue[1]);
            sbChangedValuesStr.append(", ");
        }

        return sbChangedValuesStr.toString().substring(0, sbChangedValuesStr.toString().length() - 2);
    }

    //This method extract only sensible class name from full class name
    // e.g. "class com.yetistep.anyorder.model.CityEntity" will be converted to "City"
    public static String extractClassName(String fullClassName) {
        return fullClassName.substring(fullClassName.lastIndexOf(".") + 1, fullClassName.length() - 6);
    }

    public static ActionLogEntity createActionLog(Object firstInstance, UserEntity userEntity) throws Exception {

        log.info("----- Creating Action Log for adding operation -----");

        if (firstInstance != null) {

            //String userIp = request.getHeader("X-FORWARDED-FOR") == null ? request.getRemoteAddr() : request.getHeader("X-FORWARDED-FOR");

            WebAuthenticationDetails details = (WebAuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
            String userIp = details.getRemoteAddress();

            if (userEntity != null) {

                Object id = PropertyUtils.getProperty(firstInstance, "id");

                ActionLogEntity actionLog = new ActionLogEntity();

                actionLog.setUser(userEntity);
                actionLog.setUserRole(userEntity.getUserRole());
                actionLog.setActionType(ActionType.CREATE);
                actionLog.setDescription("[" + extractClassName(firstInstance.getClass().toString()) + " - " + id + "] has been added");
                //actionLog.setUserIP(userIp);
                actionLog.setUserIP(userIp);
                actionLog.setCreatedDate(DateUtil.getCurrentTimestampSQL());

                return actionLog;

            } else {
                log.error("----- Unidentified user -----");
            }
        } else {
            log.error("----- Object is null or objects are not of same type -----");
        }
        return null;
    }

    public static ActionLogEntity createActionLog(String description, UserEntity userEntity) throws Exception {

        log.info("----- Creating Action Log for adding operation -----");
        //String userIp = request.getHeader("X-FORWARDED-FOR") == null ? request.getRemoteAddr() : request.getHeader("X-FORWARDED-FOR");

        WebAuthenticationDetails details = (WebAuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        String userIp = details.getRemoteAddress();

        if (userEntity != null) {

            ActionLogEntity actionLog = new ActionLogEntity();

            actionLog.setUser(userEntity);
            actionLog.setUserRole(userEntity.getUserRole());
            actionLog.setDescription(description);
            //actionLog.setUserIP(userIp);
            actionLog.setUserIP(userIp);
            actionLog.setCreatedDate(DateUtil.getCurrentTimestampSQL());

            return actionLog;

        } else {
            log.error("----- Unidentified user -----");
        }
        return null;
    }
}