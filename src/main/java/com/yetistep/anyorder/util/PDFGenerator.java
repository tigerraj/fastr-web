package com.yetistep.anyorder.util;


import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.color.DeviceRgb;
import com.itextpdf.kernel.color.WebColors;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.TextAlignment;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 2/20/15
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class PDFGenerator {

    private static final Logger log = Logger.getLogger(PDFGenerator.class);
    private static final String HOME_DIR = System.getProperty("catalina.home");
    @Autowired
    SystemPropertyService systemPropertyService;

    private float cellPadding = 15;
    private float paragraphPadding = 8;
    private Border headerParagraphBorderLeft = new SolidBorder(Color.BLACK, 4);
    private Border lineBorder = new SolidBorder(Color.BLACK, 1);
    private Border cellBorderWhite = new SolidBorder(Color.WHITE, 1);
    private DeviceRgb cellBackgroundColor = WebColors.getRGBColor("#" + "C7D2E8");
    private float headerFontSize = 9;
    private float statementFontSize = 15;
    private float statementBodyFontSize = 10;
    private float paragraphFontSize = 9;

    public String generateStatement(MerchantEntity merchant, StatementEntity statement, StoreEntity store, String imageUrl, Map<String, String> preferences) throws Exception {
        File invoiceFile = generateStatementPDF(merchant, statement, store, imageUrl, preferences);
        log.info("imageUrl i want to die here2:" + imageUrl + ":start here");
        if (invoiceFile == null)
            return null;

        String invoicePath = invoiceFile.getPath();

        //upload invoice to S3 Bucket
        if (!MessageBundle.isLocalHost()) {
            int noOfRetry = 3;
            for (int i = 0; i < noOfRetry; i++) {//retry three time, if exception occurs
                try {
                    String dir = getInvoiceDir(merchant, store, File.separator);
                    String bucketUrl = AmazonUtil.uploadFileToS3(invoiceFile, dir, invoiceFile.getName(), false);
                    invoicePath = AmazonUtil.cacheImage(bucketUrl);
                    break;
                } catch (Exception e) {
                    if (i == (noOfRetry - 1)) throw e;
                    GeneralUtil.wait(1500);
                }
            }
        }
        return invoiceFile.exists() ? invoicePath : null;
    }

    private File generateStatementPDF(MerchantEntity merchant, StatementEntity statement, StoreEntity store, String imageUrl, Map<String, String> preferences) throws Exception {

        File invoiceFile = null;

        try {
            invoiceFile = getFile(merchant, store, "invoice");
            PdfWriter pdfWriter = new PdfWriter(invoiceFile.getAbsolutePath());
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            PageSize pageSize = PageSize.A4;
            Document document = new Document(pdfDocument, pageSize);

            Table table = new Table(4);
            table.setWidthPercent(100);

            RatePlanEntity activeRatePlan = null;
            UnknownAreaRatePlanEntity activeUnknownAreaRatePlan = null;

            for (RatePlanEntity ratePlan : store.getRatePlans()) {
                if (ratePlan.getStatus().equals(Status.ACTIVE)) {
                    activeRatePlan = ratePlan;
                }
            }

            for (UnknownAreaRatePlanEntity unknownAreaRatePlan : store.getUnknownAreaRatePlans()) {
                if (unknownAreaRatePlan.getStatus().equals(Status.ACTIVE) && unknownAreaRatePlan.getIsKnownArea() != null && unknownAreaRatePlan.getIsKnownArea()) {
                    activeUnknownAreaRatePlan = unknownAreaRatePlan;
                }
            }
            log.info("imageUrl i want to die here3:" + imageUrl + ":start here");
            renderTableHeader(table, imageUrl, preferences, store);
            renderMiddlePart(table, statement);
            renderStatementDetail(table, statement, preferences, activeRatePlan, activeUnknownAreaRatePlan);

            Cell cell = new Cell(1, 4);
            cell.setBorder(Border.NO_BORDER);
            cell.setMarginTop(30);

            Paragraph paragraph = new Paragraph();
            paragraph.setFontSize(paragraphFontSize);
            paragraph.add("More details available in " + preferences.get("SERVER_URL") + ". For any queries please contact: " + preferences.get("SUPPORT_EMAIL"));
            cell.add(paragraph);
            table.addCell(cell);

            document.add(table);

            document.close();

        } catch (Exception e) {
            log.error("Error occurred while generating statement.", e);

            if (invoiceFile != null) {
                invoiceFile.delete();
            }
            throw e;
        }

        return invoiceFile;
    }

    private void renderTableHeader(Table table, String imageUrl, Map<String, String> preferences, StoreEntity store) throws MalformedURLException {

        //Start Logo part
        Cell logoHolderCell = new Cell(1, 4);
        logoHolderCell.setBorder(Border.NO_BORDER);
        logoHolderCell.setPadding(cellPadding);
        Image companyLogo = new Image(ImageDataFactory.create(new URL(imageUrl)));
        logoHolderCell.add(companyLogo);
        table.addCell(logoHolderCell);
        //End Logo part

        //Start company info part
        Cell companyInfoHolderCell = new Cell(1, 2);
        companyInfoHolderCell.setBorder(Border.NO_BORDER);
        companyInfoHolderCell.setPadding(cellPadding);

        Paragraph companyInfoParagraph = new Paragraph()
                .add(preferences.get("COMPANY_ADDRESS") + "\n")
                .add("United Arab Emirates\n")
                .add("Email: " + preferences.get("SUPPORT_EMAIL") + "\n")
                .add("Phone: " + preferences.get("HELPLINE_NUMBER") + "\n")
                .add("Fax: " + preferences.get("HELPLINE_NUMBER") + "\n")
                .setFontSize(headerFontSize);
        companyInfoParagraph.setBorderLeft(headerParagraphBorderLeft);
        companyInfoParagraph.setPaddingLeft(paragraphPadding);
        companyInfoHolderCell.add(companyInfoParagraph);
        table.addCell(companyInfoHolderCell);
        //End company info part

        //Start merchant info part
        Cell merchantInfoHolderCell = new Cell(1, 2);
        merchantInfoHolderCell.setBorder(Border.NO_BORDER);
        merchantInfoHolderCell.setPadding(cellPadding);

        Text text = new Text(store.getStoreBrand().getBrandName());
        Paragraph merchantInfoParagraph = new Paragraph()
                .add(text.getText() + "\n")
                .add("Address: " + store.getGivenLocation1() + "\n")
                .add("Email: " + store.getStoreBrand().getMerchant().getUser().getEmailAddress() + "\n")
                .add("Phone: " + store.getStoreBrand().getMerchant().getUser().getMobileNumber() + "\n")
                .add("Fax: " + store.getStoreBrand().getMerchant().getUser().getMobileNumber() + "\n")
                .setFontSize(headerFontSize);
        merchantInfoParagraph.setMarginLeft(50);
        merchantInfoParagraph.setBorderLeft(headerParagraphBorderLeft);
        merchantInfoParagraph.setPaddingLeft(paragraphPadding);
        merchantInfoHolderCell.add(merchantInfoParagraph);
        table.addCell(merchantInfoHolderCell);
        //End merchant info part
    }

    private void renderMiddlePart(Table table, StatementEntity statement) throws IOException {

        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(statement.getFromDate());
        Integer fromYear = fromCalendar.get(Calendar.YEAR);
        Integer fromMonth = fromCalendar.get(Calendar.MONTH) + 1;
        Integer fromDayOfMonth = fromCalendar.get(Calendar.DAY_OF_MONTH);
        Integer fromDayOfWeek = fromCalendar.get(Calendar.DAY_OF_WEEK);

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(statement.getToDate());
        Integer toYear = toCalendar.get(Calendar.YEAR);
        Integer toMonth = toCalendar.get(Calendar.MONTH) + 1;
        Integer toDayOfMonth = toCalendar.get(Calendar.DAY_OF_MONTH);
        Integer toDayOfWeek = toCalendar.get(Calendar.DAY_OF_WEEK);

        //Start Statement Month info part
        Cell statementMonthCell = new Cell(1, 4);
        statementMonthCell.setBorder(Border.NO_BORDER);
        statementMonthCell.setBorderBottom(lineBorder);
        statementMonthCell.setPadding(cellPadding);

        Paragraph statementMonthParagraph = new Paragraph("Statement ("
                + fromYear + "-" + fromMonth + "-" + fromDayOfMonth
                + " to "
                + toYear + "-" + toMonth + "-" + toDayOfMonth + ")");
        statementMonthParagraph.setTextAlignment(TextAlignment.CENTER);
        statementMonthParagraph.setFontSize(statementFontSize);

        statementMonthCell.add(statementMonthParagraph);
        table.addCell(statementMonthCell);
    }

    private void renderStatementDetail(Table table, StatementEntity statement, Map<String, String> preferences, RatePlanEntity ratePlan, UnknownAreaRatePlanEntity unknownAreaRatePlan) {
        //Start RatePlan Title
        Cell ratePlanCell = new Cell(1, 4);
        ratePlanCell.setBorder(Border.NO_BORDER);
        ratePlanCell.setPadding(cellPadding);

        Paragraph ratePlanParagraph;
        if (ratePlan != null) {
            ratePlanParagraph = new Paragraph("Subscribed Rate Plan: " + ratePlan.getName());
        } else {
            ratePlanParagraph = new Paragraph("Subscribed Rate Plan: " + unknownAreaRatePlan.getName());
        }
        ratePlanParagraph.setFontSize(12);
        ratePlanCell.add(ratePlanParagraph);
        table.addCell(ratePlanCell);
        //End RatePlan Title
        if (ratePlan != null) {
            renderRows(table, "Monthly Charge On Plan", "Total no. of Order", ratePlan.getOrderCount().toString(),
                    NumberFormaterUtil.americanStandard(ratePlan.getAmount().setScale(2, RoundingMode.HALF_UP), preferences.get("CURRENCY")));

            renderRows(table, "Extra Order Charge On Plan", "Total no. of Order", statement.getExtraOrderCount().toString(),
                    NumberFormaterUtil.americanStandard(statement.getExtraOrderCharge().setScale(2, RoundingMode.HALF_UP), preferences.get("CURRENCY")));
        } else {
            renderRows(table, "Distance Based Order Charge", "Total no. of Order", statement.getDistanceBasedOrderCount().toString(),
                    NumberFormaterUtil.americanStandard(statement.getDistanceBasedOrderAmount().setScale(2, RoundingMode.HALF_UP), preferences.get("CURRENCY")));
        }
        renderRows(table, "Outside Serving Area Delivery Charges", "Total no. of Order", statement.getOutOfServingAreaOrderCount().toString(),
                NumberFormaterUtil.americanStandard(statement.getOutOfServingAreaOrderCharge().setScale(2, RoundingMode.HALF_UP), preferences.get("CURRENCY")));
        if (statement.getOtherCharge() != null && BigDecimalUtil.isGreaterThen(statement.getOtherCharge(), BigDecimal.ZERO)) {
            renderRows(table, "", "Other Charges", statement.getOtherCharge().toString(),
                    NumberFormaterUtil.americanStandard(statement.getOtherCharge().setScale(2, RoundingMode.HALF_UP), preferences.get("CURRENCY")));
        }
        renderTotalPayableRow(table, "Total Payable",
                NumberFormaterUtil.americanStandard(statement.getTotalPayableAmount().setScale(2, RoundingMode.HALF_UP), preferences.get("CURRENCY")));
    }

    private void renderRows(Table table, String title, String col1, String col2, String col3) {

        Cell onPlanCell = new Cell(1, 4);
        onPlanCell.setBorder(Border.NO_BORDER);
        onPlanCell.setPadding(cellPadding);

        Paragraph onPlanParagraph = new Paragraph(title);
        onPlanParagraph.setFontSize(statementBodyFontSize);
        onPlanCell.add(onPlanParagraph);
        table.addCell(onPlanCell);

        //column 1
        Cell onPlanOrderCell = new Cell(1, 2);
        onPlanOrderCell.setBorder(cellBorderWhite);
        onPlanOrderCell.setBackgroundColor(cellBackgroundColor);
        onPlanOrderCell.setPadding(cellPadding);

        Paragraph onPlanOrderParagraph = new Paragraph(col1);
        onPlanOrderParagraph.setFontSize(statementBodyFontSize);
        onPlanOrderCell.add(onPlanOrderParagraph);
        table.addCell(onPlanOrderCell);

        //column 2
        Cell onPlanOrderCountCell = new Cell(1, 1);
        onPlanOrderCountCell.setBorder(cellBorderWhite);
        onPlanOrderCountCell.setBackgroundColor(cellBackgroundColor);
        onPlanOrderCountCell.setPadding(cellPadding);

        Paragraph onPlanOrderCountParagraph = new Paragraph(col2);
        onPlanOrderCountParagraph.setTextAlignment(TextAlignment.CENTER);
        onPlanOrderCountParagraph.setFontSize(statementBodyFontSize);
        onPlanOrderCountCell.add(onPlanOrderCountParagraph);
        table.addCell(onPlanOrderCountCell);

        //column 3
        Cell onPlanOrderAmountCell = new Cell(1, 1);
        onPlanOrderAmountCell.setBorder(cellBorderWhite);
        onPlanOrderAmountCell.setBackgroundColor(cellBackgroundColor);
        onPlanOrderAmountCell.setPadding(cellPadding);

        Paragraph onPlanOrderAmountParagraph = new Paragraph(col3);
        onPlanOrderAmountParagraph.setTextAlignment(TextAlignment.RIGHT);
        onPlanOrderAmountParagraph.setFontSize(statementBodyFontSize);
        onPlanOrderAmountCell.add(onPlanOrderAmountParagraph);
        table.addCell(onPlanOrderAmountCell);

    }

    private void renderTotalPayableRow(Table table, String col1, String col2) {

        Cell lineCell = new Cell(1, 4);
        lineCell.setBorder(Border.NO_BORDER);
        lineCell.setPaddingBottom(10);
        lineCell.setBorderBottom(lineBorder);
        Paragraph paragraph = new Paragraph("");
        lineCell.add(paragraph);
        table.addCell(lineCell);

        Cell totalPayableCell = new Cell(1, 3);
        totalPayableCell.setBorder(cellBorderWhite);
        totalPayableCell.setPadding(cellPadding);
        totalPayableCell.setMarginTop(20);

        Paragraph onPlanOrderParagraph = new Paragraph(col1);
        onPlanOrderParagraph.setTextAlignment(TextAlignment.RIGHT);
        totalPayableCell.add(onPlanOrderParagraph);
        table.addCell(totalPayableCell);

        //column 2
        Cell onPlanOrderCountCell = new Cell(1, 1);
        onPlanOrderCountCell.setBorder(cellBorderWhite);
        onPlanOrderCountCell.setBackgroundColor(cellBackgroundColor);
        onPlanOrderCountCell.setPadding(cellPadding);
        onPlanOrderCountCell.setMarginTop(20);

        Paragraph onPlanOrderCountParagraph = new Paragraph(col2);
        onPlanOrderCountParagraph.setTextAlignment(TextAlignment.RIGHT);
        onPlanOrderCountCell.add(onPlanOrderCountParagraph);
        table.addCell(onPlanOrderCountCell);
    }

    private File getFile(MerchantEntity merchant, StoreEntity store, String name) {
        String dir = getInvoiceDir(merchant, store, File.separator);

        File invoiceDir = new File(HOME_DIR + File.separator + dir + File.separator);
        if (!invoiceDir.exists()) {
            invoiceDir.mkdirs();
        }

        String fileName = System.currentTimeMillis() + "_" + name + ".pdf";
        return new File(invoiceDir, fileName);
    }

    private String getInvoiceDir(MerchantEntity merchant, StoreEntity store, String separator) {
        return MessageBundle.separateString(separator, "statements", "Merchant_" + merchant.getId(), "StoreBrand_" + store.getStoreBrand().getId(), "Store" + store.getId());
    }


}
