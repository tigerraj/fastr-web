package com.yetistep.anyorder.util;

import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.model.AreaEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suroz on 1/3/2017.
 */
public class AreaTreeUtil {
    public static void parseActiveAreaTree(List<AreaEntity> parenAreas, List<Integer> areaIds) {
        if (parenAreas != null) {
            for (AreaEntity area : parenAreas) {
                List<AreaEntity> activeChild = new ArrayList<>();
                for (AreaEntity child : area.getChild()) {
                    if (child.getStatus().equals(Status.ACTIVE)) {
                        activeChild.add(child);
                    }
                }
                if (activeChild.size() > 0)
                    area.setChild(activeChild);
                else
                    area.setChild(null);
                if (areaIds.contains(area.getId()))
                    area.setSelected(true);
                else
                    area.setSelected(false);
                parseActiveAreaTree(area.getChild(), areaIds);
            }
        }
    }
}
