package com.yetistep.anyorder.util;


import com.yetistep.anyorder.enums.NotifyTo;
import com.yetistep.anyorder.enums.PushNotificationRedirect;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 2/2/15
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class PushNotification {
    private Map<String, String> message;
    private List<String> tokens;
    private String extraDetail;//is attached after the redirect string only when required
    private PushNotificationRedirect pushNotificationRedirect = PushNotificationRedirect.DELIVR;
    private NotifyTo notifyTo;

    public PushNotification(){}

    public PushNotification(Map<String, String> message){
        this.message = message;
        this.tokens = new ArrayList<String>();
    }

    public PushNotification(Map<String, String> message, String token) {
        this(message);
        tokens.add(token);
    }

    public Map<String, String> getMessage() {
        return message;
    }

    public void setMessage(Map<String, String> message) {
        this.message = message;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public String getExtraDetail() {
        return extraDetail;
    }

    public void setExtraDetail(String extraDetail) {
        this.extraDetail = extraDetail;
    }

    public PushNotificationRedirect getPushNotificationRedirect() {
        return pushNotificationRedirect;
    }

    public void setPushNotificationRedirect(PushNotificationRedirect pushNotificationRedirect) {
        this.pushNotificationRedirect = pushNotificationRedirect;
    }

    public NotifyTo getNotifyTo() {
        return notifyTo;
    }

    public void setNotifyTo(NotifyTo notifyTo) {
        this.notifyTo = notifyTo;
    }
}
