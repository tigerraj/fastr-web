package com.yetistep.anyorder.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: yetistep
 * Date: 12/17/13
 * Time: 4:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class RestClient {
    private static final String GET = "GET";
    private static final String POST = "POST";

    private String url;
    private Map<String, String> headers;

    public RestClient(String url) {
        this.url = url;
        headers = new HashMap<String, String>();
    }

    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    public Response doGet() throws Exception {
        HttpURLConnection con = getConnection(GET);
        Response response = new Response(con.getResponseCode(), con.getResponseMessage());

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS.FAIL_ON_EMPTY_BEANS, false);
        ObjectReader reader = mapper.reader(Map.class);
        Map<String, Object> map = reader.readValue(con.getInputStream());
        response.setResponseBody(map);
        return response;
    }


    public Response doPost(String body) throws Exception {
        HttpURLConnection con = getConnection(POST);
        con.setDoInput(true);
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(body); // send http request
        wr.flush();
        wr.close();

        Response response = new Response(con.getResponseCode(), con.getResponseMessage());
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS.FAIL_ON_EMPTY_BEANS, false);
        //Object object = readStream(con.getInputStream())
        ObjectReader reader = mapper.reader(Map.class);
        Map<String, Object> map = reader.readValue(con.getInputStream());
        response.setResponseBody(map);
        return response;
    }

    //read input stream
    private String readStream(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();
        return response.toString();
    }


    //get http connection
    private HttpURLConnection getConnection(String method) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod(method);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        for (String key : headers.keySet())
            con.setRequestProperty(key, headers.get(key));

        return con;
    }


    public class Response {
        private Map<String, Object> responseBody;
        private String responseMsg;
        private int responseCode;


        public Response(int responseCode, String responseMsg) {
            this.responseCode = responseCode;
            this.responseMsg = responseMsg;
        }

        public Map<String, Object> getResponseBody() {
            return responseBody;
        }

        public void setResponseBody(Map<String, Object> responseBody) {
            this.responseBody = responseBody;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(int responseCode) {
            this.responseCode = responseCode;
        }

        public String getResponseMsg() {
            return responseMsg;
        }

        public void setResponseMsg(String responseMsg) {
            this.responseMsg = responseMsg;
        }

        public String toString(){
            return responseCode+" "+responseMsg+"\n"+responseBody.toString();
        }
    }

}