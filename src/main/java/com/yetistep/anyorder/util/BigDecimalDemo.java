package com.yetistep.anyorder.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 3/30/15
 * Time: 12:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class BigDecimalDemo {
    static double d;
    public static void main(String[] args) {
        System.out.println(d);
//        BigDecimal bd = new BigDecimal("55455");
//        System.out.println(BigDecimalUtil.chopToTwoDecimalPlace(bd));
//        System.out.println(bd.setScale(2, RoundingMode.DOWN));
//        System.out.println(bd.setScale(2, RoundingMode.HALF_DOWN));
//        System.out.println(bd.setScale(2, RoundingMode.HALF_EVEN));
//        System.out.println(bd.setScale(2, BigDecimal.ROUND_FLOOR));
//        System.out.println(bd.setScale(2, BigDecimal.ROUND_CEILING));
        //longTrimExample();
        //propertiesDemo();
       // encoderDecoder();
//        Get1();
//        BigDecimalDemo bd = new BigDecimalDemo();
//        bd.dogTest();
//        Integer m = 0;
//        integerTest(m);
//        propertiesTest();
    }

    class DTest{
        double d;

        public void print(){
            System.out.println("D:"+d);
        }
    }




    public static void integerTest(Integer m){
        Integer i = 0;
        if( i == 0)
            System.out.println("True");
        if(i.equals(0))
            System.out.println("Trueeee");
        if( m == 0)
            System.out.println(" m True");
        if(m.equals(0))
            System.out.println("m Trueeee");
    }

    public class Dog{
        private String name;

        public Dog(String name){
            this.name = name;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public void dogTest(){
        Dog aDog = new Dog("Max");
        foo(aDog);

        if( aDog.getName().equals("Max") ){ //true
            System.out.println( "Java passes by value." );

        }else if( aDog.getName().equals("Fifi") ){
            System.out.println( "Java passes by reference." );
        }
    }


    public void foo(Dog d) {
        d.getName().equals("Max"); // true

       /* d = new Dog("Fifi");
        d.getName().equals("Fifi"); // true*/
        d.setName("Fifi");
    }


    private static void Get()
    {
        String[] strArray = { "First", "Second" };
        String first= strArray[0];
        GetValue(strArray);
        first= strArray[0];
        System.out.println(first);
    }

    private static void GetValue(String[] str)
    {
        str[0] = "Second";
        str[1] = "First";
    }

    private static void Get1()
    {
        String first= "First";
        GetValue1(first);

        //Aba first ko value k huncha
        System.out.println("Print:"+first);
    }

    private static void GetValue1(String str)
    {
        str = "Second";
    }


    private static void longTrimExample(){
        Long currentTime = 1429100955500L;
        Timestamp t = new Timestamp(currentTime);
        System.out.println(t);
        System.out.println("Current:"+currentTime);
        currentTime = currentTime%1000 > 500 ? ((currentTime/1000)*1000)+1000 : (currentTime/1000)*1000;
        System.out.println("Current:"+currentTime);
    }



    private static void bigDecimalExample(){
        // create 3 BigDecimal objects
        BigDecimal bg1, bg2, bg3;

        bg2 = new BigDecimal("-100.123");
        bg1 = new BigDecimal("50.56");

        // subtract bg1 with bg2 using mc and assign result to bg3   fa1bc40fef9-55
        bg3 = bg1.subtract(bg2);
        System.out.println("value:"+bg3);
        System.out.println("Absolute value:"+bg3.abs());
        String str = "The Result of Subtraction is " + bg3;
    }

    private static void encoderDecoder() {
        try {
            Integer customerId = 5;
            //Long currentTime = getLongTime("2015-05-27 18:16:36");
            Long currentTime = 1432729895000L;
            Date d = new Date(currentTime);
            System.out.println("date:"+d);
            System.out.println("Current Time:"+currentTime);
            currentTime = (currentTime / 1000) * 1000;
            BigDecimal availableAMount =  new BigDecimal("14.16").setScale(2, BigDecimal.ROUND_HALF_UP);
            BigDecimal transactionAmount = new BigDecimal("2526").setScale(2, BigDecimal.ROUND_HALF_UP);
            System.out.println("Before Encode:");
            System.out.println("Customer ID:" + customerId + "\t Current Time:" + currentTime + "\t Amount:" + transactionAmount);

            transactionAmount = transactionAmount.add(new BigDecimal(currentTime));
            transactionAmount = transactionAmount.multiply(new BigDecimal(customerId));
            transactionAmount = transactionAmount.add(availableAMount);
            String[] encode = (transactionAmount+"").split(Pattern.quote("."));
            String signature = "";
            for (int i = 0; i < encode.length; i++) {
                if (signature.equals("")) {
                    Long l = Long.parseLong(encode[i]);
                    signature = Long.toHexString(l) + "-";
                } else
                    signature += encode[i];
            }

            System.out.println("Signature:" + signature);

            signature = "683eac8731c-16"; //  fa1bd47fc1e-11

            String[] decode = signature.split("-");
            String parsedResult = "";
            for (int i = 0; i < decode.length; i++) {
                if (parsedResult.equals(""))
                    parsedResult = Long.parseLong(decode[i], 16) + ".";
                else
                    parsedResult += decode[i].toString();
            }
            BigDecimal decodedData = new BigDecimal(parsedResult);
            decodedData = decodedData.subtract(availableAMount);
            System.out.println("Decoded Data After deducting AVAILABLE Amount:"+decodedData);
            decodedData = decodedData.divide(new BigDecimal(customerId));
            System.out.println("Decoded Data After dividing From customer ID:"+decodedData);
            decodedData = decodedData.subtract(new BigDecimal(currentTime));
            System.out.println("Final Amount:" + decodedData);

        } catch (Exception e) {
            e.printStackTrace();
        }


//
//        Value:1427873740159
//        HEX:14c73e8357f
//        1427873740159
//

//        Long val = new Long(070);
//        System.out.println("Value:"+val);
//
//        String hex = Long.toHexString(val);
//        System.out.println("HEX:"+hex);
//
//        Long parsedResult1 = Long.parseLong(hex, 16);
//        System.out.println(parsedResult1);
//
//        parsedResult = parsedResult/2;
//        System.out.println(parsedResult);
//
//
//        Long l = Long.MAX_VALUE;
//        System.out.println("L:"+l);
//        Long l2 = System.currentTimeMillis()*90000*900000;
//        System.out.println("L2:"+l2);
    }


    private static Long getLongTime(String s){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = sdf.parse(s);
            return d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }
}
