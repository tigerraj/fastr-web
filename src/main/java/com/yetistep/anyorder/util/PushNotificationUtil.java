package com.yetistep.anyorder.util;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.yetistep.anyorder.enums.NotifyTo;
import com.yetistep.anyorder.enums.PushNotificationRedirect;
import com.yetistep.anyorder.model.UserDeviceEntity;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;

import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

/**
* Created with IntelliJ IDEA.
* User: Chandra Prakash Panday
* Date: 1/30/15
* Time: 5:12 PM
* To change this template use File | Settings | File Templates.
*/
@Async
public class PushNotificationUtil {
    public static final String CERTIFICATE_IOS = "CertificatesDIstribution.p12";
    public static final String PASSWORD_IOS = "123456";
    private static final Logger log = Logger.getLogger(PushNotificationUtil.class);
    /*private static final String ANDROID_SM_API_KEY = "AIzaSyDQTMH605jJqB3-QzZgyzolLiCNpdIPZDQ"; //AOS
    private static final String ANDROID_DRIVER_API_KEY = "AIzaSyDQTMH605jJqB3-QzZgyzolLiCNpdIPZDQ";*/
    private static final String ANDROID_SM_API_KEY = "AIzaSyDhi9BHuiliEoHozS0MYdkvMdF5e5_r29c";
    private static final String ANDROID_DRIVER_API_KEY = "AIzaSyDhi9BHuiliEoHozS0MYdkvMdF5e5_r29c";

//    private static final String ANDROID_DRIVER_API_KEY = "AIzaSyDTWQygKDeK0aeOo5G7IKkZQdZ2gbe8B5M";
    //public static final String CERTIFICATE_IOS = "Koolkat_dev.p12";
    //public static final String PASSWORD_IOS = "password123";

    public static void sendPushNotification(UserDeviceEntity userDevice, NotifyTo notifyTo, Map<String, String> message) {
        if (userDevice != null) {
            if(userDevice.getFamily() != null && userDevice.getDeviceToken() != null) {
                PushNotification pushNotification = new PushNotification();

                pushNotification.setTokens(Collections.singletonList(userDevice.getDeviceToken()));
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(notifyTo);
                PushNotificationUtil.sendNotification(pushNotification, userDevice.getFamily());
            }
        }else{
            log.warn("Missing information of user device");
        }
    }

    public static void sendNotification(PushNotification pushNotification, String deviceFamily) {
        if (deviceFamily.equalsIgnoreCase("IOS") || deviceFamily.equalsIgnoreCase("MAC_OS_X") || deviceFamily.equalsIgnoreCase("UNKNOWN"))//send to ios
            sendNotificationToiOSDevice(pushNotification);
        else//send to android
            sendNotificationToAndroidDevice(pushNotification);
    }

    public static void sendNotificationToAndroidDevice(PushNotification pushNotification) {

        log.info("Sending push notification to Android Device: " + pushNotification.getTokens());
        try {
            if(pushNotification.getTokens().size()  > 0){
                Sender sender = null;
                int retries = 3;
                if (pushNotification.getNotifyTo().equals(NotifyTo.DRIVER) || pushNotification.getNotifyTo().equals(NotifyTo.FASTR_DRIVER) || pushNotification.getNotifyTo().equals(NotifyTo.RESTAURANT_DRIVER)) {
                    sender = new Sender(ANDROID_DRIVER_API_KEY);
                } else if(pushNotification.getNotifyTo().equals(NotifyTo.STORE_MANAGER)){
                    sender = new Sender(ANDROID_SM_API_KEY);
                }
                //sender = new Sender(ANDROID_SM_API_KEY);

                Message.Builder messageBuilder = new Message.Builder();
                messageBuilder.collapseKey("1");
                messageBuilder.timeToLive(3);
                messageBuilder.delayWhileIdle(false);
                for (Map.Entry<String, String> entry : pushNotification.getMessage().entrySet())
                {
                    messageBuilder.addData(entry.getKey(), entry.getValue());
                }
                Message message = messageBuilder.build();
                MulticastResult result = sender.send(message, pushNotification.getTokens(), retries);


                log.info("Message:"+pushNotification.getMessage().toString()+" Result:"+result.toString());
                if (result.getResults() != null) {
                    int canonicalRegId = result.getCanonicalIds();
                    if (canonicalRegId != 0) {
                    }
                } else {
                    int error = result.getFailure();
                    log.error("failed push notification ids" + error);
                }
            }
        } catch (Exception e) {
            log.error("Error occurred while sending push notification to android device ", e);
        }
    }

    private static void sendNotificationToiOSDevice(PushNotification pushNotification) {
        try {
            log.info("Sending push notification to iOS Device");
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            PushNotificationRedirect pushNotificationRedirect = pushNotification.getPushNotificationRedirect();

           /* String message = pushNotification.getMessage();

            String customMessage = "";
            if(pushNotification.getPushNotificationRedirect() != null){
                customMessage += pushNotification.getPushNotificationRedirect().toString();
            }
            if (pushNotification.getExtraDetail() != null)
                customMessage += "/" + pushNotification.getExtraDetail();*/

            String IOS_Certificate = CERTIFICATE_IOS;
            String IOS_password = PASSWORD_IOS;

            InputStream inputStream = classLoader.getResourceAsStream(IOS_Certificate);
            PayloadBuilder payloadBuilder = APNS.newPayload();
           /* payloadBuilder = payloadBuilder.badge(pushNotification.getBadge());*/
            payloadBuilder = payloadBuilder.sound("default");
            payloadBuilder = payloadBuilder.alertBody(pushNotification.getMessage().get("message"));
            for (Map.Entry<String, String> entry : pushNotification.getMessage().entrySet())
            {
                if (entry.getKey()!="message")
                    payloadBuilder = payloadBuilder.customField(entry.getKey(),entry.getValue());
            }
            String payload = payloadBuilder.build();

            ApnsService service = APNS.newService().withCert(inputStream, IOS_password).withProductionDestination().build();
            service.push(pushNotification.getTokens(), payload);
            log.info("device token list:"+pushNotification.getTokens().toString());
            log.info("Notification sent successfully");
        } catch (Exception e) {
            log.error("Error occurred while sending push notification to ios device ", e);
        }
    }

}
