package com.yetistep.anyorder.util;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.enums.UserRole;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 11/19/14
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class GeneralUtil {
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String VERIFICATION_CODE = "verificationCode";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String ADDRESS_ID = "addressId";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String MERCHANT_ID = "merchantId";
    public static final String FACEBOOK_ID = "facebookId";
    public static final String BRAND_ID = "brandId";
    public static final String STORE_MANAGER_ID = "storeManagerId";
    public static final String CART_ID = "cartId";
    public static final String ORDER_ID = "orderId";
    public static final String STORE_ID = "storeId";
    public static final String DRIVER_ID = "driverId";
    public static final String AREA_ID = "areaId";
    public static final String CUSTOMER_AREA_ID = "customerAreaId";
    public static final String API_KEY_ID = "apiKeyId";

    private static Logger log = Logger.getLogger(GeneralUtil.class);
    @Autowired
    HttpServletRequest httpServletRequest;

    public static void logError(Logger log, String message, Exception e) {
        if (e instanceof YSException)
            log.info(e.getMessage());
        else
            log.error(message, e);
    }

    public static void wait(int millisecond) {
        try {
            Thread.sleep(millisecond);
        } catch (InterruptedException e) {
            log.error("Error occurred while holding a thread", e);
        }
    }

    public static String saveImageToBucket(String encodedString, String imageName, String dir, boolean doCompress) throws Exception {
        if (encodedString == null || encodedString.isEmpty())
            throw new YSException("VLD008");

        String imgType = encodedString.split(";")[0].split("/")[1];
        String tmpDir = System.getProperty("catalina.home") + File.separator + "temp";
        File tmpImg = new File(tmpDir, imageName + "." + imgType);

        //decode the string and save the image
        ImageConverter.decodeToImage(encodedString, tmpImg);
        log.info("Image Saved locally in: " + tmpImg.getPath());

        String imgUrl = null;

        if (doCompress) {
            String compressImgName = imageName;
            ImageConverter.compressImage(tmpImg, imgType, compressImgName, 0.7f);

            compressImgName += "." + imgType;
            File compressedFile = new File(tmpDir + File.separator + compressImgName);
            log.info("Compressed and saved in " + compressedFile.getPath());

            //upload compressed image to the bucket
            log.info("Uploading coupon compress image to bucket");
            String compressUrl = AmazonUtil.uploadFileToS3(compressedFile, dir, compressedFile.getName(), true);
            imgUrl = AmazonUtil.cacheImage(compressUrl);
            return imgUrl;
        } else {
            //upload the original image to bucket
            String s3Url = AmazonUtil.uploadFileToS3(tmpImg, dir, tmpImg.getName(), true);
            return imgUrl == null ? AmazonUtil.cacheImage(s3Url) : imgUrl;
        }

    }

    /**
     * Util method to get data from HttpHeaders
     *
     * @param headers
     * @param headerDto
     */
    public static void fillHeaderCredential(HttpHeaders headers, HeaderDto headerDto, String... expectedFields) throws Exception {
        for (String field : expectedFields) {
            if (field.equals(USERNAME)) {
                headerDto.setUsername(extractHeader(headers, USERNAME));
            } else if (field.equals(PASSWORD)) {
                headerDto.setPassword(extractHeader(headers, PASSWORD));
            } else if (field.equals(VERIFICATION_CODE)) {
                headerDto.setVerificationCode(extractHeader(headers, VERIFICATION_CODE));
            } else if (field.equals(NEW_PASSWORD)) {
                headerDto.setNewPassword(extractHeader(headers, NEW_PASSWORD));
            } else if (field.equals(ID)) {
                headerDto.setId(extractHeader(headers, ID));
            } else if (field.equals(USER_ID)) {
                headerDto.setUserId(extractHeader(headers, USER_ID));
            } else if (field.equals(API_KEY_ID)) {
                headerDto.setApiKeyId(extractHeader(headers, API_KEY_ID));
            } else if (field.equals(BRAND_ID)) {
                if(headers.get(BRAND_ID) != null)
                    headerDto.setBrandId(extractHeader(headers, BRAND_ID));
            } else if (field.equals(STORE_ID)) {
                if(headers.get(STORE_ID) != null)
                    headerDto.setStoreId(extractHeader(headers, STORE_ID));
            } else if (field.equals(CART_ID)) {
                if(headers.get(CART_ID) != null)
                    headerDto.setCartId(extractHeader(headers, CART_ID));
            } else if (field.equals(ORDER_ID)) {
                if(headers.get(ORDER_ID) != null)
                    headerDto.setOrderId(extractHeader(headers, ORDER_ID));
            } else if (field.equals(ADDRESS_ID)) {
                headerDto.setAddressId(extractHeader(headers, ADDRESS_ID));
            } else if (field.equals(ACCESS_TOKEN)) {
                if(headers.get(ACCESS_TOKEN) != null)
                headerDto.setAccessToken(extractHeader(headers, ACCESS_TOKEN));
            } else if (field.equals(FACEBOOK_ID)) {
                headerDto.setFacebookId(Long.parseLong(extractHeader(headers, FACEBOOK_ID)));
            } else if (field.equals(DRIVER_ID)) {
                if (headers.get(DRIVER_ID) != null)
                    headerDto.setDriverId(extractHeader(headers, DRIVER_ID));
            } else if (field.equals(CUSTOMER_AREA_ID)) {
                if (headers.get(CUSTOMER_AREA_ID) != null)
                    headerDto.setStoreManagerId(extractHeader(headers, CUSTOMER_AREA_ID));
            } else if (field.equals(AREA_ID)) {
                if (headers.get(AREA_ID) != null)
                    headerDto.setAreaId(extractHeader(headers, AREA_ID));
            } else if(field.equals(STORE_MANAGER_ID)){
                if(headers.get(STORE_MANAGER_ID) != null)
                    headerDto.setStoreManagerId(extractHeader(headers, STORE_MANAGER_ID));
            } else if (field.equals(MERCHANT_ID)) {
                if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser") && SessionManager.getRole().equals(UserRole.ROLE_MERCHANT)) {
                    headerDto.setMerchantId(SessionManager.getMerchantId());
                } else {
                    headerDto.setMerchantId(extractHeader(headers, MERCHANT_ID));
                }
            }
        }
    }

    public static String extractHeader(HttpHeaders headers, String field) throws Exception {
        List<String> hd = headers.get(field);
        if (hd != null && hd.size() > 0)
            return hd.get(0);
        else
            throw new YSException("VLD001");
    }

    /**
     * Util method to encrypt the password.
     *
     * @param password
     * @return
     */
    public static String encryptPassword(String password) {
        if (password != null && !password.isEmpty()) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            return passwordEncoder.encode(password);
        }
        return null;
    }

    public static Boolean matchDBPassword(String rawPassword, String dbEncryptedPassword) throws Exception {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        log.info(passwordEncoder.encode(rawPassword));
        if (!passwordEncoder.matches(rawPassword, dbEncryptedPassword))
            throw new YSException("PASS002");

        return true;
    }


    public static String generateAccessToken(String osFamily) throws Exception {
        String tokenstr = null;
        log.info(" Mobile Family ====== " + osFamily);
        if (osFamily.toUpperCase().indexOf("IOS") >= 0 || osFamily.toUpperCase().indexOf("MAC") >= 0 || osFamily.toUpperCase().indexOf("UNKNOWN") >= 0) {
            tokenstr = RNCryptoEncDec.generateResponseAccessToken();
        } else {
            tokenstr = EncDecUtil.generateResponseAccessToken(MessageBundle.getSecretKey());
        }
        return tokenstr;
    }

    /*   Generates 4 digit code from 1000 to 9999  */
    public static String generateMobileCode() {
        int code = (int) (Math.random() * 9000 + 1000);
        return String.valueOf(code);
    }

    /*public static PreferenceType getTimeTakenFor(VehicleType vehicleType){
        PreferenceType preferenceType = null;
        if(VehicleType.BICYCLE.equals(vehicleType)){
            preferenceType = PreferenceType.TIME_TO_TRAVEL_ONE_KM_ON_BICYCLE;
        }else if(VehicleType.ON_FOOT.equals(vehicleType)){
            preferenceType = PreferenceType.TIME_TO_TRAVEL_ONE_KM_ON_FOOT;
        }else if(VehicleType.MOTORBIKE.equals(vehicleType)){
            preferenceType = PreferenceType.TIME_TO_TRAVEL_ONE_KM_ON_MOTORBIKE;
        }else if(VehicleType.CAR.equals(vehicleType)){
            preferenceType = PreferenceType.TIME_TO_TRAVEL_ONE_KM_ON_CAR;
        }else if(VehicleType.TRUCK.equals(vehicleType)){
            preferenceType = PreferenceType.TIME_TO_TRAVEL_ONE_KM_ON_TRUCK;
        }else if(VehicleType.OTHERS.equals(vehicleType)){
            preferenceType = PreferenceType.TIME_TO_TRAVEL_ONE_KM_ON_OTHERS;
        }
        return preferenceType;
    }*/

    public static HttpHeaders getCacheHeader() {
        Integer inSec = 0;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Cache-Control", "max-age=" + inSec);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }


    public static Integer getMinimumTimeDisplay(Integer requiredTime) {
        if (requiredTime < 1) {
            return 1;
        }
        return requiredTime;
    }

    public static Integer ifNullToZero(Integer value) {
        if (value == null) {
            return 0;
        }
        return value;
    }

    public static Boolean parseBoolean(String value) {
        boolean returnValue = false;
        if ("1".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value) ||
                "true".equalsIgnoreCase(value) || "on".equalsIgnoreCase(value))
            returnValue = true;
        return returnValue;
    }

    public static Properties parsePropertiesString(String data, String separator) throws IOException {
        String parsedData = data.replace(separator, "\n");
        final Properties prop = new Properties();
        prop.load(new StringReader(parsedData));
        return prop;
    }

    public static String generatePromoCode(String generateFrom) {

        String promoCode;

        String initial = "";
        String ending = generateFrom.substring(0, 1);
        if (generateFrom.length() < 4) {
            initial += generateFrom;
            for (int i = 0; i < (4 - generateFrom.length()); i++) {
                initial += "0";
            }
        } else {
            initial = generateFrom.substring(0, 4).contains(" ") ? generateFrom.substring(0, 4).replace(" ", "0") : generateFrom.substring(0, 4);
        }

        String randString;
        //Generating random number between 1 and 99999
        int min = 1;
        int max = 99999;
        Random random = new Random();
        int randomNum = random.nextInt(max - min + 1) + min;

        randString = randomNum + "";

        if (randString.length() < 5) {
            String pre = "";
            for (int i = 0; i < (5 - randString.length()); i++) {
                pre += "0";
            }
            randString = pre + randString;
        }

        promoCode = initial.toUpperCase() + randString + ending.toUpperCase();

        return promoCode;
    }

    public static Boolean checkPromoCodeFormat(String generatedFrom, String promoCode) {

        String initial = "";
        String ending = generatedFrom.substring(0, 1);
        if (generatedFrom.length() < 4) {
            initial += generatedFrom;
            for (int i = 0; i < (4 - generatedFrom.length()); i++) {
                initial += "0";
            }
        } else {
            initial = generatedFrom.substring(0, 4).contains(" ") ? generatedFrom.substring(0, 4).replace(" ", "0") : generatedFrom.substring(0, 4);
        }

        String regexPattern = "^" + initial.toUpperCase() + "\\d\\d\\d\\d\\d" + ending.toUpperCase() + "$";

        Pattern pattern = Pattern.compile(regexPattern);

        Matcher matcher = pattern.matcher(promoCode);

        if (matcher.find()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public static Boolean isNumeric(String numStr) {
        try {
            Integer.parseInt(numStr);
            return Boolean.TRUE;
        } catch (NumberFormatException e) {
            return Boolean.FALSE;
        }
    }
}
