package com.yetistep.anyorder.util;

import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.AuthenticatedUser;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 12/16/14
 * Time: 10:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class SessionManager {
    //private static final String BLANK = "";

    public static AuthenticatedUser getPrincipal() {
        return (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static String getUserName() {
        return getPrincipal().getUsername();
    }

    public static UserRole getRole() {
        //String role = BLANK;
        Iterator iterator = getPrincipal().getAuthorities().iterator();
        //for (Iterator iterator = getPrincipal().getAuthorities().iterator(); iterator.hasNext(); ) {
        //Currently Just One UserRole Assigned
        String role = String.valueOf(iterator.next());
            //break;
        //}
        return UserRole.valueOf(role);
    }

    public static String getFirstName() {
        return getPrincipal().getFirstName();
    }

    public static String getLastName() {
        return getPrincipal().getLastName();
    }

    public static String getMobileNumber() {
        return getPrincipal().getMobileNumber();
    }

    public static String getMerchantId() {
        return getPrincipal().getMerchantId();
    }

    public static String getStoreManagerId() {
        return getPrincipal().getStoreManagerId();
    }

    public static Integer getUserId() {
        return getPrincipal().getUserId();

    }

    public static String getFakeUserId() {
        return getPrincipal().getFakeUserId();

    }

    public static boolean isAnonymousUser(){
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser");
    }

}
