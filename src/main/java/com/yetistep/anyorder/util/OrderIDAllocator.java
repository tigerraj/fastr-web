package com.yetistep.anyorder.util;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/13/16
 * Time: 4:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class OrderIDAllocator {

    private static final long LIMIT = 1000000L;
    private static long last = 0;

    public static String getID() {
        // 10 digits.
        long id = System.currentTimeMillis() % LIMIT;
        if ( id <= last ) {
            id = (last + 1) % LIMIT;
        }
        last = id;

        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.getCurrentDate());
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return year+""+month+""+last+""+day;
    }

}
