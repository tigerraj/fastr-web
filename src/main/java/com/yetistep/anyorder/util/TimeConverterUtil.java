package com.yetistep.anyorder.util;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by suroz on 1/23/2017.
 */
public class TimeConverterUtil implements Converter {
    private SimpleDateFormat formatter = new SimpleDateFormat(
            "yyyy-mm-dd'T'HH:mm:ss");
    private SimpleDateFormat formatter1 = new SimpleDateFormat(
            "yyyy-mm-dd HH:mm:ss");

    public boolean canConvert(Class clazz) {
        // This converter is only for Date fields.
        return Timestamp.class.isAssignableFrom(clazz);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
        Timestamp timestamp = (Timestamp) value;
        writer.setValue(formatter.format(timestamp));
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        GregorianCalendar calendar = new GregorianCalendar();
        try {
            calendar.setTime(formatter.parse(reader.getValue()));
        } catch (ParseException e) {
            try {
                calendar.setTime(formatter1.parse(reader.getValue()));
            } catch (ParseException e1) {
                throw new ConversionException(e.getMessage(), e);
            }

        }
        return calendar.getTime();
    }
}