package com.yetistep.anyorder.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by root on 8/4/16.
 */
public class NumberFormaterUtil {

    private final static String americanMoneyPattern = "###,##0.00";

    public static String americanStandard(BigDecimal number, String currencySymbol) {
        DecimalFormat decimalFormat = new DecimalFormat(americanMoneyPattern);
        return currencySymbol + " " + decimalFormat.format(number);
    }

}
