package com.yetistep.anyorder.util;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

import java.lang.reflect.Field;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 12/30/14
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class ValidationUtil {

    public static boolean validateString(String value, String type) {
        if (value==null || value.isEmpty()) {
            throw new YSException("VLD003", type);
        }
        return true;
    }

    public static <T> T initializeAndUnproxy(T entity) {
        if (entity == null) {
            throw new
                    NullPointerException("Entity passed for initialization is null");
        }

        Hibernate.initialize(entity);
        if (entity instanceof HibernateProxy) {
            entity = (T) ((HibernateProxy) entity).getHibernateLazyInitializer()
                    .getImplementation();
        }
        return entity;
    }

    /*public static <T> T initializeAndUnproxy(T entity) throws IllegalArgumentException, IllegalAccessException
    {

        if(entity == null)
        {
            throw new NullPointerException("Entity passed for initialization is null");
        }

        Hibernate.initialize(entity);
        T ret = entity;

        if(entity instanceof HibernateProxy)
        {
            ret = (T)((HibernateProxy)entity).getHibernateLazyInitializer().getImplementation();
            initializeRecursively(ret);
        }

        return ret;
    }

    public static void initializeRecursively(Object entity) throws IllegalArgumentException, IllegalAccessException
    {
        Class<?> clazz = entity.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for(Field field : fields)
        {
            field.setAccessible(true);
            Object obj = field.get(entity);
            Hibernate.initialize(obj);

            if(obj instanceof HibernateProxy)
            {
                obj = ((HibernateProxy)obj).getHibernateLazyInitializer().getImplementation();
                field.set(entity, obj);
                initializeRecursively(obj);
            }
            if(obj instanceof LazyInitializer)
            {
                obj = ((LazyInitializer)obj).getImplementation();
                initializeRecursively(obj);
            }
        }
    }*/


}
