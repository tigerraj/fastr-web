package com.yetistep.anyorder.util;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;
import com.yetistep.anyorder.model.xml.APIData;

/**
 * Created by suroz on 1/19/2017.
 */
public class MyInterfaceConverter extends ReflectionConverter {
    public MyInterfaceConverter(Mapper mapper, ReflectionProvider reflectionProvider) {
        super(mapper, reflectionProvider);
    }

    @Override
    public void marshal(Object original, final HierarchicalStreamWriter writer, final MarshallingContext context) {
        writer.addAttribute("class", original.getClass().getCanonicalName());
        super.marshal(original, writer, context);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class type) {
        return APIData.class.isAssignableFrom(type);
    }

    /*@Override
    protected XStream constructXStream() {
        // The referenced XStream constructor has been deprecated as of 1.4.5.
        // We're preserving this call for broader XStream 1.4.x compatibility.
        return new XStream() {
            @Override
            protected MapperWrapper wrapMapper(MapperWrapper next) {
                return new MapperWrapper(next) {
                    @Override
                    public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                        if (definedIn == Object.class) {
                            return false;
                        }
                        return super.shouldSerializeMember(definedIn, fieldName);
                    }
                };
            }
        };
    }
    XStream sxstream = new XStream(new StaxDriver()) {
        @Override
        protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                @Override
                public boolean shouldSerializeMember(Class definedIn,
                                                     String fieldName) {
                    if (definedIn == Object.class) {
                        return false;
                    }
                    return super.shouldSerializeMember(definedIn, fieldName);
                }
            };
        }
    };


    XStream xstream = new XStream(new DomDriver()) {
        protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                    try {
                        return definedIn != Object.class || realClass(fieldName) != null;
                    } catch(CannotResolveClassException cnrce) {
                        return false;
                    }
                }
            };
        }
    };*/

    /*@Override
    protected void configureXStream(final XStream xstream) {
        super.configureXStream(xstream);
        xstream.ignoreUnknownElements(); // will it blend?
    }*/
}
