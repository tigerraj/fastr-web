package com.yetistep.anyorder.util;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.*;
import com.yetistep.anyorder.service.impl.SystemPropertyServiceImpl;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: surendraJ
 * Date: 12/17/14
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class GeoCodingUtil {

    //private static final String GOOGLE_MAP_API_KEY = "AIzaSyCdr9eotT5ocnlRRhQoDxmZqYlN-Bbk5aE";
    //private static final String GOOGLE_MAP_API_KEY = "AIzaSyBdHaY5LRnHvKdXZrK5WV57VAO46YlKRlg"; //old key
    //private static final String GOOGLE_MAP_API_KEY = "AIzaSyCvYPNNG60YMecVyfyh6L0BQxVhrhvDr0A";
    private static final Logger log = Logger.getLogger(GeoCodingUtil.class);
    //private static final String GOOGLE_MAP_API_KEY = "AIzaSyBa68akSPX0p3EDcYeR4VhRtNarocbkg-8";

    //Return In Miter
    public static Float getDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;

        int meterConversion = 1609;

        return new Float(dist * meterConversion).floatValue();
    }


    public static Float getDistanceFromLatLonInKm(String lat1, String lng1, String lat2, String lng2) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(Double.parseDouble(lat2) - Double.parseDouble(lat1));
        double dLng = Math.toRadians(Double.parseDouble(lng2) - Double.parseDouble(lng1));
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(Double.parseDouble(lat1))) * Math.cos(Math.toRadians(Double.parseDouble(lat2))) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;

        int kmConversion = 1609 / 1000;
        return new Float(dist * kmConversion).floatValue();
    }

    public static double deg2rad(double deg) {
        return deg * (Math.PI / 180);
    }

    public static List<BigDecimal> getListOfAirDistances(String[] origin, String destination[]) throws Exception {
        List<BigDecimal> distanceList = new ArrayList<BigDecimal>();
        double lat1 = Double.parseDouble(origin[0].split(",")[0]);
        double lng1 = Double.parseDouble(origin[0].split(",")[1]);
        for (int i = 0; i < destination.length; i++) {
            double lat2 = Double.parseDouble(destination[i].split(",")[0]);
            double lng2 = Double.parseDouble(destination[i].split(",")[1]);
            Float distance = getDistance(lat1, lng1, lat2, lng2);
            distanceList.add(new BigDecimal(distance));
        }
        return distanceList;
    }

    public static GeoApiContext getGeoApiContext() {
        return new GeoApiContext().setApiKey(MessageBundle.getGoogleMapApiKey());
    }


    /**
     * This method return list of distances based on origin and destination addresses.
     * Example:
     * String origin[] = [{27.2133,85.2323}];
     * String destination[] = [{27.9133,85.4323},{27.5333,85.7723}];
     * List<BigDecimal> distanceList = getListOfDistances(origin, destination);
     *
     * @param origin      Takes array of String as parameter which contains combination of both
     *                    latitude and longitude of origin address.
     * @param destination Takes array of String as parameter which contains combination of both
     *                    latitude and longitude of origin address.
     * @return list of distances BigDecimal values.
     * @throws Exception
     */
    public static Map<String, Long> getListOfDistances(String[] origin, String destination[], Integer timePerKtm) throws Exception {
        Map<String, Long> distanceAndTime = new HashMap<>();
        GeoApiContext context = GeoCodingUtil.getGeoApiContext();
        // DistanceMatrix distanceMatrix = DistanceMatrixApi.getDistanceMatrix(context, origin, destination).await();
        context.setQueryRateLimit(2);
        DistanceMatrix distanceMatrix = DistanceMatrixApi.getDistanceMatrix(context, origin, destination).departureTime(new DateTime(System.currentTimeMillis()).toMutableDateTime()).mode(TravelMode.DRIVING).trafficModel(TrafficModel.BEST_GUESS).await();
        int oTracker = 0, dTracker = 0;
        for (DistanceMatrixRow distanceMatrixRow : distanceMatrix.rows) {
            dTracker = 0;
            for (DistanceMatrixElement element : distanceMatrixRow.elements) {
                if (element.status.equals(DistanceMatrixElementStatus.OK)) {
                    distanceAndTime.put("time", element.durationInTraffic.inSeconds);
                    distanceAndTime.put("distance", element.distance.inMeters);
                } else {
                    log.info("Error in Latitude-Longitude on Origin:" + origin[oTracker] + " Destination:" + destination[dTracker]);
                    distanceAndTime.put("distance", getAssumedDistance(origin[oTracker], destination[dTracker]).longValue());
                    long time = (getAssumedDistance(origin[oTracker], destination[dTracker]).multiply(new BigDecimal(timePerKtm / 1000))).longValue();
                    distanceAndTime.put("time", time);
                }
                dTracker++;
            }
            oTracker++;
        }
        return distanceAndTime;
    }

    public static String getLatLong(String latitude, String longitude) {
        if (latitude == null || latitude.isEmpty() || longitude == null || longitude.isEmpty()) {
            throw new YSException("VLD015");
        }
        return latitude + "," + longitude;
    }

    public static BigDecimal getAssumedDistance(String origin, String destination) throws Exception {
        SystemPropertyService systemPropertyService = new SystemPropertyServiceImpl();
        BigDecimal distanceFactor = new BigDecimal(1.6);
        double lat1 = Double.parseDouble(origin.split(",")[0]);
        double lng1 = Double.parseDouble(origin.split(",")[1]);
        double lat2 = Double.parseDouble(destination.split(",")[0]);
        double lng2 = Double.parseDouble(destination.split(",")[1]);
        BigDecimal distanceInMeters = new BigDecimal(getDistance(lat1, lng1, lat2, lng2));
        return distanceFactor.multiply(distanceInMeters);
    }

    public static List<BigDecimal> getListOfAssumedDistance(String origin, String destination[]) throws Exception {
        List<BigDecimal> distanceList = new ArrayList<BigDecimal>();
        for (int i = 0; i < destination.length; i++) {
            distanceList.add(getAssumedDistance(origin, destination[i]));
        }
        return distanceList;
    }

    /*public static Integer getRequiredTime(String origin, String destination, VehicleType vehicleType) throws Exception{
        SystemPropertyService systemPropertyService = new SystemPropertyServiceImpl();
        DistanceType distanceType = DistanceType.fromInt(Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)));
        BigDecimal calculatedDistance = BigDecimal.ZERO;
        if(distanceType.equals(DistanceType.AIR_DISTANCE)){
            calculatedDistance = BigDecimalUtil.getDistanceInKiloMeters(getAssumedDistance(origin, destination));
        }else {
             List<BigDecimal> distances = getListOfDistances(new String[] {origin}, new String[]{destination});
            if(distances.size() > 0){
                calculatedDistance = BigDecimalUtil.getDistanceInKiloMeters(distances.get(0));
            }
        }
        int timeFactor = Integer.parseInt(systemPropertyService.readPrefValue(GeneralUtil.getTimeTakenFor(vehicleType)));
        BigDecimal requiredTime = calculatedDistance.multiply(new BigDecimal(timeFactor));
        return requiredTime.intValue();
    }*/

    public static Map<String, Long> calculateDistance(String lat1, String long1, String lat2, String long2, Integer timePerKm) throws Exception {
        /*String url = "https://maps.googleapis.com/maps/api/distancematrix/json";
        String origin = "?origins=" + lat1 + "," + long1;
        String unit = "&units=imperial";  //convery distnace in mile
        String destination = "&destinations=" + lat2 + "," + long2;
        String key = "&key=" + GOOGLE_MAPS_API_KEY;
        url = url + origin + destination + key;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        Long distance = new Long(0);
        Long duration = new Long(0);
        Map<String, Long> data = new HashMap<>();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            // Convert to a JSON object to print data
            JsonParser jp = new JsonParser(); //from gson
            JsonElement root = jp.parse(new InputStreamReader((InputStream) con.getContent())); //Convert the input stream to a json element
            JsonObject rootobj = root.getAsJsonObject();
            String status = rootobj.get("status").toString();
            if (status.contains("OK")) {
                JsonArray rows = (JsonArray) rootobj.get("rows");
                JsonObject row = (JsonObject) rows.get(0);
                JsonArray elements = (JsonArray) row.get("elements");
                JsonObject element = (JsonObject) elements.get(0);
                String status1 = element.get("status").toString();
                if (status1.contains("OK")) {
                    JsonObject distances = (JsonObject) element.get("distance");
                    distance = Long.valueOf(distances.get("value").toString());
                    JsonObject durations = (JsonObject) element.get("duration");
                    duration = Long.valueOf(durations.get("value").toString());
                } else
                    throw new YSException("REQ002");
            }
        } else {
            throw new YSException("REQ001");
        }
        data.put("distance", distance);
        data.put("duration", duration);
        return data;*/
        String fromAddress[] = {GeoCodingUtil.getLatLong(lat1, long1)};
        String toAddress[] = {GeoCodingUtil.getLatLong(lat2, long2)};
        Map<String, Long> distanceAndTime = GeoCodingUtil.getListOfDistances(fromAddress, toAddress, timePerKm);
        Map<String, Long> data = new HashMap<>();
        data.put("distance", distanceAndTime.get("distance"));
        data.put("duration", distanceAndTime.get("time"));
        return data;
    }

    public static LatLng getLatLongFromStreet(String streetAddress) {
        final String key = "AIzaSyDRyQc5BpKjn3c-ZSLFSaELDGphRxMpj9U";// MessageBundle.getGoogleMapApiKey();
//
        GeoApiContext context=new GeoApiContext();
        context.setApiKey(key);

        GeocodingApiRequest request=GeocodingApi.newRequest(context);
        request.address(streetAddress).components(ComponentFilter.country("AE"));
        try {
            final GeocodingResult[] result=request.await();
            if(result.length>0){
                return result[0].geometry.location;
            }else{
                throw new YSException("API008");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
