package com.yetistep.anyorder.util;

/**
 * Created by suroz on 1/24/2017.
 */

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

public class HibernateProxyConverter implements Converter {
    public boolean canConvert(final Class clazz) {
        // be responsible for Hibernate proxy.
        return HibernateProxy.class.isAssignableFrom(clazz);
    }

    public void marshal(final Object object, final HierarchicalStreamWriter writer,
                        final MarshallingContext context) {
        final HibernateProxy item = ((HibernateProxy) object);
        Hibernate.initialize(object);
        if (object instanceof HibernateProxy) {
            ((HibernateProxy) object).getHibernateLazyInitializer().getImplementation();
        }

        context.convertAnother(item);


//        APIData obj=(APIData)object;
//        writer.startNode("store");
//        writer.setValue(obj.getApiKey());
//        writer.endNode();

    }

    public Object unmarshal(final HierarchicalStreamReader reader,
                            final UnmarshallingContext context) {
        throw new ConversionException("Cannot deserialize Hibernate proxy");

    }
}
