package com.yetistep.anyorder.util;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/4/16
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class RandomValueFieldPopulator {

    public static Random random = new Random();
    public static List<String> fieldsToPopulate = new ArrayList<>();

    public static void populate(Object object, String... fields) {
        for (String field: fields)
            fieldsToPopulate.add(field);
        ReflectionUtils.doWithFields(object.getClass(), new RandomValueFieldSetterCallback(object));
        fieldsToPopulate.clear();
    }

    private static class RandomValueFieldSetterCallback implements ReflectionUtils.FieldCallback {
        private Object targetObject;

        public RandomValueFieldSetterCallback(Object targetObject) {
            this.targetObject = targetObject;
        }

        @Override
        public void doWith(Field field) throws IllegalAccessException {
            if(fieldsToPopulate.size()>0){
                if(fieldsToPopulate.contains(field.getName())){
                    Class<?> fieldType = field.getType();
                    if (!Modifier.isFinal(field.getModifiers())) {
                        Object value = generateRandomValue(fieldType/*, new WarnOnCantGenerateValueHandler(field)*/);
                        if (value!=null && !value.equals("")) {
                            ReflectionUtils.makeAccessible(field);
                            field.set(targetObject, value);
                        }
                    }
                }
            }else {
                Class<?> fieldType = field.getType();
                if (!Modifier.isFinal(field.getModifiers())) {
                    Object value = generateRandomValue(fieldType/*, new WarnOnCantGenerateValueHandler(field)*/);
                    if (value!=null && !value.equals("")) {
                        ReflectionUtils.makeAccessible(field);
                        field.set(targetObject, value);
                    }
                }
            }
        }
    }

    public static Object generateRandomValue(Class<?> fieldType/*, CantGenerateValueHandler cantGenerateValueHandler*/) {
        if (fieldType.equals(String.class)) {
            return UUID.randomUUID().toString();
        } else if (Date.class.isAssignableFrom(fieldType) && !fieldType.getSimpleName().equals("Timestamp")) {
            return new Date(System.currentTimeMillis() - random.nextInt(1));
        } else if (Timestamp.class.isAssignableFrom(fieldType)) {
            return new Timestamp(System.currentTimeMillis());
        }else if (fieldType.equals(BigDecimal.class)) {
            return new BigDecimal(Math.random());
        } else if (Number.class.isAssignableFrom(fieldType)) {
            return random.nextInt(Byte.MAX_VALUE) + 1;
        } else if (fieldType.equals(Integer.TYPE)) {
            return random.nextInt();
        } else if (fieldType.equals(Long.TYPE)) {
            return random.nextInt();
        } else if (Enum.class.isAssignableFrom(fieldType)) {
            Object[] enumValues = fieldType.getEnumConstants();
            return enumValues[random.nextInt(enumValues.length)];
        } else {
            //return cantGenerateValueHandler.handle();
            //throw new YSException("unable to populate value for field");
            return null;
        }
    }

}
