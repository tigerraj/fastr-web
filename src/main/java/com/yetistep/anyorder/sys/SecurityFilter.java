package com.yetistep.anyorder.sys;

import org.springframework.stereotype.Repository;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException, UnsupportedEncodingException{
        MyRequestWrapper myRequestWrapper = new MyRequestWrapper((HttpServletRequest) servletRequest);
        String body = myRequestWrapper.getBody();
        Boolean isBad = isBad(body);
        //To change body of implemented methods use File | Settings | File Templates.
        if (!isBad) {
            filterChain.doFilter(myRequestWrapper, servletResponse);
        } else {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.setHeader("validity", "XSS invalidation failed.");
            response.sendRedirect(request.getHeader("referer"));
        }
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private Boolean isBad(String body) throws UnsupportedEncodingException{
        Boolean isBadString = Boolean.FALSE;
        String[] badStrings = new String[]{"<script>","<img","javascript","onmouseover","onerror","onload","iframe","vbscript","<style"};
        String convertedFrontEndData = URLDecoder.decode(body, "UTF-8").toLowerCase();
        for (String badString : badStrings) {
            if (convertedFrontEndData.contains(badString)) {
                isBadString = Boolean.TRUE;
                break;
            }
        }
        return isBadString;
    }
}
