package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.dto.ResponseJsonDto;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.model.xml.APIData;

import java.util.List;
import java.util.Map;

/**
 * Created by BinaySingh on 5/18/2016.
 */
public interface StoreManagerWebService {

    public void testData(APIData xmlData) throws Exception;

    public void saveStoreManager(StoreManagerEntity storeManager) throws Exception;

    public void updateStoreManager(StoreManagerEntity storeManager) throws Exception;

    public List<AreaEntity> getAreas() throws Exception;

    public List<AreaEntity> getActiveAreas() throws Exception;

    public AreaEntity getArea(String areaId) throws Exception;

    public ResponseJsonDto createOrder(HeaderDto headerDto, RequestJsonDto requestJson) throws Exception;

    public CustomerEntity getCustomersInfo(String storeId, RequestJsonDto requestJsonDto) throws Exception;

    public StoreEntity getSpecialPrivilegeInfo(HeaderDto headerDto) throws Exception;

    public List<DriverEntity> getStoresDrivers(HeaderDto headerDto) throws Exception;

    public OrderEntity getOrderDetail(HeaderDto headerDto) throws Exception;

    public List<StoreEntity> getAllStoreList() throws Exception;

    public ResponseJsonDto assignDriverToOrder(HeaderDto headerDto) throws Exception;

    public List<DriverEntity> getAnyOrderDriverList(HeaderDto headerDto) throws Exception;

    public RequestJsonDto getTotalNumberOfOrders(HeaderDto headerDto) throws Exception;

    public PaginationDto getOrdersByStatus(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception;

    public void changeStoreManagerStatus(StoreManagerEntity storeManager) throws Exception;

    public void updateStoresNotDriver(HeaderDto headerDto, List<DriverEntity> drivers) throws Exception;

    public List<DriverEntity> getDriversNotStore(HeaderDto headerDto) throws Exception;

    public List<DriverEntity> getAnyOrderDrivers() throws Exception;

    public List<StoreEntity> getStoreList(String userId) throws Exception;

    public OrderEntity estimateDeliveryCharge(String storeId, CustomersAreaEntity customersArea) throws Exception;

    public Map<String, PaginationDto> getDriversWithOrderCount(String userId, RequestJsonDto requestJson) throws Exception;

    public PaginationDto searchOrder(RequestJsonDto requestJsonDto) throws Exception;

    public void updateOrderStatus(HeaderDto headerDto, OrderEntity order) throws Exception;

    public List<AreaEntity> getStoreServingAreas(String storeId) throws Exception;

    public List<DriverEntity> getDriverPerformance(RequestJsonDto requestJsonDto) throws Exception;

    public List<DriverEntity> getDriverListForOrder(RequestJsonDto requestJsonDto) throws Exception;

    public List<StoreEntity> getStoreAllowingDriver(String userId) throws Exception;

    public void cancelOrder(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception;

    public List<StoreEntity> getStoreListByUserRole(String userId) throws Exception;

    public PaginationDto getOrderByStatus(RequestJsonDto requestJsonDto) throws Exception;

    public Map<String, Integer> getNumberOfOrderByStatus(RequestJsonDto requestJsonDto) throws Exception;

    public List<OrderEntity> getCurrentOrderOfDrivers(String driverId) throws Exception;

    public PaginationDto getDriversByStoreBrands(RequestJsonDto requestJsonDto) throws Exception;

    public void updateOrder(String orderId, OrderEntity order) throws Exception;

    Map<String, Integer> getOrderCountByDate(RequestJsonDto requestJsonDto) throws Exception;

    PaginationDto getStatementList(RequestJsonDto requestJsonDto) throws Exception;

    StatementEntity getStatement(String statementId) throws Exception;

    void updateStatement(StatementEntity statement) throws Exception;

    void generateStatement(StatementEntity statement) throws Exception;

    PaginationDto getOrderByDrivers(RequestJsonDto requestJsonDto) throws Exception;

    PaginationDto getStoreBrandListByMerchants(RequestJsonDto requestJsonDto) throws Exception;
}
