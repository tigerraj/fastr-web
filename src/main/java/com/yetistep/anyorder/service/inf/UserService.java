package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.enums.PasswordActionType;
import com.yetistep.anyorder.model.UserEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {

    void saveUser(UserEntity user) throws Exception;

    List<UserEntity> getAllUsers() throws Exception;

    Boolean checkUserExistence(HeaderDto headerDto) throws Exception;

    void changePassword(HeaderDto headerDto) throws Exception;

    String verifyEmailAddress(HeaderDto headerDto) throws Exception;

    String performPasswordAction(HeaderDto headerDto, PasswordActionType passwordActionType) throws Exception;

}
