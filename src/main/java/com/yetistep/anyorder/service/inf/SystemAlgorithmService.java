package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.OrderEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/31/16
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */
public interface SystemAlgorithmService {

    public List<DriverEntity> assignDriver(OrderEntity orderEntity, Boolean assignDriver) throws Exception;


}
