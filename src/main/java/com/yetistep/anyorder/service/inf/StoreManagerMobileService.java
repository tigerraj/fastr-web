package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:35 AM
 * To change this template use File | Settings | File Templates.
 */
public interface StoreManagerMobileService {

    UserEntity storeManagerLogin(HeaderDto headerDto, UserDeviceEntity userDevice) throws Exception;

    StoreEntity getSpecialPrivilegeInfo(String accessToken) throws Exception;

    OrderEntity createOrder(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception;

    List<UserEntity> getCustomersByMobileNumber(HeaderDto headerDto, RequestJsonDto requestJson) throws Exception;

    CustomerEntity getCustomersInfo(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception;

    List<AreaEntity> getArea(HeaderDto headerDto) throws Exception;

    List<DriverEntity> getStoresDrivers(HeaderDto headerDto) throws Exception;

    OrderEntity getOrderDetail(HeaderDto headerDto) throws Exception;

    OrderEntity assignDriverToOrder(HeaderDto headerDto) throws Exception;

    RequestJsonDto getTotalNumberOfOrders(HeaderDto headerDto) throws Exception;

    PaginationDto getStoresOrders(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception;

    OrderEntity updateOrderStatus(HeaderDto headerDto, OrderEntity order) throws Exception;

    PaginationDto searchOrder(HeaderDto headerDto, RequestJsonDto requestJson) throws Exception;

    List<DriverEntity> getEligibleDriverListForOrder(HeaderDto headerDto) throws Exception;

    OrderEntity estimateDeliveryCharge(HeaderDto headerDto, CustomersAreaEntity customersArea) throws Exception;

    void updateDeviceToken(HeaderDto headerDto, UserDeviceEntity userDevice) throws Exception;

    List<AreaEntity> getStoreServingAreas(HeaderDto headerDto) throws Exception;

    void cancelOrder(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception;

}
