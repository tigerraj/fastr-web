package com.yetistep.anyorder.service.inf;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

/**
 * Created by sagar on 8/27/2016.
 */
public interface WebRTCService {

    public void registerOpenConnection(WebSocketSession session) throws Exception;

    public void registerCloseConnection(WebSocketSession session, CloseStatus status) throws Exception;

    public void processMessage(WebSocketSession session, String message)  throws Exception;

    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception;

    public List<WebSocketSession> getSessions()  throws Exception;

}
