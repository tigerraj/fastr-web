package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MerchantService {

    public void saveStoresBrand(HeaderDto headerDto, StoreBrandEntity storeBrand) throws Exception;

    public void updateStoresBrand(HeaderDto headerDto, StoreBrandEntity storeBrand) throws Exception;

    public StoreBrandEntity getBrandDetails(HeaderDto headerDto) throws Exception;

    public void saveServingArea(HeaderDto headerDto, List<AreaEntity> areas) throws Exception;

    public void updateServingArea(HeaderDto headerDto, List<AreaEntity> areas) throws Exception;

    public List<AreaEntity> getServingArea(HeaderDto headerDto) throws Exception;

    public void saveMerchant(MerchantEntity merchant) throws Exception;

    public void updateMerchant(HeaderDto headerDto, MerchantEntity merchant) throws Exception;

    public MerchantEntity getMerchantDetail(HeaderDto headerDto) throws Exception;

    public String saveDriver(DriverEntity driver) throws Exception;

    public void changeMerchantStatus(MerchantEntity merchant) throws Exception;

    public void changeStoreBrandStatus(StoreBrandEntity storeBrand) throws Exception;

    public void changeStoreStatus(StoreEntity store) throws Exception;

    public PaginationDto getStoreList(RequestJsonDto requestJsonDto) throws Exception;

    public List<StoreManagerEntity> getStoreManagers(String storeId) throws Exception;

    public RequestJsonDto getRatePlanOfStore(String storeId) throws Exception;

    public List<UnknownAreaRatePlanEntity> getUnknownAreaRatePlanOfStore(String storeId) throws Exception;

    public RequestJsonDto getNotification(String userId) throws Exception;

    public void updateViewedNotification(String userId, List<String> notificationIds) throws Exception;

    public PaginationDto getStoreListByStatus(String userId, Page page) throws Exception;

    public PaginationDto getStoreBrandListByMerchants(RequestJsonDto requestJsonDto) throws Exception;

    public void updateStatement(StatementEntity statement) throws Exception;

    public void generateStatement(StatementEntity statement) throws Exception;

    public String generateAPIKey(String userId) throws Exception;

    public void saveAPIKey(String userId, APIKeyEntity apiKeyEntity) throws Exception;

    public void deleteAPIKey(HeaderDto headerDto) throws Exception;

    public List<APIKeyEntity> getActiveAPIKeys(String userId) throws Exception;
}
