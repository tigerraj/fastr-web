package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.model.AreaEntity;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.StoreEntity;
import com.yetistep.anyorder.model.xml.APIData;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Suroj on 1/18/2017.
 */
public interface APIService {

    List<StoreEntity> getActiveStoresByAPIKey(String apiKey) throws Exception;

    List<AreaEntity> storeServingAreas(String apiKey, String storeId) throws Exception;

    String saveOrder(APIData xmlData) throws Exception;

    List<OrderEntity> APIOrderList(APIData xmlData) throws Exception;

    OrderEntity orderDetail(APIData xmlData) throws Exception;

    BigDecimal estimateCharge(APIData apiData) throws Exception;

}
