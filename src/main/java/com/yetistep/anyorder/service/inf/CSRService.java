package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.model.UserEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 9:54 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CSRService {

    public UserEntity getUserDetail(String userId) throws Exception;

    public UserEntity updateUser(UserEntity user) throws Exception;

    public UserEntity changeUserStatus(UserEntity user) throws Exception;
}
