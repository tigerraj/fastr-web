package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:35 AM
 * To change this template use File | Settings | File Templates.
 */
public interface DriverService {

    public void updateServingArea(HeaderDto headerDto, List<AreaEntity> areas) throws Exception;

    public List<AreaEntity> getServingArea(HeaderDto headerDto) throws Exception;

    public void updateDriver(HeaderDto headerDto, DriverEntity driver) throws Exception;

    public DriverEntity getDriver(String driverId) throws Exception;

    public PaginationDto getDriverList(RequestJsonDto requestJsonDto) throws Exception;

    public void changeDriverStatus(DriverEntity driver) throws Exception;

    public void assignDriver(HeaderDto headerDto, OrderEntity order) throws Exception;
}
