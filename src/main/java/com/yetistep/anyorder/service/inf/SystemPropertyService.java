package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.enums.PreferenceType;
import com.yetistep.anyorder.model.PreferenceTypeEntity;
import com.yetistep.anyorder.model.PreferencesEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SystemPropertyService {
    public void preferenceInitializaton();

    public List<PreferencesEntity> getAllPreferences() throws Exception;

    public PreferenceTypeEntity getPreferencesType(Integer typeId) throws Exception;

    public Boolean updateSystemPreferences(List<PreferencesEntity> preferencesEntities) throws Exception;

    public Boolean updateSystemPreferencesType(PreferenceTypeEntity preferenceType) throws Exception;

    public String readPrefValue(PreferenceType preferenceType) throws Exception;

}
