package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.model.OrderEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 7/8/16
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CustomerService {

    public OrderEntity getOrderDetail(HeaderDto headerDto) throws Exception;

    public void rateOrder(OrderEntity order) throws Exception;
}
