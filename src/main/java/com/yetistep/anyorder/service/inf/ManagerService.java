package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.UserDeviceType;
import com.yetistep.anyorder.model.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 9:54 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ManagerService {

    void saveArea(AreaEntity area) throws Exception;

    void updateArea(String areaId, AreaEntity area) throws Exception;

    void deleteArea(String areaId) throws Exception;

    PaginationDto getMerchantList(RequestJsonDto requestJsonDto) throws Exception;

    void saveRatePlan(String storeId, RatePlanEntity ratePlan) throws Exception;

    void updateRatePlan(String ratePlanId, RatePlanEntity ratePlan) throws Exception;

    void saveUnknownAreaRatePlan(String storeId, UnknownAreaRatePlanEntity unknownAreaRatePlan) throws Exception;

    void updateUnknownAreaRatePlan(String ratePlanId, UnknownAreaRatePlanEntity unknownAreaRatePlan) throws Exception;

    String generateStatement(StoreEntity store, String fromDate, String toDate) throws Exception;

    void approveStatement(StatementEntity statement) throws Exception;

    void sendPushNotification(RequestJsonDto requestJsonDto) throws Exception;

    List<UserEntity> getActiveMerchant() throws Exception;

    List<NotificationEntity> getNotificationByDeviceType(UserDeviceType userDeviceType) throws Exception;
}
