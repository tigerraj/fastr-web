package com.yetistep.anyorder.service.inf;

import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.UserDeviceType;
import com.yetistep.anyorder.model.NotificationEntity;
import com.yetistep.anyorder.model.StoreEntity;
import com.yetistep.anyorder.model.UserEntity;

import java.util.List;

/**
 * Created by BinaySingh on 5/11/2016.
 */
public interface AdminService {

    String createUser(String username, UserEntity user) throws Exception;

    public PaginationDto getUsers(RequestJsonDto requestJsonDto) throws Exception;

    public void activateStore(String storeId, StoreEntity store) throws Exception;

    public Boolean updateDefaultImage(RequestJsonDto requestJsonDto) throws Exception;

    public void updateStoreSetting(String storeId, StoreEntity store) throws Exception;

}
