package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.MerchantService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Suroj
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class MerchantServiceImpl extends AbstractManager implements MerchantService {

    private static final Logger log = Logger.getLogger(MerchantServiceImpl.class);

    @Autowired
    MerchantDao merchantDao;

    @Autowired
    AreaDao areaDao;

    @Autowired
    StoreBrandDao storeBrandDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    StoreAreaDao storeAreaDao;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    DriverDao driverDao;

    @Autowired
    DriversAreaDao driversAreaDao;

    @Autowired
    StatementDao statementDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    UserDao userDao;

    @Autowired
    UserDeviceDao userDeviceDao;

    @Autowired
    APIKeyDao apiKeyDao;

    @Override
    public void saveStoresBrand(HeaderDto headerDto, StoreBrandEntity storeBrand) throws Exception {
        log.info("Saving store Brand");
        MerchantEntity merchantEntity = merchantDao.findById(headerDto.getMerchantId());
        if (merchantEntity == null)
            throw new YSException("MER001");
        storeBrand.setMerchant(merchantEntity);

        Boolean isBrandNameExist = storeBrandDao.checkBrandNameExist(storeBrand.getBrandName());
        if (isBrandNameExist)
            throw new YSException("STR003");
        List<StoreEntity> stores = new ArrayList<>();
        for (StoreEntity store : storeBrand.getStores()) {
            AreaEntity areaEntity = areaDao.findById(store.getArea().getAreaId());
            if (areaEntity == null)
                throw new YSException("ARE001");
            StoresAreaEntity storesAreaEntity = new StoresAreaEntity();
            storesAreaEntity.setArea(areaEntity);
            storesAreaEntity.setStore(store);
            store.setArea(areaEntity);
            store.setStoreBrand(storeBrand);
            store.setStatus(Status.ACTIVE);
            store.setIsVerified(false);
            stores.add(store);
        }
        storeBrand.setStores(stores);
        storeBrand.setStatus(Status.ACTIVE);
        String brandLogo = storeBrand.getBrandLogo();
        storeBrand.setBrandLogo(null);
        storeBrandDao.save(storeBrand);

        if (brandLogo != null && !brandLogo.isEmpty()) {
            log.info("Uploading brand logo to S3 Bucket ");
            String dir = MessageBundle.separateString("/", "Merchant_" + merchantEntity.getId(), "Brand_" + storeBrand.getId());
            boolean isLocal = MessageBundle.isLocalHost();
            String brandLogoName = "brand_logo" + (isLocal ? "_tmp_" : "_") + storeBrand.getId() + System.currentTimeMillis();
            String s3PathLogo = GeneralUtil.saveImageToBucket(brandLogo, brandLogoName, dir, true);
            storeBrand.setBrandLogo(s3PathLogo);
            storeBrandDao.update(storeBrand);
        }
    }

    @Override
    public void updateStoresBrand(HeaderDto headerDto, StoreBrandEntity storeBrand) throws Exception {
        log.info("Updating store Brand");
        StoreBrandEntity storeBrandEntity = storeBrandDao.findById(headerDto.getBrandId());
        if (storeBrandEntity == null)
            throw new YSException("STR001");
        Boolean isBrandNameExist = storeBrandDao.checkBrandNameExist(storeBrand.getBrandName());
        if (isBrandNameExist && !storeBrand.getBrandName().equals(storeBrandEntity.getBrandName()))
            throw new YSException("STR003");

        List<StoreEntity> storeEntities = new ArrayList<>();

        List<Integer> storeIds = new ArrayList<>();
        List<Integer> dbStoreIdList = new ArrayList<Integer>();
        Map<Integer, StoreEntity> dbStoreMap = new HashMap<>();

        for (StoreEntity store1 : storeBrandEntity.getStores()) {
            dbStoreIdList.add(store1.getId());
            dbStoreMap.put(store1.getId(), store1);
        }

        for (StoreEntity store : storeBrand.getStores()) {
            StoreEntity storeEntity = storeDao.findById(store.getStoreId());
            AreaEntity areaEntity = areaDao.findById(store.getArea().getAreaId());
            if (areaEntity == null)
                throw new YSException("ARE001");
            if (storeEntity == null) {
                storeEntity = new StoreEntity();
                storeEntity.setStatus(Status.ACTIVE);
                storeEntity.setLatitude(store.getLatitude());
                storeEntity.setLongitude(store.getLongitude());
                storeEntity.setStoreBrand(storeBrandEntity);
            }
            storeEntity.setContactNo(store.getContactNo());
            storeEntity.setGivenLocation1(store.getGivenLocation1());
            storeEntity.setGivenLocation2(store.getGivenLocation2());
            storeEntity.setAddressNote(store.getAddressNote());
            storeEntity.setPriority(store.getPriority());
            storeEntity.setIsDistanceBaseRatePlan(store.getIsDistanceBaseRatePlan());
            storeEntity.setArea(areaEntity);
            storeDao.save(storeEntity);
            storeIds.add(storeEntity.getId());
            storeEntities.add(storeEntity);
        }

        for (Integer dbStore : dbStoreIdList) {
            if (!storeIds.contains(dbStore)) {
                dbStoreMap.get(dbStore).setStatus(Status.DELETED);
                //storeEntities.remove(dbStoreMap.get(dbStore));
            }
        }
        storeBrandEntity.setStores(storeEntities);
        storeBrandEntity.setBrandName(storeBrand.getBrandName());
        storeBrandEntity.setStatementGenerationType(storeBrand.getStatementGenerationType());
        String brandLogo = storeBrand.getBrandLogo();
        String dbBrandLogo = storeBrandEntity.getBrandLogo();/*
        if(!brandLogo.equals(dbBrandLogo))
            storeBrandEntity.setBrandLogo(null);*/
        storeBrandDao.update(storeBrandEntity);

        if (brandLogo != null && !brandLogo.isEmpty() && !brandLogo.equals(dbBrandLogo)) {
            log.info("Uploading brand logo to S3 Bucket ");
            if (dbBrandLogo != null && !dbBrandLogo.isEmpty())
                AmazonUtil.deleteFileFromBucket(AmazonUtil.getAmazonS3Key(dbBrandLogo));
            String dir = MessageBundle.separateString("/", "Merchant_" + storeBrandEntity.getMerchant().getId(), "Brand_" + storeBrandEntity.getId());
            boolean isLocal = MessageBundle.isLocalHost();
            String brandLogoName = "brand_logo" + (isLocal ? "_tmp_" : "_") + storeBrandEntity.getId() + System.currentTimeMillis();
            String s3PathLogo = GeneralUtil.saveImageToBucket(brandLogo, brandLogoName, dir, true);
            storeBrandEntity.setBrandLogo(s3PathLogo);
            storeBrandDao.update(storeBrandEntity);
        }
    }

    @Override
    public StoreBrandEntity getBrandDetails(HeaderDto headerDto) throws Exception {
        StoreBrandEntity storeBrandEntity = storeBrandDao.findById(headerDto.getBrandId());
        if (storeBrandEntity == null)
            throw new YSException("STR001");
        String fields = "storeBrandId,brandName,brandLogo,statementGenerationType,stores,status";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("stores", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,contactNo,status,isVerified,canActivate,allowStoresDriver,allowUnknownAreaServing,isDistanceBaseRatePlan");
        subAssoc.put("area", "areaId,parent");
        subAssoc.put("parent", "areaId,parent");
        List<StoreEntity> stores = new ArrayList<>();
        for (StoreEntity store : storeBrandEntity.getStores()) {
            if (!store.getStatus().equals(Status.DELETED)) {
                store.setCanActivate(isVerifiableStore(store, false));
                stores.add(store);
            }
        }
        storeBrandEntity.setStores(stores);
        return (StoreBrandEntity) ReturnJsonUtil.getJsonObject(storeBrandEntity, fields, assoc, subAssoc);
    }

    @Override
    public void saveServingArea(HeaderDto headerDto, List<AreaEntity> areas) throws Exception {
        StoreEntity storeEntity = storeDao.findById(headerDto.getStoreId());
        if (storeEntity == null)
            throw new YSException("STR002");
        List<StoresAreaEntity> storesAreaEntities = new ArrayList<>();
        for (AreaEntity area : areas) {
            AreaEntity areaEntity = areaDao.findById(area.getAreaId());
            if (areaEntity == null)
                throw new YSException("STR004");
            StoresAreaEntity storeAreaEntity = new StoresAreaEntity();
            storeAreaEntity.setStore(storeEntity);
            storeAreaEntity.setArea(areaEntity);
            storesAreaEntities.add(storeAreaEntity);
        }
        storeEntity.setStoresAreas(storesAreaEntities);
        storeDao.update(storeEntity);
    }

    public void updateServingArea(HeaderDto headerDto, List<AreaEntity> areas) throws Exception {
        StoreEntity storeEntity = storeDao.findById(headerDto.getStoreId());
        if (storeEntity == null)
            throw new YSException("STR002");

        List<AreaEntity> savedArea = areaDao.findAreaIds();
        if (!savedArea.containsAll(areas)) {
            throw new YSException("STR004");
        }

        List<StoresAreaEntity> toSavedAreas = new ArrayList<>();
        for (AreaEntity area : savedArea) {
            if (areas.contains(area)) {
                StoresAreaEntity storesAreaEntity = new StoresAreaEntity();
                storesAreaEntity.setArea(area);
                storesAreaEntity.setStore(storeEntity);
                toSavedAreas.add(storesAreaEntity);
            }
        }
        storeAreaDao.deleteAll(storeEntity.getId());
        storeAreaDao.updateAll(toSavedAreas);

        //send push notification to driver
        List<String> tokens = userDeviceDao.findDeviceTokenByStoreId(storeEntity.getId());
        if (tokens.size() > 0) {
            PushNotification pushNotification = new PushNotification();
            pushNotification.setTokens(tokens);
            Map<String, String> message = new HashMap<>();
            message.put("message", MessageBundle.getMessage("SMGR005", "push_notification.properties"));
            message.put("redirect", PushNotificationRedirect.SERVINGAREA.toString());
            pushNotification.setMessage(message);
            pushNotification.setNotifyTo(NotifyTo.STORE_MANAGER);
            PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
            log.info("Push notification has been sent successfully");
        }
    }

    @Override
    public List<AreaEntity> getServingArea(HeaderDto headerDto) throws Exception {
        StoreEntity storeEntity = storeDao.findById(headerDto.getStoreId());
        if (storeEntity == null)
            throw new YSException("STR002");
        List<AreaEntity> parentAreas = areaDao.findActiveParentAreas();

        List<AreaEntity> areas = new ArrayList<>();

        String fields = "areaId,areaName,child,selected";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();

        assoc.put("child", "areaId,areaName,child,selected");
        subAssoc.put("child", "areaId,areaName,child,latitude,longitude,street,selected");

        List<StoresAreaEntity> storesAreas = storeEntity.getStoresAreas();
        List<Integer> areaIds = new ArrayList<>();
        for (StoresAreaEntity storesArea : storesAreas) {
            areaIds.add(storesArea.getArea().getId());
        }
        AreaTreeUtil.parseActiveAreaTree(parentAreas, areaIds);
        for (AreaEntity area : parentAreas) {
            areas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area, fields, assoc, subAssoc));
        }
        return areas;
    }

    @Override
    public void saveMerchant(MerchantEntity merchant) throws Exception {
        log.info("----- Creating Merchant -----");
        UserEntity user = merchant.getUser();

        //Initial checks for merchant sign up
        if (merchantDao.emailExist(user.getUsername())) {
            log.error("----- Username (Email) already exists. -----");
            throw new YSException("MRC001");
        }
        if (merchantDao.businessTitleExist(merchant.getBusinessTitle())) {
            log.error("----- Business Title already exists. -----");
            throw new YSException("MRC002");
        }

        user.setUserStatus(UserStatus.ACTIVE);
        user.setPassword(GeneralUtil.encryptPassword(user.getPassword().trim()));
        user.setBlacklistStatus(Boolean.FALSE);
        user.setVerifiedStatus(Boolean.TRUE);
        user.setCreatedDate(DateUtil.getCurrentTimestampSQL());
        user.setUserRole(UserRole.ROLE_MERCHANT);
        user.setDeviceType(UserDeviceType.WEB);
        user.setLastActivityDate(DateUtil.getCurrentTimestampSQL());
        merchant.setUser(user);

        merchantDao.save(merchant);

        //Sending email
        log.info("Sending welcome email to " + user.getUsername());
        String body = EmailMsg.welcomeEmailForNewUser(user.getFirstName() + " " + user.getLastName(), user.getUsername(), getServerUrl());
        String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": You have been added as Merchant ";
        sendMail(user.getUsername(), body, subject);
        log.info("----- Merchant Created Successfully -----");
    }

    @Override
    public void updateMerchant(HeaderDto headerDto, MerchantEntity merchant) throws Exception {
        log.info("----- Updating merchant in service -----");
        Integer trueId = merchantDao.getTrueId(merchant.getMerchantId());
        MerchantEntity merchantEntity = merchantDao.findById(merchant.getMerchantId());
        if (SessionManager.getRole().equals(UserRole.ROLE_MERCHANT) && !GeneralUtil.matchDBPassword(headerDto.getPassword(), merchantEntity.getUser().getPassword())) {
            throw new YSException("PASS001");
        }
        if (merchantEntity == null) {
            throw new YSException("MRC003");
        }
        if (merchant.getBusinessTitle() != null) {
            if (!(merchant.getBusinessTitle().equals(merchantEntity.getBusinessTitle())) && merchantDao.businessTitleExist(merchant.getBusinessTitle())) {
                throw new YSException("MRC002");
            }
        }

        UserEntity userEntity = merchantEntity.getUser();
        UserEntity user = merchant.getUser();

        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        userEntity.setMobileNumber(user.getMobileNumber());

        merchantEntity.setUser(userEntity);

        merchantDao.save(merchantEntity);
    }

    @Override
    public MerchantEntity getMerchantDetail(HeaderDto headerDto) throws Exception {
        log.info("----- Getting Merchant detail -----");
        String fakeId = headerDto.getMerchantId();
        if (fakeId == null) {
            log.error("----- MerchantId cannot be null -----");
            throw new YSException("MRC004");
        }
        MerchantEntity merchantEntity = merchantDao.findById(fakeId);
        if (merchantEntity == null) {
            log.error("----- Merchant does not exist. -----");
            throw new YSException("MRC003");
        }
        String field = "merchantId,businessTitle,user,storeBrand";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "userId,username,firstName,lastName,mobileNumber,emailAddress");
        assoc.put("storeBrand", "storeBrandId,brandName");

        MerchantEntity merchantEntityFiltered = (MerchantEntity) ReturnJsonUtil.getJsonObject(merchantEntity, field, assoc);

        return merchantEntityFiltered;
    }

    @Override
    public String saveDriver(DriverEntity driver) throws Exception {
        log.info("----- Saving Driver -----");
        UserEntity user = driver.getUser();

        //Initial checks for driver sign up
        if (driverDao.mobileNumberExists(user.getUsername())) {
            log.error("----- Username (MobileNumber) already exists. -----");
            throw new YSException("DRV002");
        }

        if (userDao.checkIfEmailExistsWithSameRole(user.getEmailAddress(), UserRole.ROLE_DRIVER)) {
            log.error("----- Email already exists. -----");
            throw new YSException("USR004");
        }

        List<DriversAreaEntity> driversAreas = driver.getDriversArea();
        if (driversAreas != null && driversAreas.size() > 0) {
            List<DriversAreaEntity> driversAreaEntities = new ArrayList<>();

            for (DriversAreaEntity driversArea : driversAreas) {
                AreaEntity areaEntity = areaDao.findByAreaId(driversArea.getArea().getAreaId());
                if (areaEntity == null) {
                    log.error("----- Area does not exist. -----");
                    throw new YSException("ARE001");
                }
                DriversAreaEntity driversAreaEntity = new DriversAreaEntity();

                driversAreaEntity.setArea(areaEntity);
                driversAreaEntity.setDriver(driver);
                driversAreaEntity.setStatus(Status.ACTIVE);

                driversAreaEntities.add(driversAreaEntity);
            }
            driver.setDriversArea(driversAreaEntities);
        }

        String profileImage = user.getProfileImage();
        if (profileImage == null || profileImage.isEmpty() || !profileImage.startsWith("data:image/")) {
            log.error("----- Profile Image of Driver is required. -----");
            throw new YSException("DRV003");
        }
        user.setProfileImage(null);

        String code = MessageBundle.generateTokenString() + "_" + System.currentTimeMillis();

        if (driver.getStore().getStoreId() != null) {
            StoreEntity storeEntity = storeDao.findById(driver.getStore().getStoreId());
            if (storeEntity == null) {
                log.error("----- Store does not exist. -----");
                throw new YSException("STR002");
            }
            storeEntity.getDrivers().add(driver);
            driver.setStore(storeEntity);
            driver.setDriverType(DriverType.RESTAURANT);
        } else {
            driver.setStore(null);
            driver.setDriverType(DriverType.FASTR);
        }

        user.setUserStatus(UserStatus.INACTIVE);
        user.setBlacklistStatus(Boolean.FALSE);
        user.setVerifiedStatus(Boolean.FALSE);
        user.setCreatedDate(DateUtil.getCurrentTimestampSQL());
        user.setUserRole(UserRole.ROLE_DRIVER);
        user.setDeviceType(UserDeviceType.MOBILE);
        user.setLastActivityDate(DateUtil.getCurrentTimestampSQL());
        user.setVerificationCode(code);

        driver.setAvailabilityStatus(AvailabilityStatus.UNAVAILABLE);
        driver.setUser(user);

        driverDao.save(driver);

        log.info("Uploading Profile Image of User to S3 Bucket ");

        String dir = MessageBundle.separateString("/", "Users", "user_" + user.getId());
        boolean isLocal = MessageBundle.isLocalHost();
        String imageName = "pimg" + (isLocal ? "_tmp_" : "_") + user.getId() + System.currentTimeMillis();
        String s3Path = GeneralUtil.saveImageToBucket(profileImage, imageName, dir, true);
        user.setProfileImage(s3Path);
        driver.setUser(user);
        driverDao.update(driver);

        //Sending Email to Driver
        String hostName = getServerUrl();
        String url = hostName + "/assistance/create_password?key=" + code;
        log.info("Sending mail to " + user.getEmailAddress() + " with new registration: " + url);

        //send email
        String body = EmailMsg.createPasswordForNewUser(url, user.getFirstName() + " " + user.getLastName(), user.getUsername(), getServerUrl());
        String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": You have been added as a Driver";

        sendMail(user.getEmailAddress(), body, subject);
        return driver.getDriverId();
    }

    @Override
    public void changeMerchantStatus(MerchantEntity merchant) throws Exception {
        log.info("----- Changing merchant's status. -----");
        MerchantEntity merchantEntity = merchantDao.findById(merchant.getMerchantId());
        if (merchantEntity == null) {
            log.error("----- Merchant does not exist. -----");
            throw new YSException("MRC003");
        }
        if (merchant.getUser().getUserStatus() != null && !merchant.getUser().getUserStatus().equals(merchantEntity.getUser().getUserStatus())) {
            merchantEntity.getUser().setUserStatus(merchant.getUser().getUserStatus());
            merchantDao.update(merchantEntity);

            log.info("Sending mail to " + merchantEntity.getUser().getEmailAddress());

            String body = EmailMsg.notificationEmailUserStatus(merchantEntity.getUser(), getServerUrl());
            String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Your account has been";
            if (merchant.getUser().getUserStatus().equals(UserStatus.ACTIVE)) {
                subject += " re-activated.";
            } else if (merchant.getUser().getUserStatus().equals(UserStatus.INACTIVE)) {
                subject += " deactivated.";
            }

            sendMail(merchantEntity.getUser().getEmailAddress(), body, subject);
        }
    }

    @Override
    public void changeStoreBrandStatus(StoreBrandEntity storeBrand) throws Exception {
        log.info("----- Changing StoreBrand's status. -----");
        StoreBrandEntity storeBrandEntity = storeBrandDao.findById(storeBrand.getStoreBrandId());
        if (storeBrandEntity == null) {
            log.error("----- StoreBrand does not exist. -----");
            throw new YSException("STR001");
        }
        if (storeBrand.getStatus() != null && !storeBrand.getStatus().equals(storeBrandEntity.getStatus())) {
            storeBrandEntity.setStatus(storeBrand.getStatus());
            storeBrandDao.update(storeBrandEntity);

            log.info("Sending mail to " + storeBrandEntity.getMerchant().getUser().getEmailAddress());

            String body = EmailMsg.notificationEmailStoreBrandStatus(storeBrandEntity.getBrandName(), storeBrandEntity.getMerchant().getUser(), storeBrand.getStatus(), getServerUrl());
            String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Store has been";
            if (storeBrand.getStatus().equals(Status.ACTIVE)) {
                subject += " re-activated.";
            } else if (storeBrand.getStatus().equals(Status.INACTIVE)) {
                subject += " deactivated.";
            }

            sendMail(storeBrandEntity.getMerchant().getUser().getEmailAddress(), body, subject);
        }
    }

    @Override
    public PaginationDto getStoreList(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting StoreBrand list. -----");

        String merchantId = requestJsonDto.getMerchant().getMerchantId();
        Page page = requestJsonDto.getPage();
        PaginationDto paginationDto = new PaginationDto();

        if (page != null && page.getSearchFor() != null && !page.getSearchFor().equals("")) {
            Map<String, String> fieldsMap = new HashMap<>();
            fieldsMap.put("self", "storeBrandId,brandName,status");
            page.setSearchFields(fieldsMap);
        }
        Integer totalRows = storeBrandDao.getNumberOfMerchantStoreBrand(merchantId, page);
        if (page != null) {
            page.setTotalRows(totalRows);
        }
        paginationDto.setNumberOfRows(totalRows);

        List<StoreBrandEntity> storeBrandEntities = storeBrandDao.getMerchantStoreBrandList(merchantId, page);

        String field = "storeBrandId,brandName,brandLogo,status,stores";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("stores", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,status");

        List<StoreBrandEntity> storeBrandList = new ArrayList<>();
        for (StoreBrandEntity storeBrandEntity : storeBrandEntities) {
            StoreBrandEntity storeBrandEntityFiltered = (StoreBrandEntity) ReturnJsonUtil.getJsonObject(storeBrandEntity, field, assoc);
            storeBrandList.add(storeBrandEntityFiltered);
        }
        paginationDto.setData(storeBrandList);
        return paginationDto;
    }

    @Override
    public List<StoreManagerEntity> getStoreManagers(String storeId) throws Exception {
        log.info("Getting store manager of merchant:" + storeId);
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        List<StoreManagerEntity> storeManagers = storeEntity.getStoreManagers();
        List<StoreManagerEntity> returnStoreManagers = new ArrayList<>();
        String fields = "storeManagerId,availabilityStatus,user";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "firstName,lastName,mobileNumber,emailAddress,profileImage,userStatus");
        for (StoreManagerEntity storeManager : storeManagers)
            returnStoreManagers.add((StoreManagerEntity) ReturnJsonUtil.getJsonObject(storeManager, fields, assoc));
        return returnStoreManagers;
    }

    @Override
    public RequestJsonDto getRatePlanOfStore(String storeId) throws Exception {
        log.info("Getting rate plan of store id : " + storeId);
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        RequestJsonDto requestJsonDto = new RequestJsonDto();
        if (storeEntity.getIsDistanceBaseRatePlan() == null || !storeEntity.getIsDistanceBaseRatePlan()) {
            List<RatePlanEntity> ratePlanEntities = new ArrayList<>();
            String fields = "ratePlanId,name,driverType,status,createdDate,lastModified,orderCount,amount,additionalChargePerOrder,duration";
            for (RatePlanEntity ratePlanEntity : storeEntity.getRatePlans()) {
                ratePlanEntities.add((RatePlanEntity) ReturnJsonUtil.getJsonObject(ratePlanEntity, fields));
            }
            requestJsonDto.setRatePlans(ratePlanEntities);
        } else {
            List<UnknownAreaRatePlanEntity> unknownAreaRatePlanEntities = new ArrayList<>();
            String fields = "unknownAreaRatePlanId,name,status,createdDate,lastModified,startDistance,endDistance,baseCharge,additionalChargePerKM";
            for (UnknownAreaRatePlanEntity unknownAreaRatePlanEntity : storeEntity.getUnknownAreaRatePlans()) {
                if (unknownAreaRatePlanEntity.getIsKnownArea() != null && unknownAreaRatePlanEntity.getIsKnownArea()) {
                    unknownAreaRatePlanEntities.add((UnknownAreaRatePlanEntity) ReturnJsonUtil.getJsonObject(unknownAreaRatePlanEntity, fields));
                }
            }
            requestJsonDto.setUnknownAreaRatePlans(unknownAreaRatePlanEntities);
        }
        return requestJsonDto;
    }

    @Override
    public List<UnknownAreaRatePlanEntity> getUnknownAreaRatePlanOfStore(String storeId) throws Exception {
        log.info("Getting unknown area rate plan of store id : " + storeId);
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        List<UnknownAreaRatePlanEntity> unknownAreaRatePlanEntities = new ArrayList<>();
        String fields = "unknownAreaRatePlanId,status,createdDate,lastModified,startDistance,endDistance,baseCharge,additionalChargePerKM";
        for (UnknownAreaRatePlanEntity unknownAreaRatePlanEntity : storeEntity.getUnknownAreaRatePlans()) {
            if (unknownAreaRatePlanEntity.getIsKnownArea() == null || !unknownAreaRatePlanEntity.getIsKnownArea()) {
                unknownAreaRatePlanEntities.add((UnknownAreaRatePlanEntity) ReturnJsonUtil.getJsonObject(unknownAreaRatePlanEntity, fields));
            }
        }
        return unknownAreaRatePlanEntities;
    }

    @Override
    public void changeStoreStatus(StoreEntity store) throws Exception {
        log.info("----- Changing Store's status. -----");
        StoreEntity storeEntity = storeDao.findById(store.getStoreId());
        if (storeEntity == null) {
            log.error("----- Store does not exist. -----");
            throw new YSException("STR002");
        }
        if (store.getStatus() != null && !store.getStatus().equals(storeEntity.getStatus())) {
            storeEntity.setStatus(store.getStatus());
            storeDao.update(storeEntity);

            log.info("Sending mail to " + storeEntity.getStoreBrand().getMerchant().getUser().getEmailAddress());

            String body = EmailMsg.notificationEmailStoreBrandStatus(storeEntity.getStoreBrand().getBrandName() + ", " + storeEntity.getGivenLocation1(), storeEntity.getStoreBrand().getMerchant().getUser(), store.getStatus(), getServerUrl());
            String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Store has been";
            if (store.getStatus().equals(Status.ACTIVE)) {
                subject += " re-activated.";
            } else if (store.getStatus().equals(Status.INACTIVE)) {
                subject += " deactivated.";
            }

            sendMail(storeEntity.getStoreBrand().getMerchant().getUser().getEmailAddress(), body, subject);
        }
    }

    @Override
    public RequestJsonDto getNotification(String userId) throws Exception {
        log.info("Getting notification");
        UserEntity userEntity = userDao.findByUserId(userId);
        RequestJsonDto requestJsonDto = new RequestJsonDto();
        if (userEntity == null && userEntity.getMerchant() == null)
            throw new YSException("USR001");
        List<NotificationEntity> notifications = notificationDao.findMerchantNotification(userEntity.getId());
        List<NotificationEntity> notificationEntities = new ArrayList<>();
        String fields = "notificationId,content,viewed,validTill,createdDate";
        Integer notViewedCount = 0;
        for (NotificationEntity notification : notifications) {
            if (notification.getViewed() == null)
                notViewedCount++;
            notificationEntities.add((NotificationEntity) ReturnJsonUtil.getJsonObject(notification, fields));
        }
        requestJsonDto.setNotifications(notificationEntities);
        requestJsonDto.setNotViewedCount(notViewedCount);
        return requestJsonDto;
    }

    @Override
    public void updateViewedNotification(String userId, List<String> notificationIds) throws Exception {
        log.info("Updating viewed notification");
        UserEntity userEntity = userDao.findByUserId(userId);
        if (userEntity == null)
            throw new YSException("USR001");
        for (String notificationId : notificationIds) {
            NotificationEntity noticationEntity = notificationDao.findByNotificationId(notificationId);
            if (noticationEntity == null)
                throw new YSException("NOTI001");
            if (!noticationEntity.getUser().getId().equals(userEntity.getId()))
                throw new YSException("NOTI002");
            noticationEntity.setViewed(Boolean.TRUE);
            notificationDao.update(noticationEntity);
        }
    }

    @Override
    public PaginationDto getStoreListByStatus(String userId, Page page) throws Exception {
        log.info("Getting store list of user: " + userId);
        PaginationDto paginationDto = new PaginationDto();
        UserEntity userEntity = userDao.findByUserId(userId);
        if (userEntity == null)
            throw new YSException("USR001");
        if (!userEntity.getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("USR008");
        Integer totalRows = 0;
        List<StoreBrandEntity> storeBrandEntities = new ArrayList<>();
        if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            totalRows = storeBrandDao.getTotalNumberOfStoreByStatus(userEntity.getMerchant().getMerchantId(), page.getStatus());
            if (page != null) {
                page.setTotalRows(totalRows);
            }
            storeBrandEntities = storeBrandDao.getStoreBrandListByStatus(userEntity.getMerchant().getMerchantId(), page);
        } else if (userEntity.getUserRole().equals(UserRole.ROLE_ADMIN) || userEntity.getUserRole().equals(UserRole.ROLE_MANAGER) || userEntity.getUserRole().equals(UserRole.ROLE_CSR)) {
            totalRows = storeBrandDao.getTotalNumberOfStoreByStatus(null, page.getStatus());
            if (page != null) {
                page.setTotalRows(totalRows);
            }
            storeBrandEntities = storeBrandDao.getStoreBrandListByStatus(null, page);
        }
        paginationDto.setNumberOfRows(totalRows);

        paginationDto.setData(storeBrandEntities);
        String fields = "numberOfRows,data";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("data", "storeBrandId,brandName,brandLogo,status,stores");
        subAssoc.put("stores", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,status");
        for (StoreBrandEntity storeBrand : storeBrandEntities) {
            List<StoreEntity> stores = new ArrayList<>();
            for (StoreEntity storeEntity : storeBrand.getStores()) {
                if (!storeEntity.getStatus().equals(Status.DELETED)) {
                    stores.add(storeEntity);
                }
            }
            storeBrand.setStores(stores);
        }
        return (PaginationDto) ReturnJsonUtil.getJsonObject(paginationDto, fields, assoc, subAssoc);
    }

    @Override
    public PaginationDto getStoreBrandListByMerchants(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting List of StoreBrand by merchants. -----");
        PaginationDto paginationDto = new PaginationDto();
        List<String> merchantIds = requestJsonDto.getMerchantIds();
        Page page = requestJsonDto.getPage();

        Integer totalRows = storeBrandDao.getNumberOfMerchantsStoreBrands(merchantIds);
        page.setTotalRows(totalRows);
        paginationDto.setNumberOfRows(totalRows);
        List<StoreBrandEntity> storeBrandEntities = storeBrandDao.getMerchantsStoreBrands(merchantIds, page);

        String field = "storeBrandId,brandName,brandLogo,status,stores";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("stores", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,status");

        List<StoreBrandEntity> storeBrandList = new ArrayList<>();
        for (StoreBrandEntity storeBrandEntity : storeBrandEntities) {
            StoreBrandEntity storeBrandEntityFiltered = (StoreBrandEntity) ReturnJsonUtil.getJsonObject(storeBrandEntity, field, assoc);
            storeBrandList.add(storeBrandEntityFiltered);
        }
        paginationDto.setData(storeBrandList);
        return paginationDto;
    }

    @Override
    public void updateStatement(StatementEntity statement) throws Exception {
        log.info("----- Updating Statement -----");
        StatementEntity statementEntity = statementDao.findByStatementId(statement.getStatementId());
        if (statementEntity == null) {
            log.error("----- Statement does not exist. -----");
            throw new YSException("STAT001");
        }

        UserRole userRole = SessionManager.getRole();
        if (userRole.equals(UserRole.ROLE_MERCHANT) || userRole.equals(UserRole.ROLE_ADMIN) || userRole.equals(UserRole.ROLE_MANAGER)) {
            if (userRole.equals(UserRole.ROLE_MERCHANT)
                    && !SessionManager.getMerchantId().equals(statementEntity.getStore().getStoreBrand().getMerchant().getMerchantId())) {
                log.error("----- Statement does not belong to this merchant. -----");
                throw new YSException("STAT004");
            }
            if (statement.getExtraOrderCount() != null) {
                statementEntity.setExtraOrderCount(statement.getExtraOrderCount());
            }
            if (statement.getExtraOrderCharge() != null) {
                statementEntity.setExtraOrderCharge(statement.getExtraOrderCharge());
            }
            if (statement.getOutOfServingAreaOrderCount() != null) {
                statementEntity.setOutOfServingAreaOrderCount(statement.getOutOfServingAreaOrderCount());
            }
            if (statement.getOutOfServingAreaOrderCharge() != null) {
                statementEntity.setOutOfServingAreaOrderCharge(statement.getOutOfServingAreaOrderCharge());
            }
            if (statement.getDistanceBasedOrderCount() != null) {
                statementEntity.setDistanceBasedOrderCount(statement.getDistanceBasedOrderCount());
            }
            if (statement.getDistanceBasedOrderAmount() != null) {
                statementEntity.setDistanceBasedOrderAmount(statement.getDistanceBasedOrderAmount());
            }
            if (statement.getOtherCharge() != null) {
                statementEntity.setOtherCharge(statement.getOtherCharge());
            }
            BigDecimal totalPayableAmount = BigDecimal.ZERO;
            totalPayableAmount = totalPayableAmount
                    .add(BigDecimalUtil.checkNull(statementEntity.getSubscriptionRate()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getExtraOrderCharge()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getOutOfServingAreaOrderCharge()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getDistanceBasedOrderAmount()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getOtherCharge()));
            statementEntity.setTotalPayableAmount(totalPayableAmount);
            statementDao.update(statementEntity);
        } else {
            log.error("----- Does not have permission to update statement. -----");
            throw new YSException("STAT005");
        }
    }

    @Override
    public void generateStatement(StatementEntity statement) throws Exception {
        log.info("----- Generating statement in PDF file format. -----");
        StatementEntity statementEntity = statementDao.findByStatementId(statement.getStatementId());
        if (statementEntity == null) {
            log.error("----- Statement does not exit. -----");
            throw new YSException("STAT001");
        }

        StoreEntity storeEntity = statementEntity.getStore();
        PDFGenerator statementGenerator = new PDFGenerator();

        Map<String, String> preferences = new HashMap<>();
        preferences.put("CURRENCY", systemPropertyService.readPrefValue(PreferenceType.CURRENCY));
        preferences.put("HELPLINE_NUMBER", systemPropertyService.readPrefValue(PreferenceType.HELPLINE_NUMBER));
        preferences.put("SUPPORT_EMAIL", systemPropertyService.readPrefValue(PreferenceType.SUPPORT_EMAIL));
        preferences.put("COMPANY_NAME", systemPropertyService.readPrefValue(PreferenceType.COMPANY_NAME));
        preferences.put("COMPANY_ADDRESS", systemPropertyService.readPrefValue(PreferenceType.COMPANY_ADDRESS));
        preferences.put("SERVER_URL", systemPropertyService.readPrefValue(PreferenceType.SERVER_URL));

        String imageUrl = systemPropertyService.readPrefValue(PreferenceType.LOGO_FOR_PDF_EMAIL);

        String statementUrl = statementGenerator.generateStatement(storeEntity.getStoreBrand().getMerchant(), statementEntity, storeEntity, imageUrl, preferences);

        if (statementUrl == null) {
            throw new YSException("STAT003");
        }

        statementEntity.setStatementUrl(statementUrl);
        statementDao.update(statementEntity);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fromDate = dateFormat.format(statementEntity.getFromDate().getTime());
        String toDate = dateFormat.format(statementEntity.getToDate().getTime());

        String email = storeEntity.getStoreBrand().getMerchant().getUser().getEmailAddress();
        if (email != null && !email.equals("")) {
            String message = EmailMsg.sendInvoiceEmail(storeEntity, fromDate, toDate, systemPropertyService.readPrefValue(PreferenceType.SERVER_URL));
            sendAttachmentEmail(email, message, "get invoice-" + fromDate + "-" + toDate, statementUrl);
        }
        log.info("Email sent successfully with attachment: " + statementUrl + " for store " + storeEntity.getId() + " Email: " + email);
    }

    @Override
    public String generateAPIKey(String userId) throws Exception {
        UserEntity user = userDao.findByUserId(userId);
        if (user == null || !user.getUserRole().equals(UserRole.ROLE_MERCHANT))
            throw new YSException("MRC001");
        return generateUniqueAPIKey();
    }

    private String generateUniqueAPIKey() throws Exception {
        String apiKey = UUID.randomUUID().toString();
        while (!apiKeyDao.isUniqueKey(apiKey)) {
            generateUniqueAPIKey();
        }
        return apiKey;
    }

    @Override
    public void saveAPIKey(String userId, APIKeyEntity apiKeyEntity) throws Exception {
        UserEntity user = userDao.findByUserId(userId);
        if (user == null || !user.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            throw new YSException("MRC001");
        }
        if(apiKeyEntity.getId()==null) {
            if(!apiKeyDao.isUniqueKey(apiKeyEntity.getApiKey()))
                throw new YSException("API002");
            apiKeyEntity.setMerchant(user.getMerchant());
            apiKeyEntity.setStatus(Status.ACTIVE);
            apiKeyDao.update(apiKeyEntity);
        }else{
            APIKeyEntity apiKey = apiKeyDao.find(apiKeyEntity.getId());
            if(apiKey==null)
                throw new YSException("API001");
            apiKey.setApiName(apiKeyEntity.getApiName());
            apiKeyDao.update(apiKey);

        }
    }

    @Override
    public void deleteAPIKey(HeaderDto headerDto) throws Exception {
        UserEntity user = userDao.findByUserId(headerDto.getUserId());
        if (user == null || !user.getUserRole().equals(UserRole.ROLE_MERCHANT))
            throw new YSException("MRC001");
        APIKeyEntity apiKeyEntity = apiKeyDao.find(Integer.parseInt(headerDto.getApiKeyId()));
        if(apiKeyEntity == null)
            throw new YSException("API001");
        apiKeyEntity.setStatus(Status.INACTIVE);
        apiKeyDao.update(apiKeyEntity);
    }

    @Override
    public List<APIKeyEntity> getActiveAPIKeys(String userId) throws Exception{
        UserEntity user = userDao.findByUserId(userId);
        if (user == null || !user.getUserRole().equals(UserRole.ROLE_MERCHANT))
            throw new YSException("MRC001");
        return apiKeyDao.findActiveAPIKeys(user.getMerchant().getId());
    }
}
