package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.dao.impl.OrderDaoImpl;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.SystemAlgorithmService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.BigDecimalUtil;
import com.yetistep.anyorder.util.DateUtil;
import com.yetistep.anyorder.util.GeoCodingUtil;
import com.yetistep.anyorder.util.ReturnJsonUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/31/16
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class SystemAlgorithmServiceImpl implements SystemAlgorithmService {

    private static final Logger log = Logger.getLogger(DriverServiceImpl.class);

    Executor executor=Executors.newFixedThreadPool(8);

    @Autowired
    DriverDao driverDao;

    @Autowired
    AreaDao areaDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    DriversWorkLogDao driversWorkLogDao;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Override
    public List<DriverEntity> assignDriver(OrderEntity orderEntity, Boolean assignDriver) throws Exception {
        String storeId = orderEntity.getStore().getStoreId();
        AreaEntity area;
        if (orderEntity.getCustomersArea().getArea() != null) {
            String areaId = orderEntity.getCustomersArea().getArea().getAreaId();
            area = areaDao.findByAreaId(areaId);
        } else {
            area = new AreaEntity();
            area.setLatitude(orderEntity.getCustomersArea().getLatitude());
            area.setLongitude(orderEntity.getCustomersArea().getLongitude());
        }
        StoreEntity store = storeDao.findById(storeId);
        //get eligible drivers
        Integer driverLocationUpdateTimeInMinute = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVER_LOCATION_UPDATE_TIME));
        Integer driverOffsetTime = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVER_OFFSET_TIME));
        List<DriverEntity> drivers = driverDao.getSelectedDriver(store.getId(), orderEntity.getStore().getArea().getId(), store.getPriority(), systemPropertyService.readPrefValue(PreferenceType.MAX_NUM_ORDER_FOR_DRIVER), driverLocationUpdateTimeInMinute, driverOffsetTime);
        List<DriverEntity> eligibleDriversList = new ArrayList<>();
        for (StoresNotDriverEntity storesNotDriver : orderEntity.getStore().getStoresNotDrivers()) {
            drivers.remove(storesNotDriver.getDriver());
        }
        List<Integer> ids = new ArrayList<>();

        for(DriverEntity driverEntity:drivers){
            ids.add(driverEntity.getId());
        }
        Integer maxOrderServe = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.MAX_NUM_ORDER_FOR_DRIVER));
        List<OrderEntity> allOrders = orderDao.findActiveOrdersOfDriverIds(ids);
        List<String> driverAreaIds = new ArrayList<>();
        for(DriverEntity driver:drivers) {
            List<OrderEntity> activeOrders = new ArrayList<>();
            for (OrderEntity order : allOrders) {
                if (order.getDriver().getId().equals(driver.getId())){
                    activeOrders.add(order);
                }
            }
            driver.setOrders(activeOrders);
            driver.setCurrentOrderCount(activeOrders.size());
            //driver.setOrders(orders);
            if (orderEntity.getCustomersArea().getArea() == null) {
                //remove trainee driver with 1 order
                if (driver.getCurrentOrderCount() < maxOrderServe) {
                    if (driver.getExperienceLevel() != null && driver.getExperienceLevel().equals(DriverExperienceLevel.EXPERIENCED)) {
                        eligibleDriversList.add(driver);
                    } else if (driver.getCurrentOrderCount() == 0 && (driver.getExperienceLevel() == null || driver.getExperienceLevel().equals(DriverExperienceLevel.TRAINEE))) {
                        eligibleDriversList.add(driver);
                    }
                } else {
                    eligibleDriversList.add(driver);
                }
            } else {
                for (DriversAreaEntity driversArea : driver.getDriversArea()) {
                    driverAreaIds.add(driversArea.getArea().getAreaId());
                }
                if (driverAreaIds.contains(orderEntity.getCustomersArea().getArea().getAreaId())) {
                    eligibleDriversList.add(driver);
                }
            }
        }

        CountDownLatch countDownLatch=new CountDownLatch(eligibleDriversList.size());
        Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
        BigDecimal timeAtStore = new BigDecimal(systemPropertyService.readPrefValue(PreferenceType.DRIVERS_TIME_AT_STORE));
        BigDecimal timeAtCustomer = new BigDecimal(systemPropertyService.readPrefValue(PreferenceType.DRIVERS_TIME_AT_CUSTOMER));
        String storeAddress[] = {GeoCodingUtil.getLatLong(store.getLatitude(), store.getLongitude())};
        Boolean isActualDistance =DistanceType.valueOf(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)).equals(DistanceType.ACTUAL_DISTANCE);
        BigDecimal timeFactorPerKMForAerial = BigDecimal.valueOf(Double.parseDouble(systemPropertyService.readPrefValue(PreferenceType.AIR_TO_ROUTE_DISTANCE_TIME_FACTOR_PER_KM)));

        for (DriverEntity driverEntity : eligibleDriversList) {
            String customerAddress[] = {GeoCodingUtil.getLatLong(area.getLatitude(), area.getLongitude())};
            String driverAddress[] = {GeoCodingUtil.getLatLong(driverEntity.getLatitude(), driverEntity.getLongitude())};
            if (isActualDistance) {
                executor.execute(new DistanceMatrixTask(countDownLatch,driverAddress,storeAddress,customerAddress,timePerKm,driverEntity,timeAtStore,timeAtCustomer));
            } else {
                executor.execute(new MyTask(countDownLatch,driverEntity,orderEntity,area,timeAtCustomer,timeAtStore,timePerKm,timeFactorPerKMForAerial));
            }
        }
        countDownLatch.await();

        for(DriverEntity driverEntity: eligibleDriversList) {
            if (driverEntity.getCurrentOrderCount() > 0) {
                for (OrderEntity order : driverEntity.getOrders()) {
                    AreaEntity area1;
                    if (order.getCustomersArea().getArea() != null) {
                        String areaId = order.getCustomersArea().getArea().getAreaId();
                        area1 = areaDao.findByAreaId(areaId);
                    } else {
                        area1 = new AreaEntity();
                        area1.setLatitude(order.getCustomersArea().getLatitude());
                        area1.setLongitude(order.getCustomersArea().getLongitude());
                    }
                    BigDecimal timeRequireToComplete = BigDecimal.ZERO;
                    if (order.getDriver().getId().equals(driverEntity.getId())) {
                        String storeAddress1[] = {GeoCodingUtil.getLatLong(order.getStore().getLatitude(), order.getStore().getLongitude())};
                        String customerAddress1[] = {GeoCodingUtil.getLatLong(area1.getLatitude(), area1.getLongitude())};
                        String driverAddress1[] = {GeoCodingUtil.getLatLong(driverEntity.getLatitude(), driverEntity.getLongitude())};
                        BigDecimal driverToRestaurantTime1, restaurantToCustomerTime1;
                        if (isActualDistance) {
                            driverToRestaurantTime1 = new BigDecimal(GeoCodingUtil.getListOfDistances(driverAddress1, storeAddress1, timePerKm).get("time") / 60);
                            restaurantToCustomerTime1 = new BigDecimal(GeoCodingUtil.getListOfDistances(storeAddress1, customerAddress1, timePerKm).get("time") / 60);
                        } else {
                            driverToRestaurantTime1 = timeFactorPerKMForAerial.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(driverEntity.getLatitude(), driverEntity.getLongitude(), order.getStore().getLatitude(), order.getStore().getLongitude())));
                            restaurantToCustomerTime1 = timeFactorPerKMForAerial.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(order.getStore().getLatitude(), order.getStore().getLongitude(), area.getLatitude(), area.getLongitude())));
                        }
                        log.info("driver id:" + driverEntity.getId() + "driver to restaurant time:" + driverToRestaurantTime1 + "driver to restaurant time:" + restaurantToCustomerTime1
                                + "time at store:" + timeAtStore + "time at customer:" + timeAtCustomer);
                        timeRequireToComplete = timeRequireToComplete.add(driverToRestaurantTime1.add(restaurantToCustomerTime1).add(timeAtCustomer).add(timeAtStore));
                    }
                    driverEntity.setTotalOrderEtd(BigDecimalUtil.checkNull(driverEntity.getTotalOrderEtd()).add(timeRequireToComplete));
                }
            }
        }

        Collections.sort(eligibleDriversList, new Comparator<DriverEntity>() {
            public int compare(DriverEntity one, DriverEntity other) {
                return one.getTotalOrderEtd().compareTo(other.getTotalOrderEtd());
            }
        });

        List<DriverEntity> eligibleDrivers = new ArrayList<>();
        //comment out priority calculation for now
        //List<OrderEntity> lowerPriorityOrders = new ArrayList<>();
        log.info("------------ set the driver and etd to the order. driver size is: " + drivers.size() + " -------------");
        if (eligibleDriversList.size() > 0) {
            if (assignDriver) {
                DriverEntity selectedDriver = driverDao.find(eligibleDriversList.get(0).getId());
                orderEntity.setEstimatedDeliveryTime(eligibleDriversList.get(0).getCurrentOrderEtd());
                //orderEntity.setDeliveredDate(new Timestamp(System.currentTimeMillis()+(orderEntity.getEstimatedDeliveryTime().add(drivers.get(0).getTotalPreviousOrderEtd())).multiply(new BigDecimal(60*1000)).intValue()));
                orderEntity.setAssignDriver(true);
                //orderEntity.setOrderPriority(drivers.get(0).getPreviousOrderCount()+1);
                OrdersDriverEntity ordersDriver = new OrdersDriverEntity();
                ordersDriver.setDriver(selectedDriver);
                ordersDriver.setOrder(orderEntity);
                ordersDriver.setDriverStatus(OrdersDriverStatus.RUNNING);
                ordersDriver.setAssignedDate(DateUtil.getCurrentTimestampSQL());
                if (orderEntity.getOrdersDrivers() != null) {
                    /*for (OrdersDriverEntity ordersDriverE : orderEntity.getOrdersDrivers()) {
                        ordersDriverE.setDriverStatus(OrdersDriverStatus.FAILED);
                    }*/
                    orderEntity.getOrdersDrivers().add(ordersDriver);
                } else {
                    orderEntity.setOrdersDrivers(Collections.singletonList(ordersDriver));
                }
                orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);
                orderEntity.setDriver(selectedDriver);
                eligibleDrivers.addAll(eligibleDriversList);
                //lowerPriorityOrders = orderDao.getLowerPriorityOrders(drivers.get(0).getId());
            } else {
                //filter list of eligible drivers
                BigDecimal deviationTime = new BigDecimal(Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.ORDER_COMPLETION_MAXIMUM_TIME)));
                Boolean isFirst = true;
                for (DriverEntity driver : eligibleDriversList) {
                    if (BigDecimalUtil.isLessThenOrEqualTo(driver.getTotalOrderEtd(), deviationTime)) {
                        DriverEntity eligibleDriver = driverDao.find(driver.getId());
                        eligibleDriver.setCurrentOrderEtd(driver.getCurrentOrderEtd());
                        eligibleDriver.setTotalOrderEtd(driver.getTotalOrderEtd());
                        eligibleDrivers.add(eligibleDriver);
                    }
                    isFirst = false;
                }
            }
        }
        log.info("------------ reassign the driver and recalculated the etd for lower priority orders recursively ---------------");
        return eligibleDrivers;
    }

    public class MyTask implements Runnable {
        DriverEntity driver;
        OrderEntity order;
        AreaEntity area;
        BigDecimal timeAtCustomer;
        BigDecimal timeAtStore;
        Integer timePerKm;
        CountDownLatch countDownLatch;
        BigDecimal timeFactorForAerialPerKM;
        public MyTask(CountDownLatch latch, DriverEntity driver, OrderEntity order, AreaEntity area, BigDecimal timeAtCustomer, BigDecimal timeAtStore, Integer timePerKm, BigDecimal timeFactorForAerialPerKM) {
            this.driver = driver;
            this.order = order;
            this.area = area;
            this.timeAtCustomer = timeAtCustomer;
            this.timeAtStore = timeAtStore;
            this.timePerKm = timePerKm;
            this.countDownLatch=latch;
            this.timeFactorForAerialPerKM=timeFactorForAerialPerKM;


        }

        @Override
        public void run() {
            try {
                BigDecimal distanceFromDriverToStore = timeFactorForAerialPerKM.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(driver.getLatitude(), driver.getLongitude(), order.getStore().getLatitude(), order.getStore().getLongitude())));
                BigDecimal distanceFromStoreToCustomer = timeFactorForAerialPerKM.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(order.getStore().getLatitude(), order.getStore().getLongitude(), area.getLatitude(), area.getLongitude())));
                driver.setCurrentOrderEtd(distanceFromDriverToStore.add(distanceFromStoreToCustomer).add(timeAtStore).add(timeAtCustomer));
                driver.setTotalOrderEtd(driver.getCurrentOrderEtd());
            } catch (Exception e) {
            }finally {
                countDownLatch.countDown();
            }
        }
    }

    public class DistanceMatrixTask implements Runnable{

        CountDownLatch countDownLatch;
        String[] driverAddress;
        String[] storeAddress;
        String[] customerAddress;
        Integer timePerKm;
        DriverEntity driver;
        BigDecimal timeAtStore;
        BigDecimal timeAtCustomer;

        public DistanceMatrixTask(CountDownLatch countDownLatch, String[] driverAddress, String[] storeAddress, String[] customerAddress, Integer timePerKm, DriverEntity driver, BigDecimal timeAtStore, BigDecimal timeAtCustomer) {
            this.countDownLatch = countDownLatch;
            this.driverAddress = driverAddress;
            this.storeAddress = storeAddress;
            this.customerAddress = customerAddress;
            this.timePerKm = timePerKm;
            this.driver = driver;
            this.timeAtStore = timeAtStore;
            this.timeAtCustomer = timeAtCustomer;
        }

        @Override
        public void run() {
            try{
                BigDecimal driverToRestaurantTime = new BigDecimal(GeoCodingUtil.getListOfDistances(driverAddress, storeAddress, timePerKm).get("time") / 60);
                BigDecimal restaurantToCustomerTime = new BigDecimal(GeoCodingUtil.getListOfDistances(storeAddress, customerAddress, timePerKm).get("time") / 60);
                driver.setCurrentOrderEtd(driverToRestaurantTime.add(restaurantToCustomerTime).add(timeAtStore).add(timeAtCustomer));
                driver.setTotalOrderEtd(driverToRestaurantTime.add(restaurantToCustomerTime).add(timeAtStore).add(timeAtCustomer));
            } catch (Exception e) {
            }finally {
                countDownLatch.countDown();
            }
        }
    }
}
