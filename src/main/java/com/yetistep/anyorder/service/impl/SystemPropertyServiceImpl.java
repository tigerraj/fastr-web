package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.aspect.annotations.ActionLogSupport;
import com.yetistep.anyorder.dao.inf.ActionLogDao;
import com.yetistep.anyorder.dao.inf.PreferencesDao;
import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.dao.inf.UserDeviceDao;
import com.yetistep.anyorder.enums.NotifyTo;
import com.yetistep.anyorder.enums.PreferenceType;
import com.yetistep.anyorder.enums.PushNotificationRedirect;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.PreferenceSectionEntity;
import com.yetistep.anyorder.model.PreferenceTypeEntity;
import com.yetistep.anyorder.model.PreferencesEntity;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.MessageBundle;
import com.yetistep.anyorder.util.PushNotification;
import com.yetistep.anyorder.util.PushNotificationUtil;
import com.yetistep.anyorder.util.ReturnJsonUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class SystemPropertyServiceImpl implements SystemPropertyService {
    private static final Logger log = Logger.getLogger(SystemPropertyServiceImpl.class);
    private static String SYSTEM_PREF_FILE = "system_pref.properties";
    @Autowired
    PreferencesDao preferencesDaoService;

    @Autowired
    UserDao userDaoService;

    @Autowired
    ActionLogDao actionLogDaoService;

    @Autowired
    UserDeviceDao userDeviceDao;


    @Override
    public void preferenceInitializaton() {
        OutputStream output = null;
        try {
            log.info("Initializing the system_pref property file: " + new ClassPathResource(SYSTEM_PREF_FILE).getURL().toString());
            List<PreferencesEntity> preferencesEntities = preferencesDaoService.findAll();

            //Save to File
            savePropertyFileToWEB_INF(preferencesEntities);
            // Now Update

        } catch (Exception e) {
            log.error("Error occurred while initializing system preferences", e);
        } finally {

            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    log.error("Error occurred while initializing system preferences", e);
                }
            }
        }
    }

    @Override
    public List<PreferencesEntity> getAllPreferences() throws Exception {
        log.info("---------------------Fetching All Preferences--------------------------");
        return preferencesDaoService.findAll();
    }

    @Override
    public PreferenceTypeEntity getPreferencesType(Integer typeId) throws Exception {
        log.info("---------------------Fetching All Preferences--------------------------");
        PreferenceTypeEntity preferenceType = preferencesDaoService.findPreferenceType(typeId);

        String fields = "id,groupName";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();

        assoc.put("section", "id,section,preference");
        subAssoc.put("preference", "id,prefKey,value,prefTitle");

        return ((PreferenceTypeEntity) ReturnJsonUtil.getJsonObject(preferenceType, fields, assoc, subAssoc));
    }

    private void savePropertyFileToWEB_INF(List<PreferencesEntity> allPreferences) throws Exception {
        log.info("Saving Preferences in " + SYSTEM_PREF_FILE);
        Resource resource = new ClassPathResource(SYSTEM_PREF_FILE);
        File file = resource.getFile();

        FileInputStream in = new FileInputStream(file);
        Properties prop = new Properties();
        prop.load(in);
        in.close();

        FileOutputStream outputStream = new FileOutputStream(file);

        //Properties prop = new Properties();
        for (PreferencesEntity preferences : allPreferences) {
            prop.setProperty(preferences.getPrefKey().toString(), preferences.getValue());
        }
        prop.store(outputStream, null);
        outputStream.close();
    }

    @Override
    @ActionLogSupport
    public Boolean updateSystemPreferences(List<PreferencesEntity> systemPreferences) throws Exception {
        log.info("+++++++++++ Updating preferences table and property file ++++++++++++++");
        //update preference in db
        /*List<ActionLogEntity> actionLogs = new ArrayList<>();
        UserEntity userEntity = userDaoService.find(SessionManager.getUserIds());
        for (PreferencesEntity preference : systemPreferences) {
            PreferencesEntity dbPreference = preferencesDaoService.findByKey(preference.getPrefKey());
            ActionLogEntity actionLog = ActionLogUtil.createActionLog(dbPreference, preference, new String[]{"value"}, userEntity);
            if (actionLog != null) {
                actionLogs.add(actionLog);
            }
        }*/

        preferencesDaoService.updatePreferences(systemPreferences);
        for (PreferencesEntity preference : systemPreferences) {
            //send push notification to driver if driver location update time is changed
            if (preference.getPrefKey().equals(PreferenceType.DRIVER_LOCATION_UPDATE_TIME.toString())) {
                String driverLocationUpdateTime = readPrefValue(PreferenceType.DRIVER_LOCATION_UPDATE_TIME);
                if (!preference.getValue().equals(driverLocationUpdateTime)) {
                    //send only if value is changed.
                    List<String> tokens = userDeviceDao.getAllDeviceTokensForFamilyAndRole(UserRole.ROLE_DRIVER, "ANDROID");
                    if (tokens.size() > 0) {
                        PushNotification pushNotification = new PushNotification();
                        pushNotification.setTokens(tokens);
                        Map<String, String> message = new HashMap<>();
                        message.put("message", MessageBundle.getMessage("DPN004", "push_notification.properties"));
                        message.put("redirect", PushNotificationRedirect.PREFERENCE.toString());
                        message.put("driverUpdateTime", preference.getValue());
                        pushNotification.setMessage(message);
                        pushNotification.setNotifyTo(NotifyTo.DRIVER);
                        PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                        log.info("Push notification has been sent successfully");
                    }
                }
            }
        }
        /*log.info("saving action log");
        actionLogDaoService.saveAll(actionLogs);*/

        /*for (PreferencesEntity preference : systemPreferences) {
            if (preference.getPrefKey().equals(PreferenceType.ANDROID_APP_VER_NO.toString())) {
                String androidVersionNo = readPrefValue(PreferenceType.ANDROID_APP_VER_NO);
                String message = MessageBundle.getPushNotificationMsg("MOBVER001");
                if (!preference.getValue().equals(androidVersionNo)) {
                    sendNotification(UserRole.ROLE_CUSTOMER, "ANDROID", NotifyTo.CUSTOMER_ANDROID, message, preference.getValue());
                }
            } else if (preference.getPrefKey().equals(PreferenceType.IOS_APP_VER_NO.toString())) {
                String iosVersionNo = readPrefValue(PreferenceType.IOS_APP_VER_NO);
                String message = MessageBundle.getPushNotificationMsg("MOBVER001");
                if (!preference.getValue().equals(iosVersionNo)) {
                    sendNotification(UserRole.ROLE_CUSTOMER, "MAC_OS_X", NotifyTo.CUSTOMER_IOS, message, preference.getValue());
                }
            }
        }*/
        //update preference in property fle
        savePropertyFileToWEB_INF(systemPreferences);
        return true;
    }

   /* private void sendNotification(UserRole role, String family, NotifyTo notifyTo, String message, String extraDetail) throws Exception {
        //List<String> deviceTokens = userDeviceDaoService.getAllDeviceTokensForFamilyAndRole(role, family);
        PushNotification pushNotification = new PushNotification();
        //pushNotification.setTokens(deviceTokens);
        Map<String, String> pushMessage = new HashMap<>();
        pushMessage.put("message", message);
        pushNotification.setMessage(pushMessage);
        pushNotification.setPushNotificationRedirect(PushNotificationRedirect.PLAYSTORE);
        pushNotification.setExtraDetail(extraDetail);
        pushNotification.setNotifyTo(notifyTo);
        PushNotificationUtil.sendNotification(pushNotification, family);
    }*/


    @Override
    public Boolean updateSystemPreferencesType(PreferenceTypeEntity preferenceType) throws Exception {
        log.info("+++++++++++ Updating preferences table and property file ++++++++++++++");
        //update preference in db
        preferencesDaoService.updatePreferenceType(preferenceType);
        List<PreferencesEntity> systemPreferences = new ArrayList<>();
        for (PreferenceSectionEntity sectionEntity : preferenceType.getSection()) {
            systemPreferences.addAll(sectionEntity.getPreference());
        }

        //update preference in property fle
        savePropertyFileToWEB_INF(systemPreferences);
        return true;
    }

    @Override
    public String readPrefValue(PreferenceType preferenceType) throws Exception {
        Resource resource = new ClassPathResource(SYSTEM_PREF_FILE);
        return MessageBundle.getPropertyKey(preferenceType.toString(), resource.getFile());
    }



}
