package com.yetistep.anyorder.service.impl;

import com.google.maps.model.LatLng;
import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.model.xml.APIData;
import com.yetistep.anyorder.service.inf.APIService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Suroj on 1/18/2017.
 */
@Service
public class APIServiceImpl extends AbstractManager implements APIService {

    private static final Logger log = Logger.getLogger(APIServiceImpl.class);

    @Autowired
    APIKeyDao apiKeyDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    AreaDao areaDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    DriverDao driverDao;

    @Autowired
    UserDao userDao;

    @Autowired
    CustomersAreaDao customersAreaDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    SystemPropertyService systemPropertyService;

    public List<StoreEntity> getActiveStoresByAPIKey(String apiKey) throws Exception {
        log.info("Getting active store on api key");
        APIKeyEntity apiKeyEntity = apiKeyDao.findByAPIKey(apiKey);
        if (apiKeyEntity == null)
            throw new YSException("API001");
        if (!apiKeyEntity.getStatus().equals(Status.ACTIVE))
            throw new YSException("API002");
        if (!apiKeyEntity.getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("API003");
        List<StoreEntity> activeStores = new ArrayList<>();
        String fields = "storeId,storeName,givenLocation1,allowUnknownAreaServing";
        for (StoreBrandEntity storeBrand : apiKeyEntity.getMerchant().getStoreBrand()) {
            if (storeBrand.getStatus().equals(Status.ACTIVE)) {
                for (StoreEntity store : storeBrand.getStores()) {
                    if (store.getStatus().equals(Status.ACTIVE) && isVerifiableStore(store, false)) {
                        store.setStoreName(storeBrand.getBrandName());
                        if (store.getAllowStoresDriver() == null)
                            store.setAllowUnknownAreaServing(false);
                        activeStores.add((StoreEntity) ReturnJsonUtil.getJsonObject(store, fields));
                    }
                }
            }
        }
        return activeStores;
    }

    public List<AreaEntity> storeServingAreas(String apiKey, String storeId) throws Exception {
        APIKeyEntity apiKeyEntity = apiKeyDao.findByAPIKey(apiKey);
        if (apiKeyEntity == null)
            throw new YSException("API001");
        if (!apiKeyEntity.getStatus().equals(Status.ACTIVE))
            throw new YSException("API002");
        if (!apiKeyEntity.getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("API003");
        log.info("Getting serving area of store");
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        isVerifiableStore(storeEntity, true);
        List<StoresAreaEntity> storesAreas = storeEntity.getStoresAreas();
        List<AreaEntity> returnAreas = new ArrayList<>();
        Set<AreaEntity> areas = new HashSet<>();
        List<Integer> ids = new ArrayList<>();
        for (StoresAreaEntity storesArea : storesAreas) {
            if (storesArea.getArea().getStatus().equals(Status.ACTIVE) && storesArea.getArea().getParent() != null) {
                AreaEntity finalParentArea = storesArea.getArea();
                ids.add(finalParentArea.getId());
                while (finalParentArea.getParent() != null && finalParentArea.getParent().getStatus().equals(Status.ACTIVE)) {
                    finalParentArea = finalParentArea.getParent();
                    ids.add(finalParentArea.getId());
                }
                if (finalParentArea.getParent() == null) {
                    AreaEntity a = ValidationUtil.initializeAndUnproxy(finalParentArea);
                    areas.add(a);
                }
            }
        }

        String fields = "areaId,areaName,child";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("child", "areaId,areaName,latitude,longitude");
        for (AreaEntity area : areas) {
            StoreManagerWebServiceImpl.parseAreaTree(area, ids);
            returnAreas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area, fields, assoc));
        }
        return returnAreas;
    }

    public String saveOrder(APIData apiData) throws Exception {
        APIKeyEntity apiKey = apiKeyDao.findByAPIKey(apiData.getApiKey());
        if (apiKey == null)
            throw new YSException("API001");
        if (!apiKey.getStatus().equals(Status.ACTIVE))
            throw new YSException("API002");
        if (!apiKey.getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("API003");
        StoreEntity store = storeDao.findById(apiData.getStoreId());
        if (store == null)
            throw new YSException("STR001");
        if (!store.getStatus().equals(Status.ACTIVE))
            throw new YSException("STR006");
        if (!store.getStoreBrand().getStatus().equals(Status.ACTIVE))
            throw new YSException("STR005");
        if (apiData.getOrder().getOrderTime() == null)
            throw new YSException("API009");
        if (apiData.getOrder().getOrderTime() != null && apiData.getOrder().getOrderTime().compareTo(new Date()) < 0)
            throw new YSException("API006");

        List<String> storeIds = new ArrayList<>();
        for (StoreBrandEntity storeBrand : apiKey.getMerchant().getStoreBrand()) {
            for (StoreEntity sto : storeBrand.getStores()) {
                storeIds.add(sto.getStoreId());
            }
        }
        if (!storeIds.contains(store.getStoreId()))
            throw new YSException("API005");
        isVerifiableStore(store, true);
        OrderEntity order = apiData.getOrder();
        order.setStore(store);
        order.setApiKey(apiKey);
        order.setCustomersNoteToDriver(order.getOrderNote());
        order.setTotalBillAmount(order.getTotalAmount());
        order.setOrderDeliveryDate(order.getOrderTime());
        UserEntity user = userDao.findCustomerByNameAndMobileNumber(order.getCustomerName(), null, order.getMobileNumber());
        if (user == null) {
            user = new UserEntity();
            CustomerEntity customer = new CustomerEntity();
            user.setFirstName(order.getCustomerName());
            user.setEmailAddress(order.getEmail());
            user.setMobileNumber(order.getMobileNumber());
            customer.setUser(user);
            order.setCustomer(customer);
        } else {
            order.setCustomer(user.getCustomer());
        }
        CustomersAreaEntity customersArea = new CustomersAreaEntity();
        if (!store.getAllowUnknownAreaServing() && order.getAreaId() == null) {
            throw new YSException("API004");
        }
        customersArea.setStreet(order.getStreetAddress());
        if (order.getAreaId() != null) {
            AreaEntity area = areaDao.findByAreaId(order.getAreaId());
            if (area == null)
                throw new YSException("ARE001");
            List<Integer> areaIds = new ArrayList<>();
            areaIds.add(store.getArea().getId());
            for (StoresAreaEntity storesArea : store.getStoresAreas()) {
                if (storesArea.getArea().getParent() != null && storesArea.getArea().getStatus().equals(Status.ACTIVE) && storesArea.getArea().getParent().getStatus().equals((Status.ACTIVE))) {
                    areaIds.add(storesArea.getArea().getId());
                }
            }
            if (!areaIds.contains(area.getId()) && !store.getAllowUnknownAreaServing())
                throw new YSException("API010");
            customersArea.setArea(area);
        } else {
            if (order.getCustomerLatitude() != null && order.getCustomerLongitude() != null) {
                customersArea.setLatitude(order.getCustomerLatitude());
                customersArea.setLongitude(order.getCustomerLongitude());
            } else {
                //Geocoder
                if (order.getStreetAddress() == null)
                    throw new YSException("API007");
                LatLng latLng = GeoCodingUtil.getLatLongFromStreet(customersArea.getStreet());
                if (latLng == null)
                    throw new YSException("API008");
                customersArea.setLatitude(String.valueOf(latLng.lat));
                customersArea.setLongitude(String.valueOf(latLng.lng));
            }
            order.setCustomersArea(customersArea);
        }
        customersAreaDao.save(customersArea);
        order.setCustomersArea(customersArea);
        order.setOrderStatus(OrderStatus.ORDER_PLACED);
        orderDao.save(order);
        return order.getOrderId();
    }

    public List<OrderEntity> APIOrderList(APIData apiData) throws Exception {
        APIKeyEntity apiKey = apiKeyDao.findByAPIKey(apiData.getApiKey());
        if (apiKey == null)
            throw new YSException("API001");
        if (!apiKey.getStatus().equals(Status.ACTIVE))
            throw new YSException("API002");
        if (!apiKey.getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("API003");
        if (apiData.getStoreId() != null) {
            StoreEntity store = storeDao.findById(apiData.getStoreId());
            if (store == null)
                throw new YSException("STR001");
        }

        List<OrderEntity> orders = orderDao.getOrderListByAPIKey(apiData, apiKey.getMerchant().getId(), apiData.getStoreId());
        String fields = "orderId,estimatedTimeOfDelivery,orderStatus,pickupDate,paymentMode,deliveryCharge,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,store,customersArea,totalAmount,orderTime";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("store", "storeId,storeName,givenLocation1,givenLocation2,latitude,longitude");
        assoc.put("customersArea", "street,city,givenLocation,latitude,longitude");
        List<OrderEntity> orderEntity = new ArrayList<>();
        for (OrderEntity order : orders) {
            order.getStore().setStoreName(order.getStore().getStoreBrand().getBrandName());
            order.setTotalAmount(order.getTotalBillAmount());
            order.setOrderTime(order.getOrderDeliveryDate());
            order.setOrderNote(order.getCustomersNoteToDriver());
            if (order.getCustomersArea().getLatitude() == null) {
                order.getCustomersArea().setLatitude(order.getCustomersArea().getArea().getLatitude());
                order.getCustomersArea().setLongitude(order.getCustomersArea().getArea().getLongitude());
            }
            orderEntity.add((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc));
        }
        return orderEntity;
    }

    @Override
    public OrderEntity orderDetail(APIData apiData) throws Exception {
        APIKeyEntity apiKey = apiKeyDao.findByAPIKey(apiData.getApiKey());
        if (apiKey == null)
            throw new YSException("API001");
        if (!apiKey.getStatus().equals(Status.ACTIVE))
            throw new YSException("API002");
        if (!apiKey.getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("API003");
        OrderEntity order = orderDao.findById(apiData.getOrderId());
        if (order == null)
            throw new YSException("ORD004");
        BigDecimal totalPreviousOrderEtd = BigDecimal.ZERO;
        if (order.getDriver() != null) {
            totalPreviousOrderEtd = driverDao.getDriversPreviousOrderTotalEtd(order.getDriver().getDriverId(), order.getId());
            for (OrdersDriverEntity ordersDriverEntity : order.getOrdersDrivers()) {
                if (ordersDriverEntity.getDriverStatus().equals(OrdersDriverStatus.RUNNING))
                    if (ordersDriverEntity.getAssignedDate() != null && order.getEstimatedDeliveryTime() != null)
                        order.setEstimatedTimeOfDelivery(new Timestamp(ordersDriverEntity.getAssignedDate().getTime() + (order.getEstimatedDeliveryTime().add(totalPreviousOrderEtd)).multiply(new BigDecimal(60 * 1000)).longValue()));
            }
        }
        String fields = "orderId,orderDescription,estimatedTimeOfDelivery,orderStatus,pickupDate,paymentMode,deliveryCharge,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,commentOnDriver,commentOnFood,store,customer,customersArea,driver,customerSignImage,receiverName,totalAmount,orderTime,orderNote";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,area,storeBrand");
        assoc.put("customer", "user");
        assoc.put("driver", "user,latitude,longitude");
        assoc.put("customersArea", "street,city,givenLocation,latitude,longitude,additionalNote,area");
        assoc.put("orderCancel", "reason,orderStatus,cancelledDate");
        subAssoc.put("storeBrand", "brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude");
        if (order.getCustomersArea().getLatitude() == null) {
            order.getCustomersArea().setLatitude(order.getCustomersArea().getArea().getLatitude());
            order.getCustomersArea().setLongitude(order.getCustomersArea().getArea().getLongitude());
        }
        order.setTotalAmount(order.getTotalBillAmount());
        order.setOrderTime(order.getOrderDeliveryDate());
        order.setOrderNote(order.getCustomersNoteToDriver());
        return (OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc);
    }

    @Override
    public BigDecimal estimateCharge(APIData apiData) throws Exception {
        APIKeyEntity apiKey = apiKeyDao.findByAPIKey(apiData.getApiKey());
        if (apiKey == null)
            throw new YSException("API001");
        if (!apiKey.getStatus().equals(Status.ACTIVE))
            throw new YSException("API002");
        if (!apiKey.getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("API003");
        if (apiData.getLatitude() == null || apiData.getLongitude() == null)
            throw new YSException("API011");
        StoreEntity storeEntity = storeDao.findById(apiData.getStoreId());
        if (storeEntity == null)
            throw new YSException("STR001");
        if (!storeEntity.getAllowUnknownAreaServing())
            throw new YSException("API012");
        if (!storeEntity.getStatus().equals(Status.ACTIVE))
            throw new YSException("STR006");
        if (!storeEntity.getStoreBrand().getStatus().equals(Status.ACTIVE))
            throw new YSException("STR005");
        isVerifiableStore(storeEntity, true);
        List<UnknownAreaRatePlanEntity> unknownAreaRatePlans = storeEntity.getUnknownAreaRatePlans();
        List<UnknownAreaRatePlanEntity> activeUnknownAreaRatePlans = new ArrayList<>();
        UnknownAreaRatePlanEntity unknownAreaRate = new UnknownAreaRatePlanEntity();
        for (UnknownAreaRatePlanEntity unknownAreaRatePlan : unknownAreaRatePlans) {
            if (unknownAreaRatePlan.getStatus().equals(Status.ACTIVE))
                activeUnknownAreaRatePlans.add(unknownAreaRatePlan);
        }
        if (activeUnknownAreaRatePlans.size() == 0)
            throw new YSException("UNRPN001");
        BigDecimal distanceInKm = BigDecimal.ZERO;
        if (DistanceType.valueOf(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)).equals(DistanceType.ACTUAL_DISTANCE)) {
            Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
            Map<String, Long> data = GeoCodingUtil.calculateDistance(storeEntity.getLatitude(), storeEntity.getLongitude(), apiData.getLatitude(), apiData.getLongitude(), timePerKm);
            Long distance = data.get("distance");
            distanceInKm = BigDecimal.valueOf(distance).divide(BigDecimal.valueOf(1000));
        } else {
            distanceInKm = new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(storeEntity.getLatitude(), storeEntity.getLongitude(), apiData.getLatitude(), apiData.getLongitude()));
        }

        Collections.sort(activeUnknownAreaRatePlans, new Comparator<UnknownAreaRatePlanEntity>() {
            public int compare(UnknownAreaRatePlanEntity first, UnknownAreaRatePlanEntity second) {
                return first.getEndDistance().compareTo(second.getEndDistance());
            }
        });
        for (UnknownAreaRatePlanEntity unknownAreaRatePlan : activeUnknownAreaRatePlans) {
            if (BigDecimalUtil.isGreaterThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))
                    && BigDecimalUtil.isLessThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))) {
                unknownAreaRate = unknownAreaRatePlan;
                break;
            }
        }
        if (unknownAreaRate.getId() == null)
            unknownAreaRate = activeUnknownAreaRatePlans.get(activeUnknownAreaRatePlans.size() - 1);

        return unknownAreaRate.getBaseCharge().add(distanceInKm.multiply(unknownAreaRate.getAdditionalChargePerKM()));
    }
}

