package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.AdminService;
import com.yetistep.anyorder.service.inf.ManagerService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by BinaySingh on 5/11/2016.
 */
@Service
public class AdminServiceImpl extends AbstractManager implements AdminService {

    private static final Logger log = Logger.getLogger(AdminServiceImpl.class);

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    UserDao userDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    UserDeviceDao userDeviceDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    PreferencesDao preferencesDao;

    @Autowired
    ManagerService managerService;

    @Override
    public String createUser(String username, UserEntity user) throws Exception {
        log.info("----- Creating user -----");

        Boolean checkUserName = userDao.checkUserNameExist(username);
        if (username == null || checkUserName) {
            throw new YSException("USR003");
        }

        if (user.getEmailAddress() != null && !user.getEmailAddress().isEmpty()) {
            if (userDao.checkIfEmailExists(user.getEmailAddress())) {
                throw new YSException("USR004");
            }
        }
        if (userDao.checkIfMobileNumberExists(user.getUsername(), user.getUserRole())) {
            throw new YSException("USR005");
        }

        String code = MessageBundle.generateTokenString() + "_" + System.currentTimeMillis();

        /** Setting Default values **/
        user.setUsername(username);
        user.setBlacklistStatus(false);
        user.setVerificationCode(code);
        user.setUserStatus(UserStatus.INACTIVE);
        user.setVerifiedStatus(false);
        user.setDeviceType(UserDeviceType.WEB);
        user.setCreatedDate(DateUtil.getCurrentTimestampSQL());

        String profileImage = user.getProfileImage();
        user.setProfileImage(null);

        userDao.save(user);

        if (profileImage != null && !profileImage.isEmpty() && profileImage.startsWith("data:image/")) {
            log.info("Uploading Profile Image of User to S3 Bucket ");

            String dir = MessageBundle.separateString("/", "Users", "user_" + user.getId());
            boolean isLocal = MessageBundle.isLocalHost();
            String imageName = "pimg" + (isLocal ? "_tmp_" : "_") + user.getId() + System.currentTimeMillis();
            String s3Path = GeneralUtil.saveImageToBucket(profileImage, imageName, dir, true);
            user.setProfileImage(s3Path);
            userDao.update(user);
        }


        //Sending Email to Manager/CSR
        String hostName = getServerUrl();
        String url = hostName + "/assistance/create_password?key=" + code;
        log.info("Sending mail to " + user.getUsername() + " with new registration: " + url);

        //send email
        String body = EmailMsg.createPasswordForNewUser(url, user.getFirstName() + " " + user.getLastName(), user.getUsername(), getServerUrl());
        String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME);

        if (user.getUserRole().equals(UserRole.ROLE_MANAGER)) {
            subject += ": You have been added as Manager ";
        } else if (user.getUserRole().equals(UserRole.ROLE_CSR)) {
            subject += ": You have been added as CSR ";
        }

        sendMail(user.getEmailAddress(), body, subject);

        if (user.getUserRole().equals(UserRole.ROLE_MANAGER)) {
            return "Manager has been saved successfully.";
        } else if (user.getUserRole().equals(UserRole.ROLE_CSR)) {
            return "CSR has been saved successfully.";
        } else {
            return null;
        }
    }



    @Override
    public PaginationDto getUsers(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Retrieving list of users. -----");
        Page page = requestJsonDto.getPage();
        PaginationDto paginationDto = new PaginationDto();

        if (page != null && page.getSearchFor() != null && !page.getSearchFor().equals("")) {
            Map<String, String> fieldsMap = new HashMap<>();
            fieldsMap.put("self", "username,firstName,lastName,mobileNumber,emailAddress");
            page.setSearchFields(fieldsMap);
        }
        Integer totalRows = userDao.getNumberOfUsers(page);
        if (page != null) {
            page.setTotalRows(totalRows);
        }
        paginationDto.setNumberOfRows(totalRows);

        List<UserEntity> userEntities = userDao.getUsers(page);

        String field = "userId,username,firstName,lastName,mobileNumber,emailAddress,profileImage,userStatus,userRole";

        List<UserEntity> userList = new ArrayList<>();
        for (UserEntity userEntity : userEntities) {
            userList.add((UserEntity) ReturnJsonUtil.getJsonObject(userEntity, field));
        }
        paginationDto.setData(userList);
        return paginationDto;
    }

    @Override
    public void activateStore(String storeId, StoreEntity store) throws Exception {
        log.info("Activating store : " + storeId);
        StoreEntity storeEntity = storeDao.findById(storeId);
        if(storeEntity==null)
            throw new YSException("STR002");
        storeEntity.setAllowUnknownAreaServing(store.getAllowUnknownAreaServing());
        isVerifiableStore(storeEntity, true);
        storeEntity.setIsVerified(true);
        storeEntity.setAllowStoresDriver(store.getAllowStoresDriver());
        storeDao.update(storeEntity);

        //Generate Statement for the first time (initial charge)
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fromDate = dateFormat.format(DateUtil.getCurrentDate());
        String toDate = dateFormat.format(DateUtil.getCurrentDate());
        managerService.generateStatement(storeEntity, fromDate, toDate);
    }

    @Override
    public Boolean updateDefaultImage(RequestJsonDto requestJsonDto) throws Exception {
        String prefKey = requestJsonDto.getPrefKey();
        String image = requestJsonDto.getImageString();
        log.info("+++++++++ updating image " + prefKey + " ++++++++++++++++");

        if (requestJsonDto.getImageString() != null) {
            log.info("Uploading item images to S3 Bucket ");

            String dir = "";

            if (prefKey.equals(PreferenceType.DEFAULT_IMG_SEARCH.toString())) {
                dir = MessageBundle.separateString("/", "default", "search");
            } else if (prefKey.equals(PreferenceType.COMPANY_LOGO.toString())) {
                dir = MessageBundle.separateString("/", "default", "login_logo");
            } else if (prefKey.equals(PreferenceType.LOGO_FOR_PDF_EMAIL.toString())) {
                dir = MessageBundle.separateString("/", "default", "login_logo");
            }

            boolean isLocal = MessageBundle.isLocalHost();

            PreferencesEntity preference = preferencesDao.findByKey(prefKey);


            if (preference.getValue() != null && !preference.getValue().equals(""))
                AmazonUtil.deleteFileFromBucket(AmazonUtil.getAmazonS3Key(preference.getValue()));

            String itemImageUrl = "preference" + (isLocal ? "_tmp_" : "_") + preference.getId() + System.currentTimeMillis();
            String s3PathImage = GeneralUtil.saveImageToBucket(image, itemImageUrl, dir, true);
            preference.setValue(s3PathImage);

            List<PreferencesEntity> preferences = new ArrayList<>();
            preferences.add(preference);
            systemPropertyService.updateSystemPreferences(preferences);

        }
        return true;
    }

    @Override
    public void updateStoreSetting(String storeId, StoreEntity store) throws Exception {
        log.info("updating setting of store : " + storeId);
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        storeEntity.setAllowStoresDriver(store.getAllowStoresDriver());
        storeEntity.setAllowUnknownAreaServing(store.getAllowUnknownAreaServing());
        isVerifiableStore(storeEntity,true);
        storeDao.update(storeEntity);

        //send push notification to driver
        List<String> tokens = userDeviceDao.findDeviceTokenByStoreId(storeEntity.getId());
        if (tokens.size() > 0) {
            PushNotification pushNotification = new PushNotification();
            pushNotification.setTokens(tokens);
            Map<String, String> message = new HashMap<>();
            message.put("redirect", PushNotificationRedirect.PREFERENCE.toString());
            message.put("allowStoresDriver", storeEntity.getAllowStoresDriver().toString());
            message.put("allowUnknownAreaServing", storeEntity.getAllowUnknownAreaServing().toString());
            pushNotification.setMessage(message);
            pushNotification.setNotifyTo(NotifyTo.STORE_MANAGER);
            PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
            log.info("Push notification has been sent successfully");
        }
    }
}
