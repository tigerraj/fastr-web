package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.ManagerService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 9:55 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ManagerServiceImpl extends AbstractManager implements ManagerService {
    Logger log = Logger.getLogger(ManagerServiceImpl.class);

    @Autowired
    UserDao userDao;

    @Autowired
    AreaDao areaDao;

    @Autowired
    MerchantDao merchantDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    RatePlanDao ratePlanDao;

    @Autowired
    UnknownAreaRatePlanDao unknownAreaRatePlanDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    StatementDao statementDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    UserDeviceDao userDeviceDao;

    private static void parseAreaTree(AreaEntity parenArea, Integer areaId) {
        //for (AreaEntity  area: parenAreas) {
        if (parenArea != null && parenArea.getId() != null) {
            if (parenArea.getId().equals(areaId))
                throw new YSException("ARE004");
            parseAreaTree(parenArea.getParent(), areaId);
        }
    }

    @Override
    public void saveArea(AreaEntity area) throws Exception {
        log.info("Saving area " + area.getAreaName());
        List<AreaEntity> parentAreas = new ArrayList<>();
        if (area.getParent().getAreaId() != null && !area.getParent().getAreaId().isEmpty()) {
            AreaEntity parent = areaDao.findById(area.getParent().getAreaId());
            if (parent == null)
                throw new YSException("ARE002");
            parentAreas = areaDao.findParentAreasByAreaId(area.getParent().getAreaId());
            area.setParent(parent);
        } else {
            parentAreas = areaDao.findParentAreas();
            area.setParent(null);
        }
        if (parentAreas != null && parentAreas.size() > 0) {
            for (AreaEntity areaEntity : parentAreas) {
                if (areaEntity.getAreaName().equals(area.getAreaName())) {
                    throw new YSException("ARE003");
                }
            }
        }
        areaDao.save(area);
    }

    @Override
    public void updateArea(String areaId, AreaEntity area) throws Exception {
        log.info("Updating area " + area.getAreaName());
        AreaEntity areaEntity = areaDao.findById(areaId);
        if (areaEntity == null)
            throw new YSException("ARE001");
        List<AreaEntity> parentAreas = new ArrayList<>();
        areaEntity.setAreaName(area.getAreaName());
        areaEntity.setLatitude(area.getLatitude());
        areaEntity.setLongitude(area.getLongitude());
        areaEntity.setStreet(area.getStreet());
        areaEntity.setStatus(area.getStatus());
        /*if(area.getParent()!=null && area.getParent().getAreaId()!=null && !area.getParent().getAreaId().isEmpty()) {
            AreaEntity parent = areaDao.findByNotificationId(area.getParent().getAreaId());
            parseAreaTree(parent.getParent(), areaEntity.getId());
            if (parent == null)
                throw new YSException("ARE002");
            parentAreas = areaDao.findParentAreasByAreaId(area.getParent().getAreaId());
            areaEntity.setParent(parent);
        }else{
            parentAreas = areaDao.findParentAreas();
            areaEntity.setParent(null);
        }
        if(parentAreas != null && parentAreas.size()>0){
            for (AreaEntity dbArea: parentAreas){
                if(dbArea.getAreaName().equals(area.getAreaName()) && !area.getAreaName().equals(areaEntity.getAreaName())){
                    throw new YSException("ARE003");
                }
            }
        }*/
        if (areaEntity.getParent() != null)
            parentAreas = areaDao.findParentAreasByAreaId(areaEntity.getParent().getAreaId());
        else
            parentAreas = areaDao.findParentAreas();
        if (parentAreas != null && parentAreas.size() > 0) {
            for (AreaEntity dbArea : parentAreas) {
                if (dbArea.getAreaName().equals(area.getAreaName()) && !area.getAreaName().equals(areaEntity.getAreaName())) {
                    throw new YSException("ARE003");
                }
            }
        }

        areaDao.update(areaEntity);
    }

    @Override
    public void deleteArea(String areaId) throws Exception {
        AreaEntity areaEntity = areaDao.findById(areaId);
        if (areaEntity == null)
            throw new YSException("ARE001");
        if (areaEntity.getChild() != null && areaEntity.getChild().size() > 0)
            throw new YSException("ARE005");
        if ((areaEntity.getCustomersArea() != null && areaEntity.getCustomersArea().size() > 0) || (areaEntity.getDriversArea() != null && areaEntity.getDriversArea().size() > 0) || (areaEntity.getStoresArea() != null && areaEntity.getStoresArea().size() > 0))
            throw new YSException("ARE006");
        areaDao.delete(areaEntity);
    }

    @Override
    public PaginationDto getMerchantList(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting merchant list. -----");

        Page page = requestJsonDto.getPage();
        PaginationDto paginationDto = new PaginationDto();

        if (page != null && page.getSearchFor() != null && !page.getSearchFor().equals("")) {
            Map<String, String> fieldsMap = new HashMap<>();
            fieldsMap.put("self", "merchantId,businessTitle,checkoutCardId,cardLast4,cardType");
            fieldsMap.put("user", "firstName,lastName,emailAddress,mobileNumber");
            page.setSearchFields(fieldsMap);
        }
        Integer totalRows = merchantDao.getNumberOfMerchant(page);
        page.setTotalRows(totalRows);
        paginationDto.setNumberOfRows(totalRows);

        List<MerchantEntity> merchantEntities = merchantDao.getMerchantList(page);

        String field = "merchantId,businessTitle,user";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "userId,username,firstName,lastName,mobileNumber,emailAddress,userStatus");

        List<MerchantEntity> merchantList = new ArrayList<>();
        for (MerchantEntity merchantEntity : merchantEntities) {
            MerchantEntity merchantEntityFiltered = (MerchantEntity) ReturnJsonUtil.getJsonObject(merchantEntity, field, assoc);
            merchantList.add(merchantEntityFiltered);
        }
        paginationDto.setData(merchantList);
        return paginationDto;
    }

    @Override
    public void saveRatePlan(String storeId, RatePlanEntity ratePlan) throws Exception {
        log.info("Saving rate plan of store: " + storeId);
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        ratePlan.setCreatedDate(DateUtil.getCurrentTimestampSQL());
        ratePlan.setStore(storeEntity);
        ratePlan.setLastModified(DateUtil.getCurrentTimestampSQL());
        ratePlanDao.save(ratePlan);
    }

    @Override
    public void updateRatePlan(String ratePlanId, RatePlanEntity ratePlan) throws Exception {
        log.info("Updating rate plan of id: " + ratePlanId);
        RatePlanEntity ratePlanEntity = ratePlanDao.findById(ratePlanId);
        if (ratePlanEntity == null)
            throw new YSException("RPN001");
        //ratePlanEntity.setLastModified(DateUtil.getCurrentTimestampSQL());
        ratePlanEntity.setName(ratePlan.getName());
        ratePlanEntity.setStatus(ratePlan.getStatus());
        ratePlanEntity.setDriverType(ratePlan.getDriverType());
        ratePlanEntity.setAmount(ratePlan.getAmount());
        ratePlanEntity.setAdditionalChargePerOrder(ratePlan.getAdditionalChargePerOrder());
        ratePlanEntity.setDuration(ratePlan.getDuration());
        ratePlanEntity.setOrderCount(ratePlan.getOrderCount());
        ratePlanDao.update(ratePlanEntity);
    }

    @Override
    public void saveUnknownAreaRatePlan(String storeId, UnknownAreaRatePlanEntity unknownAreaRatePlan) throws Exception {
        log.info("Saving unknown area rate plan of store: " + storeId);
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        for (UnknownAreaRatePlanEntity unknownAreaRatePlanEntity : storeEntity.getUnknownAreaRatePlans()) {
            unknownAreaRatePlanEntity.setIsKnownArea(unknownAreaRatePlanEntity.getIsKnownArea()==null?false:unknownAreaRatePlanEntity.getIsKnownArea());
            if (unknownAreaRatePlanEntity.getStatus() != null && unknownAreaRatePlanEntity.getStatus().equals(Status.ACTIVE) && unknownAreaRatePlanEntity.getIsKnownArea().equals(unknownAreaRatePlan.getIsKnownArea())) {
                if ((unknownAreaRatePlan.getStartDistance() >= unknownAreaRatePlanEntity.getStartDistance() && unknownAreaRatePlan.getStartDistance() <= unknownAreaRatePlanEntity.getEndDistance()) ||
                        (unknownAreaRatePlan.getEndDistance() <= unknownAreaRatePlanEntity.getStartDistance() && unknownAreaRatePlan.getEndDistance() >= unknownAreaRatePlanEntity.getEndDistance()) ||
                        (unknownAreaRatePlanEntity.getStartDistance() >= unknownAreaRatePlan.getStartDistance() && unknownAreaRatePlanEntity.getStartDistance() <= unknownAreaRatePlan.getEndDistance()) ||
                        (unknownAreaRatePlanEntity.getEndDistance() <= unknownAreaRatePlan.getStartDistance() && unknownAreaRatePlanEntity.getEndDistance() >= unknownAreaRatePlan.getEndDistance())) {
                    throw new YSException("UNRPN002");
                }
            }
        }
        //unknownAreaRatePlan.setCreatedDate(DateUtil.getCurrentTimestampSQL());
        unknownAreaRatePlan.setStore(storeEntity);
        //unknownAreaRatePlan.setLastModified(DateUtil.getCurrentTimestampSQL());
        unknownAreaRatePlanDao.save(unknownAreaRatePlan);
    }

    @Override
    public void updateUnknownAreaRatePlan(String ratePlanId, UnknownAreaRatePlanEntity unknownAreaRatePlan) throws Exception {
        log.info("Updating unknown area rate plan of id: " + ratePlanId);
        UnknownAreaRatePlanEntity unknownAreaRatePlanEntity = unknownAreaRatePlanDao.findById(ratePlanId);
        if (unknownAreaRatePlanEntity == null)
            throw new YSException("UNRPN001");
        for (UnknownAreaRatePlanEntity unknownAreaRatePlanEntity1 : unknownAreaRatePlanEntity.getStore().getUnknownAreaRatePlans()) {
            unknownAreaRatePlanEntity1.setIsKnownArea(unknownAreaRatePlanEntity1.getIsKnownArea()==null?false:unknownAreaRatePlanEntity1.getIsKnownArea());
            if (unknownAreaRatePlanEntity1.getStatus() != null && unknownAreaRatePlanEntity1.getStatus().equals(Status.ACTIVE) && !unknownAreaRatePlanEntity1.getId().equals(unknownAreaRatePlan.getId())) {
                if ((unknownAreaRatePlan.getStartDistance() > unknownAreaRatePlanEntity1.getStartDistance() && unknownAreaRatePlan.getStartDistance() < unknownAreaRatePlanEntity1.getEndDistance()) ||
                        (unknownAreaRatePlan.getEndDistance() < unknownAreaRatePlanEntity1.getStartDistance() && unknownAreaRatePlan.getEndDistance() > unknownAreaRatePlanEntity1.getEndDistance()) ||
                        (unknownAreaRatePlanEntity1.getStartDistance() > unknownAreaRatePlan.getStartDistance() && unknownAreaRatePlanEntity1.getStartDistance() < unknownAreaRatePlan.getEndDistance()) ||
                        (unknownAreaRatePlanEntity1.getEndDistance() < unknownAreaRatePlan.getStartDistance() && unknownAreaRatePlanEntity1.getEndDistance() > unknownAreaRatePlan.getEndDistance())) {
                    throw new YSException("UNRPN002");
                }
            }
        }
        //unknownAreaRatePlanEntity.setLastModified(DateUtil.getCurrentTimestampSQL());
        unknownAreaRatePlanEntity.setName(unknownAreaRatePlan.getName());
        unknownAreaRatePlanEntity.setStatus(unknownAreaRatePlan.getStatus());
        unknownAreaRatePlanEntity.setBaseCharge(unknownAreaRatePlan.getBaseCharge());
        unknownAreaRatePlanEntity.setAdditionalChargePerKM(unknownAreaRatePlan.getAdditionalChargePerKM());
        unknownAreaRatePlanEntity.setStartDistance(unknownAreaRatePlan.getStartDistance());
        unknownAreaRatePlanEntity.setEndDistance(unknownAreaRatePlan.getEndDistance());
        unknownAreaRatePlanEntity.setIsKnownArea(unknownAreaRatePlan.getIsKnownArea());
        unknownAreaRatePlanDao.update(unknownAreaRatePlanEntity);
    }

    @Override
    public String generateStatement(StoreEntity store, String fromDate, String toDate) throws Exception {
        //String statementUrl;
        List<OrderEntity> orders = orderDao.getStoreOrders(store.getId(), fromDate, toDate);
        log.info("count orders = " + orders != null ? orders.size() : 0);

        StoreEntity storeEntity = storeDao.find(store.getId());

        RatePlanEntity ratePlanEntity = null;
        List<UnknownAreaRatePlanEntity> baseRatePlans = new ArrayList<>();
        List<UnknownAreaRatePlanEntity> unknownAreaRatePlanEntities = new ArrayList<>();

        if (storeEntity.getIsDistanceBaseRatePlan() == null || !storeEntity.getIsDistanceBaseRatePlan()) {
            for (RatePlanEntity ratePlan : storeEntity.getRatePlans()) {
                if (ratePlan.getStatus().equals(Status.ACTIVE)) {
                    ratePlanEntity = ratePlan;
                    break;
                }
            }
        }

        for (UnknownAreaRatePlanEntity unknownAreaRatePlan : storeEntity.getUnknownAreaRatePlans()) {
            if (unknownAreaRatePlan.getStatus().equals(Status.ACTIVE)) {
                if (unknownAreaRatePlan.getIsKnownArea() == null || !unknownAreaRatePlan.getIsKnownArea()) {
                    unknownAreaRatePlanEntities.add(unknownAreaRatePlan);
                } else {
                    baseRatePlans.add(unknownAreaRatePlan);
                }
            }
        }

        //PDFGenerator statementGenerator = new PDFGenerator();

        log.info("set the statement attributes");
        StatementEntity statement = new StatementEntity();
        statement.setPaidStatus(Boolean.FALSE);
        statement.setStore(storeEntity);
        statement.setOrder(orders);
        statement.setGeneratedDate(new Timestamp(System.currentTimeMillis()));
        statement.setFromDate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fromDate).getTime()));
        statement.setToDate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(toDate).getTime()));

        BigDecimal totalPayableAmount = BigDecimal.ZERO;
        BigDecimal totalExtraOrderCharge = BigDecimal.ZERO;
        BigDecimal totalOutOfServiceOrderCharge = BigDecimal.ZERO;
        BigDecimal distanceBasedOrderAmount = BigDecimal.ZERO;

        Integer totalInServingAreaOrderCount = 0;
        Integer totalExtraOrderCount = 0;
        Integer totalOutOfServiceOrderCount = 0;
        Integer totalDistanceBasedOrderCount = 0;

        log.info("calculating payable and receivable amounts");
        for (OrderEntity order : orders) {
            if (order.getCustomersArea().getArea() != null) {
                totalInServingAreaOrderCount++;
            } else {
                UnknownAreaRatePlanEntity applicableUnknownAreaRate = null;
                CustomersAreaEntity customersAreaEntity = order.getCustomersArea();
                /*Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
                Map<String, Long> distanceData = GeoCodingUtil.calculateDistance(storeEntity.getLatitude(), storeEntity.getLongitude(),
                        customersAreaEntity.getLatitude(), customersAreaEntity.getLongitude(), timePerKm);
                BigDecimal distanceInKm = new BigDecimal(distanceData.get("distance") / 1000.0);*/

                BigDecimal distanceInKm = order.getDistanceTravelInKM();

                //Start Filtering applicable unknown rateplan from the list
                Collections.sort(unknownAreaRatePlanEntities, new Comparator<UnknownAreaRatePlanEntity>() {
                    public int compare(UnknownAreaRatePlanEntity first, UnknownAreaRatePlanEntity second) {
                        return first.getEndDistance().compareTo(second.getEndDistance());
                    }
                });
                for (UnknownAreaRatePlanEntity unknownAreaRatePlan : unknownAreaRatePlanEntities) {
                    if (BigDecimalUtil.isGreaterThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))
                            && BigDecimalUtil.isLessThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))) {
                        applicableUnknownAreaRate = unknownAreaRatePlan;
                        break;
                    }
                }
                if (applicableUnknownAreaRate == null) {
                    applicableUnknownAreaRate = unknownAreaRatePlanEntities.get(unknownAreaRatePlanEntities.size() - 1);
                }
                //End Filtering applicable unknown rateplan from the list

                totalOutOfServiceOrderCharge = totalOutOfServiceOrderCharge.add(applicableUnknownAreaRate.getBaseCharge());

                totalOutOfServiceOrderCharge = totalOutOfServiceOrderCharge.add(distanceInKm.multiply(applicableUnknownAreaRate.getAdditionalChargePerKM()));
                totalOutOfServiceOrderCount++;
            }

            //For baserateplan
            if (baseRatePlans.size() > 0) {
                UnknownAreaRatePlanEntity applicableBaseRate = null;
                CustomersAreaEntity customersAreaEntity = order.getCustomersArea();
                /*Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
                Map<String, Long> distanceData = GeoCodingUtil.calculateDistance(storeEntity.getLatitude(), storeEntity.getLongitude(),
                        customersAreaEntity.getArea().getLatitude(), customersAreaEntity.getArea().getLongitude(), timePerKm);
                BigDecimal distanceInKm = new BigDecimal(distanceData.get("distance") / 1000.0);*/

                BigDecimal distanceInKm = order.getDistanceTravelInKM();

                //Start Filtering applicable unknown rateplan from the list
                Collections.sort(baseRatePlans, new Comparator<UnknownAreaRatePlanEntity>() {
                    public int compare(UnknownAreaRatePlanEntity first, UnknownAreaRatePlanEntity second) {
                        return first.getEndDistance().compareTo(second.getEndDistance());
                    }
                });
                for (UnknownAreaRatePlanEntity baseRatePlan : baseRatePlans) {
                    if (BigDecimalUtil.isGreaterThenOrEqualTo(distanceInKm, BigDecimal.valueOf(baseRatePlan.getStartDistance()))
                            && BigDecimalUtil.isLessThenOrEqualTo(distanceInKm, BigDecimal.valueOf(baseRatePlan.getStartDistance()))) {
                        applicableBaseRate = baseRatePlan;
                        break;
                    }
                }
                if (applicableBaseRate == null) {
                    applicableBaseRate = baseRatePlans.get(baseRatePlans.size() - 1);
                }
                //End Filtering applicable base rateplan from the list

                distanceBasedOrderAmount = distanceBasedOrderAmount.add(applicableBaseRate.getBaseCharge());
                distanceBasedOrderAmount = distanceBasedOrderAmount.add(distanceInKm.multiply(applicableBaseRate.getAdditionalChargePerKM()));

                statement.setSubscriptionRate(applicableBaseRate.getBaseCharge());
                statement.setSubscriptionRatePlan(applicableBaseRate.getName());

                totalDistanceBasedOrderCount++;
            }

        }
        if (ratePlanEntity != null) {
            totalExtraOrderCount = (totalInServingAreaOrderCount - ratePlanEntity.getOrderCount()) < 0 ? 0 : (totalInServingAreaOrderCount - ratePlanEntity.getOrderCount());

            totalExtraOrderCharge = BigDecimalUtil.multiply(new BigDecimal(totalExtraOrderCount), ratePlanEntity.getAdditionalChargePerOrder());

            totalPayableAmount = ratePlanEntity.getAmount()
                    .add(totalExtraOrderCharge)
                    .add(totalOutOfServiceOrderCharge);
            statement.setSubscriptionRate(ratePlanEntity.getAmount());
            statement.setSubscriptionRatePlan(ratePlanEntity.getName());
            if (totalInServingAreaOrderCount > ratePlanEntity.getOrderCount()) {
                statement.setSubscriptionRatePlanOrderCount(ratePlanEntity.getOrderCount());
            } else {
                statement.setSubscriptionRatePlanOrderCount(totalInServingAreaOrderCount);
            }
            statement.setOrderPlan(OrderPlan.SERVICE_AREA_PLAN);
        } else {
            statement.setDistanceBasedOrderAmount(distanceBasedOrderAmount);
            statement.setDistanceBasedOrderCount(totalDistanceBasedOrderCount);
            totalPayableAmount = totalPayableAmount.add(distanceBasedOrderAmount);
            statement.setOrderPlan(OrderPlan.DISTANCE_BASED_PLAN);
        }

        log.info("setting payable and receivable amounts");
        statement.setExtraOrderCount(totalExtraOrderCount);
        statement.setOutOfServingAreaOrderCount(totalOutOfServiceOrderCount);
        statement.setExtraOrderCharge(totalExtraOrderCharge);
        statement.setOutOfServingAreaOrderCharge(totalOutOfServiceOrderCharge);
        statement.setTotalPayableAmount(totalPayableAmount);

        log.info("save the statement");
        statementDao.save(statement);
/*

        Map<String, String> preferences = new HashMap<>();
        preferences.put("CURRENCY", systemPropertyService.readPrefValue(PreferenceType.CURRENCY));
        preferences.put("HELPLINE_NUMBER", systemPropertyService.readPrefValue(PreferenceType.HELPLINE_NUMBER));
        preferences.put("SUPPORT_EMAIL", systemPropertyService.readPrefValue(PreferenceType.SUPPORT_EMAIL));
        preferences.put("COMPANY_NAME", systemPropertyService.readPrefValue(PreferenceType.COMPANY_NAME));
        preferences.put("COMPANY_ADDRESS", systemPropertyService.readPrefValue(PreferenceType.COMPANY_ADDRESS));
        preferences.put("SERVER_URL", systemPropertyService.readPrefValue(PreferenceType.SERVER_URL));

        String imageUrl = systemPropertyService.readPrefValue(PreferenceType.LOGO_FOR_PDF_EMAIL);

        statementUrl = statementGenerator.generateStatement(store.getStoreBrand().getMerchant(), statement, store, imageUrl, preferences);

        if (statementUrl == null) {
            throw new YSException("STAT003");
        }

        statement.setStatementUrl(statementUrl);
        statementDao.update(statement);

        String email = store.getStoreBrand().getMerchant().getUser().getEmailAddress();
        if (email != null && !email.equals("")) {
            String message = EmailMsg.sendInvoiceEmail(store, fromDate, toDate, systemPropertyService.readPrefValue(PreferenceType.SERVER_URL));
            sendAttachmentEmail(email, message, "get invoice-" + fromDate + "-" + toDate, statementUrl);
        }
        log.info("Email sent successfully with attachment: " + statementUrl + " for store " + store.getId() + " Email: " + email);


        return statementUrl;
        */
        return null;
    }

    @Override
    public void approveStatement(StatementEntity statement) throws Exception {
        log.info("----- Clearing Statement payment. -----");
        if (SessionManager.getRole().equals(UserRole.ROLE_ADMIN) || SessionManager.getRole().equals(UserRole.ROLE_MANAGER)) {
            StatementEntity statementEntity = statementDao.findByStatementId(statement.getStatementId());
            if (statementEntity == null) {
                log.error("----- Statement does not exist. -----");
                throw new YSException("STAT001");
            }
            if (statementEntity.getPaidStatus().equals(Boolean.TRUE)) {
                log.error("----- Statement has already been paid. -----");
                throw new YSException("STAT002");
            }
            statementEntity.setPaidStatus(Boolean.TRUE);
            statementEntity.setPaidDate(DateUtil.getCurrentTimestampSQL());
            statementDao.update(statementEntity);
        } else {
            log.error("----- You have no authority for this operation. -----");
            throw new YSException("ERR002");
        }
    }

    @Override
    public List<UserEntity> getActiveMerchant() throws Exception {
        log.info("getting active merchant");
        List<UserEntity> users = userDao.findActiveMerchant();
        List<UserEntity> userEntities = new ArrayList<>();
        String field = "firstName,lastName,userId,merchant";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("merchant","businessTitle");
        for (UserEntity user : users) {
            userEntities.add((UserEntity) ReturnJsonUtil.getJsonObject(user, field, assoc));
        }
        return userEntities;
    }

    @Override
    public List<NotificationEntity> getNotificationByDeviceType(UserDeviceType userDeviceType) throws Exception {
        log.info("Getting notification");
        List<NotificationEntity> distinctMessage = new ArrayList<>();
        List<NotificationEntity> notifications = new ArrayList<>();
        List<NotificationEntity> notificationEntities = new ArrayList<>();

        String fields = "notificationId,content,viewed,validTill,createdDate,users,notifyTos";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("users", "firstName,lastName,userId");

        if (userDeviceType.equals(UserDeviceType.MOBILE)) {
            notifications = notificationDao.findMobileNotification();
            Map<Object, Object> distinctObj = new HashMap<>();
            for (NotificationEntity notification : notifications) {
                distinctObj.put(notification.getContent(), notification);
            }

            for (Map.Entry<Object, Object> entry : distinctObj.entrySet()) {
                //distinctMessage.add((NotificationEntity) entry.getValue());
                NotificationEntity notificationEntity = (NotificationEntity) entry.getValue();
                List<NotifyTo> notifyTos = new ArrayList<>();
                for (NotificationEntity notification : notifications) {
                    if (notificationEntity.getContent().equals(notification.getContent())) {
                        notifyTos.add(notification.getNotifyTo());
                        notificationEntity.setNotifyTos(notifyTos);
                    }
                }
                notificationEntities.add((NotificationEntity) ReturnJsonUtil.getJsonObject(notificationEntity, fields, assoc));
            }

            /*for (NotificationEntity notificationEntity : distinctMessage) {
                List<NotifyTo> notifyTos = new ArrayList<>();
                for (NotificationEntity notification : notifications) {
                    if (notificationEntity.getContent().equals(notification.getContent())) {
                        notifyTos.add(notification.getNotifyTo());
                        notificationEntity.setNotifyTos(notifyTos);
                    }
                }
            }*/
        } else {
            notifications = notificationDao.findWebNotification();
            Map<Object, Object> distinctObj = new HashMap<>();
            for (NotificationEntity notification : notifications) {
                distinctObj.put(notification.getContent(), notification);
            }

            for (Map.Entry<Object, Object> entry : distinctObj.entrySet()) {
                NotificationEntity notificationEntity = (NotificationEntity) entry.getValue();
                List<UserEntity> users = new ArrayList<>();
                for (NotificationEntity notification : notifications) {
                    if (notificationEntity.getContent().equals(notification.getContent())) {
                        users.add(notification.getUser());
                        notificationEntity.setUsers(users);
                    }
                }
                notificationEntities.add((NotificationEntity) ReturnJsonUtil.getJsonObject((NotificationEntity) entry.getValue(), fields, assoc));
            }

            /*for (NotificationEntity notificationEntity : distinctMessage) {
                List<UserEntity> users = new ArrayList<>();
                for (NotificationEntity notification : notifications) {
                    if (notificationEntity.getContent().equals(notification.getContent())) {
                        users.add(notification.getUser());
                        notificationEntity.setUsers(users);
                    }
                }
            }*/
        }
        Collections.sort(notificationEntities, new Comparator<NotificationEntity>() {
            public int compare(NotificationEntity first, NotificationEntity second) {
                return second.getCreatedDate().compareTo(first.getCreatedDate());
            }
        });

        /*for (NotificationEntity notification : distinctMessage) {
            notificationEntities.add((NotificationEntity) ReturnJsonUtil.getJsonObject(notification, fields, assoc));
        }*/
        return notificationEntities;
    }


    @Override
    public void sendPushNotification(RequestJsonDto requestJsonDto) throws Exception {
        log.info("Sending push notification");
        NotificationEntity notification = requestJsonDto.getNotification();
        for (NotifyTo notifyTo : requestJsonDto.getNotifyTos()) {
            notification.setNotifyTo(notifyTo);
            if (requestJsonDto.getUserIds() != null && requestJsonDto.getUserIds().size() > 0) {
                for (String userId : requestJsonDto.getUserIds()) {
                    UserEntity userEntity = userDao.findByUserId(userId);
                    if (userEntity == null)
                        throw new YSException("USR001");
                    NotificationEntity notifications = new NotificationEntity();
                    notifications.setContent(notification.getContent());
                    notifications.setValidTill(notification.getValidTill());
                    notifications.setNotifyTo(notifyTo);
                    notifications.setUser(userEntity);
                    notificationDao.save(notifications);
                }
            } else {
                //send mobile notification
                NotificationEntity notifications = new NotificationEntity();
                notifications.setContent(notification.getContent());
                notifications.setValidTill(notification.getValidTill());
                notifications.setNotifyTo(notifyTo);
                notificationDao.save(notifications);
                sendNotification("ANDROID", notification.getNotifyTo(), notification.getContent());
            }
        }
    }

    private void sendNotification(String family, NotifyTo notifyTo, String message) throws Exception {
        UserRole userRole = null;
        if(notifyTo.equals(NotifyTo.STORE_MANAGER))
            userRole = UserRole.ROLE_STORE_MANAGER;
        else if(notifyTo.equals(NotifyTo.MERCHANT))
            userRole = UserRole.ROLE_MERCHANT;
        else if(notifyTo.equals(NotifyTo.FASTR_DRIVER) || notifyTo.equals(NotifyTo.RESTAURANT_DRIVER))
            userRole = UserRole.ROLE_DRIVER;
        List<String> deviceTokens = userDeviceDao.getAllDeviceTokensForFamilyAndRole(userRole, family);
        int numberOfLoop = deviceTokens.size() / 500 + (deviceTokens.size() % 500 == 0 ? 0 : 1);
        for (int i = 0; i < numberOfLoop; i++) {
            int maxList = deviceTokens.size() > ((i + 1) * 500) ? ((i + 1) * 500) : deviceTokens.size();
            List<String> deviceTokensTemp = deviceTokens.subList(i * 500, maxList);
            PushNotification pushNotification = new PushNotification();
            pushNotification.setTokens(deviceTokensTemp);
            Map<String, String> pushMessage = new HashMap<>();
            pushMessage.put("message", message);
            pushMessage.put("redirect", PushNotificationRedirect.INFO.toString());
            pushNotification.setMessage(pushMessage);
            //pushNotification.setPushNotificationRedirect(PushNotificationRedirect.INFO);
            pushNotification.setNotifyTo(notifyTo);
            PushNotificationUtil.sendNotification(pushNotification, family);
        }
    }
}
