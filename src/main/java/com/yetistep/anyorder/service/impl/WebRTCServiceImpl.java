package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.service.inf.WebRTCService;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sagar on 8/27/2016.
 */

@Service
public class WebRTCServiceImpl implements WebRTCService {


    private static List<WebSocketSession> sessions = new ArrayList<>();

    @Override
    public void registerOpenConnection(WebSocketSession session) throws Exception {
        sessions.add(session);
        System.out.println("opening new connection");
    }

    @Override
    public void registerCloseConnection(WebSocketSession session, CloseStatus status) throws Exception  {
        sessions.remove(session);
        System.out.println ("Closing a WebSocket due to "+status.toString());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception  {
        sessions.remove(session);
        System.out.println ("Closing a WebSocket due to "+exception);
    }

    @Override
    public List<WebSocketSession> getSessions()  throws Exception {
        return sessions;
    }

    @Override
    public void processMessage(WebSocketSession session, String message)  throws Exception {

    }

}
