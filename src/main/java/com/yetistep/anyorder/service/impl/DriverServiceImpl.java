package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.DriverService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DriverServiceImpl extends AbstractManager implements DriverService {

    private static final Logger log = Logger.getLogger(DriverServiceImpl.class);

    @Autowired
    UserDao userDao;

    @Autowired
    UserDeviceDao userDeviceDao;

    @Autowired
    DriverDao driverDao;

    @Autowired
    AreaDao areaDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    DriversAreaDao driversAreaDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    StoresNotDriverDao storesNotDriverDao;

    @Autowired
    StoreBrandDao storeBrandDao;

    @Autowired
    MerchantDao merchantDao;

    @Autowired
    DriversWorkLogDao driversWorkLogDao;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    StoreManagerDao storeManagerDao;

    @Autowired
    DriversAreaDao driverAreaDao;

    /*private static void parseAreaTree(List<AreaEntity> parentAreas, List<Integer> areaIds) {
        if (parentAreas != null) {
            for (AreaEntity area : parentAreas) {
                for (AreaEntity child : area.getChild()) {
                    if (child.getStatus().equals(Status.INACTIVE))
                        area.setChild(null);
                }
                if (areaIds.contains(area.getId()))
                    area.setSelected(true);
                else
                    area.setSelected(false);
                parseAreaTree(area.getChild(), areaIds);
            }
        }
    }*/

    @Override
    public void updateServingArea(HeaderDto headerDto, List<AreaEntity> areas) throws Exception {
        DriverEntity driverEntity = driverDao.findById(headerDto.getId());
        if (driverEntity == null)
            throw new YSException("DRV001");
        List<AreaEntity> savedArea = areaDao.findAreaIds();
        if(!savedArea.containsAll(areas)){
            throw new YSException("STR004");
        }
        List<DriversAreaEntity> toSavedAreas=new ArrayList<>();
        for(AreaEntity area:savedArea){
            if(areas.contains(area)){
                DriversAreaEntity driversAreaEntity = new DriversAreaEntity();
                driversAreaEntity.setArea(area);
                driversAreaEntity.setDriver(driverEntity);
                toSavedAreas.add(driversAreaEntity);
            }
        }
        driverAreaDao.deleteAll(driverEntity.getId());
        driverAreaDao.updateAll(toSavedAreas);

        /*List<DriversAreaEntity> driversAreaEntities = driverEntity.getDriversArea();

        List<Integer> driverAreaIds = new ArrayList<>();
        List<Integer> dbDriverAreaIdList = new ArrayList<Integer>();
        Map<Integer, DriversAreaEntity> dbDriverAreaMap = new HashMap<>();

        for (DriversAreaEntity driversAreaEntity : driverEntity.getDriversArea()) {
            dbDriverAreaIdList.add(driversAreaEntity.getId());
            dbDriverAreaMap.put(driversAreaEntity.getId(), driversAreaEntity);
        }

        for (AreaEntity area : areas) {
            AreaEntity areaEntity = areaDao.findById(area.getAreaId());
            if (areaEntity == null)
                throw new YSException("ARE001");
            //DriversAreaEntity driversAreaEntity = driverEntity.getDriversArea();
            *//*for (DriversAreaEntity driversArea : driversAreaEntities) {
                if (driversArea.getArea().equals(areaEntity)) {
                    driversAreaEntity = driversArea;
                    break;
                }
            }*//*
            int i = 0;
            DriversAreaEntity driversAreaEntity = driversAreaDao.findByDriverAndAreaId(driverEntity.getId(), areaEntity.getId());
            if (driversAreaEntity == null) {
                driversAreaEntity = new DriversAreaEntity();
                driversAreaEntity.setDriver(driverEntity);
                driversAreaEntity.setArea(areaEntity);
                driversAreaEntities.add(driversAreaEntity);
                driversAreaDao.save(driversAreaEntity);
                if(++i%20==0){
                    driversAreaDao.flush();
                }
            }
            driverAreaIds.add(driversAreaEntity.getId());
        }

        for (Integer dbDriverAreaId : dbDriverAreaIdList) {
            if (!driverAreaIds.contains(dbDriverAreaId)) {
                driversAreaEntities.remove(dbDriverAreaMap.get(dbDriverAreaId));
            }
        }
        driverEntity.setDriversArea(driversAreaEntities);
        driverDao.update(driverEntity);*/
    }

    @Override
    public List<AreaEntity> getServingArea(HeaderDto headerDto) throws Exception {
        DriverEntity driverEntity = driverDao.findById(headerDto.getId());
        if (driverEntity == null)
            throw new YSException("DRV001");
        List<AreaEntity> parentAreas = areaDao.findActiveParentAreas();
        List<AreaEntity> areas = new ArrayList<>();

        String fields = "areaId,areaName,selected,child";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();

        assoc.put("child", "areaId,areaName,selected,child");
        subAssoc.put("child", "areaId,areaName,latitude,longitude,street,selected,child");

        List<DriversAreaEntity> driversAreas = driverEntity.getDriversArea();
        List<Integer> areaIds = new ArrayList<>();
        for (DriversAreaEntity driversArea : driversAreas) {
            areaIds.add(driversArea.getArea().getId());
        }
        AreaTreeUtil.parseActiveAreaTree(parentAreas, areaIds);
        for (AreaEntity area : parentAreas) {
            areas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area, fields, assoc, subAssoc));
        }
        return areas;
    }

    @Override
    public void updateDriver(HeaderDto headerDto, DriverEntity driver) throws Exception {
        log.info("----- Updating Driver detail -----");
        DriverEntity driverEntity = driverDao.findById(driver.getDriverId());
        if (driverEntity == null) {
            log.error("----- Driver does not exist. -----");
            throw new YSException("DRV001");
        }

        UserEntity user = driver.getUser();
        UserEntity userEntity = driverEntity.getUser();

        if (user.getUsername() != null && !user.getUsername().equals(userEntity.getUsername())) {
            userEntity.setUsername(user.getUsername());
        }
        if (user.getFirstName() != null && !user.getFirstName().equals(userEntity.getFirstName())) {
            userEntity.setFirstName(user.getFirstName());
        }
        if (user.getLastName() != null && !user.getLastName().equals(userEntity.getLastName())) {
            userEntity.setLastName(user.getLastName());
        }
        if (user.getEmailAddress() != null && !user.getEmailAddress().equals(userEntity.getEmailAddress())) {
            userEntity.setEmailAddress(user.getEmailAddress());
        }
        if (user.getMobileNumber() != null && !user.getMobileNumber().equals(userEntity.getMobileNumber())) {
            userEntity.setMobileNumber(user.getMobileNumber());
        }
        if (driver.getVehicleType() != null && !driver.getVehicleType().equals(driverEntity.getVehicleType())) {
            driverEntity.setVehicleType(driver.getVehicleType());
        }
        if (driver.getVehicleNumber() != null && !driver.getVehicleNumber().equals(driverEntity.getVehicleNumber())) {
            driverEntity.setVehicleNumber(driver.getVehicleNumber());
        }
        if (driver.getLicenseNumber() != null && !driver.getLicenseNumber().equals(driverEntity.getLicenseNumber())) {
            driverEntity.setLicenseNumber(driver.getLicenseNumber());
        }
        if (driver.getExperienceLevel() != null && !driver.getExperienceLevel().equals(driverEntity.getExperienceLevel())) {
            driverEntity.setExperienceLevel(driver.getExperienceLevel());
        }

        if (driver.getDriversArea() != null) {
            List<String> driversAreaId = new ArrayList<>();
            List<String> areaIdToAdd = new ArrayList<>();
            for (DriversAreaEntity driversArea : driver.getDriversArea()) {
                if (driversArea.getDriversAreaId() != null && !driversArea.getDriversAreaId().isEmpty()) {
                    driversAreaId.add(driversArea.getDriversAreaId());
                } else {
                    areaIdToAdd.add(driversArea.getArea().getAreaId());
                }
            }

            for (DriversAreaEntity driversAreaEntity : driverEntity.getDriversArea()) {
                if (!driversAreaId.contains(driversAreaEntity.getDriversAreaId())) {
                    driversAreaEntity.setStatus(Status.INACTIVE);
                }
            }

            for (String areaId : areaIdToAdd) {
                AreaEntity areaEntity = areaDao.findByAreaId(areaId);
                if (areaEntity == null) {
                    log.error("----- Area does not exist. -----");
                    throw new YSException("ARE001");
                }
                DriversAreaEntity driversArea = new DriversAreaEntity();
                driversArea.setArea(areaEntity);
                driversArea.setDriver(driverEntity);

                driverEntity.getDriversArea().add(driversArea);
            }

            userEntity.setDriver(driverEntity);
        }
        driverEntity.setUser(userEntity);

        StoreEntity store = driver.getStore();
        if (store!=null && store.getStoreId() != null && driverEntity.getStore() != null
                && !store.getStoreId().equals(driverEntity.getStore().getStoreId())) {
            StoreEntity storeEntity = storeDao.findById(store.getStoreId());
            if (storeEntity == null) {
                log.error("----- Store does not exist. -----");
                throw new YSException("STR002");
            }
            storeEntity.getDrivers().add(driverEntity);
            driverEntity.setStore(storeEntity);
            driverEntity.setDriverType(DriverType.RESTAURANT);
        } else if (driver.getDriverType() != null && !driverEntity.getDriverType().equals(driver.getDriverType())) {
            driverEntity.setDriverType(DriverType.FASTR);
        }

        driverDao.update(driverEntity);

        if (user.getProfileImage() != null && !user.getProfileImage().isEmpty() && user.getProfileImage().startsWith("data:image/")) {
            log.info("Deleting Profile Image of driver from S3 Bucket ");
            AmazonUtil.deleteFileFromBucket(AmazonUtil.getAmazonS3Key(userEntity.getProfileImage()));

            log.info("Uploading Profile Image of driver to S3 Bucket ");

            String dir = MessageBundle.separateString("/", "Users", "user_" + userEntity.getId());
            boolean isLocal = MessageBundle.isLocalHost();
            String imageName = "pimg" + (isLocal ? "_tmp_" : "_") + userEntity.getId() + System.currentTimeMillis();
            String s3Path = GeneralUtil.saveImageToBucket(user.getProfileImage(), imageName, dir, true);
            userEntity.setProfileImage(s3Path);
            driverEntity.setUser(userEntity);
            driverDao.update(driverEntity);
        }

    }

    @Override
    public DriverEntity getDriver(String driverId) throws Exception {
        log.info("----- Getting Driver detail -----");
        DriverEntity driverEntity = driverDao.findById(driverId);
        if (driverEntity == null) {
            log.error("----- Driver does not exist. -----");
            throw new YSException("DRV001");
        }

        String field = "driverId,driverType,availabilityStatus,vehicleType,vehicleNumber,licenseNumber,latitude,longitude,experienceLevel,user,store,storesNotDriver,rating,streetAddress,currentOrderCount,lastOrderDelivered";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "userId,username,firstName,lastName,mobileNumber,emailAddress,profileImage");
        assoc.put("store", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,storeBrand,contactNo");
        assoc.put("storesNotDriver", "storesNotDriverId,store");
        Map<String, String> subAssoc = new HashMap<>();
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo,merchant");
        subAssoc.put("store", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,storeBrand,contactNo");
        subAssoc.put("merchant", "merchantId,user");
        subAssoc.put("user", "userId");

        driverEntity.setCurrentOrderCount(driverDao.getDriversOrderCount(driverEntity.getDriverId()));
        OrderEntity lastOrder = driverDao.getDriversLastOrder(driverEntity.getDriverId());
        if (lastOrder != null && lastOrder.getDeliveredDate() != null) {
            driverEntity.setLastOrderDelivered(lastOrder.getDeliveredDate());
        } else if (driverEntity.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE)) {
            for (DriversWorkLogEntity workLog : driverEntity.getWorkLogs()) {
                if (workLog.getToDateTime() == null) {
                    driverEntity.setLastOrderDelivered(workLog.getFromDateTime());
                }
            }
        }
        driverEntity.setRating(orderDao.findRatingOfDriver(driverEntity.getId()));
        return (DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity, field, assoc, subAssoc);
    }

    @Override
    public PaginationDto getDriverList(RequestJsonDto requestJsonDto) throws Exception {

        log.info("----- Getting Driver list of merchant -----");

        Page page = requestJsonDto.getPage();
        PaginationDto paginationDto = new PaginationDto();
        Integer totalRows = 0;
        List<DriverEntity> driverEntities = new ArrayList<>();

        if (page != null && page.getSearchFor() != null && !page.getSearchFor().equals("")) {
            Map<String, String> fieldsMap = new HashMap<>();
            fieldsMap.put("self", "vehicleNumber,licenseNumber");
            fieldsMap.put("user", "firstName,lastName,emailAddress,mobileNumber");
            page.setSearchFields(fieldsMap);
        }

        if (requestJsonDto.getStore() != null && requestJsonDto.getStore().getStoreId() != null) {
            String storeId = requestJsonDto.getStore().getStoreId();
            StoreEntity storeEntity = storeDao.findById(storeId);

            if (storeEntity == null) {
                log.error("----- Store does not exist. -----");
                throw new YSException("STR002");
            }
            totalRows = driverDao.getTotalNumberOfStoreDriver(storeId, page);
            if (page != null) {
                page.setTotalRows(totalRows);
            }
            driverEntities = driverDao.findStoreDriver(storeId, page);

        } else if (requestJsonDto.getStoreBrand() != null && requestJsonDto.getStoreBrand().getStoreBrandId() != null) {
            String storeBrandId = requestJsonDto.getStoreBrand().getStoreBrandId();
            StoreBrandEntity storeBrandEntity = storeBrandDao.findById(storeBrandId);
            if (storeBrandEntity == null) {
                log.error("----- StoreBrand does not exist. -----");
                throw new YSException("STR001");
            }
            totalRows = driverDao.getTotalNumberOfStoreBrandDriver(storeBrandId, page);
            if (page != null) {
                page.setTotalRows(totalRows);
            }
            driverEntities = driverDao.findStoreBrandDriver(storeBrandId, page);

        } else if (requestJsonDto.getMerchant() != null && requestJsonDto.getMerchant().getMerchantId() != null) {
            String merchantId = requestJsonDto.getMerchant().getMerchantId();
            MerchantEntity merchantEntity = merchantDao.findById(merchantId);
            if (merchantEntity == null) {
                log.error("----- Merchant does not exist. -----");
                throw new YSException("MRC003");
            }
            totalRows = driverDao.getTotalNumberOfMerchantDriver(merchantId, page);
            if (page != null) {
                page.setTotalRows(totalRows);
            }
            driverEntities = driverDao.findMerchantDriver(merchantId, page);

        } else {
            UserRole userRole = SessionManager.getRole();
            if (userRole.equals(UserRole.ROLE_ADMIN) || userRole.equals(UserRole.ROLE_MANAGER)) {
                totalRows = driverDao.getTotalNumberOfAllDriver(page);
                if (page != null) {
                    page.setTotalRows(totalRows);
                }
                driverEntities = driverDao.findAll(page);
            } else if (userRole.equals(UserRole.ROLE_CSR)) {
                totalRows = driverDao.getTotalNumberOfAnyOrderDriver(page);
                if (page != null) {
                    page.setTotalRows(totalRows);
                }
                driverEntities = driverDao.findAnyOrderDriver(page);
            } else if (userRole.equals(UserRole.ROLE_STORE_MANAGER)) {
                StoreManagerEntity storeManagerEntity = storeManagerDao.findByFakeId(SessionManager.getStoreManagerId());

                if (storeManagerEntity.getStore() == null) {
                    log.error("----- Store does not exist. -----");
                    throw new YSException("STR002");
                }
                totalRows = driverDao.getTotalNumberOfStoreDriver(storeManagerEntity.getStore().getStoreId(), page);
                if (page != null) {
                    page.setTotalRows(totalRows);
                }
                driverEntities = driverDao.findStoreDriver(storeManagerEntity.getStore().getStoreId(), page);
            }
        }

        paginationDto.setNumberOfRows(totalRows);

        List<DriverEntity> driverList = new ArrayList<>();

        String fields = "driverId,driverType,availabilityStatus,vehicleType,vehicleNumber,licenseNumber,experienceLevel,currentOrderCount,lastOrderDelivered,user,store,latitude,longitude,lastOrderDelivered,currentOrderCount,rating,streetAddress";

        Map<String, String> assoc = new HashMap<>();

        assoc.put("user", "userId,firstName,lastName,emailAddress,verifiedStatus,mobileNumber,userStatus");
        assoc.put("store", "storeId,givenLocation1,givenLocation2,storeBrand");

        Map<String, String> subAssoc = new HashMap<>();

        subAssoc.put("storeBrand", "storeBrandId,brandName,merchant");
        subAssoc.put("merchant", "merchantId,user");
        subAssoc.put("user", "userId");

        for (DriverEntity driver : driverEntities) {
            driver.setCurrentOrderCount(driverDao.getDriversOrderCount(driver.getDriverId()));
            OrderEntity lastOrder = driverDao.getDriversLastOrder(driver.getDriverId());
            if (lastOrder != null && lastOrder.getDeliveredDate() != null) {
                driver.setLastOrderDelivered(lastOrder.getDeliveredDate());
            } else if (driver.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE)) {
                for (DriversWorkLogEntity workLog : driver.getWorkLogs()) {
                    if (workLog.getToDateTime() == null) {
                        driver.setLastOrderDelivered(workLog.getFromDateTime());
                    }
                }
            }
            driver.setRating(orderDao.findRatingOfDriver(driver.getId()));
            driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
        }

        paginationDto.setData(driverList);
        return paginationDto;
    }

    @Override
    public void changeDriverStatus(DriverEntity driver) throws Exception {
        log.info("----- Changing StoreManager's status. -----");
        DriverEntity driverEntity = driverDao.findById(driver.getDriverId());
        if (driverEntity == null) {
            log.error("----- Driver does not exist. -----");
            throw new YSException("DRV001");
        }
        if (driver.getUser().getUserStatus() != null && !driver.getUser().getUserStatus().equals(driverEntity.getUser().getUserStatus())) {
            driverEntity.getUser().setUserStatus(driver.getUser().getUserStatus());
            driverDao.update(driverEntity);
            log.info("Sending mail to " + driverEntity.getUser().getEmailAddress());
            String body = EmailMsg.notificationEmailUserStatus(driverEntity.getUser(), getServerUrl());
            String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Your account has been";
            if (driverEntity.getUser().getUserStatus().equals(UserStatus.ACTIVE)) {
                subject += " re-activated.";
            } else if (driverEntity.getUser().getUserStatus().equals(UserStatus.INACTIVE)) {
                subject += " deactivated.";
            }
            sendMail(driverEntity.getUser().getEmailAddress(), body, subject);
        }
        //Push Notification for inactive driver.
        if (driverEntity.getUser().getUserStatus().equals(UserStatus.INACTIVE)) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(driverEntity.getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("redirect", PushNotificationRedirect.LOGOUT.toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
    }

    @Override
    public void assignDriver(HeaderDto headerDto, OrderEntity order) throws Exception {
        log.info("Assiging driver :"+ headerDto.getDriverId()+" to order "+order.getOrderId());
        OrderEntity orderEntity = orderDao.findById(order.getOrderId());
        if(orderEntity == null)
            throw new YSException("ORD001");
        DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
        if(driverEntity == null)
            throw new YSException("DRV001");
        orderEntity.setDriver(driverEntity);
        orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);
        orderDao.update(orderEntity);

        //send push notification to driver
        List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
        if (tokens.size() > 0) {
            PushNotification pushNotification = new PushNotification();
            pushNotification.setTokens(tokens);
            Map<String, String> message = new HashMap<>();
            message.put("message", MessageBundle.getMessage("DPN001", "push_notification.properties"));
            message.put("redirect", PushNotificationRedirect.ORDER.toString());
            message.put("id", orderEntity.getOrderId().toString());
            pushNotification.setMessage(message);
            pushNotification.setNotifyTo(NotifyTo.DRIVER);
            PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
            log.info("Push notification has been sent successfully");
        }
    }
}
