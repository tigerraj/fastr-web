package com.yetistep.anyorder.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.aspect.annotations.CreateOrderValidate;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.dto.ResponseJsonDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.model.xml.APIData;
import com.yetistep.anyorder.service.inf.*;
import com.yetistep.anyorder.util.*;
import com.yetistep.anyorder.webrtc.pojo.ResponseOrderDto;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.yetistep.anyorder.util.DateUtil.*;

/**
 * Created by BinaySingh on 5/18/2016.
 */
@Service
public class StoreManagerWebServiceImpl extends AbstractManager implements StoreManagerWebService {

    Logger log = Logger.getLogger(StoreManagerWebServiceImpl.class);

    @Autowired
    AreaDao areaDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    UserDao userDao;

    @Autowired
    DriverDao driverDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    CustomersAreaDao customersAreaDao;

    @Autowired
    StoreManagerDao storeManagerDao;

    @Autowired
    DriverService driverService;

    @Autowired
    SystemAlgorithmService systemAlgorithmService;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    UserDeviceDao userDeviceDao;

    @Autowired
    StoresNotDriverDao storesNotDriverDao;

    @Autowired
    MerchantDao merchantDao;

    @Autowired
    StoreBrandDao storeBrandDao;

    @Autowired
    WebRTCService webRTCService;

    @Autowired
    APIKeyDao apiKeyDao;

    @Autowired
    StatementDao statementDao;

    public void testData(APIData xmlData) throws Exception {
        APIKeyEntity apiKey = apiKeyDao.findByAPIKey(xmlData.getApiKey());
        if (apiKey == null)
            throw new YSException("API001");
        if (apiKey.getMerchant() == null || !apiKey.getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("MRC001");
    }

    public static void parseAreaTree(AreaEntity parentAreas, List<Integer> ids) {
        List<AreaEntity> areas = new ArrayList<>();
        for (AreaEntity area : parentAreas.getChild()) {
            if (ids.contains(area.getId())) {
                final AreaEntity a = ValidationUtil.initializeAndUnproxy(area);
                areas.add(a);
                parseAreaTree(a, ids);
            }
        }
        parentAreas.setChild(areas);
    }

    public static void parseActiveAreaTree(List<AreaEntity> parenAreas) {
        if (parenAreas != null) {
            for (AreaEntity area : parenAreas) {
                List<AreaEntity> activeChild = new ArrayList<>();
                for (AreaEntity child : area.getChild()) {
                    if (child.getStatus().equals(Status.ACTIVE)) {
                        activeChild.add(child);
                    }
                }
                if (activeChild.size() > 0)
                    area.setChild(activeChild);
                else
                    area.setChild(null);
                parseActiveAreaTree(area.getChild());
            }
        }
    }

    @Override
    public void saveStoreManager(StoreManagerEntity storeManager) throws Exception {
        log.info("----- Saving StoreManager -----");
        UserEntity user = storeManager.getUser();
        Boolean checkUserNameExist = userDao.checkUserNameExist(user.getUsername());
        if (checkUserNameExist || user.getUsername() == null || user.getUsername().isEmpty()) {
            throw new YSException("USR004");
        }

        StoreEntity store = storeManager.getStore();

        if (user.getEmailAddress() != null && !user.getEmailAddress().isEmpty()) {
            if (userDao.checkIfEmailExistsWithSameRole(user.getEmailAddress(), UserRole.ROLE_STORE_MANAGER)) {
                throw new YSException("STRMGR002");
            }
        }
        if (user.getMobileNumber() != null && !user.getMobileNumber().isEmpty()) {
            if (userDao.checkIfMobileNumberExists(user.getMobileNumber(), UserRole.ROLE_STORE_MANAGER)) {
                throw new YSException("STRMGR003");
            }
        }
        if (store == null || store.getStoreId() == null) {
            throw new YSException("STR001");
        }

        StoreEntity storeEntity = storeDao.findById(store.getStoreId());

        if (storeEntity.getId() == null) {
            throw new YSException("STR001");
        }

        if (userDao.checkIfMobileNumberExists(user.getUsername(), UserRole.ROLE_STORE_MANAGER)) {
            throw new YSException("STRMGR003");
        }

        /** Setting Default values **/
        user.setUserRole(UserRole.ROLE_STORE_MANAGER);
        user.setDeviceType(UserDeviceType.WEB);
        user.setBlacklistStatus(Boolean.FALSE);
        user.setMobileVerificationStatus(true);
        user.setVerifiedStatus(false);
        user.setUserStatus(UserStatus.UNVERIFIED);
        user.setCreatedDate(DateUtil.getCurrentTimestampSQL());
        if (user.getEmailAddress().isEmpty()) {
            user.setEmailAddress(null);
        }

        String verificationCode = MessageBundle.generateTokenString() + "_" + System.currentTimeMillis();
        user.setVerificationCode(verificationCode);

        storeManager.setAvailabilityStatus(AvailabilityStatus.UNAVAILABLE);
        storeManager.setStore(storeEntity);

        storeManagerDao.save(storeManager);

        //send mail to create password for store manager
        String hostName = getServerUrl();
        String url = hostName + "/assistance/create_password?key=" + verificationCode;
        log.info("Sending mail to " + user.getUsername() + " with new registration: " + url);
        //send email
        String body = EmailMsg.createPasswordForNewUser(url, user.getFirstName() + " " + user.getLastName(), user.getUsername(), getServerUrl());
        String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": You have been added as Store Manager ";
        sendMail(user.getEmailAddress(), body, subject);
    }

    @Override
    public void updateStoreManager(StoreManagerEntity storeManager) throws Exception {
        log.info("----- Updating Store Manager -----");

        StoreManagerEntity dbStoreManager = storeManagerDao.findByFakeId(storeManager.getStoreManagerId());
        StoreManagerEntity storeManagerEntity = storeManagerDao.findById(dbStoreManager.getId(), storeManager.getStoreManagerId());

        if (storeManagerEntity == null) {
            throw new YSException("STRMGR001");
        }

        UserEntity user = storeManager.getUser();
        StoreEntity store = storeManager.getStore();

        if (user.getEmailAddress() != null && !user.getEmailAddress().isEmpty()) {
            if (userDao.checkIfEmailExistsWithSameRole(user.getEmailAddress(), UserRole.ROLE_STORE_MANAGER, user.getId())) {
                throw new YSException("STRMGR002");
            }
        }
/*        if (store == null || store.getStoreId() == null) {
            throw new YSException("STR001");
        }
        //get database store
        StoreEntity dbStore = storeDao.find(store.getId());

        if (dbStore.getId() == null) {
            throw new YSException("STR001");
        }*/

        if (userDao.checkIfMobileNumberExists(user.getUsername(), UserRole.ROLE_STORE_MANAGER, user.getId())) {
            throw new YSException("STRMGR003");
        }

        /** Setting Default values **/
        if (user.getFirstName() != null) {
            storeManagerEntity.getUser().setFirstName(user.getFirstName());
        }
        if (user.getLastName() != null) {
            storeManagerEntity.getUser().setLastName(user.getLastName());
        }
        if (user.getMobileNumber() != null) {
            storeManagerEntity.getUser().setMobileNumber(user.getMobileNumber());
        }
        if (user.getEmailAddress() != null) {
            storeManagerEntity.getUser().setEmailAddress(user.getEmailAddress());
        }
        if (user.getUserStatus() != null) {
            storeManagerEntity.getUser().setUserStatus(user.getUserStatus());
        }
//        storeManagerEntity.setStore(dbStore);

        storeManagerDao.update(dbStoreManager);
    }

    @Override
    public List<AreaEntity> getAreas() throws Exception {
        List<AreaEntity> parentAreas = areaDao.findParentAreas();
        List<AreaEntity> areas = new ArrayList<>();

        String fields = "areaId,areaName,child,status";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();

        assoc.put("child", "areaId,areaName,child,status");
        subAssoc.put("child", "areaId,areaName,child,latitude,longitude,street,status");
        for (AreaEntity area : parentAreas) {
            areas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area, fields, assoc, subAssoc));
        }
        return areas;
    }

    @Override
    public List<AreaEntity> getActiveAreas() throws Exception {
        log.info("Getting active area and subarea");
        List<AreaEntity> parentAreas = areaDao.findActiveParentAreas();
        List<AreaEntity> areas = new ArrayList<>();

        String fields = "areaId,areaName,child,status";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();

        assoc.put("child", "areaId,areaName,child,status");
        subAssoc.put("child", "areaId,areaName,child,latitude,longitude,street,status");
        parseActiveAreaTree(parentAreas);
        for (AreaEntity area : parentAreas) {
            areas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area, fields, assoc, subAssoc));
        }
        return areas;
    }

    @Override
    public AreaEntity getArea(String areaId) throws Exception {
        AreaEntity areaEntity = areaDao.findById(areaId);
        if (areaEntity == null)
            throw new YSException("ARE001");
        String fields = "areaId,areaName,latitude,longitude,street,parent,status,child";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("parent", "areaId,areaName,parent,status");
        assoc.put("child", "areaId,areaName,status");
        subAssoc.put("parent", "areaId,areaName,parent,status");
        return (AreaEntity) ReturnJsonUtil.getJsonObject(areaEntity, fields, assoc, subAssoc);
    }

    @Override
    @CreateOrderValidate
    public ResponseJsonDto createOrder(HeaderDto headerDto, RequestJsonDto requestJson) throws Exception {
        ResponseJsonDto responseJsonDto = new ResponseJsonDto();
        if (requestJson.getSaveAndAssign() == null)
            requestJson.setSaveAndAssign(false);

        OrderEntity orderEntity = requestJson.getOrder();
        orderEntity.setAssignDriver(requestJson.getSaveAndAssign());
        String driverId = requestJson.getDriverId();
        if (orderEntity.getCustomer().getUser() != null)
            orderEntity.getCustomer().getUser().setUserRole(UserRole.ROLE_CUSTOMER);
        else {
            UserEntity user = new UserEntity();
            user.setUserRole(UserRole.ROLE_CUSTOMER);
            orderEntity.getCustomer().setUser(user);
        }

        if (driverId != null) {
            DriverEntity driver = driverDao.findById(driverId);
            if (driver == null)
                throw new YSException("DRV001");

            OrdersDriverEntity ordersDriver = new OrdersDriverEntity();
            ordersDriver.setDriver(driver);
            ordersDriver.setDriverStatus(OrdersDriverStatus.RUNNING);
            ordersDriver.setAssignedDate(new Timestamp(System.currentTimeMillis()));
            ordersDriver.setOrder(orderEntity);
            orderEntity.setOrdersDrivers(Collections.singletonList(ordersDriver));
            orderEntity.setDriverType(DriverType.RESTAURANT);
            orderEntity.setDriver(driver);
            responseJsonDto.setDriverId(driverId);
        }

        if (orderEntity.getOrderDeliveryDate() != null) {
            long deliveryDateUtc = DateUtil.converLocalTimeToUtcTime(orderEntity.getOrderDeliveryDate().getTime());
            orderEntity.setOrderDeliveryDate(new Timestamp(deliveryDateUtc));
        }
        if (orderEntity.getPickupDate() != null) {
            long pickupDateUtc = DateUtil.converLocalTimeToUtcTime(orderEntity.getPickupDate().getTime());
            orderEntity.setPickupDate(new Timestamp(pickupDateUtc));
        }

        //set store and store manager for order
        if (headerDto.getStoreId() != null) {
            StoreEntity store = storeDao.findById(headerDto.getStoreId());
            if (store == null)
                throw new Exception("STE002");
            if (store.getStoreManagers().size() > 0)
                orderEntity.setStoreManager(store.getStoreManagers().get(0));
            orderEntity.setStore(store);
            orderEntity.setOrderOriginType(OrderOriginType.FASTR);
        } else if (SessionManager.getRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            StoreManagerEntity storeManager = storeManagerDao.findByFakeId(SessionManager.getStoreManagerId());
            orderEntity.setStoreManager(storeManager);
            orderEntity.setStore(storeManager.getStore());
            orderEntity.setOrderOriginType(OrderOriginType.RESTAURANT);
        } else
            throw new Exception("PAR001");

        if (orderEntity.getCustomer().getCustomersArea().get(0).getArea() != null) {
            AreaEntity area = areaDao.findByAreaId(orderEntity.getCustomer().getCustomersArea().get(0).getArea().getAreaId());
            orderEntity.getCustomer().getCustomersArea().get(0).setArea(area);
        }

        if (orderEntity.getCustomer().getCustomersArea().get(0).getCustomersAreaId() != null) {
            CustomersAreaEntity customersArea = customersAreaDao.findByCustomersAreaId(orderEntity.getCustomer().getCustomersArea().get(0).getCustomersAreaId());
            orderEntity.setCustomersArea(customersArea);
        }

        if (orderEntity.getCustomer().getCustomerId() != null) {
            CustomerEntity customer = customerDao.findByCustomerId(orderEntity.getCustomer().getCustomerId());
            if (orderEntity.getCustomer().getUser() != null && orderEntity.getCustomer().getUser().getFirstName() != null) {
                customer.getUser().setFirstName(orderEntity.getCustomer().getUser().getFirstName());
            }
            if (orderEntity.getCustomer().getCustomersArea().get(0).getCustomersAreaId() == null) {
                CustomersAreaEntity customersArea = new CustomersAreaEntity();
                if (orderEntity.getCustomer().getCustomersArea().get(0).getArea() != null) {
                    AreaEntity area = areaDao.findByAreaId(orderEntity.getCustomer().getCustomersArea().get(0).getArea().getAreaId());
                    customersArea.setArea(area);
                } else {
                    customersArea.setLatitude(orderEntity.getCustomer().getCustomersArea().get(0).getLatitude());
                    customersArea.setLongitude(orderEntity.getCustomer().getCustomersArea().get(0).getLongitude());
                    customersArea.setAdditionalNote(orderEntity.getCustomer().getCustomersArea().get(0).getAdditionalNote());
                    customersArea.setStreet(orderEntity.getCustomer().getCustomersArea().get(0).getStreet());
                }
                customersArea.setStreet(orderEntity.getCustomer().getCustomersArea().get(0).getStreet());
                customersArea.setCustomer(customer);
                customersAreaDao.save(customersArea);
                orderEntity.setCustomersArea(customersArea);
            }
            orderEntity.setCustomer(customer);

        } else {
            orderEntity.getCustomer().getCustomersArea().get(0).setCustomer(orderEntity.getCustomer());
            orderEntity.setCustomersArea(orderEntity.getCustomer().getCustomersArea().get(0));
        }

        if (orderEntity.getDriver() != null)
            updateETDAndDeliveryTimeOfOrder(orderEntity, orderEntity.getDriver());

        //set default status
        orderEntity.setOrderStatus(OrderStatus.ORDER_PLACED);

        if (requestJson.getSaveAndAssign() && systemPropertyService.readPrefValue(PreferenceType.ASSIGN_DRIVER_AUTOMATICALLY).equals("1") && driverId == null) {
            //assign driver to order
            List<DriverEntity> driverEntities = systemAlgorithmService.assignDriver(orderEntity, true);
            orderEntity.setAssignDriver(requestJson.getSaveAndAssign());
            if (driverEntities.size() == 0)
                responseJsonDto.setMessage("drivers not available. please try to assign driver later");
            else
                orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);
            /*if (orderEntity.getOrdersDrivers() == null || orderEntity.getOrdersDrivers().size() == 0) {
                throw new YSException("DRV001");
            }*/
            if (orderEntity.getDriver() != null)
                responseJsonDto.setDriverId(orderEntity.getDriver().getDriverId());
        } else if (requestJson.getSaveAndAssign() && systemPropertyService.readPrefValue(PreferenceType.ASSIGN_DRIVER_AUTOMATICALLY).equals("0") && driverId == null) {
            if (SessionManager.getRole().equals(UserRole.ROLE_CSR) || SessionManager.getRole().equals(UserRole.ROLE_ADMIN) ||
                    SessionManager.getRole().equals(UserRole.ROLE_MANAGER)) {
                List<DriverEntity> driverEntities = systemAlgorithmService.assignDriver(orderEntity, false);

                List<DriverEntity> retDrivers = new ArrayList<>();
                String fields = "driverId,driverType,availabilityStatus,vehicleType,vehicleNumber,licenseNumber,latitude,longitude,streetAddress,experienceLevel,user,rating,currentOrderCount,currentOrderEtd,totalOrderEtd";
                Map<String, String> assoc = new HashMap<>();
                assoc.put("user", "firstName,lastName,mobileNumber,emailAddress,profileImage");
                for (DriverEntity driver : driverEntities) {
                    retDrivers.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc));
                }
                responseJsonDto.setDrivers(retDrivers);
            }
        }

        if (requestJson.getSaveAndAssign()) {
            if (driverId != null) {
                DriverEntity driverEntity = driverDao.findById(driverId);
                if (driverEntity == null)
                    throw new YSException("DRV001");

                orderEntity.setDriver(driverEntity);
            }
            orderEntity.setAssignDriver(true);
            if (orderEntity.getDriver() != null)
                orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);
        } else {
            orderEntity.setOrderStatus(OrderStatus.ORDER_PLACED);
            orderEntity.setAssignDriver(false);
        }

        //Rate plan of order
        if (orderEntity.getCustomersArea().getLatitude() != null) {
            orderEntity.setOrderPlan(OrderPlan.OUTSIDE_SERVICE_AREA_PLAN);
        } else {
            if (orderEntity.getCustomersArea().getArea() != null) {
                if (orderEntity.getStore().getIsDistanceBaseRatePlan()) {
                    orderEntity.setOrderPlan(OrderPlan.DISTANCE_BASED_PLAN);
                } else {
                    orderEntity.setOrderPlan(OrderPlan.SERVICE_AREA_PLAN);
                }
            }
        }
        if(orderEntity.getOrderDeliveryDate()==null) {
            final Calendar calendar = GregorianCalendar.getInstance();
            calendar.add(Calendar.HOUR,1);
            orderEntity.setOrderDeliveryDate(calendar.getTime());
        }
        orderDao.save(orderEntity);
        responseJsonDto.setOrderId(orderEntity.getOrderId());
        if (systemPropertyService.readPrefValue(PreferenceType.ASSIGN_DRIVER_AUTOMATICALLY).equals("1")) {
            responseJsonDto.setAssignDriverAutomatically(true);
        } else {
            responseJsonDto.setAssignDriverAutomatically(false);
        }
        //send push notification to driver
        if (orderEntity.getDriver() != null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN001", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }

        SMSUtil.sendSMS("Please go to this url " + getServerUrl() + "/customer/order?orderId=" + orderEntity.getOrderId() + " to get the updates of your order.", orderEntity.getCustomer().getUser().getMobileNumber(),
                systemPropertyService.readPrefValue(PreferenceType.SMS_COUNTRY_CODE), systemPropertyService.readPrefValue(PreferenceType.SMS_PROVIDER));

        ResponseOrderDto socketOrder = new ResponseOrderDto();
        socketOrder.setOrderId(orderEntity.getOrderId());
        socketOrder.setOrderStatus(orderEntity.getOrderStatus());
        if (orderEntity.getDriver() != null)
            socketOrder.setDriverId(orderEntity.getDriver().getDriverId());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.NEW_ORDER.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketOrder));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session : sessions) {
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }

        return responseJsonDto;
    }

    @Override
    public StoreEntity getSpecialPrivilegeInfo(HeaderDto headerDto) throws Exception {
        //StoreBrandEntity storeBrand = ((StoreEntity) storeDao.findById(headerDto.getStoreId())).getStoreBrand();
        StoreEntity store = storeDao.findById(headerDto.getStoreId());
        StoreEntity retStore = new StoreEntity();
        retStore.setAllowStoresDriver(store.getAllowStoresDriver());
        retStore.setAllowUnknownAreaServing(store.getAllowUnknownAreaServing());
        return retStore;
    }

    @Override
    public CustomerEntity getCustomersInfo(String storeId, RequestJsonDto requestJsonDto) throws Exception {
        UserEntity user = userDao.findByMobileNumberAndRole(requestJsonDto.getMobileNumber(), UserRole.ROLE_CUSTOMER);
        if (user == null) {
            return new CustomerEntity();
        }
        StoreEntity storeEntity = null;
        List<String> areas = new ArrayList<>();
        if (storeId != null) {
            storeEntity = storeDao.findById(storeId);
            if (storeEntity == null) {
                throw new YSException("STR002");
            }
            for (StoresAreaEntity storesArea : storeEntity.getStoresAreas()) {
                areas.add(storesArea.getArea().getAreaId());
            }
        }

        CustomerEntity customerEntity = user.getCustomer();
        if (customerEntity == null)
            throw new YSException("ORD009");
        String fields = "customerId, user, customersArea";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("customersArea", "customersAreaId,street,givenLocation,area");
        assoc.put("user", "firstName,lastName");
        subAssoc.put("area", "areaId,areaName,parent,latitude,longitude");
        subAssoc.put("parent", "areaId,areaName,parent");
        CustomerEntity customer = (CustomerEntity) ReturnJsonUtil.getJsonObject(customerEntity, fields, assoc, subAssoc);

        //remove unknown areas
       /* List<CustomersAreaEntity> removeCustomersArea = new ArrayList<>();
        for (CustomersAreaEntity customersAreaEntity : customer.getCustomersArea()) {
            if (areas.size() > 0) {
                if (!areas.contains(customersAreaEntity.getArea().getAreaId()) || customersAreaEntity.getArea() == null) {
                    removeCustomersArea.add(customersAreaEntity);
                }
            } else {
                removeCustomersArea.addAll(customerEntity.getCustomersArea());
                break;
            }
        }*/

        for (Iterator<CustomersAreaEntity> iterator = customer.getCustomersArea().iterator(); iterator.hasNext(); ) {
            CustomersAreaEntity customersArea = iterator.next();
            if (areas.size() > 0) {
                if (customersArea.getArea() == null || !areas.contains(customersArea.getArea().getAreaId())) {
                    iterator.remove();
                }
            } else
                iterator.remove();
        }

        return customer;
    }

    @Override
    public List<DriverEntity> getStoresDrivers(HeaderDto headerDto) throws Exception {

        StoreEntity store;
        if (headerDto.getStoreId() != null) {
            store = storeDao.findById(headerDto.getStoreId());
            if (store == null)
                throw new Exception("STE002");
        } else if (SessionManager.getRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            StoreManagerEntity storeManager = storeManagerDao.findByFakeId(SessionManager.getStoreManagerId());
            store = storeManager.getStore();
        } else
            throw new Exception("PAR001");

        List<DriverEntity> storesDrivers = store.getDrivers();
        List<DriverEntity> drivers = new ArrayList<>();
        String fields = "driverId,experienceLevel,previousOrderCount,user";
        Map<String, String> assoc = new HashMap<>();

        assoc.put("user", "firstName,lastName");
        for (DriverEntity driverEntity : storesDrivers) {
            driverEntity.setPreviousOrderCount(driverDao.getDriversOrderCount(driverEntity.getDriverId()));
            if (!driverEntity.getAvailabilityStatus().equals(AvailabilityStatus.UNAVAILABLE))
                drivers.add((DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity, fields, assoc));
        }
        return drivers;
    }

    @Override
    public OrderEntity getOrderDetail(HeaderDto headerDto) throws Exception {
        OrderEntity order = orderDao.findById(headerDto.getOrderId());
        if (order == null)
            throw new YSException("ORD004");
        BigDecimal totalPreviousOrderEtd = BigDecimal.ZERO;
        if (order.getDriver() != null) {
            totalPreviousOrderEtd = driverDao.getDriversPreviousOrderTotalEtd(order.getDriver().getDriverId(), order.getId());
            for (OrdersDriverEntity ordersDriverEntity : order.getOrdersDrivers()) {
                if (ordersDriverEntity.getDriverStatus().equals(OrdersDriverStatus.RUNNING))
                    if (ordersDriverEntity.getAssignedDate() != null && order.getEstimatedDeliveryTime() != null)
                        order.setEstimatedTimeOfDelivery(new Timestamp(ordersDriverEntity.getAssignedDate().getTime() + (order.getEstimatedDeliveryTime().add(totalPreviousOrderEtd)).multiply(new BigDecimal(60 * 1000)).longValue()));
            }
        }
        String fields = "orderId,orderDisplayId,orderDescription,estimatedTimeOfDelivery,driverType,orderStatus,orderDeliveryDate,pickupDate,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,driver,customerSignImage,receiverName";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand,allowStoresDriver,allowUnknownAreaServing");
        assoc.put("customer", "customerId,user");
        assoc.put("driver", "driverId,user,latitude,longitude");
        assoc.put("customersArea", "customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        assoc.put("orderCancel", "reason,orderStatus,cancelledDate");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude");
        OrderEntity orderEntity = (OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc);
//        orderEntity.setStore(ValidationUtil.initializeAndUnproxy(orderEntity.getStore()));
        return orderEntity;
    }

    @Override
    public List<StoreEntity> getAllStoreList() throws Exception {
        List<StoreEntity> storeEntities = storeDao.findAllActive();

        List<StoreEntity> stores = new ArrayList<>();
        String fields = "storeId,givenLocation1,latitude,longitude,area,storeBrand";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("storeBrand", "brandName");
        assoc.put("area", "areaName");

        for (StoreEntity store : storeEntities)
            stores.add((StoreEntity) ReturnJsonUtil.getJsonObject(store, fields, assoc));

        for (StoreEntity storeEntity : stores) {
            storeEntity.setAddressNote(storeEntity.getStoreBrand().getBrandName() + "-" + storeEntity.getGivenLocation1() + "," + storeEntity.getArea().getAreaName());
            storeEntity.setGivenLocation1(null);
            storeEntity.setStoreBrand(null);
            storeEntity.setArea(null);
        }

        return stores;
    }

    private void updateETDAndDeliveryTimeOfOrder(OrderEntity orderEntity, DriverEntity driverEntity) throws Exception {
        //update delivery time of the order
        AreaEntity area;
        if (orderEntity.getCustomersArea() != null && orderEntity.getCustomersArea().getArea() != null) {
            area = orderEntity.getCustomersArea().getArea();
        } else {
            area = new AreaEntity();
            area.setLatitude(orderEntity.getCustomersArea().getLatitude());
            area.setLongitude(orderEntity.getCustomersArea().getLongitude());
        }

        Boolean isActualDistance = DistanceType.valueOf(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)).equals(DistanceType.ACTUAL_DISTANCE);
        Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
        BigDecimal timeAtStore = new BigDecimal(systemPropertyService.readPrefValue(PreferenceType.DRIVERS_TIME_AT_STORE));
        BigDecimal timeAtCustomer = new BigDecimal(systemPropertyService.readPrefValue(PreferenceType.DRIVERS_TIME_AT_CUSTOMER));
        BigDecimal timeFactorPerKMForAerial = BigDecimal.valueOf(Double.parseDouble(systemPropertyService.readPrefValue(PreferenceType.AIR_TO_ROUTE_DISTANCE_TIME_FACTOR_PER_KM)));
        //get active order of driver
        List<OrderEntity> orders = orderDao.findActiveOrdersOfDriver(driverEntity.getDriverId());
        if (Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.MAX_NUM_ORDER_FOR_DRIVER)) < orders.size()) {
            throw new YSException("DRV005");
        }
        driverEntity.setOrders(orders);
        if (isActualDistance) {
            String storeAddress[] = {GeoCodingUtil.getLatLong(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude())};
            String customerAddress[] = {GeoCodingUtil.getLatLong(area.getLatitude(), area.getLongitude())};
            String driverAddress[] = {GeoCodingUtil.getLatLong(driverEntity.getLatitude(), driverEntity.getLongitude())};
            BigDecimal driverToRestaurantTime = new BigDecimal(GeoCodingUtil.getListOfDistances(driverAddress, storeAddress, timePerKm).get("time") / 60);
            BigDecimal restaurantToCustomerTime = new BigDecimal(GeoCodingUtil.getListOfDistances(storeAddress, customerAddress, timePerKm).get("time") / 60);
            driverEntity.setCurrentOrderEtd(driverToRestaurantTime.add(restaurantToCustomerTime).add(timeAtStore).add(timeAtCustomer));
            driverEntity.setTotalOrderEtd(driverEntity.getCurrentOrderEtd());
        } else {
            BigDecimal distanceFromDriverToStore = timeFactorPerKMForAerial.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(driverEntity.getLatitude(), driverEntity.getLongitude(), orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude())));
            BigDecimal distanceFromStoreToCustomer = timeFactorPerKMForAerial.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(), area.getLatitude(), area.getLongitude())));
            driverEntity.setCurrentOrderEtd(distanceFromDriverToStore.add(distanceFromStoreToCustomer).add(timeAtStore).add(timeAtCustomer));
            driverEntity.setTotalOrderEtd(driverEntity.getCurrentOrderEtd());
        }
        if (orders.size() > 0) {
            for (OrderEntity order : driverEntity.getOrders()) {
                AreaEntity area1;
                if (order.getCustomersArea().getArea() != null) {
                    String areaId = order.getCustomersArea().getArea().getAreaId();
                    area1 = areaDao.findByAreaId(areaId);
                } else {
                    area1 = new AreaEntity();
                    area1.setLatitude(order.getCustomersArea().getLatitude());
                    area1.setLongitude(order.getCustomersArea().getLongitude());
                }
                BigDecimal timeRequireToComplete = BigDecimal.ZERO;
                if (order.getDriver().getId().equals(driverEntity.getId())) {
                    String storeAddress1[] = {GeoCodingUtil.getLatLong(order.getStore().getLatitude(), order.getStore().getLongitude())};
                    String customerAddress1[] = {GeoCodingUtil.getLatLong(area1.getLatitude(), area1.getLongitude())};
                    String driverAddress1[] = {GeoCodingUtil.getLatLong(driverEntity.getLatitude(), driverEntity.getLongitude())};
                    BigDecimal driverToRestaurantTime1, restaurantToCustomerTime1;
                    if (isActualDistance) {
                        driverToRestaurantTime1 = new BigDecimal(GeoCodingUtil.getListOfDistances(driverAddress1, storeAddress1, timePerKm).get("time") / 60);
                        restaurantToCustomerTime1 = new BigDecimal(GeoCodingUtil.getListOfDistances(storeAddress1, customerAddress1, timePerKm).get("time") / 60);
                    } else {
                        driverToRestaurantTime1 = timeFactorPerKMForAerial.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(driverEntity.getLatitude(), driverEntity.getLongitude(), order.getStore().getLatitude(), order.getStore().getLongitude())));
                        restaurantToCustomerTime1 = timeFactorPerKMForAerial.multiply(new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(order.getStore().getLatitude(), order.getStore().getLongitude(), area.getLatitude(), area.getLongitude())));
                    }
                    log.info("driver id:" + driverEntity.getId() + "driver to restaurant time:" + driverToRestaurantTime1 + "driver to restaurant time:" + restaurantToCustomerTime1
                            + "time at store:" + timeAtStore + "time at customer:" + timeAtCustomer);
                    timeRequireToComplete = timeRequireToComplete.add(driverToRestaurantTime1.add(restaurantToCustomerTime1).add(timeAtCustomer).add(timeAtStore));
                }
                driverEntity.setTotalOrderEtd(BigDecimalUtil.checkNull(driverEntity.getTotalOrderEtd()).add(timeRequireToComplete));
            }
        }
    }

    @Override
    public ResponseJsonDto assignDriverToOrder(HeaderDto headerDto) throws Exception {
        ResponseJsonDto responseJsonDto = new ResponseJsonDto();
        OrderEntity orderEntity = orderDao.findById(headerDto.getOrderId());
        if (orderEntity == null)
            throw new YSException("ORD004");
        List<DriverEntity> anyOrderDrivers = new ArrayList<>();
        if (headerDto.getDriverId() != null) {
            DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
            if (driverEntity == null)
                throw new YSException("DRV001");
            if (!driverEntity.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE))
                throw new YSException("DRV008");


            updateETDAndDeliveryTimeOfOrder(orderEntity, driverEntity);
            if (driverEntity.getDriverType().equals(DriverType.FASTR)) {
                Integer currentOrderCount = driverEntity.getOrders().size();
                if (Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.MAX_NUM_ORDER_FOR_DRIVER)) <= currentOrderCount)
                    throw new YSException("DRV005");
                responseJsonDto.setDriverType(DriverType.FASTR);
            } else {
                responseJsonDto.setDriverType(DriverType.RESTAURANT);
            }

            OrdersDriverEntity ordersDriverEntity = new OrdersDriverEntity();
            ordersDriverEntity.setOrder(orderEntity);
            ordersDriverEntity.setDriver(driverEntity);
            ordersDriverEntity.setDriverStatus(OrdersDriverStatus.RUNNING);
            ordersDriverEntity.setAssignedDate(DateUtil.getCurrentTimestampSQL());

            if (orderEntity.getOrdersDrivers() != null) {
                for (OrdersDriverEntity ordersDriver : orderEntity.getOrdersDrivers()) {
                    ordersDriver.setDriverStatus(OrdersDriverStatus.FAILED);
                }
                orderEntity.getOrdersDrivers().add(ordersDriverEntity);
            } else {
                orderEntity.setOrdersDrivers(Collections.singletonList(ordersDriverEntity));
            }

            orderEntity.setDriver(driverEntity);
            orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);
        } else {
            if (systemPropertyService.readPrefValue(PreferenceType.ASSIGN_DRIVER_AUTOMATICALLY).equals("1")) {
                List<DriverEntity> driverEntities = systemAlgorithmService.assignDriver(orderEntity, true);
                if (driverEntities.size() == 0)
                    responseJsonDto.setMessage("Drivers not available. Please try to assign driver later");
                else
                    orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);

                responseJsonDto.setAssignDriverAutomatically(true);
            } else {
                if (SessionManager.getRole().equals(UserRole.ROLE_CSR) || SessionManager.getRole().equals(UserRole.ROLE_ADMIN) ||
                        SessionManager.getRole().equals(UserRole.ROLE_MANAGER)) {
                    anyOrderDrivers = systemAlgorithmService.assignDriver(orderEntity, false);
                } else
                    orderEntity.setAssignDriver(true);
                responseJsonDto.setAssignDriverAutomatically(false);
            }
        }

        orderDao.update(orderEntity);
        if (orderEntity.getDriver() != null) {
            //send push notification to driver
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN001", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }

        if (anyOrderDrivers.size() > 0) {
            List<DriverEntity> retDrivers = new ArrayList<>();
            String fields = "driverId,driverType,availabilityStatus,vehicleType,vehicleNumber,licenseNumber,latitude,longitude,streetAddress,experienceLevel,user,currentOrderCount,totalOrderEtd,currentOrderEtd,rating,lastOrderDelivered,rating";
            Map<String, String> assoc = new HashMap<>();
            assoc.put("user", "firstName,lastName,mobileNumber,emailAddress,profileImage");
            for (DriverEntity driverEntity : anyOrderDrivers) {
                retDrivers.add((DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity, fields, assoc));
            }
            responseJsonDto.setDrivers(retDrivers);
        }

        ResponseOrderDto socketOrder = new ResponseOrderDto();
        socketOrder.setOrderId(orderEntity.getOrderId());
        socketOrder.setOrderStatus(orderEntity.getOrderStatus());
        if (orderEntity.getDriver() != null)
            socketOrder.setDriverId(orderEntity.getDriver().getDriverId());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.ORDER_STATUS.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketOrder));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session : sessions) {
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }
        return responseJsonDto;
    }

    @Override
    public List<DriverEntity> getAnyOrderDriverList(HeaderDto headerDto) throws Exception {

        OrderEntity orderEntity = orderDao.findById(headerDto.getOrderId());
        if (orderEntity == null)
            throw new YSException("ORD004");


        List<DriverEntity> driverEntities = systemAlgorithmService.assignDriver(orderEntity, false);
        List<DriverEntity> drivers = new ArrayList<>();
        String fields = "driverId,experienceLevel,previousOrderCount,user,streetAddress,rating";
        Map<String, String> assoc = new HashMap<>();

        assoc.put("user", "firstName,lastName");
        for (DriverEntity driverEntity : driverEntities) {
            driverEntity.setPreviousOrderCount(driverDao.getDriversOrderCount(driverEntity.getDriverId()));
            driverEntity.setRating(orderDao.findRatingOfDriver(driverEntity.getId()));
            if (!driverEntity.getAvailabilityStatus().equals(AvailabilityStatus.UNAVAILABLE))
                drivers.add((DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity, fields, assoc));
        }
        return drivers;
    }

    @Override
    public RequestJsonDto getTotalNumberOfOrders(HeaderDto headerDto) throws Exception {
        Integer storeId = null;

        if (headerDto.getStoreId() != null) {
            StoreEntity storeEntity = storeDao.findById(headerDto.getStoreId());
            if (storeEntity == null)
                throw new YSException("STR002");
            storeId = storeEntity.getId();
        }
        return orderDao.getNumberOfClassifiedOrdersWeb(storeId);
    }

    @Override
    public PaginationDto getOrdersByStatus(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception {

        if (requestJsonDto.getOrderDisplayStatus() == null)
            throw new YSException("");

        List<Integer> storeId = new ArrayList<>();
        Boolean isMerchant = false;
        if (headerDto.getUserId() != null) {
            UserEntity user = userDao.findByUserId(headerDto.getUserId());
            if (user == null)
                throw new YSException("USR001");
            if (user.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
                isMerchant = true;
                for (StoreBrandEntity storesBrand : user.getMerchant().getStoreBrand()) {
                    for (StoreEntity store : storesBrand.getStores()) {
                        storeId.add(store.getId());
                    }
                }
            }
            if (user.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
                storeId.add(user.getStoreManager().getStore().getId());
            }
            /*StoreEntity storeEntity = storeDao.findByNotificationId(headerDto.getStoreId());
            storeId = storeEntity.getId();*/
        }

        Integer totalRows = ((isMerchant && storeId.size() == 0) ? 0 : orderDao.getTotalNumberOfOrdersByStatusWeb(storeId, requestJsonDto.getOrderDisplayStatus()));

        PaginationDto paginationDto = new PaginationDto();

        Page page = requestJsonDto.getPage();
        paginationDto.setNumberOfRows(totalRows);
        page.setTotalRows(totalRows);
        List<OrderEntity> orders = ((isMerchant && storeId.size() == 0) ? null : orderDao.getOrdersByStatusWeb(storeId, page, requestJsonDto.getOrderDisplayStatus()));

        List<OrderEntity> returnOrders = new ArrayList<>();

        String fields = "orderId,orderDisplayId,orderDescription,orderDeliveryDate,pickupDate,driverType,orderStatus,paymentMode,customersAreaType,assignDriver,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,driver";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,latitude,longitude,priority,area,storeBrand");
        assoc.put("customer", "customerId,user");
        assoc.put("driver", "driverId,user,latitude,longitude");
        assoc.put("customersArea", "customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude");
        if (orders != null) {
            for (OrderEntity order : orders)
                returnOrders.add((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc));
        }


        paginationDto.setData(returnOrders);

        return paginationDto;

    }

    @Override
    public void changeStoreManagerStatus(StoreManagerEntity storeManager) throws Exception {
        log.info("----- Changing StoreManager's status. -----");
        StoreManagerEntity storeManagerEntity = storeManagerDao.findByFakeId(storeManager.getStoreManagerId());
        if (storeManagerEntity == null) {
            log.error("----- StoreManager does not exist. -----");
            throw new YSException("STRMGR001");
        }
        if (storeManager.getUser().getUserStatus() != null && !storeManager.getUser().getUserStatus().equals(storeManagerEntity.getUser().getUserStatus())) {
            storeManagerEntity.getUser().setUserStatus(storeManager.getUser().getUserStatus());
            storeManagerDao.update(storeManagerEntity);

            log.info("Sending mail to " + storeManagerEntity.getUser().getEmailAddress());

            String body = EmailMsg.notificationEmailUserStatus(storeManagerEntity.getUser(), getServerUrl());
            String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Your account has been";
            if (storeManagerEntity.getUser().getUserStatus().equals(UserStatus.ACTIVE)) {
                subject += " re-activated.";
            } else if (storeManagerEntity.getUser().getUserStatus().equals(UserStatus.INACTIVE)) {
                subject += " deactivated.";
            }

            sendMail(storeManagerEntity.getUser().getEmailAddress(), body, subject);
        }
    }

    @Override
    public void updateStoresNotDriver(HeaderDto headerDto, List<DriverEntity> drivers) throws Exception {
        StoreEntity storeEntity = storeDao.findById(headerDto.getStoreId());
        if (storeEntity == null)
            throw new YSException("STR002");
        List<StoresNotDriverEntity> storesNotDriverEntities = storeEntity.getStoresNotDrivers();

        List<Integer> driverNotStoreIds = new ArrayList<>();
        List<Integer> dbDriverNotAreaIdList = new ArrayList<Integer>();
        Map<Integer, StoresNotDriverEntity> dbDriverNotAreaMap = new HashMap<>();

        for (StoresNotDriverEntity storesNotDriverEntity : storeEntity.getStoresNotDrivers()) {
            dbDriverNotAreaIdList.add(storesNotDriverEntity.getId());
            dbDriverNotAreaMap.put(storesNotDriverEntity.getId(), storesNotDriverEntity);
        }

        for (DriverEntity driver : drivers) {
            DriverEntity driverEntity = driverDao.findById(driver.getDriverId());
            if (driverEntity == null)
                throw new YSException("DRV001");
            StoresNotDriverEntity storesNotDriverEntity = storesNotDriverDao.findByDriverAndStoreId(driverEntity.getId(), storeEntity.getId());
            if (storesNotDriverEntity == null) {
                storesNotDriverEntity = new StoresNotDriverEntity();
                storesNotDriverEntity.setDriver(driverEntity);
                storesNotDriverEntity.setStore(storeEntity);
                storesNotDriverDao.save(storesNotDriverEntity);
                storesNotDriverEntities.add(storesNotDriverEntity);
            }
            driverNotStoreIds.add(storesNotDriverEntity.getId());
        }
        for (Integer dbDriverAreaId : dbDriverNotAreaIdList) {
            if (!driverNotStoreIds.contains(dbDriverAreaId)) {
                storesNotDriverEntities.remove(dbDriverNotAreaMap.get(dbDriverAreaId));
            }
        }
        storeEntity.setStoresNotDrivers(storesNotDriverEntities);
        storeDao.update(storeEntity);
    }

    @Override
    public List<DriverEntity> getDriversNotStore(HeaderDto headerDto) throws Exception {
        StoreEntity storeEntity = storeDao.findById(headerDto.getStoreId());
        if (storeEntity == null)
            throw new YSException("STR002");
        List<StoresNotDriverEntity> storesNotDriverEntities = storeEntity.getStoresNotDrivers();
        List<DriverEntity> driverEntities = new ArrayList<>();
        String fields = "driverId,driverType,availabilityStatus,vehicleType,vehicleNumber,licenseNumber,latitude,longitude,experienceLevel,user";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "firstName,lastName,mobileNumber,emailAddress,profileImage");

        for (StoresNotDriverEntity storesNotDriverEntity : storesNotDriverEntities) {
            driverEntities.add((DriverEntity) ReturnJsonUtil.getJsonObject(storesNotDriverEntity.getDriver(), fields, assoc));
        }
        return driverEntities;
    }

    @Override
    public List<DriverEntity> getAnyOrderDrivers() throws Exception {
        log.info("Getting drivers");
        List<DriverEntity> drivers = driverDao.getAnyOrderDrivers();
        List<DriverEntity> driverEntities = new ArrayList<>();
        String fields = "driverId,user";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "firstName,lastName,mobileNumber,emailAddress,profileImage");

        for (DriverEntity driver : drivers) {
            driverEntities.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc));
        }
        return driverEntities;
    }

    @Override
    public List<StoreEntity> getStoreList(String userId) throws Exception {
        log.info("Getting store list");
        UserEntity userEntity = userDao.findByUserId(userId);
        if (userEntity == null)
            throw new YSException("USR001");
        List<StoreEntity> storeEntities = new ArrayList<>();
        String fields = "storeId,givenLocation1,givenLocation1,area,storeBrand,addressNote,latitude,longitude,allowStoresDriver,allowUnknownAreaServing";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("storeBrand", "brandName");
        assoc.put("area", "areaName");
        if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            for (StoreBrandEntity storeBrand : userEntity.getMerchant().getStoreBrand()) {
                if (storeBrand.getStatus().equals(Status.ACTIVE)) {
                    for (StoreEntity store : storeBrand.getStores()) {
                        if (store.getStatus().equals(Status.ACTIVE) && !store.getStoresAreas().isEmpty() && (store.getIsDistanceBaseRatePlan() || !store.getRatePlans().isEmpty()) && ((store.getAllowUnknownAreaServing() != null && store.getAllowUnknownAreaServing()) || !store.getUnknownAreaRatePlans().isEmpty())) {
                            store.setAddressNote(store.getStoreBrand().getBrandName() + "-" + store.getGivenLocation1() + "," + store.getArea().getAreaName());
                            storeEntities.add((StoreEntity) ReturnJsonUtil.getJsonObject(store, fields, assoc));
                        }
                    }
                }
            }
        } else if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            StoreEntity store = userEntity.getStoreManager().getStore();
            if (store.getStatus().equals(Status.ACTIVE) && !store.getStoresAreas().isEmpty() && (store.getIsDistanceBaseRatePlan() || !store.getRatePlans().isEmpty()) && ((store.getAllowUnknownAreaServing() != null && store.getAllowUnknownAreaServing()) || !store.getUnknownAreaRatePlans().isEmpty())) {
                store.setAddressNote(store.getStoreBrand().getBrandName() + "-" + store.getGivenLocation1() + "," + store.getArea().getAreaName());
                storeEntities.add((StoreEntity) ReturnJsonUtil.getJsonObject(store, fields, assoc));
            }
        } else {
            for (StoreEntity storeEntity : storeDao.findAllActive()) {
                if (!storeEntity.getStoresAreas().isEmpty() && (storeEntity.getIsDistanceBaseRatePlan() || !storeEntity.getRatePlans().isEmpty()) && ((storeEntity.getAllowUnknownAreaServing() != null && storeEntity.getAllowUnknownAreaServing()) || !storeEntity.getUnknownAreaRatePlans().isEmpty())) {
                    storeEntity.setAddressNote(storeEntity.getStoreBrand().getBrandName() + "-" + storeEntity.getGivenLocation1() + "," + storeEntity.getArea().getAreaName());
                    storeEntities.add((StoreEntity) ReturnJsonUtil.getJsonObject(storeEntity, fields, assoc));
                }
            }
        }
        return storeEntities;
    }

    @Override
    public OrderEntity estimateDeliveryCharge(String storeId, CustomersAreaEntity customersArea) throws Exception {
        log.info("Calculating store rate");
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        List<UnknownAreaRatePlanEntity> unknownAreaRatePlans = storeEntity.getUnknownAreaRatePlans();
        List<UnknownAreaRatePlanEntity> activeUnknownAreaRatePlans = new ArrayList<>();
        UnknownAreaRatePlanEntity unknownAreaRate = new UnknownAreaRatePlanEntity();
        for (UnknownAreaRatePlanEntity unknownAreaRatePlan : unknownAreaRatePlans) {
            if (unknownAreaRatePlan.getStatus().equals(Status.ACTIVE))
                activeUnknownAreaRatePlans.add(unknownAreaRatePlan);
        }
        if (activeUnknownAreaRatePlans.size() == 0)
            throw new YSException("UNRPN001");
        BigDecimal distanceInKm = BigDecimal.ZERO;
        if (DistanceType.valueOf(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)).equals(DistanceType.ACTUAL_DISTANCE)) {
            Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
            Map<String, Long> data = GeoCodingUtil.calculateDistance(storeEntity.getLatitude(), storeEntity.getLongitude(), customersArea.getLatitude(), customersArea.getLongitude(), timePerKm);
            Long distance = data.get("distance");
            Long duration = data.get("duration");
            distanceInKm = BigDecimal.valueOf(distance).divide(BigDecimal.valueOf(1000));
        } else {
            distanceInKm = new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(storeEntity.getLatitude(), storeEntity.getLongitude(), customersArea.getLatitude(), customersArea.getLongitude()));
        }

        Collections.sort(activeUnknownAreaRatePlans, new Comparator<UnknownAreaRatePlanEntity>() {
            public int compare(UnknownAreaRatePlanEntity first, UnknownAreaRatePlanEntity second) {
                return first.getEndDistance().compareTo(second.getEndDistance());
            }
        });
        for (UnknownAreaRatePlanEntity unknownAreaRatePlan : activeUnknownAreaRatePlans) {
            if (BigDecimalUtil.isGreaterThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))
                    && BigDecimalUtil.isLessThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))) {
                unknownAreaRate = unknownAreaRatePlan;
                break;
            }
        }
        if (unknownAreaRate.getId() == null)
            unknownAreaRate = activeUnknownAreaRatePlans.get(activeUnknownAreaRatePlans.size() - 1);

        BigDecimal estimatedCharge = unknownAreaRate.getBaseCharge();
        estimatedCharge = estimatedCharge.add(distanceInKm.multiply(unknownAreaRate.getAdditionalChargePerKM()));
        OrderEntity order = new OrderEntity();
        order.setEstimatedDeliveryCharge(estimatedCharge);
        //order.setEstimatedDeliveryTime(BigDecimal.valueOf(duration));
        order.setDistanceInKm(distanceInKm);
        return order;
    }

    public Map<String, PaginationDto> getDriversWithOrderCount(String userId, RequestJsonDto requestJson) throws Exception {
        Map<String, PaginationDto> drivers = new HashMap();
        if (userId == null)
            userId = SessionManager.getFakeUserId();
        UserEntity userEntity = userDao.findByUserId(userId);
        if (userEntity == null)
            throw new YSException("USR001");
        PaginationDto onDutyPagination = new PaginationDto();
        PaginationDto offDutyPagination = new PaginationDto();

        List<DriverEntity> onDutyDrivers = new ArrayList<>();
        List<DriverEntity> offDutyDrivers = new ArrayList<>();

        Page page = requestJson.getPage();
        Integer totalRows;

        if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            StoreManagerEntity storeManager = userEntity.getStoreManager();

            totalRows = driverDao.getTotalNumberOfStoreDriverByStatus(storeManager.getStore().getStoreId(), page, AvailabilityStatus.AVAILABLE);
            onDutyPagination.setNumberOfRows(totalRows);
            page.setTotalRows(totalRows);
            onDutyDrivers.addAll(driverDao.findStoreDriverByStatus(storeManager.getStore().getStoreId(), page, AvailabilityStatus.AVAILABLE));

            totalRows = driverDao.getTotalNumberOfStoreDriverByStatus(storeManager.getStore().getStoreId(), page, AvailabilityStatus.UNAVAILABLE);
            offDutyPagination.setNumberOfRows(totalRows);
            page.setTotalRows(totalRows);
            offDutyDrivers.addAll(driverDao.findStoreDriverByStatus(storeManager.getStore().getStoreId(), page, AvailabilityStatus.UNAVAILABLE));
        } else if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            MerchantEntity merchant = userEntity.getMerchant();

            totalRows = driverDao.getTotalNumberOfMerchantDriverByStatus(merchant.getMerchantId(), page, AvailabilityStatus.AVAILABLE);
            onDutyPagination.setNumberOfRows(totalRows);
            page.setTotalRows(totalRows);
            onDutyDrivers.addAll(driverDao.findMerchantDriverByStatus(merchant.getMerchantId(), page, AvailabilityStatus.AVAILABLE));

            totalRows = driverDao.getTotalNumberOfMerchantDriverByStatus(merchant.getMerchantId(), page, AvailabilityStatus.UNAVAILABLE);
            offDutyPagination.setNumberOfRows(totalRows);
            page.setTotalRows(totalRows);
            offDutyDrivers.addAll(driverDao.findMerchantDriverByStatus(merchant.getMerchantId(), page, AvailabilityStatus.UNAVAILABLE));
        } else if (SessionManager.getRole().equals(UserRole.ROLE_CSR) || SessionManager.getRole().equals(UserRole.ROLE_ADMIN)
                || SessionManager.getRole().equals(UserRole.ROLE_MANAGER)) {

            totalRows = driverDao.getTotalNumberOfAnyOrderDriverByStatus(page, AvailabilityStatus.AVAILABLE);
            onDutyPagination.setNumberOfRows(totalRows);
            page.setTotalRows(totalRows);
            onDutyDrivers.addAll(driverDao.findAnyOrderDriverByStatus(page, AvailabilityStatus.AVAILABLE));

            totalRows = driverDao.getTotalNumberOfAnyOrderDriverByStatus(page, AvailabilityStatus.UNAVAILABLE);
            offDutyPagination.setNumberOfRows(totalRows);
            page.setTotalRows(totalRows);
            offDutyDrivers.addAll(driverDao.findAnyOrderDriverByStatus(page, AvailabilityStatus.UNAVAILABLE));
        }

        List<DriverEntity> retOnDutyDrivers = new ArrayList<>();
        List<DriverEntity> retOffDutyDrivers = new ArrayList<>();
        String fields = "driverId,driverType,availabilityStatus,vehicleType,vehicleNumber,licenseNumber,latitude,longitude,experienceLevel,currentOrderCount,user,store,rating,lastOrderDelivered,lastLocationUpdate";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("user", "userId,firstName,lastName");
        assoc.put("store", "storeId,storeBrand");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");

        for (DriverEntity driver : onDutyDrivers) {
            driver.setCurrentOrderCount(driverDao.getDriversOrderCount(driver.getDriverId()));
            OrderEntity lastOrder = driverDao.getDriversLastOrder(driver.getDriverId());
            if (lastOrder != null && lastOrder.getDeliveredDate() != null) {
                driver.setLastOrderDelivered(lastOrder.getDeliveredDate());
            } else if (driver.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE)) {
                for (DriversWorkLogEntity workLog : driver.getWorkLogs()) {
                    if (workLog.getToDateTime() == null) {
                        driver.setLastOrderDelivered(workLog.getFromDateTime());
                    }
                }
            }
            retOnDutyDrivers.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
        }

        for (DriverEntity driver : offDutyDrivers) {
            driver.setCurrentOrderCount(driverDao.getDriversOrderCount(driver.getDriverId()));
            OrderEntity lastOrder = driverDao.getDriversLastOrder(driver.getDriverId());
            if (lastOrder != null && lastOrder.getDeliveredDate() != null)
                driver.setLastOrderDelivered(lastOrder.getDeliveredDate());
            retOffDutyDrivers.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
        }

        Collections.sort(onDutyDrivers, new Comparator<DriverEntity>() {
            public int compare(DriverEntity first, DriverEntity second) {
                return first.getCurrentOrderCount().compareTo(second.getCurrentOrderCount());
            }
        });
        Collections.sort(offDutyDrivers, new Comparator<DriverEntity>() {
            public int compare(DriverEntity first, DriverEntity second) {
                return first.getCurrentOrderCount().compareTo(second.getCurrentOrderCount());
            }
        });
        onDutyPagination.setData(retOnDutyDrivers);
        offDutyPagination.setData(retOffDutyDrivers);
        drivers.put("onDutyDrivers", onDutyPagination);
        drivers.put("offDutyDrivers", offDutyPagination);
        return drivers;
    }

    @Override
    public PaginationDto searchOrder(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Searching order -----");
        Page page = requestJsonDto.getPage();
        PaginationDto paginationDto = new PaginationDto();
        Integer totalRows = 0;
        List<OrderEntity> orderEntities = new ArrayList<>();
        if (page != null && page.getSearchFor() != null && !page.getSearchFor().equals("")) {
            Map<String, String> fieldsMap = new HashMap<>();
            fieldsMap.put("self", "orderDisplayId");
            fieldsMap.put("store", "givenLocation1,givenLocation2,addressNote");
            fieldsMap.put("store#storeBrand", "brandName");
            //fieldsMap.put("customer#user", "firstName,lastName,emailAddress,mobileNumber");
            page.setSearchFields(fieldsMap);
        }


        if (SessionManager.getRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            totalRows = orderDao.getNumberOfStoreManagerSearchedOrder(page, SessionManager.getStoreManagerId(), requestJsonDto.getOrderDisplayStatus());
            page.setTotalRows(totalRows);
            paginationDto.setNumberOfRows(totalRows);
            orderEntities = orderDao.getStoreManagerSearchedOrder(page, SessionManager.getStoreManagerId(), requestJsonDto.getOrderDisplayStatus());
        } else if (SessionManager.getRole().equals(UserRole.ROLE_MERCHANT)) {
            totalRows = orderDao.getNumberOfMerchantSearchedOrder(page, SessionManager.getMerchantId(), requestJsonDto.getOrderDisplayStatus());
            page.setTotalRows(totalRows);
            paginationDto.setNumberOfRows(totalRows);
            orderEntities = orderDao.getMerchantSearchedOrder(page, SessionManager.getMerchantId(), requestJsonDto.getOrderDisplayStatus());
        } else if (SessionManager.getRole().equals(UserRole.ROLE_MANAGER) || SessionManager.getRole().equals(UserRole.ROLE_ADMIN) || SessionManager.getRole().equals(UserRole.ROLE_CSR)) {
            totalRows = orderDao.getNumberOfSearchedOrder(page, requestJsonDto.getOrderDisplayStatus());
            page.setTotalRows(totalRows);
            paginationDto.setNumberOfRows(totalRows);
            orderEntities = orderDao.getSearchedOrder(page, requestJsonDto.getOrderDisplayStatus());
        }

        List<OrderEntity> returnOrders = new ArrayList<>();

        String fields = "orderId,orderDisplayId,orderDescription,driverType,orderDeliveryDate,pickupDate,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,driver";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,latitude,longitude,priority,area,storeBrand");
        assoc.put("customer", "customerId,user");
        assoc.put("driver", "driverId,user,latitude,longitude");
        assoc.put("customersArea", "customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude");

        for (OrderEntity order : orderEntities) {
            returnOrders.add((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc));
        }

        paginationDto.setData(returnOrders);

        return paginationDto;
    }

    @Override
    public void updateOrderStatus(HeaderDto headerDto, OrderEntity order) throws Exception {
        log.info("Updating order status of order: " + headerDto.getOrderId() + " to status: " + order.getOrderStatus());
        OrderEntity orderEntity = orderDao.findById(headerDto.getOrderId());
        if (orderEntity == null)
            throw new YSException("ORD004");
        if (order.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_DELIVER) && !orderEntity.getOrderStatus().equals(OrderStatus.AT_STORE))
            throw new YSException("ORD007");
        if (orderEntity.getOrderStatus().ordinal() >= order.getOrderStatus().ordinal()) {
            throw new YSException("ORD014");
        }

        orderEntity.setOrderStatus(order.getOrderStatus());
        orderDao.update(orderEntity);
        //send push notification to driver
        if (order.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_DELIVER)) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN002", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
    }

    @Override
    public List<AreaEntity> getStoreServingAreas(String storeId) throws Exception {
        log.info("Getting serving area of store");
        StoreEntity storeEntity = storeDao.findById(storeId);
        if (storeEntity == null)
            throw new YSException("STR002");
        List<StoresAreaEntity> storesAreas = storeEntity.getStoresAreas();
        List<AreaEntity> returnAreas = new ArrayList<>();
        Set<AreaEntity> areas = new HashSet<>();
        List<Integer> ids = new ArrayList<>();
        for (StoresAreaEntity storesArea : storesAreas) {
            if (storesArea.getArea().getStatus().equals(Status.ACTIVE) && storesArea.getArea().getParent() != null) {
                AreaEntity finalParentArea = storesArea.getArea();
                ids.add(finalParentArea.getId());
                while (finalParentArea.getParent() != null && finalParentArea.getParent().getStatus().equals(Status.ACTIVE)) {
                    finalParentArea = finalParentArea.getParent();
                    ids.add(finalParentArea.getId());
                }
                if (finalParentArea.getParent() == null) {
                    AreaEntity a = ValidationUtil.initializeAndUnproxy(finalParentArea);
                    areas.add(a);
                }
            }
        }

        String fields = "areaId,areaName,child";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("child", "areaId,areaName,latitude,longitude");
        for (AreaEntity area : areas) {
            parseAreaTree(area, ids);
            returnAreas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area, fields, assoc));
        }
        return returnAreas;
    }

    @Override
    public List<DriverEntity> getDriverPerformance(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting list of driver performance. -----");
        List<DriverEntity> returnData = new ArrayList<>();
        List<String> driverIds = requestJsonDto.getDriverIds();
        Date fromDate = requestJsonDto.getFromDate();
        Date toDate = requestJsonDto.getToDate();
        for (String driverId : driverIds) {
            returnData.addAll(getDriverPerformance(driverId, fromDate, toDate));
        }
        return returnData;
    }

    private List<DriverEntity> getDriverPerformance(String driverId, Date fromDate, Date toDate) throws Exception {
        log.info("----- Getting driver performance. -----");
        DriverEntity driverEntity = driverDao.findById(driverId);
        if (driverEntity == null) {
            log.error("----- Driver does not exist. -----");
            throw new YSException("DRV001");
        }
        List<Date> uniqueWorkingDates = new ArrayList<>();
        List<DriversWorkLogEntity> driversWorkLogEntities = driverEntity.getWorkLogs();

        Collections.sort(driversWorkLogEntities, new Comparator<DriversWorkLogEntity>() {
            public int compare(DriversWorkLogEntity first, DriversWorkLogEntity second) {
                return first.getFromDateTime().compareTo(second.getFromDateTime());
            }
        });

        List<DriversWorkLogEntity> finalWorkLogData = new ArrayList<>();
        for (DriversWorkLogEntity driversWorkLog : driversWorkLogEntities) {
            if (driversWorkLog.getToDateTime() != null) {
                if (checkDate(driversWorkLog.getFromDateTime(), driversWorkLog.getToDateTime())) {
                    finalWorkLogData.add(driversWorkLog);
                } else {
                    do {
                        DriversWorkLogEntity driversWorkLogEntity = new DriversWorkLogEntity();
                        driversWorkLogEntity.setFromDateTime(driversWorkLog.getFromDateTime());
                        if (checkDate(driversWorkLog.getFromDateTime(), driversWorkLog.getToDateTime())) {
                            driversWorkLogEntity.setToDateTime(driversWorkLog.getToDateTime());
                        } else {
                            driversWorkLogEntity.setToDateTime(getTodayEndDateAccFromDate(driversWorkLog.getFromDateTime()));
                        }
                        finalWorkLogData.add(driversWorkLogEntity);
                        driversWorkLog.setFromDateTime(getTomorrowStartDateAccFromDate(driversWorkLog.getFromDateTime()));
                    }
                    while (!checkDate(driversWorkLog.getFromDateTime(), driversWorkLog.getToDateTime()));
                }
            } else {
                Date lastDate = DateUtil.getCurrentTimestampSQL();
                do {
                    DriversWorkLogEntity driversWorkLogEntity = new DriversWorkLogEntity();
                    Long longDate = driversWorkLog.getFromDateTime().getTime();
                    java.util.Date utilDate = new Date(longDate);
                    driversWorkLogEntity.setFromDateTime(utilDate);
                    if (checkDate(driversWorkLog.getFromDateTime(), DateUtil.getCurrentDate())) {
                        driversWorkLogEntity.setToDateTime(DateUtil.currentUtilDate());
                    } else {
                        driversWorkLogEntity.setToDateTime(getTodayEndDateAccFromDate(driversWorkLog.getFromDateTime()));
                    }
                    finalWorkLogData.add(driversWorkLogEntity);
                    driversWorkLog.setFromDateTime(getTomorrowStartDateAccFromDate(driversWorkLog.getFromDateTime()));
                }
                while (!checkDate(driversWorkLog.getFromDateTime(), getTomorrowStartDateAccFromDate(lastDate)));
            }
        }
        for (DriversWorkLogEntity driversWorkLog : finalWorkLogData) {
            if (fromDate != null && toDate != null) {
                if (driversWorkLog.getFromDateTime().after(fromDate) && driversWorkLog.getFromDateTime().before(toDate)) {
                    if (!hasDate(uniqueWorkingDates, driversWorkLog.getFromDateTime())) {
                        uniqueWorkingDates.add(driversWorkLog.getFromDateTime());
                    }
                }
            } else {
                if (!hasDate(uniqueWorkingDates, driversWorkLog.getFromDateTime())) {
                    uniqueWorkingDates.add(driversWorkLog.getFromDateTime());
                }
            }
        }
        List<DriverEntity> returnDrivers = new ArrayList<>();
        for (Date workingDate : uniqueWorkingDates) {
            DriverEntity returnDriver = new DriverEntity();
            returnDriver.setDate(workingDate);
            //for duty hour calculation
            BigDecimal dutyHour = BigDecimal.ZERO;
            for (DriversWorkLogEntity driversWorkLog : finalWorkLogData) {
                if (checkDate(workingDate, driversWorkLog.getFromDateTime())) {
                    dutyHour = dutyHour.add(BigDecimal.valueOf(calculateTimeInSecond(driversWorkLog.getFromDateTime(), driversWorkLog.getToDateTime())));
                }
            }
            if (dutyHour.equals(BigDecimal.ZERO)) {
                dutyHour = dutyHour.add(BigDecimal.valueOf(24 * 60 * 60));
            }
            returnDriver.setDutyHour(dutyHour);
            //EOF for duty hour calculation

            //for assigned order
            List<OrdersDriverEntity> todayAssignedOrderDriver = new ArrayList<>();
            for (OrdersDriverEntity ordersDriver : driverEntity.getOrdersDrivers()) {
                if (checkDate(workingDate, ordersDriver.getAssignedDate())) {
                    todayAssignedOrderDriver.add(ordersDriver);
                }
            }
            returnDriver.setOrderAssigned(todayAssignedOrderDriver.size());
            //EOF for assigned order

            //Order Completed count
            List<OrderEntity> todayOrderDelivered = new ArrayList<>();
            BigDecimal distanceCovered = BigDecimal.ZERO;
            for (OrderEntity order : driverEntity.getOrders()) {
                if (order.getOrderStatus().equals(OrderStatus.DELIVERED)) {
                    if (checkDate(workingDate, order.getDeliveredDate())) {
                        todayOrderDelivered.add(order);
                        Map<String, Long> data;
                        Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
                        if (order.getCustomersArea().getArea() == null) {
                            data = GeoCodingUtil.calculateDistance(order.getStore().getLatitude(), order.getStore().getLongitude(), order.getCustomersArea().getLatitude(), order.getCustomersArea().getLongitude(), timePerKm);
                        } else {
                            data = GeoCodingUtil.calculateDistance(order.getStore().getLatitude(), order.getStore().getLongitude(), order.getCustomersArea().getArea().getLatitude(), order.getCustomersArea().getArea().getLongitude(), timePerKm);
                        }
                        distanceCovered = distanceCovered.add(BigDecimal.valueOf(data.get("distance")));
                    }
                }
            }
            returnDriver.setDistanceCovered(distanceCovered);
            returnDriver.setOrderCompleted(todayOrderDelivered.size());
            //EOF order completed count

            //Busy Hour
            BigDecimal busyHour = BigDecimal.ZERO;
            for (OrdersDriverEntity orderDriver : todayAssignedOrderDriver) {
                if (orderDriver.getOrder().getDriver().getId().equals(orderDriver.getDriver().getId())) {
                    if (orderDriver.getOrder().getOrderStatus().equals(OrderStatus.DELIVERED))
                        busyHour = busyHour.add(BigDecimal.valueOf(calculateTimeInSecond(orderDriver.getAssignedDate(), orderDriver.getOrder().getDeliveredDate())));
                    if (orderDriver.getOrder().getOrderStatus().equals(OrderStatus.CANCELLED))
                        busyHour = busyHour.add(BigDecimal.valueOf(calculateTimeInSecond(orderDriver.getAssignedDate(), orderDriver.getOrder().getOrderCancel().getCancelledDate())));
                } else {
                    List<OrdersDriverEntity> ordersDrivers = orderDriver.getOrder().getOrdersDrivers();
                    Collections.sort(ordersDrivers, new Comparator<OrdersDriverEntity>() {
                        public int compare(OrdersDriverEntity first, OrdersDriverEntity second) {
                            return first.getAssignedDate().compareTo(second.getAssignedDate());
                        }
                    });
                    for (OrdersDriverEntity ordersDriver : ordersDrivers) {
                        if (orderDriver.getAssignedDate().before(ordersDriver.getAssignedDate())) {
                            busyHour = busyHour.add(BigDecimal.valueOf(calculateTimeInSecond(orderDriver.getAssignedDate(), orderDriver.getAssignedDate())));
                            break;
                        }
                    }

                }
            }
            returnDriver.setBusyHour(busyHour);
            String field = "driverId,user";
            Map<String, String> assoc = new HashMap<>();
            assoc.put("user", "userId,firstName,lastName");
            DriverEntity driver = (DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity, field, assoc);
            returnDriver.setUser(driver.getUser());
            //EOF busy hour
            returnDrivers.add(returnDriver);
        }
        return returnDrivers;
    }

    @Override
    public List<DriverEntity> getDriverListForOrder(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting Driver list for order -----");
        List<DriverEntity> driverEntities;
        String areaId = requestJsonDto.getArea().getAreaId();
        AreaEntity areaEntity = null;
        String storeId = requestJsonDto.getStore().getStoreId();
        StoreEntity storeEntity = storeDao.findById(storeId);

        if (storeEntity == null) {
            log.error("----- Store does not exist. -----");
            throw new YSException("STR002");
        }
        if (areaId != null) {
            areaEntity = areaDao.findByAreaId(areaId);
            if (areaEntity == null) {
                log.error("----- Area does not exist. -----");
                throw new YSException("ARE001");
            }
        }

        driverEntities = driverDao.findStoreDriverForOrder(storeId);
        List<DriverEntity> driverList = new ArrayList<>();
        String fields = "driverId,driverType,availabilityStatus,vehicleType,vehicleNumber,licenseNumber,experienceLevel,currentOrderCount,lastOrderDelivered,user,store,latitude,longitude,lastOrderDelivered,currentOrderCount,rating,streetAddress,timeRequiredToCompleteOrders";

        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "userId,firstName,lastName,emailAddress,mobileNumber,userStatus");
        assoc.put("store", "storeId,givenLocation1,givenLocation2,storeBrand");

        Map<String, String> subAssoc = new HashMap<>();
        subAssoc.put("storeBrand", "storeBrandId,brandName,merchant");
        subAssoc.put("merchant", "merchantId,user");
        subAssoc.put("user", "userId");

        List<Integer> storesNotDriverIds = new ArrayList<>();
        for (StoresNotDriverEntity storesNotDriver : storeEntity.getStoresNotDrivers()) {
            storesNotDriverIds.add(storesNotDriver.getDriver().getId());
        }
        for (DriverEntity driver : driverEntities) {
            if (!storesNotDriverIds.contains(driver.getId())) {
                driver.setCurrentOrderCount(driverDao.getDriversOrderCount(driver.getDriverId()));
                OrderEntity lastOrder = driverDao.getDriversLastOrder(driver.getDriverId());
                if (lastOrder != null && lastOrder.getDeliveredDate() != null) {
                    driver.setLastOrderDelivered(lastOrder.getDeliveredDate());
                } else if (driver.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE) && driver.getCurrentOrderCount() == 0) {
                    for (DriversWorkLogEntity workLog : driver.getWorkLogs()) {
                        if (workLog.getToDateTime() == null) {
                            driver.setLastOrderDelivered(workLog.getFromDateTime());
                        }
                    }
                }
                Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
                Long orderCompletionTime = timeRequiredToCompleteOrder(driver, timePerKm);
                Long orderCompletionTimeInHour = (orderCompletionTime / (60 * 60));
                driver.setTimeRequiredToCompleteOrders(orderCompletionTime);
                driver.setRating(driverDao.getDriversAvgRating(driver.getDriverId()));
                log.info(driver.getUser().getFirstName() + " order count: " + driver.getCurrentOrderCount() + " experience level: " + driver.getExperienceLevel());
                log.info(orderCompletionTimeInHour);
                log.info(Long.valueOf(systemPropertyService.readPrefValue(PreferenceType.ORDER_COMPLETION_MAXIMUM_TIME)));

                if (((driver.getCurrentOrderCount() < Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.MAX_NUM_ORDER_FOR_DRIVER))
                        && driver.getExperienceLevel().equals(DriverExperienceLevel.EXPERIENCED))
                        || (driver.getCurrentOrderCount().equals(0)
                        && driver.getExperienceLevel().equals(DriverExperienceLevel.TRAINEE)))
                        && (orderCompletionTimeInHour <= Long.valueOf(systemPropertyService.readPrefValue(PreferenceType.ORDER_COMPLETION_MAXIMUM_TIME)))) {
                    if (areaEntity == null) {
                        driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                    } else {
                        List<Integer> driverAreaIds = new ArrayList<>();
                        for (DriversAreaEntity driversAreaEntity : driver.getDriversArea()) {
                            driverAreaIds.add(driversAreaEntity.getArea().getId());
                        }
                        if (driverAreaIds.contains(areaEntity.getId())) {
                            driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                        }
                    }
                }
            }
        }
        return driverList;
    }

    @Override
    public List<StoreEntity> getStoreAllowingDriver(String userId) throws Exception {
        log.info("Gettting store allowing driver list");
        UserEntity userEntity = userDao.findByUserId(userId);
        if (userEntity == null)
            throw new YSException("USR001");
        List<StoreEntity> storeEntities = new ArrayList<>();
        String fields = "storeId,givenLocation1,givenLocation1,area,storeBrand,addressNote,latitude,longitude";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("storeBrand", "brandName");
        assoc.put("area", "areaName");
        if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            for (StoreBrandEntity storeBrand : userEntity.getMerchant().getStoreBrand()) {
                if (storeBrand.getStatus().equals(Status.ACTIVE)) {
                    for (StoreEntity store : storeBrand.getStores()) {
                        if (store.getStatus().equals(Status.ACTIVE) && store.getAllowStoresDriver() != null && store.getAllowStoresDriver() && !store.getStoresAreas().isEmpty() && (store.getIsDistanceBaseRatePlan() || !store.getRatePlans().isEmpty()) && ((store.getAllowUnknownAreaServing() != null && store.getAllowUnknownAreaServing()) || !store.getUnknownAreaRatePlans().isEmpty())) {
                            store.setAddressNote(store.getStoreBrand().getBrandName() + "-" + store.getGivenLocation1() + "," + store.getArea().getAreaName());
                            storeEntities.add((StoreEntity) ReturnJsonUtil.getJsonObject(store, fields, assoc));
                        }
                    }
                }
            }
        } else if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            StoreEntity store = userEntity.getStoreManager().getStore();
            if (store.getStatus().equals(Status.ACTIVE) && store.getAllowStoresDriver() != null && store.getAllowStoresDriver() && !store.getStoresAreas().isEmpty() && (store.getIsDistanceBaseRatePlan() || !store.getRatePlans().isEmpty()) && ((store.getAllowUnknownAreaServing() != null && store.getAllowUnknownAreaServing()) || !store.getUnknownAreaRatePlans().isEmpty())) {
                store.setAddressNote(store.getStoreBrand().getBrandName() + "-" + store.getGivenLocation1() + "," + store.getArea().getAreaName());
                storeEntities.add((StoreEntity) ReturnJsonUtil.getJsonObject(store, fields, assoc));
            }
        } else {
            for (StoreEntity storeEntity : storeDao.findAll()) {
                if (storeEntity.getStatus().equals(Status.ACTIVE) && storeEntity.getAllowStoresDriver() != null && storeEntity.getAllowStoresDriver() && storeEntity.getStoreBrand().getStatus().equals(Status.ACTIVE)
                        && storeEntity.getStoreBrand().getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE)
                        && !storeEntity.getStoresAreas().isEmpty() && (storeEntity.getIsDistanceBaseRatePlan() || !storeEntity.getRatePlans().isEmpty()) && ((storeEntity.getAllowUnknownAreaServing() != null && storeEntity.getAllowUnknownAreaServing()) || !storeEntity.getUnknownAreaRatePlans().isEmpty())) {
                    storeEntity.setAddressNote(storeEntity.getStoreBrand().getBrandName() + "-" + storeEntity.getGivenLocation1() + "," + storeEntity.getArea().getAreaName());
                    storeEntities.add((StoreEntity) ReturnJsonUtil.getJsonObject(storeEntity, fields, assoc));
                }
            }
        }
        return storeEntities;
    }


    @Override
    public void cancelOrder(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Canceling the order -----");
        UserEntity userEntity = userDao.findByUserId(SessionManager.getFakeUserId());
        if (headerDto.getOrderId() == null)
            throw new YSException("ORD010");
        if (requestJsonDto.getOrderCancelReason() == null || requestJsonDto.getOrderCancelReason().isEmpty())
            throw new YSException("ORD011");

        OrderEntity orderEntity = orderDao.findById(headerDto.getOrderId());
        if (orderEntity == null)
            throw new YSException("ORD004");
        if (orderEntity.getOrderStatus().equals(OrderStatus.DELIVERED))
            throw new YSException("ORD012");
        if (orderEntity.getOrderStatus().equals(OrderStatus.CANCELLED))
            throw new YSException("ORD013");


        OrderCancelEntity orderCancelEntity = new OrderCancelEntity();
        orderCancelEntity.setOrderStatus(orderEntity.getOrderStatus());
        orderCancelEntity.setCancelledDate(new Timestamp(System.currentTimeMillis()));
        orderCancelEntity.setReason(requestJsonDto.getOrderCancelReason());
        orderCancelEntity.setUser(userEntity);
        orderCancelEntity.setOrder(orderEntity);
        orderEntity.setOrderCancel(orderCancelEntity);
        orderEntity.setOrderStatus(OrderStatus.CANCELLED);
        orderDao.update(orderEntity);

        //send push notification to driver
        if (orderEntity.getDriver() != null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN003", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
    }


    @Override
    public List<StoreEntity> getStoreListByUserRole(String userId) throws Exception {
        log.info("Getting store by user role: " + userId);
        UserEntity userEntity = userDao.findByUserId(userId);
        if (userEntity == null)
            throw new YSException("USR001");
        List<StoreEntity> storeEntities = new ArrayList<>();
        if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            if (userEntity.getStoreManager() != null && userEntity.getStoreManager().getStore() != null) {
                storeEntities.add(userEntity.getStoreManager().getStore());
            }
        } else if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            if (userEntity.getMerchant() != null) {
                for (StoreBrandEntity storeBrandEntity : userEntity.getMerchant().getStoreBrand()) {
                    if (storeBrandEntity.getStatus().equals(Status.ACTIVE)) {
                        for (StoreEntity storeEntity : storeBrandEntity.getStores()) {
                            if (storeEntity.getStatus().equals(Status.ACTIVE))
                                storeEntities.add(storeEntity);
                        }
                    }
                }
            }
        } else {
            storeEntities = storeDao.findAllActive();
        }
        List<StoreEntity> stores = new ArrayList<>();
        String fields = "storeId,givenLocation1,latitude,longitude,area,storeBrand";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("storeBrand", "brandName");
        assoc.put("area", "areaName");

        for (StoreEntity store : storeEntities)
            stores.add((StoreEntity) ReturnJsonUtil.getJsonObject(store, fields, assoc));

        for (StoreEntity storeEntity : stores) {
            storeEntity.setAddressNote(storeEntity.getStoreBrand().getBrandName() + "-" + storeEntity.getGivenLocation1() + "," + storeEntity.getArea().getAreaName());
            storeEntity.setGivenLocation1(null);
            storeEntity.setStoreBrand(null);
            storeEntity.setArea(null);
        }
        return stores;
    }

    @Override
    public PaginationDto getOrderByStatus(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting Order by status. -----");
        Page page = requestJsonDto.getPage();
        UserEntity user = requestJsonDto.getUser();
        StoreEntity store = requestJsonDto.getStore();
        StoreBrandEntity storeBrand = requestJsonDto.getStoreBrand();
        List<OrderStatus> orderStatus = requestJsonDto.getOrderStatuses();
        PaginationDto paginationDto = new PaginationDto();

        Integer totalRows = 0;
        List<OrderEntity> orderEntities = new ArrayList<>();

        if (page != null && page.getSearchFor() != null && !page.getSearchFor().equals("")) {
            Map<String, String> fieldsMap = new HashMap<>();
            fieldsMap.put("self", "orderId,orderDisplayId,receiverName");
            fieldsMap.put("store", "givenLocation1,givenLocation2,addressNote,contactNo");
            fieldsMap.put("store#storeBrand", "brandName");
            fieldsMap.put("customer#user", "firstName,lastName,emailAddress,mobileNumber");
            //fieldsMap.put("driver#user", "firstName,lastName,emailAddress,mobileNumber");
            page.setSearchFields(fieldsMap);
        }

        if (user != null && user.getUserId() != null) {
            UserEntity userEntity = userDao.findByUserId(user.getUserId());
            if (userEntity == null) {
                log.error("----- User does not exist. -----");
                throw new YSException("USR001");
            }
            if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
                totalRows = orderDao.getNumberOfStoreManagerOrder(page, userEntity.getStoreManager().getStore().getStoreId(), orderStatus);
                page.setTotalRows(totalRows);
                paginationDto.setNumberOfRows(totalRows);
                orderEntities = orderDao.getStoreManagerOrder(page, userEntity.getStoreManager().getStore().getStoreId(), orderStatus);
            } else if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
                totalRows = orderDao.getNumberOfMerchantOrder(page, userEntity.getUserId(), orderStatus);
                page.setTotalRows(totalRows);
                paginationDto.setNumberOfRows(totalRows);
                orderEntities = orderDao.getMerchantOrder(page, user.getUserId(), orderStatus);
            } else if (userEntity.getUserRole().equals(UserRole.ROLE_ADMIN) || userEntity.getUserRole().equals(UserRole.ROLE_MANAGER) || userEntity.getUserRole().equals(UserRole.ROLE_CSR)) {
                totalRows = orderDao.getNumberOfAllOrder(page, orderStatus);
                page.setTotalRows(totalRows);
                paginationDto.setNumberOfRows(totalRows);
                orderEntities = orderDao.getAllOrder(page, orderStatus);
            }

        } else if (store != null && store.getStoreId() != null) {
            StoreEntity storeEntity = storeDao.findById(store.getStoreId());
            if (storeEntity == null) {
                log.error("----- Store does not exist. -----");
                throw new YSException("STR002");
            }
            totalRows = orderDao.getNumberOfStoreOrder(page, storeEntity.getStoreId(), orderStatus);
            page.setTotalRows(totalRows);
            paginationDto.setNumberOfRows(totalRows);
            orderEntities = orderDao.getStoreOrder(page, storeEntity.getStoreId(), orderStatus);
        } else if (storeBrand != null) {
            StoreBrandEntity storeBrandEntity = storeBrandDao.findById(storeBrand.getStoreBrandId());
            if (storeBrandEntity == null) {
                log.error("----- StoreBrand does not exist. -----");
                throw new YSException("STR001");
            }
            totalRows = orderDao.getNumberOfStoreBrandOrder(page, storeBrandEntity.getStoreBrandId(), orderStatus);
            page.setTotalRows(totalRows);
            paginationDto.setNumberOfRows(totalRows);
            orderEntities = orderDao.getStoreBrandOrder(page, storeBrandEntity.getStoreBrandId(), orderStatus);
        }

        List<OrderEntity> returnOrders = new ArrayList<>();

        String fields = "orderId,orderDisplayId,orderDescription,driverType,orderDeliveryDate,pickupDate,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,driver";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,latitude,longitude,priority,area,storeBrand,allowStoresDriver");
        assoc.put("customer", "customerId,user");
        assoc.put("driver", "driverId,user,latitude,longitude");
        assoc.put("customersArea", "customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude");

        for (OrderEntity order : orderEntities) {
            returnOrders.add((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc));
        }

        paginationDto.setData(returnOrders);
        return paginationDto;
    }

    @Override
    public Map<String, Integer> getNumberOfOrderByStatus(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting number of orders by status. -----");

        Map<String, Integer> numberOfOrderByStatus = new HashMap<>();

        UserEntity user = requestJsonDto.getUser();
        StoreEntity store = requestJsonDto.getStore();
        StoreBrandEntity storeBrand = requestJsonDto.getStoreBrand();

        if (user != null && user.getUserId() != null) {
            UserEntity userEntity = userDao.findByUserId(user.getUserId());
            if (userEntity == null) {
                log.error("----- User does not exist. -----");
                throw new YSException("USR001");
            }
            if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
                for (OrderStatus orderStatus : OrderStatus.values()) {
                    numberOfOrderByStatus.put(orderStatus.toString(), orderDao.getNumberOfStoreManagerOrder(userEntity.getStoreManager().getStore().getStoreId(), orderStatus));
                }
            } else if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
                for (OrderStatus orderStatus : OrderStatus.values()) {
                    numberOfOrderByStatus.put(orderStatus.toString(), orderDao.getNumberOfMerchantOrder(userEntity.getUserId(), orderStatus));
                }
            } else if (userEntity.getUserRole().equals(UserRole.ROLE_ADMIN) || userEntity.getUserRole().equals(UserRole.ROLE_MANAGER) || userEntity.getUserRole().equals(UserRole.ROLE_CSR)) {
                for (OrderStatus orderStatus : OrderStatus.values()) {
                    numberOfOrderByStatus.put(orderStatus.toString(), orderDao.getNumberOfAllOrder(orderStatus));
                }
            }
        } else if (store != null && store.getStoreId() != null) {
            StoreEntity storeEntity = storeDao.findById(store.getStoreId());
            if (storeEntity == null) {
                log.error("----- Store does not exist. -----");
                throw new YSException("STR002");
            }
            for (OrderStatus orderStatus : OrderStatus.values()) {
                numberOfOrderByStatus.put(orderStatus.toString(), orderDao.getNumberOfStoreOrder(store.getStoreId(), orderStatus));
            }
        } else if (storeBrand != null) {
            StoreBrandEntity storeBrandEntity = storeBrandDao.findById(storeBrand.getStoreBrandId());
            if (storeBrandEntity == null) {
                log.error("----- StoreBrand does not exist. -----");
                throw new YSException("STR001");
            }
            for (OrderStatus orderStatus : OrderStatus.values()) {
                numberOfOrderByStatus.put(orderStatus.toString(), orderDao.getNumberOfStoreBrandOrder(storeBrand.getStoreBrandId(), orderStatus));
            }
        }
        return numberOfOrderByStatus;
    }

    public List<OrderEntity> getCurrentOrderOfDrivers(String driverId) throws Exception {
        log.info("Getting current order of driver: " + driverId);
        DriverEntity driverEntity = driverDao.findById(driverId);
        if (driverEntity == null)
            throw new YSException("DRV001");
        List<OrderEntity> orders = driverDao.getDriversRunningOrderList(driverEntity.getDriverId());
        List<OrderEntity> returnOrders = new ArrayList<>();
        String fields = "orderId,orderDisplayId,orderDescription,orderDeliveryDate,pickupDate,estimatedTimeOfDelivery,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand,contactNo");
        assoc.put("customer", "customerId,user");
        assoc.put("customersArea", "customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude,street,parent");
        subAssoc.put("parent", "areaId,areaName,latitude,longitude,street,parent");
        for (OrderEntity order : orders) {
            returnOrders.add((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc));
        }
        return returnOrders;
    }

    @Override
    public PaginationDto getDriversByStoreBrands(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting Drivers list by storeBrands. -----");
        PaginationDto paginationDto = new PaginationDto();
        List<String> storeBrandIds = requestJsonDto.getStoreBrandIds();
        Integer totalRows = 0;
        if(storeBrandIds!=null){
            totalRows = driverDao.getNumberOfStoreBrandsDrivers(storeBrandIds);
        }
        Page page = requestJsonDto.getPage();
        page.setTotalRows(totalRows);
        paginationDto.setNumberOfRows(totalRows);
        List<DriverEntity> driverEntities = new ArrayList<>();
        if(storeBrandIds!=null) {
            driverDao.getStoreBrandsDrivers(storeBrandIds, page);
        }
        List<DriverEntity> anyOrderDrivers = driverDao.getAnyOrderDrivers();
        driverEntities.addAll(anyOrderDrivers);

        List<DriverEntity> returnDriverEntities = new ArrayList<>();

        String fields = "driverId,driverType,experienceLevel,previousOrderCount,user";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "firstName,lastName");
        for (DriverEntity driverEntity : driverEntities) {
            returnDriverEntities.add((DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity, fields, assoc));
        }
        paginationDto.setData(returnDriverEntities);
        return paginationDto;
    }

    @Override
    public void updateOrder(String orderId, OrderEntity order) throws Exception {
        log.info("Updating order : " + orderId);
        OrderEntity orderEntity = orderDao.findById(orderId);
        if (orderEntity == null)
            throw new YSException("ORD001");
        DriverEntity driverEntity = driverDao.findById(order.getDriver().getDriverId());
        if (driverEntity == null)
            throw new YSException("DRV001");
        orderEntity.setDeliveryCharge(order.getDeliveryCharge());
        orderEntity.setOrderDescription(order.getOrderDescription());
        orderEntity.setDriver(order.getDriver());
        orderEntity.setDriverType(driverEntity.getDriverType());
        orderEntity.setOrderDeliveryDate(order.getOrderDeliveryDate());
        orderEntity.setGrandTotal(order.getGrandTotal());
        orderEntity.setCustomersNoteToDriver(order.getCustomersNoteToDriver());
        if (order.getCustomer().getCustomersArea().get(0).getArea() != null) {
            AreaEntity area = areaDao.findByAreaId(order.getCustomer().getCustomersArea().get(0).getArea().getAreaId());
            orderEntity.getCustomer().getCustomersArea().get(0).setArea(area);
        }

        if (order.getCustomer().getCustomersArea().get(0).getCustomersAreaId() != null) {
            CustomersAreaEntity customersArea = customersAreaDao.findByCustomersAreaId(order.getCustomer().getCustomersArea().get(0).getCustomersAreaId());
            orderEntity.setCustomersArea(customersArea);
        }

        if (orderEntity.getCustomer().getCustomerId() != null) {
            CustomerEntity customer = customerDao.findByCustomerId(orderEntity.getCustomer().getCustomerId());

            if (orderEntity.getCustomer().getCustomersArea().get(0).getCustomersAreaId() == null) {
                CustomersAreaEntity customersArea = new CustomersAreaEntity();
                if (orderEntity.getCustomer().getCustomersArea().get(0).getArea() != null) {
                    AreaEntity area = areaDao.findByAreaId(orderEntity.getCustomer().getCustomersArea().get(0).getArea().getAreaId());
                    customersArea.setArea(area);
                } else {
                    customersArea.setLatitude(orderEntity.getCustomer().getCustomersArea().get(0).getLatitude());
                    customersArea.setLongitude(orderEntity.getCustomer().getCustomersArea().get(0).getLongitude());
                    customersArea.setAdditionalNote(orderEntity.getCustomer().getCustomersArea().get(0).getAdditionalNote());
                    customersArea.setStreet(orderEntity.getCustomer().getCustomersArea().get(0).getStreet());
                }
                customersArea.setStreet(orderEntity.getCustomer().getCustomersArea().get(0).getStreet());
                customersArea.setCustomer(customer);
                customersAreaDao.save(customersArea);
                orderEntity.setCustomersArea(customersArea);
            }
            orderEntity.setCustomer(customer);

        } else {
            orderEntity.getCustomer().getCustomersArea().get(0).setCustomer(orderEntity.getCustomer());
            orderEntity.setCustomersArea(orderEntity.getCustomer().getCustomersArea().get(0));
        }

        orderDao.update(orderEntity);
    }

    public Map<String, Integer> getOrderCountByDate(RequestJsonDto requestJsonDto) throws Exception{
        log.info("----- Getting number of orders by date. -----");
        Map<String, Integer> orderCountByDate = new HashMap<>();
        UserEntity user = requestJsonDto.getUser();
        Integer unassignedOrderCount=0, assignedOrderCount=0, totalOrderCount=0;
        if (user != null && user.getUserId() != null) {
            UserEntity userEntity = userDao.findByUserId(user.getUserId());
            if (userEntity == null) {
                log.error("----- User does not exist. -----");
                throw new YSException("USR001");
            }
            if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
                totalOrderCount = orderDao.getStoreOrderCountByDate(userEntity.getStoreManager().getStore().getStoreId(), requestJsonDto, OrderDisplayStatus.ALL);
                assignedOrderCount = orderDao.getStoreOrderCountByDate(userEntity.getStoreManager().getStore().getStoreId(), requestJsonDto, OrderDisplayStatus.RUNNING);
                unassignedOrderCount = orderDao.getStoreOrderCountByDate(userEntity.getStoreManager().getStore().getStoreId(), requestJsonDto, OrderDisplayStatus.WAITING);
            } else if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
                totalOrderCount = orderDao.getMerchantOrderCountByDate(userEntity.getMerchant().getMerchantId(), requestJsonDto, OrderDisplayStatus.ALL);
                assignedOrderCount = orderDao.getMerchantOrderCountByDate(userEntity.getMerchant().getMerchantId(), requestJsonDto, OrderDisplayStatus.RUNNING);
                unassignedOrderCount = orderDao.getMerchantOrderCountByDate(userEntity.getMerchant().getMerchantId(), requestJsonDto, OrderDisplayStatus.WAITING);
            } else if (userEntity.getUserRole().equals(UserRole.ROLE_ADMIN) || userEntity.getUserRole().equals(UserRole.ROLE_MANAGER) || userEntity.getUserRole().equals(UserRole.ROLE_CSR)) {
                totalOrderCount = orderDao.getAllOrderCountByDate(requestJsonDto, OrderDisplayStatus.ALL);
                assignedOrderCount = orderDao.getAllOrderCountByDate(requestJsonDto, OrderDisplayStatus.RUNNING);
                unassignedOrderCount = orderDao.getAllOrderCountByDate(requestJsonDto, OrderDisplayStatus.WAITING);
            }
            orderCountByDate.put("totalOrderCount", totalOrderCount);
            orderCountByDate.put("unassignedOrderCount", unassignedOrderCount);
            orderCountByDate.put("assignedOrderCount", assignedOrderCount);
        }
        return orderCountByDate;
    }

    @Override
    public PaginationDto getStatementList(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting list of statement. -----");
        Date fromDate = requestJsonDto.getFromDate();
        Date toDate = requestJsonDto.getToDate();

        String userId = requestJsonDto.getUser() != null ? requestJsonDto.getUser().getUserId() : null;
        UserEntity userEntity = userDao.findByUserId(userId);
        if(userEntity == null)
            throw new YSException("USR001");

        PaginationDto paginationDto = new PaginationDto();
        List<StatementEntity> statementEntities = new ArrayList<>();
        Page page = requestJsonDto.getPage();
        Integer totalRows;
        Map<String, String> fieldsMap = new HashMap<>();
        fieldsMap.put("self", "statementId,subscriptionRatePlan");
        fieldsMap.put("store", "givenLocation1,givenLocation2,addressNote");
        fieldsMap.put("store#storeBrand", "brandName");
        page.setSearchFields(fieldsMap);
        if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            totalRows = statementDao.getNumberOfMerchantStatements(userEntity.getMerchant().getMerchantId(), page, fromDate, toDate);
            paginationDto.setNumberOfRows(totalRows);
            if (page != null) {
                page.setTotalRows(totalRows);
            }
            statementEntities = statementDao.getMerchantStatements(userEntity.getMerchant().getMerchantId(), page, fromDate, toDate);
        } else if (userEntity.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)) {
            totalRows = statementDao.getNumberOfStoreStatements(userEntity.getStoreManager().getStore().getStoreId(), page, fromDate, toDate);
            paginationDto.setNumberOfRows(totalRows);
            if (page != null) {
                page.setTotalRows(totalRows);
            }
            statementEntities = statementDao.getStoreStatements(userEntity.getStoreManager().getStore().getStoreId(), page, fromDate, toDate);
        } else {
            if (SessionManager.getRole().equals(UserRole.ROLE_ADMIN) || SessionManager.getRole().equals(UserRole.ROLE_MANAGER)) {
                totalRows = statementDao.getNumberOfAllStatements(page, fromDate, toDate);
                paginationDto.setNumberOfRows(totalRows);
                if (page != null) {
                    page.setTotalRows(totalRows);
                }
                statementEntities = statementDao.getAllStatements(page, fromDate, toDate);
            }
        }

        String field = "statementId,paidStatus,fromDate,toDate,paidDate,statementUrl,statementAmount,subscriptionRate," +
                "extraOrderCount,extraOrderCharge,outOfServingAreaOrderCount,outOfServingAreaOrderCharge,subscriptionRatePlan,totalPayableAmount," +
                "distanceBasedOrderCount,distanceBasedOrderAmount,otherCharge,store,subscriptionRatePlanOrderCount,orderPlan";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,storeBrand");
        Map<String, String> subAssoc = new HashMap<>();
        subAssoc.put("storeBrand", "storeBrandId,brandName,merchant");
        subAssoc.put("merchant", "merchantId,businessTitle,user");
        subAssoc.put("user", "userId,firstName,lastName");

        List<StatementEntity> statementListFiltered = new ArrayList<>();
        for (StatementEntity statementEntity : statementEntities) {
            statementListFiltered.add((StatementEntity) ReturnJsonUtil.getJsonObject(statementEntity, field, assoc, subAssoc));
        }
        paginationDto.setData(statementListFiltered);
        return paginationDto;
    }

    @Override
    public StatementEntity getStatement(String statementId) throws Exception {
        log.info("----- Getting statement. -----");
        StatementEntity statementEntity = statementDao.findByStatementId(statementId);
        if (statementEntity == null) {
            log.error("----- Statement does not exist. -----");
            throw new YSException("STAT001");
        }
        String field = "statementId,paidStatus,fromDate,toDate,paidDate,statementUrl,statementAmount,subscriptionRate," +
                "extraOrderCount,extraOrderCharge,outOfServingAreaOrderCount,outOfServingAreaOrderCharge,subscriptionRatePlan,totalPayableAmount," +
                "distanceBasedOrderCount,distanceBasedOrderAmount,otherCharge,store,subscriptionRatePlanOrderCount,orderPlan";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,storeBrand");
        Map<String, String> subAssoc = new HashMap<>();
        subAssoc.put("storeBrand", "storeBrandId,brandName,merchant");
        subAssoc.put("merchant", "merchantId,businessTitle,user");
        subAssoc.put("user", "userId,firstName,lastName");

        return (StatementEntity) ReturnJsonUtil.getJsonObject(statementEntity, field, assoc, subAssoc);
    }

    @Override
    public void updateStatement(StatementEntity statement) throws Exception {
        log.info("----- Updating Statement -----");
        StatementEntity statementEntity = statementDao.findByStatementId(statement.getStatementId());
        if (statementEntity == null) {
            log.error("----- Statement does not exist. -----");
            throw new YSException("STAT001");
        }

        UserRole userRole = SessionManager.getRole();
        if (userRole.equals(UserRole.ROLE_MERCHANT) || userRole.equals(UserRole.ROLE_ADMIN) || userRole.equals(UserRole.ROLE_MANAGER)) {
            if (userRole.equals(UserRole.ROLE_MERCHANT)
                    && !SessionManager.getMerchantId().equals(statementEntity.getStore().getStoreBrand().getMerchant().getMerchantId())) {
                log.error("----- Statement does not belong to this merchant. -----");
                throw new YSException("STAT004");
            }
            if (statement.getExtraOrderCount() != null) {
                statementEntity.setExtraOrderCount(statement.getExtraOrderCount());
            }
            if (statement.getExtraOrderCharge() != null) {
                statementEntity.setExtraOrderCharge(statement.getExtraOrderCharge());
            }
            if (statement.getOutOfServingAreaOrderCount() != null) {
                statementEntity.setOutOfServingAreaOrderCount(statement.getOutOfServingAreaOrderCount());
            }
            if (statement.getOutOfServingAreaOrderCharge() != null) {
                statementEntity.setOutOfServingAreaOrderCharge(statement.getOutOfServingAreaOrderCharge());
            }
            if (statement.getDistanceBasedOrderCount() != null) {
                statementEntity.setDistanceBasedOrderCount(statement.getDistanceBasedOrderCount());
            }
            if (statement.getDistanceBasedOrderAmount() != null) {
                statementEntity.setDistanceBasedOrderAmount(statement.getDistanceBasedOrderAmount());
            }
            if (statement.getOtherCharge() != null) {
                statementEntity.setOtherCharge(statement.getOtherCharge());
            }
            BigDecimal totalPayableAmount = BigDecimal.ZERO;
            totalPayableAmount = totalPayableAmount
                    .add(BigDecimalUtil.checkNull(statementEntity.getSubscriptionRate()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getExtraOrderCharge()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getOutOfServingAreaOrderCharge()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getDistanceBasedOrderAmount()))
                    .add(BigDecimalUtil.checkNull(statementEntity.getOtherCharge()));
            statementEntity.setTotalPayableAmount(totalPayableAmount);
            statementDao.update(statementEntity);
        } else {
            log.error("----- Does not have permission to update statement. -----");
            throw new YSException("STAT005");
        }
    }

    @Override
    public void generateStatement(StatementEntity statement) throws Exception {
        log.info("----- Generating statement in PDF file format. -----");
        StatementEntity statementEntity = statementDao.findByStatementId(statement.getStatementId());
        if (statementEntity == null) {
            log.error("----- Statement does not exit. -----");
            throw new YSException("STAT001");
        }

        StoreEntity storeEntity = statementEntity.getStore();
        PDFGenerator statementGenerator = new PDFGenerator();

        Map<String, String> preferences = new HashMap<>();
        preferences.put("CURRENCY", systemPropertyService.readPrefValue(PreferenceType.CURRENCY));
        preferences.put("HELPLINE_NUMBER", systemPropertyService.readPrefValue(PreferenceType.HELPLINE_NUMBER));
        preferences.put("SUPPORT_EMAIL", systemPropertyService.readPrefValue(PreferenceType.SUPPORT_EMAIL));
        preferences.put("COMPANY_NAME", systemPropertyService.readPrefValue(PreferenceType.COMPANY_NAME));
        preferences.put("COMPANY_ADDRESS", systemPropertyService.readPrefValue(PreferenceType.COMPANY_ADDRESS));
        preferences.put("SERVER_URL", systemPropertyService.readPrefValue(PreferenceType.SERVER_URL));

        String imageUrl = systemPropertyService.readPrefValue(PreferenceType.LOGO_FOR_PDF_EMAIL);

        String statementUrl = statementGenerator.generateStatement(storeEntity.getStoreBrand().getMerchant(), statementEntity, storeEntity, imageUrl, preferences);

        if (statementUrl == null) {
            throw new YSException("STAT003");
        }

        statementEntity.setStatementUrl(statementUrl);
        statementDao.update(statementEntity);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fromDate = dateFormat.format(statementEntity.getFromDate().getTime());
        String toDate = dateFormat.format(statementEntity.getToDate().getTime());

        String email = storeEntity.getStoreBrand().getMerchant().getUser().getEmailAddress();
        if (email != null && !email.equals("")) {
            String message = EmailMsg.sendInvoiceEmail(storeEntity, fromDate, toDate, systemPropertyService.readPrefValue(PreferenceType.SERVER_URL));
            sendAttachmentEmail(email, message, "get invoice-" + fromDate + "-" + toDate, statementUrl);
        }
        log.info("Email sent successfully with attachment: " + statementUrl + " for store " + storeEntity.getId() + " Email: " + email);
    }

    public PaginationDto getOrderByDrivers(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting orders by drivers. -----");
        PaginationDto paginationDto = new PaginationDto();
        Page page = requestJsonDto.getPage();
        List<String> driverIds = requestJsonDto.getDriverIds();
        Date fromDate = requestJsonDto.getFromDate();
        Date toDate = requestJsonDto.getToDate();
        List<OrderStatus> orderStatuses = requestJsonDto.getOrderStatuses();
        List<String> storeIds = requestJsonDto.getStoreIds();
        if(storeIds==null){
            storeIds = new ArrayList<>();
            if(requestJsonDto.getStoreManager().getStoreManagerId()!=null) {
                StoreManagerEntity storeManagerEntity = storeManagerDao.findByFakeId(requestJsonDto.getStoreManager().getStoreManagerId());
                if(storeManagerEntity==null)
                    throw new YSException("STRMGR001");
                storeIds.add(storeManagerEntity.getStore().getStoreId());
            }
        }


        Integer totalRows = orderDao.getNumberOfOrdersByDrivers(driverIds, fromDate, toDate, orderStatuses, storeIds);
        page.setTotalRows(totalRows);
        paginationDto.setNumberOfRows(totalRows);
        List<OrderEntity> orderEntities = orderDao.getOrdersByDrivers(driverIds, fromDate, toDate, page, orderStatuses, storeIds);

        List<OrderEntity> returnOrders = new ArrayList<>();

        String fields = "orderId,orderDisplayId,orderDescription,driverType,orderDeliveryDate,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,driver,orderPlan,distanceTravelInKM";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store", "storeId,givenLocation1,givenLocation2,latitude,longitude,priority,area,storeBrand");
        assoc.put("customer", "customerId,user");
        assoc.put("driver", "driverId,user,latitude,longitude");
        assoc.put("customersArea", "customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude");

        for (OrderEntity order : orderEntities) {
            OrderEntity orderFiltered = (OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc);
            returnOrders.add(orderFiltered);
        }

        paginationDto.setData(returnOrders);
        return paginationDto;
    }

    @Override
    public PaginationDto getStoreBrandListByMerchants(RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Getting List of StoreBrand by merchants. -----");
        PaginationDto paginationDto = new PaginationDto();
        List<String> merchantIds = requestJsonDto.getMerchantIds();
        Page page = requestJsonDto.getPage();

        Integer totalRows = storeBrandDao.getNumberOfMerchantsStoreBrands(merchantIds);
        page.setTotalRows(totalRows);
        paginationDto.setNumberOfRows(totalRows);
        List<StoreBrandEntity> storeBrandEntities = storeBrandDao.getMerchantsStoreBrands(merchantIds, page);

        String field = "storeBrandId,brandName,brandLogo,status,stores";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("stores", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,status");

        List<StoreBrandEntity> storeBrandList = new ArrayList<>();
        for (StoreBrandEntity storeBrandEntity : storeBrandEntities) {
            StoreBrandEntity storeBrandEntityFiltered = (StoreBrandEntity) ReturnJsonUtil.getJsonObject(storeBrandEntity, field, assoc);
            storeBrandList.add(storeBrandEntityFiltered);
        }
        paginationDto.setData(storeBrandList);
        return paginationDto;
    }
}

