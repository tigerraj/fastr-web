package com.yetistep.anyorder.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.aspect.annotations.MobileService;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.DriverMobileService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.service.inf.WebRTCService;
import com.yetistep.anyorder.util.*;
import com.yetistep.anyorder.webrtc.pojo.ResponseDriverDto;
import com.yetistep.anyorder.webrtc.pojo.ResponseOrderDto;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DriverMobileServiceImpl extends AbstractManager implements DriverMobileService {

    private static final Logger log = Logger.getLogger(DriverMobileServiceImpl.class);

    @Autowired
    UserDao userDao;

    @Autowired
    UserDeviceDao userDeviceDao;

    @Autowired
    DriverDao driverDao;

    @Autowired
    AreaDao areaDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    DriversAreaDao driversAreaDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    StoresNotDriverDao storesNotDriverDao;

    @Autowired
    StoreBrandDao storeBrandDao;

    @Autowired
    MerchantDao merchantDao;

    @Autowired
    DriversWorkLogDao driversWorkLogDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    WebRTCService webRTCService;

    @Override
    public DriverEntity driverLogin(HeaderDto headerDto, UserDeviceEntity userDevice) throws Exception {
        String userName = headerDto.getUsername();
        String password = headerDto.getPassword();
        String accessToken = UUID.randomUUID().toString();
        userDevice.setAccessToken(MD5Hash.getMD5(accessToken));
        UserEntity driver = userDao.findByUsernameMobile(userName);
        if (driver == null)
            throw new YSException("DRV001");
        if (!driver.getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("DRV011");


        if (driver.getDriver().getStore() != null) {
            if (!driver.getDriver().getStore().getStatus().equals(Status.ACTIVE))
                throw new YSException("STR004");

            isVerifiableStore(driver.getDriver().getStore(), true);
        }
        GeneralUtil.matchDBPassword(password, driver.getPassword());
        List<UserDeviceEntity> dbUserDevices = driver.getUserDevices();
        if (dbUserDevices.size() > 0) {
            List<String> uuidList = new ArrayList<>();
            for (UserDeviceEntity dbUserDevice : dbUserDevices) {
                uuidList.add(dbUserDevice.getDeviceUuid());
                if (dbUserDevice.getDeviceUuid() != null) {
                    if (dbUserDevice.getDeviceUuid().equals(userDevice.getDeviceUuid())) {
                        dbUserDevice.setAccessToken(userDevice.getAccessToken());
                        if (userDevice.getDeviceToken() != null)
                            dbUserDevice.setDeviceToken(userDevice.getDeviceToken());
                    }
                }
            }
            if (!uuidList.contains(userDevice.getDeviceUuid())) {
                userDevice.setUser(driver);
                dbUserDevices.add(userDevice);
            }
        } else {
            userDevice.setUser(driver);
            dbUserDevices.add(userDevice);
        }

        userDao.update(driver);
        DriverEntity driverEntity = new DriverEntity();
        driverEntity.setDriverId(driver.getDriver().getDriverId());
        driverEntity.setAccessToken(accessToken);
        driverEntity.setUpdateLocationInMinute(Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVER_LOCATION_UPDATE_TIME)));
        return driverEntity;
    }

    @Override
    public List<OrderEntity> getDriverCurrentOrderList(HeaderDto headerDto) throws Exception{
        log.info("Getting current order of driver");
        DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
        if(driverEntity==null)
            throw new YSException("DRV001");
        List<OrderEntity> orders = orderDao.findActiveOrdersOfDriver(driverEntity.getDriverId());
        List<OrderEntity> returnOrders = new ArrayList<>();
        String fields = "orderId,orderDisplayId,orderDescription,orderDeliveryDate,estimatedTimeOfDelivery,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,elapsedTimeInMin";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand,contactNo");
        assoc.put("customer","customerId,user");
        assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude,street,parent");
        subAssoc.put("parent", "areaId,areaName,latitude,longitude,street,parent");
        for(OrderEntity order: orders) {
            if(order.getDeliveryCharge()==null)
                order.setDeliveryCharge(BigDecimal.ZERO);
            if(order.getPrioritized()==null)
                order.setPrioritized(1);
            if(order.getOrderPriority()==null)
                order.setOrderPriority(1);
            if (order.getCustomersNoteToDriver() == null)
                order.setCustomersNoteToDriver("");
            if (order.getPaymentMode() != null) {
                if (order.getPaymentMode().equals(PaymentMode.CARD))
                    order.setPaymentMode(PaymentMode.ONLINE);
            }
            Long elpasedTime = 0L;
            for (OrdersDriverEntity ordersDriverEntity : order.getOrdersDrivers()) {
                if (ordersDriverEntity.getDriver().getId().equals(order.getDriver().getId())) {
                    elpasedTime = DateUtil.calculateTimeInMinute(ordersDriverEntity.getAssignedDate(), DateUtil.getCurrentTimestampSQL());
                    break;
                }
            }
            order.setElapsedTimeInMin(elpasedTime.intValue());

            //calculate estimated time of delivery
            if(order.getDriver()!=null){
                BigDecimal totalPreviousOrderEtd = driverDao.getDriversPreviousOrderTotalEtd(order.getDriver().getDriverId(), order.getId());
                for (OrdersDriverEntity ordersDriverEntity: order.getOrdersDrivers()){
                    if (ordersDriverEntity.getDriverStatus().equals(OrdersDriverStatus.RUNNING))
                        if(ordersDriverEntity.getAssignedDate()!=null && order.getEstimatedDeliveryTime()!=null)
                            order.setEstimatedTimeOfDelivery(new Timestamp(ordersDriverEntity.getAssignedDate().getTime()+(order.getEstimatedDeliveryTime().add(totalPreviousOrderEtd)).multiply(new BigDecimal(60*1000)).longValue()));
                }
            }
            /*if (order.getCustomersArea().getArea() != null) {
                AreaEntity area = order.getCustomersArea().getArea().getParent();
                if (area != null && area.getParent() != null) {
                    while (area.getParent() != null) {
                        area = area.getParent();
                        area.setChild(Collections.singletonList(area));
                    }
                }
                order.getCustomersArea().setArea(area);
            }*/
            returnOrders.add((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc));
        }
        return returnOrders;
    }

    @Override
    public PaginationDto driverPastOrderList(HeaderDto headerDto, Page page) throws Exception {
        log.info("Getting past order of driver: " + headerDto.getDriverId());
        PaginationDto paginationDto = new PaginationDto();
        Integer totalRows = orderDao.getNoOfPastOrdersByDriverId(headerDto.getDriverId());
        paginationDto.setNumberOfRows(totalRows);

        if (page != null) {
            page.setTotalRows(totalRows);
        }
        List<OrderEntity> pastOrders = orderDao.getPastOrdersByDriverId(headerDto.getDriverId(), page);
        for(OrderEntity order : pastOrders){
             order.setDeliveryCharge(order.getDeliveryCharge()==null?BigDecimal.ZERO:order.getDeliveryCharge());
            if (order.getDeliveryCharge() == null)
                order.setDeliveryCharge(BigDecimal.ZERO);
            if (order.getPrioritized() == null)
                order.setPrioritized(1);
            if (order.getOrderPriority() == null)
                order.setOrderPriority(1);
            if (order.getCustomersNoteToDriver() == null)
                order.setCustomersNoteToDriver("");
            if (order.getPaymentMode().equals(PaymentMode.CARD))
                order.setPaymentMode(PaymentMode.ONLINE);
        }
        paginationDto.setData(pastOrders);
        String fields = "numberOfRows,data";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("data","orderId,orderDisplayId,orderDescription,orderDeliveryDate,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,totalBillAmount,grandTotal");
        subAssoc.put("customer","customerId,user");
        subAssoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand,contactNo");
        subAssoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area","areaId,areaName,latitude,longitude,street");
        return (PaginationDto) ReturnJsonUtil.getJsonObject(paginationDto, fields, assoc, subAssoc);
    }

    @Override
    @MobileService
    public DriverEntity getDriveProfile(HeaderDto headerDto) throws Exception{
        log.info("Getting driver detail: "+headerDto.getDriverId());
        DriverEntity driverEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken())).getUser().getDriver();
        if(driverEntity==null)
            throw new YSException("DRV001");
        String fields = "driverId,driverType,availabilityStatus,vehicleType,licenseNumber,latitude,longitude,experienceLevel,previousOrderCount,currentOrderCount,rating,user,store,rating,updateLocationInMinute";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String>  subAssoc = new HashMap<>();
        assoc.put("user","userId,firstName,lastName,mobileNumber,emailAddress,profileImage");
        assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand,contactNo");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("area","areaId,areaName,latitude,longitude,street");

        Integer previousOrderCount = orderDao.getNoOfDeliveredOrdersByDriverId(headerDto.getDriverId());
        Integer currentOrderCount = orderDao.getNoOfCurrentOrdersByDriverId(headerDto.getDriverId());
        driverEntity.setPreviousOrderCount(previousOrderCount);
        driverEntity.setCurrentOrderCount(currentOrderCount);
        driverEntity.setRating(driverDao.getDriversAvgRating(driverEntity.getDriverId()));
        driverEntity.setUpdateLocationInMinute(Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVER_LOCATION_UPDATE_TIME)));
        return (DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity, fields, assoc, subAssoc);
    }

    @Override
    public void updateOrderStatus(HeaderDto headerDto, OrderEntity order) throws Exception{
        log.info("Updating order status of order: "+headerDto.getId() +" to status: "+ order.getOrderStatus());
        OrderEntity orderEntity = orderDao.findById(headerDto.getId());
        if(orderEntity==null)
            throw new YSException("ORD004");
        if (orderEntity.getOrderStatus().ordinal() >= order.getOrderStatus().ordinal()) {
            throw new YSException("ORD014");
        }
        orderEntity.setOrderStatus(order.getOrderStatus());
        orderDao.update(orderEntity);
        /*if(orderEntity.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_DELIVER))
            throw new YSException("ORD006");*/
        if(orderEntity.getOrderStatus().equals(OrderStatus.DELIVERED)){
            if(order.getOrderStatus().equals(OrderStatus.DELIVERED) || order.getOrderStatus().equals(OrderStatus.CANCELLED)){
                BigDecimal distanceInKm = BigDecimal.ZERO;
                if(DistanceType.valueOf(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)).equals(DistanceType.ACTUAL_DISTANCE)) {
                    Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
                    Map<String, Long> distanceData = new HashMap<>();
                    if(orderEntity.getCustomersArea().getArea()!=null) {
                        distanceData = GeoCodingUtil.calculateDistance(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                                orderEntity.getCustomersArea().getArea().getLatitude(), orderEntity.getCustomersArea().getArea().getLongitude(), timePerKm);
                    }else{
                        distanceData = GeoCodingUtil.calculateDistance(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                                orderEntity.getCustomersArea().getLatitude(), orderEntity.getCustomersArea().getLongitude(), timePerKm);
                    }
                    distanceInKm = new BigDecimal(distanceData.get("distance") / 1000.0);
                    orderEntity.setDistanceTravelInKM(distanceInKm);
                    orderEntity.setDistanceType(DistanceType.ACTUAL_DISTANCE);
                }else{
                    if(orderEntity.getCustomersArea().getArea()!=null) {
                        distanceInKm = new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                                orderEntity.getCustomersArea().getArea().getLatitude(), orderEntity.getCustomersArea().getArea().getLongitude()));
                    }else {
                        distanceInKm = new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                                orderEntity.getCustomersArea().getLatitude(), orderEntity.getCustomersArea().getLongitude()));
                    }
                    orderEntity.setDistanceTravelInKM(distanceInKm);
                    orderEntity.setDistanceType(DistanceType.AIR_DISTANCE);
                }
            }
            orderEntity.setDeliveredDate(DateUtil.getCurrentTimestampSQL());
            orderEntity.setReceiverName(order.getReceiverName());
            if (order.getCustomerSignImage() != null && !order.getCustomerSignImage().isEmpty()) {
                log.info("Uploading customer signature to S3 Bucket ");
                String dir = MessageBundle.separateString("/", "Order_" + orderEntity.getId(), "Customer_" + orderEntity.getCustomer().getId());
                boolean isLocal = MessageBundle.isLocalHost();
                String customerSignatureImageName = "customer_signature_logo" + (isLocal ? "_tmp_" : "_") + orderEntity.getCustomer().getId() + System.currentTimeMillis();
                String s3PathLogo = GeneralUtil.saveImageToBucket(order.getCustomerSignImage(), customerSignatureImageName, dir, true);
                orderEntity.setCustomerSignImage(s3PathLogo);
            }
        }

        orderDao.update(orderEntity);

        ResponseOrderDto socketOrder = new ResponseOrderDto();
        socketOrder.setOrderId(orderEntity.getOrderId());
        socketOrder.setOrderStatus(orderEntity.getOrderStatus());
        if(orderEntity.getDriver()!=null)
            socketOrder.setDriverId(orderEntity.getDriver().getDriverId());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.ORDER_STATUS.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketOrder));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session: sessions){
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }
        //send push notification to driver
        List<String> tokens = userDeviceDao.findDeviceTokenByStoreId(orderEntity.getStore().getId());
        if (tokens.size() > 0) {
            String pushMessage = "";
            if (order.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_DELIVER))
                pushMessage = "SMGR001";
            else if (order.getOrderStatus().equals(OrderStatus.AT_STORE))
                pushMessage = "SMGR002";
            else if (order.getOrderStatus().equals(OrderStatus.AT_CUSTOMER))
                pushMessage = "SMGR003";
            else if (order.getOrderStatus().equals(OrderStatus.DELIVERED))
                pushMessage = "SMGR004";
            PushNotification pushNotification = new PushNotification();
            pushNotification.setTokens(tokens);
            Map<String, String> message = new HashMap<>();
            message.put("message", MessageBundle.getMessage(pushMessage, "push_notification.properties"));
            message.put("redirect", PushNotificationRedirect.ORDER.toString());
            message.put("id", orderEntity.getOrderId().toString());
            pushNotification.setMessage(message);
            pushNotification.setNotifyTo(NotifyTo.STORE_MANAGER);
            PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
            log.info("Push notification has been sent successfully");
        }
    }

    @Override
    public void updateDriverStatus(HeaderDto headerDto, DriverEntity driver) throws Exception {
        log.info("Updating driver status of id: " + headerDto.getDriverId() + " to status: " + driver.getAvailabilityStatus());
        DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
        if (driverEntity == null)
            throw new YSException("DRV001");
        if (driver.getAvailabilityStatus().equals(AvailabilityStatus.UNAVAILABLE) && driverDao.getDriversOrderCount(driverEntity.getDriverId())>0)
            throw new YSException("DRV009");
        if (driverEntity.getStore() != null && driver.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE) &&
                (!driverEntity.getStore().getStatus().equals(Status.ACTIVE) || !driverEntity.getStore().getStoreBrand().getStatus().equals(Status.ACTIVE) || !driverEntity.getStore().getStoreBrand().getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE)))
            throw new YSException("DRV012");
        driverEntity.setAvailabilityStatus(driver.getAvailabilityStatus());
        if (driver.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE)) {
            if (driver.getLatitude() == null || driver.getLongitude() == null)
                throw new YSException("DRV004");
            driverEntity.setLatitude(driver.getLatitude());
            driverEntity.setLongitude(driver.getLongitude());
            driverEntity.setLastLocationUpdate(DateUtil.getCurrentTimestampSQL());
        }
        driverDao.update(driverEntity);

        ResponseDriverDto socketDriver = new ResponseDriverDto();
        socketDriver.setDriverId(driverEntity.getDriverId());
        socketDriver.setAvailabilityStatus(driverEntity.getAvailabilityStatus());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.DRIVER_AVAILABILITY_STATUS.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketDriver));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session: sessions){
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }

        DriversWorkLogEntity driversWorkLogEntity;
        if(driver.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE)){
            DriversWorkLogEntity dBDriversWorkLog = driversWorkLogDao.findByDriverWithNoToDate(driverEntity.getId());
            if(dBDriversWorkLog!=null) {
                dBDriversWorkLog.setToDateTime(DateUtil.getCurrentTimestampSQL());
                driversWorkLogDao.update(dBDriversWorkLog);
            }
            driversWorkLogEntity = new DriversWorkLogEntity();
            driversWorkLogEntity.setFromDateTime(DateUtil.getCurrentTimestampSQL());
            driversWorkLogEntity.setDriver(driverEntity);
            driversWorkLogDao.save(driversWorkLogEntity);
        } else {
            driversWorkLogEntity = driversWorkLogDao.findByDriverWithNoToDate(driverEntity.getId());
            if(driversWorkLogEntity!=null) {
                driversWorkLogEntity.setToDateTime(DateUtil.getCurrentTimestampSQL());
                driversWorkLogDao.update(driversWorkLogEntity);
            }
        }
    }

    @Override
    public void changePassword(HeaderDto headerDto) throws Exception {
        log.info("++++++ Changing User " +headerDto.getDriverId() + " Password ++++++++");
        UserEntity userEntity = userDao.findByDriverId(headerDto.getDriverId());
        if(userEntity==null)
            throw new YSException("USR001");
        GeneralUtil.matchDBPassword(headerDto.getPassword(), userEntity.getPassword());
        if(headerDto.getPassword().equals(headerDto.getNewPassword()))
            throw new YSException("PASS003");
        userEntity.setPassword(GeneralUtil.encryptPassword(headerDto.getNewPassword()));
        userEntity.getDriver().setAvailabilityStatus(AvailabilityStatus.UNAVAILABLE);
        userDao.update(userEntity);
    }

    @Override
    public List<NotificationEntity> getNotification(HeaderDto headerDto) throws Exception {
        log.info("Getting notifications driver: "+headerDto.getDriverId());
        DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
        if(driverEntity==null)
            throw new YSException("DRV001");
        NotifyTo notifyTo = driverEntity.getDriverType().equals(DriverType.FASTR)? NotifyTo.FASTR_DRIVER : NotifyTo.RESTAURANT_DRIVER;
        List<NotificationEntity> notifications = notificationDao.findDriverNotification(notifyTo);
        return notifications;
    }


    @Override
    public void updateDeviceToken(HeaderDto headerDto, UserDeviceEntity userDevice) throws Exception {
        log.info("Updating device token of driver: "+headerDto.getDriverId());
        DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
        if(driverEntity==null)
            throw new YSException("DRV001");
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        /*if(userDeviceEntity==null) {
            userDeviceEntity = new UserDeviceEntity();
            userDeviceEntity.setUser(driverEntity.getUser());
        }*/
        userDeviceEntity.setDeviceToken(userDevice.getDeviceToken());
        userDeviceEntity.setFamily(userDevice.getFamily());
        userDeviceDao.update(userDeviceEntity);
    }

    @Override
    public void updateDriverLocation(HeaderDto headerDto, DriverEntity driver) throws Exception {
        log.info("Updating device token of driver: " + headerDto.getDriverId());
        DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
        if (driverEntity == null)
            throw new YSException("DRV001");
        if (driver.getLatitude() == null || driver.getLongitude() == null)
            throw new YSException("DRV004");
        driverEntity.setLastLocationUpdate(DateUtil.getCurrentTimestampSQL());
        driverEntity.setLatitude(driver.getLatitude());
        driverEntity.setLongitude(driver.getLongitude());
        driverEntity.setStreetAddress(driver.getStreetAddress());

        DriverEntity socketDriver = new DriverEntity();
        socketDriver.setId(driverEntity.getId());
        socketDriver.setLatitude(driverEntity.getLatitude());
        socketDriver.setLongitude(driverEntity.getLongitude());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.DRIVER_AVAILABILITY_STATUS.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketDriver));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session: sessions){
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }

        driverDao.update(driverEntity);
    }


    @Override
    public OrderEntity getOrderDetails(HeaderDto headerDto) throws Exception {
        log.info("Getting order detail of: " + headerDto.getId());
        OrderEntity orderEntity = orderDao.findById(headerDto.getId());
        if (orderEntity == null)
            throw new YSException("ORD004");
        if (!orderEntity.getDriver().getDriverId().equals(headerDto.getDriverId())) {
            //throw new YSException("DRV010");
        }

        orderEntity.setDeliveryCharge(orderEntity.getDeliveryCharge() == null ? BigDecimal.ZERO : orderEntity.getDeliveryCharge());
        if (orderEntity.getDeliveryCharge() == null)
            orderEntity.setDeliveryCharge(BigDecimal.ZERO);
        if (orderEntity.getPrioritized() == null)
            orderEntity.setPrioritized(1);
        if (orderEntity.getOrderPriority() == null)
            orderEntity.setOrderPriority(1);
        if (orderEntity.getCustomersNoteToDriver() == null)
            orderEntity.setCustomersNoteToDriver("");
        if (orderEntity.getOrderDescription() == null)
            orderEntity.setOrderDescription("");
        if (orderEntity.getPaymentMode().equals(PaymentMode.CARD))
            orderEntity.setPaymentMode(PaymentMode.ONLINE);
        Long elpasedTime = 0L;
        for (OrdersDriverEntity ordersDriverEntity : orderEntity.getOrdersDrivers()) {
            if (ordersDriverEntity.getDriver().getId().equals(orderEntity.getDriver().getId())) {
                elpasedTime = DateUtil.calculateTimeInMinute(ordersDriverEntity.getAssignedDate(), DateUtil.getCurrentTimestampSQL());
                break;
            }
        }
        orderEntity.setElapsedTimeInMin(elpasedTime.intValue());
        //calculate estimated time of delivery
        if (orderEntity.getDriver() != null) {
            BigDecimal totalPreviousOrderEtd = driverDao.getDriversPreviousOrderTotalEtd(orderEntity.getDriver().getDriverId(), orderEntity.getId());
            for (OrdersDriverEntity ordersDriverEntity : orderEntity.getOrdersDrivers()) {
                if (ordersDriverEntity.getDriverStatus().equals(OrdersDriverStatus.RUNNING))
                    if (ordersDriverEntity.getAssignedDate() != null && orderEntity.getEstimatedDeliveryTime() != null)
                        orderEntity.setEstimatedTimeOfDelivery(new Timestamp(ordersDriverEntity.getAssignedDate().getTime() + (orderEntity.getEstimatedDeliveryTime().add(totalPreviousOrderEtd)).multiply(new BigDecimal(60 * 1000)).longValue()));
            }
        }
        String fields = "orderId,orderDisplayId,estimatedTimeOfDelivery,orderDescription,orderDeliveryDate,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,totalBillAmount,grandTotal,elapsedTimeInMin";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("customer", "customerId,user");
        assoc.put("store", "storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand,contactNo");
        assoc.put("customersArea", "customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand", "storeBrandId,brandName,brandLogo");
        subAssoc.put("user", "firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area", "areaId,areaName,latitude,longitude,street");
        return (OrderEntity) ReturnJsonUtil.getJsonObject(orderEntity, fields, assoc, subAssoc);
    }

    @Override
    public void forgotPassword(HeaderDto headerDto) throws Exception {
        log.info("Forgotting password of driver");
        UserEntity userEntity = userDao.findDriverByUserName(headerDto.getUsername());
        if (userEntity == null)
            throw new YSException("USR001");
        String verificationCode = MessageBundle.generateTokenString() + "_" + System.currentTimeMillis();
        userEntity.setVerificationCode(verificationCode);
        userDao.update(userEntity);

        String hostName = getServerUrl();
        String url = hostName + "/assistance/reset_password?key=" + verificationCode;
        log.info("Sending mail to " + headerDto.getUsername() + " with password reset url: " + url);

        String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Forgot your Password!";
        String body = EmailMsg.resetForgotPassword(url, userEntity.getFirstName() + " " + userEntity.getLastName(), getServerUrl());

        sendMail(userEntity.getEmailAddress(), body, subject);
    }

    @Override
    public void logoutDriver(HeaderDto headerDto) throws Exception {
        log.info("Updating driver status of id: " + headerDto.getDriverId());
        DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
        if (driverEntity == null)
            throw new YSException("DRV001");

        if (driverDao.getDriversOrderCount(driverEntity.getDriverId()) > 0)
            throw new YSException("DRV009");

        driverEntity.setAvailabilityStatus(AvailabilityStatus.UNAVAILABLE);
        DriverEntity socketDriver = new DriverEntity();
        socketDriver.setId(driverEntity.getId());
        socketDriver.setAvailabilityStatus(driverEntity.getAvailabilityStatus());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.DRIVER_AVAILABILITY_STATUS.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketDriver));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session : sessions) {
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }
        driverDao.update(driverEntity);
        DriversWorkLogEntity driversWorkLogEntity = driversWorkLogDao.findByDriverWithNoToDate(driverEntity.getId());
        if (driversWorkLogEntity != null) {
            driversWorkLogEntity.setToDateTime(DateUtil.getCurrentTimestampSQL());
            driversWorkLogDao.update(driversWorkLogEntity);
        }
    }
}
