package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.dao.inf.UserDeviceDao;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.service.inf.UserService;
import com.yetistep.anyorder.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserServiceImpl extends AbstractManager implements UserService {

    private static final Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    UserDao userDaoService;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    UserDeviceDao userDeviceDao;

    /* Used For Only Manager and Account Registration */
    public void saveUser(UserEntity user) throws Exception {
        user.getCustomer().setUser(user);
        userDaoService.save(user);
    }

    @Override
    public List<UserEntity> getAllUsers() throws Exception {
        return userDaoService.getAllManager();
    }

    @Override
    public Boolean checkUserExistence(HeaderDto headerDto) throws Exception {
        log.info("++++++++ Checking User Existence +++++++++++++++");

        UserEntity userEntity = userDaoService.findByUserName(headerDto.getUsername());
        if (userEntity != null) {
            throw new YSException("USR002");
        }
        return true;
    }

    @Override
    public String verifyEmailAddress(HeaderDto headerDto) throws Exception {
        String successMsg = "";
        log.info("+++++++++++ Verifying Email +++++++++++");

        //Update Verification UserStatus True
        updateVerificationStatus(headerDto.getVerificationCode());
        //Create Password
//        resetForgotPassword(headerDto.getVerificationCode(), headerDto.getPassword());

        successMsg = "Your email has been verified successfully";
        return successMsg;
    }

    @Override
    public void changePassword(HeaderDto headerDto) throws Exception {

        log.info("++++++ Changing User " + headerDto.getId() + " Password ++++++++");
        UserEntity user = userDaoService.findByUserId(headerDto.getId());
        GeneralUtil.matchDBPassword(headerDto.getPassword(), user.getPassword());
        if (headerDto.getPassword().equals(headerDto.getNewPassword())) {
            throw new YSException("PASS002");
        }
        user.setPassword(GeneralUtil.encryptPassword(headerDto.getNewPassword()));
        userDaoService.update(user);

        //send push notification to driver
        if (user.getStoreManager() != null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(user.getStoreManager().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("redirect", PushNotificationRedirect.LOGOUT.toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.STORE_MANAGER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        } else if (user.getDriver() != null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(user.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("redirect", PushNotificationRedirect.LOGOUT.toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.STORE_MANAGER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
    }

    private void setPasswordForUser(String code, String password) throws Exception {
        log.info("Setting password for user");
        UserEntity userEntity = userDaoService.findByVerificationCode(code);
        if (userEntity == null)
            throw new YSException("USR002");

        userEntity.setPassword(GeneralUtil.encryptPassword(password.trim()));
        userEntity.setVerificationCode("");
        userEntity.setVerifiedStatus(true);
        userEntity.setUserStatus(UserStatus.ACTIVE);
        userDaoService.update(userEntity);
    }

    private void resetForgotPassword(String code, String password) throws Exception {
        log.info("reset forgot password");
        UserEntity userEntity = userDaoService.findByVerificationCode(code);
        if (userEntity == null)
            throw new YSException("USR002");

        userEntity.setPassword(GeneralUtil.encryptPassword(password.trim()));
        userEntity.setVerificationCode("");
        userDaoService.update(userEntity);
        //send push notification to driver
        if (userEntity.getStoreManager() != null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(userEntity.getStoreManager().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("redirect", PushNotificationRedirect.LOGOUT.toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.STORE_MANAGER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        } else if (userEntity.getDriver() != null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(userEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("redirect", PushNotificationRedirect.LOGOUT.toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.STORE_MANAGER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
    }

    private void updateVerificationStatus(String code) throws Exception {
        log.info("Updating verification status");
        UserEntity userEntity = userDaoService.findByVerificationCode(code);
        if (userEntity == null)
            throw new YSException("USR002");
        userEntity.setVerifiedStatus(true);
        userEntity.setUserStatus(UserStatus.ACTIVE);
        userEntity.setVerificationCode("");
        userDaoService.update(userEntity);

    }

    private void resendVerificationMail(String userName) throws Exception {
        log.info("Resending verification email");
        String verificationCode = MessageBundle.generateTokenString() + "_" + System.currentTimeMillis();

        UserEntity userEntity = userDaoService.findByUserName(userName);
        if (userEntity == null)
            throw new YSException("USR002");

        if (userEntity.getVerifiedStatus())
            throw new YSException("USR006");

        userEntity.setVerificationCode(verificationCode);
        userDaoService.update(userEntity);

         /* Send Email */
        //if (userEntity.getUserRole().equals(UserRole.ROLE_MERCHANT)) {
            String hostName = getServerUrl();
            String url = hostName + "/assistance/create_password?key=" + verificationCode;
            log.debug("Re-sending mail to " + userName + " with verify_url: " + url);
            String body = EmailMsg.createPasswordForNewUser(url, userEntity.getFirstName() + " " + userEntity.getLastName(), userEntity.getUsername(), getServerUrl());
            String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": You have been added as Merchant ";
            sendMail(userEntity.getUsername(), body, subject);
        //}
    }

    private void forgotPassword(String userName) throws Exception {
        log.info("Performing forgot password of user: " + userName);
        String verificationCode = MessageBundle.generateTokenString() + "_" + System.currentTimeMillis();
        //Checking User and Setting Verification Code
        UserEntity user = userDaoService.findByUserName(userName);
        if (user == null)
            throw new YSException("USR001");

        user.setVerificationCode(verificationCode);
        userDaoService.update(user);

        String hostName = getServerUrl();
        String url = hostName + "/assistance/reset_password?key=" + verificationCode;
        log.info("Sending mail to " + userName + " with password reset url: " + url);

        String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Forgot your Password!";
        String body = EmailMsg.resetForgotPassword(url, user.getFirstName() + " " + user.getLastName(), getServerUrl());

        sendMail(userName, body, subject);

    }

    @Override
    public String performPasswordAction(HeaderDto headerDto, PasswordActionType actionType) throws Exception {
        String successMsg = "";
        if (actionType.equals(PasswordActionType.NEW)) {
            log.info("+++++++++++ Creating Password +++++++++++");
            //set password and make user verified and active
            setPasswordForUser(headerDto.getVerificationCode(), headerDto.getPassword());

            successMsg = "Your password has been created successfully";

        } else if (actionType.equals(PasswordActionType.RESET)) {
            log.info("+++++++++++ Resetting password +++++++++++");
            //Resetting Password
            resetForgotPassword(headerDto.getVerificationCode(), headerDto.getPassword());
            successMsg = "Your password has been reset successfully";

        } else if (actionType.equals(PasswordActionType.FORGOT)) {
            log.info("+++++++++++Performing Forgot password +++++++++++");

            String email = headerDto.getUsername();
            forgotPassword(email);
            successMsg = "Successfully sent email to " + email + " to reset the password.";
        } else if (actionType.equals(PasswordActionType.RESEND)) {
            log.info("++++++++++++ Resending password reset email +++++++++++");

            resendVerificationMail(headerDto.getUsername());
            successMsg = "Successfully resend verification email to " + headerDto.getUsername();
        }
        return successMsg;
    }

}
