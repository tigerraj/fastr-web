package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.enums.PreferenceType;
import com.yetistep.anyorder.enums.UserStatus;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.service.inf.CSRService;
import com.yetistep.anyorder.service.inf.SystemPropertyService;
import com.yetistep.anyorder.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 9:55 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CSRServiceImpl extends AbstractManager implements CSRService {

    Logger log = Logger.getLogger(CSRServiceImpl.class);

    @Autowired
    UserDao userDao;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Override
    public UserEntity getUserDetail(String userId) throws Exception {
        log.info("----- Getting user detail. -----");
        UserEntity userEntity = userDao.findByUserId(userId);
        if (userEntity == null) {
            log.error("----- User does not exist. -----");
            throw new YSException("USR001");
        }
        String field = "userId,userRole,username,firstName,lastName,mobileNumber,emailAddress,profileImage,userStatus";
        return (UserEntity) ReturnJsonUtil.getJsonObject(userEntity, field);
    }

    @Override
    public UserEntity updateUser(UserEntity user) throws Exception {
        log.info("----- Updating user. -----");
        UserEntity userEntity = userDao.findByUserId(user.getUserId());
        if (userEntity == null) {
            log.error("----- User does not exist. -----");
            throw new YSException("USR001");
        }
        if (user.getFirstName() != null) {
            userEntity.setFirstName(user.getFirstName());
        }
        if (user.getLastName() != null) {
            userEntity.setLastName(user.getLastName());
        }
        if (user.getMobileNumber() != null) {
            userEntity.setMobileNumber(user.getMobileNumber());
        }
        if (user.getEmailAddress() != null) {
            userEntity.setEmailAddress(user.getEmailAddress());
        }

        String profileImage = user.getProfileImage();

        userDao.update(userEntity);

        if (profileImage != null && !profileImage.isEmpty() && profileImage != userEntity.getProfileImage()) {
            log.info("Deleting Profile Image of user from S3 Bucket ");
            AmazonUtil.deleteFileFromBucket(AmazonUtil.getAmazonS3Key(userEntity.getProfileImage()));

            log.info("Uploading Profile Image of user to S3 Bucket ");

            String dir = MessageBundle.separateString("/", "Users", "user_" + userEntity.getId());
            boolean isLocal = MessageBundle.isLocalHost();
            String imageName = "pimg" + (isLocal ? "_tmp_" : "_") + userEntity.getId() + System.currentTimeMillis();
            String s3Path = GeneralUtil.saveImageToBucket(user.getProfileImage(), imageName, dir, true);
            userEntity.setProfileImage(s3Path);
            userDao.update(userEntity);
        }

        String field = "userId,userRole,username,firstName,lastName,mobileNumber,emailAddress,profileImage,userStatus";
        return (UserEntity) ReturnJsonUtil.getJsonObject(userEntity, field);
    }

    @Override
    public UserEntity changeUserStatus(UserEntity user) throws Exception {
        log.info("----- Changing user status. -----");
        UserEntity userEntity = userDao.findByUserId(user.getUserId());
        if (userEntity == null) {
            log.error("----- User does not exist. -----");
            throw new YSException("USR001");
        }
        if (user.getUserStatus() != null && !user.getUserStatus().equals(userEntity.getUserStatus())) {
            userEntity.setUserStatus(user.getUserStatus());
            userDao.update(userEntity);

            log.info("Sending mail to " + userEntity.getEmailAddress());

            String body = EmailMsg.notificationEmailUserStatus(userEntity, getServerUrl());
            String subject = systemPropertyService.readPrefValue(PreferenceType.APPLICATION_NAME) + ": Your account has been";
            if (userEntity.getUserStatus().equals(UserStatus.ACTIVE)) {
                subject += " re-activated.";
            } else if (userEntity.getUserStatus().equals(UserStatus.INACTIVE)) {
                subject += " deactivated.";
            }

            sendMail(userEntity.getEmailAddress(), body, subject);
        }
        String field = "userId,userRole,username,firstName,lastName,mobileNumber,emailAddress,profileImage,userStatus";
        return (UserEntity) ReturnJsonUtil.getJsonObject(userEntity, field);
    }
}
