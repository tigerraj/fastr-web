package com.yetistep.anyorder.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.aspect.annotations.CreateOrderValidate;
import com.yetistep.anyorder.aspect.annotations.MobileService;
import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.PaginationDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.*;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.inf.*;
import com.yetistep.anyorder.util.*;
import com.yetistep.anyorder.webrtc.pojo.ResponseOrderDto;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@MobileService
public class StoreManagerMobileServiceImpl extends AbstractManager implements StoreManagerMobileService {

    private static final Logger log = Logger.getLogger(StoreManagerMobileServiceImpl.class);

    @Autowired
    AreaDao areaDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    UserDao userDao;

    @Autowired
    DriverDao driverDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    CustomersAreaDao customersAreaDao;

    @Autowired
    StoreManagerDao storeManagerDao;

    @Autowired
    DriverService driverService;

    @Autowired
    SystemAlgorithmService systemAlgorithmService;

    @Autowired
    SystemPropertyService systemPropertyService;

    @Autowired
    UserDeviceDao userDeviceDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    WebRTCService webRTCService;

    private static void parseAreaTree(List<AreaEntity> parentAreas, List<StoresAreaEntity> storesAreas) {
        if (parentAreas != null) {
            for (AreaEntity area : parentAreas) {
                List<AreaEntity> areas = new ArrayList<>();
                for (StoresAreaEntity storesArea : storesAreas) {
                    if (storesArea.getArea().getStatus().equals(Status.ACTIVE) && storesArea.getArea().getParent() != null) {
                        for (AreaEntity child : area.getChild()) {
                            if (child.getId().equals(storesArea.getArea().getParent().getId()))
                                areas.add(storesArea.getArea());
                        }
                    }
                }
                area.setChild(areas);
                parseAreaTree(area.getChild(), storesAreas);
            }
        }
    }

    private static void parseAreaTree(AreaEntity parentAreas, List<Integer> ids) {
        List<AreaEntity> areas = new ArrayList<>();
        for (AreaEntity area : parentAreas.getChild()) {
            if (ids.contains(area.getId())) {
                areas.add(area);
                parseAreaTree(area, ids);
            }
        }
        parentAreas.setChild(areas);
    }

    @Override
    public UserEntity storeManagerLogin(HeaderDto headerDto, UserDeviceEntity userDevice) throws Exception {
        log.info("Logging store manager: " + headerDto.getUsername());
        String userName = headerDto.getUsername();
        String password = headerDto.getPassword();
        ValidationUtil.validateString(userDevice.getFamily(), "Invalid Family");
        ValidationUtil.validateString(userDevice.getDeviceUuid(), "Device uuid is required");
        String accessToken = UUID.randomUUID().toString();
        userDevice.setAccessToken(MD5Hash.getMD5(accessToken));
        //UserEntity storeManager = userDao.findStoreManagerByUsernameMobile(userName);
        UserEntity storeManager = userDao.findStoreManagerByUsernameMobileNo(userName);
        if (storeManager == null || !storeManager.getUserRole().equals(UserRole.ROLE_STORE_MANAGER) || storeManager.getStoreManager().getStore() == null) {
            throw new YSException("USR001");
        }
        if (!storeManager.getUserStatus().equals(UserStatus.ACTIVE)) {
            throw new YSException("USR002");
        }

        log.info("status of the store is: "+storeManager.getStoreManager().getStore().getStatus());
        log.info("status of the brand is: "+storeManager.getStoreManager().getStore().getStoreBrand().getStatus());
        log.info("user status of the merchant is: "+storeManager.getStoreManager().getStore().getStoreBrand().getMerchant().getUser().getUserStatus());

        if (!isVerifiableStore(storeManager.getStoreManager().getStore(), false)
                || !storeManager.getStoreManager().getStore().getStatus().equals(Status.ACTIVE)
                || !storeManager.getStoreManager().getStore().getStoreBrand().getStatus().equals(Status.ACTIVE)
                || !storeManager.getStoreManager().getStore().getStoreBrand().getMerchant().getUser().getUserStatus().equals(UserStatus.ACTIVE)) {
            throw new YSException("STR004");
        }
        GeneralUtil.matchDBPassword(password, storeManager.getPassword());
        List<UserDeviceEntity> dbUserDevices = storeManager.getUserDevices();
        if (dbUserDevices.size() > 0) {
            List<String> uuidList = new ArrayList<>();
            for (UserDeviceEntity dbUserDevice : dbUserDevices) {
                uuidList.add(dbUserDevice.getDeviceUuid());
                if (dbUserDevice.getDeviceUuid().equals(userDevice.getDeviceUuid())) {
                    dbUserDevice.setAccessToken(userDevice.getAccessToken());
                    if (userDevice.getDeviceToken() != null)
                        dbUserDevice.setDeviceToken(userDevice.getDeviceToken());
                }
            }
            if (!uuidList.contains(userDevice.getDeviceUuid())) {
                userDevice.setUser(storeManager);
                dbUserDevices.add(userDevice);
            }
        } else {
            userDevice.setUser(storeManager);
            dbUserDevices.add(userDevice);
        }
        storeManager.setUserDevices(dbUserDevices);
        userDao.update(storeManager);
        String fields = "userId,firstName,lastName,mobileNumber,emailAddress,accessToken";
        storeManager.setAccessToken(accessToken);
        return ((UserEntity) ReturnJsonUtil.getJsonObject(storeManager, fields));
    }

    @Override
    public StoreEntity getSpecialPrivilegeInfo(String accessToken) throws Exception {
        StoreEntity storeBrand = ((UserDeviceEntity) userDeviceDao.findByAccessToken(MD5Hash.getMD5(accessToken))).getUser().getStoreManager().getStore();
        StoreEntity retStore = new StoreEntity();
        retStore.setAllowStoresDriver(storeBrand.getAllowStoresDriver());
        retStore.setAllowUnknownAreaServing(storeBrand.getAllowUnknownAreaServing());
        return retStore;
    }

    @Override
    @CreateOrderValidate
    public OrderEntity createOrder(HeaderDto headerDto, RequestJsonDto requestJson) throws Exception {
        OrderEntity orderEntity = requestJson.getOrder();
        String driverId = requestJson.getDriverId();
        if(requestJson.getSaveAndAssign()==null)
            requestJson.setSaveAndAssign(false);

        orderEntity.setAssignDriver(requestJson.getSaveAndAssign());

        if(orderEntity.getCustomer().getUser()!=null)
            orderEntity.getCustomer().getUser().setUserRole(UserRole.ROLE_CUSTOMER);
        else{
            UserEntity user = new UserEntity();
            user.setUserRole(UserRole.ROLE_CUSTOMER);
            orderEntity.getCustomer().setUser(user);
        }

        log.info("+++++++ assigning stores own driver to the order +++++++++");
        if(driverId!=null){
            DriverEntity driver = driverDao.findById(driverId);
            if (driver==null)
                throw new YSException("DRV001");

            OrdersDriverEntity ordersDriver = new OrdersDriverEntity();
            ordersDriver.setDriver(driver);
            ordersDriver.setDriverStatus(OrdersDriverStatus.RUNNING);
            ordersDriver.setOrder(orderEntity);
            ordersDriver.setAssignedDate(new Timestamp(System.currentTimeMillis()));
            orderEntity.setOrdersDrivers(Collections.singletonList(ordersDriver));
            orderEntity.setDriverType(DriverType.RESTAURANT);
            orderEntity.setDriver(driver);
            orderEntity.setAssignDriver(true);
        }

        if(orderEntity.getOrderDeliveryDate()!=null) {
            if(orderEntity.getOrderDeliveryDate().compareTo(new Date()) < 0)
                throw new YSException("API006");
            long deliveryDateUtc = DateUtil.converLocalTimeToUtcTime(orderEntity.getOrderDeliveryDate().getTime());
            orderEntity.setOrderDeliveryDate(new Timestamp(deliveryDateUtc));
        }

        if(orderEntity.getPickupDate()!=null) {
            long pickupDateUtc = DateUtil.converLocalTimeToUtcTime(orderEntity.getPickupDate().getTime());
            orderEntity.setPickupDate(new Timestamp(pickupDateUtc));
        }

        //get store manager and store by users access token
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        orderEntity.setStoreManager(userDeviceEntity.getUser().getStoreManager());
        orderEntity.setStore(userDeviceEntity.getUser().getStoreManager().getStore());

        //set new customersArea to order
        if(orderEntity.getCustomer().getCustomersArea().get(0).getArea()!=null){
            AreaEntity area = areaDao.findByAreaId(orderEntity.getCustomer().getCustomersArea().get(0).getArea().getAreaId());
            orderEntity.getCustomer().getCustomersArea().get(0).setArea(area);
        }

        //set existing customersArea to order
        if(orderEntity.getCustomer().getCustomersArea().get(0).getCustomersAreaId()!=null){
            CustomersAreaEntity customersArea = customersAreaDao.findByCustomersAreaId(orderEntity.getCustomer().getCustomersArea().get(0).getCustomersAreaId());
            orderEntity.setCustomersArea(customersArea);
        }


        //set existing customer to order
        if(orderEntity.getCustomer().getCustomerId()!=null) {
            CustomerEntity customer = customerDao.findByCustomerId(orderEntity.getCustomer().getCustomerId());
            if (orderEntity.getCustomer().getUser() != null && orderEntity.getCustomer().getUser().getFirstName() != null) {
                customer.getUser().setFirstName(orderEntity.getCustomer().getUser().getFirstName());
            }
            if(orderEntity.getCustomer().getCustomersArea().get(0).getCustomersAreaId()==null){
                CustomersAreaEntity customersArea = new CustomersAreaEntity();
                if(orderEntity.getCustomer().getCustomersArea().get(0).getArea()!=null){
                    AreaEntity area = areaDao.findByAreaId(orderEntity.getCustomer().getCustomersArea().get(0).getArea().getAreaId());
                    customersArea.setArea(area);
                } else {
                    customersArea.setLatitude(orderEntity.getCustomer().getCustomersArea().get(0).getLatitude());
                    customersArea.setLongitude(orderEntity.getCustomer().getCustomersArea().get(0).getLongitude());
                    customersArea.setAdditionalNote(orderEntity.getCustomer().getCustomersArea().get(0).getAdditionalNote());
                    customersArea.setStreet(orderEntity.getCustomer().getCustomersArea().get(0).getStreet());
                }
                customersArea.setStreet(orderEntity.getCustomer().getCustomersArea().get(0).getStreet());
                customersArea.setCustomer(customer);
                customersAreaDao.save(customersArea);
                orderEntity.setCustomersArea(customersArea);
            }
            orderEntity.setCustomer(customer);

        }  else {
            //set new customer to order
            orderEntity.getCustomer().getCustomersArea().get(0).setCustomer(orderEntity.getCustomer());
            orderEntity.setCustomersArea(orderEntity.getCustomer().getCustomersArea().get(0));
        }

        if(orderEntity.getDriver()!=null)
            updateETDAndDeliveryTimeOfOrder(orderEntity, orderEntity.getDriver());

        //set default status
        orderEntity.setOrderStatus(OrderStatus.ORDER_PLACED);

        if(requestJson.getSaveAndAssign() && systemPropertyService.readPrefValue(PreferenceType.ASSIGN_DRIVER_AUTOMATICALLY).equals("1") && driverId==null){
            orderEntity.setAssignDriver(requestJson.getSaveAndAssign());
            //assign driver to order
            systemAlgorithmService.assignDriver(orderEntity, true);
            if(orderEntity.getOrdersDrivers().size()==0){
                throw new YSException("DRV001");
            }
            orderEntity.setAssignDriver(true);
        } else if(requestJson.getSaveAndAssign() && systemPropertyService.readPrefValue(PreferenceType.ASSIGN_DRIVER_AUTOMATICALLY).equals("0") && driverId==null){
            orderEntity.setAssignDriver(true);
            orderEntity.setOrderStatus(OrderStatus.ORDER_PLACED);
        }

        if(requestJson.getSaveAndAssign() && driverId != null){
            DriverEntity driverEntity = driverDao.findById(requestJson.getDriverId());
            if(driverEntity==null)
                throw new YSException("DRV001");
            orderEntity.setAssignDriver(requestJson.getSaveAndAssign());
            orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);
            orderEntity.setDriver(driverEntity);
        } else if(!requestJson.getSaveAndAssign()) {
            orderEntity.setAssignDriver(false);
            orderEntity.setOrderStatus(OrderStatus.ORDER_PLACED);
        }
        orderEntity.setOrderOriginType(OrderOriginType.RESTAURANT);

        //Rate plan of order
        if (orderEntity.getCustomersArea().getLatitude() != null) {
            orderEntity.setOrderPlan(OrderPlan.OUTSIDE_SERVICE_AREA_PLAN);
        } else {
            if (orderEntity.getCustomersArea().getArea() != null) {
                if (orderEntity.getStore().getIsDistanceBaseRatePlan()) {
                    orderEntity.setOrderPlan(OrderPlan.DISTANCE_BASED_PLAN);
                } else {
                    orderEntity.setOrderPlan(OrderPlan.SERVICE_AREA_PLAN);
                }
            }
        }
        if(orderEntity.getOrderDeliveryDate()==null) {
            final Calendar calendar = GregorianCalendar.getInstance();
            calendar.add(Calendar.HOUR,1);
            orderEntity.setOrderDeliveryDate(calendar.getTime());
        }
        orderDao.save(orderEntity);

        //send push notification to driver
        if(orderEntity.getDriver()!=null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN001", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }

        SMSUtil.sendSMS("Please go to this url " + getServerUrl() + "/customer/order?orderId=" + orderEntity.getOrderId() + " to get the updates of your order.", orderEntity.getCustomer().getUser().getMobileNumber(),
                systemPropertyService.readPrefValue(PreferenceType.SMS_COUNTRY_CODE), systemPropertyService.readPrefValue(PreferenceType.SMS_PROVIDER));

        ResponseOrderDto socketOrder = new ResponseOrderDto();
        socketOrder.setOrderId(orderEntity.getOrderId());
        socketOrder.setOrderStatus(orderEntity.getOrderStatus());
        if(orderEntity.getDriver()!=null)
            socketOrder.setDriverId(orderEntity.getDriver().getDriverId());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.NEW_ORDER.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketOrder));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session: sessions){
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }

        //return created order
        String fields = "id,orderId,orderDisplayId,assignDriver,orderDeliveryDate,pickupDate,orderDescription,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,driver,store,customer,customersArea";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand");
        assoc.put("driver","driverId,experienceLevel,user");
        assoc.put("customer","customerId,user");
        assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area","areaId,areaName,latitude,longitude,street");
        return  (OrderEntity) ReturnJsonUtil.getJsonObject(orderEntity, fields, assoc, subAssoc);
    }

    private void updateETDAndDeliveryTimeOfOrder(OrderEntity orderEntity, DriverEntity driverEntity) throws Exception {
        //update delivery time of the order
        AreaEntity area;
        if(orderEntity.getCustomersArea().getArea()!=null) {
            area = orderEntity.getCustomersArea().getArea();
        } else  {
            area  = new AreaEntity();
            area.setLatitude(orderEntity.getCustomersArea().getLatitude());
            area.setLongitude(orderEntity.getCustomersArea().getLongitude());
        }

        Integer previousOrderTotalEtd = driverDao.getDriversTotalEtd(driverEntity.getDriverId()).intValue();
        String storeAddress[] = {GeoCodingUtil.getLatLong(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude())};
        String customerAddress[] = {GeoCodingUtil.getLatLong(area.getLatitude(), area.getLongitude())};
        String driverAddress[] = {GeoCodingUtil.getLatLong(driverEntity.getLatitude(), driverEntity.getLongitude())};
        Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
        BigDecimal driverToRestaurantTime = new BigDecimal(GeoCodingUtil.getListOfDistances(driverAddress, storeAddress, timePerKm).get("time")/60);
        BigDecimal restaurantToCustomerTime = new BigDecimal(GeoCodingUtil.getListOfDistances(storeAddress, customerAddress, timePerKm).get("time")/60);
        BigDecimal timeAtStore = new BigDecimal(systemPropertyService.readPrefValue(PreferenceType.DRIVERS_TIME_AT_STORE));
        BigDecimal timeAtCustomer = new BigDecimal(systemPropertyService.readPrefValue(PreferenceType.DRIVERS_TIME_AT_CUSTOMER));
        BigDecimal etd  = driverToRestaurantTime.add(restaurantToCustomerTime).add(timeAtCustomer).add(timeAtStore);
        orderEntity.setEstimatedDeliveryTime(etd);
    }

    //currently not used
    @Override
    public List<UserEntity> getCustomersByMobileNumber(HeaderDto headerDto, RequestJsonDto requestJson) throws Exception {
        List<UserEntity> users = userDao.findCustomersByMobileNumber(requestJson.getMobileNumber());
        List<UserEntity> userEntities = new ArrayList<>();
        String fields = "firstName,lastName,mobileNumber,customer";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("customer", "customerId");
        for (UserEntity user: users) {
            userEntities.add((UserEntity) ReturnJsonUtil.getJsonObject(user, fields, assoc));
        }
        return userEntities;
    }

    @Override
    public CustomerEntity getCustomersInfo(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception {
        UserEntity user = userDao.findByMobileNumberAndRole(requestJsonDto.getMobileNumber(), UserRole.ROLE_CUSTOMER);
        if(user==null){
            return new CustomerEntity();
        }
        CustomerEntity customerEntity =  user.getCustomer();
        String fields = "customerId, user, customersArea";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("customersArea", "customersAreaId,street,givenLocation,area");
        assoc.put("user", "firstName,lastName");
        subAssoc.put("area", "areaId,areaName,parent");
        subAssoc.put("parent", "areaId,areaName,parent");
        CustomerEntity customer =  (CustomerEntity) ReturnJsonUtil.getJsonObject(customerEntity, fields, assoc, subAssoc);

        //remove unknown areas
        List<CustomersAreaEntity> removeCustomersArea = new ArrayList<>();
        for (CustomersAreaEntity customersAreaEntity: customerEntity.getCustomersArea()){
            if (customersAreaEntity.getArea() == null){
                removeCustomersArea.add(customersAreaEntity);
            }
        }

        customer.getCustomersArea().removeAll(removeCustomersArea);
        return customer;
    }

    @Override
    public List<AreaEntity> getArea(HeaderDto headerDto) throws Exception{
        List<AreaEntity> parentAreas = areaDao.findParentAreas();

        List<AreaEntity> areas = new ArrayList<>();

        String fields = "areaId,areaName,child";
        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();

        assoc.put("child", "areaId,areaName,child");
        subAssoc.put("child", "areaId,areaName,child");
        for(AreaEntity area : parentAreas){
            areas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area,fields,assoc,subAssoc));
        }
        return areas;
    }

    @Override
    public List<DriverEntity> getStoresDrivers(HeaderDto headerDto) throws Exception {

        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if (userDeviceEntity == null)
            throw new YSException("STRMGR001");
        List<DriverEntity> storesDrivers = userDeviceEntity.getUser().getStoreManager().getStore().getDrivers();

        List<DriverEntity> drivers = new ArrayList<>();
        String fields = "driverId,experienceLevel,previousOrderCount,user";
        Map<String, String> assoc = new HashMap<>();

        assoc.put("user", "firstName,lastName");
        for(DriverEntity driverEntity : storesDrivers){
            driverEntity.setPreviousOrderCount(driverDao.getDriversOrderCount(driverEntity.getDriverId()));
            if (!driverEntity.getAvailabilityStatus().equals(AvailabilityStatus.UNAVAILABLE))
                drivers.add((DriverEntity) ReturnJsonUtil.getJsonObject(driverEntity,fields,assoc));
        }
        return drivers;
    }

    @Override
    public OrderEntity getOrderDetail(HeaderDto headerDto) throws Exception {
        OrderEntity order = orderDao.findById(headerDto.getOrderId());

            if(order==null)
                throw new YSException("ORD004");

            if(order.getDriver()!=null){
                BigDecimal totalPreviousOrderEtd = driverDao.getDriversPreviousOrderTotalEtd(order.getDriver().getDriverId(), order.getId());
                for (OrdersDriverEntity ordersDriverEntity: order.getOrdersDrivers()){
                    if (ordersDriverEntity.getDriverStatus().equals(OrdersDriverStatus.RUNNING))
                        if(order.getEstimatedDeliveryTime()!=null)
                            order.setEstimatedTimeOfDelivery(new Timestamp(ordersDriverEntity.getAssignedDate().getTime()+(order.getEstimatedDeliveryTime().add(totalPreviousOrderEtd)).multiply(new BigDecimal(60*1000)).longValue()));
                }
            }

            String fields = "orderId,orderDisplayId,assignDriver,orderDescription,orderDeliveryDate,pickupDate,estimatedTimeOfDelivery,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,driver,store,customer,customersArea";

            Map<String, String> assoc = new HashMap<>();
            Map<String, String> subAssoc = new HashMap<>();
            assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand");
            assoc.put("driver","driverId,experienceLevel,user");
            assoc.put("customer","customerId,user");
            assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
            subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
            subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
            subAssoc.put("area","areaId,areaName,latitude,longitude,street");
        return  (OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc);

    }

    @Override
    public OrderEntity assignDriverToOrder(HeaderDto headerDto) throws Exception {
        OrderEntity orderEntity = orderDao.findById(headerDto.getOrderId());

        if(headerDto.getDriverId()!=null) {
            DriverEntity driverEntity = driverDao.findById(headerDto.getDriverId());
            if (driverEntity==null)
                throw new YSException("DRV001");
            if (!driverEntity.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE))
                throw new YSException("DRV008");
            if(orderEntity==null)
                throw new YSException("ORD004");

            OrdersDriverEntity ordersDriverEntity = new OrdersDriverEntity();
            ordersDriverEntity.setOrder(orderEntity);
            ordersDriverEntity.setDriver(driverEntity);
            ordersDriverEntity.setDriverStatus(OrdersDriverStatus.RUNNING);
            ordersDriverEntity.setAssignedDate(DateUtil.getCurrentTimestampSQL());

            if(orderEntity.getOrdersDrivers()!=null){
                for (OrdersDriverEntity ordersDriver : orderEntity.getOrdersDrivers()){
                    ordersDriver.setDriverStatus(OrdersDriverStatus.FAILED);
                }
                orderEntity.getOrdersDrivers().add(ordersDriverEntity);
            } else {
                orderEntity.setOrdersDrivers(Collections.singletonList(ordersDriverEntity));
            }

            orderEntity.setDriver(driverEntity);
            orderEntity.setOrderStatus(OrderStatus.ORDER_STARTED);
        } else {
            if( systemPropertyService.readPrefValue(PreferenceType.ASSIGN_DRIVER_AUTOMATICALLY).equals("1") ) {
                systemAlgorithmService.assignDriver(orderEntity, true);
                orderDao.update(orderEntity);
            } else
                orderEntity.setAssignDriver(true);
        }
        orderDao.update(orderEntity);
        if(orderEntity.getDriver()!=null) {
            //send push notification to driver
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN001", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
        //return updated order
        String fields = "orderId,orderDisplayId,assignDriver,orderDescription,orderDeliveryDate,pickupDate,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,driver,store,customer,customersArea";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand");
        assoc.put("driver","driverId,experienceLevel,user");
        assoc.put("customer","customerId,user");
        assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area","areaId,areaName,latitude,longitude,street");
        return  (OrderEntity) ReturnJsonUtil.getJsonObject(orderEntity, fields, assoc, subAssoc);
    }

    @Override
    public RequestJsonDto getTotalNumberOfOrders(HeaderDto headerDto) throws Exception {
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if (userDeviceEntity == null)
            throw new YSException("STRMGR001");
        Integer storeId = userDeviceEntity.getUser().getStoreManager().getStore().getId();
        return orderDao.getNumberOfClassifiedOrders(storeId);
    }

    @Override
    public PaginationDto getStoresOrders(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception {
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if (userDeviceEntity == null)
            throw new YSException("STRMGR001");
        StoreEntity store = userDeviceEntity.getUser().getStoreManager().getStore();
        List<Integer> storeId = new ArrayList<>();
        storeId.add(store.getId());
        Integer totalRows = orderDao.getTotalNumberOfOrdersByStatus(storeId, requestJsonDto.getOrderDisplayStatus());

        PaginationDto paginationDto = new PaginationDto();

        Page page = requestJsonDto.getPage();
        if(page!=null){
            paginationDto.setNumberOfRows(totalRows);
            page.setTotalRows(totalRows);
        }
        List<OrderEntity> orders = orderDao.getOrdersByStatus(storeId, page, requestJsonDto.getOrderDisplayStatus());

        List<OrderEntity> returnOrders = new ArrayList<>();

        String fields = "id,orderId,orderDisplayId,assignDriver,orderDescription,driverType,orderDeliveryDate,pickupDate,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,driver,customer,customersArea";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store","storeId,priority,storeBrand,area");
        assoc.put("customer","customerId,user");
        assoc.put("driver","driverId,experienceLevel,user");
        assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area","areaId,areaName,latitude,longitude,street");
        for (OrderEntity order: orders)
            returnOrders.add ((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc));

        paginationDto.setData(returnOrders);
        return paginationDto;
    }

    @Override
    public OrderEntity updateOrderStatus(HeaderDto headerDto, OrderEntity order) throws Exception {
        log.info("Updating order status of order: "+headerDto.getOrderId() +" to status: "+ order.getOrderStatus());
        OrderEntity orderEntity = orderDao.findById(headerDto.getOrderId());
        if (orderEntity == null)
            throw new YSException("ORD004");
        if (order.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_DELIVER) && !orderEntity.getOrderStatus().equals(OrderStatus.AT_STORE))
            throw new YSException("ORD007");
        if (orderEntity.getOrderStatus().ordinal() >= order.getOrderStatus().ordinal()) {
            throw new YSException("ORD014");
        }
        if(order.getOrderStatus().equals(OrderStatus.DELIVERED) || order.getOrderStatus().equals(OrderStatus.CANCELLED)){
            BigDecimal distanceInKm = BigDecimal.ZERO;
            if(DistanceType.valueOf(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)).equals(DistanceType.ACTUAL_DISTANCE)) {
                Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
                Map<String, Long> distanceData = new HashMap<>();
                if(orderEntity.getCustomersArea().getArea()!=null) {
                    distanceData = GeoCodingUtil.calculateDistance(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                            orderEntity.getCustomersArea().getArea().getLatitude(), orderEntity.getCustomersArea().getArea().getLongitude(), timePerKm);
                }else{
                    distanceData = GeoCodingUtil.calculateDistance(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                            orderEntity.getCustomersArea().getLatitude(), orderEntity.getCustomersArea().getLongitude(), timePerKm);
                }
                distanceInKm = new BigDecimal(distanceData.get("distance") / 1000.0);
                orderEntity.setDistanceTravelInKM(distanceInKm);
                orderEntity.setDistanceType(DistanceType.ACTUAL_DISTANCE);
            }else{
                if(orderEntity.getCustomersArea().getArea()!=null) {
                    distanceInKm = new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                            orderEntity.getCustomersArea().getArea().getLatitude(), orderEntity.getCustomersArea().getArea().getLongitude()));
                }else {
                    distanceInKm = new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(orderEntity.getStore().getLatitude(), orderEntity.getStore().getLongitude(),
                            orderEntity.getCustomersArea().getLatitude(), orderEntity.getCustomersArea().getLongitude()));
                }
                orderEntity.setDistanceTravelInKM(distanceInKm);
                orderEntity.setDistanceType(DistanceType.AIR_DISTANCE);
            }
        }
        orderEntity.setOrderStatus(order.getOrderStatus());
        orderDao.update(orderEntity);

        //send push notification to driver
        if (order.getOrderStatus().equals(OrderStatus.EN_ROUTE_TO_DELIVER)) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN002", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
        String fields = "orderId,orderDisplayId,orderDescription,orderDeliveryDate,pickupDate,assignDriver,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,driver,store,customer,customersArea";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand");
        assoc.put("driver","driverId,experienceLevel,user");
        assoc.put("customer","customerId,user");
        assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area","areaId,areaName,latitude,longitude,street");
        return  (OrderEntity) ReturnJsonUtil.getJsonObject(orderEntity, fields, assoc, subAssoc);
    }

    @Override
    public PaginationDto searchOrder(HeaderDto headerDto, RequestJsonDto requestJson) throws Exception {

        Page page = requestJson.getPage();
        if(page==null)
            throw new YSException("VLD002");
        String searchString = requestJson.getSearchString();

        PaginationDto paginationDto = new PaginationDto();
        Integer totalRows = orderDao.getTotalNumbersOfSearchedOrders(searchString);

        page.setTotalRows(totalRows);
        paginationDto.setNumberOfRows(totalRows);
        List<OrderEntity> orders = orderDao.searchOrder(page, searchString);

        List<OrderEntity> retOrders = new ArrayList<>();
        String fields = "orderId,orderDisplayId,orderDescription,assignDriver,orderDeliveryDate,pickupDate,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,driver,store,customer,customersArea";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand");
        assoc.put("driver","driverId,experienceLevel,user");
        assoc.put("customer","customerId,user");
        assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress");
        subAssoc.put("area","areaId,areaName,latitude,longitude,street");
        for (OrderEntity order: orders){
            retOrders.add((OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc));
        }
        paginationDto.setData(retOrders);
        return paginationDto;
    }

    @Override
    public List<DriverEntity> getEligibleDriverListForOrder(HeaderDto headerDto) throws Exception {
        log.info("----- Getting Driver list for order -----");
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if (userDeviceEntity == null)
            throw new YSException("STRMGR001");

        OrderEntity orderEntity = null;
        AreaEntity areaEntity = null;
        CustomersAreaEntity customersAreaEntity = null;
        if (headerDto.getOrderId() != null) {
            orderEntity = orderDao.findById(headerDto.getOrderId());
            if (orderEntity == null)
                throw new YSException("ORD004");
        } else if (headerDto.getOrderId() != null) {
            areaEntity = areaDao.findById(headerDto.getAreaId());
            if (areaEntity == null)
                throw new YSException("ARE001");
        }
        if (headerDto.getCustomerAreaId() != null) {
            customersAreaEntity = customersAreaDao.findByCustomersAreaId(headerDto.getCustomerAreaId());
            if (customersAreaEntity == null)
                throw new YSException("ORD004");
        }
        StoreEntity storeEntity = userDeviceEntity.getUser().getStoreManager().getStore();
        List<DriverEntity> driverEntities = driverDao.findStoreDriverForOrder(storeEntity.getStoreId());
        if (storeEntity == null) {
            log.error("----- Store does not exist. -----");
            throw new YSException("STR002");
        }

        List<DriverEntity> driverList = new ArrayList<>();

        String fields = "driverId,driverType,availabilityStatus,orderDeliveryDate,pickupDate,vehicleType,vehicleNumber,licenseNumber,experienceLevel,currentOrderCount,lastOrderDelivered,user,store,latitude,longitude,lastOrderDelivered,currentOrderCount,rating,streetAddress,timeRequiredToCompleteOrders";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("user", "userId,firstName,lastName,emailAddress,mobileNumber,userStatus");
        assoc.put("store", "storeId,givenLocation1,givenLocation2,storeBrand");
        Map<String, String> subAssoc = new HashMap<>();
        subAssoc.put("storeBrand", "storeBrandId,brandName,merchant");
        subAssoc.put("merchant", "merchantId,user");
        subAssoc.put("user", "userId");

        List<Integer> storesNotDriverIds = new ArrayList<>();
        for (StoresNotDriverEntity storesNotDriver : storeEntity.getStoresNotDrivers()) {
            storesNotDriverIds.add(storesNotDriver.getDriver().getId());
        }
        for (DriverEntity driver : driverEntities) {
            if (!storesNotDriverIds.contains(driver.getId())) {
                driver.setCurrentOrderCount(driverDao.getDriversOrderCount(driver.getDriverId()));
                OrderEntity lastOrder = driverDao.getDriversLastOrder(driver.getDriverId());
                if (lastOrder != null && lastOrder.getDeliveredDate() != null) {
                    driver.setLastOrderDelivered(lastOrder.getDeliveredDate());
                } else if (driver.getAvailabilityStatus().equals(AvailabilityStatus.AVAILABLE) && driver.getCurrentOrderCount() == 0) {
                    for (DriversWorkLogEntity workLog : driver.getWorkLogs()) {
                        if (workLog.getToDateTime() == null) {
                            driver.setLastOrderDelivered(workLog.getFromDateTime());
                        }
                    }
                }
                Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
                Long waitingTime = timeRequiredToCompleteOrder(driver, timePerKm);
                Long waitingTimeInHour = (waitingTime / (60 * 60));
                driver.setTimeRequiredToCompleteOrders(waitingTime);
                driver.setRating(driverDao.getDriversAvgRating(driver.getDriverId()));
                if (((driver.getCurrentOrderCount() < Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.MAX_NUM_ORDER_FOR_DRIVER))
                        && driver.getExperienceLevel().equals(DriverExperienceLevel.EXPERIENCED))
                        || (driver.getCurrentOrderCount().equals(0)
                        && driver.getExperienceLevel().equals(DriverExperienceLevel.TRAINEE)))
                        && (waitingTimeInHour <= Long.valueOf(systemPropertyService.readPrefValue(PreferenceType.ORDER_COMPLETION_MAXIMUM_TIME)))) {
                    List<Integer> driverAreaIds = new ArrayList<>();
                    if (orderEntity != null) {
                        if (orderEntity.getCustomersArea().getArea() == null) {
                            driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                        } else {
                            for (DriversAreaEntity driversAreaEntity : driver.getDriversArea()) {
                                driverAreaIds.add(driversAreaEntity.getArea().getId());
                            }
                            if (driverAreaIds.contains(orderEntity.getCustomersArea().getId())) {
                                driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                            }
                        }
                    } else if (areaEntity != null) {
                        for (DriversAreaEntity driversAreaEntity : driver.getDriversArea()) {
                            driverAreaIds.add(driversAreaEntity.getArea().getId());
                        }
                        if (driverAreaIds.contains(orderEntity.getCustomersArea().getId())) {
                            driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                        }
                    } else if (customersAreaEntity != null) {
                        if (customersAreaEntity.getArea() != null) {
                            for (DriversAreaEntity driversAreaEntity : driver.getDriversArea()) {
                                driverAreaIds.add(driversAreaEntity.getArea().getId());
                            }
                            if (driverAreaIds.contains(orderEntity.getCustomersArea().getId())) {
                                driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                            }
                        } else {
                            driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                        }
                    } else {
                        driverList.add((DriverEntity) ReturnJsonUtil.getJsonObject(driver, fields, assoc, subAssoc));
                    }
                }
            }
        }
        return driverList;
    }

    @Override
    public OrderEntity estimateDeliveryCharge(HeaderDto headerDto, CustomersAreaEntity customersArea) throws Exception {
        log.info("Calculating store rate");
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if (userDeviceEntity == null)
            throw new YSException("STRMGR001");
        StoreEntity storeEntity = userDeviceEntity.getUser().getStoreManager().getStore();
        if (storeEntity == null)
            throw new YSException("STR002");
        isVerifiableStore(storeEntity, true);
        List<UnknownAreaRatePlanEntity> unknownAreaRatePlans = storeEntity.getUnknownAreaRatePlans();
        List<UnknownAreaRatePlanEntity> activeUnknownAreaRatePlans = new ArrayList<>();
        UnknownAreaRatePlanEntity unknownAreaRate = new UnknownAreaRatePlanEntity();
        for (UnknownAreaRatePlanEntity unknownAreaRatePlan : unknownAreaRatePlans) {
            if (unknownAreaRatePlan.getStatus().equals(Status.ACTIVE))
                activeUnknownAreaRatePlans.add(unknownAreaRatePlan);
        }
        if (activeUnknownAreaRatePlans.size() == 0)
            throw new YSException("UNRPN001");
        BigDecimal distanceInKm = BigDecimal.ZERO;
        if(DistanceType.valueOf(systemPropertyService.readPrefValue(PreferenceType.AIR_OR_ACTUAL_DISTANCE_SWITCH)).equals(DistanceType.ACTUAL_DISTANCE)) {
            Integer timePerKm = Integer.parseInt(systemPropertyService.readPrefValue(PreferenceType.DRIVING_TIME_PER_KM));
            Map<String, Long> data = GeoCodingUtil.calculateDistance(storeEntity.getLatitude(), storeEntity.getLongitude(), customersArea.getLatitude(), customersArea.getLongitude(), timePerKm);
            Long distance = data.get("distance");
            Long duration = data.get("duration");
            distanceInKm = BigDecimal.valueOf(distance).divide(BigDecimal.valueOf(1000));
        } else {
            distanceInKm = new BigDecimal(GeoCodingUtil.getDistanceFromLatLonInKm(storeEntity.getLatitude(), storeEntity.getLongitude(), customersArea.getLatitude(), customersArea.getLongitude()));
        }

        Collections.sort(activeUnknownAreaRatePlans, new Comparator<UnknownAreaRatePlanEntity>() {
            public int compare(UnknownAreaRatePlanEntity first, UnknownAreaRatePlanEntity second) {
                return first.getEndDistance().compareTo(second.getEndDistance());
            }
        });
        for (UnknownAreaRatePlanEntity unknownAreaRatePlan : activeUnknownAreaRatePlans) {
            if (BigDecimalUtil.isGreaterThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))
                    && BigDecimalUtil.isLessThenOrEqualTo(distanceInKm, BigDecimal.valueOf(unknownAreaRatePlan.getStartDistance()))) {
                unknownAreaRate = unknownAreaRatePlan;
                break;
            }
        }
        if (unknownAreaRate.getId() == null)
            unknownAreaRate = activeUnknownAreaRatePlans.get(activeUnknownAreaRatePlans.size() - 1);

        BigDecimal estimatedCharge = unknownAreaRate.getBaseCharge();
        estimatedCharge = estimatedCharge.add(distanceInKm.multiply(unknownAreaRate.getAdditionalChargePerKM()));

        OrderEntity order = new OrderEntity();
        order.setEstimatedDeliveryCharge(estimatedCharge);
        //order.setEstimatedDeliveryTime(BigDecimal.valueOf(duration));
        order.setDistanceInKm(distanceInKm);
        return order;
    }

    @Override
    public void updateDeviceToken(HeaderDto headerDto, UserDeviceEntity userDevice) throws Exception {
        log.info("Updating device token of store manager");
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if (userDeviceEntity == null)
            throw new YSException("STRMGR001");
        userDeviceEntity.setDeviceToken(userDevice.getDeviceToken());
        userDeviceEntity.setFamily(userDevice.getFamily());
        userDeviceDao.update(userDeviceEntity);
    }

    @Override
    public List<AreaEntity> getStoreServingAreas(HeaderDto headerDto) throws Exception {
        log.info("Getting serving area of store");
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if (userDeviceEntity == null || !userDeviceEntity.getUser().getUserRole().equals(UserRole.ROLE_STORE_MANAGER) || userDeviceEntity.getUser().getStoreManager().getStore() == null)
            throw new YSException("STRMGR001");

        StoreEntity storeEntity = userDeviceEntity.getUser().getStoreManager().getStore();
        if (storeEntity == null)
            throw new YSException("STR002");
        List<StoresAreaEntity> storesAreas = storeEntity.getStoresAreas();
        List<AreaEntity> returnAreas = new ArrayList<>();
        Set<AreaEntity> areas = new HashSet<>();
        List<Integer> ids = new ArrayList<>();
        for (StoresAreaEntity storesArea : storesAreas) {
            if (storesArea.getArea().getStatus().equals(Status.ACTIVE) && storesArea.getArea().getParent() != null) {
                AreaEntity finalParentArea = storesArea.getArea();
                ids.add(finalParentArea.getId());
                while (finalParentArea.getParent() != null && finalParentArea.getParent().getStatus().equals(Status.ACTIVE)) {
                    finalParentArea = finalParentArea.getParent();
                    ids.add(finalParentArea.getId());
                }
                if (finalParentArea.getParent() == null)
                    areas.add(finalParentArea);
            }
        }

        String fields = "areaId,areaName,child";
        Map<String, String> assoc = new HashMap<>();
        assoc.put("child", "areaId,areaName");
        for (AreaEntity area : areas) {
            parseAreaTree(area, ids);
            returnAreas.add((AreaEntity) ReturnJsonUtil.getJsonObject(area, fields, assoc));
        }
        return returnAreas;
    }

    @Override
    public void cancelOrder(HeaderDto headerDto, RequestJsonDto requestJsonDto) throws Exception {
        log.info("----- Canceling the order -----");
        UserDeviceEntity userDeviceEntity = userDeviceDao.findByAccessToken(MD5Hash.getMD5(headerDto.getAccessToken()));
        if(headerDto.getOrderId()==null)
            throw new YSException("ORD010");
        if(requestJsonDto.getOrderCancelReason()==null||requestJsonDto.getOrderCancelReason().isEmpty())
            throw new YSException("ORD011");

        UserEntity userEntity = userDeviceEntity.getUser();
        OrderEntity orderEntity = orderDao.findById(headerDto.getOrderId());
        if(orderEntity==null)
            throw new YSException("ORD004");
        if(orderEntity.getOrderStatus().equals(OrderStatus.DELIVERED))
            throw new YSException("ORD012");
        if (orderEntity.getOrderStatus().equals(OrderStatus.CANCELLED))
            throw new YSException("ORD013");

        orderEntity.setOrderStatus(OrderStatus.CANCELLED);

        OrderCancelEntity orderCancelEntity = new OrderCancelEntity();
        orderCancelEntity.setOrderStatus(orderEntity.getOrderStatus());
        orderCancelEntity.setCancelledDate(new Timestamp(System.currentTimeMillis()));
        orderCancelEntity.setReason(requestJsonDto.getOrderCancelReason());
        orderCancelEntity.setUser(userEntity);
        orderCancelEntity.setOrder(orderEntity);
        orderEntity.setOrderCancel(orderCancelEntity);
        orderDao.update(orderEntity);

        ResponseOrderDto socketOrder = new ResponseOrderDto();
        socketOrder.setOrderId(orderEntity.getOrderId());
        socketOrder.setOrderStatus(orderEntity.getOrderStatus());
        if(orderEntity.getDriver()!=null)
            socketOrder.setDriverId(orderEntity.getDriver().getDriverId());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode messageMessage = mapper.createObjectNode();
        messageMessage.put("type", "message");
        messageMessage.put("messageType", WebSocketMessageType.ORDER_STATUS.toString());
        messageMessage.put("message", mapper.writeValueAsString(socketOrder));
        List<WebSocketSession> sessions = webRTCService.getSessions();
        for (WebSocketSession session: sessions){
            session.sendMessage(new TextMessage(mapper.writeValueAsString(messageMessage)));
        }

        //send push notification to driver
        if (orderEntity.getDriver() != null) {
            List<String> tokens = userDeviceDao.findDeviceTokenByUserId(orderEntity.getDriver().getUser().getId());
            if (tokens.size() > 0) {
                PushNotification pushNotification = new PushNotification();
                pushNotification.setTokens(tokens);
                Map<String, String> message = new HashMap<>();
                message.put("message", MessageBundle.getMessage("DPN003", "push_notification.properties"));
                message.put("redirect", PushNotificationRedirect.ORDER.toString());
                message.put("id", orderEntity.getOrderId().toString());
                pushNotification.setMessage(message);
                pushNotification.setNotifyTo(NotifyTo.DRIVER);
                PushNotificationUtil.sendNotificationToAndroidDevice(pushNotification);
                log.info("Push notification has been sent successfully");
            }
        }
    }

}