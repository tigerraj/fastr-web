package com.yetistep.anyorder.service.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Sagar
 * Date: 12/5/14
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */

import com.yetistep.anyorder.abs.AbstractManager;
import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.enums.Status;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.enums.UserStatus;
import com.yetistep.anyorder.model.AuthenticatedUser;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.util.YSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Custom Spring Authentication and Authorization Class
 */
@Service
public class SpringUserDetailsServiceImpl extends AbstractManager implements UserDetailsService {

    @Autowired
    private UserDao userDaoService;

    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {
        UserEntity user = userDaoService.findByUserName(username);
        if(user==null)
            throw new YSException("USR001");
        if(!user.getUserStatus().equals(UserStatus.ACTIVE))
            throw new YSException("USR008");
        List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());
        return buildUserForAuthentication(user, authorities);

    }

    private AuthenticatedUser buildUserForAuthentication(UserEntity user,
                                                         List<GrantedAuthority> authorities) {
        Boolean enabled = false;

        if(user.getVerifiedStatus())
            enabled = true;
        AuthenticatedUser authenticatedUser = new AuthenticatedUser(user.getUsername(),
                user.getPassword(), enabled, true, true, true, authorities);
        if (user.getUserRole().equals(UserRole.ROLE_STORE_MANAGER) && (!user.getStoreManager().getStore().getStatus().equals(Status.ACTIVE) || !user.getStoreManager().getStore().getStoreBrand().getStatus().equals(Status.ACTIVE) || !isVerifiableStore(user.getStoreManager().getStore(), false)))
            throw new YSException("STR004");
        if(user.getUserRole().equals(UserRole.ROLE_MERCHANT)){
            String merchantId = user.getMerchant().getMerchantId();
            authenticatedUser.setMerchantId(merchantId);
            authenticatedUser.setBusinessName(user.getMerchant().getBusinessTitle());
        }
        if(user.getUserRole().equals(UserRole.ROLE_STORE_MANAGER)){
            String storeManagerId = user.getStoreManager().getStoreManagerId();
            authenticatedUser.setStoreManagerId(storeManagerId);
        }
        authenticatedUser.setFakeUserId(user.getUserId());

        return authenticatedUser;
    }

    private List<GrantedAuthority> buildUserAuthority(UserRole userRole) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        // Build user's authorities
        try {
            setAuths.add(new SimpleGrantedAuthority(userRole.toString()));
            return new ArrayList<GrantedAuthority>(setAuths);
        }catch (Exception e) {
            throw e;
        }
    }

    public void setUserDaoService(UserDao userDaoService) {
        this.userDaoService = userDaoService;
    }

}
