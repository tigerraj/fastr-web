package com.yetistep.anyorder.service.impl;

import com.yetistep.anyorder.dao.inf.DriverDao;
import com.yetistep.anyorder.dao.inf.OrderDao;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.enums.OrdersDriverStatus;
import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.model.OrderRatingEntity;
import com.yetistep.anyorder.model.OrdersDriverEntity;
import com.yetistep.anyorder.service.inf.CustomerService;
import com.yetistep.anyorder.util.ReturnJsonUtil;
import com.yetistep.anyorder.util.YSException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 7/8/16
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    Logger log = Logger.getLogger(CustomerServiceImpl.class);

    @Autowired
    OrderDao orderDao;

    @Autowired
    DriverDao driverDao;

    @Override
    public OrderEntity getOrderDetail(HeaderDto headerDto) throws Exception {
        OrderEntity order = orderDao.findById(headerDto.getOrderId());
        if(order==null)
            throw new YSException("ORD004");

        if(order.getDriver()!=null){
            BigDecimal totalPreviousOrderEtd = driverDao.getDriversPreviousOrderTotalEtd(order.getDriver().getDriverId(), order.getId());
            for (OrdersDriverEntity ordersDriverEntity: order.getOrdersDrivers()){
                if (ordersDriverEntity.getDriverStatus().equals(OrdersDriverStatus.RUNNING))
                    if(order.getEstimatedDeliveryTime()!=null)
                    order.setEstimatedTimeOfDelivery(new Timestamp(ordersDriverEntity.getAssignedDate().getTime()+(order.getEstimatedDeliveryTime().add(totalPreviousOrderEtd)).multiply(new BigDecimal(60*1000)).longValue()));
            }
        }

        String fields = "orderId,orderDisplayId,orderDescription,estimatedTimeOfDelivery,driverType,orderStatus,paymentMode,customersAreaType,orderOriginType,deliveryCharge,totalBillAmount,grandTotal,createdDate,deliveredDate,dispatchDate,estimatedDeliveryTime,prioritized,orderPriority,commentOnDriver,commentOnFood,customersNoteToDriver,store,customer,customersArea,driver,orderDeliveryDate,pickupDate,orderRating";

        Map<String, String> assoc = new HashMap<>();
        Map<String, String> subAssoc = new HashMap<>();
        assoc.put("store","storeId,givenLocation1,givenLocation2,addressNote,latitude,longitude,priority,area,storeBrand");
        assoc.put("customer","customerId,user");
        assoc.put("customersArea","customersAreaId,street,city,givenLocation,latitude,longitude,additionalNote,area");
        assoc.put("driver", "driverId,user");
        assoc.put("orderRating", "orderRatingId,ratingByCustomer,commentByCustomer");
        subAssoc.put("storeBrand","storeBrandId,brandName,brandLogo");
        subAssoc.put("user","firstName,lastName,mobileNumber,emailAddress,profileImage");
        subAssoc.put("area","areaId,areaName,latitude,longitude");
        return  (OrderEntity) ReturnJsonUtil.getJsonObject(order, fields, assoc, subAssoc);
    }

    @Override
    public void rateOrder(OrderEntity order) throws Exception {
        log.info("----- Rating order. -----");
        OrderEntity orderEntity = orderDao.findById(order.getOrderId());
        if (orderEntity == null) {
            log.error("----- Order does not exist. -----");
            throw new YSException("ORD004");
        }
        if (orderEntity.getOrderRating() != null) {
            log.error("----- Order has already been rated. -----");
            throw new YSException("ORD008");
        }
        OrderRatingEntity orderRating = order.getOrderRating();
        orderRating.setOrder(orderEntity);
        orderEntity.setOrderRating(order.getOrderRating());
        orderDao.update(orderEntity);
        //update driver average rating
        DriverEntity driverEntity = orderEntity.getDriver();
        driverEntity.setRating(driverDao.getDriversAvgRating(driverEntity.getDriverId()));
        driverDao.update(driverEntity);
    }
}
