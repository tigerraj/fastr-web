insert into preferences_sections (section, group_id) values ("Country And currency",1),("Support configuration",1),("Company Information",1)
,("Version Configuration",1),("driver assign algorithm",2);

INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('CURRENCY', 'Rs.', '1', 'Currency');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('COUNTRY', 'Nepal', '1', 'Country');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('SMS_PROVIDER', 'Twillo', '1', 'Sms Provider');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('SMS_COUNTRY_CODE', '977', '1', 'Country Code');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('HELPLINE_NUMBER', '20', '2', 'Helpline Number');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('ADMIN_EMAIL', 'admin@anyorder.ae', '2', 'Admin Email');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('SUPPORT_EMAIL', 'admin@yetistep.com', '2', 'Support Email');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('SERVER_URL', 'http://52.74.163.231/', '2', 'Server Url');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('COMPANY_NAME', 'Any Order Services', '3', 'Company Name');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('COMPANY_LOGO', '', '3', 'Company Logo');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('LOGO_FOR_PDF_EMAIL', '', '3', 'Logo for PDF And Email');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('COMPANY_ADDRESS', 'Sunrize Bizpark, Dillibazzar, Kathmandu, Nepal', '3', 'Company Address');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('CONTACT_NO', '9849540028', '3', 'Contact No');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('COMPANY_EMAIL', 'admin@yetistep.com', '3', 'Company Email');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('COMPANY_WEBSITE', 'yetistep.com', '3', 'Company Website');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('REGISTRATION_NO', '34534', '3', 'Registratin No');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('APPLICATION_NAME', 'Any Order Delivery Services', '3', 'Application Name');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('DEFAULT_IMG_SEARCH', '', '3', 'Default Search Image');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('ANDROID_APP_VER_NO', '0.01', '4', 'Merchant Application Version No');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('WEB_APP_VER_NO', '0.01', '4', 'Web Application Version No');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('IOS_APP_VER_NO', '0.01', '4', 'Driver Application Version No');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('GOOGLE_TRAFFIC_MODEL', 'BEST_GUESS', '5', 'Google Map API Traffic Model');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('GOOGLE_TRAVEL_MODE', 'Car', '5', 'Google Map API Travel Mode');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('ASSIGN_DRIVER_AUTOMATICALLY', '0', '5', 'Assign Driver Automatically On the Order');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('DEVIATION_TIME_IN_MIN', '20', '5', 'Deviation Time In Minute');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('MAX_NUM_ORDER_FOR_DRIVER', '4', '5', 'Maximum Number of Order for Driver');
INSERT INTO `any_order_db`.`preferences` (`pref_key`, `value`, `section_id`, `pref_title`) VALUES ('DRIVER_LOCATION_UPDATE_TIME', '20', '5', 'Driver Location Update Time in Minute');

