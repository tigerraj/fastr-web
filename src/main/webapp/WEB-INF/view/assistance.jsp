<%@ page import="com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const" %>
<%@ page import="com.yetistep.anyorder.abs.Constant" %>
<%@include file="includes/header.jsp" %>
<%@include file="includes/menu_primary.jsp" %>
<div class="container pt25 center-me positionabsolute">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-1 separator">
            <div class="text-center pv50">
                <p><i class="fa fa-key fa-4x" aria-hidden="true"></i></p>

                <p class="font24 c-t-light">Welcome to <strong class="c-t-dark"><%=Constant.SUPPORT_NAME%> Support</strong><br>Set up your
                    Password.
                </p>
            </div>
        </div>

        <div class="col-sm-5 c-t-dark separator-fx-div pt15 pb30">
            <form action="" class="" method="post" id="setPasswordForm" novalidate>
                <div class="form-group floating mt25">
                    <label class="control-label">Password</label>

                    <div class="input-group ">
                        <input type="password" class="form-control input-lg c-b-trans" placeholder="" name="password"
                               id="password"
                               minlength="6" required>
                        <span class="input-group-addon cursorpointer" id="blinkPassword" type="button"><i
                                class="fa fa-eye"></i></span>

                    </div>

                </div>
                <div class="form-group floating mt25">
                    <label class="control-label">Confirm Password</label>

                    <div class="input-group ">
                        <input type="password" class="form-control input-lg c-b-trans" placeholder="" name="cpassword"
                               id="cpassword"
                               minlength="6" required>
                        <span class="input-group-addon cursorpointer" id="cblinkPassword" type="button"><i
                                class="fa fa-eye"></i></span>

                    </div>

                </div>

                <div class="clearfix">
                    <button class="btn btn-success c-b-success pull-right btn-lg" type="submit">Set Password</button>
                </div>
            </form>

        </div>
    </div>
</div>

<%@include file="includes/scripts.jsp" %>
<%@include file="includes/validate.jsp" %>


<script src="${pageContext.request.contextPath}/resources/custom/js/login.min.js"></script>
<script type="text/javascript">

    $(function () {
        if ($('#email').val()) {
            $('#email').blur().focus();
        }

        setPasswordModule.init();
    });
</script>
</body>
</html>

