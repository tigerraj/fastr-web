<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Reports</h3>
                        </div>

                        <div class="content-body box-me pv25">
                            <form id="reportForm" action="" method="POST">
                                <div class="row mb15">
                                    <div class="col-md-10">
                                        <div class="col-sm-3">
                                            <label for="reportFromDate">From Date</label>

                                            <div class="form-group">
                                                <div class="input-group date" id="reportFromDatePicker">
                                                    <input type="text" id="reportFromDate" name="reportFromDate"
                                                           class="form-control"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="reportFromTime">From Time</label>

                                            <div class="form-group">
                                                <div class="input-group date" id="reportFromTimePicker">
                                                    <input type="text" id="reportFromTime" name="reportFromTime"
                                                           class="form-control"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="reportToDate">To Date</label>

                                            <div class="form-group">
                                                <div class="input-group date" id="reportToDatePicker">
                                                    <input type="text" id="reportToDate" name="reportToDate"
                                                           class="form-control"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="reportToTime">To Time</label>

                                            <div class="form-group">
                                                <div class="input-group date" id="reportToTimePicker">
                                                    <input type="text" id="reportToTime" name="reportToTime"
                                                           class="form-control "/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 hidden">
                                            <label for="reportMerchants">Select Merchant</label>
                                            <select id="reportMerchants" name="reportMerchant"
                                                    class="form-control select-picker-reports"
                                                    data-actions-box="true" data-size="7" multiple >
                                            </select>
                                        </div>
                                        <div class="col-sm-3 hidden">
                                            <label for="reportStores">Select Store</label>
                                            <select id="reportStores" name="reportStore"
                                                    class="form-control select-picker-reports"
                                                    data-actions-box="true" data-size="7" multiple >
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="reportDrivers">Select Driver</label>
                                            <select id="reportDrivers" name="reportDrivers"
                                                    class="form-control select-picker-reports"
                                                    data-actions-box="true" data-size="7" multiple>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="reportOrderStatus">Select State of Order</label>
                                            <select id="reportOrderStatus" name="reportOrderStatus"
                                                    class="form-control select-picker-reports"
                                                    data-actions-box="true" data-size="7" multiple>
                                                <option value="ORDER_PLACED"> Order Placed</option>
                                                <option value="ORDER_STARTED"> Order Started</option>
                                                <option value="EN_ROUTE_TO_PICK_UP"> En Route to Pick Up
                                                </option>
                                                <option value="AT_STORE"> At Store</option>
                                                <option value="EN_ROUTE_TO_DELIVER"> En Route to Deliver
                                                </option>
                                                <option value="AT_CUSTOMER"> At Customer</option>
                                                <option value="DELIVERED"> Delivered</option>
                                                <option value="CANCELLED"> Cancelled</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" id="btnGenerateReport" class="btn btn-default btn-block" disabled>
                                            Generate Report
                                        </button>
                                        <button type="reset" class="btn btn-default btn-block">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="table-ao-wrapper">
                                    <table id="reportsTable" class="table table-ao w100p"></table>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<%@include file="../includes/datatables.jsp" %>
<%@include file="../includes/validate.jsp" %>

<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<script src="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/reports.js"></script>

<script>
    $(function () {
        reportsModule.init();
    });
</script>
<style>
    .pagination {
        float: right;
    }
</style>

</body>
</html>