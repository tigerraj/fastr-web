<%--
  Created by IntelliJ IDEA.
  User: Pratik
  Date: 8/8/2016
  Time: 11:50 AM
  To change this template use File | Settings | File Templates.
--%>

<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>
<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Driver Performance</h3>
                        </div>

                        <div class="content-body box-me">
                            <div class="row mb15">
                                <div class="col-sm-3">
                                    <label for="">Select Driver</label>
                                    <select id="" multiple class="form-control select-picker-driver" data-actions-box="true" data-size="10">
                                        <option value="">Driver One</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label for="">From</label>

                                    <div class="form-group">
                                        <div class='input-group date' id='fromDatePicker'>
                                            <input type='text' class="form-control"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label for="">To</label>

                                    <div class="form-group">
                                        <div class='input-group date' id='toDatePicker'>
                                            <input type='text' class="form-control"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1">
                                    <label for="">&nbsp;</label><br>

                                    <div class="">
                                        <button type="button" class="btn btn-default btn-block" id="goBtn">
                                            Go
                                        </button>
                                        <%-- <button type="button" class="btn btn-default"
                                                 onclick="$('.select-picker-driver').selectpicker('deselectAll');"
                                                 style="width: 50%">Clear
                                         </button>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="table-ao-wrapper">
                                <table id="listDriversTable" class="table table-ao w100p">
                                    <thead>
                                    <tr>
                                        <th >S/N</th>
                                        <th >Date</th>
                                        <th >Name</th>
                                        <th>Duty Hour</th>
                                        <%--<th>Busy Hour</th>--%>
                                        <th>Order Assigned</th>
                                        <th>Order Completed</th>
                                        <th>Distance Travelled</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<%@include file="../includes/datatables.jsp" %>

<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<script src="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/driver.min.js"></script>

<script>
    $(function(){
       driverPerformanceModule.init();
    });
</script>
<style>
    .pagination{
        float: right;
    }
</style>

</body>
</html>


