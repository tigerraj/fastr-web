<%@ page import="com.yetistep.anyorder.enums.PaymentMode" %>
<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50 col-md-8">
                        <div class="content-header">
                            <h3 class="content-title">Add Order</h3>
                        </div>

                        <div class="content-body box-me">
                            <form id="createOrderForm" class="order-form" method="POST" action="" data-status="save"
                                  data-customer="unknown" data-location="new">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="mt0 mb25"> Basic Information </h4>

                                        <div class="form-group floating">
                                            <label for="pickUpLocation" class="control-label">Pick Up Location</label>
                                            <input type="text" class="form-control input-lg" id="pickUpLocation"
                                                   name="pickUpLocation">
                                        </div>

                                        <div id="knownAreaWrapper">
                                            <h4 class="mt0 mb25">
                                                Dropoff Location
                                                <div id="locationType" class="btn-group ml20 pull-right" role="group"
                                                     aria-label="Choose Drop off Location type">
                                                    <div class="btn-group btn-group-sm" role="group">
                                                        <button type="button" data-status="new"
                                                                class="js-location-type btn btn-default active">New
                                                        </button>
                                                    </div>
                                                    <div class="btn-group btn-group-sm" role="group">
                                                        <button type="button" data-status="existing"
                                                                class="js-location-type btn btn-default disabled">
                                                            Existing
                                                        </button>
                                                    </div>
                                                </div>
                                            </h4>
                                            <div class="form-group floating">
                                                <label for="mainArea" class="control-label">Main Area</label>
                                                <input type="text" class="form-control input-lg" id="mainArea"
                                                       name="mainArea">
                                            </div>
                                            <div class="form-group floating">
                                                <label for="subArea" class="control-label">Sub Area</label>
                                                <input type="text" class="form-control input-lg" id="subArea"
                                                       name="subArea" disabled>
                                            </div>
                                            <div class="form-group floating">
                                                <label for="houseNumber" class="control-label">Street Address/House
                                                    Number</label>
                                                <input type="text" class="form-control input-lg" id="houseNumber"
                                                       name="houseNumber">
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <button type="button" id="addAddressBtn"
                                                            class="btn btn-default c-b-trans pull-right hidden"
                                                            data-open="false">Add Other Address
                                                    </button>
                                                </div>
                                            </div>
                                        </div> <!-- end of Known Area Wrapper -->


                                        <div class="">

                                            <h4>Mode of Payment</h4>

                                            <div class="displayinline">
                                                <input id="cod" type="radio" name="modeOfPayment"
                                                       class="with-font form-control"
                                                       checked value="<%=PaymentMode.COD%>"/>
                                                <label for="cod" class="control-label">COD</label>
                                            </div>

                                            <div class="displayinline mh25">
                                                <input id="creditCard" type="radio" name="modeOfPayment"
                                                       class="with-font form-control" value="<%=PaymentMode.CARD%>"/>
                                                <label for="creditCard" class="control-label">Credit Card</label>
                                            </div>

                                            <div class="displayinline">
                                                <input id="pos" type="radio" name="modeOfPayment"
                                                       class="with-font form-control" value="<%=PaymentMode.POS%>"/>
                                                <label for="pos" class="control-label">Card on Delivery</label>
                                            </div>

                                        </div>

                                        <div class="form-group mt25 mb0">
                                            <textarea class="form-control"
                                                      placeholder="Special Request From Customer To The Driver"
                                                      id="customerNote"
                                                      name="customerNote" rows="2"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="mt0 mb25"> Customer &amp; Order Information </h4>

                                        <div class="form-group floating">
                                            <label for="customerPhone" class="control-label">Customer
                                                Phone</label>
                                            <input type="number" class="form-control input-lg" id="customerPhone"
                                                   name="customerPhone" maxlength="10" pattern="\d{10}">
                                        </div>
                                        <div class="form-group floating">
                                            <label for="customerName" class="control-label">Customer
                                                Name</label>
                                            <input type="text" class="form-control input-lg" id="customerName"
                                                   name="customerName">
                                        </div>

                                        <div class="form-group floating">
                                            <label for="deliveryDate" class="control-label">Delivery
                                                Date</label>
                                            <input type="text" class="form-control input-lg" id="deliveryDate"
                                                   name="deliveryDate">
                                        </div>
                                        <div class="form-group floating">
                                            <label for="deliveryTime" class="control-label">Delivery
                                                Time</label>
                                            <input type="text" class="form-control input-lg" id="deliveryTime"
                                                   name="deliveryTime">
                                        </div>
                                        <div class="form-group floating">
                                            <label for="deliveryFee" class="control-label">Delivery Fee</label>
                                            <input type="number" class="form-control input-lg"
                                                   name="deliveryFee"
                                                   id="deliveryFee">
                                        </div>
                                        <div class="form-group floating">
                                            <label for="orderTotal" class="control-label">Order Total</label>
                                            <input type="number" class="form-control input-lg" id="orderTotal"
                                                   name="orderTotal">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control"
                                                      placeholder="Order Note" id="orderNote" rows="2"
                                                      name="orderNote"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div id="unknownAreaWrapper" class="row mt25 hidden">
                                    <h4> Unknown Dropoff Location
                                        <button type="button" id="closeAddressBtn" class="btn btn-default c-b-trans">Close Other Address </button>
                                    </h4>

                                    <div class="col-md-8">
                                        <div id="googleMapsWrapper" class="">
                                            <input id="pac-input" name="pac-input"
                                                   class="controls form-control w400 mt15 p15 border0 h50"
                                                   type="text"
                                                   placeholder="Enter postal code to focus map and make marker">

                                            <div id="map" class="w100p h400"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <section class="text-center p20 c-b-green c-t-white col-xs-6 form-block">
                                                <p>Distance</p>
                                                <span id="routeDeliveryDistance"> 0 </span>
                                            </section>
                                            <section
                                                    class="text-center p20 c-b-green c-t-white col-xs-6 form-block">
                                                <p>Delivery Fee</p>
                                                <span id="routeDeliveryFee"> 0 </span>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <div class="form-group floating mt30">
                                                <label for="buildingNumber" class="control-label">Building
                                                    Number</label>
                                                <input type="text" class="form-control input-lg" id="buildingNumber"
                                                       name="buildingNumber">
                                            </div>
                                            <div class="form-group floating">
                                                <label for="streetAddress" class="control-label">Street Address</label>
                                                <input type="text" class="form-control input-lg" id="streetAddress"
                                                       name="streetAddress">
                                            </div>
                                            <div class="form-group floating">
                                                <label for="city" class="control-label">City</label>
                                                <input type="text" class="form-control input-lg" id="city" name="city">
                                            </div>

                                            <button class="btn pull-right" id="removeMarker" type="button">Remove Marker
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr class="hr-mod">
                                        <button type="button" class="btn btn-danger pull-right btn-lg"
                                                onclick="window.history.back()">Cancel
                                        </button>
                                        <button type="button" id="orderSave" name="orderSave"
                                                class="btn btn-success c-b-success pull-right btn-lg mh15">
                                            Save
                                        </button>
                                        <button type="button" id="orderSaveAssign" name="orderSaveAssign"
                                                class="btn btn-success c-b-success pull-right btn-lg">
                                            Save & Assign
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="saveAndAssignModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg w1000">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Assign Order</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div id="anyOrderDriverBox" class="hidden">
                            <div class="box-me pt10">
                                <div class="ph15 ">
                                    <h4>Select driver from the list</h4>

                                    <div id="anyOrderDriverWrapper" class="mb15 h500 overflowauto"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-success btn-lg btn-block" id="confirmAnyOrderDriverBtn"
                                            disabled="disabled">Confirm
                                        Driver
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div style="height: 570px;" class="w100p" id="mapCanvasModal"></div>
                        <button class="btn btn-success btn-lg btn-block" id="bookAnyOrderDriverBtn">Book <%=Constant.SUPPORT_NAME%>
                            Driver
                        </button>
                    </div>
                    <div class="col-md-6">
                        <div id="merchantDriverBox" class="box-me pt10">
                            <div class="ph15 ">
                                <h4>Select driver from the list</h4>

                                <div id="ownDriverWrapper" class="mb15 h500 overflowauto"></div>
                            </div>
                            <button class="btn btn-success btn-lg btn-block" id="assignOwnDriverBtn">Assign Own Driver
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/validate.jsp" %>
<script src="${pageContext.request.contextPath}/resources/custom/js/maps.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
`
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<script src="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/order.js"></script>
<script type="text/javascript">
    var initGoogleMaps = function () {
        var config = {
            zoom: 9,
            center: {lat: 25.234026, lng: 55.170419},
            isMultiMarker: false,
            searchInput: 'pac-input',
            mapCanvas: "map",
        };
        superMap = newMapModule.init(config);
        $('#googleMapsWrapper').closest('.row').addClass('hidden');
    };
    $(function () {
        $("li[data-menu='orders']").addClass('active');
        orderModule.create();
    });
    var superMap;
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjbtflnGL0mEj7aHh9VOHPAa_0cqbJabY&callback=initGoogleMaps&libraries=places"
        async defer></script>

</body>
</html>

