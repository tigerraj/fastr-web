<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>
<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <sec:authorize
                                    access="!hasRole('ROLE_STORE_MANAGER')">
                                <a href="${pageContext.request.contextPath}/merchant/driver_form"
                                   class="btn btn-success pull-right " id="addDriverBtn"><i class="fa fa-plus"></i>
                                    Driver</a>
                            </sec:authorize>
                            <sec:authorize
                                    access="hasAnyRole('ROLE_SMANGER', 'ROLE_ADMIN','ROLE_MANAGER','ROLE_CSR','ROLE_MERCHANT')">
                                <a href="${pageContext.request.contextPath}/smanager/driver_performance"
                                   class="btn btn-success pull-right mr15">Driver Performance</a>
                            </sec:authorize>
                            <h3 class="content-title">List Driver</h3>
                        </div>

                        <div class="content-body box-me">
                            <div class="table-ao-wrapper">
                                <table id="listDriversTable" class="table table-ao w100p">
                                    <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Driver Name</th>
                                        <th>Contact</th>
                                        <th>Email</th>
                                        <th>Belongs To</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<%@include file="../includes/datatables.jsp" %>


<script src="${pageContext.request.contextPath}/resources/custom/js/driver.min.js"></script>

<script>
    $(function () {
        $("li[data-menu='drivers']").addClass('active');
        listDriverModule.init();
    });


</script>

</body>
</html>

