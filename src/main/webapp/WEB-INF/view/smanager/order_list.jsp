<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>
<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content clearfix pb0">
                    <div class="content-nav clearfix">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>
                    <div class="content-main order-list-page clearfix">
                        <div id="col-orders" class="col-lg-3 col-md-4 p0">
                            <div class="content-header c-b-active c-t-white clearfix text-center font16 m0 pv10"><p
                                    class="col-xs-11 m0 p0"> Orders </p>

                                <div class="dropdown col-xs-1 p0">
                                    <button type="button"
                                            class="btn btn-default dropdown-toggle c-bo-trans c-b-trans c-t-white border0 p0"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false"><i class="fa fa-fw fa-lg fa-ellipsis-v"
                                                                     aria-hidden="true"></i></button>
                                    <ul class="dropdown-menu">
                                        <li><a id="orderTypeCurrent" href="#?order=current">Current Orders</a></li>
                                        <li><a id="orderTypePast" href="#?order=past">Successful Order</a></li>
                                        <li><a id="orderTypeCancelled" href="#?order=cancelled">Cancelled Order</a></li>
                                    </ul>
                                </div>
                            </div> <!-- end of Order Type -->

                            <form class="c-b-gray-light" role="search" id="searchOrderForm">
                                <div class="row">
                                    <div class="col-xs-10">
                                        <div class="form-group menu-search-box mb0">
                                            <div class="input-group">
                                                <span id="searchOrderStatusDropdown" class="input-group-addon pl0">
                                                    <button type="button" class="btn c-b-trans dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-search"></i> <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li class="active"><a data-type="" href="#">All Orders</a></li>
                                                        <li><a data-type="RUNNING" href="#">Current Orders</a></li>
                                                        <li><a data-type="DELIVERED" href="#">Successful Orders</a></li>
                                                        <li><a data-type="CANCELLED" href="#">Cancelled Orders</a></li>
                                                    </ul>
                                                </span>
                                                <input type="search" class="form-control c-b-trans" placeholder="Search All Orders..." id="searchOrderInput" data-type="" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="p15">
                                            <a class="cursorpointer" id="hideOrderList">
                                                <i class="fa fa-fw fa-lg fa-chevron-left c-t-gray-dark"
                                                   aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                            </form>


                            <div class="content-body clearfix">
                                <div class="row m0">
                                    <div class="col-xs-12 c-b-gray-light p0 p15">
                                        <div class="pull-left">
                                            <a class="cursorpointer" id="refreshOrdersListBtn">
                                                <i class="fa fa-fw fa-lg fa-refresh c-t-gray-dark"
                                                   aria-hidden="true"></i>
                                            </a>
                                        </div>


                                        <div class="dropdown pull-right">
                                            <button type="button"
                                                    class="btn btn-default dropdown-toggle c-bo-trans c-b-trans p0"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Most Recent <span class="ml5 caret"></span>
                                            </button>
                                            <ul class="dropdown-menu changevalue" id="orderSort2">
                                                <li>
                                                    <a id="orderFilterRecent" href="javascript:"
                                                       data-sortby="createdDate" data-direction="desc">Most Recent</a>
                                                </li>
                                                <li>
                                                    <a id="orderFilterWaiting" href="javascript:"
                                                       data-sortby="createdDate" data-direction="asc">Waiting Time</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> <!-- end of Order Filter -->
                                <div class="row m0">
                                    <div id="currentOrderWrapper"> <!-- Nav tabs -->
                                        <ul class="nav nav-pills nav-justified mb10" role="tablist" id="rootOrderTabs">
                                            <li role="presentation" class="">
                                                <a href="#orderListWaitingTab" class="c-b-trans"
                                                   aria-controls="orderListWaitingTab" role="tab" data-toggle="tab">
                                                    <p class="font30 mb0"><strong id="orderListWaitingCount"></strong>
                                                    </p>

                                                    <p class="text-uppercase mb0"> Waiting Orders </p>
                                                </a>
                                            </li>
                                            <li role="presentation" class="">
                                                <a href="#orderListRunningTab" class="c-b-trans"
                                                   aria-controls="orderListRunningTab" role="tab" data-toggle="tab">
                                                    <p class="font30 mb0"><strong id="orderListRunningCount"></strong>
                                                    </p>

                                                    <p class="text-uppercase mb0"> Running Orders </p>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content " id="ordersTabs">
                                            <div role="tabpanel" class="tab-pane fade  "
                                                 id="orderListWaitingTab">
                                                <div class="col-xs-12 p15"> <%--<ul id="orderListWaiting" class="list-unstyled clearfix "> </ul>--%>
                                                    <div id="orderListWaiting"> <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs nav-justified nav-pills" role="tablist"
                                                            id="waitingOrdersTabs">
                                                            <li role="presentation" class="tab-green ">
                                                                <a href="#orderListWaitingTab-orderPlaced" role="tab"
                                                                   data-toggle="tab" data-status="ORDER_PLACED"
                                                                   class="mr5">
                                                                    <strong id="orderPlacedCount"
                                                                            class="font18"></strong><br><span
                                                                        class="font13">Order
                                                                    Placed</span>
                                                                </a>
                                                            </li>
                                                            <li role="presentation" class="tab-orange">
                                                                <a href="#orderListWaitingTab-orderStarted"
                                                                   role="tab" data-toggle="tab"
                                                                   data-status="ORDER_STARTED">
                                                                    <strong id="orderStartedCount"
                                                                            class="font18"></strong><Br><span
                                                                        class="font13">Order
                                                                    Started</span>
                                                                </a>
                                                            </li>
                                                        </ul> <!-- Tab panes -->
                                                        <div class="tab-content mt15">
                                                            <div role="tabpanel" class="tab-pane fade"
                                                                 id="orderListWaitingTab-orderPlaced">
                                                                <ul class="list-unstyled clearfix"
                                                                    data-type="ORDER_PLACED">
                                                                        <li class="flex-parent order-list-item-new positionrelative opacity20">
                                                                            <div class="flex-child w20p">
                                                                                <div class="text-center time-elapsed-wrapper order-list-error">
                                                                                    <span class="font24 c-b-body ph5"></span>
                                                                                    <br>
                                                                                    <span class="font12 c-b-body ph30 w100p"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="flex-child w80p">
                                                                                <div class="flex-parent ">
                                                                                    <div class="flex-child w10p">
                                                                                        <div class="ico text-center">
                                                                                            <i class="aoicon aoicon-store"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="flex-child w90p">
                                                                                        <div class="font18 c-b-gray-light w100p p5"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="flex-parent c-t-blue-sky">
                                                                                    <div class="flex-child w10p">
                                                                                        <div class="ico text-center">
                                                                                            <i class="fa fa-map-marker"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="flex-child w90p">
                                                                                        <div class="c-b-blue-light w100p p5"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                </ul>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade"
                                                                 id="orderListWaitingTab-orderStarted">
                                                                <ul class="list-unstyled clearfix"
                                                                    data-type="ORDER_STARTED"></ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="orderListRunningTab">
                                                <div class="col-xs-12 p15">
                                                    <div id="orderListRunning"> <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs nav-justified nav-pills" role="tablist"
                                                            id="runningOrdersTabs">
                                                            <li role="presentation" class="tab-green ">
                                                                <a href="#orderListRunningTab-inRouteToPickUp"
                                                                   role="tab" data-toggle="tab"
                                                                   data-status="EN_ROUTE_TO_PICK_UP">
                                                                    <strong id="inRouteTopickUpCount"></strong><br><span
                                                                        class="font13">Enroute
                                                                    to Pickup</span>
                                                                </a>
                                                            </li>
                                                            <li role="presentation" class="tab-orange">
                                                                <a href="#orderListRunningTab-atStore" role="tab"
                                                                   data-toggle="tab" data-status="AT_STORE" class="mh5">
                                                                    <strong id="atStoreCount"></strong><Br><span
                                                                        class="font13">At Store</span>
                                                                </a>
                                                            </li>
                                                            <li role="presentation" class="tab-red">
                                                                <a href="#orderListRunningTab-inRouteToDelivery"
                                                                   role="tab"
                                                                   data-toggle="tab" data-status="EN_ROUTE_TO_DELIVER">
                                                                    <strong id="inRouteToDeliveryCount"></strong><br>
                                                                    <span class="font13">Enroute to Delivery</span>
                                                                </a>
                                                            </li>
                                                        </ul> <!-- Tab panes -->
                                                        <div class="tab-content mt15">
                                                            <div role="tabpanel" class="tab-pane fade"
                                                                 id="orderListRunningTab-inRouteToPickUp">
                                                                <ul class="list-unstyled clearfix"
                                                                    data-type="EN_ROUTE_TO_PICK_UP"></ul>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade"
                                                                 id="orderListRunningTab-atStore">
                                                                <ul class="list-unstyled clearfix"
                                                                    data-type="AT_STORE"></ul>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade"
                                                                 id="orderListRunningTab-inRouteToDelivery">
                                                                <ul class="list-unstyled clearfix"
                                                                    data-type="EN_ROUTE_TO_DELIVER"></ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--<ul id="orderListRunning" class="list-unstyled clearfix "></ul>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="pastOrderWrapper">
                                        <ul class="nav nav-pills nav-justified mb10" role="tablist">
                                            <li role="presentation" class="">
                                                <a href="#" class="c-b-trans" aria-controls="orderListPastTab"
                                                   role="tab" data-toggle="tab" data-status="DELIVERED">
                                                    <p class="font30 mb0">
                                                        <strong class="counterHere"></strong>
                                                    </p>

                                                    <p class="text-uppercase mb0"> Successful Orders </p>
                                                </a>
                                            </li>
                                        </ul>
                                        <ul class="ordersHere p15" data-type="DELIVERED">
                                            <li class="flex-parent order-list-item-new positionrelative opacity20">
                                                <div class="flex-child w20p">
                                                    <div class="text-center time-elapsed-wrapper order-list-error">
                                                        <span class="font24 c-b-body ph5"></span>
                                                        <br>
                                                        <span class="font12 c-b-body ph30 w100p"></span>
                                                    </div>
                                                </div>
                                                <div class="flex-child w80p">
                                                    <div class="flex-parent ">
                                                        <div class="flex-child w10p">
                                                            <div class="ico text-center">
                                                                <i class="aoicon aoicon-store"></i>
                                                            </div>
                                                        </div>
                                                        <div class="flex-child w90p">
                                                            <div class="font18 c-b-gray-light w100p p5"></div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-parent c-t-blue-sky">
                                                        <div class="flex-child w10p">
                                                            <div class="ico text-center">
                                                                <i class="fa fa-map-marker"></i>
                                                            </div>
                                                        </div>
                                                        <div class="flex-child w90p">
                                                            <div class="c-b-blue-light w100p p5"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="cancelledOrderWrapper">
                                        <ul class="nav nav-pills nav-justified mb10" role="tablist">
                                            <li role="presentation" class="">
                                                <a href="#" class="c-b-trans" aria-controls="orderListCancelledTab"
                                                   role="tab" data-toggle="tab" data-status="CANCELLED">
                                                    <p class="font30 mb0">
                                                        <strong class="counterHere"></strong>
                                                    </p>

                                                    <p class="text-uppercase mb0"> Cancelled Orders </p>
                                                </a>
                                            </li>
                                        </ul>
                                        <ul class="ordersHere p15" data-type="CANCELLED">
                                            <li class="flex-parent order-list-item-new positionrelative opacity20">
                                                <div class="flex-child w20p">
                                                    <div class="text-center time-elapsed-wrapper order-list-error">
                                                        <span class="font24 c-b-body ph5"></span>
                                                        <br>
                                                        <span class="font12 c-b-body ph30 w100p"></span>
                                                    </div>
                                                </div>
                                                <div class="flex-child w80p">
                                                    <div class="flex-parent ">
                                                        <div class="flex-child w10p">
                                                            <div class="ico text-center">
                                                                <i class="aoicon aoicon-store"></i>
                                                            </div>
                                                        </div>
                                                        <div class="flex-child w90p">
                                                            <div class="font18 c-b-gray-light w100p p5"></div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-parent c-t-blue-sky">
                                                        <div class="flex-child w10p">
                                                            <div class="ico text-center">
                                                                <i class="fa fa-map-marker"></i>
                                                            </div>
                                                        </div>
                                                        <div class="flex-child w90p">
                                                            <div class="c-b-blue-light w100p p5"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="searchOrderWrapper">
                                        <ul class="nav nav-pills nav-justified mb10" role="tablist">
                                            <li role="presentation" class="active">
                                                <button class="btn positionabsolute right0 z9" id="closeSearchBtn">X
                                                </button>
                                                <a href="#" class="c-b-trans" aria-controls="orderListSearchTab"
                                                   role="tab" data-toggle="tab">
                                                    <p class="font30 mb0">
                                                        <strong class="counterHere"></strong>
                                                    </p>

                                                    <p class="text-uppercase mb0"> Search Results </p>
                                                </a>
                                            </li>
                                        </ul>
                                        <ul class="ordersHere p15" data-type="searchResults">
                                            <li class="flex-parent order-list-item-new positionrelative opacity20">
                                                <div class="flex-child w20p">
                                                    <div class="text-center time-elapsed-wrapper order-list-error">
                                                        <span class="font24 c-b-body ph5"></span>
                                                        <br>
                                                        <span class="font12 c-b-body ph30 w100p"></span>
                                                    </div>
                                                </div>
                                                <div class="flex-child w80p">
                                                    <div class="flex-parent ">
                                                        <div class="flex-child w10p">
                                                            <div class="ico text-center">
                                                                <i class="aoicon aoicon-store"></i>
                                                            </div>
                                                        </div>
                                                        <div class="flex-child w90p">
                                                            <div class="font18 c-b-gray-light w100p p5"></div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-parent c-t-blue-sky">
                                                        <div class="flex-child w10p">
                                                            <div class="ico text-center">
                                                                <i class="fa fa-map-marker"></i>
                                                            </div>
                                                        </div>
                                                        <div class="flex-child w90p">
                                                            <div class="c-b-blue-light w100p p5"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div> <!-- end of Order List -->
                            </div>
                        </div> <!-- end of Orders col -->
                        <div id="col-maps" class="col-lg-6 col-md-4 p0">
                            <div class="content-header c-b-active c-t-white clearfix text-center font16 m0 pv10">
                                <p class="col-xs-12 m0 p0">
                                    <a class="cursorpointer pull-left" id="showOrderList">
                                        <i class="fa fa-fw fa-lg fa-chevron-right c-t-white mr10"
                                           aria-hidden="true"></i>
                                    </a>
                                    Currently Serving
                                    <a class="cursorpointer pull-right" id="showDriverList">
                                        <i class="fa fa-fw fa-lg fa-chevron-left c-t-white mr10" aria-hidden="true"></i>
                                    </a>
                                </p>
                            </div>
                            <div class="content-body clearfix"> <%--<div id="map" class="w100p h500"></div>--%>
                                <div id="googleMapsWrapper" class="">
                                    <input id="pac-input"
                                           class="controls form-control mt15 p15 border0 h50 driver-search-map"
                                           type="text" placeholder="Search Driver">

                                    <div class="pr20 ml10 font14" id="mapToggles" style="background: white">
                                        <div class="checkbox">
                                            <input id="storesCheckbox" type="checkbox" checked
                                                   class="with-font form-control">
                                            <label for="storesCheckbox" class="control-label fontnormal">Stores</label>
                                        </div>
                                        <div class="checkbox">
                                            <input id="driversCheckbox" type="checkbox" checked
                                                   class="with-font form-control">
                                            <label for="driversCheckbox"
                                                   class="control-label fontnormal">Drivers</label>
                                        </div>

                                    </div>

                                    <div id="map" class="w100p h100p"></div>
                                </div>

                                <div id="googleMapsWrapperRoute" class="hidden">
                                    <button class="positionabsolute z999 p10" style="right: 5px;margin-top: 5px;"
                                            id="closeLinkedMapsBtn">
                                    </button>
                                    <div id="map2" class="w100p h100p"></div>
                                </div>

                            </div>
                        </div>
                        <div id="col-driverslist" class="col-lg-3 col-md-4 p0">
                            <div class="content-header c-b-active c-t-white clearfix text-center font16 m0 pv10">
                                <p class="col-xs-12 m0 p0"> Drivers </p>
                            </div>
                            <div class="content-body clearfix">
                                <div class="row m0">
                                    <div class="col-xs-12 c-b-gray-light p0 p15">
                                        <div class="pull-left">
                                            <a class="cursorpointer" id="hideDriverList">
                                                <i class="fa fa-fw fa-lg fa-chevron-right c-t-gray-dark"
                                                   aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <div class="pull-right">
                                            <a class="cursorpointer" id="refreshDriversListBtn">
                                                <i class="fa fa-fw fa-lg fa-refresh c-t-gray-dark"
                                                   aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div> <!-- end of Driver Filter -->
                                <div class="row m0">
                                    <div> <!-- Nav tabs -->
                                        <ul class="nav nav-pills nav-justified mb10" role="tablist"
                                            id="driverTabsWrapper">
                                            <li role="presentation" class="active">
                                                <a href="#driverListDutyOnTab" class="c-b-trans"
                                                   aria-controls="driverListDutyOnTab" role="tab" data-toggle="tab">
                                                    <p class="font30 mb0"><strong id="driverListDutyOnCount"></strong></p>

                                                    <p class="text-uppercase mb0"> On Duty </p>
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#driverListDutyOffTab" class="c-b-trans"
                                                   aria-controls="driverListDutyOffTab" role="tab" data-toggle="tab">
                                                    <p class="font30 mb0"><strong id="driverListDutyOffCount"></strong></p>

                                                    <p class="text-uppercase mb0"> Off Duty </p>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="driverTabContent">
                                            <div role="tabpanel" class="tab-pane fade in active"
                                                 id="driverListDutyOnTab">
                                                <div class="col-xs-12 p15">
                                                    <ul id="driverListDutyOnWrapper"
                                                        class="list-unstyled clearfix overflowauto"
                                                        data-type="AVAILABLE">
                                                            <li class="driver-list-item clearfix mv10 pv15 positionrelative text-center flex-grid">
                                                                <div class="col-md-6 col-sm-12 text-left flex-gridcell flex-center flex-justify-start">
                                                                    <p class="c-b-gray-light p15 w100p"><strong> <a href="#"></a><br><span class="rating"></span> </strong></p>
                                                                </div>
                                                                <div class="col-md-3 col-sm-6 col-xs-12 flex-gridcell flex-center flex-col">
                                                                    <p> <strong class="c-b-gray-light ph35"> </strong>
                                                                    <br> <small class="text-uppercase"> Waiting Time</small> </p>
                                                                </div>
                                                                <div class="col-md-3 col-sm-6 col-xs-12 flex-gridcell flex-center">
                                                                    <a href="#" class="btn">
                                                                        <strong class="c-b-gray-light font30 ph30"> </strong> 
                                                                        <br>
                                                                        <small class="text-uppercase"> Orders</small> 
                                                                    </a> 
                                                                </div>
                                                                <div class="col-xs-12 text-left"> Location Update: <strong class="c-b-gray-light ph55 ml5"></strong> </div>
                                                            </li>
                                                        </ul>
                                                </div>
                                            </div> <!-- end of driver list duty on tab -->
                                            <div role="tabpanel" class="tab-pane fade" id="driverListDutyOffTab">
                                                <div class="col-xs-12 p15">
                                                    <ul id="driverListDutyOffWrapper"
                                                        class="list-unstyled clearfix overflowauto "
                                                        data-type="UNAVAILABLE"></ul>
                                                </div>
                                            </div> <!-- end of driver list duty off -->
                                        </div>
                                    </div>
                                </div> <!-- end of Drivers List -->
                            </div>
                        </div> <!-- end of content main -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade order-detail-modal" id="orderDetailModal" tabindex="-1" role="dialog" data-orderid="">
    <div class="modal-dialog model-lg w1000">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Order Details</h4></div>
            <div class="modal-body p0">
                <div class="row">
                    <div class="col-sm-6 pr0">
                        <div class="positionrelative">
                            <div class="w100p" id="modalMap"></div>
                            <label class="order-label text-uppercase fontnormal positionabsolute displayblock"
                                   style="top: 0; right: 0;">order id <span id="orderId">-</span></label></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="store-from flex-parent">
                            <div class="flex-col w75 text-center"><i class="aoicon aoicon-store fa-2x"></i></div>
                            <div class="flex-col store-detail"><p class="name mb0 fontbold"></p>

                                <p class="address mb0 text-muted"></p>

                                <p class="contact mb0 text-muted"></p></div>
                        </div>
                        <div class="deliver-to flex-parent">
                            <div class="flex-col w75 text-center"><i class="aoicon aoicon-marker fa-2x"></i></div>
                            <div class="flex-col user-detail"><p class="mb0 fontbold"> Delivery Place </p>

                                <p class="name mb0 text-muted fontbold"></p>

                                <p class="address mb0 text-muted"></p>

                                <p class="contact mb0 text-muted"></p></div>
                        </div>
                        <div class="datetime flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-clock-o fa-2x"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Requested Delivery Time </p>

                                <p class="datetimeval text-muted mb0"></p></div>
                        </div>
                        <div class="hidden estdatetime flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-clock-o fa-2x"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Estimated Delivery Time </p>

                                <p class="estdatetimeval text-muted mb0"></p></div>
                        </div>
                        <div class="deliveryCharge flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-2x fa-money"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Delivery Charge </p>

                                <p class="deliverfeeval text-muted mb0"></p></div>
                        </div>
                        <div class="info flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-info-circle fa-2x"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Order Status </p>

                                <p class="info-status text-muted mb0"></p></div>
                        </div>
                        <div class="paymentmode flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-credit-card fa-2x"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Payment </p>

                                <p class="paymentmodeval text-muted mb0"></p></div>
                        </div>
                        <div class="totalBillAmt flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-2x fa-money"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Total Bill Amount </p>

                                <p class="totalfeeval text-muted mb0"></p></div>
                        </div>
                        <div class="orderdescription flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-comment-o fa-2x"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Store Order Id</p>

                                <p class="orderdescriptionval text-muted mb0"></p></div>
                        </div>
                        <div class="comment flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-comment-o fa-2x"></i></div>
                            <div class="flex-col"><p class="mb0 fontbold"> Note to Driver </p>

                                <p class="commentval text-muted mb0"></p></div>
                        </div>
                        <div class="deliver-driver flex-parent">
                            <div class="flex-col w75 text-center"><i class="fa fa-motorcycle fa-2x"></i></div>
                            <div class="flex-col" style="width: 84%;"><a class="pull-right mr15" href="#"
                                                                         data-type="reAssign" id="reAssignDriver"
                                                                         data-toggle="modal"
                                                                         data-target="#driverAssignModal">Re-Assign</a>

                                <p class="mb0 fontbold"> Driver </p>

                                <p class="driver-name text-muted mb0"></p></div>
                        </div>
                        <div class="deliver-recipient flex-parent hidden">
                            <div class="flex-col w75 text-center"><i class="fa fa-user fa-2x"></i></div>
                            <div class="flex-col" style="width: 84%;"><a href="#" class="pull-right mr15"
                                                                         id="viewSignature">View Signature</a>

                                <p class="mb0 fontbold"> Recipient </p>

                                <p class="recipient-name text-muted mb0"></p>

                                <p class="signature-image mb0 hidden"><img src="" alt="Recipient Image" height="200px"
                                                                           width="161px"></p></div>
                        </div>
                        <div class="mv15 modal-buttons">
                            <button class="pull-right btn btn-success mr15" id="cancelOrderBtn" data-toggle="modal"
                                    data-target="#cancelOrderModel">Cancel Order
                            </button>
                            <button class="pull-right btn btn-success mr15" id="assignDriverBtn" data-toggle="modal"
                                    data-target="#driverAssignModal" data-dismiss="modal">Assign Driver
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade " id="driverAssignModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg w1000">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Assign Order</h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div id="anyOrderDriverBox">
                            <div class="box-me pt10">
                                <div class="ph15 ">
                                    <h4>Select <%=Constant.SUPPORT_NAME%> driver</h4>
                                    <input type="search" id="anyOrderDriverSearch" class="form-control mt15 ph15 js-driver-search" placeholder="<%=Constant.SUPPORT_NAME%> Driver Name" data-type="anyOrder">
                                    <div id="anyOrderDriverWrapper" class="mb15 pv15 h500 overflowauto"></div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-xs-6">
                                    <button class="btn btn-danger btn-lg btn-block" id="cancelAnyOrderDriverBtn">
                                        Cancel
                                    </button>
                                </div>
                                <div class="col-xs-6">
                                    <button class="btn btn-success btn-lg btn-block" id="confirmAnyOrderDriverBtn"
                                            disabled="disabled">Confirm Driver
                                    </button>
                                </div>
                            </div> -->
                        </div>
                        <!-- <div style="height: 570px;" class="w100p" id="mapCanvasModal"></div> -->
                        <button class="btn btn-success btn-lg btn-block hidden" id="bookAnyOrderDriverBtn">Book <%=Constant.SUPPORT_NAME%>
                            Driver
                        </button>
                        <button class="btn btn-success btn-lg btn-block" id="confirmAnyOrderDriverBtn" disabled="disabled">Book <%=Constant.SUPPORT_NAME%> Driver </button>
                    </div>
                    <div class="col-md-6" id="hideForCSR">
                        <form action="" method="POST" id="assignOwnDriverForm">
                            <div class="box-me pt10">
                                <div class="ph15 ">
                                    <h4>Select Store driver</h4>
                                    <input type="search" id="storeDriverSearch" class="form-control mt15 ph15 js-driver-search" placeholder="Store Driver Name" data-type="store">
                                    <div id="ownDriverWrapper" class="mb15 pv15 h500 overflowauto"></div>
                                </div>
                            </div>
                            <button class="btn btn-success btn-lg btn-block" id="assignOwnDriverBtn">Assign Own Driver
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="cancelOrderModel" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancel Order</h4></div>
            <div class="modal-body">
                <form id="cancelOrderForm" action="" method="post">
                    <div class="form-group"><label for="reasonsFor">Reason for cancelling order.</label> <textarea
                            name="reasonsFor" id="reasonsFor" class="form-control" rows="5"></textarea></div>
                    <div class="form-group">
                        <button class="btn btn-lg btn-danger pull-right " type="button" data-dismiss="modal">Cancel
                        </button>
                        <button class="btn btn-lg btn-success pull-right mr15" type="submit">Confirm</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="driverCurrentOrderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="order-current-driver"></span>Order in Progress</h4></div>
            <div class="modal-body p0">
                <div id="driverCurrentOrderList"></div>
            </div>
        </div>
    </div>
</div>

<script>
    var fromSocket = false;
</script>

<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/validate.jsp" %>
<%--<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>--%>
<script src="${pageContext.request.contextPath}/resources/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjbtflnGL0mEj7aHh9VOHPAa_0cqbJabY&libraries=places"
        async defer></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/maps.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/order_maps.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/order_list.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
<%--<script src="${pageContext.request.contextPath}/resources/custom/js/websocket.js"></script>--%>

<script>
    $(function () {
        // Hide Sidebar
        $("li[data-menu='orders']").addClass('active');
        $('.navbar-ao-dashboard a[data-toggle="menu"]').trigger('click');

        // Tab
        $('[data-toggle="tab"]').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        // Navigation
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        $('.nav a').click(function () {
            $(this).tab('show');
            window.location.hash = this.hash;
        });

        $('#sidebar').find('.nav-sidebar-list').find('a').on('click', function (event) {
            event.preventDefault();
            var link = $(this)[0].href;
            if (link.indexOf('orderListWaitingTab-orderPlaced') >= 0) {
                $('a[href="#orderListWaitingTab"]').click();
                $('a[href="#orderListWaitingTab-orderPlaced"]').click();
            } else if (link.indexOf('orderListWaitingTab-orderStarted') >= 0) {
                $('a[href="#orderListWaitingTab"]').click();
                $('a[href="#orderListWaitingTab-orderStarted"]').click();
            }

        });


        orderListModule.init();
    });


</script>


</body>
</html>

