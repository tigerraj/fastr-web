<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>
<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Billing Statements</h3>
                        </div>

                        <div class="content-body box-me">
                            <div class="table-ao-wrapper">
                                <table id="listBillsTable" class="table table-ao w100p">
                                    <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Merchant Name</th>
                                        <th>Statement Period</th>
                                        <th>Rate Plan Type</th>
                                        <th colspan="2" class="text-center">Order Charge</th>
                                        <th colspan="2" class="text-center">Extra Order Charge</th>
                                        <th colspan="2" class="text-center">Outside Range</th>
                                        <th>Other Charges</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>No. of Orders</th>
                                        <th>Amount</th>
                                        <th>No. of Orders</th>
                                        <th>Amount</th>
                                        <th>No. of Orders</th>
                                        <th>Amount</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade " id="statementModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Statement</h4>
            </div>
            <div class="modal-body">
                <form id="statementModalForm" data-id="" novalidate>


                    <div class="form-group">
                        <label for="merchantName">Merchant Name</label>
                        <input type="text" id="merchantName" disabled class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="statementPeriod">Statement Period</label>
                        <input type="text" id="statementPeriod" disabled class="form-control">
                    </div>

                    <h4>Monthly Plan/ Distance Based Order Charge</h4>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="dbOrders">No. of Orders</label>
                                <input type="number" id="dborders" name="dborders" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="dbAmount">Amount</label>
                                <input type="number" id="dbAmount" name="dbAmount" class="form-control" min="0">
                            </div>
                        </div>
                    </div>


                    <h4>Extra Order Charge</h4>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="ecOrders">No. of Orders</label>
                                <input type="number" id="ecOrders" name="ecOrders" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="ecAmount">Amount</label>
                                <input type="number" id="ecAmount" name="ecAmount" class="form-control" min="0">
                            </div>
                        </div>
                    </div>


                    <h4>Outside Range</h4>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="orOrders">No. of Orders</label>
                                <input type="number" id="orOrders" name="orOrders" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="orAmount">Amount</label>
                                <input type="number" id="orAmount" name="orAmount" class="form-control" min="0">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="otherCharges">Other Charges</label>
                        <input type="number" id="otherCharges" name="otherCharges" class="form-control" min="0">
                    </div>


                    <h4>Total Amount</h4>

                    <div class="form-group">
                        <input type="text" id="totalAmount" class="form-control" disabled>
                    </div>

                    <div class="clearfix"></div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success mt10">Save Changes</button>
                        <a class="btn btn-success mt10 generateStatementBtn" href="#" data-dismiss="modal">Generate
                            Statement</a>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/datatables.jsp" %>
<%@include file="../includes/validate.jsp" %>

<script src="${pageContext.request.contextPath}/resources/custom/js/billing.min.js"></script>

<script>
    $(function () {
        billingModule.list();
        $("li[data-menu='billing']").addClass('active');
    });
</script>

</body>
</html>