<%@ page import="com.yetistep.anyorder.abs.Constant" %>
<%@include file="includes/header.jsp" %>
<%@include file="includes/menu_primary.jsp" %>
<div class="container pt25 center-me positionabsolute">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-1 separator">
            <div class="text-center pv50">
                <p><i class="fa fa-key fa-4x" aria-hidden="true"></i></p>

                <p class="font24 c-t-light">Welcome to <strong class="c-t-dark"><%=Constant.SUPPORT_NAME%> Support</strong><br>Sign In for
                    all<br>Delivery management
                </p>
            </div>
        </div>

        <div class="col-sm-5 c-t-dark separator-fx-div pt15 pb30">
            <form action="" class="" method="post" id="signInForm" novalidate>
                <p class="font28 mb30">Sign in or <a href="${pageContext.request.contextPath}/signup">Register</a></p>

                <div class="form-group floating">
                    <label class="control-label">Email</label>
                    <input type="email" class="form-control input-lg" placeholder="" name="email" id="email" autofocus required>
                </div>
                <div class="form-group floating mt25">
                    <label class="control-label">Password</label>

                    <div class="input-group ">
                        <input type="password" class="form-control input-lg c-b-trans" placeholder="" name="password"
                               id="password"
                               minlength="6" required>
                        <span class="input-group-addon cursorpointer" id="blinkPassword" type="button"><i
                                class="fa fa-eye"></i></span>

                    </div>

                </div>

                <div class="clearfix">
                    <a href="#" data-toggle="modal" data-target="#emailModal" class="mr15" data-type="FORGOT">Forgot Password.</a>
                    <a href="#" data-toggle="modal" data-target="#emailModal" data-type="RESEND">Resend Link.</a>
                    <button class="btn btn-success c-b-success pull-right btn-lg">Sign In</button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade " id="emailModal" tabindex="-1" role="dialog" data-type>
    <div class="modal-dialog modal-sm w400">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <form id="emailForm">
                    <%--<p class="help-block mb0">Please enter your email to recover password.</p>--%>
                    <div class="input-group ao-input-group">
                        <input type="email" class="form-control border0" placeholder="Email" name="emailRecover" id="emailRecover">
                    </div>


                    <div class="clearfix"></div>
                    <button type="submit" class="btn btn-success btn-block btn-lg mt10" id="recoverSubmitBtn">Recover Password</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<%@include file="includes/scripts.jsp" %>
<%@include file="includes/validate.jsp" %>

<script src="${pageContext.request.contextPath}/resources/custom/js/login.min.js"></script>
<script type="text/javascript">
    $(function() {
        loginModule.init();
        setTimeout(function () {
            if ($('#email').val()) {
                $('#email').blur().focus();
            }
        }, 1000);
    });
</script>
</body>
</html>

