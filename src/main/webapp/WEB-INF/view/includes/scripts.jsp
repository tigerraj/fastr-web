<%--express order modal--%>
<!-- <div class="modal fade " id="expressModal" tabindex="-1" role="dialog">
    <div class="modal-dialog w1000">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Express Booking</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="expressForm">
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Select Store</div>
                                        <div class="panel-body h150 overflowauto">
                                            <div id="expressStoreList"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel panel-danger">
                                        <div class="panel-heading">Main Area</div>
                                        <div class="panel-body h150 overflowauto">
                                            <div id="expressMainArea"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">Sub Area</div>
                                        <div class="panel-body h150 overflowauto">
                                            <div id="expressSubArea"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button class="">Go</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> -->

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/vendors/jquery/dist/jquery.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/custom/js/Main.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/admin.min.js"></script>

<!-- <script>
    $(function(){
       expressOrderModule.init();
    });
</script> -->


<script>

    $(function () {

        // Loader
        $('body').addClass('loader_div clearfix').append('<div class="loader"></div>');

        // Form
        $('.form-group.floating .form-control').on('focus blur', function (e) {
            $(this).parents('.form-group.floating').toggleClass('focused', (e.type === 'focus' || this.value.length > 0 ));
        }).trigger('blur');

        $('.nav-sidebar').children('li').tooltip({container: 'body'}).tooltip('disable');

        // Sidebar
        $('[data-toggle="menu"]').click(function () {
            if ($(this).attr('data-status') == "true") {
                $(this).attr('data-status', 'false');
                $('#sidebar').attr('data-menu', 'sm');
                $('#content').attr('data-content', 'lg');
                $('.nav-sidebar').children('li').tooltip('enable');
                // $('#aoLogo').attr('src', $('#aoLogo').attr('src').replace('logo-lg.png', 'logo-sm.png'));
                if (!$('li[data-menu="orders"]').hasClass('closed')) {
                    $('.nav-sidebar-list-clone').addClass('move-right');
                }
            } else {
                $(this).attr('data-status', 'true');
                $('#sidebar').attr('data-menu', 'lg');
                $('#content').attr('data-content', 'sm');
                $('.nav-sidebar').children('li').tooltip('disable');
                $('.nav-sidebar-list-clone').removeClass('move-right');
                //$('#aoLogo').attr('src', $('#aoLogo').attr('src').replace('logo-sm.png', 'logo-lg.png'));
            }
            Main.saveInLocalStorage('isMenuLarge', $(this).attr('data-status'));
            $(window).trigger('resize');
        });

        var isMenuLargeStorage = Main.getFromLocalStorage('isMenuLarge');
        var isMenuLargeView = $('[data-toggle="menu"]').attr('data-status');

        if (isMenuLargeStorage !== isMenuLargeView) {
            $('[data-toggle="menu"]').click();
        }

        $('li[data-menu="orders"]').on('click', function (event) {
            event.stopPropagation();
            $(this).toggleClass('closed').blur();

            if ($(this).closest('#sidebar').attr('data-menu') === 'sm') {
                if (!$('.nav-sidebar-list-clone').length) {
                    var topValue = $(this).position().top + 2;
                    var leftValue = $('#sidebar').width();
                    var elem = $(this).find('.nav-sidebar-list').clone();
                    elem.removeClass('nav-sidebar-list').addClass('nav-sidebar-list-clone');
                    elem.css({
                        position: 'absolute',
                        top: topValue + 'px',
                        left: leftValue,
                    });
                    $('body').append(elem);
                }
                if ($(this).hasClass('closed')) {
                    $('.nav-sidebar-list-clone').removeClass('move-right');
                } else {
                    $('.nav-sidebar-list-clone').addClass('move-right');
                }
            }
        });

        $('.collapse').on('hidden.bs.collapse', function () {
            $(this).parent().find('.nav-collapse .fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        });

        $('.collapse').on('shown.bs.collapse', function () {
            $(this).parent().find('.nav-collapse .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        });

        // Notification List
        if (Main.userRole === "ROLE_MERCHANT") {
            if ($('#notificationTogglr').length) {
                adminModule.notification();
                $('#notificationTogglr').on('click', function (event) {
                    event.preventDefault();
                    $('#notificationListSidebar').toggleClass('in');
                });
            }
        }

        // Modal
        $('.modal').on('hidden.bs.modal', function (e) {
            $('.modal-backdrop.in').remove();
        });

        // User
        if ($('#userRoleTop').length) {
            $('#userRoleTop').text(Main.ucfirst(Main.getFromLocalStorage('userRole').replace('ROLE_', '').replace('_', ' ')));
        }

        if (Main.getFromLocalStorage('userRole') === "ROLE_MERCHANT") {
            $('#userRoleTop').text(Main.getFromLocalStorage('businessName'));
        }

        if ($('.dataTable').length) {
        	$(window).resize();
        }

        adminModule.orderCount();

        $('body').removeClass('loader_div clearfix');
        $('.loader').remove();
        $('[data-toggle="tooltip"]').tooltip();

    });

	function colorAndScroll (e) {
        var searchBar = $(e.target);
        var searchKey = searchBar.val();
        var wrapper = $('#servingAreaWrapper');
        if (searchKey === '') {
            wrapper.find('span.c-t-success').removeClass('c-t-success');
            wrapper.parent().animate({
                scrollTop: 0
            }, 500);
            return;
        }
        $.each(wrapper.find('div.s-area-box-header span.fontbold.font16'), function (index, data) {
            var that = $(this);
            var text = $(this).html();
            if (text.toLowerCase().indexOf(searchKey.toLowerCase()) > -1 && text != "Select All") {
                that.addClass('c-t-success');
                wrapper.parent().animate({
                    scrollTop: (that.parents().eq(2).position().top)
                }, 500);
                return false;
            }
        });
        var modal = $('#servingAreaModal');
        if (modal.length) {
            modal.one('hidden.bs.modal', function() {
                if (searchBar.val().length) {
                    searchBar.val('').trigger('keyup');
                    return;
                }
            });
        }
    }


</script>