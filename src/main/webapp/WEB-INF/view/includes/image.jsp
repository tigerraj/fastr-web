<!-- Modal -->
<div class="modal fade" id="picCropModal" tabindex="-1" role="dialog" aria-labelledby="picCropModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="picCropModalLabel">Crop Image</h4>
            </div>
            <div class="modal-body">

                <div id="cropWrapper" class="positionrelative">
                    <button class="btn btn-success positionabsolute" id="btnSetImage">Set Image</button>
                </div>

            </div>
            <%-- <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 <button type="button" class="btn btn-primary">Save changes</button>
             </div>--%>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/vendors/Croppie/croppie.css">
<script src="${pageContext.request.contextPath}/resources/vendors/Croppie/croppie.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/imagecropper.min.js"></script>
<script>
    $(function(){
        imageCropperModule.init();
    });

</script>
