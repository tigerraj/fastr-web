<%@ page import="com.yetistep.anyorder.abs.Constant" %>
<div class="sidebar" id="sidebar" data-menu="lg">
    <div class="text-center pt15 pb50 ph15">
        <img id="aoLogo" src="${pageContext.request.contextPath}/resources/custom/images/logo-lg.png" alt=<%=Constant.APP_NAME%>" Logo" class="img-responsive center-block w120">
    </div>

    <ul class="nav nav-stacked nav-sidebar">


        <sec:authorize access="hasRole('ROLE_MERCHANT')">
            <li data-menu="stores" data-toggle="tooltip" title="Stores"><a href="#"
                                      onclick="window.location.href = ('/merchant/store_list?merchantId='+Main.getFromLocalStorage('merchantId'))"><i
                    class="fa fa-bank"></i> <span>Stores </span></a></li>
        </sec:authorize>

        <sec:authorize access="hasAnyRole('ROLE_ADMIN, ROLE_MANAGER')">
            <li data-menu="merchants" data-toggle="tooltip" title="Merchants"><a href="${pageContext.request.contextPath}/merchant/merchant_list"><i
                    class="fa fa-users"></i> <span>Merchants </span></a></li>
        </sec:authorize>

        <sec:authorize access="hasAnyRole('ROLE_ADMIN, ROLE_MANAGER')">
            <!-- <li data-menu="dashboard" data-toggle="tooltip" title="Dashboard"><a href="${pageContext.request.contextPath}/"><i class="fa fa-dashboard"></i>
<span>            Dashboard </span></a></li> -->

            <li data-menu="stores" data-toggle="tooltip" title="Stores"><a href="${pageContext.request.contextPath}/merchant/store_list">
                <i class="fa fa-bank"></i> <span>Stores </span></a></li>
        </sec:authorize>


        <li data-menu="orders" data-toggle="tooltip" title="Orders" class="closed">
            <div>
                <a href="#">
                    <i class="fa fa-file-text"></i>
                    <span>Orders </span>
                </a>
                <div>
                    <ul class="list-unstyled nav-sidebar-list">
                        <li> <a href="${pageContext.request.contextPath}/smanager/order_list?order=current#orderListWaitingTab">Waiting<span class="js-order-count-waiting label label-primary">0</span> </a> </li>
                        <li> <a href="${pageContext.request.contextPath}/smanager/order_list?order=current#orderListRunningTab">Running<span class="js-order-count-running label label-warning">0</span> </a> </li>
                        <li> <a href="${pageContext.request.contextPath}/smanager/order_list?order=past">Delivered<span class="js-order-count-delivered label label-success">0</span> </a> </li>
                        <li> <a href="${pageContext.request.contextPath}/smanager/order_list?order=cancelled">Cancelled<span class="js-order-count-cancelled label label-danger">0</span> </a> </li>
                    </ul>
                </div>
            </div>
        </li>

        <sec:authorize access="hasAnyRole('ROLE_MERCHANT, ROLE_CSR, ROLE_MANAGER, ROLE_ADMIN, ROLE_STORE_MANAGER')">
            <li data-menu="drivers" data-toggle="tooltip" title="Drivers"><a href="${pageContext.request.contextPath}/smanager/driver_list"><i
                    class="fa fa-motorcycle"></i> <span>Drivers </span></a></li>
        </sec:authorize>

        <sec:authorize access="hasAnyRole('ROLE_ADMIN, ROLE_MANAGER')">
            <li data-menu="area" data-toggle="tooltip" title="Area"><a
                    href="${pageContext.request.contextPath}/csr/area_list"><i
                    class="fa fa-map-marker"></i> <span>Area </span></a></li>
        </sec:authorize>
        <li data-menu="reports" data-toggle="tooltip" title="Reports"><a
                href="${pageContext.request.contextPath}/smanager/reports">
            <i class="fa fa-file"></i> <span>Reports </span></a></li>
        <li data-menu="billing" data-toggle="tooltip" title="Billing"><a
                href="${pageContext.request.contextPath}/smanager/billing"><i
                class="fa fa-list-alt"></i> <span>Billing </span></a></li>

        <sec:authorize access="hasAnyRole('ROLE_ADMIN, ROLE_MANAGER')">
            <li data-menu="notification" data-toggle="tooltip" title="Send Notification"><a href="${pageContext.request.contextPath}/organizer/send_notification"><i
                    class="fa fa-paper-plane"></i> <span>Send Notification</span></a></li>

            <!-- <li data-menu="reports" data-toggle="tooltip" title="Reports"><a href="${pageContext.request.contextPath}/organizer/reports"><i
                    class="fa fa-paper-plane"></i> <span>Reports</span></a></li> -->
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_MERCHANT')">
            <li data-menu="api" data-toggle="tooltip" title="API"><a href="${pageContext.request.contextPath}/merchant/api"><i
                    class="fa fa-plug"></i> <span> API </span></a></li>
        </sec:authorize>
    </ul>
</div>
<!-- end of Sidebar -->

<aside id="notificationListSidebar" class="notification-list-container c-t-white pb30">
    <h3 class="m20"> Notifications </h3>

    <div id="notificationList">
        <p class="m20"> No notifications available </p>
    </div>
</aside>
<!-- end of notification list sidebar -->