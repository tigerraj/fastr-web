<%@ page import="com.yetistep.anyorder.abs.Constant" %>

<nav class="navbar-ao-dashboard navbar navbar-default c-b-dash-menu borderradius0 border0 mb0 h85">
    <div class="navbar-header h85">
        <ul class="nav navbar-nav navbar-left mr10 h85">
            <li>
                <a href="#" data-toggle="menu" data-status="true" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-2x fa-navicon"></i>
                </a>
            </li>
        </ul>
        <div class="navbar-brand text-uppercase">
            <strong> <%=Constant.SUPPORT_NAME%> Dispatch </strong> <br>
            <small> Order Management System</small>
        </div>
    </div> <!-- end of Nav Header -->
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav h85 count-list">
            <li class="dropdown h85 pv20 border0 mr10">
                <button type="button" class="dropdown-toggle btn mt2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                <ul id="countRange" class="dropdown-menu">
                    <li><a href="#">All</a></li>
                    <li><a href="#">Today</a></li>
                    <li><a href="#">Yesterday</a></li>
                    <li><a href="#">Current Month</a></li>
                </ul>
            </li>
            <li class="count-block m0">
                <p class="text-uppercase m0">Total Orders</p>
                <strong id="countTotalOrder" class="c-t-dark"></strong>
            </li>
            <li class="count-block">
                <p class="text-uppercase m0">Unassigned Orders</p>
                <strong id="countUnassignedOrder" class="c-t-error"></strong>
            </li>
            <li class="count-block">
                <p class="text-uppercase m0">Assigned Orders</p>
                <strong id="countAssignedOrder" class="c-t-gray-dark"></strong>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right h85">
            <%-- <li>
                 <a href="#" data-target="#expressModal" data-toggle="modal">
                     Express
                 </a>
             </li>--%>
            <li>
                <a href="${pageContext.request.contextPath}/smanager/order_form">
                    <i class="aoicon aoicon-order fa-2x"></i> <span class="visible-xs">Create Order</span>
                </a>
            </li>
            <sec:authorize access="hasRole('ROLE_MERCHANT')">
                <li><a href="#" id="notificationTogglr"><i class="fa fa-2x fa-bell"></i>

                    <p id="notificationCount" class="badge mt20neg ml15neg c-b-badge"></p></a></li>

            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li><a href="${pageContext.request.contextPath}/admin/preferences"><i
                        class="fa fa-2x fa-cog"></i> <span
                        class="visible-xs">Preferences</span></a></li>
            </sec:authorize>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown"><span id="userRoleTop"></span> <span
                        class="fa fa-caret-down"></span></a>
                <ul class="dropdown-menu">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="${pageContext.request.contextPath}/organizer/user_list"> User Management </a>
                        </li>
                        <!--<li><a href="#">Change Password</a></li>-->
                    </sec:authorize>

                    <sec:authorize access="hasRole('ROLE_MERCHANT')">
                        <li><a href="${pageContext.request.contextPath}/merchant/profile">Profile</a></li>
                    </sec:authorize>
                    <li><a href="#" onclick="Main.doLogout()">Sign Out</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

<%--
<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
        <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a href="#">HTML</a></li>
        <li><a href="#">CSS</a></li>
        <li><a href="#">JavaScript</a></li>
    </ul>
</div>--%>
