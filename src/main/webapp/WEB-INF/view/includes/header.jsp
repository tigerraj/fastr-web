<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><%=Constant.TITLE%> Delivery Services</title>
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/custom/images/fav-ico.png">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css">
    <%--<link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">--%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/vendors/font-awesome/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/helpers/helpers.min.css">

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/custom/css/styles.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/custom/css/aoicon.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/custom/css/CustomRadioCheck.min.css">
    <%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/custom/css/styles.css.map">--%>

    <script>
        var pageContext = "${pageContext.request.contextPath}/";
    </script>

    <script>

    </script>

</head>
<body>
<div id="pageContext" data-context="${pageContext.request.contextPath}"> </div>
