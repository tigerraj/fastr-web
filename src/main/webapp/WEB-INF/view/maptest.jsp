<%@include file="includes/header.jsp" %>
<%@include file="includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content clearfix">
                    <div class="content-nav clearfix">
                        <%@include file="includes/menu_content.jsp" %>
                    </div>
                    <div class="content-main">
                        <br>

                        <div id="map_canvas" style="width:700px; height:500px; margin-left:80px;"></div>
                        <button onclick="newMap();">Load Another Map</button>
                        <br>

                        <div id="map_canvas2" style="width:700px; height:500px; margin-left:80px;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="includes/scripts.jsp" %>
<script src="${pageContext.request.contextPath}/resources/custom/js/maps.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjbtflnGL0mEj7aHh9VOHPAa_0cqbJabY&callback=initGoogleMaps&libraries=places"
        async defer></script>

<script>
    var map1, myOptions1;
    var map2, myOptions2;
    function initGoogleMaps() {
        myOptions1 = {
            zoom: 8,
            center: new google.maps.LatLng(28.3949, 84.1240),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapCanvas: "map_canvas",
            mapVar: map1,
        };

        soloMarkersModule.init(myOptions1);

        var markers1 = [
            {
                lat: 28.3949,
                lng: 82.1240,
                type: "Store",
                title: "XYZ Store<Br>Order No:123",
                icon: "http://www.luxpopuli.com/es/wp-content/uploads/2015/10/map-marker.png"
            },
            {
                lat: 28.3949,
                lng: 83.1240,
                type: "Store",
                title: "ABC Store<Br>Order No:369",
                icon: "https://camo.githubusercontent.com/e8783c1b9fc99532bedbab3b8df9ce1b03d6f70b/68747470733a2f2f7777772e6272696172636c6966662e6564752f6d656469612f3339343033372f6d61726b65722e706e67"
            }
        ];

        soloMarkersModule.setMarkers(markers1);
    }

    function newMap() {
        if (typeof(google) == "undefined") {
            console.log('Waiting for maps to load...');
            setTimeout(function () {
                newMap();
            }, 500)

            return;
        }

        console.log('Maps loaded...');


        myOptions2 = {
            zoom: 6,
            //center: new google.maps.LatLng(28.3949, 84.1240),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapCanvas: "map_canvas2",
            mapVar: map2,
        };

        linkedMarkersModule.init(myOptions2);

        var markers2 = [
            {
                driver: {
                    lat: 26.3800,
                    lng: 82.1200,
                    type: "Driver",
                    title: "Driver Name<Br>driver@mail.com",
                    icon: "https://camo.githubusercontent.com/e8783c1b9fc99532bedbab3b8df9ce1b03d6f70b/68747470733a2f2f7777772e6272696172636c6966662e6564752f6d656469612f3339343033372f6d61726b65722e706e67"
                },
                store: {
                    lat: 27.3949,
                    lng: 83.1240,
                    type: "Store",
                    title: "ABC Store<Br>store@mail.com",
                    icon: "http://www.luxpopuli.com/es/wp-content/uploads/2015/10/map-marker.png"
                },
                customer: {
                    lat: 28.4500,
                    lng: 84.1388,
                    type: "Customer",
                    title: "Customer Name<Br>customer@mail.com",
                    icon: "http://4.imimg.com/data4/GK/EJ/MY-2/free-map-marker-icon-green-darker-250x250.png"
                }

            },

            {
                driver: {
                    lat: 23.3800,
                    lng: 82.1200,
                    type: "Driver",
                    title: "Driver Name<Br>driver@mail.com",
                    icon: "https://camo.githubusercontent.com/e8783c1b9fc99532bedbab3b8df9ce1b03d6f70b/68747470733a2f2f7777772e6272696172636c6966662e6564752f6d656469612f3339343033372f6d61726b65722e706e67"
                },
                store: {
                    lat: 24.3949,
                    lng: 83.1240,
                    type: "Store",
                    title: "ABC Store<Br>store@mail.com",
                    icon: "http://www.luxpopuli.com/es/wp-content/uploads/2015/10/map-marker.png"
                },
                customer: {
                    lat: 28.4500,
                    lng: 82.1388,
                    type: "Customer",
                    title: "Customer Name<Br>customer@mail.com",
                    icon: "http://4.imimg.com/data4/GK/EJ/MY-2/free-map-marker-icon-green-darker-250x250.png"
                }

            },

        ];

        linkedMarkersModule.setMarkers(markers2);
    }


</script>

</body>
</html>

