<%@ page import="com.yetistep.anyorder.abs.Constant" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><%=Constant.TITLE%> Delivery Services</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/vendors/font-awesome/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/helpers/helpers.scss">

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/custom/css/styles.css">

    <!-- jQuery -->
    <script src="${pageContext.request.contextPath}/resources/vendors/jquery/dist/jquery.min.js"></script>

</head>
<body>
<nav class="navbar navbar-default navbar-ao c-b-menu">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-menu-ao"
                    aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="${pageContext.request.contextPath}/resources/custom/images/logo.png" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-menu-ao">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Toll Free: 012 256 458</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container pt25">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-1 separator">
            <div class="text-center pv50">
                <p class="font24 c-t-light">Don’t worry & just fill your email<br>and we’ll help you<br><strong class="c-t-dark">Reset Your Password</strong></p>
            </div>
        </div>

        <div class="col-sm-5 c-t-dark separator-fx-div pv30">
            <form action="" class="" method="post" id="forgotPWForm" novalidate>
                <p class="font28 ">Forgot Password</p>

                <div class="form-group">
                    <input type="email" class="form-control input-lg" placeholder="Email" name="nm_email" required>
                </div>


                <div class="form-group clearfix">
                    <button class="btn btn-success c-b-success pull-right btn-lg">Reset Password</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/jquery-validation-bootstrap-tooltip/jquery-validate.bootstrap-tooltip.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $('#forgotPWForm').validate({
        submitHandler: function () {
            alert("form valid");
        }
    });
</script>
</body>
</html>