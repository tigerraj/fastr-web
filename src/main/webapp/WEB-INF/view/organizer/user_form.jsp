<%@ page import="com.yetistep.anyorder.enums.UserRole" %>
<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Add User</h3>
                        </div>

                        <div class="content-body box-me">
                            <form action="" id="addUserForm" novalidate>
                                <h4>Basic Information</h4>

                                <div class="row mt25">
                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <div class="control-label">First Name</div>
                                            <input type="text" class="form-control input-lg" id="firstName"
                                                   name="firstName">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <div class="control-label">Last Name</div>
                                            <input type="text" class="form-control input-lg" id="lastName"
                                                   name="lastName">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt25">
                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <div class="control-label">Mobile Number</div>
                                            <input type="text" class="form-control input-lg" id="mobileNumber"
                                                   name="mobileNumber">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <div class="control-label">Email</div>
                                            <input type="email" class="form-control input-lg" id="email" name="email">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Role</h4>

                                        <div class="displayinline">
                                            <input id="cod" type="radio" name="userRole" class="with-button"
                                                   checked value="<%=UserRole.ROLE_MANAGER%>"/>
                                            <label for="cod">Manager</label>
                                        </div>

                                        <div class="displayinline mh25">
                                            <input id="creditCard" type="radio" name="userRole"
                                                   class="with-button" value="<%=UserRole.ROLE_CSR%>"/>
                                            <label for="creditCard">CSR</label>
                                        </div>


                                    </div>
                                </div>

                                <hr class="hr-mod">
                                <div class="clearfix">
                                    <button class="btn btn-danger pull-right btn-lg" type="button" onclick="parent.history.back();">Cancel</button>
                                    <button class="btn btn-success c-b-success pull-right btn-lg mh15" type="submit">Add User</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>

<%@include file="../includes/validate.jsp" %>


<script src="${pageContext.request.contextPath}/resources/custom/js/users.min.js"></script>

<script>
    $("li[data-menu='users']").addClass('active');
    $(function () {
        addEditUserModule.init();
    });

</script>

</body>
</html>

