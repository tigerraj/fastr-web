<%@ page import="com.sun.org.glassfish.external.statistics.Stats" %>
<%@ page import="com.yetistep.anyorder.enums.Status" %>
<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <%--<h3 class="content-title">Rate List</h3>--%>
                        </div>

                        <div class="content-body box-me pv25">
                            <div class="row">
                                <div class="col-md-6 hidden" id="knownAreaServingRatePlanWrapper">
                                    <a class="btn btn-success pull-right "id="addRatePlanWBtn">
                                        <%--data-target="#WServiceAreaModal"
                                        data-toggle="modal" href=""--%>
                                        <i class="fa fa-plus"></i> Rate Plan</a>
                                    <h4 class="title-border">Rate Plan Within Service Area</h4>

                                    <div class="clearfix"></div>
                                    <div id="WServiceArea"></div>
                                </div>

                                <div class="col-md-6" id="distanceBasedKnownServingAreaRatePlanWrapper">
                                    <a class="btn btn-success pull-right "id="addDBSARPBtn">
                                        <%--data-target="#WServiceAreaModal"
                                        data-toggle="modal" href=""--%>
                                        <i class="fa fa-plus"></i> Rate Plan</a>
                                    <h4 class="title-border">Rate Plan Within Service Area <small>(Distance Based)</small></h4>

                                    <div class="clearfix"></div>
                                    <div id="DBServiceArea"></div>
                                </div>



                                <div class="col-md-6">
                                    <div class="">
                                        <a class="btn btn-success pull-right mt10neg" data-toggle="modal"
                                           data-target="#WOServiceAreaModal" href=""><i
                                                class="fa fa-plus" id="addRatePlanWOBtn"></i> Rate Plan</a>
                                        <h4 class="title-border">Rate Plan Out of Service Area</h4>

                                        <div class="clearfix"></div>

                                        <div id="WOServiceArea"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="WServiceAreaModal" tabindex="-1" role="dialog" data-type="WSA" >
    <div class="modal-dialog modal-sm w400">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Rate Plan</h4>
            </div>
            <div class="modal-body">
                <form id="WSAForm">
                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control border0" placeholder="Name"
                               aria-describedby="charge-per-order" name="rateName" id="rateName">
                    </div>

                    <div class="ao-form-inline">
                        <label>Max Order Counts</label>

                        <div class="pull-right">
                            <%--<input type="text" class="mr10">
                            <span>to</span>--%>
                            <input type="text" class="ml10" name="maxOrderCounts" id="maxOrderCounts">
                        </div>
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control" placeholder="Charges"
                               aria-describedby="charge-per-month" name="chargePerMonth" id="chargePerMonth">
                        <span class="input-group-addon" id="charge-per-month">Per Month</span>
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control" placeholder="Extra Charges"
                               aria-describedby="charge-per-order" name="chargePerOrder" id="chargePerOrder">
                        <span class="input-group-addon" id="charge-per-order">Per Order</span>
                    </div>

                   <%-- <div class="form-group floating">
                        <select name="WSADuration" id="WSADuration"
                                class="form-control input-lg select-picker">
                            <option value="" selected disabled>Duration</option>
                            <option value="WEEKLY">Weekly</option>
                            <option value="MONTHLY">Monthly</option>
                            <option value="YEARLY">Yearly</option>
                        </select>
                    </div>--%>

                    <div class="form-group floating" id="WSAStatusWrapper">
                        <select name="WSAstatus" id="WSAstatus"
                                class="form-control input-lg select-picker" data-title="Status">
                            <option value="<%=Status.ACTIVE%>">Active</option>
                            <option value="<%=Status.INACTIVE%>">Inactive</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <button type="submit" class="btn btn-success btn-block btn-lg mt10">Add</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade " id="WOServiceAreaModal" tabindex="-1" role="dialog" data-type="WOSA" >
    <div class="modal-dialog modal-sm w400">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Rate Plan</h4>
            </div>
            <div class="modal-body">
                <form id="WOSAForm">
                    <div class="ao-form-inline">
                        <label>Serving Range</label>

                        <div class="pull-right">
                            <input type="text" class="mr10" name="servingRangeFrom" id="servingRangeFrom">
                            <span>to</span>
                            <input type="text" class="ml10" name="servingRangeTo" id="servingRangeTo">
                        </div>
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control border0" placeholder="Base Fare" name="baseFare"
                               id="baseFare">
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control" placeholder="Extra Charges"
                               aria-describedby="charge-per-KM" id="chargePerKM" name="chargePerKM">
                        <span class="input-group-addon" id="charge-per-KM">Per KM</span>
                    </div>

                    <div class="form-group floating" id="WOSAstatusWrapper">
                        <select name="WOSAstatus" id="WOSAstatus"
                                class="form-control input-lg select-picker" data-title="Status">
                            <option value="ACTIVE">Active</option>
                            <option value="INACTIVE">Inactive</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <button type="submit" class="btn btn-success btn-block btn-lg mt10">Add</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade " id="distanceBasedRPModal" tabindex="-1" role="dialog" data-type="WOSA" >
    <div class="modal-dialog modal-sm w400">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Rate Plan</h4>
            </div>
            <div class="modal-body">
                <form id="distanceBasedRPForm">
                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control border0" placeholder="Name"
                               aria-describedby="charge-per-order" name="rateName" id="distanceBasedRPrateName">
                    </div>


                    <div class="ao-form-inline">
                        <label>Serving Range</label>
                        <div class="pull-right">
                            <input type="text" class="mr10" name="servingRangeFrom" id="distanceBasedRPservingRangeFrom">
                            <span>to</span>
                            <input type="text" class="ml10" name="servingRangeTo" id="distanceBasedRPservingRangeTo">
                        </div>
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control border0" placeholder="Base Fare" name="baseFare"
                               id="distanceBasedRPbaseFare">
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control" placeholder="Extra Charges"
                               aria-describedby="charge-per-KM" id="distanceBasedRPchargePerKM" name="chargePerKM">
                        <span class="input-group-addon" id="distanceBasedRPcharge-per-KM">Per KM</span>
                    </div>

                    <div class="form-group floating hidden" id="distanceBasedRPstatusWrapper">
                        <select name="WOSAstatus" id="distanceBasedRPstatus"
                                class="form-control input-lg select-picker" data-title="Status">
                            <option value="ACTIVE">Active</option>
                            <option value="INACTIVE">Inactive</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <button type="submit" class="btn btn-success btn-block btn-lg mt10">Add</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/validate.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<script src="${pageContext.request.contextPath}/resources/custom/js/rate.min.js"></script>

<script>

    $(function () {
        var storeId = Main.getNamedParameter('storeId');
        ratesModule.init();
    });
</script>

<sec:authorize access="hasAnyRole('ROLE_ADMIN, ROLE_MANAGER')">
    <script>

    </script>
</sec:authorize>


</body>
</html>

