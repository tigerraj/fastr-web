<%@ page import="com.yetistep.anyorder.enums.DriverType" %>
<%@ page import="com.yetistep.anyorder.enums.NotifyTo" %>
<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Send Notification</h3>
                        </div>
                        <div class="content-body">
                            <div class="row mt25">
                                <div class="col-md-5">

                                    <form id="sendNotificationForm" class="clearfix box-me pv25 ph10" method="POST" action="">
                                        <div class="form-group clearfix">
                                            <div class="col-md-4">
                                                <label for="notifyApp" class="control-label"> Platform </label>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="displayinline mr15">
                                                    <input id="notifyApp" type="radio"
                                                           name="notificationPlatform"
                                                           class="with-font form-control"
                                                           value="notifyApp" data-status="false"/>
                                                    <label for="notifyApp" class="control-label">App</label>
                                                </div>
                                                <div class="displayinline">
                                                    <input id="notifyWeb" type="radio"
                                                           name="notificationPlatform"
                                                           class="with-font form-control" value="notifyWeb" data-status="false"/>
                                                    <label for="notifyWeb" class="control-label">Web</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="notifyAppSection" class="form-group clearfix">
                                            <div class="col-md-4">
                                                <label for="notifyToStoreManager" class="control-label"> To </label>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="displayinline mr15">
                                                    <input id="notifyToStoreManager" type="checkbox"
                                                           name="notifyTo" class="with-font form-control"
                                                           value="<%=NotifyTo.STORE_MANAGER%>"/>
                                                    <label for="notifyToStoreManager" class="control-label">Store
                                                        Manager</label>
                                                </div>

                                                <div class="displayinline mr15">
                                                    <input id="notifyToAnyOrder" type="checkbox" name="notifyTo"
                                                           class="with-font form-control"
                                                           value="<%=NotifyTo.FASTR_DRIVER%>"/>
                                                    <label for="notifyToAnyOrder" class="control-label"><%=Constant.APP_NAME%> Driver</label>
                                                </div>

                                                <div class="displayinline">
                                                    <input id="notifyToRestaurant" type="checkbox"
                                                           name="notifyTo" class="with-font form-control"
                                                           value="<%=NotifyTo.RESTAURANT_DRIVER%>"/>
                                                    <label for="notifyToRestaurant" class="control-label">Restaurant
                                                        Driver</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="notifyWebSection" class="form-group clearfix hidden">
                                            <div class="col-md-4">
                                                <label for="notifyToMerchant" class="control-label"> To </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select name="notifyTo" id="notifyToMerchant"
                                                        data-value="MERCHANT"
                                                        class="form-control select-picker inline-select"
                                                        data-actions-box="true" multiple>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-md-4">
                                                <label for="notificationValidity" class="control-label"> Valid
                                                    Till </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="notificationValidity" id="notificationValidity"
                                                       class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-md-4">
                                                <label for="notificationMessage" class="control-label"> Message </label>
                                            </div>
                                            <div class="col-md-8">
                                                <textarea name="notificationMessage" id="notificationMessage"
                                                          class="form-control"
                                                          placeholder="Enter notification message"> </textarea>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-success pull-right mt15"> Send
                                                </button>
                                            </div>
                                        </div>
                                    </form> <!-- end of send notification form -->
                                </div> <!-- end of box me -->
                                <div class="visible-sm visible-xs mt20"></div>
                                <div class="col-md-7">
                                    <section id="notificationHistory" class="box-me p20">
                                        No notifications !
                                    </section> <!-- end of Notification History -->
                                </div>
                            </div> <!-- end of row -->
                        </div> <!-- end of content body -->
                    </div>
                </div> <!-- end of content -->
            </div>
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div>
<!-- end of content wrapper -->
<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<%@include file="../includes/validate.jsp" %>
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<script src="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/custom/js/send_notification.min.js"></script>

<script type="text/javascript">
    $(function () {
        sendNotificationModule.init();
        $("li[data-menu='notification']").addClass('active');
    });
</script>

</body>
</html>
