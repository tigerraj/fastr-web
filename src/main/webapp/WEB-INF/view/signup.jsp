<%@ page import="com.yetistep.anyorder.abs.Constant" %>
<%@include file="includes/header.jsp" %>
<%@include file="includes/menu_primary.jsp" %>

<div class="container pt25 ">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-1 separator">
            <div class="text-center pv150">
                <p><i class="fa fa-user-plus fa-4x" aria-hidden="true"></i></p>

                <p class="font24 c-t-light">Welcome to <strong class="c-t-dark"><%=Constant.SUPPORT_NAME%> Support</strong><br>Sign Up for
                    all<br>Delivery management
                </p>
            </div>
        </div>

        <div class="col-sm-5 c-t-dark separator-fx-div">
            <form action="" class="" method="post" id="registrationForm" novalidate>
                <p class="font28 mb15">Registration</p>

                <div class="form-group floating">
                    <label class="control-label">Business Title</label>
                    <input type="text" class="form-control input-lg" placeholder=""
                           name="businessTitle" id="businessTitle" required>
                </div>
                <div class="form-group floating">
                    <label class="control-label">First Name</label>
                    <input type="text" class="form-control input-lg" placeholder="" name="firstName" id="firstName"
                           required>
                </div>
                <div class="form-group floating">
                    <label class="control-label">Last Name</label>
                    <input type="text" class="form-control input-lg" placeholder="" name="lastName" id="lastName"
                           required>
                </div>
                <div class="form-group floating">
                    <label class="control-label">Email</label>
                    <input type="email" class="form-control input-lg"
                           placeholder="This email should be used later to login." name="email" id="email" required>
                </div>
                <div class="form-group floating">
                    <label class="control-label">Password</label>
                    <input type="password" class="form-control input-lg" placeholder="" name="password" id="password"
                           minlength="6" required>
                </div>
                <div class="form-group floating">
                    <label class="control-label">Contact Number</label>
                    <input type="text" class="form-control input-lg" placeholder="" data-mask="9999999999"
                           name="contact" id="contact"
                           required>
                </div>

                <div class="clearfix">
                    <button class="btn btn-success c-b-success pull-right btn-lg" id="btnNext">Register</button>
                </div>
            </form>
            <form action="" method="post" id="paymentForm" novalidate class="pv30">
                <p class="font28 mb15">Payment Information</p>

                <div class="form-group floating">
                    <label class="control-label">Card Holder's Name</label>
                    <input type="text" class="form-control input-lg" placeholder=""
                           name="cardHolderName" id="cardHolderName">
                </div>
                <div class="form-group floating">
                    <label class="control-label">Card Number</label>
                    <input type="text" data-mask="9999-9999-9999-9999" class="form-control input-lg"
                           placeholder="" required name="cardNumber" id="cartNumber">
                </div>
                <div class="form-group floating">
                    <label class="control-label">CVC</label>
                    <input type="text" data-mask="999" class="form-control input-lg" placeholder="" required
                           name="cvc" id="cvc">
                </div>


                <div class="form-group floating">
                    <label class="control-label">Expiry</label>

                    <div class='input-group date mt10 ' data-action="datePicker" data-date-format='month-year'>

                        <input type='text' class="form-control input-lg c-b-trans" placeholder="" required
                               name="expiryDate" id="expirtyDate">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>

                <div class="">
                    <button class="btn btn-success c-b-success pull-right btn-lg">Register</button>
                </div>
            </form>

        </div>
    </div>
</div>


<%@include file="includes/scripts.jsp" %>
<%@include file="includes/validate.jsp" %>

<script src="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"> </script>
<script src="${pageContext.request.contextPath}/resources/custom/js/signup.min.js"></script>
<script type="text/javascript">

    signUpModule.init();

</script>
</body>
</html>