<%@ page import="com.yetistep.anyorder.abs.Constant" %>
<!DOCTYPE html>
<html class="customer-app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Order Status | <%=Constant.TITLE%> </title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/vendors/starability/css/starability.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/css/customer.css">
</head>
<body>
<div id="pageContext" data-context="${pageContext.request.contextPath}"></div>
<header class="c-b-brand">
    <div class="header center-block clearfix">
        <img src="${pageContext.request.contextPath}/resources/custom/images/logo.png"
             alt=<%=Constant.APP_NAME%>" Deliver Services Logo"
             class="img-responsive pull-left" height="25px">

        <h1 class="pull-right text-normal"> <%=Constant.APP_NAME%> Delivery Services </h1>
    </div>
</header> <!-- end of header -->
<main class="main-container center-block text-center">
    <div id="orderDetailSection" class="hidden">
        <section>
            <h2 class="text-normal"> Estimated Arrival </h2>
            <strong id="etd" class="stronger"> 15 Min </strong>
        </section>
        <section>
            <h2 class="text-uppercase text-normal"> Order Status </h2>
            <strong id="orderStatus" class="text-uppercase c-t-success"> Dispatched </strong>
        </section>
        <section>
            <h2 class="text-uppercase text-normal"> Store </h2>
            <strong id="storeName" class="text-uppercase"> Fire and Ice Pizza </strong>
            <br>
            <small id="storeAddress"> 125 Financial Road, UAE</small>
        </section>
        <section class="c-b-white c-t-grey pad-20 mrgn-20">
            <h2 class="text-normal text-left mrgn-t-0"> Driver Details </h2>

            <div id="driverInfoSection" class="media">
                <div class="media-left">
                    <img id="driverImage" src="" alt="" height="55px" width="55px">
                </div>
                <div class="media-body text-left">
                    <strong id="driverName"> Robbin Gorkhali </strong>
                    <br>
                    <small id="driverNumber"> 971 - 456657416</small>
                </div>
                <div class="media-right">
                    <a href="tel://971-456657416" id="callDriver" class="icon-call icon-rounded c-b-primary"></a>
                </div>
            </div>
        </section>
    </div> <!-- end of Order Detail Section -->
    <div id="orderRatingSection" class="hidden mrgn-20">
        <section class="mrgn-20">
            <h2><strong> Rate Your Experience </strong></h2>

            <form id="orderRatingForm" method="PUT" action="">
                <fieldset class="starability-growRotate center-block">
                    <input type="radio" id="rate5" class="rating-star" name="rating" value="5"/>
                    <label for="rate5" title="Amazing" class="rating-label">5 stars</label>
                    <input type="radio" id="rate4" class="rating-star" name="rating" value="4"/>
                    <label for="rate4" title="Very good" class="rating-label">4 stars</label>
                    <input type="radio" id="rate3" class="rating-star" name="rating" value="3"/>
                    <label for="rate3" title="Average" class="rating-label">3 stars</label>
                    <input type="radio" id="rate2" class="rating-star" name="rating" value="2"/>
                    <label for="rate2" title="Not good" class="rating-label">2 stars</label>
                    <input type="radio" id="rate1" class="rating-star" name="rating" value="1"/>
                    <label for="rate1" title="Terrible" class="rating-label">1 star</label>
                </fieldset>
                <fieldset class="center-block">
                    <label for="rateComment" class="text-left">
                        <small> Comment</small>
                    </label>
                    <textarea name="rateComment" id="rateComment" class="c-t-white" rows="7"
                              placeholder="We can improve our delivery service when you rate us"></textarea>
                </fieldset>
                <fieldset>
                    <button type="submit" role="button" class="btn c-b-primary c-t-white"> Rate Us</button>
                </fieldset>
            </form>
        </section>
        <section>
            <strong class="text-uppercase"> Thank You </strong>
        </section>
    </div> <!-- end of Order Rating Section -->
</main> <!-- end of Main Content -->
<script src="${pageContext.request.contextPath}/resources/vendors/jquery/dist/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/customer.min.js"></script>
<script>
    $(function () {
        customerModule.init();
    });
</script>
</body>
</html>
