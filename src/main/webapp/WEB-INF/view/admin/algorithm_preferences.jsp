<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Preferences</h3>
                        </div>

                        <div class="content-body box-me">
                            <section class="preferences-form ao-prefs  clearfix">

                                <div class="btn_tabs main_tabs">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="col-xs-6 p0 " role="presentation">
                                            <a href="/admin/preferences">General</a>
                                        </li>
                                        <li class="col-xs-6 p0 active" role="presentation">
                                            <a href="/admin/algorithm_preferences">Algorithm</a>
                                        </li>
                                    </ul>
                                </div>


                                <div class="main_content mt25">

                                    <div class="form_container form_editable form_section">
                                        <form id="form_settings" action="" method="POST" role="form"
                                              class="display_settings">

                                        </form>
                                    </div>
                                </div>

                            </section>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<div class="fileinput fileinput-new" data-provides="fileinput">
    <div class="fileinput-new thumbnail positionrelative"
         style="width: 200px; height: 150px;">
        <div class="center-me positionabsolute text-center">
            <i class="fa fa-image fa-4x"></i>
        </div>

    </div>
    <div class="fileinput-preview fileinput-exists thumbnail ImageContainer"
         style="max-width: 200px; max-height: 150px;" ></div>
    <div>
                    <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Select image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="imageInput" data-upload="image"
                               accept="image/*">
                    </span>
        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    </div>
</div>--%>

<div class="form_field_template hidden">
    <div class="form-group clearfix">
        <label class="col-lg-4 floated_label"></label>

        <div class="col-lg-8">
            <div class="form-control info_display none_editable"></div>
            <div class="info_edit editable hidden">
                <input type="text" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="form_select_template hidden">
    <div class="form-group clearfix">
        <label class="col-lg-4"></label>
        <div class="col-lg-8">
            <div class="form-control info_display none_editable"></div>
            <div class="info_edit editable hidden">
                <select class="col-xs-12 ph10 pv5 m0 form-control" data-style="form-control">
                </select>
            </div>
        </div>
    </div>
</div>

<div class="form_section_template hidden">
    <section class="form_group">
        <div class="form_head">


            <div class="detail_options pull-right">
                <a class="p0 btn edit_btn none_editable glyphicon glyphicon-edit"></a>

                <div class="action_buttons editable hidden">
                    <a class="p0 btn save_btn glyphicon glyphicon-floppy-disk"></a>
                    <a class="p0 btn cancel_btn glyphicon glyphicon-remove"></a>
                </div>
            </div>

            <span class="section_title"></span>
        </div>

        <div class="form_content">
            <div class="row-ao">
            </div>
        </div>
    </section>
</div>


<div class="image_template hidden">
    <div class="form-group clearfix">
        <label class="col-md-4"> </label>

        <div class="col-md-8">
            <%--<div class="image_container clearfix">
                <img src="" id="smanagerImageOutput" name="smanagerImageOutput"
                     aria-describedby="smanagerImageOutput"
                     class="js-store-logo-output c-b-pure pull-right" height="200px" width="200px"
                     data-type="Logo &#xa;" data-height="200"
                     data-width="200">
            </div>
            <input type="file" accept="image/*" id="smanagerImageInput" class="image_input invisible-full"
                   name="smanagerImageInput"
                   aria-describedby="smanagerImageInput">--%>



            <div class="">

                <div class="image_container">
                    <img src="" style="max-height: 140px;" id="smanagerImageOutput" name="smanagerImageOutput">
                </div>
                <div>
                    <span class="btn btn-default btn-file mt5">
                        <span class="fileinput-exists">Change</span>
                        <input type="file" data-upload="image"
                               accept="image/*" class="image_input"
                               aria-invalid="false">
                    </span>





                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>


<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<%@include file="../includes/image.jsp" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/js/preferences.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.main_tabs .active').click(function (e) {
            e.preventDefault();
        });
        Preferences.loadSettings(2);
        Preferences.loadEditSettings();
        //imageModule.bindCropComplete();
    });

    /*$(window).load(function () {
     $('.image_input').on('change', function () {
     alert(this.files[0].name);
     });
     });*/

</script>

</body>
</html>
