<%@ page import="com.yetistep.anyorder.abs.Constant" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><%=Constant.TITLE%> Delivery Services</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/vendors/font-awesome/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/helpers/helpers.scss">

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/custom/css/styles.css">

    <!-- jQuery -->
    <script src="${pageContext.request.contextPath}/resources/vendors/jquery/dist/jquery.min.js"></script>

</head>
<body>

<div id="pageContext" data-context=""></div>
<div class="container pt25">
    <div class="row">

        <div class="col-sm-4 col-sm-offset-4 c-t-dark pv30">
            <form action="" class="" method="post" id="forgotPWForm" novalidate>
                <p class="font20">Forgot Password</p>

                <div class="form-group">
                    <input type="email" class="form-control input-lg" placeholder="Email" name="email" id="email"
                           required>
                </div>


                <div class="form-group clearfix">
                    <button class="btn btn-success c-b-success btn-block btn-lg" id="submitBtn" type="submit">Reset
                        Password
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/validate.jsp" %>


<script src="${pageContext.request.contextPath}/resources/custom/js/login.min.js"></script>
<script type="text/javascript">

    $(function () {
        forgotPWMobileModule.init();
    });
</script>
</body>
</html>