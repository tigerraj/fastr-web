<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <%--<h3 class="content-title"></h3>--%>
                        </div>

                        <div class="content-body areas-list">
                            <div class="row">
                                <div class="col-md-3 root-areas">
                                    <a class="btn btn-success pull-right mt15" href="${pageContext.request.contextPath}/csr/area_form?type=addRoot">Add Main Area</a>
                                    <div class="clearfix"></div>
                                    <h3 class="content-title">Main Area</h3>

                                    <div class="root-area-wrapper">
                                        <ul id="rootAreaList" class="list-unstyled"></ul>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="sub-areas">
                                        <a class="btn btn-success pull-right mt15neg hide" id="addSubAreaBtn" href="">Add Sub Area</a>
                                        <h4 class="title-border">Sub Areas</h4>
                                        <div id="subAreasWrapper">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="box-me">
                                                        <ul class="list-unstyled" id="subAreaList"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>
<script src="${pageContext.request.contextPath}/resources/custom/js/area.min.js"></script>
<script>
    $("li[data-menu='area']").addClass('active');
</script>
<script>
    $(function(){
        listAreaModule.init();
    });
</script>

</body>
</html>

