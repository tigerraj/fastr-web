<%@ page import="com.yetistep.anyorder.enums.StorePriority" %>
<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <button class="btn btn-primary pull-right" id="editBtn" data-formtype="view">Edit</button>
                            <h3 class="content-title">Add Store</h3>
                        </div>

                        <div class="content-body box-me">
                            <form id="storeForm" method="POST" action="">
                                <legend>Store</legend>
                                <div class="row mb50">
                                    <div class="col-sm-2">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail positionrelative"
                                                 style="width: 200px; height: 150px;" data-trigger="fileinput">
                                                <div class="center-me positionabsolute text-center">
                                                    <i class="fa fa-image fa-4x"></i>

                                                    <p class="text-center">Store Logo</p>
                                                </div>

                                            </div>
                                            <div id="storeImageContainer"
                                                 class="fileinput-preview fileinput-exists thumbnail"
                                                 style="max-width: 200px; max-height: 150px;"></div>
                                            <div>
                                                        <span class="btn btn-default btn-file">
                                                            <span class="fileinput-new">Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="file" id="imgDriver" data-upload="image"
                                                                   accept="image/*">
                                                        </span>
                                                <a href="#" class="btn btn-default fileinput-exists"
                                                   data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">

                                        <div class="form-group floating mt15">
                                            <label for="storeName" class="control-label">Store Name</label>
                                            <input type="text" class="form-control input-lg"
                                                   id="storeName" name="storeName">
                                        </div>

                                        <div>
                                            <h5>Statement Generation will be done on monthly basis.</h5>

                                            <%-- <div class="displayinline">
                                                 <input id="weekly" type="radio" name="modeOfStatementGeneration"
                                                        class="with-button form-control"
                                                        value="WEEKLY"/>
                                                 <label for="weekly" class="control-label">Weekly</label>
                                             </div>

                                             <div class="displayinline mh25">
                                                 <input id="fortnightly" type="radio" name="modeOfStatementGeneration"
                                                        value="FORTNIGHTLY"
                                                        class="with-button form-control"/>
                                                 <label for="fortnightly" class="control-label">Fortnightly</label>
                                             </div>

                                             <div class="displayinline">
                                                 <input id="monthly" type="radio" name="modeOfStatementGeneration"
                                                        class="with-button form-control"
                                                        value="MONTHLY"/>
                                                 <label for="monthly" class="control-label">Monthly</label>
                                             </div>--%>
                                        </div>
                                    </div>
                                </div>

                                <legend>Store Locations</legend>
                                <div class="row">
                                    <div class="col-md-8">
                                        <input id="pac-input" class="controls form-control w400 mt15 p15 border0 h50"
                                               type="text"
                                               placeholder="Enter postal code to focus map and make marker">

                                        <div id="map" class="w100p h550"></div>

                                        <div class="clearfix mt15">
                                            <button class="btn" id="removeMarker" type="button">Remove Marker
                                            </button>
                                            <button class="btn marker_nav" id="prevMarker" type="button"
                                                    disabled="disabled">Prev Marker
                                            </button>
                                            <button class="btn marker_nav" id="nextMarker" type="button"
                                                    disabled="disabled">Next Marker
                                            </button>

                                            <small class="pull-right text-muted">
                                                Right click on store location marker to remove the store.
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="storeDetailsForm">


                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group floating">
                                                    <label for="contactNumber" class="control-label">Contact
                                                        Number</label>
                                                    <input type="text" class="form-control input-lg"
                                                           id="contactNumber" name="contactNumber">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group floating">
                                                    <%--<label for="priority" class="control-label">Priority</label>--%>
                                                    <select name="priority" id="priority"
                                                            class="form-control input-lg select-picker" data-title="Select Priority">
                                                        <option value="<%=StorePriority.PRIORITIZED%>">Prioritized</option>
                                                        <option value="<%=StorePriority.NON_PRIORITIZED%>">Non Prioritized</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group floating">
                                            <label for="streetAddress" class="control-label">Address Line 1</label>
                                            <input type="text" class="form-control input-lg"
                                                   id="streetAddress" name="streetAddress">
                                        </div>
                                        <div class="form-group floating mb15">
                                            <label for="streetAddressTwo" class="control-label">Address Line 2</label>
                                            <input type="text" class="form-control input-lg"
                                                   id="streetAddressTwo" name="streetAddressTwo">
                                        </div>

                                        <%--<input type="hidden" class="form-control input-lg"
                                               id="latitude" name="latitude">
                                        <input type="hidden" class="form-control input-lg"
                                               id="longitude" name="longitude">--%>


                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="mainArea" class="control-label-select">Main Area</label>
                                                <div class="form-group floating mb15">

                                                    <select name="mainArea" id="mainArea"
                                                            class="form-control input-lg select-picker"
                                                            data-title="Select Main Area">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="subArea" class="control-label-select">Sub Area</label>
                                                <div class="form-group floating mb15">
                                                    <select class="form-control input-lg select-picker"
                                                            id="subArea" name="subArea" data-title="Select Sub-Area">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <label for="isDistanceBaseRatePlan" class="control-label-select">Distance Base Rate Plan</label>
                                        <div class="form-group floating ">
                                            <select class="form-control input-lg select-picker"
                                                    id="isDistanceBaseRatePlan" name="isDistanceBaseRatePlan"
                                                    data-title="Distance Based Rate Plan">
                                                <option value="true">Yes</option>
                                                <option value="false">No</option>
                                            </select>
                                        </div>


                                        <div class="form-group mt20">
                                                <textarea class="form-control h140"
                                                          placeholder="Address Note" id="addressNote"
                                                          name="addressNote"></textarea>
                                        </div>


                                    </div>
                                </div>

                                <div class="row mt25">
                                    <div class="location-box-wrapper" id="locationBoxWrapper">

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">

                                    </div>
                                </div>


                                <hr class="hr-mod">
                                <div class="clearfix" id="formBtns">
                                    <button class="btn btn-danger pull-right btn-lg" type="button"
                                            onclick="window.history.back()">Cancel
                                    </button>
                                    <button class="btn btn-success c-b-success pull-right btn-lg mr15" id="submitBtn">
                                        Add Store
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/validate.jsp" %>
<%@include file="../includes/image.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<script src="${pageContext.request.contextPath}/resources/custom/js/maps.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/store.min.js"></script>

<script>
    var pageMode = Main.getNamedParameter('type');
    //var mapReadOnly;
    $(function () {
        /*if (pageMode == "add") {
         addStoreModule.init();
         mapReadOnly = false;
         } else if (pageMode == "view") {
         mapReadOnly = true;
         var storeId = Main.getNamedParameter('storeBrandId');
         viewEditStoreModule.init();
         }
         */

    });
</script>


<script>
    var superMap;
    var initGoogleMaps = function () {
        var myOptions1 = {
            zoom: 8,
            center: new google.maps.LatLng(25.18, 55.20),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapCanvas: "map",
            searchInput: "pac-input",
            mapVar: superMap,
            readOnly: false,
            isMultiMarker: true,
            disableDoubleClickZoom: true,
            markerIcon: {
                url: pageContext + "resources/custom/images/map-ico/map_merchant.png",
                scaledSize: new google.maps.Size(50, 50)
            }
        };

        superMap = newMapModule.init(myOptions1);

        addEditStoreModule.init()
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjbtflnGL0mEj7aHh9VOHPAa_0cqbJabY&callback=initGoogleMaps&libraries=places"
        async defer></script>

</body>
</html>

