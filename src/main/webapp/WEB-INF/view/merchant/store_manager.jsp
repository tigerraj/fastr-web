<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <a class="btn btn-primary pull-right"
                               href="#" data-target="#storeManagerModal" data-toggle="modal" id="addStoreBtn"><i class="fa fa-plus"></i> Store Manager</a>

                            <h3 class="content-title">Store Manager</h3>

                            <div class="clearfix"></div>
                        </div>


                        <div class="content-body">
                            <div class="row" id="storeManagerBoxWrapper">
                                <div class="col-sm-3">
                                    <div class="box-me p15">
                                        No Store Managers available.
                                        <a href="#" class="addStoreBtn" href="javascript;" data-target="#storeManagerModal" data-toggle="modal">Click here</a> to add new store manager.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade store-manager-modal" id="storeManagerModal" tabindex="-1" role="dialog" data-smanagerid="">
    <div class="modal-dialog modal-sm w400" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Store Manager</h4>
            </div>
            <div class="modal-body">
                <form action="" id="storeManagerForm">
                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control border0" placeholder="First Name" name="firstName"
                               id="firstName">
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="text" class="form-control border0" placeholder="Last Name" name="lastName"
                               id="lastName">
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="email" class="form-control border0" placeholder="Email" name="emailAddress"
                               id="emailAddress">
                    </div>

                    <div class="input-group ao-input-group">
                        <input type="number" class="form-control border0" placeholder="Contact Number" name="mobileNumber"
                               id="mobileNumber">
                    </div>

                    <div class="clearfix"></div>
                    <button type="submit" class="btn btn-success btn-block btn-lg mt10">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/validate.jsp" %>
<script>
    var merchantId = Main.getNamedParameter('merchantId');
    if (merchantId) {
        merchantId = merchantId.replace('#', '');
    } else {
        merchantId = Main.getFromLocalStorage('merchantId');
    }


</script>


<script src="${pageContext.request.contextPath}/resources/custom/js/smanager.min.js"></script>

<script>
    $(function () {
        sManagerModule.init();
    });
</script>

</body>
</html>

