<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">

                            <a class="btn btn-primary pull-right"
                               href="${pageContext.request.contextPath}/merchant/store_form" id="addStoreBtn">Add
                                Store
                            </a>

                            <h3 class="content-title">Stores
                                <button type="button" id="showStoreSearchInput" class="btn btn-lg p0 mt5neg c-b-trans" data-toggle="tooltip" title="Search Store"><i
                                        class="fa fa-search"></i></button>
                            </h3>

                            <div class="clearfix"></div>

                            <div class="store-search pb10 hidden">
                                <input type="search" class="form-control p10 search-trans" id="searchStoreInList" placeholder="Enter store name here...">
                            </div>
                        </div>


                        <div class="content-body">
                            <h4 class="mb15">Active Store Brands</h4>

                            <div class="row">
                                <div class="flex-grid" id="storeBoxWrapper">
                                    <div class="col-sm-3">
                                        <div class="box-me p15">
                                            No Active Stores Available.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mb15">Inactive Store Brands</h4>

                            <div class="row">
                                <div class="flex-grid" id="inactiveStores">
                                    <div class="col-sm-3 mb25">
                                        <div class="box-me p15">
                                            No Inactive Stores Available.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%-- <h4 class="mb15">Verified Stores</h4>

                             <div class="row">
                                 <div id="verifiedStores">
                                     <div class="col-sm-3 mb25">
                                         <div class="box-me p15">
                                             No Verified Stores Available.
                                         </div>
                                     </div>
                                 </div>
                             </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade store-location-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="clearfix"></div>
                <div class="text-center">
                    <img src="" alt="" class="brand-logo w200">
                    <h4 class="modal-title" id="myModalLabel"></h4>

                    <p class="fontbold store-count"></p>
                </div>
            </div>
            <div class="modal-body">
                <p class="body-title">Store Locations</p>

                <div class="row" id="storeLocationWrapper"></div>
            </div>
        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>
<script>
    var merchantId = Main.getNamedParameter('merchantId');
    if (merchantId) {
        merchantId = merchantId.replace('#', '');
    } else {
        merchantId = Main.getFromLocalStorage('merchantId');
    }


</script>


<script src="${pageContext.request.contextPath}/resources/custom/js/store.min.js"></script>

<script>
    $(function () {
        listStoreModule.init();
    });

</script>

<script>
    $("li[data-menu='stores']").addClass('active');
</script>


</body>
</html>

