<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">ABC Store</h3>
                        </div>

                        <div class="content-body">
                            <div class="row">
                                <div class="col-sm-6 mb40">
                                    <div class="box-me p25 store-info-box">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <img src="http://placehold.it/120x120&text=Logo" alt="Store Logo">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="c-t-light">
                                                    <p class="c-t-dark fontbold font18" data-type="infoLocation">Location 1</p>
                                                    <p data-type="infoStreetAddress">1053, Boudhadwar Marg</p>
                                                    <p data-type="infoStreetAddressTwo">Boudhha</p>
                                                    <p data-type="infoCityCountry">Kathmandu Nepal</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <p class="text-center mv50">
                                                    <a href="#">Add Serving Area</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 mb40">
                                    <div class="box-me p25 store-info-box">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <img src="http://placehold.it/120x120&text=Logo" alt="Store Logo">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="c-t-light">
                                                    <p class="c-t-dark fontbold font18" data-type="infoLocation">Location 2</p>
                                                    <p data-type="infoStreetAddress">1053, Boudhadwar Marg</p>
                                                    <p data-type="infoStreetAddressTwo">Boudhha</p>
                                                    <p data-type="infoCityCountry">Kathmandu Nepal</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <p class="text-center mv50">
                                                    <a href="#">Add Serving Area</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 mb40">
                                    <div class="box-me p25 store-info-box">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <img src="http://placehold.it/120x120&text=Logo" alt="Store Logo">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="c-t-light">
                                                    <p class="c-t-dark fontbold font18" data-type="infoLocation">Location 3</p>
                                                    <p data-type="infoStreetAddress">1053, Boudhadwar Marg</p>
                                                    <p data-type="infoStreetAddressTwo">Boudhha</p>
                                                    <p data-type="infoCityCountry">Kathmandu Nepal</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <p class="text-center mv50">
                                                    <a href="#">Add Serving Area</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="picCropModal" tabindex="-1" role="dialog" aria-labelledby="picCropModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="picCropModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">

                <div id="cropWrapper" class="positionrelative">
                    <button class="btn btn-success positionabsolute" id="btnSetImage">Set Image</button>
                </div>

            </div>
            <%-- <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 <button type="button" class="btn btn-primary">Save changes</button>
             </div>--%>
        </div>
    </div>
</div>

<%@include file="../includes/scripts.jsp" %>

<script src="${pageContext.request.contextPath}/resources/custom/js/maps.min.js"></script>
<script>
    var config = {
        multiMarker: true,
        countLabel: true,
        curPage: "add_store",
        markerIcon: "${pageContext.request.contextPath}/resources/custom/images/pin-store.png",
        curLocation: "Kathmandu Nepal",
        hasInput: true
    }

    var initGoogleMaps = function () {
        mapsModule.init(config);
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjbtflnGL0mEj7aHh9VOHPAa_0cqbJabY&callback=initGoogleMaps&libraries=places"
        async defer></script>


<link rel="stylesheet"
      href="${pageContext.request.contextPath}/resources/vendors/EasyAutocomplete/dist/easy-autocomplete.css">
<script src="${pageContext.request.contextPath}/resources/vendors/EasyAutocomplete/dist/jquery.easy-autocomplete.min.js"></script>
<script>
    var options = {
        data: ["9841445096", "123456789", "897654321", "354867531321", "51384351351"],
    };
    $("#customerPhone").easyAutocomplete(options);
</script>

<script src="${pageContext.request.contextPath}/resources/custom/js/imagecropper.min.js"></script>
<script>
    imageCropperModule.init();
</script>

<script>

</script>


</body>
</html>

