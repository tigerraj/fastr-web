<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Driver Profile</h3>
                        </div>

                        <div class="content-body">
                            <div class="row mb15">
                                <div class="col-md-7">
                                    <div class="box-me driver-card">
                                        <div class="flex-item w280 c-b-blue-light">
                                            <div class="image-wrapper positionrelative">
                                                <img src=""
                                                     alt="" class="w100p driver-image">

                                                <p class="exp-level positionabsolute">-</p>
                                            </div>
                                            <div class="name-rating-wrapper text-center">
                                                <p class="driver-name">-</p>

                                                <div class="rating"></div>
                                            </div>
                                        </div>

                                        <div class="flex-item pv25 text-center">
                                            <div class="border-right">
                                                <div class="driver-info-box">
                                                    <p class="store-name">-</p>

                                                    <p class="info-label">Associated store</p>
                                                </div>

                                                <div class="driver-info-box">
                                                    <p class="driver-email">-</p>

                                                    <p class="info-label">email</p>
                                                </div>

                                                <div class="driver-info-box">
                                                    <p class="driver-phone">-</p>

                                                    <p class="info-label">phone</p>
                                                </div>

                                                <div class="driver-info-box">
                                                    <p class="driver-location">-</p>

                                                    <p class="info-label">current location</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="flex-item pv25 text-center">
                                            <div class="driver-info-box">
                                                <p class="license-number">-</p>

                                                <p class="info-label">license number</p>
                                            </div>

                                            <div class="driver-info-box">
                                                <p class="vehicle-number">-</p>

                                                <p class="info-label">vehicle number</p>
                                            </div>

                                            <div class="driver-info-box">
                                                <p class="vehicle-type">-</p>

                                                <p class="info-label">vehicle</p>
                                            </div>

                                            <div class="driver-info-box">
                                                <a href="#" data-toggle="modal" data-target="#servingAreaModal">Serving
                                                    Areas</a><br>
                                                <a href="#" id="editProfileLink">Edit Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="content-header">
                                <h3 class="content-title">Blacklisted Stores</h3>
                            </div>
                            <div class="row mv15 overflowauto h350" id="blacklistedStores">
                                <div class="col-md-3 mb25">
                                    <div class="bl-store-box box-me p15">
                                        <p class="fontbold">No blacklisted stores available.</p>

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade serving-area-modal" id="servingAreaModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Serving Area</h4>
            </div>
            <div class="modal-body p5">
                <div class="m10  selectAllWrapper">
                    <div class="pull-left">
                        <div class="form-group w240">
                            <input type="text" class="form-control" placeholder="Search main area..."
                                   onkeyup="colorAndScroll(event)">
                        </div>
                    </div>
                    <div class="pull-right">
                        <input id="selectAllCheckbox" type="checkbox" name="selectAllGod"
                               class="with-font form-control">
                        <label for="selectAllCheckbox" class="control-label fontnormal">Select All</label>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="overflowauto h500">
                    <div id="servingAreaWrapper" class="responsive grid"></div>
                </div>
                <hr class="hr-mod mh10">
                <div class="mb10 mh10">
                    <button type="button" class="btn btn-success pull-right" id="saveChangesBtn">Save changes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger pull-right mr10">Cancel</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../includes/scripts.jsp" %>
<%--<%@include file="../includes/customScrollBar.jsp" %>--%>
<%@include file="../includes/selectPicker.jsp" %>
<script src="${pageContext.request.contextPath}/resources/vendors/masonry/dist/masonry.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/driver.min.js"></script>
<script>
    $("li[data-menu='drivers']").addClass('active');
</script>

<script>
    $(function () {
        viewDriverModule.init();
    });
</script>


</body>
</html>

