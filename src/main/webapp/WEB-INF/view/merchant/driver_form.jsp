<%@ page import="com.yetistep.anyorder.enums.VehicleType" %>
<%@ page import="com.yetistep.anyorder.enums.DriverExperienceLevel" %>
<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <%--<button id="editBtn" class="btn btn-primary pull-right" data-formtype="view">Edit</button>--%>
                            <h3 class="content-title">Add Driver</h3>
                        </div>

                        <div class="content-body box-me">
                            <form id="addDriverForm" novalidate >
                                <h4>Basic Information</h4>

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail positionrelative"
                                         style="width: 200px; height: 150px;">
                                        <div class="center-me positionabsolute text-center">
                                            <i class="fa fa-image fa-4x"></i>
                                        </div>

                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 200px; max-height: 150px;" id="driverImageContainer"></div>
                                    <div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" id="imgDriver" name="imgDriver" data-upload="image" accept="image/*">
                                    </span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>

                                <div class="row mt15">
                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <label for="firstName" class="control-label">First Name</label>
                                            <input type="text" id="firstName" class="form-control input-lg"
                                                   name="firstName">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <label for="lastName" class="control-label">Last Name</label>
                                            <input type="text" id="lastName" class="form-control input-lg"
                                                   name="lastName">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <label for="emailAddress" class="control-label">Email</label>
                                            <input type="email" id="emailAddress" class="form-control input-lg"
                                                   name="emailAddress">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <label for="mobileNumber" class="control-label">Phone No.</label>
                                            <input type="text" id="mobileNumber" class="form-control input-lg"
                                                   name="mobileNumber" placeholder="This will be your username.">
                                        </div>
                                    </div>


                                </div>

                                <div class="row mt15" id="storeAssociationWrapper">
                                    <div class="col-sm-12">
                                        <h4>Store Association</h4>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group floating">
                                            <label for="storeAssociation" class="control-label">Select Store</label>
                                            <select id="storeAssociation" class="form-control input-lg select-picker"
                                                    name="storeAssociation" required data-title="Select Store">

                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row mt15">
                                    <div class="col-sm-12">
                                        <h4>Vehicle Information</h4>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group floating">
                                            <%--<label for="vehicleType" class="control-label">Vehicle Type</label>--%>
                                            <select id="vehicleType" class="form-control input-lg select-picker"
                                                    name="vehicleType">
                                                <option value="" selected disabled>Vehicle Type</option>
                                                <option value="<%=VehicleType.BIKE%>">Bike</option>
                                                <option value="<%=VehicleType.CAR%>">Car</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <label for="vehicleNo" class="control-label">Vehicle No.</label>
                                            <input type="text" id="vehicleNo" class="form-control input-lg"
                                                   name="vehicleNo">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group floating">
                                            <label for="licenseNo" class="control-label">License No.</label>
                                            <input type="text" id="licenseNo" class="form-control input-lg"
                                                   name="licenseNo">
                                        </div>
                                    </div>
                                </div>

                                <%--<a href="">Add Serving Area</a>--%>

                                <h4>Experience Level</h4>

                                <div class="displayinline mr25">
                                    <input id="experienced" type="radio" name="exp" class="with-font form-control" value="<%=DriverExperienceLevel.EXPERIENCED%>"/>
                                    <label for="experienced" class="control-label">Experienced</label>
                                </div>

                                <div class="displayinline">
                                    <input id="traniee" type="radio" name="exp" class="with-font form-control" value="<%=DriverExperienceLevel.TRAINEE%>"/>
                                    <label for="traniee" class="control-label">Trainee</label>
                                </div>


                                <hr class="hr-mod">
                                <div class="clearfix" id="formBtns">
                                    <button type="button" class="btn btn-success c-b-success btn-lg" id="addServingAreaBtn" data-toggle="modal" data-target="#servingAreaModal">Add Serving Areas</button>
                                    <button class="btn btn-danger pull-right btn-lg" type="button" onclick="window.history.back()">Cancel</button>
                                    <button class="btn btn-success c-b-success pull-right btn-lg mr10" type="submit">Add</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade serving-area-modal" id="servingAreaModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Serving Area</h4>
            </div>
            <div class="modal-body p5">
                <div class="m10  selectAllWrapper">
                    <div class="pull-left">
                        <div class="form-group w240">
                            <input type="text" class="form-control" placeholder="Search main area..."
                                   onkeyup="colorAndScroll(event)">
                        </div>
                    </div>
                    <div class="pull-right">
                        <input id="selectAllCheckbox" type="checkbox" name="selectAllGod" class="with-font form-control">
                        <label for="selectAllCheckbox" class="control-label fontnormal">Select All</label>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="overflowauto h500">
                    <div id="servingAreaWrapper" class="responsive grid"></div>
                </div>
                <hr class="hr-mod mh10">
                <div class="mb10 mh10">
                    <button type="button" class="btn btn-success pull-right" id="confirmServingAreaBtn">Confirm</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger pull-right mr10">Cancel</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/validate.jsp" %>
<%@include file="../includes/image.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<script src="${pageContext.request.contextPath}/resources/vendors/masonry/dist/masonry.pkgd.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/custom/js/driver.min.js"></script>
<script>
    $(function(){
        addEditDriverModule.init();
    });
</script>


</body>
</html>

