<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>


<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <a href="#"
                               class="btn btn-primary pull-right" id="addMerchantBtn">Add Merchant</a>

                            <h3 class="content-title">Merchant List</h3>
                        </div>

                        <div class="content-body box-me">
                            <div class="table-ao-wrapper">
                                <table id="listMerchantTable" class="table table-ao w100p">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Business Title</th>
                                        <th>Contact Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/selectPicker.jsp" %>
<%@include file="../includes/datatables.jsp" %>

<script src="${pageContext.request.contextPath}/resources/custom/js/merchant.min.js"></script>
<script>
    $(function () {
        listMerchantModule.init();
    });

</script>


<script>
    $("li[data-menu='merchants']").addClass('active');
</script>


</body>
</html>

