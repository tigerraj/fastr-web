<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>
<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>
                    <div class="ph50">
                        <div class="content-header">
                            <a class="btn btn-primary pull-right" data-type="generate" data-toggle="modal"
                               data-target="#APIKeyModal">Generate API Key</a>
                            <h3 class="content-title">API</h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="content-body box-me">
                            <div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#APITableTab" aria-controls="APITableTab" role="tab" data-toggle="tab">API Keys</a></li>
                                    <li role="presentation"><a href="#APIDocuTab" aria-controls="APIDocuTab" role="tab" data-toggle="tab">Documentation</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="APITableTab">
                                        <h4> API Keys </h4>
                                        <div class="table-ao-wrapper">
                                            <table id="APIKeysTable" class="table table-ao w100p"></table>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="APIDocuTab">
                                        <h4 class="positionrelative">
                                            API Documentation
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-pills positionabsolute right0 top0">
                                                <li role="presentation" class="active"><a href=".lang-xml" class="pv2 ph15 borderradius0" aria-controls="lang-xml" role="tab" data-toggle="tab">XML</a></li>
                                                <li role="presentation"><a href=".lang-json" class="pv2 ph15 borderradius0" aria-controls="lang-json" role="tab" data-toggle="tab">JSON</a>
                                                </li>
                                            </ul>
                                        </h4>

                                        <div class="alert alert-warning" role="alert">
                                            <p>
                                                <strong> Prerequisite </strong> API Key, Store Id, Area ID for known
                                                areas or LatLng for if store has opted for unknown serving area.
                                            </p>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Get Store ID API</div>
                                            <div class="panel-body">
                                                <p> To get Store ID make a request with access token </p>
                                                <p><strong> Request </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
                                                        <p><strong> API End Point: </strong> api/xml/get_active_stores </p>
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;APIKey> 125503dd-2690-4fbb-9a1f-09965591ee62 &lt;/APIKey>
&lt;/root> </code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
                                                        <p><strong> API End Point: </strong> api/json/get_active_stores </p>
<pre class="line-numbers"><code class="language-json">{
    "apiKey": "125503dd-2690-4fbb-9a1f-09965591ee62"
}</code></pre>
                                                    </div>
                                                </div> <!-- Request Tab -->
                                                <p><strong> Response </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;stores>
        &lt;store>
            &lt;storeId>1c9b0ff1-ccea-4f0a-a681-d5b1754b5703&lt;/storeId>
            &lt;street>Downtown Dubai Sheikh Mohammed bin Rashid Boulevard&lt;/street>
            &lt;allowUnknownAreaServing>true&lt;/allowUnknownAreaServing>
            &lt;storeName>API Test Store 1&lt;/storeName>
        &lt;/store>        
        &lt;store>
            &lt;storeId>e8f47bdd-70ce-46ba-a830-3e3313f1d713&lt;/storeId>
            &lt;street> Unnamed Road&lt;/street>
            &lt;allowUnknownAreaServing>true&lt;/allowUnknownAreaServing>
            &lt;storeName>API Test Store 2&lt;/storeName>
        &lt;/store>
    &lt;/stores>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
<pre class="line-numbers"><code class="language-json">{
    "stores": [
        {
            "storeId": "1c9b0ff1-ccea-4f0a-a681-d5b1754b5703",
            "givenLocation1": "Downtown Dubai Sheikh Mohammed bin Rashid Boulevard",
            "allowUnknownAreaServing": false,
            "storeName": "API Test Store 1"
        },
        {
            "storeId": "bcd64398-4806-49b4-aaee-c16658de85bb",
            "givenLocation1": " Umm Al Quwain - Al Shuwaib Road",
            "allowUnknownAreaServing": false,
            "storeName": "API Test Store 2"
        },
        {
            "storeId": "e8f47bdd-70ce-46ba-a830-3e3313f1d713",
            "givenLocation1": " Unnamed Road",
            "allowUnknownAreaServing": true,
            "storeName": "API Test Store 2"
        }
    ]
}</code></pre>
                                                    </div>
                                                </div> <!-- Response Tab -->
                                            </div>
                                        </div> <!-- end of Store Id Panel -->

                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Get Area API</div>
                                            <div class="panel-body">
                                                <p> Now, we have <em> Store Id </em> of all active stores. To get <em>
                                                    Area Id </em> for any particular store </p>
                                                <p><strong> Request </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
                                                        <p><strong> API End Point: </strong> api/xml/get_serving_area </p>
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;APIKey> 125503dd-2690-4fbb-9a1f-09965591ee62 &lt;/APIKey>
    &lt;storeId> d4df81dd-5ba4-4799-b8fc-575de8492a96 &lt;/storeId>
&lt;/root> </code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
                                                        <p><strong> API End Point: </strong> api/json/get_serving_area </p>
<pre class="line-numbers"><code class="language-json">{
    "apiKey": "125503dd-2690-4fbb-9a1f-09965591ee62",
    "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96"
}</code></pre>
                                                    </div>
                                                </div> <!-- Request Tab -->
                                                <p><strong> Response </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;areas>
        &lt;area>
            &lt;areaId>4884a26b-ad2b-44a1-a2c6-9e5a1ab3daf5&lt;/areaId>
            &lt;areaName>kathmandu&lt;/areaName>
            &lt;child>
                &lt;area>
                    &lt;areaId>9ead03b6-3681-4747-a894-5e8f9fdff400&lt;/areaId>
                    &lt;areaName>charkhal&lt;/areaName>
                    &lt;latitude>27.7073012&lt;/latitude>
                    &lt;longitude>85.32720940000002&lt;/longitude>
                &lt;/area>
            &lt;/child>
        &lt;/area>
    &lt;/areas>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
<pre class="line-numbers"><code class="language-json">{
    "areas": [
        {
            "areaId": "4884a26b-ad2b-44a1-a2c6-9e5a1ab3daf5",
            "areaName": "kathmandu",
            "child": [
                {
                    "areaId": "9ead03b6-3681-4747-a894-5e8f9fdff400",
                    "areaName": "charkhal",
                    "latitude": "27.7073012",
                    "longitude": "85.32720940000002"
                }
            ]
        }
    ]
}</code></pre>
                                                    </div>
                                                </div> <!-- Response Tab -->
                                            </div>
                                        </div> <!-- end of Area Id Panel -->

                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Create Order API</div>
                                            <div class="panel-body">
                                                <p> Please note that we have <em> main area </em> and it will have one
                                                    or more <em> sub area </em> where the order needs to be delivered,
                                                    meaning we need to extract <em> sub area id. </em> Now we have <em>
                                                        store ID, area id (sub area id) </em> we can make a request to
                                                    create an order. </p>

                                                <p> If the store has opted for <em> unknown area serving </em> they can
                                                    use <em> street address and/or latitude and longitude </em> for the
                                                    order <em> drop off location </em>, we prefer <em> latitude and
                                                        longitude </em> as its more accurate. </p>

                                                <p> So, to create an order </p>

                                                <p><strong> Request </strong></p>
                                                <div class="alert alert-warning" role="alert">
                                                    <p><strong> In case of Known Serving Area </strong></p>
                                                </div>
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
                                                        <p><strong> API End Point: </strong> api/xml/save_order </p>
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;APIKey>125503dd-2690-4fbb-9a1f-09965591ee62&lt;/APIKey>
    &lt;storeId>dc659aef-0df5-4586-b5a9-f318c764ce52&lt;/storeId>
    &lt;order>
        &lt;email>customer@customer.com&lt;/email>
        &lt;customerName>test customer&lt;/customerName>
        &lt;mobileNumber>9876543210&lt;/mobileNumber>
        &lt;areaId>9ead03b6-3681-4747-a894-5e8f9fdff400&lt;/areaId> (Required)
        &lt;paymentMode>COD&lt;/paymentMode> (Required)
        &lt;totalAmount>0.00&lt;/totalAmount>
        &lt;orderNote>My Note&lt;/orderNote>
        &lt;orderTime>2016-10-17 11:26:00&lt;/orderTime>  (Should be greater than now, ensure the time format is correct)
    &lt;/order>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
                                                        <p><strong> API End Point: </strong> api/json/save_order </p>
<pre class="line-numbers"><code class="language-json">{
    "apiKey": "125503dd-2690-4fbb-9a1f-09965591ee62",
    "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96",
    "order": {
        "email": "customer@customer.com",
        "customerName": "test customer",
        "mobileNumber": "9876543210",
        "areaId": "9ead03b6-3681-4747-a894-5e8f9fdff400", (Required)
        "paymentMode": "CARD", (Required)
        "totalAmount": "100.00",
        "orderNote": "My Note",
        "orderTime": "2018-01-27 11:26:00" (Should be geater than now, ensure the time format is correct)
    }
}</code></pre>
                                                    </div>
                                                </div> <!-- end of known request tab -->

                                                <div class="alert alert-warning" role="alert">
                                                    <p><strong> In case of Unknown Serving Area </strong></p>
                                                </div>
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;APIKey>125503dd-2690-4fbb-9a1f-09965591ee62&lt;/APIKey>
    &lt;storeId>d4df81dd-5ba4-4799-b8fc-575de8492a96&lt;/storeId>
    &lt;order>
        &lt;email>customer@customer.com&lt;/email>
        &lt;customerName>test customer&lt;/customerName>
        &lt;mobileNumber>9876543210&lt;/mobileNumber>
        &lt;paymentMode>COD&lt;/paymentMode>
        &lt;totalAmount>0.00&lt;/totalAmount>
        &lt;orderNote>My Note&lt;/orderNote>
        &lt;orderTime>2018-1-27 11:26:00&lt;/orderTime>

        &lt;streetAddress>123 High Street&lt;/streetAddress>
        &lt;customerLatitude>25.0698221&lt;/customerLatitude>
        &lt;customerLongitude>55.12848740000004&lt;/customerLongitude>

        Or

        &lt;customerLatitude>25.0698221&lt;/customerLatitude>
        &lt;customerLongitude>55.12848740000004&lt;/customerLongitude>

        Or

        &lt;streetAddress>123 High Street&lt;/streetAddress>
        
    &lt;/order>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
<pre class="line-numbers"><code class="language-json">{
    "apiKey": "125503dd-2690-4fbb-9a1f-09965591ee62",
    "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96",
    "order": {
        "email": "customer@customer.com",
        "customerName": "test customer",
        "mobileNumber": "9876543210",
        "paymentMode": "COD",
        "totalAmount": "0.00",
        "orderNote": "My Note",
        "orderTime": "2018-1-27 11:26:00",

        "streetAddress": "123 High Street",
        "customerLatitude": "25.0698221",
        "customerLongitude": "55.12848740000004",

        or

        "customerLatitude": "25.0698221",
        "customerLongitude": "55.12848740000004",

        or

        "streetAddress": "123 High Street"
    }
}</code></pre>
                                                    </div>
                                                </div> <!-- end of unknown request tab -->

                                                <p><strong> Response </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;orderId>e6df1dff-ca25-4fbb-8f70-c67f5d569550&lt;/orderId>
    &lt;message>Order has been saved successfully&lt;/message>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
<pre class="line-numbers"><code class="language-json">{
    "orderId": "a82c12dc-3348-4c92-bae9-657effc95d93",
    "message": "Order has been saved successfully"
}</code></pre>
                                                    </div>

                                                </div> <!-- Response Tab -->
                                            </div>
                                        </div> <!-- end of Create Order Panel -->

                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Get Order List API</div>
                                            <div class="panel-body">
                                                <p> Now, to view the orders </p>
                                                <div class="alert alert-info" role="alert">
                                                    <p><strong> Info: </strong> If we pass <em> Store ID </em> we get
                                                        the orders from particular store, else we will get orders for
                                                        all the store for the particular merchant </p>
                                                </div>
                                                <p><strong> Request </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
                                                        <p><strong> API End Point: </strong> api/xml/order_list </p>
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;APIKey>125503dd-2690-4fbb-9a1f-09965591ee62&lt;/APIKey>
    &lt;orderDisplayStatus>CANCELLED/DELIVERED/RUNNING&lt;/orderDisplayStatus>
    &lt;storeId>d4df81dd-5ba4-4799-b8fc-575de8492a96&lt;/storeId>  (Optional)
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
                                                        <p><strong> API End Point: </strong> api/json/order_list </p>
<pre class="line-numbers"><code class="language-json">{
    "apiKey": "125503dd-2690-4fbb-9a1f-09965591ee62",
    "orderDisplayStatus": "RUNNING",
    "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96" (Optional)
}</code></pre>
                                                    </div>

                                                </div> <!-- Request Tab -->
                                                <p><strong> Response </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;orders>
        &lt;order>
            &lt;orderId>e6df1dff-ca25-4fbb-8f70-c67f5d569550&lt;/orderId>
            &lt;orderStatus>ORDER_PLACED&lt;/orderStatus>
            &lt;paymentMode>COD&lt;/paymentMode>
            &lt;createdDate>2017-18-25T14:18:25&lt;/createdDate>
            &lt;store>
                &lt;storeId>d4df81dd-5ba4-4799-b8fc-575de8492a96&lt;/storeId>
                &lt;street> Trishakti Marg&lt;/street>
                &lt;givenLocation2>Kathmandu Bagmati&lt;/givenLocation2>
                &lt;latitude>27.7172453&lt;/latitude>
                &lt;longitude>85.3239605&lt;/longitude>
                &lt;storeName>API Test Store 1&lt;/storeName>
            &lt;/store>
            &lt;customersArea>
                &lt;street>123 High Street&lt;/street>
                &lt;latitude>25.3767697&lt;/latitude>
                &lt;longitude>55.44305840000001&lt;/longitude>
            &lt;/customersArea>
            &lt;totalAmount>0.00&lt;/totalAmount>
            &lt;orderTime>2018-26-27T11:26:00&lt;/orderTime>
        &lt;/order>
        &lt;order>
            &lt;orderId>1726aab8-9de0-4639-87b7-071531453418&lt;/orderId>
            &lt;orderStatus>ORDER_PLACED&lt;/orderStatus>
            &lt;paymentMode>COD&lt;/paymentMode>
            &lt;createdDate>2017-41-25T14:41:20&lt;/createdDate>
            &lt;store>
                &lt;storeId>d4df81dd-5ba4-4799-b8fc-575de8492a96&lt;/storeId>
                &lt;street> Trishakti Marg&lt;/street>
                &lt;givenLocation2>Kathmandu Bagmati&lt;/givenLocation2>
                &lt;latitude>27.7172453&lt;/latitude>
                &lt;longitude>85.3239605&lt;/longitude>
                &lt;storeName>API Test Store 1&lt;/storeName>
            &lt;/store>
            &lt;customersArea>
                &lt;street>234&lt;/street>
                &lt;latitude>25.3767697&lt;/latitude>
                &lt;longitude>55.44305840000001&lt;/longitude>
            &lt;/customersArea>
            &lt;totalAmount>0.00&lt;/totalAmount>
            &lt;orderTime>2018-26-27T11:26:00&lt;/orderTime>
        &lt;/order>
    &lt;/orders>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
<pre class="line-numbers"><code class="language-json">{
    "orders": [
        {
            "orderId": "0fe5241d-e7b2-4856-a089-b50fffc388ad",
            "orderStatus": "ORDER_PLACED or CANCELLED or DELIVERED",
            "paymentMode": "COD",
            "createdDate": "2017-01-26 10:27:53",
            "estimatedDeliveryTime": 0,
            "store": {
                "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96",
                "givenLocation1": " Trishakti Marg",
                "givenLocation2": "Kathmandu Bagmati",
                "latitude": "27.7172453",
                "longitude": "85.3239605",
                "storeName": "API Test Store 1"
            },
            "customersArea": {
                "street": "123 High Street",
                "latitude": "25.0698221",
                "longitude": "55.12848740000004"
            },
            "totalAmount": 0,
            "orderTime": "2018-01-27 11:26:00"
        },
        {
            "orderId": "80524e22-3482-453c-a74f-a52fb429e2e6",
            "orderStatus": "ORDER_PLACED",
            "paymentMode": "CARD",
            "createdDate": "2017-01-26 10:29:56",
            "estimatedDeliveryTime": 0,
            "store": {
                "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96",
                "givenLocation1": " Trishakti Marg",
                "givenLocation2": "Kathmandu Bagmati",
                "latitude": "27.7172453",
                "longitude": "85.3239605",
                "storeName": "API Test Store 1"
            },
            "customersArea": {
                "street": "123 High Street",
                "latitude": "25.0698221",
                "longitude": "55.12848740000004"
            },
            "totalAmount": 110,
            "orderTime": "2018-01-27 11:26:00"
        }
    ]
}</code></pre>
                                                    </div>
                                                </div> <!-- Response Tab -->
                                            </div>
                                        </div> <!-- end of Order List Panel -->

                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Get Order Detail API</div>
                                            <div class="panel-body">
                                                <p> Now, to view detail of any particular order </p>
                                                <p><strong> Request </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
                                                        <p><strong> API End Point: </strong> api/xml/order_detail </p>
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;APIKey>125503dd-2690-4fbb-9a1f-09965591ee62&lt;/APIKey>
    &lt;orderId>d4df81dd-5ba4-4799-b8fc-575de8492a96&lt;/orderId> 
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
                                                        <p><strong> API End Point: </strong> api/json/order_detail </p>
<pre class="line-numbers"><code class="language-json">{
    "apiKey": "125503dd-2690-4fbb-9a1f-09965591ee62",
    "orderId":"cf6aebca-72d2-4d25-baf7-18b5cb93fa9e"
}</code></pre>
                                                    </div>
                                                </div> <!-- Request Tab -->
                                                <p><strong> Response </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;order>
        &lt;orderId>1726aab8-9de0-4639-87b7-071531453418&lt;/orderId>
        &lt;orderStatus>ORDER_PLACED&lt;/orderStatus>
        &lt;paymentMode>COD&lt;/paymentMode>
        &lt;createdDate>2017-41-25T14:41:20&lt;/createdDate>
        &lt;store>
            &lt;storeId>d4df81dd-5ba4-4799-b8fc-575de8492a96&lt;/storeId>
            &lt;street> Trishakti Marg&lt;/street>
            &lt;givenLocation2>Kathmandu Bagmati&lt;/givenLocation2>
            &lt;addressNote>test&lt;/addressNote>
            &lt;latitude>27.7172453&lt;/latitude>
            &lt;longitude>85.3239605&lt;/longitude>
            &lt;storeBrand>
                &lt;brandName>API Test Store 1&lt;/brandName>
                &lt;brandLogo>https://d3419qev6ibwlc.cloudfront.net/Merchant_258/Brand_259/brand_logo_tmp_2591484903149435.png&lt;/brandLogo>
            &lt;/storeBrand>
            &lt;area>
                &lt;areaId>9ead03b6-3681-4747-a894-5e8f9fdff400&lt;/areaId>
                &lt;areaName>charkhal&lt;/areaName>
                &lt;latitude>27.7073012&lt;/latitude>
                &lt;longitude>85.32720940000002&lt;/longitude>
            &lt;/area>
        &lt;/store>
        &lt;customer>
            &lt;user>
                &lt;firstName>test customer&lt;/firstName>
                &lt;mobileNumber>9876543210&lt;/mobileNumber>
                &lt;emailAddress>superman@superman.com&lt;/emailAddress>
            &lt;/user>
        &lt;/customer>
        &lt;customersArea>
            &lt;street>234&lt;/street>
            &lt;latitude>25.3767697&lt;/latitude>
            &lt;longitude>55.44305840000001&lt;/longitude>
        &lt;/customersArea>
        &lt;orderNote>My Note&lt;/orderNote>
        &lt;totalAmount>0.00&lt;/totalAmount>
        &lt;orderTime>2018-26-27T11:26:00&lt;/orderTime>
    &lt;/order>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
<pre class="line-numbers"><code class="language-json">{
    "order": {
        "orderId": "c4d2003e-64a0-4cf1-8662-09159d266cc5",
        "orderDisplayId": "2017095716126",
        "orderStatus": "ORDER_PLACED",
        "paymentMode": "COD",
        "totalBillAmount": 0,
        "createdDate": "2017-01-26 11:01:07",
        "estimatedDeliveryTime": 0,
        "orderDeliveryDate": "2018-01-27 11:26:00",
        "customersNoteToDriver": "My Note",
        "store": {
            "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96",
            "givenLocation1": " Trishakti Marg",
            "givenLocation2": "Kathmandu Bagmati",
            "addressNote": "test",
            "latitude": "27.7172453",
            "longitude": "85.3239605",
            "priority": "PRIORITIZED",
            "allowStoresDriver": false,
            "allowUnknownAreaServing": true,
            "storeBrand": {
                "storeBrandId": "39edc627-7e37-4c87-a12b-964c57bb5f1f",
                "brandName": "API Test Store 1",
                "brandLogo": "https://d3419qev6ibwlc.cloudfront.net/Merchant_258/Brand_259/brand_logo_tmp_2591484903149435.png"
            },
            "area": {
                "areaId": "9ead03b6-3681-4747-a894-5e8f9fdff400",
                "areaName": "charkhal",
                "latitude": "27.7073012",
                "longitude": "85.32720940000002"
            }
        },
        "customer": {
            "customerId": "5ce41a36-0cff-47eb-aa17-17ea6e11795d",
            "user": {
                "firstName": "test customer",
                "mobileNumber": "9876543210",
                "emailAddress": "customer@customer.com"
            }
        },
        "customersArea": {
            "customersAreaId": "e277a017-549c-41f6-b107-17d13f160913",
            "street": "123 Street ABC",
            "latitude": "25.0698221",
            "longitude": "55.12848740000004"
        }
    }
}</code></pre>
                                                    </div>
                                                </div> <!-- Response Tab -->
                                            </div>
                                        </div> <!-- end of Order Detail Panel -->

                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Get Estimated Charge API</div>
                                            <div class="panel-body">
                                                <p><strong> Request </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
                                                        <p><strong> API End Point: </strong> api/xml/estimate_charge </p>
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;APIKey>125503dd-2690-4fbb-9a1f-09965591ee62&lt;/APIKey>
    &lt;storeId>1c9b0ff1-ccea-4f0a-a681-d5b1754b5703&lt;/storeId>
    &lt;latitude>25.12&lt;/latitude>
    &lt;longitude>84.25&lt;/longitude>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
                                                        <p><strong> API End Point: </strong> api/json/estimate_charge </p>
<pre class="line-numbers"><code class="language-json">{
    "apiKey": "125503dd-2690-4fbb-9a1f-09965591ee62",
    "storeId": "d4df81dd-5ba4-4799-b8fc-575de8492a96",
    "latitude": "25.12",
    "longitude": "84.25"
}</code></pre>
                                                    </div>
                                                </div> <!-- Request Tab -->
                                                <p><strong> Response </strong></p>
                                                <!-- Tab panes -->
                                                <div class="tab-content positionrelative">
                                                    <div role="tabpanel" class="tab-pane fade in active lang-xml">
<pre class="line-numbers"><code class="language-markup">&lt;root>
    &lt;message>Order estimate charge has been calculated&lt;/message>
    &lt;estimateCharge>2419.29&lt;/estimateCharge>
&lt;/root></code></pre>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade lang-json">
<pre class="line-numbers"><code class="language-json">{
    "message": "Order estimate charge has been calculated",
    "estimateCharge": 2419.29
}</code></pre>
                                                    </div>
                                                </div> <!-- Response Tab -->
                                            </div>
                                        </div> <!-- end of Estimate Charge Panel -->

                                        <div class="panel panel-danger">
                                            <div class="panel-heading">Error Codes</div>
                                            <div class="panel-body">
                                                <ul>
                                                    <li><strong> API001 </strong> = API key does not exist.</li>
                                                    <li><strong> API002 </strong> = API key is not unique.</li>
                                                    <li><strong> API003 </strong> = Merchant is inactive.</li>
                                                    <li><strong> API004 </strong> = Store doesnot allow unknown area.
                                                    </li>
                                                    <li><strong> API005 </strong> = Store doesnot associate with API
                                                        key.
                                                    </li>
                                                    <li><strong> API006 </strong> = Order time must be greater than
                                                        current time.
                                                    </li>
                                                    <li><strong> API007 </strong> = Street address is missing.</li>
                                                    <li><strong> API008 </strong> = Street address is invalid.</li>
                                                    <li><strong> API009 </strong> = Order delivery date is required.
                                                    </li>
                                                    <li><strong> API010 </strong> = Area doesnot belong to store serving
                                                        area.
                                                    </li>
                                                    <li><strong> UNRPN001 </strong> = Unknown area rate plan does not
                                                        exist.
                                                    </li>
                                                    <li><strong> RPN001 </strong> = Rate plan does not exist.</li>
                                                    <li><strong> ARE001 </strong> = Area does not exist.</li>
                                                    <li><strong> STR001 </strong> = Stores Brand does not exist.</li>
                                                    <li><strong> STR004 </strong> = Store is unverified or inactive.
                                                    </li>
                                                    <li><strong> STR005 </strong> = Store Brand is inactive.</li>
                                                    <li><strong> STR006 </strong> = Store is inactive.</li>
                                                    <li><strong> API011 </strong> = Latitude or Longitude is missing.
                                                    </li>
                                                    <li><strong> API012 </strong> = Store doesnot serve unknown area.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div> <!-- end of Docu tab content -->
                                </div> <!-- end of tab content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="APIKeyModal" tabindex="-1" role="dialog"
     aria-labelledby="API Key">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> API Details</h4>
            </div>
            <div class="modal-body p5 m20">
                <form id="APIKeyForm">
                    <div class="form-group">
                        <label for="APIName" class="control-label">API Name</label>
                        <input type="text" id="APIName" class="form-control input-lg"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="APIKey" class="control-label">API Key</label>
                        <input type="text" id="APIKey" class="form-control input-lg"
                               readonly>
                    </div>
                    <hr class="hr-mod">
                    <div class="clearfix">
                        <div class="pull-right">
                            <button class="btn btn-success c-b-success btn-lg mr15"
                                    type="submit">Save Changes
                            </button>
                            <button type="reset" class="btn btn-danger btn-lg" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%@include file="../includes/scripts.jsp" %>
<%@include file="../includes/datatables.jsp" %>
<%@include file="../includes/syntax.jsp" %>
<script src="${pageContext.request.contextPath}/resources/custom/js/api.min.js"></script>
<script>
    apiModule.init();
</script>
</body>
</html>