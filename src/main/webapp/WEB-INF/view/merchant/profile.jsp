<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Profile</h3>
                        </div>

                        <div class="content-body ">
                            <div class="row mt25">
                                <div class="col-md-4">
                                    <form id="profileUpdateForm" method="POST" action="" autocomplete="off"
                                          class="box-me p25">
                                        <h4 class="mb25">Basic Information</h4>


                                        <div class="form-group floating">
                                            <label for="businessTitle" class="control-label">Business Title</label>
                                            <input type="text" class="form-control input-lg"
                                                   id="businessTitle" name="businessTitle" disabled>
                                        </div>

                                        <div class="form-group floating">
                                            <label for="username" class="control-label">Email</label>
                                            <input type="email" class="form-control input-lg"
                                                   id="username" name="username" disabled>
                                        </div>

                                        <div class="form-group floating">
                                            <label for="firstName" class="control-label">First Name </label>
                                            <input type="text" class="form-control input-lg"
                                                   id="firstName" name="firstName">
                                        </div>

                                        <div class="form-group floating">
                                            <label for="lastName" class="control-label">Last Name</label>
                                            <input type="text" class="form-control input-lg"
                                                   id="lastName" name="lastName">
                                        </div>

                                        <div class="form-group floating">
                                            <label for="mobileNumber" class="control-label">Contact Number</label>
                                            <input type="tel" class="form-control input-lg"
                                                   id="mobileNumber" name="mobileNumber">
                                        </div>

                                        <div class="form-group floating">
                                            <label for="curPassword" class="control-label">Current Password</label>
                                            <input type="password" id="curPassword" name="curPassword"
                                                   class="form-control input-lg" autocomplete="new-password">
                                        </div>


                                        <hr class="hr-mod">
                                        <div class="clearfix">
                                            <a href="#" id="changePasswordLink">Change Password</a>
                                            <button class="btn btn-success c-b-success pull-right btn-lg"
                                                    type="submit">Save Changes
                                            </button>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-md-4" style="display: none" id="passwordFormWrapper">
                                    <form id="userChangePWForm" method="POST" action="" class="box-me p25">
                                        <h4 class="mb25">Change Password</h4>

                                        <div class="form-group floating">
                                            <label for="curPassword1" class="control-label">Current Password</label>
                                            <input type="password" class="form-control input-lg"
                                                   id="curPassword1" name="curPassword1">
                                        </div>

                                        <div class="form-group floating">
                                            <label for="newPassword" class="control-label">New Password</label>
                                            <input type="password" class="form-control input-lg"
                                                   id="newPassword" name="newPassword">
                                        </div>

                                        <div class="form-group floating">
                                            <label for="confirmPassword" class="control-label">Confirm New
                                                Password</label>
                                            <input type="password" class="form-control input-lg"
                                                   id="confirmPassword" name="confirmPassword">
                                        </div>

                                        <hr class="hr-mod">
                                        <div class="clearfix">
                                            <div class="pull-right">
                                                <button class="btn btn-success c-b-success btn-lg mr15"
                                                        type="submit">Save Changes
                                                </button>
                                                <button type="button" class="btn btn-danger btn-lg" id="cancelPasswordChange">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>

                        <%-- <div class="content-body box-me mt50">

                         </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../includes/scripts.jsp" %>
<script src="${pageContext.request.contextPath}/resources/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendors/jquery-validation-bootstrap-tooltip/jquery-validate.bootstrap-tooltip.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom/js/profile.min.js"></script>

<script>
    $(function () {
        profileModule.init();
    });
</script>


</body>
</html>

