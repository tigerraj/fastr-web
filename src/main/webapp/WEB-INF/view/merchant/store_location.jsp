<%@include file="../includes/header.jsp" %>
<%@include file="../includes/menu_sidebar.jsp" %>

<div class="content-wrapper c-b-body" id="content" data-content="sm">
    <div class="container-fluid">
        <div class="row">
            <div class="pr0">
                <div class="content">
                    <div class="content-nav">
                        <%@include file="../includes/menu_content.jsp" %>
                    </div>

                    <div class="ph50">
                        <div class="content-header">
                            <h3 class="content-title">Store Locations</h3>
                        </div>

                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="store-location-modal">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <div class="text-center">
                                                    <img src="http://placehold.it/175x175&text=SLogo" alt=""
                                                         class="brand-logo w200">
                                                    <h4 class="brand-title" id="myModalLabel"></h4>

                                                    <p class="fontbold store-count"></p>
                                                    <p class="fontbold store-status"></p>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p class="body-title">Store Locations</p>

                                                <div class="row" id="storeLocationWrapper">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade serving-area-modal" id="servingAreaModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Serving Area</h4>
            </div>
            <div class="modal-body p5">
                <div class="m10  selectAllWrapper">
                    <div class="pull-left">
                        <div class="form-group w240">
                            <input type="text" class="form-control" placeholder="Search main area..."
                                   onkeyup="colorAndScroll(event)">
                        </div>
                    </div>
                    <div class="pull-right">
                        <input id="selectAllCheckbox" type="checkbox" name="selectAllGod"
                               class="with-font form-control">
                        <label for="selectAllCheckbox" class="control-label fontnormal">Select All</label>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class=" h500 overflowauto">
                    <div id="servingAreaWrapper" class="responsive grid"></div>
                </div>
                <hr class="hr-mod mh10">
                <div class="mb10 mh10">
                    <button type="button" class="btn btn-success pull-right" id="saveChangesBtn">Save changes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger pull-right mr10">Cancel</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="blackListedModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Blacklisted Drivers</h4>
            </div>
            <div class="modal-body p5">
                <div class="m10  selectAllWrapper">
                    <div class="pull-right">
                        <input id="selectAllDriverCheckbox" type="checkbox" name="selectAllGod"
                               class="with-font form-control">
                        <label for="selectAllDriverCheckbox" class="control-label fontnormal">Select All</label>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="h500 overflowauto">
                    <div id="driverListWrapper" class="responsive grid"></div>
                </div>
                <hr class="hr-mod mh10">
                <div class="mb10 mh10">
                    <button type="button" class="btn btn-success pull-right" id="saveBLDriverChangesBtn">Save changes
                    </button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger pull-right mr10">Cancel</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ratePlanModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg w1000" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Rate Plans</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="title-border">Rate Plan Within Service Area</h4>

                        <div id="WServiceArea"></div>
                    </div>
                    <div class="col-md-6">
                        <h4 class="title-border">Rate Plan Out of Service Area</h4>

                        <div id="WOServiceArea"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="verityOptionModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Settings</h4>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group">
                        <div class="">
                            <input id="allowStoresDrivers" type="checkbox" class="with-font form-control"/>
                            <label for="allowStoresDrivers" class="control-label">Allow stores drivers</label>
                        </div>

                        <div class="">
                            <input id="allowUnknownArea" type="checkbox" class="with-font form-control" />
                            <label for="allowUnknownArea" class="control-label">Allow unknown area serving</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" id="confirmVerifyStore" type="button">Confirm</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<%@include file="../includes/scripts.jsp" %>

<script src="${pageContext.request.contextPath}/resources/vendors/masonry/dist/masonry.pkgd.min.js"></script>
<script>
    var merchantId = Main.getNamedParameter('merchantId');
    merchantId = merchantId.replace('#', '');
    if (merchantId == "" || merchantId == null) {
        merchantId = Main.getFromLocalStorage('merchantId');
    }

    var storeBrandId = Main.getNamedParameter('storeBrandId').replace('#', '');
</script>


<script src="${pageContext.request.contextPath}/resources/custom/js/storeLocation.min.js"></script>

<script>
    $(function () {
        storeLocationModule.init();
    });

</script>

<script>
    //$("li[data-menu='stores']").addClass('active');
</script>


</body>
</html>

