/**
 * Created by Lunek on 7/26/2016.
 */

var sendNotificationModule = (function () {
    "use strict";
    var sendNotification = {
        init: function () {
            this.ui.app.prop('checked', true);
            this.togglePlatform();
            this.ui.validDate.datetimepicker({
                useStrict: true,
                minDate: moment(),
                format: 'YYYY-MM-DD'
            }).val(moment().format('YYYY-MM-DD')).blur();
            this.events();
            this.validation();
        },
        ui: {
            sendNotificationForm: $('#sendNotificationForm'),
            platform: $('[name="notificationPlatform"]'),
            app: $('#notifyApp'),
            appSection: $('#notifyAppSection'),
            web: $('#notifyWeb'),
            webSection: $('#notifyWebSection'),
            notifyMerchant: $('#notifyToMerchant'),
            notifyTo: $('[name="notifyTo"]'),
            validDate: $('#notificationValidity'),
            message: $('#notificationMessage'),
            history: $('#notificationHistory'),
            webHistoryDb: [],
            appHistoryDb: []
        },
        events: function () {
            var that = this;
            this.ui.sendNotificationForm.on('click', this.ui.platform.selector, function () {
                that.togglePlatform();
            });
        },
        validation: function () {
            var that = this;
            this.ui.sendNotificationForm.validate({
                rules: {
                    notifyTo: {
                        required: true
                    },
                    notificationValidity: {
                        date: true,
                        required: true
                    },
                    notificationMessage: {
                        required: true,
                        minlength: 3
                    }
                },
                submitHandler: function () {
                    var data = {},
                        notification = {},
                        users = [],
                        notifyTo = [],
                        selectedPlatform = "";

                    notification.content = that.ui.message.val();
                    notification.validTill = moment(that.ui.validDate.val()).format("YYYY-MM-DDTHH:mm:ss");

                    selectedPlatform = $(that.ui.platform.selector + ':checked').val();
                    if (selectedPlatform === 'notifyWeb') {
                        notifyTo.push(that.ui.notifyMerchant.attr('data-value'));
                        users = that.ui.notifyMerchant.val();
                        data.userIds = users;
                    } else {
                        $.each($(that.ui.notifyTo.selector + ":checked"), function (index, val) {
                            notifyTo.push(val.value);
                        });
                    }
                    data.notifyTos = notifyTo;
                    data.notification = notification;

                    that.send(data);
                    return false;
                }
            });
        },
        send: function (sendData) {
            var that = this,
                url = "",
                params = {},
                callback;

            url = "/organizer/send_notification";
            params = sendData;
            callback = function (status, data) {
                if (data.success) {
                    var button = function () {
                        location.reload();
                    };
                    button.text = 'Close';
                    Main.popDialog('Success', data.message, [button], 'success');
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            Main.request(url, params, callback, {});
        },
        togglePlatform: function () {
            var selectedPlatform = $(this.ui.platform.selector + ':checked');
            if (selectedPlatform.val() === 'notifyApp') {
                this.ui.webSection.addClass('hidden');
                this.ui.appSection.removeClass('hidden');
                this.ui.notifyMerchant.val("").selectpicker('refresh');
                this.getHistory('MOBILE');
            } else if (selectedPlatform.val() === 'notifyWeb') {
                this.ui.webSection.removeClass('hidden');
                this.ui.appSection.addClass('hidden');
                if (!this.ui.notifyMerchant.find('option').length) {
                    this.getMerchantList();
                }
                this.getHistory('WEB');
            }
        },
        getMerchantList: function () {
            var that = this,
                url = "",
                callback;

            url = "/organizer/get_active_merchant";
            callback = function (status, data) {
                if (data.success) {
                    var list = data.params.merchants;
                    that.displayMerchantList(list);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },
        displayMerchantList: function (data) {
            if (data.length) {
                var option = "",
                    empty = " ",
                    userId = "",
                    fname = "",
                    lname = "",
                    businessTitle = "";
                $.each(data, function (index, val) {
                    userId = val.userId || "";
                    fname = val.firstName || " ";
                    lname = val.lastName || " ";
                    if (val.merchant) {
                        businessTitle = val.merchant.businessTitle || "";
                    }
                    option += '<option value="' + userId + '">' + businessTitle + '</option>';
                });
                this.ui.notifyMerchant.html(option).selectpicker('refresh');
            }
        },
        getHistory: function (historyType) {
            if (!historyType) {
                return;
            }
            if (historyType === 'MOBILE') {
                if (this.ui.app.attr('data-status') === 'true') {
                    this.displayHistory(this.ui.appHistoryDb);
                    return;
                }
            }
            if (historyType === 'WEB') {
                if (this.ui.web.attr('data-status') === 'true') {
                    this.displayHistory(this.ui.webHistoryDb);
                    return;
                }
            }
            var that = this,
                url = "",
                params = {},
                callback;

            url = "/organizer/notification";
            params.userDeviceType = historyType;
            callback = function (status, data) {
                if (data.success) {
                    var historyData = data.params.notifications;
                    that.displayHistory(historyData, historyType);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.loaderDiv = this.ui.history.selector;
            Main.request(url, params, callback, {});
        },
        displayHistory: function (data, device) {
            if (data.length) {
                if (device === "WEB") {
                    this.ui.web.attr('data-status', 'true');
                    this.ui.webHistoryDb = data;
                }
                else if (device === 'MOBILE') {
                    this.ui.app.attr('data-status', 'true');
                    this.ui.appHistoryDb = data;
                }
                var notification = "";
                $.each(data, function (index, val) {
                    notification += '<div class="notification-past p20 c-b-dash-menu">';
                    notification += '<p data-id="' + val.notificationId + '">' + val.content + '</p>';
                    notification += '<div class="row">';
                    notification += '<small class="c-t-gray-dark col-sm-3">' + moment(val.createdDate).fromNow() + '</small>';
                    notification += '<div class="col-sm-9 text-right">';
                    // if (val.users && val.users.length) {
                    //     $.each(val.users, function (index, val) {
                    //         notification += '<span class="past-notification-recepient" data-id="' + val.userId + '" data-first="' + val.firstName + '" data-last="' + val.lastName + '">';
                    //         notification += val.firstName.charAt(0) + val.lastName.charAt(0);
                    //         notification += '</span>';
                    //     });
                    // }
                    // if (val.notifyTos && val.notifyTos.length) {
                    //     $.each(val.notifyTos, function (index, val) {
                    //         var receipients = val.split('_');
                    //         notification += '<span class="past-notification-recepient" data-first="' + receipients[0].toLowerCase() + '" data-last="' + receipients[1].toLowerCase() + '">';
                    //         notification += receipients[0].charAt(0).toUpperCase() + receipients[1].charAt(0).toUpperCase();
                    //         notification += '</span>';
                    //     });
                    // }
                    notification += '</div> </div>';
                    notification += '</div>';
                });
                this.ui.history.html(notification);
            } else {
                if (device === "WEB") {
                    this.ui.web.attr('data-status', 'false');
                }
                else if (device === 'MOBILE') {
                    this.ui.app.attr('data-status', 'false');
                }
                this.ui.history.html("No notifications to show!");
            }
        },
    };
    return {
        init: function () {
            sendNotification.init();
        }
    };
})();