/**
 * Created by Pratik on 7/11/2016.
 */

var ordersMainMap, orderDetailModalMap, assignOrderModalMap;
var orderListModule = (function () {
    var curOrderType = ""; // CurrentOrders, PastOrders, CancelledOrders
    var curWrapper, curCountWrapper, driversLocation = [];
    var searchDriverDBon = [];
    var searchDriverDBoff = [];
    var onDutyDriversLocation = [];
    var offDutyDriversLocation = [];
    var storesLocation = [];
    var isSearchOrder = false;

    var orderList = {
        ui: {

            orderWaitingWrapper: $('#orderListWaiting'),
            orderRunningWrapper: $('#orderListRunning'),
            orderDetailModal: $('#orderDetailModal'),

            driverAssignModal: $('#driverAssignModal'),
            ownDriverWrapper: $('#ownDriverWrapper'),
            assignOwnDriverBtn: $('#assignOwnDriverBtn'),

            bookAnyOrderDriverBtn: $('#bookAnyOrderDriverBtn'),
            anyOrderDriverWrapper: $('#anyOrderDriverWrapper'),

            ownDriverRadio: $('input[name="storeDriver"][type="radio"]'),
            anyOrderDriverRadio: $('input[name="anyOrderDriverRadio"][type="radio"]'),

            assignOwnDriverForm: $('#assignOwnDriverForm'),

            ordersTabs: $('#ordersTabs'),

            googleMapsWrapper: $('#googleMapsWrapper'),
            googleMapsWrapperRoute: $('#googleMapsWrapperRoute'),

            orderSort2: $('#orderSort2'),
            driverSort2: $('#driverSort2'),

            orderTypeCurrentLink: $('#orderTypeCurrent'),
            orderTypePastLink: $('#orderTypePast'),
            orderTypeCancelledLink: $('#orderTypeCancelled'),

            currentOrderWrapper: $('#currentOrderWrapper'),
            pastOrderWrapper: $('#pastOrderWrapper'),
            cancelledOrderWrapper: $('#cancelledOrderWrapper'),
            searchOrderWrapper: $('#searchOrderWrapper'),
            searchOrderForm: $('#searchOrderForm'),
            searchOrderInput: $('#searchOrderInput'),
            searchOrderStatusDropdown: $('#searchOrderStatusDropdown'),
            // mapCanvasModal: $('#mapCanvasModal'),

            confirmAnyOrderDriverBtn: $('#confirmAnyOrderDriverBtn'),

            anyOrderDriverBox: $('#anyOrderDriverBox'),

            cancelAnyOrderDriverBtn: $('#cancelAnyOrderDriverBtn'),


            driverListDutyOnWrapper: $('#driverListDutyOnWrapper'),
            driverListDutyOffWrapper: $('#driverListDutyOffWrapper'),
            driverListDutyOnCount: $('#driverListDutyOnCount'),
            driverListDutyOffCount: $('#driverListDutyOffCount'),
            driverTabContent: $('#driverTabContent'),

            dispatchButton: $('[data-type="dispatchOrder"]'),

            searchDriverInMap: $('#pac-input'),
            closeSearchBtn: $('#closeSearchBtn'),

            driverNameLink: $('a[data-title="driverNames"]'),

            viewInMapsLink: $('a[data-type="showInMaps"]'),


            cancelOrderModel: $('#cancelOrderModel'),
            cancelOrderForm: $('#cancelOrderForm'),
            reasonsForCancel: $('#reasonsFor'),
            cancelConfirmBtn: $('#cancelConfirmBtn'),
            refreshOrdersListBtn: $('#refreshOrdersListBtn'),
            refreshDriversListBtn: $('#refreshDriversListBtn'),

            hideDriverListLink: $('#hideDriverList'),
            hideOrderListLink: $('#hideOrderList'),
            colMapsWrapper: $('#col-maps'),
            colDriverListWrapper: $('#col-driverslist'),
            colOrderListWrapper: $('#col-orders'),
            showDriverListLink: $('#showDriverList'),
            showOrderListLink: $('#showOrderList'),

            //orders tabs
            rootOrderTabs: $('#rootOrderTabs'),
            waitingOrdersTabs: $('#waitingOrdersTabs'),
            runningOrdersTabs: $('#runningOrdersTabs'),

            //order counts
            orderListWaitingCount: $('#orderListWaitingCount'),
            orderListRunningCount: $('#orderListRunningCount'),
            orderPlacedCount: $('#orderPlacedCount'),
            orderStartedCount: $('#orderStartedCount'),
            inRouteTopickUpCount: $('#inRouteTopickUpCount'),
            atStoreCount: $('#atStoreCount'),
            inRouteToDeliveryCount: $('#inRouteToDeliveryCount'),

            //order wrappers
            orderPlacedWrapper: $('#orderPlaced'),
            orderStartedWrapper: $('#orderStarted'),
            inRouteToPickUpWrapper: $('#inRouteToPickUp'),
            atStoreWrapper: $('#atStore'),
            inRouteToDeliveryWrapper: $('#inRouteToDelivery'),

            // driver orders modal
            driverCurrentOrderModal: $('#driverCurrentOrderModal'),
            driverCurrentOrderList: $('#driverCurrentOrderList'),
            driverCurrentOrderName: $('.order-current-driver'),


            orderListBlackWrapper: $('.order-waiting-actions'),


            //toggle Maps
            storesCheckbox: $('#storesCheckbox'),
            driversCheckbox: $('#driversCheckbox'),

            mapTogglesWrapper: $('#mapToggles'),

            // Driver Search on Assign Modal
            assignDriverSearch: $('.js-driver-search')
        },

        init: function () {
            curOrderType = "CurrentOrders";
            this.events();

            this.getDriversByStatus();

            this.validateCancelOrder();

            var searchKey = Main.getNamedParameter('searchKey');
            var searchOrderStatus = Main.getNamedParameter('searchOrderStatus') || "";
            if (searchOrderStatus) {
                this.ui.searchOrderStatusDropdown.find('a[data-type="'+searchOrderStatus+'"]').trigger('click');
            }
            var searchPage = Main.getNamedParameter('searchPage') || 1;
            if (searchKey) {
                isSearchOrder = true;
                this.searchOrder(searchKey, searchOrderStatus, searchPage);
                return;
            }

            var orderType = Main.getNamedParameter('order');
            if (orderType == "past") {
                this.showPastOrders();
            } else if (orderType == "cancelled") {
                this.showCancelledOrders();
            } else {
                //this.showCurrentOrders(true);
                this.findActiveOrderTab();
            }


            this.ui.googleMapsWrapperRoute.addClass('hidden');

        },

        events: function () {
            var that = this;

            this.ui.searchOrderForm.on('submit', function (e) {
                e.preventDefault();
                var searchParams = "";
                var searchValue = that.ui.searchOrderInput.val();
                if (!searchValue) {
                    return;
                }
                searchParams += 'searchKey='+searchValue;
                var searchStatus = that.ui.searchOrderInput.attr('data-type');
                if (searchStatus) {
                    searchParams += '&searchOrderStatus='+searchStatus;
                }
                window.location.href = pageContext + "smanager/order_list?" + searchParams;
            });

            this.ui.searchOrderStatusDropdown.on('click', '.dropdown-menu li a', function(event) {
                $(this).parent().addClass('active').siblings().removeClass('active');
                that.ui.searchOrderInput.attr({
                    'data-type': $(this).attr('data-type'),
                    'placeholder': 'Search ' + $(this).text() + ' ...'
                });
            });

            this.ui.orderDetailModal.on('show.modal.bs', function (e) {
                var btn = $(e.relatedTarget);
                var orderId = btn.attr('data-orderid');
                $(this).attr('data-orderid', orderId);
                if (btn.attr('data-status') === 'successful') {
                    $(this).attr('data-status', 'successful');
                }
                that.getOrderDetails(orderId);

            }).on('hide.modal.bs', function () {
                var orderId = $(this).attr('data-orderid');
                $(this).removeAttr('data-orderid');
                $(this).find('#reAssignDriver').removeAttr('data-orderid').removeAttr('data-storeid');
                $(this).find('#cancelOrderBtn').removeAttr('data-orderid');
                $(this).find('.deliver-recipient').addClass('hidden');
                $('#modalMap').css('height', 0);

                //console.log(that.ui.googleMapsWrapperRoute.is(':visible'));
                //console.log(that.ui.googleMapsWrapper.is(':visible'));

                if (that.ui.googleMapsWrapperRoute.is(':visible')) {
                    $(document).find('a[data-type="showInMaps"][data-orderid="' + orderId + '"]').click();
                }


            }).on('shown.modal.bs', function () {
                var modalHeight = ($(this).find('.modal-body').height());
                $('#modalMap').css('height', modalHeight);
            });

            this.ui.driverCurrentOrderModal.on('show.modal.bs', function (event) {
                var elem = $(event.relatedTarget);
                $(this).attr({
                    'data-driver': elem.attr('data-driver'),
                    'data-driverName': elem.attr('data-driverName')
                });
                that.getDriverCurrentOrders($(this).attr('data-driver'));

                $(this).one('click', '.btn[data-toggle="modal"]', function () {
                    that.ui.driverCurrentOrderModal.modal('hide');
                });

            }).on('hide.modal.bs', function (event) {
                $(this).attr('data-driver', '');
                that.ui.driverCurrentOrderList.html("");
            });

            this.ui.orderDetailModal.on('click', '#viewSignature', function (event) {
                event.preventDefault();
                that.ui.orderDetailModal.find('.signature-image').toggleClass('hidden');
            });

            this.ui.driverAssignModal.on('show.modal.bs', function (e) {
                that.ui.assignOwnDriverBtn.attr('disabled', 'disabled');

                var btn = $(e.relatedTarget);
                var storeId = btn.attr('data-storeid');
                var orderId = btn.attr('data-orderid');

                var type = btn.attr('data-type');
                var storeDriver = btn.attr('data-storeDriver'); 

                if (Main.getFromLocalStorage('userRole') === "ROLE_CSR" || storeDriver === 'false') {
                    $('#hideForCSR').remove();
                    that.ui.anyOrderDriverBox.parent().removeClass('col-md-6').addClass('col-md-12');
                } else {
                    setTimeout(function () {
                        var customerAreaid = that.ui.driverAssignModal.attr('data-customerareaid');
                        that.getDriversByStore(storeId, customerAreaid);
                    }, 500);
                }

                $(this).attr('data-storeid', storeId).attr('data-type', type).attr('data-orderid', orderId);

                // that.setUpMapAssignModal(orderId);
                that.ui.bookAnyOrderDriverBtn.click();

                that.ui.orderDetailModal.modal('hide');

            }).on('hidden.modal.bs', function () {
                var orderId = $(this).attr('data-orderid');
                $(this).removeAttr('data-storeid').removeAttr('data-type').removeAttr('data-driverid').removeAttr('data-orderid').removeAttr('data-customerareaid');
                that.ui.confirmAnyOrderDriverBtn.attr('disabled', 'disabled');
                that.ui.ownDriverWrapper.html("");
                that.ui.anyOrderDriverWrapper.html("");

                // that.ui.mapCanvasModal.show();
                // that.ui.bookAnyOrderDriverBtn.show();

                if (that.ui.googleMapsWrapperRoute.is(':visible')) {
                    $(document).find('a[data-type="showInMaps"][data-orderid="' + orderId + '"]').click();
                }


            }).on('change', this.ui.ownDriverRadio.selector, function (e) {
                var radio = $(e.target);
                that.ui.assignOwnDriverBtn.removeAttr('disabled');
                that.ui.driverAssignModal.attr('data-driverid', radio.attr('value'));
            }).on('change', this.ui.anyOrderDriverRadio.selector, function () {
                that.ui.confirmAnyOrderDriverBtn.removeAttr('disabled');
            });

            this.ui.assignOwnDriverForm.submit(function (e) {
                e.preventDefault();

                var orderId = that.ui.driverAssignModal.attr('data-orderid');
                var driverId = that.ui.driverAssignModal.attr('data-driverid');
                var type = that.ui.driverAssignModal.attr('data-type');

                if (orderId && driverId) {
                    that.assignDriver(orderId, driverId);
                }
            });


            $(window).resize(function () {
                that.fixContentHeights();
            });

            //<--- Main Map Load Here --->
            $(window).load(function () {

                var myOptions1 = {
                    zoom: 8,
                    center: new google.maps.LatLng(25.18, 55.20),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapCanvas: "map",
                };

                ordersMainMap = orderMapsModule.init(myOptions1);

                var input = document.getElementById('pac-input');
                ordersMainMap.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

                var toggleWrapper = document.getElementById('mapToggles');
                ordersMainMap.controls[google.maps.ControlPosition.LEFT_CENTER].push(toggleWrapper);


                that.fixContentHeights();
            });
            //<--- Main Map Load Here --->

            $(".dropdown-menu.changevalue").on('click', 'li a', function () {
                $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });

            this.ui.orderSort2.find('li a').on('click', function () {
                var sortBy = $(this).attr('data-sortby');
                var direction = $(this).attr('data-direction');

                if (typeof(sortBy) == "undefined" || typeof(direction) == "undefined") {
                    return;
                }

                if (sortBy != "createdDate") {
                    if (direction == "asc") {
                        $(this).attr('data-direction', "desc");
                    } else {
                        $(this).attr('data-direction', "asc");
                    }
                }

                /*if (curOrderType === "CurrentOrders") {
                 var hash = Main.getURLhash();
                 hash = hash[0];

                 that.getOrdersByStatus(orderType, curWrapper, "desc", "createdDate");
                 if (hash === "orderListRunningTab") {
                 //that.getOrdersByStatus("PROCESSING", that.ui.orderRunningWrapper, direction, sortBy);
                 } else {
                 //that.getOrdersByStatus("WAITING", that.ui.orderWaitingWrapper, direction, sortBy);
                 }

                 } else {
                 that.getOrdersByStatus(curOrderType, curWrapper, direction, sortBy);
                 }*/

                that.getOrdersByStatus(curOrderType, curWrapper, direction, sortBy);
            });


            this.ui.driverSort2.find('li a').on('click', function () {
                var sortBy = $(this).attr('data-sortby');
                var direction = $(this).attr('data-direction');

                if (typeof(sortBy) == "undefined" || typeof(direction) == "undefined") {
                    return;
                }

                if (direction == "asc") {
                    $(this).attr('data-direction', "desc");
                } else {
                    $(this).attr('data-direction', "asc");
                }

                //that.getDriversByStatus(sortBy, direction);
            });

            this.ui.orderTypeCurrentLink.on('click', function () {
                that.showCurrentOrders();
                //that.configOrderList();
                //that.findActiveOrderTab();
            });
            this.ui.orderTypePastLink.on('click', function () {
                that.showPastOrders();
            });
            this.ui.orderTypeCancelledLink.on('click', function () {
                that.showCancelledOrders();
            });

            this.ui.bookAnyOrderDriverBtn.on('click', function () {
                var orderId = that.ui.driverAssignModal.attr('data-orderid');
                var storeId = that.ui.driverAssignModal.attr('data-storeid');
                that.ui.anyOrderDriverBox.removeClass('hidden');
                if (orderId !== "" && storeId !== "") {
                    that.assignDriver(orderId, null);
                }
            });

            // this.ui.cancelAnyOrderDriverBtn.on('click', function () {
            //     $('input[name="anyOrderDriverRadio"][type="radio"]').prop('checked', false);
            //     that.ui.anyOrderDriverBox.addClass('hidden');
            //     // that.ui.mapCanvasModal.show();
            //     // that.ui.bookAnyOrderDriverBtn.show();

            // });

            this.ui.confirmAnyOrderDriverBtn.on('click', function () {
                var orderId = that.ui.driverAssignModal.attr('data-orderid');
                var driverId = $('input[name="anyOrderDriverRadio"][type="radio"]:checked').val();
                if (driverId) {
                    that.assignDriver(orderId, driverId);
                }
            });

            $(document).on('click', this.ui.dispatchButton.selector, function (e) {
                var link = $(e.target);
                var orderId = link.attr('data-orderid');
                if (orderId) {
                    that.dispatchOrder(orderId);
                }
            });

            $(document).on('click', this.ui.orderListBlackWrapper.selector, function (e) {
                e.stopPropagation();
                $(e.target).find('a[data-type="showInMaps"]').click();
            });


            $(document).on('click', this.ui.viewInMapsLink.selector, function (e) {
                var link = $(e.target);
                var orderId = link.attr('data-orderid');
                var storeLat = link.attr('data-store-lat');
                var storeLng = link.attr('data-store-lng');
                var customerLat = link.attr('data-customer-lat');
                var customerLng = link.attr('data-customer-lng');
                var driverLat = link.attr('data-driver-lat');
                var driverLng = link.attr('data-driver-lng');
                var brandName = link.attr('data-brand-name');
                var customerName = link.attr('data-customer-name');
                var driverName = link.attr('data-driver-name');
                var orderDisplayId = link.attr('data-order-display-id');

                if (orderId) {
                    var loc_markers = {};
                    if (!isNaN(Number(storeLat)) && !isNaN(Number(storeLng))) {
                        loc_markers.store = {
                            orderId: orderDisplayId,
                            lat: Number(storeLat),
                            lng: Number(storeLng),
                            title: brandName,
                            icon: pageContext + "resources/custom/images/map-ico/map_merchant.png"
                        };
                    }

                    if (!isNaN(Number(customerLat)) && !isNaN(Number(customerLng))) {
                        loc_markers.customer = {
                            orderId: orderDisplayId,
                            lat: Number(customerLat),
                            lng: Number(customerLng),
                            title: customerName,
                            icon: pageContext + "resources/custom/images/map-ico/map_customer.png"
                        };
                    }

                    if (!isNaN(Number(driverLat)) && !isNaN(Number(driverLng))) {
                        loc_markers.driver = {
                            orderId: orderDisplayId,
                            lat: Number(driverLat),
                            lng: Number(driverLng),
                            title: driverName,
                            icon: pageContext + "resources/custom/images/map-ico/map_store_driver.png"
                        };
                    }


                    that.showOrderInMap(loc_markers);
                }
            });

            this.ui.closeSearchBtn.on('click', function () {
                window.location.href = pageContext + 'smanager/order_list';
            });


            this.ui.driverTabContent.on('click', this.ui.driverNameLink.selector, function (e) {
                var link = $(e.target);
                var name = link.text();
                that.ui.searchDriverInMap.val(name).trigger('focus');
            });


            this.ui.searchDriverInMap.on('focus', function () {
                //change the lookup db after changing the tabs...
                that.ui.searchDriverInMap.autocomplete('setOptions', {
                    lookup: (that.ui.driverTabContent.find('div.tab-pane.active').attr('id') === "driverListDutyOnTab") ? searchDriverDBon : searchDriverDBoff
                });
            });

            this.ui.cancelOrderModel.on('show.modal.bs', function (e) {
                var link = $(e.relatedTarget);
                var orderId = link.attr('data-orderid');
                $(this).attr('data-orderid', orderId);
            }).on('hidden.modal.bs', function () {
                that.ui.reasonsForCancel.val("");
                $(this).removeAttr('data-orderid');
            });

            this.ui.refreshOrdersListBtn.on('click', function () {
                $('#orderFilterRecent').click();
            });

            this.ui.refreshDriversListBtn.on('click', function () {
                that.getDriversByStatus();
            });

            this.ui.hideDriverListLink.on('click', function () {
                that.showHideDriver('hide');
            });

            this.ui.hideOrderListLink.on('click', function () {
                that.showHideOrders('hide');
            });

            this.ui.showDriverListLink.on('click', function () {
                that.showHideDriver('show');
            }).hide();

            this.ui.showOrderListLink.on('click', function () {
                that.showHideOrders('show');
            }).hide();


            $('#driverTabsWrapper a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                that.changeDriverListTab();
            });

            $(document).on('maps.ready', function () {
                that.changeDriverListTab();
            });

            $('a[href="#orderListRunningTab"]').on('click', function () {
                if ($('#orderListRunning .tab-content>.active').length === 0) {
                    $('#runningOrdersTabs li:first a').click();
                }
            });

            $('a[href="#orderListWaitingTab"]').on('click', function () {
                if ($('#orderListWaiting .tab-content>.active').length === 0) {
                    $('#waitingOrdersTabs li:first a').click();
                }
            });

            $('#rootOrderTabs a[data-toggle="tab"], #waitingOrdersTabs a[data-toggle="tab"], #runningOrdersTabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                that.configOrderList();
            });

            $('#closeLinkedMapsBtn').on('click', function () {
                that.ui.googleMapsWrapper.removeClass('hidden');
                that.ui.googleMapsWrapperRoute.addClass('hidden');

                if (typeof(google) !== "undefined") {
                    google.maps.event.trigger(superMap, 'resize');
                }
            });

            $(document).on('maps.ready', function () {
                that.getStoreList();
            });

            $(document).on('maps.marker.clicked', function (e, data) {
                if (data.driverInfo) {
                    if (data.driverInfo.driverId) {
                        var driverId = data.driverInfo.driverId;
                        var listItem = that.ui.driverTabContent.find('.driver-list-item[data-id="' + driverId + '"]');
                        that.scrollDriver(listItem);
                    }
                }
            });


            this.ui.storesCheckbox.on('click', function () {
                that.toggleMarkers('Store');
            });

            this.ui.driversCheckbox.on('click', function () {
                that.toggleMarkers('Driver');
            });

            this.ui.assignDriverSearch.on('keyup', function(event) {
                event.preventDefault();
                var driverType = $(event.target).attr('data-type');
                var searchTerm = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$';

                var reg = RegExp(searchTerm, 'i');
                var selector = (driverType === 'store') ? that.ui.ownDriverWrapper : that.ui.anyOrderDriverWrapper;

                var drivers = selector.children('label').removeClass('hidden').filter(function(index) {
                    var id = $(this).attr('data-id');
                    return !reg.test(id);
                }).addClass('hidden');
            });

            // Order List Pager
            $(document).on('click', '.js-order-pager a', function(event) {
                event.preventDefault();
                var data = $(this).closest('.js-order-pager').data();
                var pageNumber = data.page;
                var liClass = $(this).parent().attr('class');
                if (liClass === 'next') {
                    pageNumber++;
                } else if (liClass === 'previous') {
                    if (pageNumber <= 1) {
                        pageNumber = 1;
                    } else {
                        pageNumber--;
                    }
                }
                var page = {
                    pageNumber: pageNumber
                };
                if (data.order === 'DELIVERED') {
                    that.getOrdersByStatus("DELIVERED", that.ui.pastOrderWrapper.find('.ordersHere'), "desc", "createdDate", null, page);
                    return;
                }
                if (data.order === 'CANCELLED') {
                    that.getOrdersByStatus("CANCELLED", that.ui.cancelledOrderWrapper.find('.ordersHere'), "desc", "createdDate", null, page);
                    return;
                }
                if (data.order === 'searchResults') {
                    var searchKey = Main.getNamedParameter('searchKey') || "";
                    var searchParams = Main.getNamedParameter('searchOrderStatus') || "";
                    that.searchOrder(searchKey, searchParams, page);
                }

            });

        },

        findActiveOrderTab: function () {
            var urlHash = Main.getURLhash();
            urlHash = urlHash[0];
            switch (urlHash) {
                case "orderListWaitingTab-orderPlaced":
                    $('a[href="#orderListWaitingTab"]').click();
                    $('a[href="#orderListWaitingTab-orderPlaced"]').click();
                    break;

                case "orderListWaitingTab-orderStarted":
                    $('a[href="#orderListWaitingTab"]').click();
                    $('a[href="#orderListWaitingTab-orderStarted"]').click();
                    break;

                case "orderListRunningTab-inRouteToPickUp":
                    $('a[href="#orderListRunningTab"]').click();
                    $('a[href="#orderListRunningTab-inRouteToPickUp"]').click();
                    break;

                case "orderListRunningTab-atStore":
                    $('a[href="#orderListRunningTab"]').click();
                    $('a[href="#orderListRunningTab-atStore"]').click();
                    break;

                case "orderListRunningTab-inRouteToDelivery":
                    $('a[href="#orderListRunningTab"]').click();
                    $('a[href="#orderListRunningTab-inRouteToDelivery"]').click();
                    break;

                case "orderListRunningTab":
                    $('#runningOrdersTabs li:first a').click();
                    break;
                case "orderListWaitingTab":
                    $('#waitingOrdersTabs li:first a').click();
                    break;

                default:
                    $('a[href="#orderListWaitingTab"]').click();

            }

            //this.configOrderList();


        },

        configOrderList: function () {
            var rootTab = $('#currentOrderWrapper div#ordersTabs>div.active').attr('id');
            var childTab = $('#' + rootTab + ' div.tab-content>div.active').attr('id');
            if (!childTab) {
                return;
            }
            var that = this;
            var orderType = "";
            var wrapper;

            that.ui.currentOrderWrapper.show();
            that.ui.pastOrderWrapper.hide();
            that.ui.cancelledOrderWrapper.hide();
            that.ui.searchOrderWrapper.hide();
            that.fixContentHeights();

            /*console.log(rootTab);
             console.log(childTab);*/


            switch (childTab) {
                case "orderListWaitingTab-orderPlaced":
                    orderType = "ORDER_PLACED";
                    break;

                case "orderListWaitingTab-orderStarted":
                    orderType = "ORDER_STARTED";
                    break;

                case "orderListRunningTab-inRouteToPickUp":
                    orderType = "EN_ROUTE_TO_PICK_UP";
                    break;

                case "orderListRunningTab-atStore":
                    orderType = "AT_STORE";
                    break;

                case "orderListRunningTab-inRouteToDelivery":
                    orderType = "EN_ROUTE_TO_DELIVER";
                    break;
            }


            curWrapper = $('#' + childTab).find('ul');

            curOrderType = orderType;

            that.getOrdersByStatus(orderType, curWrapper, "desc", "createdDate");


        },

        changeDriverListTab: function () {
            var target = this.ui.driverTabContent.find('div.active').attr('id');
            if (target === "driverListDutyOnTab") {
                this.showMarkersInMap(onDutyDriversLocation);
            } else {
                this.showMarkersInMap(offDutyDriversLocation);
            }
        },

        showHideDriver: function (option) {
            var that = this;
            if (option === "hide") {
                this.ui.colDriverListWrapper.hide();

                if (this.ui.colOrderListWrapper.is(':visible')) {
                    this.ui.colMapsWrapper.removeClass('col-lg-6').addClass('col-lg-9');
                } else {
                    this.ui.colMapsWrapper.removeClass('col-lg-6').removeClass('col-lg-9').addClass('col-lg-12');
                }

                this.ui.showDriverListLink.show();
            } else {

                if (this.ui.colOrderListWrapper.is(':visible')) {
                    this.ui.colMapsWrapper.removeClass('col-lg-12').removeClass('col-lg-9').addClass('col-lg-6');
                } else {
                    this.ui.colMapsWrapper.removeClass('col-lg-12').removeClass('col-lg-6').addClass('col-lg-9');
                }

                this.ui.colDriverListWrapper.show();
                this.ui.showDriverListLink.hide();
            }

            if (typeof(google) !== "undefined") {
                google.maps.event.trigger(ordersMainMap, 'resize');
            }
        },


        showHideOrders: function (option) {
            var that = this;
            if (option === "hide") {
                this.ui.colOrderListWrapper.hide();

                if (this.ui.colDriverListWrapper.is(':visible')) {
                    this.ui.colMapsWrapper.removeClass('col-lg-6').addClass('col-lg-9');
                } else {
                    this.ui.colMapsWrapper.removeClass('col-lg-6').removeClass('col-lg-9').addClass('col-lg-12');
                }


                this.ui.showOrderListLink.show();
            } else {

                if (this.ui.colDriverListWrapper.is(':visible')) {
                    this.ui.colMapsWrapper.removeClass('col-lg-9').addClass('col-lg-6');
                } else {
                    this.ui.colMapsWrapper.removeClass('col-lg-12').removeClass('col-lg-6').addClass('col-lg-9');
                }

                this.ui.colOrderListWrapper.show();
                this.ui.showOrderListLink.hide();
            }

            if (typeof(google) !== "undefined") {
                google.maps.event.trigger(ordersMainMap, 'resize');
            }
        },


        /*setUpMapAssignModal: function (orderId) {
            var that = this;
            var mapAssign;


            var callback = function (status, data) {
                if (data.success) {
                    var brandName = "",
                        storeLocation1 = "",
                        storeLocation2 = "",
                        storeNumber = "",
                        customerFirstName = "",
                        customerLastName = "",
                        customerLocation1 = "",
                        customerLocation2 = "",
                        customerNumber = "",
                        storeLat = "",
                        storeLng = "",
                        driverLat = "",
                        driverLng = "",
                        customerLat = "",
                        customerLng = "",
                        store = {},
                        customer = {},
                        user = {},
                        driver = {},
                        customerArea = {},
                        markers = {};
                    var orderDetails = data.params.order;

                    if (!$.isEmptyObject(orderDetails)) {
                        if (!$.isEmptyObject(orderDetails.store)) {
                            store = orderDetails.store;
                            brandName = Main.fixUndefinedVariable(store.storeBrand.brandName);
                            storeLocation1 = Main.fixUndefinedVariable(store.givenLocation1);
                            storeLocation2 = Main.fixUndefinedVariable(store.givenLocation2);
                            storeNumber = Main.fixUndefinedVariable(store.contactNo);
                            storeLat = Main.fixUndefinedVariable(store.latitude);
                            storeLng = Main.fixUndefinedVariable(store.longitude);
                            if (storeLat !== "" && storeLng !== "") {
                                markers.store = {
                                    lat: Number(storeLat),
                                    lng: Number(storeLng),
                                    title: brandName + "<Br>" + storeLocation1 + " " + storeLocation2 + "<br>" + storeNumber,
                                    icon: pageContext + "resources/custom/images/map-ico/map_merchant.png"
                                };
                            }

                            if (store.allowStoresDriver === false) {
                                that.ui.assignOwnDriverForm.parent().addClass('hidden');
                                that.ui.anyOrderDriverBox.parent().removeClass('col-md-6').addClass('col-md-12');
                            } else {
                                that.ui.assignOwnDriverForm.parent().removeClass('hidden');
                                that.ui.anyOrderDriverBox.parent().addClass('col-md-6').removeClass('col-md-12');
                            }


                        }

                        if (!$.isEmptyObject(orderDetails.customer)) {
                            customer = orderDetails.customer;

                            if (!$.isEmptyObject(customer.user)) {
                                user = customer.user;
                                customerFirstName = Main.fixUndefinedVariable(user.firstName);
                                customerLastName = Main.fixUndefinedVariable(user.lastName);
                                customerNumber = Main.fixUndefinedVariable(user.mobileNumber);
                            }
                        }

                        if (!$.isEmptyObject(orderDetails.customersArea)) {
                            customerArea = orderDetails.customersArea;

                            customerLocation1 = Main.fixUndefinedVariable(customerArea.street);
                            //customerLocation2 = Main.fixUndefinedVariable(customerArea.area.areaName);
                            if (typeof(customerArea.area) != "undefined") {
                                customerLocation2 = Main.fixUndefinedVariable(customerArea.area.areaName);
                                customerLat = Main.fixUndefinedVariable(customerArea.area.latitude);
                                customerLng = Main.fixUndefinedVariable(customerArea.area.longitude);
                                that.ui.driverAssignModal.attr('data-customerareaid', customerArea.area.areaId);
                            } else {
                                customerLocation2 = "";
                            }


                            if (customerLat !== "" && customerLng !== "") {
                                markers.customer = {
                                    lat: Number(customerLat),
                                    lng: Number(customerLng),
                                    title: customerFirstName + " " + customerLastName + "<Br>" + customerLocation1 + " " + customerLocation2 + "<br>" + customerNumber,
                                    icon: pageContext + "resources/custom/images/map-ico/map_customer.png"
                                };
                            }
                        }

                        if (!$.isEmptyObject(orderDetails.driver)) {
                            driver = orderDetails.driver;
                            driverLat = Main.fixUndefinedVariable(driver.latitude);
                            driverLng = Main.fixUndefinedVariable(driver.longitude);

                            if (driverLat !== "" && driverLng !== "") {
                                markers.driver = {
                                    lat: Number(driverLat),
                                    lng: Number(driverLng),
                                    title: Main.fixUndefinedVariable(driver.user.firstName) + " " + Main.fixUndefinedVariable(driver.user.lastName),
                                    icon: pageContext + "resources/custom/images/map-ico/map_store_driver.png"
                                };
                            }
                        }


                    }
                    var mapConfigAssign = {
                        zoom: 14,
                        center: new google.maps.LatLng(28.3949, 84.1240),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapCanvas: "mapCanvasModal",
                        mapVar: mapAssign
                    };


                    setTimeout(function () {
                        linkedMarkersModule.init(mapConfigAssign);
                        linkedMarkersModule.setMarkers([markers]);
                    }, 500);

                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };

            callback.requestType = "GET";
            var url = pageContext + "smanager/get_order_detail";

            Main.request(url, {}, callback, {orderId: orderId});
        },*/

        showCurrentOrders: function (loadBoth) {
            var that = this;
            curOrderType = "CurrentOrders";
            that.ui.currentOrderWrapper.show();
            that.ui.pastOrderWrapper.hide();
            that.ui.cancelledOrderWrapper.hide();
            that.ui.searchOrderWrapper.hide();
            that.fixContentHeights();

            that.configOrderList();
            if (loadBoth) {
                //that.getOrdersByStatus("PROCESSING", that.ui.orderRunningWrapper, "desc", "createdDate");
                //that.getOrdersByStatus("WAITING", that.ui.orderWaitingWrapper, "desc", "createdDate");
                return;
            }

            var hash = Main.getURLhash();
            hash = hash[0];
            if (hash === "orderListRunningTab") {
                //that.getOrdersByStatus("PROCESSING", that.ui.orderRunningWrapper, "desc", "createdDate");
            } else {
                //that.getOrdersByStatus("WAITING", that.ui.orderWaitingWrapper, "desc", "createdDate");
            }


        },
        showPastOrders: function () {
            var that = this;
            curOrderType = "DELIVERED";
            curWrapper = that.ui.pastOrderWrapper.find('.ordersHere');
            //curCountWrapper = that.ui.pastOrderWrapper.find('.counterHere');
            that.ui.currentOrderWrapper.hide();
            that.ui.pastOrderWrapper.show();
            that.ui.cancelledOrderWrapper.hide();
            that.ui.searchOrderWrapper.hide();
            that.fixContentHeights();

            var page = {
                pageSize: 100,
                pageNumber: 1
            };

            that.getOrdersByStatus("DELIVERED", that.ui.pastOrderWrapper.find('.ordersHere'), "desc", "createdDate", null, page);

        },

        showCancelledOrders: function () {
            var that = this;
            curOrderType = "CANCELLED";
            curWrapper = that.ui.cancelledOrderWrapper.find('.ordersHere');
            //curCountWrapper = that.ui.cancelledOrderWrapper.find('.counterHere');
            that.ui.currentOrderWrapper.hide();
            that.ui.pastOrderWrapper.hide();
            that.ui.cancelledOrderWrapper.show();
            that.ui.searchOrderWrapper.hide();
            that.fixContentHeights();

            var page = {
                pageSize: 100,
                pageNumber: 1
            };

            that.getOrdersByStatus("CANCELLED", that.ui.cancelledOrderWrapper.find('.ordersHere'), "desc", "createdDate", null, page);
        },

        addTemplate: function (orderStatus) {
            var template = '<li class="flex-parent order-list-item-new positionrelative opacity20"> \
                <div class="flex-child w20p"> \
                    <div class="text-center time-elapsed-wrapper order-list-error"> \
                        <span class="font24 c-b-body ph5"></span> \
                        <br> \
                        <span class="font12 c-b-body ph25 w100p"></span> \
                    </div> \
                </div> \
                <div class="flex-child w80p"> \
                    <div class="flex-parent "> \
                        <div class="flex-child w10p"> \
                            <div class="ico text-center"> \
                                <i class="aoicon aoicon-store"></i> \
                            </div> \
                        </div> \
                        <div class="flex-child w90p"> \
                            <div class="font18 c-b-gray-light w100p p5"></div> \
                        </div> \
                    </div> \
                    <div class="flex-parent c-t-blue-sky"> \
                        <div class="flex-child w10p"> \
                            <div class="ico text-center"> \
                                <i class="fa fa-map-marker"></i> \
                            </div> \
                        </div> \
                        <div class="flex-child w90p"> \
                            <div class="c-b-blue-light w100p p5"></div> \
                        </div> \
                    </div> \
                </div> \
            </li>';

            var selectors = '.ordersHere, #currentOrderWrapper .tab-pane > ul';
            $(selectors).empty();
            if (orderStatus) {
                selectors = ' ul[data-type="'+orderStatus+'"] ';
            }
            $(selectors).html(template);
        },

        fixContentHeights: function () {

            var orderTab = this.ui.ordersTabs,
                driverTab = this.ui.driverTabContent,
                maps = this.ui.googleMapsWrapper,
                windowHeight = $(window).height(),
                ordersTabHeight = windowHeight - orderTab.offset().top,
                driversTabHeight = windowHeight - driverTab.offset().top,
                mapsHeight = windowHeight - maps.offset().top;

            if (parseInt(orderTab.css('height')) !== ordersTabHeight) {
                orderTab.css({
                    height: ordersTabHeight,
                    overflow: 'auto'
                });
            }

            if (parseInt(driverTab.css('height')) !== driversTabHeight) {
                driverTab.css({
                    height: driversTabHeight,
                    overflow: 'auto'
                });
            }

            if (parseInt(maps.css('height')) !== mapsHeight) {
                maps.css({
                    height: mapsHeight
                });

                this.ui.googleMapsWrapperRoute.css({
                    height: mapsHeight
                });
            }

            if ($('.ordersHere:visible').length) {
                $('.ordersHere').css('height', windowHeight - $('.ordersHere:visible').offset().top).css('overflow', 'auto');
            }

        },

          getPagination: function(pageNumber, totalPage, orderStatus) {
            
            if (!pageNumber || !totalPage || !orderStatus) {
                return;
            }
            var maxPageNumber = Math.ceil(totalPage / 100);
            var template = '<nav class="js-order-pager" data-order="'+orderStatus+'" data-page="'+pageNumber+'" aria-label="Order List Pager">';
            template += '<ul class="pager"> <li class="previous';
            if (pageNumber <= 1) {
                template += ' disabled ';
            }
            template += '"><a href="#"><span aria-hidden="true">&larr;</span> Prev</a></li>';
            template += '<li class="text-center line30"> <strong>' + pageNumber + '</strong> of ' + maxPageNumber + ' </li>';
            template += '<li class="next';
            if (pageNumber >= maxPageNumber) {
                template += ' disabled ';
            }
            template += '"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li> </ul> </nav>';

            return template;
        },

        getOrdersByStatus: function (orderStatus, orderWrapper, sortdirection, sortBy, orderData, pageInfo) {
            var that = this;
            that.addTemplate(orderStatus);
            that.getOrderCounts();
            var callback = function (status, data) {
                if (data.success) {
                    var orders = data.params.orderList;
                    var totalNumberofData = orders.numberOfRows;
                    //countWrapper.html(orders.numberOfRows);

                    var orderListTmpl = "";

                    if (orderStatus === 'searchResults') {
                        that.ui.searchOrderWrapper.find('.counterHere').text(totalNumberofData);
                    }

                    if (orderData && orders.data.length === 0) {
                        return;
                    }

                    $.each(orders.data, function (index, data) {
                        var brandName = "",
                            storeLocation1 = "",
                            storeLocation2 = "",
                            customerFirstName = "",
                            customerLastName = "",
                            customerLocation1 = "",
                            customerLocation2 = "",
                            driverFirstName = "",
                            driverLastName = "",
                            driverContactNo = "",

                            orderDisplayId = "",
                            orderCreatedDateTime = "",
                            deliveredDateTime = "",
                            orderDeliveryTime = "",
                            orderDeliveryTimeUnit = "",
                            orderId = "",
                            storeId = "",
                            store = {},
                            customer = {},
                            user = {},
                            driver = {},
                            customerArea = {},
                            grayScaleClass = "",

                            orderStatusClass = "",
                            inRouteToPickUpOrders = "",
                            inRouteToPickUpCount = 0,
                            atStoreOrders = "",
                            atStoreCount = 0,
                            inRouteToDeliveryOrders = "",
                            inRouteToDeliveryCount = 0,

                            driverLat, driverLng,
                            storeLat, storeLng,
                            customerLat, customerLng,
                            showStoreDriver = false;

                        var driverDetails = "", assignBtn = "";
                        var orderListTemp = "";
                        var orderTypeForSearch = "";
                        if (!$.isEmptyObject(data)) {


                            orderId = Main.fixUndefinedVariable(data.orderId);

                            orderDisplayId = Main.fixUndefinedVariable(data.orderDisplayId);
                            orderCreatedDateTime = Main.fixUndefinedVariable(data.createdDate, 'Not Available');
                            //console.log(orderCreatedDateTime);
                            var remDateTime;
                            if (new Date(orderCreatedDateTime).getTime() > Date.now())
                            //fix order created date in future(at dubai)
                                remDateTime = moment(new Date(orderCreatedDateTime).getTime() - 110 * 60 * 1000).toNow().split(' ');
                            else
                                remDateTime = moment(orderCreatedDateTime).toNow().split(' ');

                            //console.log(remDateTime);

                            orderDeliveryTime = Main.fixUndefinedVariable((remDateTime[1] == "a" || remDateTime[1] == "an") ? "1" : remDateTime[1]);

                            orderDeliveryTimeUnit = (remDateTime[2] == "few") ? remDateTime[2] + " " + remDateTime[3] : remDateTime[2];

                            orderDeliveryTimeUnit = Main.fixUndefinedVariable(orderDeliveryTimeUnit, 'Not Available');

                            /* console.log(orderDeliveryTime);
                             console.log(orderDeliveryTimeUnit);*/

                            deliveredDateTime = Main.fixUndefinedVariable(data.deliveredDate, 'Not Available');

                            if (orderDeliveryTimeUnit === "minutes") {
                                orderDeliveryTimeUnit = "mins";
                            }

                            orderStatusClass = "order-list-default"; //order-list-warning, order-list-error,

                            if (orderDeliveryTime > 20 && orderDeliveryTimeUnit === "minutes") {
                                orderStatusClass = "order-list-warning";
                            } else if (orderDeliveryTimeUnit.indexOf("day") > -1 || orderDeliveryTimeUnit.indexOf('hour') > -1 || orderDeliveryTimeUnit.indexOf('month') > -1 || orderDeliveryTimeUnit.indexOf('year') > -1) {
                                orderStatusClass = "order-list-error";
                            }

                            if (orderDeliveryTimeUnit === "day") {
                                orderDeliveryTime = "1";
                            }

                            if (orderDeliveryTime === "1" && orderDeliveryTimeUnit === "few seconds") {
                                orderDeliveryTime = "Few";
                                orderDeliveryTimeUnit = "seconds";
                            }

                            /*if (orderStatus === "DELIVERED") {
                             orderDeliveryTime = '<span class="font16">' + moment.duration(moment(deliveredDateTime).diff(moment(orderCreatedDateTime))).humanize() + '</span>';
                             orderDeliveryTimeUnit = "";
                             }*/

                            if (!$.isEmptyObject(data.store)) {
                                store = data.store;
                                storeId = Main.fixUndefinedVariable(store.storeId);
                                if (!$.isEmptyObject(store.storeBrand)) {
                                    brandName = Main.fixUndefinedVariable(store.storeBrand.brandName, 'Not Available');
                                } else {
                                    brandName = 'Not Available';
                                }

                                storeLocation1 = Main.fixUndefinedVariable(store.givenLocation1);
                                storeLocation2 = Main.fixUndefinedVariable(store.givenLocation2);

                                storeLat = store.latitude || store.area.latitude;
                                storeLng = store.longitude || store.area.longitude;

                                showStoreDriver = store.allowStoresDriver || false;
                            }

                            if (!$.isEmptyObject(data.customer)) {
                                customer = data.customer;

                                if (!$.isEmptyObject(customer.user)) {
                                    user = customer.user;
                                    customerFirstName = Main.fixUndefinedVariable(user.firstName);
                                    customerLastName = Main.fixUndefinedVariable(user.lastName);
                                    //console.log(customerFirstName);
                                    //console.log(customerLastName);
                                } else {
                                    customerFirstName = 'Not Available';
                                }
                            }

                            if (!$.isEmptyObject(data.customersArea)) {
                                customerArea = data.customersArea;

                                customerLocation1 = Main.fixUndefinedVariable(customerArea.street);
                                if (typeof(customerArea.area) != "undefined") {
                                    customerLocation2 = Main.fixUndefinedVariable(customerArea.area.areaName, 'Not Available');
                                } else {
                                    customerLocation2 = 'Not Available';
                                }

                                if (customerArea.latitude && customerArea.longitude) {
                                    customerLat = customerArea.latitude;
                                    customerLng = customerArea.longitude;
                                } else if (customerArea.area && customerArea.area.latitude && customerArea.area.longitude) {
                                    customerLat = customerArea.area.latitude;
                                    customerLng = customerArea.area.longitude;
                                }

                            }

                            if (typeof(data.driver) == "undefined") {
                                assignBtn = '<a href="#" class="btn-order-list  btn-sm text-uppercase flex-gridcell order-waiting-assign" ' +
                                    'data-toggle="modal" data-storeDriver="'+showStoreDriver+'" data-target="#driverAssignModal" data-storeid=' + storeId + ' ' +
                                    'data-orderId= ' + orderId + ' data-type="assign">Assign</a>';
                            } else {
                                driver = data.driver;

                                driverFirstName = Main.fixUndefinedVariable(driver.user.firstName);
                                driverLastName = Main.fixUndefinedVariable(driver.user.lastName);

                                if (driverFirstName === "" && driverLastName === "") {
                                    driverFirstName = 'Not Available';
                                }

                                driverContactNo = Main.fixUndefinedVariable(driver.user.mobileNumber, 'Not Available');

                                driverLat = driver.latitude;
                                driverLng = driver.longitude;

                                assignBtn = '<a href="#" class="btn-order-list  btn-sm text-uppercase flex-gridcell order-waiting-assign" ' +
                                    'data-toggle="modal" data-storeDriver="'+showStoreDriver+'" data-target="#driverAssignModal" data-storeid=' + storeId + ' ' +
                                    'data-orderId= ' + orderId + ' data-type="reAssign">Re-Assign</a>';
                                /*
                                 driverDetails += "        <div class=\"media\">";
                                 driverDetails += "            <div class=\"media-left media-top\">";
                                 driverDetails += "                <i class=\"fa fa-motorcycle\" aria-hidden=\"true\"><\/i>";
                                 driverDetails += "            <\/div>";
                                 driverDetails += "            <div class=\"media-body\">";
                                 driverDetails += "                <strong class=\"media-heading\">" + driverFirstName + " " + driverLastName + "<\/strong> <br>";
                                 driverDetails += "                <small> " + driverContactNo + "<\/small>";
                                 driverDetails += "            <\/div>";
                                 driverDetails += "        <\/div>";*/


                                driverDetails += "<div class=\"flex-parent \">";
                                driverDetails += "    <div class=\"flex-child w10p\">";
                                driverDetails += "        <div class=\"ico text-center\"> <i class=\"fa fa-motorcycle\" aria-hidden=\"true\"><\/i> <\/div>";
                                driverDetails += "    <\/div>";
                                driverDetails += "    <div class=\"flex-child w90p\">";
                                driverDetails += "        <div class=\"store-name \">" + driverFirstName + " " + driverLastName + "<\/div>";
                                driverDetails += "    <\/div>";
                                driverDetails += "<\/div>";


                                /*<div class="flex-parent ">
                                 <div class="flex-child w10p">
                                 <div class="ico text-center"> <i class="aoicon aoicon-store"></i> </div>
                                 </div>
                                 <div class="flex-child w90p">
                                 <div class="store-name font18">One Kathmandu</div>
                                 </div>
                                 </div>*/
                            }

                        }

                        if (orderStatus === "searchResults") {
                            orderTypeForSearch += "<div class=\"media\">";
                            orderTypeForSearch += "            <div class=\"media-left media-top\">";
                            orderTypeForSearch += "                <i class=\"fa fa-file-text\"><\/i>";
                            orderTypeForSearch += "            <\/div>";
                            orderTypeForSearch += "            <div class=\"media-body\">";
                            orderTypeForSearch += "                <strong class=\"media-heading\">Order Status<\/strong> <br>";
                            orderTypeForSearch += "                <small> " + Main.ucfirst(Main.globalReplace(Main.fixUndefinedVariable(data.orderStatus), '_', ' ')) + "<\/small>";
                            orderTypeForSearch += "            <\/div>";
                            orderTypeForSearch += "        <\/div>";
                        }

                        if (data.orderStatus == "DELIVERED" || data.orderStatus == "CANCELLED") {
                            assignBtn = "";
                        }

                        var dispatch = '<a href="#" class="btn-order-list btn-sm text-uppercase flex-gridcell order-waiting-assign" ' +
                            'data-orderId= ' + orderId + ' data-type="dispatchOrder">Dispatch</a>';

                        if (data.orderStatus != "AT_STORE") {
                            dispatch = "";
                        }


                        if (data.assignDriver === false) {
                            grayScaleClass = 'filter-grayscale';
                        } else {
                            grayScaleClass = '';
                        }

                        /*console.log(orderDeliveryTime);
                         console.log(orderDeliveryTimeUnit);*/

                        orderListTemp += "<li class=\"flex-parent order-list-item-new positionrelative " + grayScaleClass + " \" data-id=\"" + orderId + "\">";
                        orderListTemp += "    <div class=\"flex-child w20p\">";

                        orderListTemp += "        <div class=\"text-center time-elapsed-wrapper  " + orderStatusClass + " \">";
                        orderListTemp += "            <span class=\"time-duration font24 \">" + orderDeliveryTime + "<\/span><Br>";
                        orderListTemp += "            <span class=\"time-unit fontbold font12 text-capitalize \">" + orderDeliveryTimeUnit + " ago<\/span>";
                        orderListTemp += "        <\/div>";
                        orderListTemp += "    <\/div>";
                        orderListTemp += "    <div class=\"flex-child w80p\">";
                        orderListTemp += "        <div class=\"flex-parent \">";
                        orderListTemp += "            <div class=\"flex-child w10p\">";
                        orderListTemp += "                <div class=\"ico text-center\">";
                        orderListTemp += "                    <i class=\"aoicon aoicon-store\"><\/i>";
                        orderListTemp += "                <\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "            <div class=\"flex-child w90p\">";
                        orderListTemp += "                <div class=\"store-name font18\">" + brandName + "<\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "        <\/div>";
                        orderListTemp += "";
                        orderListTemp += "        <div class=\"flex-parent c-t-blue-sky\">";
                        orderListTemp += "            <div class=\"flex-child w10p\">";
                        orderListTemp += "                <div class=\"ico text-center\">";
                        orderListTemp += "                    <i class=\"fa fa-map-marker\"><\/i>";
                        orderListTemp += "                <\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "            <div class=\"flex-child w90p\">";
                        orderListTemp += "                <div class=\"customer-location text-ellipsis fontbold\">" + customerLocation1 + " " + customerLocation2 + "<\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "        <\/div>";
                        orderListTemp += driverDetails;
                        orderListTemp += "    <\/div>";
                        orderListTemp += "    <div class=\"text-center flex-grid flex-center invisible order-waiting-actions\">";
                        orderListTemp += '<a href="#" class="invisible btn-order-list btn-sm text-uppercase flex-gridcell" ' +
                            'data-orderId= ' + orderId + ' data-type="showInMaps" ' +
                            'data-order-display-id= "' + orderDisplayId + '"' +
                            'data-driver-lat="' + driverLat + '" data-driver-lng="' + driverLng + '" ' +
                            'data-store-lat="' + storeLat + '" data-store-lng="' + storeLng + '" ' +
                            'data-customer-lat="' + customerLat + '" data-customer-lng="' + customerLng + '" ' +
                            'data-customer-name="' + customerFirstName + ' ' + customerLastName + '" ' +
                            'data-brand-name="' + brandName + '" ' +
                            'data-driver-name="' + driverFirstName + ' ' + driverLastName + '">View in map</a>';

                        orderListTemp += "        <a href=\"#\"";
                        orderListTemp += "           class=\"btn-order-list  btn-sm text-uppercase flex-gridcell\"";
                        if (data.orderStatus === 'DELIVERED') {
                            orderListTemp += 'data-status="successful"';
                        }
                        orderListTemp += "           data-toggle=\"modal\" data-target=\"#orderDetailModal\" data-orderid=" + orderId + " data-storeid=" + store.storeId + ">";
                        orderListTemp += "            View Details <\/a>";
                        orderListTemp += assignBtn;
                        orderListTemp += dispatch;

                        orderListTemp += "    <\/div>";
                        orderListTemp += "<\/li>";

                        if (orderStatus === "PROCESSING") {
                            if (data.orderStatus === "EN_ROUTE_TO_PICK_UP" || data.orderStatus === "ORDER_STARTED") {
                                inRouteToPickUpOrders += orderListTemp;
                                inRouteToPickUpCount++;
                            } else if (data.orderStatus === "AT_STORE") {
                                atStoreOrders += orderListTemp;
                                atStoreCount++;
                            } else if (data.orderStatus === "EN_ROUTE_TO_DELIVER" || data.orderStatus === "AT_CUSTOMER") {
                                inRouteToDeliveryOrders += orderListTemp;
                                inRouteToDeliveryCount++;
                            }
                        } else {
                            orderListTmpl += orderListTemp;
                        }

                    });

                    if (orderStatus == "PROCESSING") {
                        orderWrapper.find('#inRouteToPickUp ul').html(inRouteToPickUpOrders || "<strong>No orders available.</strong>");
                        orderWrapper.find('#inRouteTopickUpCount').html(inRouteToPickUpCount);
                        orderWrapper.find('#atStore ul').html(atStoreOrders || "<strong>No orders available.</strong>");
                        orderWrapper.find('#atStoreCount').html(atStoreCount);
                        orderWrapper.find('#inRouteToDelivery ul').html(inRouteToDeliveryOrders || "<strong>No orders available.</strong>");
                        orderWrapper.find('#inRouteToDeliveryCount').html(inRouteToDeliveryCount);
                    } else {
                        if (orderStatus === 'DELIVERED' || orderStatus === 'CANCELLED' || orderStatus === 'searchResults') {
                            orderListTmpl += that.getPagination(pageInfo.pageNumber, totalNumberofData, orderStatus);
                        }
                        orderWrapper.html(orderListTmpl);
                        if (orderStatus === 'searchResults') {
                            orderWrapper.scrollTop(0);
                        }
                    }
                    
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
                that.fixContentHeights();
            };

            if (orderData) {
                callback("", orderData);
                return;
            }

            callback.requestType = "POST";
            if (fromSocket) {
                fromSocket = false;
            } else {
                callback.loaderDiv = that.ui.ordersTabs.selector;
            }

            var url = pageContext + 'smanager/order_list_by_status';//"smanager/get_orders_by_status";
            var data = {
                page: {
                    pageNumber: 1,
                    sortBy: sortBy,
                    sortOrder: sortdirection
                },
                user: {
                    userId: Main.getFromLocalStorage('userId')
                },
                orderStatuses: [orderStatus]
            };

            if (orderStatus === 'DELIVERED' || orderStatus === 'CANCELLED') {
                data.page.pageSize = 100;
            }
            if (pageInfo) {
                if (pageInfo.pageSize) {
                    data.page.pageSize = pageInfo.pageSize;
                }
                if (!pageInfo.pageNumber) {
                    pageInfo.pageNumber = 1;
                }
                data.page.pageNumber = pageInfo.pageNumber;
            }

            Main.request(url, data, callback, {});
        },

        getOrderDetails: function (orderId) {
            var that = this;
            var callback = function (status, data) {
                if (data.success) {
                    that.setUpOrderModel(data.params.order);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };

            callback.requestType = "GET";
            var url = pageContext + "smanager/get_order_detail";

            Main.request(url, {}, callback, {orderId: orderId});
        },

        setUpOrderModel: function (orderDetails) {
            var modal = this.ui.orderDetailModal;
            var brandName = "",
                storeLocation1 = "",
                storeLocation2 = "",
                storeNumber = "",
                customerFirstName = "",
                customerLastName = "",
                customerLocation1 = "",
                customerLocation2 = "",
                customerNumber = "",
                customerNote = "",
                orderStatus = "",
                orderDisplayId = "",
                orderCreatedDateTime = "",
                orderDeliveryTime = "",
                orderDeliveryTimeUnit = "",
                orderId = "",
                paymentMode = "",
                storeLat = "",
                storeLng = "",
                driverLat = "",
                driverLng = "",
                customerLat = "",
                customerLng = "",
                deliveryCharge = "",
                totalBillAmt = "",
                recipientName = "",
                recipientSignature = "",
                orderCancelReason = "",
                orderCancelledDate = "",
                orderCancelBlock = "",
                store = {},
                customer = {},
                user = {},
                driver = {},
                customerArea = {},
                markers = {};


            if (!$.isEmptyObject(orderDetails)) {
                orderId = Main.fixUndefinedVariable(orderDetails.orderId);
                orderDisplayId = Main.fixUndefinedVariable(orderDetails.orderDisplayId);
                orderCreatedDateTime = Main.fixUndefinedVariable(orderDetails.createdDate, 'Not Available');

                var remDateTime = moment(orderCreatedDateTime).toNow().split(' ');
                orderDeliveryTime = Main.fixUndefinedVariable((remDateTime[1] == "a") ? "1" : remDateTime[1]);
                orderDeliveryTimeUnit = (remDateTime[2] == "few") ? remDateTime[2] + " " + remDateTime[3] : remDateTime[2];
                orderDeliveryTimeUnit = Main.fixUndefinedVariable(orderDeliveryTimeUnit);

                customerNote = Main.fixUndefinedVariable(orderDetails.customersNoteToDriver, 'Not Available');
                paymentMode = Main.fixUndefinedVariable(orderDetails.paymentMode, 'Not Available');
                orderStatus = Main.fixUndefinedVariable(Main.globalReplace(orderDetails.orderStatus, '_', ' '), 'Not Available');

                deliveryCharge = Main.fixUndefinedVariable(orderDetails.deliveryCharge == undefined ? "" : orderDetails.deliveryCharge.toFixed(2), 'Not Available');
                totalBillAmt = Main.fixUndefinedVariable(orderDetails.totalBillAmount == undefined ? "" : orderDetails.totalBillAmount.toFixed(2), 'Not Available');

                if (!$.isEmptyObject(orderDetails.store)) {
                    store = orderDetails.store;
                    if (!$.isEmptyObject(store.storeBrand)) {
                        brandName = Main.fixUndefinedVariable(store.storeBrand.brandName, 'Not Available');
                    } else {
                        brandName = "";
                    }

                    storeLocation1 = Main.fixUndefinedVariable(store.givenLocation1);
                    storeLocation2 = Main.fixUndefinedVariable(store.givenLocation2);
                    storeNumber = Main.fixUndefinedVariable(store.contactNo, 'Not Available');
                    storeLat = Main.fixUndefinedVariable(store.latitude);
                    storeLng = Main.fixUndefinedVariable(store.longitude);
                    if (storeLat !== "" && storeLng !== "") {
                        markers.store = {
                            lat: Number(storeLat),
                            lng: Number(storeLng),
                            title: brandName + "<Br>" + storeLocation1 + " " + storeLocation2 + "<br>" + storeNumber,
                            icon: pageContext + "resources/custom/images/map-ico/map_merchant.png"
                        };
                    }
                }

                if (!$.isEmptyObject(orderDetails.customer)) {
                    customer = orderDetails.customer;

                    if (!$.isEmptyObject(customer.user)) {
                        user = customer.user;
                        customerFirstName = Main.fixUndefinedVariable(user.firstName);
                        customerLastName = Main.fixUndefinedVariable(user.lastName);
                        customerNumber = Main.fixUndefinedVariable(user.mobileNumber);
                    }
                } else {
                    customerFirstName = 'Not Available';
                }

                if (!$.isEmptyObject(orderDetails.customersArea)) {
                    customerArea = orderDetails.customersArea;

                    customerLocation1 = Main.fixUndefinedVariable(customerArea.street);
                    //customerLocation2 = Main.fixUndefinedVariable(customerArea.area.areaName);
                    if (typeof(customerArea.area) != "undefined") {
                        customerLocation2 = Main.fixUndefinedVariable(customerArea.area.areaName);
                        customerLat = Main.fixUndefinedVariable(customerArea.area.latitude);
                        customerLng = Main.fixUndefinedVariable(customerArea.area.longitude);
                    } else {
                        customerLocation2 = "";
                        customerLat = "";
                        customerLng = "";
                    }

                    if (customerLat !== "" && customerLng !== "") {
                        markers.customer = {
                            lat: Number(customerLat),
                            lng: Number(customerLng),
                            title: customerFirstName + " " + customerLastName + "<Br>" + customerLocation1 + " " + customerLocation2 + "<br>" + customerNumber,
                            icon: pageContext + "resources/custom/images/map-ico/map_customer.png"
                        };
                    }
                }

                if (!$.isEmptyObject(orderDetails.driver)) {
                    driver = orderDetails.driver;
                    var driverName = Main.fixUndefinedVariable(driver.user.firstName) + " " + Main.fixUndefinedVariable(driver.user.lastName);
                    if (driverName == " ") {
                        driverName = "Not Available";
                    }
                    modal.find('.driver-name').text(driverName);

                    driverLat = Main.fixUndefinedVariable(driver.latitude);
                    driverLng = Main.fixUndefinedVariable(driver.longitude);

                    if (driverLat !== "" && driverLng !== "") {
                        markers.driver = {
                            lat: Number(driverLat),
                            lng: Number(driverLng),
                            title: Main.fixUndefinedVariable(driver.user.firstName) + " " + Main.fixUndefinedVariable(driver.user.lastName),
                            icon: pageContext + "resources/custom/images/map-ico/map_store_driver.png"
                        };
                    }
                }

                if (orderDetails.orderStatus && orderDetails.orderStatus === 'DELIVERED') {
                    if (orderDetails.customerSignImage && orderDetails.receiverName) {
                        recipientName = orderDetails.receiverName;
                        recipientSignature = orderDetails.customerSignImage;
                    }
                }

                if (orderDetails.orderCancel) {
                    orderCancelReason = orderDetails.orderCancel.reason || "";
                    orderCancelledDate = orderDetails.orderCancel.cancelledDate || "";
                    orderCancelBlock = '<div class="cancelDetailBlock"><div class="info flex-parent"> ' +
                        '<div class="flex-col w75 text-center"> ' +
                        '<i class="fa fa-info-circle fa-2x"></i> ' +
                        '</div> ' +
                        '<div class="flex-col"> ' +
                        '<p class="mb0 fontbold"> Order Cancel Reason</p> ' +
                        '<p class="info-status text-muted mb0">' + orderCancelReason + '</p> </div> ' +
                        '</div>' +
                        '<div class="info flex-parent"> ' +
                        '<div class="flex-col w75 text-center"> ' +
                        '<i class="fa fa-info-circle fa-2x"></i> ' +
                        '</div> ' +
                        '<div class="flex-col"> ' +
                        '<p class="mb0 fontbold"> Order Cancel Date</p> ' +
                        '<p class="info-status text-muted mb0">' + orderCancelledDate + '</p> </div> ' +
                        '</div></div>';
                } else {
                    orderCancelBlock = "";
                }

            }

            modal.find('.modal-body .cancelDetailBlock').remove();
            modal.find('#orderId').text(orderDisplayId);
            modal.find('.store-detail .name').text(brandName);
            modal.find('.store-detail .address').text(storeLocation1 + " " + storeLocation2);
            modal.find('.store-detail .contact').text(storeNumber);
            modal.find('.user-detail .name').text(customerFirstName + " " + customerLastName);
            modal.find('.user-detail .address').text(customerLocation1 + " " + customerLocation2);
            modal.find('.user-detail .contact').text(customerNumber);

            //modal.find('.datetimeval').text(moment.utc(Main.fixUndefinedVariable(orderDetails.orderDeliveryDate)).format('MMMM Do YYYY, h:mm:ss a')); //orderDeliveryTime + " " + orderDeliveryTimeUnit
            modal.find('.datetimeval').text(Main.fixUndefinedVariable(moment(orderDetails.orderDeliveryDate).format('dddd, MMMM Do YYYY, h:mm:ss A'))); //orderDeliveryTime + " " + orderDeliveryTimeUnit
            if (orderDetails.orderStatus === 'ORDER_STARTED' || orderDetails.orderStatus === 'AT_STORE' || orderDetails.orderStatus === 'EN_ROUTE_TO_DELIVER' || orderDetails.orderStatus === 'AT_CUSTOMER') {
                modal.find('.estdatetime').removeClass('hidden');
                modal.find('.estdatetimeval').text(Main.fixUndefinedVariable(moment(orderDetails.estimatedTimeOfDelivery).format('dddd, MMMM Do YYYY, h:mm:ss A')));
            } else {
                modal.find('.estdatetime').addClass('hidden');
            }
            modal.find('.info-status').text(Main.ucfirst(Main.globalReplace(orderStatus, '_', ' ')));
            modal.find('.paymentmodeval').text(paymentMode);
            modal.find('.commentval').text(customerNote);
            modal.find('.orderdescription .orderdescriptionval').text(Main.fixUndefinedVariable(orderDetails.orderDescription, 'Not Available'));
            modal.find('#reAssignDriver').attr('data-orderid', orderId).attr('data-storeid', store.storeId).attr('data-type', 'reAssign');
            modal.find('.deliveryCharge .deliverfeeval').text(Main.getFromLocalStorage('currency') + " " + deliveryCharge);
            modal.find('.totalBillAmt .totalfeeval').text(Main.getFromLocalStorage('currency') + " " + totalBillAmt);

            if (orderCancelBlock !== "") {
                modal.find('.modal-body .paymentmode').before(orderCancelBlock);
            }

            if (deliveryCharge === "") {
                modal.find('.deliveryCharge').addClass('hide');
            } else {
                modal.find('.deliveryCharge').removeClass('hide');
            }

            if (totalBillAmt === "") {
                modal.find('.totalBillAmt').addClass('hide');
            } else {
                modal.find('.totalBillAmt').removeClass('hide');
            }

            var mapLoc;
            mapConfig = {
                zoom: 10,
                center: new google.maps.LatLng(28.3949, 84.1240),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapCanvas: "modalMap",
            };

            setTimeout(function () {
                mapLoc = linkedMarkersModule.init(mapConfig);
                //console.log(markers);
                linkedMarkersModule.setMarkers([markers], mapLoc);
            }, 500);


            var assignDriverBtn = modal.find('.modal-buttons #assignDriverBtn');
            if (orderDetails.orderStatus === "ORDER_PLACED") {
                modal.find('.deliver-driver').addClass('hide');
                //modal.find('.modal-buttons').removeClass('hide');
                assignDriverBtn.attr('data-orderid', orderId).attr('data-storeid', store.storeId).attr('data-type', 'assign').removeClass('hide');

            } else {
                assignDriverBtn.addClass('hide');
                modal.find('.deliver-driver').removeClass('hide');
                //modal.find('.modal-buttons').addClass('hide');
            }

            if (orderDetails.orderStatus == "DELIVERED" || orderDetails.orderStatus == "CANCELLED") {
                modal.find('#reAssignDriver').addClass('hide');
                modal.find('#cancelOrderBtn').addClass('hide');
            } else {
                modal.find('#cancelOrderBtn').removeClass('hide');
            }

            if (orderDetails.orderStatus === 'DELIVERED') {
                var recipientEl = modal.find('.deliver-recipient');
                recipientEl.find('.recipient-name').text(recipientName);
                recipientEl.find('.signature-image > img').attr('src', recipientSignature);
                recipientEl.removeClass('hidden');
            } else {
                modal.find('.deliver-recipient').addClass('hidden');
            }

            var cancelOrderBtn = modal.find('.modal-buttons #cancelOrderBtn');
            cancelOrderBtn.attr('data-orderid', orderId);
        },

        getDriversByStore: function (storeId, customerAreaId) {
            var that = this;
            var url = pageContext + "smanager/get_eligible_driver_list"; //"driver/get_driver_list";
            var callback = function (status, data) {
                if (data.success) {
                    /*if($.isEmptyObject(data.params.driverList.data)){
                     return;
                     }*/
                    var storeDrivers = data.params.driverList;
                    var storeDriversUI = "";
                    $.each(storeDrivers, function (index, data) {
                        if (data.user) {
                            if (data.user.userStatus === "INACTIVE") {
                                return;
                            }
                        } else {
                            return;
                        }

                        if (data.availabilityStatus === "UNAVAILABLE") {
                            return;
                        }

                        var rating = data.rating;

                        var ratingUI = that.makeRatingStars(rating);
                        var name = data.user.firstName + ' ' + data.user.lastName;

                        // var waitingTime = data.lastOrderDelivered;
                        // var remDateTime = moment(waitingTime).toNow(true);

                        storeDriversUI += '<label for="' + data.driverId + '" data-id="'+name+'"><div class="od-box box-me p15 mv15 mh5"> ' +
                            '<div class="flex-parent"> ' +
                            '<div class="flex-child w50">' +
                            '<input id="' + data.driverId + '" type="radio" name="storeDriver" class="with-font form-control" value="' + data.driverId + '"> ' +
                            '<label for="' + data.driverId + '" class="control-label fontnormal cursorpointer"></label> ' +
                            '</div> ' +
                            '<div class="flex-child text-center"> ' +
                            '<img src="' + pageContext + 'resources/custom/images/map-ico/map_store_driver.png" alt="Driver Icon" class="w50"> </div> ' +
                            '<div class="flex-child w600 border-right"> ' +
                            '<p class="fontbold mb0">' + name + '</p> ' +
                            '<p class="mb0"> ' + Main.fixUndefinedVariable(data.streetAddress) + '</p> ' +
                            '<div class="rating"> ' +
                            ratingUI +
                            '</div> </div> ' +
                            // '<div class="flex-child text-center border-right"> ' +
                            // '<p class="mb0">' + remDateTime + '</p> <p class="text-muted font12">Waiting Time</p> </div> ' +
                            '<div class="flex-child text-center"> ' +
                            '<p class="mb0">' + data.currentOrderCount + '</p> <p class="text-muted font12">Current Orders</p> </div> ' +
                            '</div> </div></label>';
                    });
                    that.ui.ownDriverWrapper.html((storeDriversUI === "") ? "No drivers available in this store." : storeDriversUI);
                } else {
                    if (data.code === "MERR005") {
                        Main.popDialog("Error", "Can't fetch the drivers right now.", null, 'error');
                    } else {
                        Main.popDialog("Error", data.message, null, 'error');
                    }

                }
            };
            callback.requestType = "POST";
            callback.loaderDiv = that.ui.assignOwnDriverForm.selector;
            var data = {
                "store": {
                    "storeId": storeId
                }, "area": {
                    "areaId": customerAreaId
                }
            };

            /*"page": {
             "pageNumber": 1,
             "pageSize": 10
             },*/
            Main.request(url, data, callback, {});
        },

        assignDriver: function (orderId, driverId) {
            var that = this;         
            var url = pageContext + "smanager/assign_driver_to_order"; //"smanager/assign_order";
            var callback = function (status, data) {
                if (data.success) {
                    var drivers = data.params.data.drivers || null;
                    if (drivers !== null) {
                        if(drivers.length>0){
                            that.getAnyorderDrivers(drivers);
                        } else {
                            that.ui.anyOrderDriverWrapper.html(data.message);
                        }
                    } else {
                        // Main.popDialog('Success', data.message, null, 'success');
                        that.ui.driverAssignModal.modal('hide');
                        var el = document.createElement('p');
                        el.innerHTML = data.message;
                        el.className += 'p20 borderradius5 positionabsolute center-me c-b-active c-t-white notify';
                        document.body.appendChild(el);
                        setTimeout(function() {
                            el.className += ' notify-gone';
                            setTimeout(function() {
                                document.body.removeChild(el);
                            }, 500);
                        }, 1200);
                    }

                } else {
                    if (data.code === "MERR005") {
                        Main.popDialog("Error", "Can't fetch the drivers right now.", null, 'error');
                    } else {
                        Main.popDialog("Error", data.message, null, 'error');
                    }
                }
            };
            callback.requestType = "GET";
            var headerParams = {
                orderid: orderId
            };
            if (driverId) {
                headerParams.driverid = driverId;
            }
            if (that.ui.anyOrderDriverBox.parent().hasClass('col-md-12')) {
                callback.loaderDiv = that.ui.anyOrderDriverWrapper.closest('.modal-body');
            } else {
                callback.loaderDiv = that.ui.anyOrderDriverWrapper;
            }
            Main.request(url, {}, callback, headerParams);
        },

        getAnyorderDrivers: function (anyOrderDrivers) {
            var that = this;
            var driversUI = "";
            $.each(anyOrderDrivers, function (index, data) {
                var rating = data.rating;

                var ratingUI = that.makeRatingStars(rating);
                // var waitingTime = data.lastOrderDelivered || "";
                // var remDateTime = "";
                // if (waitingTime) {
                //     remDateTime = moment(waitingTime).toNow(true);
                // }
                var totalTimeToCompleteAllOrders = data.totalOrderEtd || "";
                var completionTime = "";
                if (totalTimeToCompleteAllOrders) {
                    completionTime = moment().to(moment().add(totalTimeToCompleteAllOrders,'m'));
                }

                var name = data.user.firstName + ' ' + data.user.lastName;

                driversUI += '<label for="' + data.driverId + '" data-id="'+name+'" class="w100p"><div class="od-box box-me p15 mv15 mh5"> ' +
                    '<div class="flex-parent"> ' +
                    '<div class="flex-child w50">' +
                    '<input id="' + data.driverId + '" type="radio" name="anyOrderDriverRadio" class="with-font form-control" value="' + data.driverId + '"> ' +
                    '<label for="' + data.driverId + '" class="control-label fontnormal cursorpointer"></label> ' +
                    '</div> ' +
                    '<div class="flex-child text-center"> ' 
+                    '<img src="' + pageContext + 'resources/custom/images/map-ico/map_store_driver.png" alt="Driver Icon" class="w50"> </div> ' +
                    '<div class="flex-child w600 border-right"> ' +
                    '<p class="fontbold mb0">' + name + '</p> ' +
                    '<p class="mb0">' + Main.fixUndefinedVariable(data.streetAddress) + '</p> ' +
                    '<div class="rating">' +
                    ratingUI +
                    ' </div> </div> ';
                    // '<div class="flex-child text-center border-right hidden"> ' +
                    // '<p class="mb0">' + remDateTime + '</p> <p class="text-muted font12">Waiting Time</p> </div> ' +
                    if (that.ui.anyOrderDriverBox.parent().hasClass('col-md-12')) {
                        driversUI += '<div class="flex-child text-center border-right"> ' +
                        '<p class="mb0">' + completionTime + '</p> <p class="text-muted font12">ETD</p> </div> ';
                    }
                    driversUI += '<div class="flex-child text-center"> ' +
                    '<p class="mb0">' + data.currentOrderCount + '</p> <p class="text-muted font12">Current Orders</p> </div> ' +
                    '</div> </div></label>';
            });
            that.ui.anyOrderDriverWrapper.html(driversUI);
            that.ui.anyOrderDriverBox.removeClass('hide');
            // that.ui.mapCanvasModal.hide();
            // that.ui.bookAnyOrderDriverBtn.hide();

        },

        getDriversByStatus: function (sortdirection, sortBy) {
            var that = this;
            searchDriverDBon = [];
            searchDriverDBoff = [];
            var callback = function (status, data) {
                if (data.success) {
                    var offDutyDrivers, onDutyDrivers, firstName, lastName, ordersCount, waitingTime, waitingTimeUnit, onDutyUI, offDutyUI;

                    var drivers = data.params.data;

                    if (drivers.onDutyDrivers) {
                        onDutyDrivers = drivers.onDutyDrivers.data;
                        onDutyUI = "";
                        that.ui.driverListDutyOnCount.text(onDutyDrivers.length);

                        $.each(onDutyDrivers, function (index, data) {
                            if (data.user) {
                                firstName = data.user.firstName;
                                lastName = data.user.lastName;
                            }

                            ordersCount = data.currentOrderCount;
                            waitingTime = data.lastOrderDelivered;
                            if (waitingTime) {
                                waitingTime = moment(waitingTime).toNow(true);
                                if (waitingTime.indexOf('minutes') > 0) {
                                    waitingTime = Main.globalReplace(waitingTime, 'minutes', 'mins');
                                }
                            } else {
                                waitingTime = "-";
                            }
                            waitingTimeUnit = '';
                            //console.log(data.rating);
                            var ratingUI = that.makeRatingStars(data.rating);
                            // var ordersDisplayId = "";
                            // if (!$.isEmptyObject(data.orders)) {
                            //     $.each(data.orders, function (index, data) {
                            //         ordersDisplayId += data.orderDisplayId + "<br>";
                            //     });
                            // }

                            var locationUpdatedTimestamp, locationUpdated;
                            locationUpdatedTimestamp = data.lastLocationUpdate || "";
                            if (locationUpdatedTimestamp) {
                                locationUpdated = moment(locationUpdatedTimestamp).fromNow();
                            } else {
                                locationUpdated = 'N/A';
                            }


                            onDutyUI += '<li class="driver-list-item clearfix mv10 pv15 positionrelative text-center flex-grid"  data-id="' + data.driverId + '"> ' +
                                '<div class="col-md-6 col-sm-12 text-left flex-gridcell flex-center flex-justify-start"> ' +
                                '<p class="driver-name"><strong> <a href="#" data-title="driverNames">' + firstName + ' ' + lastName + '</a><br><span class="rating">' + ratingUI + '</span> </strong></p>' +
                                '</div>' +
                                ' <div class="col-md-3 col-sm-6 col-xs-12 flex-gridcell flex-center flex-col"> ' +
                                '<p> <strong class="driver-waiting"> ' + waitingTime + ' </strong> <small> ' + waitingTimeUnit + '</small> <br> ' +
                                '<small class="text-uppercase"> Waiting Time</small> </p> </div> ' +
                                '<div class="col-md-3 col-sm-6 col-xs-12 flex-gridcell flex-center"> <a href="#" class="btn"';
                            if (ordersCount >= 1) {
                                onDutyUI += 'data-toggle="modal" data-target="#driverCurrentOrderModal" data-driver="' + data.driverId + '" data-driverName="' + firstName + ' ' + lastName + '">';
                            }
                            onDutyUI += '<strong class="driver-order-success font30 c-t-success"> ' + ordersCount + ' </strong> <br> ' +
                                '<small class="text-uppercase"> Orders</small> </a> </div> '+
                                '<div class="col-xs-12 text-left"> Location Update: <strong>' + locationUpdated + '</strong> </div>' +
                                '</li>';

                            var lat = data.latitude;
                            var lng = data.longitude;
                            var title = firstName + " " + lastName + "<br>" + Main.ucfirst(Main.globalReplace(data.driverType, '_', ' '));
                            var icon = pageContext + "resources/custom/images/map-ico/map_store_driver.png";
                            if (ordersCount === 0 || ordersCount == 1) {
                                icon = pageContext + "resources/custom/images/map-ico/driver_green.png";
                            } else if (ordersCount == 2) {
                                icon = pageContext + "resources/custom/images/map-ico/driver_yellow.png";
                            } else if (ordersCount > 2) {
                                icon = pageContext + "resources/custom/images/map-ico/driver_red.png";
                            }

                            onDutyDriversLocation.push({
                                lat: lat,
                                lng: lng,
                                type: "Driver",
                                title: title,
                                icon: icon,
                                driverInfo: data
                            });

                            if (lat && lng) {
                                searchDriverDBon.push({
                                    locationKey: orderMapsModule.locationToKey({latitude: lat, longitude: lng}),
                                    value: firstName + " " + lastName
                                });
                            }

                        });

                        //that.waitForMaps(onDutyDriversLocation);

                        that.ui.driverListDutyOnWrapper.html((onDutyUI !== "") ? onDutyUI : "<li>No on duty drivers</li>");
                    }
                    if (drivers.offDutyDrivers) {
                        offDutyDrivers = drivers.offDutyDrivers.data;
                        offDutyUI = "";
                        that.ui.driverListDutyOffCount.text(offDutyDrivers.length);

                        $.each(offDutyDrivers, function (index, data) {
                            if (data.user) {
                                firstName = data.user.firstName;
                                lastName = data.user.lastName;
                            }

                            ordersCount = data.currentOrderCount;
                            waitingTime = data.lastOrderDelivered;
                            if (waitingTime) {
                                waitingTime = moment(waitingTime).toNow(true);
                            } else {
                                waitingTime = "-";
                            }
                            waitingTimeUnit = '';

                            var ratingUIoff = that.makeRatingStars(data.rating);

                            offDutyUI += '<li class="driver-list-item clearfix mv10 pv15 positionrelative text-center flex-grid"  data-id="' + data.driverId + '"> ' +
                                '<div class="col-md-6 col-sm-12 text-left flex-gridcell flex-center flex-justify-start"> ' +
                                '<p class="driver-name">' +
                                '<strong><a href="#" data-title="driverNames">' + firstName + ' ' + lastName + '</a><br><span class="rating">' + ratingUIoff + '</span> </strong>' +
                                '</p> </div>' +
                                '</li>';


                            var lat = data.latitude;
                            var lng = data.longitude;
                            var title = firstName + " " + lastName + "<br>" + Main.ucfirst(Main.globalReplace(data.driverType, '_', ' '));
                            var icon = pageContext + "resources/custom/images/map-ico/map_store_driver_inactive.png";


                            offDutyDriversLocation.push({
                                lat: lat,
                                lng: lng,
                                type: "Driver",
                                title: title,
                                icon: icon,
                                driverInfo: data
                            });

                            if (lat && lng) {
                                searchDriverDBoff.push({
                                    locationKey: orderMapsModule.locationToKey({latitude: lat, longitude: lng}),
                                    value: firstName + " " + lastName
                                });
                            }
                        });


                        //that.waitForMaps(offDutyDriversLocation);
                        that.ui.driverListDutyOffWrapper.html((offDutyUI !== "") ? offDutyUI : "<li>No off duty drivers</li>");
                    }


                    //<----- search driver  ------>
                    that.ui.searchDriverInMap.autocomplete({
                        lookup: (that.ui.driverTabContent.find('div.tab-pane.active').attr('id') === "driverListDutyOnTab") ? searchDriverDBon : searchDriverDBoff, //searchDriverDB,
                        onSelect: function (suggestion) {
                            var markers = orderMapsModule.getMarkers('Driver');
                            //console.log(markers);
                            //console.log(suggestion);
                            var listItem;
                            that.ui.colDriverListWrapper.find('a[data-title="driverNames"]').each(function (index, data) {
                                if ($(this).text() == suggestion.value) {
                                    listItem = $(this).parents().eq(3);
                                    that.scrollDriver(listItem);
                                }
                            });

                            var driverId = listItem.attr('data-id');
                            //console.log(driverId);

                            orderMapsModule.removeAllMarkerAnimation(markers);
                            markers[driverId].setAnimation(google.maps.Animation.BOUNCE);
                            ordersMainMap.setCenter(markers[driverId].position);
                            ordersMainMap.setZoom(16);

                        },
                        showNoSuggestionNotice: true,
                        noSuggestionNotice: "Location unavailable"
                    });

                    that.showMarkersInMap(onDutyDriversLocation);

                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
                that.fixContentHeights();
            };

            callback.requestType = "POST";
            callback.loaderDiv = that.ui.driverTabContent.selector;
            var url = pageContext + "smanager/drivers_by_status_with_order_count";
            var header = {
                userId: Main.getFromLocalStorage('userId')
            };

            var data = {
                "page": {
                    "pageNumber": 1,
                    "pageSize": 200,
                }
            };

            if (sortBy) {
                data.page.sortBy = sortBy;
            }

            if (sortdirection) {
                data.page.sortOrder = sortdirection;
            }

            Main.request(url, data, callback, header);
        },

        scrollDriver: function (listItem) {
            var that = this;

            listItem.css('background', 'rgb(240,230,140)');
            that.ui.driverTabContent.scrollTop(0);
            that.ui.driverTabContent.animate({
                scrollTop: listItem.offset().top - 270
            }, 0, function () {
                setTimeout(function () {
                    listItem.css('background', '#ffffff');
                }, 2000);
            });
        },

        showMarkersInMap: function (locs) {
            var that = this;
            if (!ordersMainMap) {
                console.log(locs);
                setTimeout(function () {
                    that.showMarkersInMap(locs);
                }, 500);
            } else {
                orderMapsModule.setMarkers(ordersMainMap, locs, true);
            }
        },

        dispatchOrder: function (orderId) {
            var that = this;
            var url = pageContext + "smanager/update_order_status";
            var callback = function (status, data) {
                if (data.success) {
                    var btn = function () {
                        //that.showCurrentOrders();
                        that.configOrderList();
                        $('#popDialog').modal('hide');
                    };
                    btn.text = "Close";
                    var button = [btn];
                    Main.popDialog('Success', data.message, button, 'success', false);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "POST";
            Main.request(url, {"orderStatus": "EN_ROUTE_TO_DELIVER"}, callback, {orderid: orderId});
        },

        makeRatingStars: function (ratingNumber) {
            var ratingUI = "";
            var ratingCounter = 0;
            for (ratingCounter; ratingCounter < Number(ratingNumber); ratingCounter++) {
                ratingUI += '<i class="fa fa-star c-t-gold"></i>';
            }
            var remainingRating = 5 - ratingNumber;
            for (var j = 0; j < Number(remainingRating); j++) {
                ratingUI += '<i class="fa fa-star-o"></i>';
            }

            return ratingUI;
        },

        searchOrder: function (searchKey, orderStatus, page) {
            var that = this;
            var searchPaging = {};
            searchPaging.pageNumber = page.pageNumber || 1;
            that.ui.orderSort2.closest('.row').addClass('hidden');
            that.ui.currentOrderWrapper.hide();
            that.ui.pastOrderWrapper.hide();
            that.ui.cancelledOrderWrapper.hide();
            that.ui.searchOrderWrapper.show();
            var callback = function (status, data) {
                that.ui.searchOrderInput.val(decodeURI(searchKey));
                if (data.success) {
                    var orders = {}; //data.params.orderSearchResult.data;
                    orders.params = {};
                    orders.params.orderList = {};
                    orders.params.orderList.data = data.params.orderSearchResult.data;
                    orders.params.orderList.numberOfRows = data.params.orderSearchResult.numberOfRows;
                    orders.success = data.success;
                    orders.message = data.message;
                    data = {};
                    that.getOrdersByStatus("searchResults", that.ui.searchOrderWrapper.find('.ordersHere'), "", "", orders, searchPaging);
                }
            };
            callback.requestType = "POST";
            callback.loaderDiv = that.ui.searchOrderWrapper.selector + ' .ordersHere';
            var data = {
                "page": {
                    "pageSize": 100,
                    "pageNumber": 1,
                    "searchFor": decodeURI(searchKey),
                    "sortBy": "orderStatus",
                    "sortOrder": "asc"
                }
            };
            if (page && page.pageNumber) {
                data.page.pageNumber = page.pageNumber || 1;
            }
            if (orderStatus) {
                data.orderDisplayStatus = orderStatus;
            }

            var url = pageContext + "smanager/search_order";

            Main.request(url, data, callback, {});
        },

        validateCancelOrder: function () {
            var that = this;
            that.ui.cancelOrderForm.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    reasonsFor: {
                        required: true
                    }
                },
                submitHandler: function () {
                    var orderId = that.ui.cancelOrderModel.attr('data-orderid');
                    if (orderId) {
                        that.cancelOrder(orderId, that.ui.reasonsForCancel.val());
                    }
                }
            });
        },

        cancelOrder: function (orderId, reasonFor) {
            var that = this;
            var url = pageContext + "smanager/cancel_order";
            var callback = function (status, data) {
                if (data.success) {
                    var btn = function () {
                        //that.showCurrentOrders();
                        that.configOrderList();
                        $('#popDialog').modal('hide');
                        that.ui.cancelOrderModel.modal('hide');
                        that.ui.orderDetailModal.modal('hide');
                    };
                    btn.text = "Close";
                    var button = [btn];
                    Main.popDialog('Success', data.message, button, 'success', false);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "POST";
            Main.request(url, {"orderCancelReason": reasonFor}, callback, {orderId: orderId});
        },

        getStoreList: function () {
            var url = pageContext + "smanager/get_store_list_by_user_role"; //get_all_store_list";
            var callback = function (status, data) {
                if (data.success) {
                    var stores = data.params.stores;

                    //console.log(stores.length);


                    $.each(stores, function (index, data) {
                        var storeId = data.storeId;
                        var lat = Number(data.latitude);
                        var lng = Number(data.longitude);
                        var title = data.addressNote;
                        var icon = pageContext + "resources/custom/images/map-ico/map_merchant.png";

                        storesLocation.push({
                            storeId: storeId,
                            lat: lat,
                            lng: lng,
                            type: "Store",
                            title: title,
                            icon: icon
                        });

                    });

                    orderMapsModule.setMarkers(ordersMainMap, storesLocation);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {userId: Main.getFromLocalStorage('userId')});
        },


        showOrderInMap: function (markers) {
            var that = this;
            //console.log(markers);

            orderMapsModule.generateDirections(ordersMainMap, markers);


            /*that.ui.googleMapsWrapper.addClass('hidden');
             that.ui.googleMapsWrapperRoute.removeClass('hidden');*/

            /*  var mapOrderRoute = {
             zoom: 10,
             center: new google.maps.LatLng(28.3949, 84.1240),
             mapTypeId: google.maps.MapTypeId.ROADMAP,
             mapCanvas: "map2",
             };

             //setTimeout(function () {
             superMap2 = linkedMarkersModule.init(mapOrderRoute);
             that.fixContentHeights();
             linkedMarkersModule.setMarkers([markers], superMap2);*/
            //}, 500);


        },

        getOrderCounts: function () {
            var that = this;

            var callback = function (status, data) {
                if (data.success) {
                    var counts = data.params.numberOfOrderByStatus;

                    that.ui.orderListWaitingCount.text(counts.ORDER_PLACED + counts.ORDER_STARTED);
                    that.ui.orderListRunningCount.text(counts.EN_ROUTE_TO_PICK_UP + counts.AT_STORE + counts.AT_CUSTOMER);
                    that.ui.orderPlacedCount.text(counts.ORDER_PLACED);
                    that.ui.orderStartedCount.text(counts.ORDER_STARTED);
                    that.ui.inRouteTopickUpCount.text(counts.EN_ROUTE_TO_PICK_UP);
                    that.ui.atStoreCount.text(counts.AT_STORE);
                    that.ui.inRouteToDeliveryCount.text(counts.AT_CUSTOMER);
                    that.ui.pastOrderWrapper.find('.counterHere').text(counts.DELIVERED);
                    that.ui.cancelledOrderWrapper.find('.counterHere').text(counts.CANCELLED);


                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };

            callback.requestType = "POST";
            var url = pageContext + "smanager/get_order_num_by_status";
            var data = {
                "user": {
                    "userId": Main.getFromLocalStorage('userId')
                }
            };

            Main.request(url, data, callback, {orderId: orderId});
        },

        getSearchResults: function (orderStatus, orderWrapper, countWrapper, sortdirection, sortBy, orderData) {
            var that = this;
            var callback = function (status, data) {
                if (data.success) {
                    var orders = data.params.orders;
                    countWrapper.html(orders.numberOfRows);

                    var orderListTmpl = "";


                    $.each(orders.data, function (index, data) {
                        var brandName = "",
                            storeLocation1 = "",
                            storeLocation2 = "",
                            customerFirstName = "",
                            customerLastName = "",
                            customerLocation1 = "",
                            customerLocation2 = "",
                            driverFirstName = "",
                            driverLastName = "",
                            driverContactNo = "",

                            orderDisplayId = "",
                            orderCreatedDateTime = "",
                            deliveredDateTime = "",
                            orderDeliveryTime = "",
                            orderDeliveryTimeUnit = "",
                            orderId = "",
                            storeId = "",
                            store = {},
                            customer = {},
                            user = {},
                            driver = {},
                            customerArea = {},
                            grayScaleClass = "",

                            orderStatusClass = "",
                            inRouteToPickUpOrders = "",
                            inRouteToPickUpCount = 0,
                            atStoreOrders = "",
                            atStoreCount = 0,
                            inRouteToDeliveryOrders = "",
                            inRouteToDeliveryCount = 0,

                            driverLat, driverLng,
                            storeLat, storeLng,
                            customerLat, customerLng;

                        var driverDetails = "", assignBtn = "";
                        var orderListTemp = "";
                        var orderTypeForSearch = "";
                        if (!$.isEmptyObject(data)) {

                            orderId = Main.fixUndefinedVariable(data.orderId);

                            orderDisplayId = Main.fixUndefinedVariable(data.orderDisplayId);
                            orderCreatedDateTime = Main.fixUndefinedVariable(data.createdDate);
                            var remDateTime;
                            if (new Date(orderCreatedDateTime).getTime() > Date.now())
                            //fix order created date in future(at dubai)
                                remDateTime = moment(new Date(orderCreatedDateTime).getTime() - 110 * 60 * 1000).toNow().split(' ');
                            else
                                remDateTime = moment(orderCreatedDateTime).toNow().split(' ');

                            orderDeliveryTime = Main.fixUndefinedVariable((remDateTime[1] == "a" || remDateTime[1] == "an") ? "1" : remDateTime[1]);

                            orderDeliveryTimeUnit = (remDateTime[2] == "few") ? remDateTime[2] + " " + remDateTime[3] : remDateTime[2];

                            orderDeliveryTimeUnit = Main.fixUndefinedVariable(orderDeliveryTimeUnit);

                            deliveredDateTime = Main.fixUndefinedVariable(data.deliveredDate);

                            if (orderDeliveryTimeUnit === "minutes") {
                                orderDeliveryTimeUnit = "mins";
                            }

                            orderStatusClass = "order-list-default"; //order-list-warning, order-list-error,

                            if (orderDeliveryTime > 20 && orderDeliveryTimeUnit === "minutes") {
                                orderStatusClass = "order-list-warning";
                            } else if (orderDeliveryTimeUnit.indexOf("day") > -1 || orderDeliveryTimeUnit.indexOf('hour') > -1 || orderDeliveryTimeUnit.indexOf('month') > -1 || orderDeliveryTimeUnit.indexOf('year') > -1) {
                                orderStatusClass = "order-list-error";
                            }

                            if (orderDeliveryTimeUnit === "day") {
                                orderDeliveryTime = "1";
                            }

                            if (orderStatus === "DELIVERED") {
                                orderDeliveryTime = '<span class="font16">' + moment.duration(moment(deliveredDateTime).diff(moment(orderCreatedDateTime))).humanize() + '</span>';
                                orderDeliveryTimeUnit = "";
                            }

                            if (!$.isEmptyObject(data.store)) {
                                store = data.store;
                                storeId = Main.fixUndefinedVariable(store.storeId);
                                if (!$.isEmptyObject(store.storeBrand)) {
                                    brandName = Main.fixUndefinedVariable(store.storeBrand.brandName);
                                } else {
                                    brandName = "";
                                }

                                storeLocation1 = Main.fixUndefinedVariable(store.givenLocation1);
                                storeLocation2 = Main.fixUndefinedVariable(store.givenLocation2);

                                storeLat = store.latitude || store.area.latitude;
                                storeLng = store.longitude || store.area.longitude;
                            }

                            if (!$.isEmptyObject(data.customer)) {
                                customer = data.customer;

                                if (!$.isEmptyObject(customer.user)) {
                                    user = customer.user;

                                    customerFirstName = Main.fixUndefinedVariable(user.firstName);
                                    customerLastName = Main.fixUndefinedVariable(user.lastName);
                                }
                            } else {
                                customerFirstName = 'Not Avaiable';
                            }

                            if (!$.isEmptyObject(data.customersArea)) {
                                customerArea = data.customersArea;

                                customerLocation1 = Main.fixUndefinedVariable(customerArea.street);
                                if (typeof(customerArea.area) != "undefined") {
                                    customerLocation2 = Main.fixUndefinedVariable(customerArea.area.areaName);
                                } else {
                                    customerLocation2 = "";
                                }

                                customerLat = customerArea.latitude || customerArea.area.latitude;
                                customerLng = customerArea.longitude || customerArea.area.longitude;

                            }

                            var showStoreDriver = false;

                            if (typeof(data.driver) == "undefined") {
                                assignBtn = '<a href="#" class="btn-order-list btn-sm text-uppercase flex-gridcell order-waiting-assign" ' +
                                    'data-toggle="modal" data-storeDriver="'+showStoreDriver+'" data-target="#driverAssignModal" data-storeid=' + storeId + ' ' +
                                    'data-orderId= ' + orderId + ' data-type="assign">Assign</a>';
                            } else {
                                driver = data.driver;

                                driverFirstName = Main.fixUndefinedVariable(driver.user.firstName);
                                driverLastName = Main.fixUndefinedVariable(driver.user.lastName);
                                driverContactNo = Main.fixUndefinedVariable(driver.user.mobileNumber);

                                driverLat = driver.latitude;
                                driverLng = driver.longitude;

                                assignBtn = '<a href="#" class="btn-order-list btn-sm text-uppercase flex-gridcell order-waiting-assign" ' +
                                    'data-toggle="modal" data-storeDriver="'+showStoreDriver+'" data-target="#driverAssignModal" data-storeid=' + storeId + ' ' +
                                    'data-orderId= ' + orderId + ' data-type="reAssign">Re-Assign</a>';

                                driverDetails += "        <div class=\"media\">";
                                driverDetails += "            <div class=\"media-left media-top\">";
                                driverDetails += "                <i class=\"fa fa-motorcycle\" aria-hidden=\"true\"><\/i>";
                                driverDetails += "            <\/div>";
                                driverDetails += "            <div class=\"media-body\">";
                                driverDetails += "                <strong class=\"media-heading\">" + driverFirstName + " " + driverLastName + "<\/strong> <br>";
                                driverDetails += "                <small> " + driverContactNo + "<\/small>";
                                driverDetails += "            <\/div>";
                                driverDetails += "        <\/div>";
                            }

                        }

                        if (orderStatus === "searchResults") {
                            orderTypeForSearch += "<div class=\"media\">";
                            orderTypeForSearch += "            <div class=\"media-left media-top\">";
                            orderTypeForSearch += "                <i class=\"fa fa-file-text\"><\/i>";
                            orderTypeForSearch += "            <\/div>";
                            orderTypeForSearch += "            <div class=\"media-body\">";
                            orderTypeForSearch += "                <strong class=\"media-heading\">Order Status<\/strong> <br>";
                            orderTypeForSearch += "                <small> " + Main.ucfirst(Main.globalReplace(Main.fixUndefinedVariable(data.orderStatus), '_', ' ')) + "<\/small>";
                            orderTypeForSearch += "            <\/div>";
                            orderTypeForSearch += "        <\/div>";
                        }

                        if (data.orderStatus == "DELIVERED" || data.orderStatus == "CANCELLED") {
                            assignBtn = "";
                        }

                        var dispatch = '<a href="#" class="btn-order-list btn-sm text-uppercase flex-gridcell order-waiting-assign" ' +
                            'data-orderId= ' + orderId + ' data-type="dispatchOrder">Dispatch</a>';

                        if (data.orderStatus != "AT_STORE") {
                            dispatch = "";
                        }


                        if (data.assignDriver === false) {
                            grayScaleClass = 'filter-grayscale';
                        } else {
                            grayScaleClass = '';
                        }

                        orderListTemp += "<li class=\"flex-parent order-list-item-new positionrelative " + grayScaleClass + " \">";
                        orderListTemp += "    <div class=\"flex-child w20p\">";
                        orderListTemp += "        <div class=\"text-center time-elapsed-wrapper  " + orderStatusClass + " \">";
                        orderListTemp += "            <span class=\"time-duration font24 \">" + orderDeliveryTime + "<\/span><Br>";
                        orderListTemp += "            <span class=\"time-unit fontbold font12 text-capitalize \">" + orderDeliveryTimeUnit + " ago<\/span>";
                        orderListTemp += "        <\/div>";
                        orderListTemp += "    <\/div>";
                        orderListTemp += "    <div class=\"flex-child w80p\">";
                        orderListTemp += "        <div class=\"flex-parent \">";
                        orderListTemp += "            <div class=\"flex-child w10p\">";
                        orderListTemp += "                <div class=\"ico text-center\">";
                        orderListTemp += "                    <i class=\"aoicon aoicon-store\"><\/i>";
                        orderListTemp += "                <\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "            <div class=\"flex-child w90p\">";
                        orderListTemp += "                <div class=\"store-name font18\">" + brandName + "<\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "        <\/div>";
                        orderListTemp += "";
                        orderListTemp += "        <div class=\"flex-parent c-t-blue-sky\">";
                        orderListTemp += "            <div class=\"flex-child w10p\">";
                        orderListTemp += "                <div class=\"ico text-center\">";
                        orderListTemp += "                    <i class=\"fa fa-map-marker\"><\/i>";
                        orderListTemp += "                <\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "            <div class=\"flex-child w90p\">";
                        orderListTemp += "                <div class=\"customer-location fontbold\">" + customerLocation1 + " " + customerLocation2 + "<\/div>";
                        orderListTemp += "            <\/div>";
                        orderListTemp += "        <\/div>";
                        orderListTemp += "    <\/div>";
                        orderListTemp += "    <div class=\"text-center flex-grid flex-center invisible order-waiting-actions\">";
                        orderListTemp += '<a href="#" class="invisible btn-order-list btn-sm text-uppercase flex-gridcell" ' +
                            'data-orderId= ' + orderId + ' data-type="showInMaps" ' +
                            'data-driver-lat="' + driverLat + '" data-driver-lng="' + driverLng + '" ' +
                            'data-store-lat="' + storeLat + '" data-store-lng="' + storeLng + '" ' +
                            'data-customer-lat="' + customerLat + '" data-customer-lng="' + customerLng + '" ' +
                            'data-customer-name="' + customerFirstName + ' ' + customerLastName + '" ' +
                            'data-brand-name="' + brandName + '" ' +
                            'data-driver-name="' + driverFirstName + ' ' + driverLastName + '">View in map</a>';

                        orderListTemp += "        <a href=\"#\"";
                        orderListTemp += "           class=\" btn-order-list  btn-sm text-uppercase flex-gridcell\"";
                        if (data.orderStatus === 'DELIVERED') {
                            orderListTemp += 'data-status="successful"';
                        }
                        orderListTemp += "           data-toggle=\"modal\" data-target=\"#orderDetailModal\" data-orderid=" + orderId + " data-storeid=" + store.storeId + ">";
                        orderListTemp += "            View Details <\/a>";
                        orderListTemp += assignBtn;
                        orderListTemp += dispatch;

                        orderListTemp += "    <\/div>";
                        orderListTemp += "<\/li>";

                        if (orderStatus === "PROCESSING") {
                            if (data.orderStatus === "EN_ROUTE_TO_PICK_UP" || data.orderStatus === "ORDER_STARTED") {
                                inRouteToPickUpOrders += orderListTemp;
                                inRouteToPickUpCount++;
                            } else if (data.orderStatus === "AT_STORE") {
                                atStoreOrders += orderListTemp;
                                atStoreCount++;
                            } else if (data.orderStatus === "EN_ROUTE_TO_DELIVER" || data.orderStatus === "AT_CUSTOMER") {
                                inRouteToDeliveryOrders += orderListTemp;
                                inRouteToDeliveryCount++;
                            }
                        } else {
                            orderListTmpl += orderListTemp;
                            that.fixContentHeights();
                        }

                    });

                    if (orderStatus == "PROCESSING") {
                        orderWrapper.find('#inRouteToPickUp ul').html(inRouteToPickUpOrders || "<strong>No orders available.</strong>");
                        orderWrapper.find('#inRouteTopickUpCount').html(inRouteToPickUpCount);
                        orderWrapper.find('#atStore ul').html(atStoreOrders || "<strong>No orders available.</strong>");
                        orderWrapper.find('#atStoreCount').html(atStoreCount);
                        orderWrapper.find('#inRouteToDelivery ul').html(inRouteToDeliveryOrders || "<strong>No orders available.</strong>");
                        orderWrapper.find('#inRouteToDeliveryCount').html(inRouteToDeliveryCount);
                    } else {
                        orderWrapper.html(orderListTmpl);
                    }


                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };

            if (orderData) {
                var orders = {
                    params: {
                        orders: {
                            data: orderData,
                            numberOfRows: orderData.length
                        }
                    },

                    success: true
                };
                callback("", orders);

                that.fixContentHeights();
                return;
            }

            callback.requestType = "POST";
            callback.loaderDiv = that.ui.ordersTabs.selector;
            var url = pageContext + "smanager/get_orders_by_status";
            var data = {
                page: {
                    pageSize: 100,
                    pageNumber: 1,
                    "sortBy": sortBy,
                    "sortOrder": sortdirection
                },
                orderDisplayStatus: orderStatus
            };
            var header = {
                userId: Main.getFromLocalStorage('userId')
            };

            Main.request(url, data, callback, header);
        },
        getDriverCurrentOrders: function (driverId) {
            if (!driverId) {
                return;
            }
            var url = "smanager/get_current_orders_of_driver",
                header = {},
                that = this,
                callback;

            header.driverId = driverId;
            callback = function (status, data) {
                if (data.success) {
                    var details = data.params.orders;
                    that.showDriverCurrentOrders(details);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = 'GET';
            Main.request(url, {}, callback, header);
        },
        showDriverCurrentOrders: function (data) {
            var that = this,
                orderList = "No orders available";
            this.ui.driverCurrentOrderName.html(this.ui.driverCurrentOrderModal.attr('data-driverName') + " - ");
            if (!$.isEmptyObject(data)) {
                var block = '<ul class="list-unstyled">';
                $.each(data, function (index, val) {
                    var blockGroup = "",
                        blockHead = "",
                        orderFrom = "",
                        orderTo = "",
                        orderId = "",
                        count = 0,
                        storeName = "Not Available",
                        storeLocation = "",
                        customerName = "Not Available",
                        customerLocation = "";

                    orderDisplayId = val.orderDisplayId || "";
                    orderId = val.orderId || "";
                    storeId = (val.store) ? val.store.storeId || "" : "";
                    count = ++index;

                    if (val.store) {
                        if (val.store.storeBrand) {
                            if (val.store.storeBrand.brandName) {
                                storeName = val.store.storeBrand.brandName;
                            }
                        }
                        if (val.store.givenLocation1) {
                            storeLocation = val.store.givenLocation1;
                        }
                    }

                    if (val.customer) {
                        if (val.customer.user) {
                            if (val.customer.user.firstName) {
                                customerName = val.customer.user.firstName + " ";
                            }
                            if (val.customer.user.lastName) {
                                customerName = val.customer.user.lastName;
                            }
                        }
                    }

                    if (val.customersArea) {
                        if (val.customersArea.area) {
                            if (val.customersArea.area) {
                                if (val.customersArea.area.parent) {
                                    if (val.customersArea.area.parent.areaName) {
                                        customerLocation = val.customersArea.area.parent.areaName;
                                        if (val.customersArea.area.areaName) {
                                            customerLocation += ", " + val.customersArea.area.areaName;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    blockHead = '<div class="media">';
                    blockHead += '<div class="media-left media-top"> <i class="fa fa-file-text" aria-hidden="true"></i> </div>';
                    blockHead += '<div class="media-body"> <strong class="media-heading"> Order Id </strong> <br> <small> ' + orderDisplayId + ' </small> </div>';
                    blockHead += '<div class="media-right text-right"> <span class="label borderradius50 c-b-active">' + count + ' </span> </div>';
                    blockHead += '</div>';

                    orderFrom = '<div class="media"> <div class="media-left media-top"> <i class="aoicon aoicon-store" aria-hidden="true"> </i> </div>';
                    orderFrom += '<div class="media-body"> <strong class="media-heading"> ' + storeName + ' </strong> <br> <small> ' + storeLocation + ' </small> </div> </div>';

                    orderTo = '<div class="media"> <div class="media-left media-top"> <i class="fa fa-user" aria-hidden="true"> </i> </div>';
                    orderTo += '<div class="media-body"> <strong class="media-heading"> ' + customerName + ' </strong> <br> <small> ' + customerLocation + ' </small> </div> </div>';

                    blockGroup = '<li class="clearfix p20';
                    if (data.length !== count) {
                        blockGroup += ' nav-tabs';
                    }
                    blockGroup += ' ">';
                    blockGroup += '<div class="col-xs-12 mb10"> ' + blockHead + ' </div>';
                    blockGroup += '<div class="col-xs-5"> ' + orderFrom + ' </div>';
                    blockGroup += '<div class="col-xs-2 font30 text-center c-t-blue-light"> <i class="glyphicon glyphicon-arrow-right" aria-hidden="true"> </i> </div>';
                    blockGroup += '<div class="col-xs-5"> ' + orderTo + ' </div>';
                    blockGroup += '<div class="col-xs-12"> <a href="#" class="btn text-uppercase" data-toggle="modal" data-target="#orderDetailModal" data-orderid="' + orderId + '" data-storeid="' + storeId + '"> View Details </a> </div>';
                    blockGroup += '</li>';
                    block += blockGroup;
                });
                block += '</ul>';
                orderList = block;
            } else {
                orderList = 'No orders available';
            }
            this.ui.driverCurrentOrderList.html(orderList);
        },


        toggleMarkers: function (type) {
            var markers;
            //console.log(type);
            /*if (type === "Store") {
             markers = orderMapsModule.getMarkers();
             $.each(markers, function (index, marker) {
             if (marker.mapData.type === "Store") {
             marker.setVisible(!marker.visible);
             }
             });
             } else {

             }*/

            markers = orderMapsModule.getMarkers(type);
            $.each(markers, function (index, marker) {
                marker.setVisible(!marker.visible);
            });

        }
    };


    return {
        init: function () {
            orderList.init();
        },
    };
})
();