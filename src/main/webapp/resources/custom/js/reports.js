var reportsModule = (function () {
    reports = {
        init: function () {
            this.events();
            if (Main.getFromLocalStorage('userRole') === "ROLE_ADMIN" || Main.getFromLocalStorage('userRole') === "ROLE_MANAGER") {
                this.ui.merchants.closest('.col-sm-3').removeClass('hidden');
                this.ui.stores.closest('.col-sm-3').removeClass('hidden');
                this.getMerchants();
            }else if(Main.getFromLocalStorage('userRole') === "ROLE_MERCHANT"){
                this.ui.stores.closest('.col-sm-3').removeClass('hidden');
                this.getStores(Main.getFromLocalStorage('merchantId'));
            }else if(Main.getFromLocalStorage('userRole') === "ROLE_STORE_MANAGER"){
                this.getDrivers();
            }
            this.ui.orderStatus.selectpicker('selectAll');
        },
        ui: {
            form: $('#reportForm'),
            fromDate: $('#reportFromDate'),
            fromTime: $('#reportFromTime'),
            toDate: $('#reportToDate'),
            toTime: $('#reportToTime'),
            fromDatePicker: $('#reportFromDatePicker'),
            fromTimePicker: $('#reportFromTimePicker'),
            toDatePicker: $('#reportToDatePicker'),
            toTimePicker: $('#reportToTimePicker'),
            merchants: $('#reportMerchants'),
            stores: $('#reportStores'),
            drivers: $('#reportDrivers'),
            orderStatus: $('#reportOrderStatus'),
            table: $('#reportsTable'),
            btnGenerateReport: $('#btnGenerateReport')
        },
        isSelectedAll: function (option) {
            if (!option.val()) {
                return;
            }
            var selectedSize = option.val().length;
            var optionSize = option.find('option').length;
            if (selectedSize === optionSize) {
                requestAnimationFrame(function () {
                    option.siblings('.btn').attr('title', 'All selected');
                    option.siblings('.btn').find('span.filter-option').text('All selected');
                });
            }
        },
        events: function () {
            var that = this;
            $('.select-picker-reports').selectpicker({
                liveSearch: true,
                //selectedTextFormat: 'count > 3',

            });

            $('.select-picker-reports').on('changed.bs.select', function (e) {
                that.isSelectedAll($(this));
            });

            this.ui.fromDatePicker.datetimepicker({
                useCurrent: false,
                format: 'YYYY-MM-DD',
                maxDate: 'now',
                defaultDate: moment().subtract(1, 'days')
            });

            this.ui.fromTimePicker.datetimepicker({
                format: 'LTS',
                defaultDate: moment().hours(8).minutes(0).second(0)
            });

            this.ui.toDatePicker.datetimepicker({
                useCurrent: true,
                format: 'YYYY-MM-DD',
                maxDate: 'now',
                defaultDate: 'now'
            });

            this.ui.toTimePicker.datetimepicker({
                format: 'LTS',
                //defaultDate: moment().format('YYYY-MM-DD HH:mm:ss')//current time
                defaultDate: moment().hours(1).minutes(0).second(0)
            });

            this.ui.merchants.on('change', function () {
                that.getStores();
                that.ui.btnGenerateReport.prop('disabled', true);
            });

            this.ui.stores.on('change', function () {
                that.getDrivers();
            });

            this.ui.drivers.on('change', function(event) {
                event.preventDefault();
                if ($(this).find('option:selected').length) {
                    that.ui.btnGenerateReport.prop('disabled', false);
                }
            });

            this.ui.form.on('submit', function (e) {
                e.preventDefault();
                that.generateTable();
            });

        },
        resetForm: function () {
            this.ui.merchants.prop('disabled', true).selectpicker('refresh');
            this.ui.stores.prop('disabled', true).selectpicker('refresh');
            this.ui.drivers.prop('disabled', true).selectpicker('refresh');
            this.ui.orderStatus.prop('disabled', true).selectpicker('refresh');
        },
        getMerchants: function () {
            var that = this;
            var url = "/organizer/get_merchant_list ";
            var params = {
                page: {
                    "pageNumber": 1,
                    "pageSize": 10000
                }
            };
            var callback = function (status, data) {
                if (data.success) {
                    var list = data.params.merchantList.data;
                    that.setMerchants(list);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            Main.request(url, params, callback, {});
        },
        setMerchants: function (data) {
            if (data.length) {
                var options = "";
                $.each(data, function (index, val) {
                    var status = val.user.userStatus || "";
                    var option = '<option ';
                    if (status === 'INACTIVE') {
                        option += 'data-content="' + val.businessTitle + '   <span class=\'label label-danger\'>inactive</span>" ';
                    }
                    option += ' value="' + val.merchantId + '">' + val.businessTitle + '</option>';
                    options += option;
                });
                this.ui.merchants.append(options).prop('disabled', false).selectpicker('refresh');
                // this.ui.merchants.selectpicker('selectAll');
                // this.ui.merchants.selectpicker('refresh');
            }
        },
        getStores: function (merchantId) {
            var that = this;
            var selectedMerchantIds = this.ui.merchants.val()== undefined? []:this.ui.merchants.val();
            if(merchantId !== undefined){
                selectedMerchantIds = [merchantId];
            }
            var url = "/smanager/get_stores_by_merchants";
            var params = {
                page: {
                    "pageNumber": 1,
                    "pageSize": 10000
                },
                merchantIds: selectedMerchantIds
            };
            var callback = function (status, data) {
                if (data.success) {
                    var list = data.params.storeBrandList.data;
                    that.setStores(list);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            if (selectedMerchantIds != null && selectedMerchantIds.length > 0) {
                Main.request(url, params, callback, {});
            }
        },
        setStores: function (data) {
            if (data.length) {
                var options = "";
                $.each(data, function (index, val) {
                    var option = "";
                    var status = val.status || "";
                    option = '<option ';
                    if (status === 'INACTIVE') {
                        option += 'data-content="' + val.brandName + '   <span class=\'label label-danger\'>inactive</span>" ';
                    }
                    option += 'value="' + val.storeBrandId + '">' + val.brandName + '</option>';
                    options += option;
                });
                this.ui.stores.html(options).prop('disabled', false).selectpicker('refresh');
                // this.ui.stores.selectpicker('selectAll');
                // this.ui.stores.selectpicker('refresh');
            }
        },
        getDrivers: function () {
            var that = this;
            var selectedStoreBrandIds = this.ui.stores.val();
            console.log(this.ui.stores.val());
            var url = "/smanager/get_drivers_by_stores";
            var params = {
                page: {
                    "pageNumber": 1,
                    "pageSize": 10000
                },
                storeBrandIds: selectedStoreBrandIds
            };
            console.log(params)
            var callback = function (status, data) {
                if (data.success) {
                    var list = data.params.driverList.data;
                    that.setDrivers(list);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            if (Main.getFromLocalStorage('userRole') === "ROLE_STORE_MANAGER") {
                Main.request(url, params, callback, {});
            }else {
                if (selectedStoreBrandIds != null && selectedStoreBrandIds.length > 0) {
                    Main.request(url, params, callback, {});
                }
            }
        },
        setDrivers: function (data) {
            if (data.length) {
                var options = "";
                $.each(data, function (index, val) {
                    var option = "";
                    option = '<option value="' + val.driverId + '">' + val.user.firstName + ' ' + val.user.lastName + '</option>';
                    options += option;
                });
                this.ui.drivers.html(options).prop('disabled', false).selectpicker('refresh');
                // this.ui.drivers.selectpicker('selectAll');
                // this.ui.drivers.selectpicker('refresh');
            }
        },
        reportsDTInstance: "",
        generateTable: function () {
            var that = this;
            var callback = function (status, data) {
                if (data.success) {
                    var responseRows = data.params.orderList.numberOfRows;
                    var orders = data.params.orderList.data;
                    var tableData = [];
                    $.each(orders, function (index, data) {
                        var orderNo = "",
                            orderCreated = "",
                            orderCompleted = "",
                            totalTimeTaken = "",
                            storeName = "",
                            dropOffAddress = "",
                            orderState = "",
                            orderPlan = "",
                            billAmount = "",
                            deliveryFee = "",
                            paymentMode = "",
                            driverName = "",
                            distanceTravelled = "",
                            row = [];

                        orderNo = data.orderDisplayId || "";
                        orderCreated = data.createdDate || "";
                        orderCompleted = data.deliveredDate || "";

                        totalTimeTaken = moment(orderCompleted) - moment(orderCreated);

                        var d = moment.duration(totalTimeTaken, 'milliseconds');
                        var hours = Math.floor(d.asHours());
                        var mins = Math.floor(d.asMinutes()) - hours * 60;
                        totalTimeTaken = (hours + " hrs " + mins + " mins");

                        if (!$.isEmptyObject(data.store)) {
                            if (!$.isEmptyObject(data.store.storeBrand)) {
                                storeName = Main.fixUndefinedVariable(data.store.storeBrand.brandName)
                                    + "-" + Main.fixUndefinedVariable(data.store.givenLocation1)
                                    + ", " + Main.fixUndefinedVariable(data.store.givenLocation2);
                            }
                        }

                        if (!$.isEmptyObject(data.customersArea)) {
                            if (!$.isEmptyObject(data.customersArea.area)) {
                                dropOffAddress = data.customersArea.area.areaName;
                            }
                        }

                        if (data.orderStatus != null) {
                            var splitted = data.orderStatus.split("_") || "";
                            var i = 0;
                            for (i; i < splitted.length; i++) {
                                orderState += Main.ucfirst(splitted[i]) + " ";
                            }
                        } else {
                            orderState = "";
                        }

                        if (data.orderPlan != null) {
                            var splitted = data.orderPlan.split("_") || "";
                            var i = 0;
                            for (i; i < splitted.length; i++) {
                                orderPlan += Main.ucfirst(splitted[i]) + " ";
                            }
                        } else {
                            orderPlan = "";
                        }

                        if (data.totalBillAmount != null) {
                            billAmount = Main.getFromLocalStorage("currency") + " " + data.totalBillAmount;
                        } else {
                            billAmount = 'N/A';
                        }
                        if (data.deliveryCharge != null) {
                            deliveryFee = Main.getFromLocalStorage("currency") + " " + data.deliveryCharge;
                        } else {
                            deliveryFee = 'N/A';
                        }
                        paymentMode = data.paymentMode || "";

                        if (!$.isEmptyObject(data.driver)) {
                            if (!$.isEmptyObject(data.driver.user)) {
                                driverName = data.driver.user.firstName + " " + data.driver.user.lastName || "";
                            }
                        }

                        if (data.distanceTravelInKM) {
                            distanceTravelled = data.distanceTravelInKM
                                + " KM";
                        } else {
                            distanceTravelled = "N/A";
                        }


                        row = [index + 1, orderNo, orderCreated, orderCompleted, totalTimeTaken, storeName, dropOffAddress, orderState, orderPlan, billAmount, deliveryFee, paymentMode, driverName, distanceTravelled];
                        row = $.extend({}, row);
                        tableData.push(row);
                    });

                    if ( $.fn.DataTable.isDataTable( that.ui.table ) ) {
                        that.reportsDTInstance.destroy();
                        that.ui.table.empty();
                    }
                        
                    var option = {};
                    option.data = tableData;
                    option.columns = [
                        { title : "S/N" },
                        { title : "Order No" },
                        { title : "Order Created" },
                        { title : "Order Completed" },
                        { title : "Total Time Taken" },
                        { title : "Store Name" },
                        { title : "Drop Off Address" },
                        { title : "Order State" },
                        { title : "Order Plan" },
                        { title : "Bill Amount" },
                        { title : "Delivery Fee" },
                        { title : "Payment Mode" },
                        { title : "Driver" },
                        { title : "Distance Traveled" }
                    ];
                    option.lengthMenu = [[10, 25, 50, -1], [10, 25, 50, "All"]];
                    option.language = {
                        "info": "_END_ Items of _MAX_",
                        "search": "_INPUT_",
                        "searchPlaceholder": "Search..."
                    };
                    option.info = true;
                    option.dom = 'lBfrtip';
                    option.buttons = [{
                        extend: 'csv',
                        text: 'Export CSV',
                        extension: '.csv',
                        className: 'btn btn-success',
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        },
                    }];
                    option.deferRender = true;
                    that.reportsDTInstance = that.ui.table.DataTable(option);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };

            var driverIds = this.ui.drivers.val();
            var storeIds = this.ui.stores.val();
            var orderStatuses = this.ui.orderStatus.val();
            var fromDate = this.ui.fromDate.val();
            var fromTime =  moment(this.ui.fromTime.val(), "h:mm:ss a").format("HH:mm:ss");
            var toDate = this.ui.toDate.val();
            var toTime = moment(this.ui.toTime.val(), "h:mm:ss a").format("HH:mm:ss");
            var fromDateTime = fromDate + "T" + fromTime;
            var toDateTime = toDate + "T" + toTime;

            var params = {
                page: {
                    "pageNumber": 1,
                    "pageSize": 10000
                },
                driverIds: driverIds,
                storeIds: storeIds,
                orderStatuses: orderStatuses,
                fromDate: fromDateTime,
                toDate: toDateTime
            };
            if(Main.getFromLocalStorage('userRole') === "ROLE_STORE_MANAGER"){
                var storeManager = {
                    storeManagerId:Main.getFromLocalStorage('storeManagerId')
                };
                params.storeManager = storeManager
            }
            console.log(params)
            var url = pageContext + "smanager/order_report";

            callback.requestType = "POST";

            Main.request(url, params, callback, {});
        },
    };

    return {
        init: function () {
            reports.init();
        }
    };
})();