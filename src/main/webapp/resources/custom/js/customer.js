/**
 * Created by Lunek on 7/15/2016.
 */

var customerModule = (function () {

    "use strict";

    var Main = {
        pageContext: document.getElementById('pageContext').getAttribute('data-context') || undefined,
        request: function (url, parameter, callback, headers) {
            if (headers) {
                var header = {};
            }

            if (parameter.stringify !== false) {
                headers['Content-Type'] = 'application/json';
            }

            var requestType = "";
            if (callback) {
                var loaderDiv = callback.loaderDiv === undefined ? 'body' : callback.loaderDiv;
                requestType = callback.requestType;
                if (loaderDiv && $('.loader', loaderDiv).length) {
                    $(loaderDiv).addClass('loader_div clearfix').append('<div class="loader"></div>');
                }
                var hideLoader = function () {
                    if (loaderDiv) {
                        $(loaderDiv).removeClass('loader_div').children('.loader').hide().remove();
                    }
                };
            }
            $.ajax({
                url: Main.modifyURL(url),
                type: requestType || "POST",
                data: parameter.stringify ? parameter : JSON.stringify(parameter),
                headers: headers,
                async: callback.async || true,
                statusCode: {},
                success: function (data) {
                    if (callback) {
                        return callback("success", data);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (typeof xmlHttpRequest != "undefined" && (xmlHttpRequest.readyState === 0 || xmlHttpRequest.status === 0)) return;
                    if (callback && errorThrown != "abort") {
                        return callback("error", {
                            success: false,
                            message: XMLHttpRequest.getResponseHeader("errorMessage"),
                            code: XMLHttpRequest.getResponseHeader("errorCode")
                        });
                    }
                },
                complete: function () {
                    setTimeout(hideLoader, 1500);
                }
            });
        },

        globalReplace: function (string, toRemove, toReplace) {
            if (!string && !string.length) {
                return;
            }
            if (!toRemove && !toRemove.length) {
                return;
            }
            var rgxObj = new RegExp(toRemove, 'g');
            return string.replace(rgxObj, toReplace);
        },


        modifyURL: function (url) {
            if (url.indexOf("#") !== 0) {
                if (url.indexOf("://") === -1) {
                    if (url.indexOf("/") !== 0) url = "/" + url;
                    if (Main.pageContext) {
                        if (url.indexOf(Main.pageContext) > -1)
                            return url;
                        else
                            return Main.pageContext + url;
                    }

                }
                return Main.globalReplace(url, '#', '');
            }
        },
        getNamedParameter: function (key) {
            if (!key) {
                return false;
            }
            var url = window.location.search;
            var pathArr = url.split('?');
            if (pathArr.length === 1) {
                return null;
            }
            pathArr = pathArr[1].split('&');
            pathArr = Main.removeValue(pathArr, "");
            var value = "";
            for (var i = 0; i < pathArr.length; i++) {
                var keyValue = pathArr[i].split('=');
                if (keyValue[0] == key) {
                    value = keyValue[1];
                    break;
                }
            }
            return value;
        },
        removeValue: function (value, remove) {
            if (value.indexOf(remove) > -1) {
                value.splice(value.indexOf(remove), 1);
                Main.removeValue(value, remove);
            }
            return value;
        },
        sanitizeDateTime: function (dateTime) {
            var dateTimeSanitized = dateTime + "";
            return dateTimeSanitized.replace(/-/g, '/');
        },
        ucfirst: function (word) {
            if (word) {
                word += "";
                return word.substr(0, 1).toUpperCase() + word.substr(1).toLowerCase();
            } else {
                return "";
            }
        },
        getFormattedDateTime: function (dateTime) {
            if (dateTime) {
                var date = new Date(dateTime) + "";
                return date.substr(0, 24);
            } else {
                return "N/A";
            }
        }
    };

    var customerOrder = {
        init: function (id) {
            this.ui.orderDetailSection.addClass('hidden');
            this.ui.orderRatingSection.addClass('hidden');
            this.getOrderDetails(id);
            this.events();
        },
        ui: {
            orderDetailSection: $('#orderDetailSection'),
            etd: $('#etd'),
            orderStatus: $('#orderStatus'),
            storeName: $('#storeName'),
            storeAddress: $('#storeAddress'),
            driverSection: $("#driverInfoSection"),
            driverImage: $('#driverImage'),
            driverName: $('#driverName'),
            driverNumber: $('#driverNumber'),
            callDriver: $('#callDriver'),
            orderRatingSection: $('#orderRatingSection'),
            orderRatingForm: $('#orderRatingForm'),
            rateComment: $('#rateComment'),
            rating: $('input[name="rating"]'),
            rateSubmit: $('#orderRatingForm button[type="submit"]')
        },
        events: function () {
            var that = this;
            $(':radio').change(function () {
                console.log("woah");
            });
            this.ui.orderRatingForm.on('change', this.ui.rating.selector, function () {
                if (that.isRatingValid()) {
                    that.ui.rateSubmit.prop('disabled', false);
                }
            });
            this.ui.orderRatingForm.on('submit', function (event) {
                event.preventDefault();
                that.submitRating();
            });
        },
        getOrderDetails: function (orderId) {
            var url = "", header = {}, callback,
                that = this;
            url = "/customer/get_order_detail";
            header.orderId = orderId;
            callback = function (status, data) {
                if (data.success) {
                    var details = data.params.order;
                    that.showData(details);
                } else {
                    customerApp.showMessage();
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, header);
        },
        rateOrder: function (rating) {
            if (!rating) {
                return;
            }
            var url = "", params = {}, order = {}, orderRating = {}, callback;
            url = "/customer/rate_order";
            orderRating.ratingByCustomer = rating.value;
            orderRating.commentByCustomer = rating.comment || "";
            order.orderId = rating.orderId;
            order.orderRating = orderRating;
            params.order = order;
            callback = function (status, data) {
                if (data.success) {
                    customerApp.showMessage('We will continue to improve our service', 'Thank you for rating');
                } else {
                    customerApp.showMessage('Please refresh or reload the page', 'Sorry, unexpected error');
                }
            };
            callback.requestType = "PUT";
            Main.request(url, params, callback, {});
        },
        showData: function (data) {
            if (data) {
                if (data.orderRating) {
                    customerApp.showMessage('Order has already been delivered', "Thank you for using our service");
                } else if (data.orderStatus === "DELIVERED" && data.orderRating === undefined) {
                    this.displayOrderRating(data.orderId);
                } else {
                    this.displayOrderDetails(data);
                }
            }
        },
        displayOrderDetails: function (data) {
            var etd = "",
                deliveryDate = "",
                orderStatus = "",
                orderStatusArr = "",
                storeName = "",
                storeAddress = "",
                driverImage = "",
                driverName = "",
                driverNumber = "";

            deliveryDate = data.estimatedTimeOfDelivery || "";
            this.ui.etd.text(Main.getFormattedDateTime(deliveryDate));

            orderStatus = data.orderStatus || "";
            orderStatusArr = orderStatus.split('_');
            orderStatus = "";
            $.each(orderStatusArr, function (index, val) {
                orderStatus += Main.ucfirst(val) + " ";
            });
            this.ui.orderStatus.text(orderStatus);

            if (data.store) {
                storeAddress = data.store.givenLocation1 || "";
                if (storeAddress) {
                    storeAddress += ", ";
                }
                storeAddress += data.store.givenLocation2 || "";
                this.ui.storeAddress.text(storeAddress);

                if (data.store.storeBrand) {
                    storeName = data.store.storeBrand.brandName || "";
                    this.ui.storeName.text(storeName);
                }
            }

            if (data.driver) {
                if (data.driver.user) {
                    driverImage = data.driver.user.profileImage || "";
                    this.ui.driverImage.attr('src', driverImage);

                    driverNumber = data.driver.user.mobileNumber || "";
                    this.ui.driverNumber.text(driverNumber);
                    this.ui.callDriver.attr('href', "tel://" + driverNumber);

                    driverName = data.driver.user.firstName || "";
                    driverName += " ";
                    driverName += data.driver.user.lastName || "";
                    this.ui.driverName.text(driverName);
                }
            } else {
                this.ui.driverSection.html("No Driver assigned").addClass('text-left');
            }
            this.ui.orderDetailSection.removeClass('hidden');
        },
        displayOrderRating: function (data) {
            this.ui.rateSubmit.prop('disabled', true);
            this.ui.orderRatingSection.removeClass('hidden');
            this.ui.orderRatingForm.attr('data-id', data);
        },
        isRatingValid: function () {
            return this.ui.rating.is(':checked');
        },
        submitRating: function () {
            var rating = {};
            rating.value = parseInt(this.ui.rating.val());
            rating.comment = this.ui.rateComment.val();
            rating.orderId = this.ui.orderRatingForm.attr('data-id');
            this.rateOrder(rating);
        }
    };

    var customerApp = {
        ui: {
            container: $('.main-container')
        },
        init: function () {
            var id = Main.getNamedParameter('orderId');
            if (this.match(id)) {
                customerOrder.init(id);
            } else {
                this.showMessage();
            }
        },
        showMessage: function (msg, header) {
            var message = msg || "Sorry, couldn't find your order";
            var heading = header || "404";
            this.ui.container.html('<section> <p> <strong>' + heading + '</strong> </p> <p> <small>' + message + '</small> </p> </section>');
        },
        match: function (data) {
            if (data) {
                return /^[a-zA-Z0-9-/s]+$/.test(data);
            } else {
                return false;
            }
        }
    };

    return {
        init: function () {
            customerApp.init();
        }
    };
})();