/**
 * Created by PSABIN on 1/17/2017.
 */

var apiModule = (function () {

    "use strict";

    var api = {
        init: function () {
            this.events();
            this.listKeys();
        },
        ui: {
            keysDT: $('#APIKeysTable'),
            keysModal: $('#APIKeyModal'),
            keyForm: $('#APIKeyForm'),
            keyName: $('#APIName'),
            keyAPI: $('#APIKey'),
            keyDelete: $('[data-type="delete"]')
        },
        events: function () {
            var that = this;
            that.ui.keysModal.on('shown.bs.modal', function (e) {
                that.ui.keyForm[0].reset();
                var calle = $(e.relatedTarget);
                var type = calle.attr('data-type');
                if (type === 'generate') {
                    that.generate();
                } else {
                    var key = calle.attr('data-key');
                    var name = calle.attr('data-name');
                    var id = calle.attr('data-id');
                    if (!key || !name || !id) {
                        return;
                    }
                    that.ui.keyAPI.val(key);
                    that.ui.keyName.val(name);
                    that.ui.keysModal.attr('data-id', id);
                }
            }).on('hidden.bs.modal', function () {
                that.ui.keyForm[0].reset();
                that.ui.keysModal.removeAttr('data-id');
            });

            that.ui.keyForm.on('submit', function (e) {
                e.preventDefault();
                var keyName = that.ui.keyName.val() || null;
                var keyAPI = that.ui.keyAPI.val() || null;
                var keyId = that.ui.keysModal.attr('data-id') || null;
                that.save(keyName, keyAPI, keyId);
            });

            that.ui.keyAPI.on('focus', function (e) {
                e.preventDefault();
                $(this).select();
            });

            $('#APITableTab').on('click', that.ui.keyDelete.selector, function (e) {
                e.preventDefault();
                var calle = $(this);
                console.log(calle);
                var id = calle.attr('data-id');
                if (!id) {
                    console.error('No id found');
                    return;
                }
                var yes = function () {
                    that.delete(id);
                };
                yes.text = 'Yes';
                var no = function () {
                    $('.modal').modal('hide');
                };
                no.text = 'No';
                var buttons = [yes, no];
                Main.popDialog('Confirmation', 'Are you sure you want to delete the key?', buttons);
            });
        },
        generate: function () {
            var that = this;
            var url = "/merchant/generate_api_key";
            var header = {};
            header.userId = Main.getFromLocalStorage('userId');
            var callback = function (status, data) {
                if (data.success) {
                    var key = data.params.apiKey;
                    that.ui.keyAPI.val(key);
                } else {
                    $('.modal').modal('hide');
                    Main.popDialog('Error', data.message, []);
                }
            };
            callback.requestType = 'GET';
            Main.request(url, {}, callback, header);
        },
        save: function (name, key, id) {
            if (!name || !key) {
                console.error('No required data found');
                return;
            }
            var that = this;
            var url = "/merchant/save_api_key";
            var params = {};
            params.apiKey = key;
            params.apiName = name;
            if (id) {
                params.id = id;
            }
            var header = {};
            header.userId = Main.getFromLocalStorage('userId');
            var callback = function (status, data) {
                if (data.success) {
                    that.ui.keysModal.modal('hide');
                    that.listKeys();
                } else {
                    Main.popDialog('Error', data.message, []);
                }
            };
            Main.request(url, params, callback, header);
        },
        delete: function (id) {
            if (!id) {
                return;
            }
            var url = "/merchant/delete_api_key";
            var header = {};
            header.apiKeyId = id;
            header.userId = Main.getFromLocalStorage('userId');
            var callback = function (status, data) {
                if (data.success) {
                    that.listKeys();
                } else {
                    Main.popDialog('Error', data.message, [], 'error');
                }
            };
            callback.requestType = 'GET';
            Main.request(url, {}, callback, header);
        },
        listKeys: function () {
            var that = this;
            var callback = function (status, data) {
                if (data.success) {
                    var responseRows = data.params.apiKeys.numberOfRows;
                    var orders = data.params.apiKeys;
                    var tableData = [];
                    $.each(orders, function (index, data) {
                        var apiName = data.apiName || "",
                            apiDate = data.createdDate || "",
                            apiKey = data.apiKey || "",
                            action = "",
                            row = [];

                        var editKey = '<button type="button" class="btn btn-sm btn-default" data-type="edit" data-id="' + data.id + '" data-name="'+ apiName +'" data-key="' + apiKey + '" data-toggle="modal" data-target="#APIKeyModal"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> </button>';
                        var deleteKey = '<button type="button" class="btn btn-sm btn-default" data-type="delete" data-id="' + data.id + '"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> </button>';
                        action = editKey + ' ' + deleteKey;
                        row = [index + 1, apiName, apiDate, apiKey, action];
                        row = $.extend({}, row);
                        tableData.push(row);
                    });

                    if ($.fn.DataTable.isDataTable(that.ui.keysDT)) {
                        that.ui.keysDTInstance.destroy();
                        that.ui.keysDT.empty();
                    }

                    var option = {};
                    option.data = tableData;
                    option.columns = [
                        {title: "S/N"},
                        {title: "API Name"},
                        {title: "Created Date"},
                        {title: "Key"},
                        {title: "Action", sortable: false}
                    ];
                    option.lengthMenu = [[10, 25, 50, -1], [10, 25, 50, "All"]];
                    option.language = {
                        "info": "_END_ Items of _MAX_",
                        "search": "_INPUT_",
                        "searchPlaceholder": "Search..."
                    };
                    option.info = true;
                    option.dom = 'lfrtip';
                    option.deferRender = true;
                    that.ui.keysDTInstance = that.ui.keysDT.DataTable(option);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = 'GET';
            var url = "/merchant/get_active_api_keys";
            var header = {};
            header.userId = Main.getFromLocalStorage('userId');

            Main.request(url, {}, callback, header);
        }
    };

    return {
        init: function () {
            api.init();
        }
    };

})();