/**
 * Created by Lunek on 6/27/2016.
 */

var orderModule = (function () {
    "use strict";
    var submitOrderId;

    var createOrder = {
        ui: {
            formSubmitState: false,
            stores: [],
            storeDb: [],
            mainAreas: [],
            mainAreaDb: [],
            mainAreaSubDb: [],
            subAreas: [],
            subAreaDb: [],
            streets: [],
            streetDb: [],
            markerDb: [],
            customerPhoneDb: [],
            placesAutocompleteDb: [],
            placesAutocompleteService: {},
            form: $('#createOrderForm'),
            storeList: $('#pickUpLocation'),
            customerPhone: $('#customerPhone'),
            customerName: $('#customerName'),
            deliveryDate: $('#deliveryDate'),
            deliveryTime: $('#deliveryTime'),
            deliveryFee: $('#deliveryFee'),
            orderTotal: $('#orderTotal'),
            orderNote: $("#orderNote"),
            paymentMode: $('[name="modeOfPayment"]'),
            locationType: $('.js-location-type'),
            locationTypes: $('#locationType'),
            houseNumber: $('#houseNumber'),
            mainArea: $('#mainArea'),
            subArea: $('#subArea'),
            customerNote: $("#customerNote"),
            map: $('#map'),
            buildingNumber: $('#buildingNumber'),
            streetAddress: $('#streetAddress'),
            city: $('#city'),
            newAddrBtn: $('#addAddressBtn'),
            closeAddrBtn: $('#closeAddressBtn'),
            knownAreaWrapper: $('#knownAreaWrapper'),
            unknownAreaWrapper: $('#unknownAreaWrapper'),
            submitBtn: $('#orderSave'),
            cancelBtn: $('#orderCancel'),
            submitAssign: $('#orderSaveAssign'),
            saveAndAssignModal: $('#saveAndAssignModal'),
            ownDriverWrapper: $('#ownDriverWrapper'),
            bookAnyOrderDriverBtn: $('#bookAnyOrderDriverBtn'),
            assignOwnDriverBtn: $('#assignOwnDriverBtn'),
            ownDriverRadio: $('input[name="storeDriver"][type="radio"]'),
            anyOrderDriverWrapper: $('#anyOrderDriverWrapper'),
            anyOrderDriverBox: $('#anyOrderDriverBox'),
            merchantDriverBox: $('#merchantDriverBox'),
            mapCanvasModal: $('#mapCanvasModal'),
            anyOrderDriverRadio: $('input[name="anyOrderDriverRadio"][type="radio"]'),
            confirmAnyOrderDriverBtn: $('#confirmAnyOrderDriverBtn'),
            routeDeliveryDistance: $('#routeDeliveryDistance'),
            routeDeliveryFee: $('#routeDeliveryFee')
        },
        init: function () {
            this.events();
            this.getStores();
            this.initAutoPlaces();
            this.ui.deliveryDate.datetimepicker({
                useStrict: true,
                showTodayButton: true,
                minDate: moment(),
                /*inline: true,*/
               format: 'YYYY-MM-DD'
            }).val(moment().format('YYYY-MM-DD')).blur();

            this.ui.deliveryTime.datetimepicker({
                useStrict: true,
                defaultDate: moment().add(1, 'hour'),
                format: 'LTS',
                useCurrent: true
            }).blur();
        },
        events: function () {
            var that = this;
            this.validator();

            $(window).on('beforeunload', function () {
                if (!that.ui.formSubmitState) {
                    return 'Your data will not be saved. Are you sure to continue?';
                }
            });

            this.ui.form.on('db.store.on', this.ui.storeList.selector, function () {
                that.ui.storeList.autocomplete({
                    minChars: 0,
                    zIndex: 9000,
                    lookup: that.ui.storeDb,
                    autoSelectFirst: true,
                    onSelect: function (suggestion) {
                        if (suggestion.data === $(this).attr('data-old')) {
                            return;
                        }
                        $(this).attr({
                            "data-id": suggestion.data,
                            "data-lat": suggestion.lat,
                            "data-lng": suggestion.lng,
                            "data-driver": suggestion.allowStoresDriver || "false"
                        });
                        that.getStoresArea(suggestion.data);
                        $(this).blur().valid();
                        if (suggestion.allowUnknownAreaServing) {
                            that.ui.newAddrBtn.removeClass('hidden');
                        } else {
                            that.ui.newAddrBtn.addClass('hidden');
                        }
                        if (that.ui.customerPhone.val() && that.ui.customerPhone.valid()) {
                            that.getCustomerInfo(that.ui.customerPhone.val(), suggestion.data);
                        }
                        if (that.ui.markerDb.length && that.ui.form.attr('data-location') === 'unknown') {
                            $(document).trigger('maps.marker.add', [that.ui.markerDb.pop()]);
                        }
                    }
                });
            });

            this.ui.form.on('db.area.on', this.ui.mainArea.selector, function () {
                $(this).autocomplete({
                    minChars: 0,
                    zIndex: 9000,
                    lookup: that.ui.mainAreaDb,
                    autoSelectFirst: true,
                    onSelect: function (suggestion) {
                        if (suggestion.data.areaId === $(this).attr('data-id')) {
                            return;
                        }
                        $(this).attr('data-id', suggestion.data.areaId);
                        $(this).blur().valid();
                        that.ui.subArea.removeAttr('disabled data-id data-lat data-lng').val('');
                        that.ui.subArea.autocomplete({
                            minChars: 0,
                            zIndex: 9000,
                            lookup: that.ui.mainAreaDb[suggestion.data.areaId],
                            autoSelectFirst: true,
                            onSelect: function (suggestion) {
                                if (suggestion.data.areaId === $(this).attr('data-id')) {
                                    return;
                                }
                                $(this).attr({
                                    'data-id': suggestion.data.areaId,
                                    'data-lat': suggestion.data.latitude,
                                    'data-lng': suggestion.data.longitude
                                });
                                $(this).blur().valid();
                            }
                        });
                    }
                });
            });
            this.ui.locationType.on("change", function () {
                if ($('#locationType').find('.active').data('status') === "new") {
                    that.ui.houseNumber.trigger('db.street.on');
                } else {
                    that.ui.houseNumber.trigger('db.autoPlaces.on');
                }
            });
            this.ui.form.on('db.street.on', this.ui.houseNumber.selector, function () {
                that.ui.houseNumber.autocomplete({
                    minChars: 1,
                    zIndex: 9000,
                    lookup: that.ui.streetDb,
                    autoSelectFirst: true,
                    onSelect: function (suggestion) {
                        $(this).attr('data-id', suggestion.data.areaId).valid();
                        $(this).parents('.form-group.floating').addClass('focused');
                        that.ui.mainArea.val(suggestion.data.mainArea.value).attr('data-id', suggestion.data.mainArea.data.areaId).blur().valid();
                        that.ui.subArea.val(suggestion.data.subArea.value).attr({
                            'data-id': suggestion.data.subArea.data.areaId,
                            'data-lat': suggestion.data.subArea.data.latitude,
                            'data-lng': suggestion.data.subArea.data.longitude
                        }).blur().valid();
                    }
                });
                var lastAddress = that.ui.streetDb[that.ui.streetDb.length - 1];
                that.ui.houseNumber.val(lastAddress.value).attr('data-id', lastAddress.data.areaId).blur().valid();
                that.ui.mainArea.val(lastAddress.data.mainArea.value).attr('data-id', lastAddress.data.mainArea.data.areaId).blur().valid();
                that.ui.subArea.val(lastAddress.data.subArea.value).attr({
                    'data-id': lastAddress.data.subArea.data.areaId,
                    'data-lat': lastAddress.data.subArea.data.latitude,
                    'data-lng': lastAddress.data.subArea.data.longitude
                }).blur().valid();
            });

            this.ui.form.on('db.autoPlaces.on', this.ui.houseNumber.selector, function () {
                that.ui.houseNumber.autocomplete({
                    minChars: 1,
                    zIndex: 9000,
                    onSearchStart: function () {
                    },
                    lookup: function (query, done) {
                        var result = {};
                        if (query && !$.isEmptyObject(google.maps.places)) {
                            if ($.isEmptyObject(that.ui.placesAutocompleteService)) {
                                that.ui.placesAutocompleteService = new google.maps.places.AutocompleteService();
                            }
                            that.ui.placesAutocompleteService.getQueryPredictions({ input: query }, function (predictions, status) {
                                if (status != google.maps.places.PlacesServiceStatus.OK) {
                                    console.error(status);
                                    return;
                                }
                                that.ui.placesAutocompleteDb = [];
                                $.each(predictions, function(index, val) {
                                    if (val.description) {
                                        that.ui.placesAutocompleteDb.push({value: val.description});
                                    }
                                });
                                if (that.ui.placesAutocompleteDb.length) {
                                    result['suggestions'] = that.ui.placesAutocompleteDb;
                                }
                                done(result);
                            });
                        }
                    }
                });
            });

            this.ui.form.on('blur', this.ui.customerPhone.selector, function () {
                var no = $(this).val();
                var storeId = that.ui.storeList.attr('data-id') || null;
                if (no === $(this).attr('data-old')) {
                    return;
                }
                if (no !== "" && that.ui.customerPhone.valid()) {
                    that.getCustomerInfo(no, storeId);
                }
            });

            this.ui.form.on('change', this.ui.storeList.selector, function () {
                if (!$(this).valid()) {
                    $(this).removeAttr('data-id');
                }
            });

            this.ui.form.on('blur', this.ui.mainArea.selector, function () {
                if (!that.ui.mainArea.valid()) {
                    that.ui.subArea.prop('disabled', true);
                } else {
                    that.ui.subArea.prop('disabled', false);
                }
            });

            this.ui.form.on('click', this.ui.newAddrBtn.selector, function () {
                that.ui.locationType.addClass('disabled');
                that.ui.knownAreaWrapper.addClass('hidden');
                that.ui.unknownAreaWrapper.removeClass('hidden');

                google.maps.event.trigger(superMap, "resize");
                // $(this).attr('data-open', true);
                // $(this).text('Close Other Address');
                that.ui.form.attr('data-location', 'unknown');
                that.ui.city.rules("add", {
                    required: true
                });
                that.ui.streetAddress.rules("add", {
                    required: true
                });
                that.ui.buildingNumber.rules("add", {
                    required: true
                });
                // that.ui.houseNumber.rules("remove", "required");
                that.ui.mainArea.rules("remove");
                that.ui.subArea.rules("remove");
            });

            this.ui.form.on('click', this.ui.closeAddrBtn.selector, function () {
                console.log('clicked');
                that.ui.locationType.removeClass('disabled');
                if (that.ui.form.attr('data-customer') !== 'known') {
                    that.ui.locationType.filter('[data-status="existing"]').addClass('disabled');
                }
                that.ui.knownAreaWrapper.removeClass('hidden');
                that.ui.unknownAreaWrapper.addClass('hidden');
                // $(this).attr('data-open', false);
                // $(this).text('Add Other Address');
                that.ui.form.attr('data-location', 'known');
                // that.ui.houseNumber.rules("add", {
                //     required: true
                // });
                that.ui.mainArea.rules("add", {
                    required: true
                });
                that.ui.subArea.rules("add", {
                    required: true
                });
                that.ui.city.rules("remove");
                that.ui.streetAddress.rules("remove");
                that.ui.buildingNumber.rules("remove");
            });

            this.ui.form.on('click', this.ui.cancelBtn.selector, function (event) {
                event.preventDefault();
                window.location = pageContext + "smanager/order_list";
            });

            this.ui.form.on('click', this.ui.submitAssign.selector, function () {
                if (that.ui.form.valid()) {

                    that.ui.form.attr('data-status', $(this).attr('name'));
                    var storeId = that.ui.storeList.attr('data-id');

                    if (typeof(storeId) === "undefined") {
                        Main.popDialog('Error', 'Please select the pick up location to save & assign driver.');
                        return;
                    }

                    var areaId = that.ui.subArea.attr('data-id');

                    if (typeof(areaId) === "undefined") {
                        var locationType = that.ui.form.attr('data-location');
                        if (locationType === "known") {
                            Main.popDialog('Error', 'Please select the drop location to save & assign driver.');
                            return;
                        }

                    }
                    if (that.ui.storeList.attr('data-driver') === 'true') {
                        that.getDriversByStore(storeId, areaId);
                    }

                    that.ui.saveAndAssignModal.modal('show');
                }
            });

            this.ui.form.on('click', this.ui.submitBtn.selector, function () {
                that.ui.form.attr('data-status', $(this).attr('name'));
                that.ui.form.submit();
            });

            this.ui.form.on('click', this.ui.locationType.selector, function () {
                if (!$(this).hasClass('disabled')) {
                    if (!$(this).hasClass('active')) {
                        that.ui.locationType.removeClass('active');
                        $(this).addClass('active');
                    }
                    that.ui.houseNumber.val("");
                    that.ui.mainArea.val("");
                    that.ui.subArea.val("");
                    if ($(this).attr('data-status') === 'existing') {
                        that.ui.form.attr('data-location', 'known');
                        that.setExistingAreaAutoComplete();
                    } else {
                        that.ui.form.attr('data-location', 'new');
                        // that.ui.houseNumber.rules('remove');
                        // that.ui.houseNumber.rules('add', {
                        //     required: true
                        // });
                        that.setAreaAutoComplete();
                    }
                    that.ui.mainArea.focus();
                }
            });

            this.ui.saveAndAssignModal.on('show.modal.bs', function () {
                var mapConfigAssign = {
                    zoom: 10,
                    center: new google.maps.LatLng(28.3949, 84.1240),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapCanvas: "mapCanvasModal",
                    //mapVar: mapAssign
                };
                linkedMarkersModule.init(mapConfigAssign);

                var markers = {
                    store: {
                        lat: Number(that.ui.storeList.data('lat')),
                        lng: Number(that.ui.storeList.data('lng')),
                        title: that.ui.storeList.val(),
                        icon: pageContext + "resources/custom/images/map-ico/map_merchant.png"
                    },
                    customer: {
                        lat: Number(that.ui.subArea.data('lat')),
                        lng: Number(that.ui.subArea.data('lng')),
                        title: that.ui.customerName.val() + "<br>" + that.ui.customerPhone.val(),
                        icon: pageContext + "resources/custom/images/map-ico/map_customer.png"
                    }
                };
                linkedMarkersModule.setMarkers([markers]);

                that.ui.assignOwnDriverBtn.attr('disabled', 'disabled');
                that.checkAllowedDriver();

            }).on('hidden.modal.bs', function () {
                that.ui.form.removeAttr('data-drivertype');
                that.ui.form.removeAttr('data-driver');
            });

            this.ui.saveAndAssignModal.on('change', this.ui.ownDriverRadio.selector, function (e) {
                var radio = $(e.target);
                that.ui.assignOwnDriverBtn.removeAttr('disabled');
                that.ui.form.attr('data-driver', radio.attr('value'));
            }).on('change', this.ui.anyOrderDriverRadio.selector, function () {
                that.ui.confirmAnyOrderDriverBtn.removeAttr('disabled');
            });

            this.ui.saveAndAssignModal.on('click', '[data-dismiss="modal"]', function (event) {
                event.preventDefault();
                if (that.ui.form.attr('data-order') === 'saved') {
                    var btn1 = function () {
                        window.location = pageContext + 'smanager/order_list';
                    };
                    btn1.text = "Close";
                    var btn2 = function () {
                        window.location = pageContext + 'smanager/order_form';
                    };
                    btn2.text = "Add Another Order";

                    var button = [btn1, btn2];
                    Main.popDialog('Order complete', 'Order has already been saved', button, 'success', true);
                    that.ui.form.removeAttr('data-order');
                }
            });

            this.ui.assignOwnDriverBtn.click(function () {
                that.ui.form.attr('data-drivertype', 'ownDriver');
                that.ui.form.submit();
            });

            this.ui.bookAnyOrderDriverBtn.click(function () {
                that.ui.form.attr('data-drivertype', 'anyOrderDriver');
                that.ui.form.removeAttr('data-driver');
                that.ui.ownDriverRadio.prop('checked', false);
                that.ui.assignOwnDriverBtn.attr('disabled', 'disabled');
                that.ui.form.submit();
            });

            this.ui.deliveryTime.on('focus', function (e) {
                //$(this).val(moment().add(1, 'hour').format('hh:mm:ss'))
            });

            $(document).on('maps.marker.add', function (event, data) {
                event.preventDefault();

                //console.log(data);

                if (data) {
                    that.setMapInfo(data);
                    that.ui.markerDb.push(data);
                }
            });

            $(document).on('maps.marker.remove', function (event, data) {
                event.preventDefault();

                //console.log(data);


                if (data) {
                    that.resetRate();
                    that.ui.markerDb = [];
                }
            });

            this.ui.confirmAnyOrderDriverBtn.on('click', function () {
                var orderId = submitOrderId;
                var driverId = $('input[name="anyOrderDriverRadio"][type="radio"]:checked').val();
                if (driverId) {
                    that.assignDriver(orderId, driverId);
                }
            });
        },
        getAreas: function (outerCallback) {
            var url = "/smanager/get_active_area_list",
                params = {},
                headers = {},
                callback,
                that = this;
            if (typeof outerCallback === 'function') {
                callback = outerCallback;
            } else {
                callback = function (status, data) {
                    if (data.success) {
                        var areaList = data.params.areas;
                        that.setAreaAutoComplete(areaList);
                    } else {
                        Main.popDialog('Error', data.message, null, 'error');
                    }
                };
            }
            callback.requestType = "GET";
            Main.request(url, params, callback, headers);
        },
        getStores: function (outerCallback) {
            var url = "/smanager/get_store_list",
                params = {},
                headers = {userId: Main.getFromLocalStorage('userId')},
                callback,
                that = this;

            if (typeof outerCallback === 'function') {
                callback = outerCallback;
            } else {
                callback = function (status, data) {
                    if (data.success) {
                        var storeList = data.params.stores;
                        that.setStoreAutoComplete(storeList);
                    } else {
                        Main.popDialog('Error', data.message, null, 'error');
                    }
                };
            }
            callback.requestType = "GET";
            Main.request(url, params, callback, headers);
        },
        getStoresArea: function (storeId) {
            if (!storeId) {
                return;
            }
            var that = this,
                url = "",
                header = {},
                callback;

            url = "/smanager/get_store_serving_area_list";
            header.storeId = storeId;
            callback = function (status, data) {
                if (data.success) {
                    that.ui.storeList.attr('data-old', storeId);
                    var storeAreaList = data.params.areas;
                    that.setAreaAutoComplete(storeAreaList);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = 'GET';
            Main.request(url, {}, callback, header);
        },
        getCustomerInfo: function (number, storeId, outerCallback) {
            var url = "/smanager/get_customers_info",
                params = {},
                headers = {},
                callback,
                that = this;

            if (storeId) {
                headers.storeId = storeId;
            }
            params.mobileNumber = number;
            if (typeof outerCallback === 'function') {
                callback = outerCallback;
            } else {
                callback = function (status, data) {
                    if (data.success) {
                        that.ui.customerPhone.attr('data-old', number);
                        var customerList = data.params.customer;
                        that.setCustomerInfo(customerList);
                    } else {
                        Main.popDialog('Error', data.message, null, 'error');
                    }
                };
            }
            Main.request(url, params, callback, headers);
        },
        getDriversByStore: function (storeId, customerAreaId) {
            var that = this;
            var url = pageContext + "smanager/get_eligible_driver_list";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var storeDrivers = data.params.driverList;
                    var storeDriversUI = "";
                    if (storeDrivers.length) {
                        $.each(storeDrivers, function (index, data) {
                            //console.log(data);
                            var rating = data.rating;

                            var ratingUI = that.makeRatingStars(rating);

                            var waitingTime = data.totalOrderEtd;
                            var remDateTime = moment(waitingTime).toNow(true);

                            storeDriversUI += '<label for="' + data.driverId + '"><div class="od-box box-me p15 mv15 mh5"> ' +
                                '<div class="flex-parent"> ' +
                                '<div class="flex-child w50">' +
                                '<input id="' + data.driverId + '" type="radio" name="storeDriver" class="with-font form-control" value="' + data.driverId + '"> ' +
                                '<label for="' + data.driverId + '" class="control-label fontnormal cursorpointer"></label> ' +
                                '</div> ' +
                                '<div class="flex-child text-center"> ' +
                                '<img src="' + pageContext + 'resources/custom/images/map-ico/map_store_driver.png" alt="Driver Icon" class="w50"> </div> ' +
                                '<div class="flex-child w600 border-right"> ' +
                                '<p class="fontbold mb0">' + data.user.firstName + ' ' + data.user.lastName + '</p> ' +
                                '<p class="mb0">' + Main.fixUndefinedVariable(data.streetAddress) + '</p> ' +
                                '<div class="rating">' +
                                ratingUI +
                                ' </div> </div> ' +
                                '<div class="flex-child text-center border-right"> ' +
                                '<p class="mb0">' + remDateTime + '</p> <p class="text-muted font12">Waiting Time</p> </div> ' +
                                '<div class="flex-child text-center"> ' +
                                '<p class="mb0">' + data.currentOrderCount + '</p> <p class="text-muted font12">Currect Orders</p> </div> ' +
                                '</div> </div></label>';
                        });
                    } else {
                        storeDriversUI = "No driver available";
                    }
                    that.ui.ownDriverWrapper.html(storeDriversUI);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "POST";
            var data = {
                "store": {
                    "storeId": storeId
                }, "area": {
                    "areaId": customerAreaId
                }
            };
            /* var data = {
             "page": {
             "pageNumber": 1,
             "pageSize": 100
             }, "store": {
             "storeId": storeId
             }
             };*/
            Main.request(url, data, callback, {});
        },
        getRate: function (data) {
            var that = this,
                url = "",
                params = {},
                header = {},
                callback;

            if (!data && !data.lat && !data.lng && !data.storeId) {
                return;
            }

            url = "/smanager/estimate_delivery_charge";
            params.latitude = data.lat;
            params.longitude = data.lng;
            header.storeid = data.storeId;
            callback = function (status, data) {
                if (data.success) {
                    var rateData = data.params.data;
                    that.setRate(rateData);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            Main.request(url, params, callback, header);
        },
        setStoreAutoComplete: function (data) {
            var that = this,
                store = {};
            that.ui.storeDb = [];
            that.ui.stores = [];
            if (data.length) {
                $.each(data, function (index, val) {
                    store = {};
                    store.data = val.storeId;
                    store.value = val.addressNote;
                    store.lat = val.latitude;
                    store.lng = val.longitude;
                    store.allowStoresDriver = val.allowStoresDriver || false;
                    store.allowUnknownAreaServing = val.allowUnknownAreaServing || false;
                    that.ui.storeDb.push(store);
                    that.ui.stores.push(store.value);
                });
                that.ui.storeList.trigger('db.store.on');
                if (Main.userRole === 'ROLE_STORE_MANAGER') {
                    that.ui.storeList.val(that.ui.storeDb[0].value).focus().prop('readonly', true);
                }
            }
        },
        setAreaAutoComplete: function (data) {
            var that = this;
            if (data) {
                that.ui.mainAreaSubDb = [];
                that.ui.subAreaDb = [];
                that.ui.mainAreaDb = [];
                that.ui.mainAreaSubDb = [];
                var mainArea = {},
                    subArea = {},
                    group = {};
                $.each(data, function (index, mainVal) {
                    if (mainVal.child.length) {
                        $.each(mainVal.child, function (index, subVal) {
                            subArea = {};
                            group = {};
                            group.areaId = subVal.areaId;
                            group.status = subVal.status;
                            group.latitude = subVal.latitude;
                            group.longitude = subVal.longitude;
                            group.parent = mainVal.areaName;
                            group.parentId = mainVal.areaId;
                            subArea.value = subVal.areaName;
                            that.ui.subAreas.push(subVal.areaName);
                            subArea.data = group;
                            that.ui.mainAreaSubDb.push(subArea);
                            that.ui.subAreaDb.push(subArea);
                        });
                    }
                    mainArea = {};
                    group = {};
                    mainArea.value = mainVal.areaName;
                    that.ui.mainAreas.push(mainVal.areaName);
                    group.areaId = mainVal.areaId;
                    group.status = mainVal.status;
                    mainArea.data = group;
                    that.ui.mainAreaDb.push(mainArea);
                    that.ui.mainAreaDb[mainVal.areaId] = that.ui.mainAreaSubDb;
                    that.ui.mainAreaSubDb = [];
                });
            }
            this.ui.houseNumber.autocomplete('dispose').removeAttr('data-id').trigger('db.autoPlaces.on');
            this.ui.mainArea.autocomplete('dispose').prop('readonly', false).removeAttr('data-id');
            this.ui.subArea.autocomplete('dispose').prop('readonly', false).removeAttr('data-id data-lat data-lng');
            this.ui.mainArea.trigger('db.area.on');
        },
        setExistingAreaAutoComplete: function (data) {
            var that = this;
            if (data) {
                that.ui.streets = [];
                that.ui.streetDb = [];
                var mainArea = {},
                    subArea = {},
                    street = {},
                    mainAreaGroup = {},
                    subAreaGroup = {},
                    streetGroup = {};

                $.each(data, function (index, value) {
                    mainArea = {};
                    subArea = {};
                    street = {};
                    mainAreaGroup = {};
                    subAreaGroup = {};
                    streetGroup = {};

                    // Street
                    street.value = value.street;
                    streetGroup.areaId = value.customersAreaId;
                    streetGroup.mainArea = mainArea;
                    streetGroup.subArea = subArea;
                    street.data = streetGroup;

                    // Main Area
                    mainArea.value = value.area.parent.areaName;
                    mainAreaGroup.areaId = value.area.parent.areaId;
                    mainArea.data = mainAreaGroup;

                    // Sub Area
                    subArea.value = value.area.areaName;
                    subAreaGroup.areaId = value.area.areaId;
                    subAreaGroup.latitude = value.area.latitude;
                    subAreaGroup.longitude = value.area.longitude;
                    subArea.data = subAreaGroup;

                    that.ui.streetDb.push(street);
                    that.ui.streets.push(value.street);
                });
            }

            this.ui.houseNumber.autocomplete('dispose').removeAttr('data-id');
            this.ui.mainArea.autocomplete('dispose').prop('readonly', true).removeAttr('data-id');
            this.ui.subArea.autocomplete('dispose').prop('readonly', true).removeAttr('data-id data-lat data-lng');
            this.ui.houseNumber.trigger('db.street.on');
            /*if (this.ui.locationTypes.find('.active').data('status') === "new"){
             that.ui.houseNumber.rules("remove");
             }else{
             this.ui.houseNumber.rules('remove', {
             lookup: 'streets',
             messages: {
             lookup: 'Please choose a Known Address'
             }
             });
             }*/
        },
        setCustomerInfo: function (data) {
            if (!$.isEmptyObject(data)) {
                var customerArea = [];
                this.ui.locationType.removeClass('disabled').removeClass('active');
                if (data.customersArea.length) {
                    customerArea = data.customersArea[data.customersArea.length - 1];
                    // this.ui.houseNumber.attr('data-id', customerArea.customersAreaId).val(customerArea.street).blur();
                    // this.ui.subArea.attr({
                    //     'data-id': customerArea.area.areaId,
                    //     'data-lat': customerArea.area.latitude,
                    //     'data-lng': customerArea.area.longitude
                    // }).val(customerArea.area.areaName).blur();
                    // this.ui.mainArea.attr('data-id', customerArea.area.parent.areaId).val(customerArea.area.parent.areaName).blur();
                    this.ui.locationType.filter('[data-status="existing"]').addClass('active');
                    this.ui.form.attr('data-location', 'known');
                    this.setExistingAreaAutoComplete(data.customersArea);
                } else {
                    this.ui.form.attr('data-location', 'new');
                    this.ui.houseNumber.removeAttr('data-id').val("").trigger('db.autoPlaces.on');
                    this.ui.mainArea.removeAttr('data-id').val("");
                    this.ui.subArea.removeAttr('data-id data-lat data-lng').val("");
                    this.ui.locationType.filter('[data-status="existing"]').addClass('disabled');
                    this.ui.locationType.filter('[data-status="new"]').click();
                    this.ui.streetDb = [];
                }

                if (!$.isEmptyObject(data.user)) {
                    this.ui.customerName.val(Main.fixUndefinedVariable(data.user.firstName) + " " + Main.fixUndefinedVariable(data.user.lastName)).blur().prop('disabled', true);
                }

                if (data.customerId) {
                    this.ui.customerName.attr('data-id', data.customerId);
                    this.ui.form.attr('data-customer', 'known');
                }
            } else {
                this.ui.form.attr('data-customer', 'unknown');
                this.ui.customerName.removeAttr('data-id').prop('disabled', false).val("");
                this.ui.houseNumber.removeAttr('data-id').val("").trigger('db.autoPlaces.on');
                this.ui.mainArea.removeAttr('data-id').val("");
                this.ui.subArea.removeAttr('data-id').val("");
                this.ui.locationType.filter('[data-status="existing"]').addClass('disabled');
                this.ui.locationType.filter('[data-status="new"]').click();
                this.ui.streetDb = [];
            }
        },
        setMapInfo: function (data) {
            var position = data.position;
            position.storeId = this.ui.storeList.attr('data-id') || null;
            if (!position.storeId) {
                return;
            }
            this.ui.city.val(data.city || "").blur();
            this.ui.streetAddress.val(data.sublocality || data.locality || "").blur();
            this.getRate(position);
        },
        setRate: function (data) {
            if (data) {
                if (data.estimatedDeliveryCharge) {
                    this.ui.routeDeliveryFee.html(Main.getFromLocalStorage('currency') + " " + Main.numberWithCommas(data.estimatedDeliveryCharge));
                }
                if (data.distanceInKm) {
                    this.ui.routeDeliveryDistance.html(data.distanceInKm.toFixed(2) + ' km');
                }
            }
        },
        resetRate: function () {
            this.ui.routeDeliveryFee.html('0');
            this.ui.routeDeliveryDistance.html('0');
        },
        checkAllowedDriver: function () {
            if (this.ui.storeList.attr('data-driver') === 'false') {
                this.ui.merchantDriverBox.parent().addClass('hidden');
                this.ui.anyOrderDriverBox.parent().removeClass('col-md-6').addClass('col-md-12');
            } else {
                this.ui.merchantDriverBox.parent().removeClass('hidden');
                this.ui.anyOrderDriverBox.parent().addClass('col-md-6').removeClass('col-md-12');
            }
        },
        initAutoPlaces: function () {
            this.ui.houseNumber.trigger('db.autoPlaces.on');
        },
        validator: function () {
            var that = this;
            this.ui.form.validate({
                rules: {
                    pickUpLocation: {
                        required: true,
                        lookup: 'stores'
                    },
                    customerPhone: {
                        number: true,
                        rangelength: [9, 10]
                    },
                    deliveryDate: {
                        date: true
                    },
                    deliveryFee: {
                        number: true,
                        min: 0
                    },
                    orderTotal: {
                        number: true,
                        min: 0
                    },
                    /* houseNumber: {
                     required: true
                     },*/
                    mainArea: {
                        required: true,
                        lookup: 'mainAreas'
                    },
                    subArea: {
                        required: true,
                        lookup: 'subAreas'
                    }
                },
                messages: {
                    pickUpLocation: {
                        lookup: "Please select store from the list"
                    },
                    mainArea: {
                        lookup: "Please select known mainarea from the list"
                    },
                    subArea: {
                        lookup: "Please select known subarea from the list"
                    }
                },
                submitHandler: function () {
                    var customerPhone = "",
                        customerName = "",
                        customerLastName = "",
                        deliveryDate = "",
                        deliveryTime = "",
                        deliveryFee = "",
                        description = "",
                        orderTotal = "",
                        orderNote = "",
                        modeOfPayment = "",
                        houseNumber = "",
                        mainAreaId = "",
                        subAreaId = "",
                        customerNote = "",
                        streetAddress = "",
                        city = "",
                        buildingNumber = "",
                        latitude = "",
                        longitude = "",
                        orderStatus = "",
                        splitName = [],
                        order = {},
                        customer = {},
                        user = {},
                        area = {},
                        customersArea = {},
                        content = {},
                        header = {};

                    if (Main.getFromLocalStorage('userRole') !== 'ROLE_STORE_MANAGER') {
                        header.storeId = that.ui.storeList.attr('data-id');
                    }

                    description = that.ui.orderNote.val();
                    deliveryDate = that.ui.deliveryDate.val();
                    deliveryTime = that.ui.deliveryTime.val();
                    deliveryFee = that.ui.deliveryFee.val();
                    orderTotal = that.ui.orderTotal.val();
                    customerNote = that.ui.customerNote.val();
                    modeOfPayment = that.ui.paymentMode.filter(':checked').val();

                    customerName = that.ui.customerName.val();
                    customerPhone = that.ui.customerPhone.val();

                    mainAreaId = that.ui.mainArea.attr('data-id');
                    subAreaId = that.ui.subArea.attr('data-id');
                    houseNumber = that.ui.houseNumber.val();

                    streetAddress = that.ui.streetAddress.val();
                    city = that.ui.city.val();
                    buildingNumber = that.ui.buildingNumber.val();

                    if (that.ui.form.attr('data-customer') === 'unknown') {
                        if (customerName && customerName.length) {
                            splitName = (customerName) ? customerName.split(" ") : null;
                            user.firstName = ((splitName[0]).length) ? splitName[0] : null;
                            if (splitName.length > 1) {
                                splitName[0] = "";
                                user.lastName = splitName.join(" ").trim();
                            }
                        }
                        if (customerPhone && customerPhone.length) {
                            user.mobileNumber = customerPhone || null;
                        }
                        if (!$.isEmptyObject(user)) {
                            customer.user = user;
                        }
                    } else {
                        customer.customerId = that.ui.customerName.attr('data-id');
                    }

                    if (that.ui.form.attr('data-location') === 'unknown') {
                        if (streetAddress && streetAddress.length) {
                            customersArea.street = streetAddress || null;
                        }
                        if (buildingNumber && buildingNumber.length) {
                            customersArea.additionalNote = buildingNumber || null;
                        }
                        if (that.ui.markerDb && that.ui.markerDb.length) {
                            var marker = that.ui.markerDb.pop();
                            customersArea.latitude = marker.position.lat;
                            customersArea.longitude = marker.position.lng;
                        }
                    } else {
                        if (that.ui.form.attr('data-location') === 'known') {
                            customersArea.customersAreaId = that.ui.houseNumber.attr('data-id');
                        } else {
                            if (subAreaId && subAreaId.length) {
                                area.areaId = subAreaId || null;
                            }
                            if (houseNumber && houseNumber.length) {
                                customersArea.street = houseNumber || null;
                            }
                            customersArea.area = area;
                        }
                    }

                    if (!$.isEmptyObject(customersArea)) {
                        customer.customersArea = [];
                        customer.customersArea.push(customersArea);
                    }

                    if (description && description.length) {
                        order.orderDescription = description || null;
                    }
                    if (deliveryDate && deliveryDate.length && deliveryTime && deliveryTime.length) {
                        order.orderDeliveryDate = moment(deliveryDate + " " + deliveryTime, "YYYY-MM-DD h:mm:ss A").format("YYYY-MM-DDTHH:mm:ss");
                    }
                    if (deliveryFee && deliveryFee.length) {
                        order.deliveryCharge = deliveryFee || null;
                    }
                    if (orderTotal && orderTotal.length) {
                        order.totalBillAmount = orderTotal || null;
                    }
                    if (customerNote && customerNote.length) {
                        order.customersNoteToDriver = customerNote || null;
                    }
                    if (modeOfPayment && modeOfPayment.length) {
                        order.paymentMode = modeOfPayment || null;
                    }

                    if (that.ui.form.attr('data-status') === "orderSaveAssign") {
                        content.saveAndAssign = true;

                        if (that.ui.form.attr('data-drivertype') === "ownDriver") {
                            content.driverId = that.ui.form.attr('data-driver');
                        }


                    } else {
                        content.saveAndAssign = false;
                    }

                    order.customer = customer;
                    content.order = order;
                    //that.ui.saveAndAssignModal.modal('hide');
                    that.save(content, header);
                    return false;
                },
                invalidHandler: function () {
                    that.ui.saveAndAssignModal.modal('hide');
                }
            });
            $.validator.addMethod("lookup", function (value, element, param) {
                var result = true;
                if ($.inArray(value, that.ui[param]) < 0) {
                    result = false;
                }
                return result;
            }, 'Please enter known entity.');
        },
        save: function (params, header) {
            var that = this;
            this.ui.submitBtn.prop('disabled', true);
            this.ui.submitAssign.prop('disabled', true);
            var url = "/smanager/create_order";
            var callback = function (status, data) {
                if (data.success) {
                    that.ui.form.attr('data-order', 'saved');
                    that.ui.formSubmitState = true;
                    var details = data.params.data;
                    submitOrderId = details.orderId;
                    if (details.drivers && details.drivers.length) {
                        that.getAnyorderDrivers(details.drivers);
                    } else {
                        that.ui.saveAndAssignModal.modal('hide');
                        var btn1 = function () {
                            window.location = pageContext + 'smanager/order_form';
                        };
                        btn1.text = "Add Another Order";
                        var btn2 = function () {
                            if(details.driverId){
                                window.location = pageContext + 'smanager/order_list#orderListRunningTab';
                            }else{
                                window.location = pageContext + 'smanager/order_list#orderListWaitingTab';
                            }
                        };
                        btn2.text = "Close";

                        var button = [btn1, btn2];
                        Main.popDialog('Success', data.message, button, 'success', true);
                    }
                    if (that.ui.form.attr('data-status') === 'orderSaveAssign') {
                        that.ui.saveAndAssignModal.data('bs.modal').options.backdrop = 'static';
                        that.ui.saveAndAssignModal.data('bs.modal').options.keyboard = false;
                    }
                } else {
                    that.ui.saveAndAssignModal.modal('hide');
                    Main.popDialog('Error', data.message, null, 'error');
                    that.ui.submitBtn.prop('disabled', false);
                    that.ui.submitAssign.prop('disabled', false);
                }
            };
            Main.request(url, params, callback, header);
        },

        getAnyorderDrivers: function (anyOrderDrivers) {
            var that = this;
            var driversUI = "";
            $.each(anyOrderDrivers, function (index, data) {
                var rating = data.rating || 0;
                var ratingUI = that.makeRatingStars(rating);

                var etdTime = data.totalOrderEtd;
                var remDateTime = moment.duration(etdTime, "minutes").humanize(true);

                driversUI += '<label for="' + data.driverId + '"><div class="od-box box-me p15 mv15 mh5"> ' +
                    '<div class="flex-parent"> ' +
                    '<div class="flex-child w50">' +
                    '<input id="' + data.driverId + '" type="radio" name="anyOrderDriverRadio" class="with-font form-control" value="' + data.driverId + '"> ' +
                    '<label for="' + data.driverId + '" class="control-label fontnormal cursorpointer"></label> ' +
                    '</div> ' +
                    '<div class="flex-child text-center"> ' +
                    '<img src="' + pageContext + 'resources/custom/images/map-ico/map_store_driver.png" alt="Driver Icon" class="w50"> </div> ' +
                    '<div class="flex-child w600 border-right"> ' +
                    '<p class="fontbold mb0">' + data.user.firstName + ' ' + data.user.lastName + '</p> ' +
                    '<p class="mb0">' + Main.fixUndefinedVariable(data.streetAddress) + '</p> ' +
                    '<div class="rating"> ' +
                    ratingUI +
                    ' </div> </div> ' +
                    '<div class="flex-child text-center border-right"> ' +
                    '<p class="mb0">' + remDateTime + '</p> <p class="text-muted font12">ETD</p> </div> ' +
                    '<div class="flex-child text-center"> ' +
                    '<p class="mb0">' + Main.fixUndefinedVariable(data.currentOrderCount) + '</p> <p class="text-muted font12">Currect Orders</p> </div> ' +
                    '</div> </div></label>';
            });
            that.ui.anyOrderDriverWrapper.html(driversUI);
            that.ui.anyOrderDriverBox.removeClass('hidden');
            that.ui.mapCanvasModal.hide();
            that.ui.bookAnyOrderDriverBtn.hide();
        },


        assignDriver: function (orderId, driverId) {
            console.log(orderId);
            console.log(driverId);
            var that = this;
            var url = pageContext + "smanager/assign_driver_to_order"; //"smanager/assign_order";
            var callback = function (status, data) {
                if (data.success) {
                    var drivers = data.params.drivers;
                    if (!drivers) {
                        drivers = data.params.data.drivers;
                    }
                    if (drivers.length > 0) {
                        that.getAnyorderDrivers(drivers);
                    } else {
                        that.ui.saveAndAssignModal.modal('hide');
                        var btn = function () {
                            window.location = pageContext + 'smanager/order_list#orderListRunningTab';
                        };
                        btn.text = "Close";
                        var button = [btn];
                        Main.popDialog('Success', data.message, button, 'success', true);
                    }

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            var headerParams = {
                orderid: orderId
            };
            if (driverId) {
                headerParams.driverid = driverId;
            }
            //console.log(orderId)
            Main.request(url, {}, callback, headerParams);
        },

        makeRatingStars: function (ratingNumber) {
            var ratingUI = "";
            var ratingCounter = 0;
            for (ratingCounter; ratingCounter < Number(ratingNumber); ratingCounter++) {
                ratingUI += '<i class="fa fa-star c-t-gold"></i>';
            }
            var remainingRating = 5 - ratingNumber;
            for (var j = 0; j < Number(remainingRating); j++) {
                ratingUI += '<i class="fa fa-star-o"></i>';
            }

            return ratingUI;
        },
    };

    return {
        create: function () {
            createOrder.init();
        }
    };

})();