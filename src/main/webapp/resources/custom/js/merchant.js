/**
 * Created by Pratik on 6/10/2016.
 */

var listMerchantModule = (function () {
    var listMerchant = {
            ui: {
                superTable: $('#listMerchantTable'),
                merchantStatus: $('.activation-status'),
                addMerchantBtn: $('#addMerchantBtn')

            },
            init: function () {
                this.getMerchantList();
                this.events();
            },
            events: function () {
                var that = this;
                this.ui.superTable.on('click', this.ui.merchantStatus, function (e) {
                    var link = $(e.target);
                    var id = link.attr('data-id');
                    var status = link.attr('data-status');
                    if (typeof(id) != "undefined" && typeof(status) != "undefined") {
                        var btnCancel = function () {
                            $('#popDialog').modal('hide');
                        };
                        btnCancel.text = "Cancel";

                        var btnConfirm = function () {
                            that.changeMerchantStatus(id, status);
                            $('#popDialog').modal('hide');
                        };
                        btnConfirm.text = "Yes";

                        var button = [btnConfirm, btnCancel];

                        Main.popDialog("Confirm", "Are you sure you want to change the status?", button, null, null);

                    }
                });

                this.ui.addMerchantBtn.on('click', function () {
                    Main.doLogout('/signup');
                });
            },

            getMerchantList: function () {
                var that = this;
                var dataFilter = function (data, type) {
                    if (data.success) {
                        var responseRows = data.params.merchantList.numberOfRows;
                        var merchants = data.params.merchantList.data;
                        var tableData = [];
                        var businessTitle = "",
                            name = "",
                            email = "",
                            mobile = "",
                            status = "",
                            actionActivation = "",
                            action = "",
                            row = [];
                        $.each(merchants, function (index, data) {
                            var user = data.user || {};
                            businessTitle = "";
                            name = "";
                            email = "";
                            mobile = "";
                            status = "";
                            actionActivation = "";
                            action = "";
                            row = [];

                            businessTitle = data.businessTitle || "";
                            name = user.firstName || "";
                            name += " ";
                            name += user.lastName || "";
                            name = name.trim();

                            email = user.emailAddress || "";
                            mobile = user.mobileNumber || "";
                            status = user.userStatus || "";
                            status = Main.ucfirst(status);

                            if (user.userStatus) {
                                if (user.userStatus === "ACTIVE") {
                                    actionActivation = ' <span class="ph5">|</span> ' +
                                        '<a class="activation-status " href="#" data-id="' + data.merchantId + '" data-status="INACTIVE">Deactivate</a>';
                                } else if (data.user.userStatus === "UNVERIFIED") {
                                    actionActivation = '';
                                } else if (data.user.userStatus === "INACTIVE") {
                                    actionActivation = ' <span class="ph5">|</span> ' +
                                        '<a class="activation-status " href="#" data-id="' + data.merchantId + '" data-status="ACTIVE">Activate</a>';
                                }
                            }

                            action = '<div class="action_links">' +
                                '<a href="' + pageContext + 'merchant/profile?merchantId=' + data.merchantId + '">Profile</a>' +
                                actionActivation +
                                '</div>';

                            row = [
                                index + 1,
                                "<a href='" + window.pageContext + "merchant/store_list?merchantId=" + data.merchantId+ "&userId="+data.user.userId+"'>" + businessTitle + "</a>",
                                name,
                                email,
                                mobile,
                                status,
                                action
                            ];

                            row = $.extend({}, row);
                            tableData.push(row);
                        });

                        var response = {};
                        response.data = tableData;
                        response.recordsTotal = responseRows;
                        response.recordsFiltered = responseRows;
                        return response;

                        //console.log(merchants);
                        //that.generateFootable(merchants);

                    } else {
                        Main.popDialog("Error", data.message, null, 'error');
                    }
                };

                var params = {
                    "page": {
                        "pageNumber": 1,
                        "pageSize": 10
                    }
                };

                dataFilter.columns = [
                    {"sortable": false},
                    {"name": "businessTitle"},
                    {"name": "user#firstName"},
                    {"name": "user#emailAddress"},
                    {"name": "user#mobileNumber"},
                    {"name": "user#userStatus"},
                    {"sortable": false}
                ];

                dataFilter.requestType = "POST";
                dataFilter.url = pageContext + "organizer/get_merchant_list";
                dataFilter.params = params;
                that.ui.superTable = Main.createDataTable(that.ui.superTable, dataFilter);
            },

            changeMerchantStatus: function (merchantId, status) {
                var that = this;
                var url = pageContext + "merchant/change_merchant_status";
                var callback = function (status, data) {
                    if (data.success) {
                        that.ui.superTable.ajax.reload();
                        /*var btn = function () {
                         window.location = pageContext + 'merchant/merchant_list';
                         };
                         btn.text = "Close";
                         var button = [btn];
                         Main.popDialog('Success', data.message, button, 'success', true);*/
                    } else {
                        Main.popDialog('Error', data.message, null, 'error');
                    }
                };
                callback.requestType = "PUT";
                var data = {
                    "merchant": {
                        "merchantId": merchantId,
                        "user": {
                            "userStatus": status
                        }
                    }
                };
                Main.request(url, data, callback, {});
            }
        }
        ;

    return {
        init: function () {
            listMerchant.init();
        }
    }
})
();