/**
 * Created by Pratik on 6/2/2016.
 */

var addEditDriverModule = (function () {
    "use strict";

    var driverId = "";
    var storeId = "";
    var merchantId = "";
    var userId = "";
    var associatedStoreId = "";
    var checkedInputIds = [];
    var isEdit = false;
    var addDriver = {
        ui: {
            addDriverForm: $("#addDriverForm"),
            firstName: $("#firstName"),
            imgDriver: $("#imgDriver"),
            lastName: $("#lastName"),
            emailAddress: $("#emailAddress"),
            mobileNumber: $("#mobileNumber"),
            vehicleType: $("#vehicleType"),
            vehicleNo: $("#vehicleNo"),
            licenseNo: $("#licenseNo"),
            experienceLevel: $("input[name='exp']"),
            pageTitle: $('.content-title'),
            driverImageWrapper: $('#driverImageContainer'),
            fileInputWrapper: $('.fileinput'),
            storeAssociationSelect: $('#storeAssociation'),
            storeAssociationWrapper: $('#storeAssociationWrapper'),
            editBtn: $('#editBtn'),
            formBtns: $('#formBtns'),

            addServingAreaBtn: $('#addServingAreaBtn'),
            servingAreaModal: $('.serving-area-modal'),
            servingAreaWrapper: $('#servingAreaWrapper'),
            selectAllGod: $('input[name="selectAllGod"]'),
            selectAllBox: $('input[name="selectAllBox"]'),
            confirmServingAreaBtn: $('#confirmServingAreaBtn')
        },
        init: function () {
            driverId = Main.getNamedParameter('driverId');
            storeId = Main.getNamedParameter('storeId');
            merchantId = Main.getNamedParameter('merchantId');
            this.event();
            this.validation();
            this.ui.editBtn.hide();

            this.getAvailableAreas();

            userId = Main.getFromLocalStorage('userId');
            //console.log(driverId);
            if (typeof (driverId) !== "undefined") {
                if (driverId !== null) {
                    isEdit = true;
                    this.ui.editBtn.show();
                    this.ui.pageTitle.text('Edit Driver Profile');
                    this.ui.addDriverForm.find('button[type="submit"]').text('Save Changes');
                    this.getDriverDetails();

                } else {
                    this.getStoresByMerchant(userId);
                }
            } else {
                this.getStoresByMerchant(userId);
            }


        },
        event: function () {
            var that = this;
            this.ui.editBtn.click(function () {
                var formtype = $(this).attr('data-formtype');
                if (formtype == "view") {
                    $(this).attr('data-formtype', 'edit');
                    that.pageMode('edit');
                } else if (formtype == "edit") {
                    $(this).attr('data-formtype', 'view');
                    that.pageMode('view');
                }
            });

            this.ui.addServingAreaBtn.on('click', function (e) {

            });

            this.ui.servingAreaModal.on('shown.bs.modal', function () {
                $('.grid').masonry({
                    // options
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item',
                    percentPosition: true
                });

            }).on('hide.bs.modal', function () {
                $('.grid').masonry('destroy');

                $(this).attr('data-storeid', "");

            }).on('show.bs.modal', function (e) {
                //that.getAvailableAreas();
            });

            this.ui.selectAllGod.change(function () {
                if ($(this).is(':checked')) {
                    that.ui.servingAreaWrapper.find('input[type="checkbox"]').prop('checked', true);
                } else {
                    that.ui.servingAreaWrapper.find('input[type="checkbox"]').prop('checked', false);
                }

            });


            this.ui.servingAreaWrapper.on('change', that.ui.selectAllBox, function (e) {
                var target = e.target;
                if (target.checked) {
                    $("div[data-parentid='" + target.id + "'] input[type='checkbox']").prop('checked', true);
                } else {
                    $("div[data-parentid='" + target.id + "'] input[type='checkbox']").prop('checked', false);
                }

            });

            this.ui.confirmServingAreaBtn.on('click', function () {
                var checkedInputs = that.ui.servingAreaWrapper.find('input[name="subAreas"]:checked');
                $.each(checkedInputs, function (index, input) {
                    checkedInputIds.push(input.id);
                });


                if (isEdit) {
                    that.saveServingAreas(checkedInputIds);
                } else {
                    that.ui.servingAreaModal.modal('hide');
                }

            });

            this.ui.storeAssociationSelect.on('change', function () {
                storeId = $(this).val();
            });


        },

        pageMode: function (mode) {
            var that = this;
            if (mode == "edit") {
                //readOnly = false;
                that.ui.editBtn.text('Cancel');
                that.ui.formBtns.removeClass('hide');
                that.ui.fileInputWrapper.find('.btn-file, .fileinput-exists.btn').removeClass('hide');
                that.ui.addDriverForm.find('input, select, textarea').removeAttr('disabled');
                that.ui.addDriverForm.find('.select-picker').selectpicker('refresh');
            } else if (mode == "view") {
                that.ui.editBtn.text('Edit');
                that.ui.formBtns.addClass('hide');
                setTimeout(function () {
                    that.ui.fileInputWrapper.find('.btn-file, .fileinput-exists.btn').addClass('hide');
                }, 200);
                that.ui.addDriverForm.find('input, select, textarea').attr('disabled', 'disabled');

            }
        },

        getDriverDetails: function () {
            var that = this;
            var url = pageContext + "driver/get_driver_detail?driverId=" + driverId;
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var driverDetails = data.params.driverDetail;

                    that.ui.vehicleType.val(driverDetails.vehicleType).focus().blur();
                    that.ui.vehicleNo.val(driverDetails.vehicleNumber).focus().blur();
                    that.ui.licenseNo.val(driverDetails.licenseNumber).focus().blur();
                    $("input[name='exp'][value='" + driverDetails.experienceLevel + "']").prop('checked', true);
                    that.ui.mobileNumber.val(driverDetails.user.mobileNumber).focus().blur();
                    that.ui.emailAddress.val(driverDetails.user.emailAddress).focus().blur();
                    that.ui.firstName.val(driverDetails.user.firstName).focus().blur();
                    that.ui.lastName.val(driverDetails.user.lastName).focus().blur();
                    that.ui.emailAddress.val();


                    if (!$.isEmptyObject(driverDetails.store)) {
                        associatedStoreId = driverDetails.store.storeId;
                        storeId = associatedStoreId;
                    } else {
                        that.ui.storeAssociationWrapper.remove();
                    }

                    try {
                        userId = driverDetails.store.storeBrand.merchant.user.userId;
                        that.getStoresByMerchant(userId);
                    } catch (e) {
                        userId = "";
                        console.warn('User id not found');
                    }


                    var imgSrc = driverDetails.user.profileImage;
                    if (typeof(imgSrc) == "undefined") {
                        imgSrc = 'http://placehold.it/175x175&text=DriverImage';
                    }

                    that.ui.fileInputWrapper.removeClass('fileinput-new').addClass('fileinput-exists');
                    that.ui.fileInputWrapper.find('.btn-file, .fileinput-exists.btn').removeClass('hide');
                    that.ui.driverImageWrapper.html('<img src="' + imgSrc + '" style="max-height: 140px;">');
                    $('.select-picker').selectpicker('refresh');

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        validation: function () {
            var that = this;
            this.ui.addDriverForm.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    firstName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    emailAddress: {
                        required: true
                    },
                    mobileNumber: {
                        required: true,
                        number: true,
                        minlength: 9,
                        maxlength: 10
                    },
                    vehicleType: {
                        required: true
                    },
                    vehicleNo: {
                        required: true
                    },
                    licenseNo: {
                        required: true
                    },
                    exp: {
                        required: true
                    }
                },
                submitHandler: function () {
                    console.log(checkedInputIds.length);
                    console.log(isEdit);
                    if (checkedInputIds.length === 0 && !isEdit) {
                        Main.popDialog('Error', "Please select the serving areas.", null, 'error');
                        return;
                    }
                    var driverAreas = [];
                    $.each(checkedInputIds, function (index, data) {
                        driverAreas.push({area: {areaId: data}});
                    });

                    var callback = function (status, data) {
                        console.log(data);
                        if (data.success) {
                            var button1 = function () {
                                if (data.params.driverId) {
                                    window.location = Main.modifyURL('merchant/driver_view?driverId=' + data.params.driverId);
                                }
                                else {
                                    window.location = Main.modifyURL('merchant/driver_view?driverId=' + driverId);
                                }
                                //window.location.reload();
                            };
                            button1.text = "Close";
                            var button = [button1];
                            Main.popDialog("", data.message, button, 'success', true);
                        } else {
                            //form.find('button[type="submit"]').removeAttr('disabled');
                            Main.popDialog('Error', data.message, null, 'error');
                        }
                    };


                    var data = {
                        "driver": {
                            "vehicleType": that.ui.vehicleType.val(),
                            "vehicleNumber": that.ui.vehicleNo.val(),
                            "licenseNumber": that.ui.licenseNo.val(),
                            "experienceLevel": $("input[name='exp']:checked").val(),
                            "user": {
                                "username": that.ui.mobileNumber.val(),
                                "firstName": that.ui.firstName.val(),
                                "lastName": that.ui.lastName.val(),
                                "emailAddress": that.ui.emailAddress.val(),
                                "mobileNumber": that.ui.mobileNumber.val(),
                                "profileImage": $('.fileinput-preview.fileinput-exists.thumbnail img').attr('src')
                            },
                            "store": {
                                storeId: (that.ui.storeAssociationSelect.val() !== "null") ? that.ui.storeAssociationSelect.val() : null
                            }
                            //"driversArea": driverAreas
                        }
                    };

                    if (!isEdit) {
                        data.driver.driversArea = driverAreas;
                    }

                    if (storeId === null) {
                        var storeAssoId = that.ui.storeAssociationSelect.val();
                        if (storeAssoId === null || storeAssoId === "null") {
                            data.driver.store = null;
                        } else {
                            data.driver.store.storeId = storeAssoId;
                        }

                    }

                    if (typeof(data.driver.user.profileImage) === 'undefined') {
                        Main.popDialog('Error', "Please select driver image.", null, 'error');
                        return;
                    }

                    if (isEdit) {
                        data.driver.driverId = driverId;
                        Main.request(pageContext + 'driver/update_driver', data, callback, {id: driverId});
                    } else {
                        Main.request(pageContext + 'merchant/save_driver', data, callback, {});
                    }

                }
            });
        },

        getStoresByMerchant: function (userId) {
            var that = this;
            var url = pageContext + "smanager/get_store_allowing_driver";
            var callback = function (status, data) {
                //console.log(data);
                if (data.success) {
                    var stores = "";
                    if (Main.getFromLocalStorage('userRole') === "ROLE_ADMIN") {
                        stores += '<option value="null">'+appName+'</option>';
                    }
                    $.each(data.params.stores, function (index, storeDetail) {
                        stores += "<option value='" + storeDetail.storeId + "'>" + storeDetail.storeBrand.brandName + " - " + storeDetail.givenLocation1 + "</option>";
                        //console.log(storeDetail.storeId);

                    });


                    //console.log(associatedStoreId);

                    if (stores !== "") {
                        that.ui.storeAssociationSelect.append(stores).selectpicker('refresh');
                        that.ui.storeAssociationSelect.val(associatedStoreId).selectpicker('refresh');
                    }

                    setTimeout(function () {
                        if (isEdit) {
                            that.pageMode('edit');
                        }
                    }, 200);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {userId: userId});
        },
        getAvailableAreas: function () {
            var that = this;
            var url = pageContext + "smanager/get_active_area_list";
            var callback = function (status, data) {
                if (data.success) {
                    // console.log(data);
                    var availableAreaTempl = "";

                    $.each(data.params.areas, function (index, mainArea) {
                        //console.log(mainArea);
                        if (typeof(mainArea.areaName) != "undefined") {
                            if (mainArea.child && mainArea.child.length) {
                                availableAreaTempl += '<div class="grid-item"> ' +
                                    '<div class="s-area-box box-me">' +
                                    '<div class="s-area-box-header pt5 ph10"> ' +
                                    '<div class="pull-right">' +
                                    '<input id="' + mainArea.areaId + '" type="checkbox" name="selectAllBox" class="with-font form-control">' +
                                    '<label for="' + mainArea.areaId + '" class="control-label fontnormal">Select All</label>' +
                                    '</div>' +
                                    '<span class="fontbold font16">' + mainArea.areaName + '</span>' +
                                    '</div>';

                                $.each(mainArea.child, function (index, subArea) {
                                    availableAreaTempl += '<div class="pt5 ph10" data-parentid="' + mainArea.areaId + '"> ' +
                                        '<input id="' + subArea.areaId + '" type="checkbox" name="subAreas" class="with-font form-control" value="' + subArea.areaId + '"/> ' +
                                        '<label for="' + subArea.areaId + '" class="control-label fontnormal">' + subArea.areaName + '</label> </div> ';
                                });
                                availableAreaTempl += '</div></div>';
                            }
                        }
                    });

                    that.ui.servingAreaWrapper.html(availableAreaTempl);

                    if (isEdit) {
                        that.getCheckedAreas();
                    }


                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        getCheckedAreas: function () {
            var that = this;
            var url = pageContext + "driver/get_serving_area";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var servingAreas = data.params.servingArea;
                    $.each(servingAreas, function (index, servingArea) {
                        if (typeof(servingArea.child) != "undefined") {
                            if (servingArea.child && servingArea.child.length) {

                                //console.log(checkedInputIds);
                                $.each(servingArea.child, function (index, servingChild) {
                                    //checkedInputIds.push(servingChild.areaId);
                                    if (servingChild.selected) {
                                        //console.log(servingChild.areaName);
                                        $('input[id="' + servingChild.areaId + '"]').prop('checked', true);

                                    }

                                });
                                //console.log(checkedInputIds);
                            }

                        }
                    });


                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {id: driverId});
        },

        saveServingAreas: function (servingAreas) {
            var that = this;
            var url = pageContext + "driver/update_driver_serving_area";
            var callback = function (status, data) {
                if (data.success) {
                    that.ui.servingAreaModal.modal('hide');

                    /*var button1 = function () {
                     window.location = Main.modifyURL(document.URL);
                     };
                     button1.text = "Close";
                     var button = [button1];
                     Main.popDialog("", data.message, button, 'success', true);*/
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = [];

            $.each(servingAreas, function (index, areaId) {
                data.push({
                    "areaId": areaId
                });
            });

            Main.request(url, data, callback, {id: driverId});
        }
    };

    return {
        init: function () {
            addDriver.init();
        }
    };
})();

var listDriverModule = (function () {

    var storeId = "";
    var listDriver = {
        ui: {
            superTable: $('#listDriversTable'),
            addDriverBtn: $('#addDriverBtn'),
            changeStatusLink: $('.trigger_status')
        },

        init: function () {
            storeId = Main.getNamedParameter('storeId');

            if (storeId !== "") {
                var newAddDriverHref = "";
                if (storeId !== null) {
                    newAddDriverHref = this.ui.addDriverBtn.attr('href') + "?&storeId=" + storeId;
                    this.ui.addDriverBtn.attr('href', newAddDriverHref);
                } else {
                    if (Main.getFromLocalStorage('userRole') == "ROLE_MERCHANT") {
                        newAddDriverHref = this.ui.addDriverBtn.attr('href') + "?&merchantId=" + Main.getFromLocalStorage('merchantId');
                        this.ui.addDriverBtn.attr('href', newAddDriverHref);
                    }
                }
            }

            this.events();
            this.getAvailableDrivers();
        },

        events: function () {
            var that = this;
            this.ui.superTable.on('click', this.ui.changeStatusLink.selector, function (e) {
                var alink = $(e.target);
                var id = alink.attr('data-id');
                var status = alink.attr('data-status');
                if (typeof(id) != "undefined") {

                    var btnCancel = function () {
                        $('#popDialog').modal('hide');
                        return;
                    };
                    btnCancel.text = "Cancel";

                    var btnConfirm = function () {
                        that.changeDriverStatus(id, status);
                        $('#popDialog').modal('hide');
                        return;
                    };
                    btnConfirm.text = "Yes";

                    var button = [btnConfirm, btnCancel];

                    Main.popDialog("Confirm", "Are you sure you want to change the status?", button);


                }

            });
        },

        changeDriverStatus: function (driverId, status) {
            var that = this;
            var url = pageContext + "smanager/change_driver_status";
            var callback = function (status, data) {
                if (data.success) {
                    that.ui.superTable.ajax.reload();
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = {
                "driver": {
                    "driverId": driverId,
                    "user": {
                        "userStatus": status
                    }
                }
            };
            Main.request(url, data, callback, {});
        },

        getAvailableDrivers: function () {
            var that = this;
            var dataFilter = function (data, type) {
                if (data.success) {
                    var responseRows = data.params.driverList.numberOfRows;
                    var drivers = data.params.driverList.data;
                    var tableData = [];
                    $.each(drivers, function (index, data) {
                        var driverName = "",
                            driverFirstName = "",
                            driverLastName = "",
                            driverContact = "",
                            driverEmail = "",
                            driverType = "",
                            driverStatus = "",
                            deactivate = "",
                            action = "",
                            row = [];

                        if (!$.isEmptyObject(data.store)) {
                            if (!$.isEmptyObject(data.store.storeBrand)) {
                                driverType = Main.fixUndefinedVariable(data.store.storeBrand.brandName);
                            }
                        } else {
                            driverType = appName;
                        }


                        if (data.user) {
                            driverFirstName = data.user.firstName || "";
                            driverLastName = data.user.lastName || "";
                            driverName = '<a href="' + pageContext + 'merchant/driver_view?driverId=' + data.driverId + '">' + driverFirstName + ' ' + driverLastName + '</a>';
                            driverContact = data.user.mobileNumber || "-";
                            driverEmail = data.user.emailAddress || "";
                            driverStatus = data.user.userStatus || "";
                            if (driverStatus) {
                                driverStatus = Main.ucfirst(driverStatus);
                            }
                        }

                        deactivate = '<a class="trigger_status" href="#" data-id="' + data.driverId + '" data-status="INACTIVE">Deactivate</a>';

                        if (data.user.userStatus !== "ACTIVE") {
                            deactivate = '<a class="trigger_status" href="#" data-id="' + data.driverId + '" data-status="ACTIVE">Activate</a>';
                        }

                        if (data.user.verifiedStatus === false) {
                            deactivate = '-';
                        }

                        action = '<div class="action_links">' +
                            deactivate +
                                /*' | <a href="' + pageContext + 'merchant/driver_form?driverId=' + data.driverId + '">Profile</a>' +*/
                            '</div>';

                        row = [index + 1, driverName, driverContact, driverEmail, driverType, driverStatus, action];
                        row = $.extend({}, row);
                        tableData.push(row);
                    });
                    var response = {};
                    response.data = tableData;
                    response.recordsTotal = responseRows;
                    response.recordsFiltered = responseRows;
                    return response;
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            var params = {
                "page": {
                    "pageNumber": 1,
                    "pageSize": 10
                }, "store": {
                    "storeId": storeId
                }, "merchant": {
                    "merchantId": Main.getFromLocalStorage('merchantId')
                }
            };
            dataFilter.columns = [
                {"sortable": false},
                {"name": "user#firstName"},
                {"name": "user#contact"},
                {"name": "user#emailAddress"},
                {"name": "driverType"},
                {"name": "user#userStatus"},
                {"sortable": false}
            ];
            dataFilter.requestType = "POST";
            dataFilter.url = pageContext + "driver/get_driver_list";
            dataFilter.params = params;
            that.ui.superTable = Main.createDataTable(that.ui.superTable, dataFilter);
        }
    };

    return {
        init: function () {
            listDriver.init();
        }
    };
})();


var viewDriverModule = (function () {
    var driverId = "";
    var viewDriver = {
        ui: {
            driverImage: $('.driver-image'),
            driverExperience: $('.exp-level'),
            driverName: $('.driver-name'),
            driverRating: $('.rating'),
            associatedStore: $('.store-name'),
            driverEmail: $('.driver-email'),
            driverPhone: $('.driver-phone'),
            driverLocation: $('.driver-location'),
            licenseNumber: $('.license-number'),
            vehicleNumber: $('.vehicle-number'),
            vehicleType: $('.vehicle-type'),
            blackListedWrapper: $('#blacklistedStores'),
            addBlackListedBtn: $('#addBlacklistedBtn'),
            servingAreaModal: $('.serving-area-modal'),
            servingAreaWrapper: $('#servingAreaWrapper'),
            selectAllGod: $('input[name="selectAllGod"]'),
            selectAllBox: $('input[name="selectAllBox"]'),
            saveChangesBtn: $('#saveChangesBtn'),
            editProfileLink: $('#editProfileLink')
        },

        init: function () {
            driverId = Main.globalReplace(Main.getNamedParameter('driverId'), '#', '');
            this.events();
            this.getAvailableAreas();
            this.getDriverDetails();

            if (driverId === null) {
                var button1 = function () {
                    window.location = Main.modifyURL(pageContext);
                };
                button1.text = "Close";
                var button = [button1];
                Main.popDialog('Error', 'Invalid driver Id', button, null);
                return;
            }
        },

        events: function () {
            var that = this;
            this.ui.servingAreaModal.on('shown.bs.modal', function () {
                $('.grid').masonry({
                    // options
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item',
                    percentPosition: true
                });

            }).on('hide.bs.modal', function () {
                $('.grid').masonry('destroy');

                $(this).attr('data-storeid', "");

            }).on('show.bs.modal', function (e) {
                //that.getAvailableAreas();
            });

            this.ui.selectAllGod.change(function () {
                if ($(this).is(':checked')) {
                    that.ui.servingAreaWrapper.find('input[type="checkbox"]').prop('checked', true);
                } else {
                    that.ui.servingAreaWrapper.find('input[type="checkbox"]').prop('checked', false);
                }

            });


            this.ui.servingAreaWrapper.on('change', that.ui.selectAllBox, function (e) {
                var target = e.target;
                if (target.checked) {
                    $("div[data-parentid='" + target.id + "'] input[type='checkbox']").prop('checked', true);
                } else {
                    $("div[data-parentid='" + target.id + "'] input[type='checkbox']").prop('checked', false);
                }

            });


            this.ui.saveChangesBtn.on('click', function () {
                // alert('here');

                var checkedInputs = that.ui.servingAreaWrapper.find('input[name="subAreas"]:checked');
                var checkedInputIds = [];
                if (checkedInputs.length > 0) {
                    $.each(checkedInputs, function (index, input) {
                        checkedInputIds.push(input.id);
                    });
                }
                that.saveServingAreas(checkedInputIds);
                that.ui.servingAreaModal.modal('hide');

            });

            this.ui.editProfileLink.on('click', function () {
                window.location.href = pageContext + 'merchant/driver_form?driverId=' + driverId;
            });
        },

        saveServingAreas: function (servingAreas) {
            var that = this;
            var url = pageContext + "driver/update_driver_serving_area";
            var callback = function (status, data) {
                if (data.success) {
                    that.ui.servingAreaModal.modal('hide');
                    var button1 = function () {
                        window.location = Main.modifyURL(document.URL);
                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = [];

            $.each(servingAreas, function (index, areaId) {
                data.push({
                    "areaId": areaId
                });
            });

            Main.request(url, data, callback, {id: driverId});
        },

        getDriverDetails: function () {
            var that = this;
            var url = pageContext + "driver/get_driver_detail?driverId=" + driverId;
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var driverDetails = data.params.driverDetail;

                    that.ui.driverName.text(driverDetails.user.firstName + ' ' + driverDetails.user.lastName);
                    that.ui.licenseNumber.text(driverDetails.licenseNumber);
                    that.ui.vehicleNumber.text(driverDetails.vehicleNumber);
                    that.ui.vehicleType.text(driverDetails.vehicleType);
                    that.ui.driverExperience.text(driverDetails.experienceLevel);
                    that.ui.driverEmail.text(driverDetails.user.emailAddress);
                    that.ui.driverPhone.text(driverDetails.user.mobileNumber);
                    that.ui.driverExperience.text(driverDetails.experienceLevel);
                    that.ui.driverLocation.text(Main.fixUndefinedVariable(driverDetails.streetAddress));

                    var associatedStore = "";
                    if (typeof(driverDetails.store) == "undefined") {
                        associatedStore = Main.globalReplace(Main.ucfirst(driverDetails.driverType), '_', ' ');
                    } else {
                        associatedStore = driverDetails.store.storeBrand.brandName + " - " + driverDetails.store.givenLocation1;
                    }

                    that.ui.associatedStore.text(associatedStore);

                    var imgSrc = driverDetails.user.profileImage;
                    if (typeof(imgSrc) == "undefined") {
                        imgSrc = 'http://placehold.it/175x175&text=DriverImage';
                    }

                    that.ui.driverImage.attr('src', imgSrc);

                    //console.log(driverDetails.storesNotDriver.length);

                    if (driverDetails.storesNotDriver.length > 0) {
                        that.getBlackListedBox(driverDetails.storesNotDriver);
                    }

                    that.ui.driverRating.html(that.makeRatingStars(driverDetails.rating));


                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        getBlackListedBox: function (blackListedStores) {
            var that = this;
            var blackListedUI = "";
            $.each(blackListedStores, function (index, storeDetails) {
                blackListedUI += '<div class="col-md-3 mb25"> ' +
                    '<div class="bl-store-box box-me p15"> ' +
                    '<p class="bl-store-name fontbold">' + storeDetails.store.storeBrand.brandName + '</p> ' +
                    '<p class="bl-store-address">' + storeDetails.store.givenLocation1 + ' ' + storeDetails.store.givenLocation2 + '</p> ' +
                    '<p class="bl-store-contact">' + storeDetails.store.contactNo + '</p> ' +
                    '</div> ' +
                    '</div>';
            });

            that.ui.blackListedWrapper.html(blackListedUI);
        },

        getAvailableAreas: function () {
            var that = this;
            var url = pageContext + "smanager/get_active_area_list";
            var callback = function (status, data) {
                if (data.success) {
                    // console.log(data);
                    var availableAreaTempl = "";

                    $.each(data.params.areas, function (index, mainArea) {
                        //console.log(mainArea);
                        if (typeof(mainArea.areaName) != "undefined") {
                            if (mainArea.child && mainArea.child.length) {
                                availableAreaTempl += '<div class="grid-item"> ' +
                                    '<div class="s-area-box box-me">' +
                                    '<div class="s-area-box-header pt5 ph10"> ' +
                                    '<div class="pull-right">' +
                                    '<input id="' + mainArea.areaId + '" type="checkbox" name="selectAllBox" class="with-font form-control">' +
                                    '<label for="' + mainArea.areaId + '" class="control-label fontnormal">Select All</label>' +
                                    '</div>' +
                                    '<span class="fontbold font16">' + mainArea.areaName + '</span>' +
                                    '</div>';

                                $.each(mainArea.child, function (index, subArea) {
                                    availableAreaTempl += '<div class="pt5 ph10" data-parentid="' + mainArea.areaId + '"> ' +
                                        '<input id="' + subArea.areaId + '" type="checkbox" name="subAreas" class="with-font form-control" value="' + subArea.areaId + '"/> ' +
                                        '<label for="' + subArea.areaId + '" class="control-label fontnormal">' + subArea.areaName + '</label> </div> ';
                                });
                                availableAreaTempl += '</div></div>';
                            }
                        }
                    });

                    that.ui.servingAreaWrapper.html(availableAreaTempl);

                    that.getCheckedAreas();


                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        getCheckedAreas: function () {
            var that = this;
            var url = pageContext + "driver/get_serving_area";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var servingAreas = data.params.servingArea;
                    $.each(servingAreas, function (index, servingArea) {
                        if (typeof(servingArea.child) != "undefined") {
                            if (servingArea.child && servingArea.child.length) {
                                $.each(servingArea.child, function (index, servingChild) {

                                    if (servingChild.selected) {
                                        //console.log(servingChild.areaName);
                                        $('input[id="' + servingChild.areaId + '"]').prop('checked', true);
                                    }

                                });
                            }

                        }
                    });


                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {id: driverId});
        },

        makeRatingStars: function (ratingNumber) {
            var ratingUI = "";
            var ratingCounter = 0;
            for (ratingCounter; ratingCounter < Number(ratingNumber); ratingCounter++) {
                ratingUI += '<i class="fa fa-star c-t-gold"></i>';
            }
            var remainingRating = 5 - ratingNumber;
            for (var j = 0; j < Number(remainingRating); j++) {
                ratingUI += '<i class="fa fa-star-o"></i>';
            }

            return ratingUI;
        }


    };

    return {
        init: function () {
            viewDriver.init();
        }
    };
})();

var superTable;
var driverPerformanceModule = (function () {
    var storeId = "";

    var driverPerformance = {
        ui: {
            fromDatePicker: $('#fromDatePicker'),
            toDatePicker: $('#toDatePicker'),
            superTable: $('#listDriversTable'),
            driverPicker: $('.select-picker-driver'),
            goBtn: $('#goBtn')
        },
        init: function () {
            this.events();
            storeId = Main.getNamedParameter('storeId');
            this.getDrivers();

            this.ui.driverPicker.selectpicker({
                liveSearch: true
            });
            this.ui.fromDatePicker.datetimepicker({
                format: 'YYYY-MM-DD'
            });
            this.ui.fromDatePicker.find('input').val(moment().subtract(1, 'week').format('YYYY-MM-DD'));
            this.ui.toDatePicker.datetimepicker({
                useCurrent: false,
                format: 'YYYY-MM-DD',
                maxDate: 'now'

            });
            this.ui.toDatePicker.find('input').val(moment().format('YYYY-MM-DD'));

            superTable = this.ui.superTable.dataTable({
                retrieve: true,
                columns: [
                    {"sortable": false},
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": true},
                    /*{"sortable": true},*/
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": true}
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        text: 'Export CSV',
                        extension: '.csv',
                        className: 'btn btn-success',
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }
                ]
            });

        },

        events: function () {
            var that = this;
            this.ui.fromDatePicker.on("dp.change", function (e) {
                that.ui.toDatePicker.data("DateTimePicker").minDate(e.date);
                //that.getDriverPerformance(that.ui.driverPicker.val(), that.ui.fromDatePicker.find('input').val(), that.ui.toDatePicker.find('input').val());
            });
            this.ui.toDatePicker.on("dp.change", function (e) {
                that.ui.fromDatePicker.data("DateTimePicker").maxDate(e.date);
                //that.getDriverPerformance(that.ui.driverPicker.val(), that.ui.fromDatePicker.find('input').val(), that.ui.toDatePicker.find('input').val());
            });

            this.ui.goBtn.on('click', function () {
                that.getDriverPerformance(that.ui.driverPicker.val(), that.ui.fromDatePicker.find('input').val(), that.ui.toDatePicker.find('input').val());
            });

        },
        getDrivers: function () {
            var that = this;
            var callback = function (status, data) {
                if (data.success) {
                    var drivers = data.params.driverList.data;
                    var driverUI = "";
                    $.each(drivers, function (index, data) {
                        var driverId, firstName = "", lastName = "";
                        if (data.driverId) {
                            driverId = data.driverId;
                        } else {
                            return;
                        }

                        if (!$.isEmptyObject(data.user)) {
                            firstName = data.user.firstName;
                            lastName = data.user.lastName;
                        }

                        driverUI += "<option value='" + driverId + "'>" + firstName + " " + lastName + "</option>";
                    });

                    that.ui.driverPicker.html(driverUI).selectpicker('refresh');
                    //that.ui.driverPicker.selectpicker('selectAll');

                    //that.getDriverPerformance(that.ui.driverPicker.val(), that.ui.fromDatePicker.find('input').val(), that.ui.toDatePicker.find('input').val());
                }
            };


            callback.requestType = "POST";
            var url = pageContext + "driver/get_driver_list";

            var data = {
                "page": {
                    "pageNumber": 1,
                    "pageSize": 100000
                }, "store": {
                    "storeId": storeId
                }, "merchant": {
                    "merchantId": Main.getFromLocalStorage('merchantId')
                }
            };

            Main.request(url, data, callback, {});
        },

        getDriverPerformance: function (driverIds, fromDate, toDate) {
            if (!driverIds) {
                superTable.fnClearTable();
                return;
            }
            var that = this;
            var callback = function (status, data) {
                if (data.success) {
                    var drivers = data.params.driverPerformance;
                    if (!drivers.length) {
                        return;
                    }
                    var rows = [];
                    $.each(drivers, function (index, data) {


                        var row = [
                            (index + 1),
                            moment(data.date).format('Y-MM-DD'),
                            data.user.firstName + " " + data.user.lastName,
                            that.secondsToHours(data.dutyHour),
                            /*that.secondsToHours(data.busyHour),*/
                            (!data.orderAssigned) ? '-' : data.orderAssigned,
                            (!data.orderCompleted) ? '-' : data.orderCompleted,
                            that.meterToKM(data.distanceCovered)
                        ];
                        row = $.extend({}, row);
                        rows.push(row);
                    });
                    superTable.fnClearTable();
                    superTable.fnAddData(rows);
                    superTable.fnAdjustColumnSizing(true);
                    superTable.fnDraw();
                }
            };
            callback.requestType = "POST";
            var data = {
                "driverIds": driverIds,
                "fromDate": fromDate,
                "toDate": toDate
            };
            var url = pageContext + 'smanager/get_driver_performance';
            Main.request(url, data, callback, {});
        },

        secondsToHours: function (seconds) {
            if (!seconds) {
                return '-';
            }
            //console.log(seconds);
            var x = seconds;
            var d = moment.duration(x, 'seconds');
            var hours = Math.floor(d.asHours());
            var mins = Math.floor(d.asMinutes()) - hours * 60;

            return hours + " Hrs, " + mins + " mins";
        },

        meterToKM: function (meters) {
            if (!meters) {
                return "-";
            }

            return (meters / 1000) + "KM";
        }
    };

    return {
        init: function () {
            driverPerformance.init();
        }
    };
})();