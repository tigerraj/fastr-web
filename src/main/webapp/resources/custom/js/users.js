/**
 * Created by Pratik on 5/3/2016.
 */

var addEditUserModule = (function () {
    "use strict";
    var isEdit = false;
    var userId = null;
    var addEditUser = {
        ui: {
            addUserForm: $('#addUserForm'),
            firstName: $("#firstName"),
            lastName: $("#lastName"),
            mobileNumber: $("#mobileNumber"),
            email: $("#email"),
            userRole: $("input[name='userRole']"),
            pageTitle: $('.content-title')
        },

        init: function () {
            this.validation();

            userId = Main.getNamedParameter('userId');
            if (userId != null) {
                this.ui.pageTitle.text('Edit User');
                this.ui.addUserForm.find('button[type="submit"]').text('Save Changes');
                this.ui.addUserForm.find('button[type="reset"]').hide();
                this.ui.userRole.attr('disabled', 'disabled');
                this.getUserDetail(Main.globalReplace(userId, '#', ''));
                isEdit = true;
            }
        },

        events: function () {

        },

        validation: function () {
            var that = this;
            this.ui.addUserForm.validate({
                rules: {
                    firstName: {
                        required: true
                    },
                    lastName: {
                        required: true,
                    },
                    mobileNumber: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    userRole: {
                        required: true,
                    },
                },
                submitHandler: function () {
                    var callback = function (status, data) {

                        if (data.success) {
                            var button1 = function () {
                                window.location = Main.modifyURL(document.URL);
                            };
                            button1.text = "Close";
                            var button = [button1];
                            Main.popDialog("Success", data.message, button, 'success', true);

                        } else {
                            Main.popDialog('Error', data.message, null, 'error');
                        }
                    };

                    var data = {
                        "user": {
                            "firstName": that.ui.firstName.val(),
                            "lastName": that.ui.lastName.val(),
                            "mobileNumber": that.ui.mobileNumber.val(),
                            "emailAddress": that.ui.email.val(),
                            "userRole": $('input[name="userRole"]:checked').val()
                        }
                    };

                    var url = pageContext + 'admin/create_user';
                    var header = {};


                    if (isEdit) {
                        url = pageContext + "csr/update_user";
                        data.user.userId = userId;
                        callback.requestType = "PUT";
                    } else {
                        header = {"username": that.ui.email.val()};
                        callback.requestType = "POST";
                    }

                    Main.request(url, data, callback, header);

                }
            });


        },

        getUserDetail: function (userId) {
            var that = this;
            var url = pageContext + "csr/get_user/" + userId;
            var callback = function (success, data) {
                if (data.success) {
                    var userDetail = data.params.userDetail;
                    that.ui.firstName.val(userDetail.firstName).focus().blur();
                    that.ui.lastName.val(userDetail.lastName).focus().blur();
                    that.ui.mobileNumber.val(userDetail.mobileNumber).focus().blur();
                    that.ui.email.val(userDetail.emailAddress).focus().blur();
                    $('input[name="userRole"][value="' + userDetail.userRole + '"]').prop('checked', true);
                } else {
                    Main.popDialog('Error', data.message, null, null);
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        }
    };

    return {
        init: function () {
            addEditUser.init();
        }
    };

})();

var listUsersModule = (function () {
    "use strict";

    var listUsers = {
        ui: {
            listUsersTable: $('#listUsersTable'),
            changeStatusLink: $('.trigger_status'),
        },

        init: function () {
            this.events();
            this.getAllUsers();
        },

        events: function () {
            var that = this;
            this.ui.listUsersTable.on('click', this.ui.changeStatusLink.selector, function (e) {
                var alink = $(e.target);
                var id = alink.attr('data-id');
                var status = alink.attr('data-status');
                if (typeof(id) != "undefined") {
                    var btnCancel = function () {
                        $('#popDialog').modal('hide');
                        return;
                    };
                    btnCancel.text = "Cancel";

                    var btnConfirm = function () {
                        that.changeUserStatus(id, status);
                        $('#popDialog').modal('hide');
                        return;
                    };
                    btnConfirm.text = "Yes";

                    var button = [btnConfirm, btnCancel];

                    Main.popDialog("Confirm", "Are you sure you want to change the status?", button, null, null);

                }
            });
        },

        getAllUsers: function () {
            var that = this;
            var dataFilter = function (data, type) {
                if (data.success) {
                    var responseRows = data.params.users.numberOfRows;
                    var users = data.params.users.data;
                    var tableData = [];
                    $.each(users, function (index, data) {
                        var row = [];
                        var deactivate = '<a class="trigger_status" href="#" data-id="' + data.userId + '" data-status="INACTIVE">Deactivate</a>';

                        if (data.userStatus != "ACTIVE") {
                            deactivate = '<a class="trigger_status" href="#" data-id="' + data.userId + '" data-status="ACTIVE">Activate</a>';
                        }

                        var action = '<div class="action_links">' +

                            '<a href="' + pageContext + 'organizer/user_form?userId=' + data.userId + '">Profile</a> | ' + deactivate +
                            '</div>';
                        var userRole = data.userRole || "";
                        userRole = Main.ucfirst(Main.globalReplace(userRole, '_', ' '));
                        var userStatus = data.userStatus || "";
                        userStatus = Main.ucfirst(userStatus);

                        row = [index + 1, data.firstName + ' ' + data.lastName, data.emailAddress, userRole, userStatus, action];
                        row = $.extend({}, row);
                        tableData.push(row);
                    });

                    var response = {};
                    response.data = tableData;
                    response.recordsTotal = responseRows;
                    response.recordsFiltered = responseRows;
                    return response;

                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };

            var params = {
                "page": {
                    "pageNumber": 1,
                    "pageSize": 10
                }
            };

            dataFilter.columns = [
                {"sortable": false},
                {"name": "user#fullName", "orderable": false},
                {"name": "emailAddress", "orderable": false},
                {"name": "userRole"},
                {"name": "userStatus"},
                {"sortable": false}
            ];

            dataFilter.requestType = "POST";
            dataFilter.url = pageContext + "admin/get_users";
            dataFilter.params = params;
            that.ui.listUserDT = Main.createDataTable(that.ui.listUsersTable, dataFilter);
        },

        changeUserStatus: function (userId, status) {
            var that = this;
            var url = pageContext + "csr/change_user_status";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(that.ui.listUserDT);
                    that.ui.listUserDT.ajax.reload();
                    /*var btn = function () {
                     //window.location = pageContext + 'organizer/user_list';
                     console.log(that.ui.listUserDT);

                     };
                     btn.text = "Close";
                     var button = [btn];
                     Main.popDialog('Success', data.message, button, 'success', true);*/
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = {
                "user": {
                    "userId": userId,
                    "userStatus": status

                }
            };
            Main.request(url, data, callback, {});
        },


    };

    return {
        init: function () {
            listUsers.init();
        }
    };

})();