/**
 * Created by Pratik on 6/23/2016.
 */

var ratesModule = (function () {
    "use strict"
    var storeId = Main.getNamedParameter('storeId');
    var allRatesWServingArea = {};
    var allRatesWOServingArea = {};
    var allRatesDistanceBasedServingArea = {};
    var minValueOfUnknown = "";
    var isDistancebased = false;
    var rates = {
        ui: {
            addRatePlanWBtn: $('#addRatePlanWBtn'),
            addRatePlanWOBtn: $('#addRatePlanWOBtn'),
            ratePlanWSAWrapper: $('#WServiceArea'),
            ratePlanWOSAWrapper: $('#WOServiceArea'),
            WServiceAreaModal: $('#WServiceAreaModal'),
            WOServiceAreaModal: $('#WOServiceAreaModal'),
            WSAForm: $('#WSAForm'),
            rateNameInput: $('#rateName'),
            maxOrderCountsInput: $('#maxOrderCounts'),
            chargePerMonthInput: $('#chargePerMonth'),
            chargePerOrderInput: $('#chargePerOrder'),
            WSADurationInput: $('#WSADuration'),
            statusInput: $('#WSAstatus'),
            WSAStatusWrapper: $('#WSAStatusWrapper'),


            WOSAForm: $('#WOSAForm'),
            servingRangeFromInput: $('#servingRangeFrom'),
            servingRangeToInput: $('#servingRangeTo'),
            baseFareInput: $('#baseFare'),
            chargePerKMInput: $('#chargePerKM'),
            WOSAstatusInput: $('#WOSAstatus'),
            WOSAstatusWrapper: $('#WOSAstatusWrapper'),


            distanceBasedRPModal: $('#distanceBasedRPModal'),
            distanceBasedRPForm: $('#distanceBasedRPForm'),
            distanceBasedRPrateName: $('#distanceBasedRPrateName'),
            distanceBasedRPservingRangeFrom: $('#distanceBasedRPservingRangeFrom'),
            distanceBasedRPservingRangeTo: $('#distanceBasedRPservingRangeTo'),
            distanceBasedRPbaseFare: $('#distanceBasedRPbaseFare'),
            distanceBasedRPchargePerKM: $('#distanceBasedRPchargePerKM'),
            distanceBasedRPstatus: $('#distanceBasedRPstatus'),
            distanceBasedRPstatusWrapper: $('#distanceBasedRPstatusWrapper'),

            knownAreaServingRatePlanWrapper: $('#knownAreaServingRatePlanWrapper'),
            distanceBasedKnownServingAreaRatePlanWrapper: $('#distanceBasedKnownServingAreaRatePlanWrapper'),

            addDBSARPBtn: $('#addDBSARPBtn'),
            DBServiceArea: $('#DBServiceArea'),
        },
        init: function () {
            this.events();
            this.getAllRatesWOServingArea();
            this.WOSAValidation();

            if (Main.getNamedParameter('isDistancebasedRP')) {
                if (Main.getNamedParameter('isDistancebasedRP') == "true") {
                    isDistancebased = true;
                }
            }

            if (isDistancebased) {
                this.DistanceBasedRatePlanValidation();
                this.getAllDistanceBasedServingArea();
                this.ui.distanceBasedKnownServingAreaRatePlanWrapper.removeClass('hidden');
                this.ui.knownAreaServingRatePlanWrapper.addClass('hidden');
            } else {
                this.getAllRatesWServingArea();
                this.WSAValidation();
                this.ui.distanceBasedKnownServingAreaRatePlanWrapper.addClass('hidden');
                this.ui.knownAreaServingRatePlanWrapper.removeClass('hidden');
            }
        },
        events: function () {
            var that = this;
            this.ui.WServiceAreaModal.on('show.modal.bs', function (e) {
                var btn = $(e.relatedTarget);
                var rateId = btn.data('rateplanid');
                //console.log(rateId);
                that.ui.WSAStatusWrapper.hide();
                if (typeof(rateId) !== "undefined") {
                    $(this).attr('data-rateplanid', rateId);
                    $(this).find('.modal-title').html('Edit Rate Plan');
                    that.ui.WSAForm.find('button[type="submit"]').html('Save Changes');
                    that.WSAsetFormData(rateId);
                    that.ui.WSAStatusWrapper.show();
                }

            }).on('hidden.modal.bs', function (e) {
                $(this).find('.modal-title').html('Add Rate Plan');
                $(this).removeAttr('data-rateplanid');
                that.ui.WSAForm.find('button[type="submit"]').html('Add');
                that.ui.statusInput.selectpicker('val', '');
                that.ui.WSADurationInput.selectpicker('val', '');
                $(this).find('input').val('');
            });

            this.ui.WOServiceAreaModal.on('show.modal.bs', function (e) {
                var btn = $(e.relatedTarget);
                var rateId = btn.data('rateplanid');
                that.ui.WOSAstatusWrapper.hide()
                //console.log(rateId);
                if (typeof(rateId) !== "undefined") {
                    $(this).attr('data-rateplanid', rateId);
                    $(this).find('.modal-title').html('Edit Rate Plan');
                    that.ui.WOSAForm.find('button[type="submit"]').html('Save Changes');
                    that.WOSAsetFormData(rateId);
                    that.ui.WOSAstatusWrapper.show()
                } else {
                    that.ui.servingRangeFromInput.val(minValueOfUnknown);
                }

            }).on('hidden.modal.bs', function (e) {
                $(this).find('.modal-title').html('Add Rate Plan');
                $(this).removeAttr('data-rateplanid');
                that.ui.WOSAForm.find('button[type="submit"]').html('Add');
                that.ui.WOSAstatusInput.selectpicker('val', '');
                $(this).find('input').val('');
                //that.ui.WSADurationInput.selectpicker('val', '');
            });


            this.ui.distanceBasedRPModal.on('show.modal.bs', function (e) {
                var btn = $(e.relatedTarget);
                var rateId = btn.data('rateplanid');
                that.ui.distanceBasedRPstatusWrapper.hide()
                //console.log(rateId);
                if (typeof(rateId) !== "undefined") {
                    $(this).attr('data-rateplanid', rateId);
                    $(this).find('.modal-title').html('Edit Rate Plan');
                    that.ui.distanceBasedRPstatusWrapper.removeClass('hidden');
                    that.ui.distanceBasedRPForm.find('button[type="submit"]').html('Save Changes');
                    that.DistanceBasedRatePlan(rateId);
                    that.ui.distanceBasedRPstatusWrapper.show()
                } else {
                    //that.ui.servingRangeFromInput.val(minValueOfUnknown);
                }

            }).on('hidden.modal.bs', function (e) {
                $(this).find('.modal-title').html('Add Rate Plan');
                $(this).removeAttr('data-rateplanid');
                that.ui.distanceBasedRPForm.find('button[type="submit"]').html('Add');
                that.ui.distanceBasedRPstatus.selectpicker('val', '');
                $(this).find('input').val('');
                //that.ui.WSADurationInput.selectpicker('val', '');
            });

            this.ui.addRatePlanWBtn.on('click', function () {
                that.ui.WServiceAreaModal.modal('show');
            });

            this.ui.addDBSARPBtn.on('click', function () {
                that.ui.distanceBasedRPModal.modal('show');
            });

        },

        WSAsetFormData: function (ratePlanId) {

            var curData = allRatesWServingArea[ratePlanId];
            //console.log(curData);
            this.ui.rateNameInput.val(curData.name);
            this.ui.statusInput.selectpicker('val', curData.status);
            this.ui.maxOrderCountsInput.val(curData.orderCount);
            this.ui.chargePerMonthInput.val(curData.amount);
            this.ui.chargePerOrderInput.val(curData.additionalChargePerOrder);
            this.ui.WSADurationInput.selectpicker('val', curData.duration);


            $('.select-picker').selectpicker('refresh');
        },
        WSAValidation: function () {
            var that = this;
            this.ui.WSAForm.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    maxOrderCounts: {
                        required: true,
                        number: true

                    },
                    chargePerMonth: {
                        required: true,
                        number: true
                    },
                    chargePerOrder: {
                        required: true,
                        number: true
                    },
                    /*WSADuration: {
                     required: true
                     },
                     WSAstatus: {
                     required: true
                     },*/
                    rateName: {
                        required: true
                    },
                },
                submitHandler: function () {
                    that.addEditRatesWServingArea();
                }
            });
        },
        addEditRatesWServingArea: function () {
            var that = this;

            var callback = function (status, data) {
                if (data.success) {
                    that.ui.WServiceAreaModal.modal('hide');
                    var button1 = function () {
                        window.location = Main.modifyURL(document.URL);
                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };


            var data = {
                "name": that.ui.rateNameInput.val(),
                //"status": that.ui.statusInput.val(),
                "driverType": enumAppName,
                "orderCount": that.ui.maxOrderCountsInput.val(),
                "amount": that.ui.chargePerMonthInput.val(),
                "additionalChargePerOrder": that.ui.chargePerOrderInput.val(),
                "duration": "MONTHLY"//that.ui.WSADurationInput.val()
            };

            var ratePlanId = that.ui.WServiceAreaModal.attr('data-rateplanid');

            if (!ratePlanId) {
                callback.requestType = "POST";
                data.status = "ACTIVE";
                var url = pageContext + "organizer/save_rate_plan";
                Main.request(url, data, callback, {storeid: storeId});
            } else {
                callback.requestType = "PUT";
                data.status = that.ui.statusInput.val();
                var url = pageContext + "organizer/update_rate_plan";
                Main.request(url, data, callback, {id: ratePlanId});
            }

        },
        getAllRatesWServingArea: function () {
            var that = this;
            var url = pageContext + "merchant/get_rate_plan";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var allRatePlans = '';
                    var ratePlans = data.params.ratePlans.ratePlans;
                    if (ratePlans.length) {
                        $.each(ratePlans, function (index, data) {
                            allRatesWServingArea[data.ratePlanId] = data;
                            allRatePlans += that.drawRatesWServingArea(data);
                        });
                        that.ui.ratePlanWSAWrapper.html(allRatePlans);
                        that.ui.addRatePlanWBtn.remove();
                    } else {
                        that.ui.ratePlanWSAWrapper.html("No rate plans available.");
                    }

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";

            Main.request(url, {}, callback, {storeid: storeId});

        },
        drawRatesWServingArea: function (ratePlan) {
            var tmpl = '<div class="item-box "> ' +
                '<a href="" class="pull-right font14" data-rateplanid="' + ratePlan.ratePlanId + '" data-toggle="modal" data-target="#WServiceAreaModal">Edit</a> ' +
                '<h4>' + Main.fixUndefinedVariable(ratePlan.name) + '</h4>' +
                '<div class="clearfix"></div> ' +
                '<div class="row"> ' +
                '<div class="col-sm-5 text-center"> ' +
                '<p class="fontbold mb0 order-range">' + Main.fixUndefinedVariable(ratePlan.orderCount) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">number of orders</p> ' +
                '</div> ' +
                '<div class="col-sm-5 col-sm-offset-2 text-center"> ' +
                '<p class="fontbold mb0 charge-amount">' + Main.getFromLocalStorage('currency') + ' ' + Main.fixUndefinedVariable(ratePlan.amount) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">charge/month</p> ' +
                '</div> ' +
                '</div> <hr class="hr-mod"> ' +
                '<div class="row"> ' +
                '<div class="col-sm-5 "> ' +
                '<p class=" text-muted">Per extra order above ' + Main.fixUndefinedVariable(ratePlan.orderCount) + '</p> ' +
                '</div> ' +
                '<div class="col-sm-5 col-sm-offset-2 text-center"> ' +
                '<p class="fontbold mb0 extra-charge-amount">' + Main.getFromLocalStorage('currency') + ' ' + Main.fixUndefinedVariable(ratePlan.additionalChargePerOrder) + '</p> ' +
                '</div> ' +
                '</div> ' +
                '</div>';
            return tmpl;
        },
//********************//
        WOSAsetFormData: function (ratePlanId) {
            //console.log(allRatesWOServingArea);
            var curData = allRatesWOServingArea[ratePlanId];

            this.ui.WOSAstatusInput.selectpicker('val', curData.status);
            this.ui.servingRangeFromInput.val(curData.startDistance);
            this.ui.servingRangeToInput.val(curData.endDistance);
            this.ui.baseFareInput.val(curData.baseCharge);
            this.ui.chargePerKMInput.val(curData.additionalChargePerKM);


        },
        WOSAValidation: function () {
            var that = this;
            this.ui.WOSAForm.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    servingRangeFrom: {
                        required: true,
                        number: true
                    },
                    servingRangeTo: {
                        required: true,
                        number: true,
                        greaterThan: that.ui.servingRangeFromInput.selector
                    },
                    baseFare: {
                        required: true,
                        number: true
                    },
                    chargePerKM: {
                        required: true,
                        number: true
                    },
                    WOSAstatus: {
                        required: true
                    },
                },
                submitHandler: function () {
                    that.addEditRatesWOServingArea();
                }
            });
        },
        addEditRatesWOServingArea: function () {
            var that = this;

            var callback = function (status, data) {
                if (data.success) {
                    that.ui.WOServiceAreaModal.modal('hide');
                    var button1 = function () {
                        //window.location = Main.modifyURL(document.URL);
                        $('#popDialog').modal('hide');

                        that.getAllRatesWOServingArea();
                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };


            var data = {
                "status": "ACTIVE",
                "isKnownArea": false,
                "startDistance": that.ui.servingRangeFromInput.val(),
                "endDistance": that.ui.servingRangeToInput.val(),
                "baseCharge": that.ui.baseFareInput.val(),
                "additionalChargePerKM": that.ui.chargePerKMInput.val()
            };

            var ratePlanId = that.ui.WOServiceAreaModal.attr('data-rateplanid');
            console.log(ratePlanId);
            if (!ratePlanId) {


                callback.requestType = "POST";
                var url = pageContext + "organizer/save_unknown_area_rate_plan";
                Main.request(url, data, callback, {storeid: storeId});


            } else {
                callback.requestType = "PUT";
                data.status = that.ui.WOSAstatusInput.val();
                var url = pageContext + "organizer/update_unknown_area_rate_plan";
                Main.request(url, data, callback, {id: ratePlanId});
            }
        },
        getAllRatesWOServingArea: function () {
            var that = this;
            var url = pageContext + "merchant/get_unknown_area_rate_plan";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var allRatePlans = '';
                    $.each(data.params.ratePlans, function (index, data) {
                        allRatesWOServingArea[data.unknownAreaRatePlanId] = data;
                        allRatePlans += that.drawRatesWOServingArea(data);

                        minValueOfUnknown = Main.fixUndefinedVariable(Number(data.endDistance) + 1);
                    });
                    that.ui.ratePlanWOSAWrapper.html(allRatePlans);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";

            Main.request(url, {}, callback, {storeid: storeId});
        },
        drawRatesWOServingArea: function (ratePlan) {
            var tmpl = '<div class="item-box "> ' +
                '<a href="" class="pull-right font14" data-rateplanid="' + ratePlan.unknownAreaRatePlanId + '" data-toggle="modal" data-target="#WOServiceAreaModal">Edit</a> ' +
                '<div class="clearfix"></div> ' +
                '<div class="row"> ' +
                '<div class="col-sm-4 text-center"> ' +
                '<p class="fontbold mb0 km-range">' + Main.fixUndefinedVariable(ratePlan.startDistance) + '-' + Main.fixUndefinedVariable(ratePlan.endDistance) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">KM Range</p> ' +
                '</div> ' +
                '<div class="col-sm-4 text-center"> ' +
                '<p class="fontbold mb0 fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + Main.fixUndefinedVariable(ratePlan.baseCharge) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">base fare</p> ' +
                '</div> ' +
                '<div class="col-sm-4 text-center"> ' +
                '<p class="fontbold mb0 extra-fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + Main.fixUndefinedVariable(ratePlan.additionalChargePerKM) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">extra charge/KM</p> ' +
                '</div> ' +
                '</div> ' +
                '</div>';
            return tmpl;
        },
//--------------------//
        DistanceBasedRatePlan: function (ratePlanId) {
            console.log(allRatesDistanceBasedServingArea);
            var curData = allRatesDistanceBasedServingArea[ratePlanId];
            this.ui.distanceBasedRPstatus.selectpicker('val', curData.status);
            this.ui.distanceBasedRPservingRangeFrom.val(curData.startDistance);
            this.ui.distanceBasedRPservingRangeTo.val(curData.endDistance);
            this.ui.distanceBasedRPbaseFare.val(curData.baseCharge);
            this.ui.distanceBasedRPchargePerKM.val(curData.additionalChargePerKM);
            this.ui.distanceBasedRPrateName.val(curData.name);

        },
        DistanceBasedRatePlanValidation: function () {
            var that = this;
            this.ui.distanceBasedRPForm.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    rateName: {
                        required: true
                    },
                    servingRangeFrom: {
                        required: true,
                        number: true
                    },
                    servingRangeTo: {
                        required: true,
                        number: true,
                        greaterThan: that.ui.distanceBasedRPservingRangeFrom.selector
                    },
                    baseFare: {
                        required: true,
                        number: true
                    },
                    chargePerKM: {
                        required: true,
                        number: true
                    },
                    WOSAstatus: {
                        required: true
                    },
                },
                submitHandler: function () {
                    that.addEditDistanceBasedRatePlan();
                }
            });
        },
        addEditDistanceBasedRatePlan: function () {
            var that = this;

            var callback = function (status, data) {
                if (data.success) {
                    that.ui.distanceBasedRPModal.modal('hide');
                    var button1 = function () {
                        window.location.reload();
                        //$('#popDialog').modal('hide');

                        //that.getAllRatesWOServingArea(); -- TODO
                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };


            var data = {
                "name": that.ui.distanceBasedRPrateName.val(),
                "status": "ACTIVE",
                "isKnownArea": true,
                "startDistance": that.ui.distanceBasedRPservingRangeFrom.val(),
                "endDistance": that.ui.distanceBasedRPservingRangeTo.val(),
                "baseCharge": that.ui.distanceBasedRPbaseFare.val(),
                "additionalChargePerKM": that.ui.distanceBasedRPchargePerKM.val()
            };

            var ratePlanId = that.ui.distanceBasedRPModal.attr('data-rateplanid');
            console.log(ratePlanId);
            if (!ratePlanId) {
                callback.requestType = "POST";
                var url = pageContext + "organizer/save_unknown_area_rate_plan";
                Main.request(url, data, callback, {storeid: storeId});


            } else {
                callback.requestType = "PUT";
                data.status = that.ui.distanceBasedRPstatus.val();
                var url = pageContext + "organizer/update_unknown_area_rate_plan";
                Main.request(url, data, callback, {id: ratePlanId});
            }
        },

        getAllDistanceBasedServingArea: function () {
            var that = this;
            var url = pageContext + "merchant/get_rate_plan";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var allRatePlans = '';
                    $.each(data.params.ratePlans.unknownAreaRatePlans, function (index, data) {
                        allRatesDistanceBasedServingArea[data.unknownAreaRatePlanId] = data;
                        allRatePlans += that.drawRatesDistanceBasedServingArea(data);

                        minValueOfUnknown = Main.fixUndefinedVariable(Number(data.endDistance) + 1);
                    });
                    that.ui.DBServiceArea.html(allRatePlans);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";

            Main.request(url, {}, callback, {storeid: storeId});
        },
        drawRatesDistanceBasedServingArea: function (ratePlan) {
            var tmpl = '<div class="item-box "> ' +
                '<a href="" class="pull-right font14" data-rateplanid="' + ratePlan.unknownAreaRatePlanId + '" data-toggle="modal" data-target="#distanceBasedRPModal">Edit</a> ' +
                '<div class="clearfix"></div> ' +
                '<div class="row"> ' +
                '<h4 class="pl25">' + Main.fixUndefinedVariable(ratePlan.name) + '</h4>' +
                '<div class="col-sm-4 text-center"> ' +
                '<p class="fontbold mb0 km-range">' + Main.fixUndefinedVariable(ratePlan.startDistance) + '-' + Main.fixUndefinedVariable(ratePlan.endDistance) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">KM Range</p> ' +
                '</div> ' +
                '<div class="col-sm-4 text-center"> ' +
                '<p class="fontbold mb0 fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + Main.fixUndefinedVariable(ratePlan.baseCharge) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">base fare</p> ' +
                '</div> ' +
                '<div class="col-sm-4 text-center"> ' +
                '<p class="fontbold mb0 extra-fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + Main.fixUndefinedVariable(ratePlan.additionalChargePerKM) + '</p> ' +
                '<p class="text-uppercase text-muted mb0">extra charge/KM</p> ' +
                '</div> ' +
                '</div> ' +
                '</div>';
            return tmpl;
        },

    };

    return {
        init: function () {
            rates.init();
        }
    }
})();