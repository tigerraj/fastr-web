/**
 * Created by Pratik on 5/4/2016.
 */

var forgotPWModule = (function(){
    var forgotPW = {
        ui:{
            forgotPWForm: $('#forgotPWForm')
        },
        init:function(){
            this.events();
        },
        events:function() {
            var that = this;

            this.ui.forgotPWForm.validate({
                submitHandler: function (form) {
                    alert("form valid");
                }
            });
        }
    }

    return {
        init: function(){
            forgotPW.init();
        }
    }
})();