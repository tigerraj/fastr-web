var orderMapsModule = (function () {
    "use strict";
    var markers = {};
    var storeMarkers = {};
    var driverMarkers = {};
    var customerMarkers = {};
    var orderRoutes = {};

    return {
        init: function (config) {
            var canvas = config.mapCanvas;
            var mapConfig = {
                zoom: config.zoom,
                center: config.center,
                mapTypeId: config.mapTypeId
            };

            var map = new google.maps.Map(document.getElementById(canvas), mapConfig);

            setTimeout(function () {
                $(document).trigger('maps.ready');
            }, 500);

            return map;
        },

        setMarkers: function (map, inMarkers) { //set markers only
            var that = this;
            //console.log(inMarkers);
            var markerType = that.getMarkerType(inMarkers);
            if (markerType === "Driver") {
                that.removeMarkersbyType(driverMarkers);
                driverMarkers = {};
            }

            var bounds = new google.maps.LatLngBounds();

            //var counter = 0;

            $.each(inMarkers, function (index, data) {
                //console.log(data);
                if (typeof(data) == "undefined") {
                    return;
                }

                if (!data.lat || !data.lng) {
                    return;
                }

                var latlng = new google.maps.LatLng(Number(data.lat), Number(data.lng));
                //console.log(index + " - " + data.type + ' - ' + latlng);

                var icon = {};
                icon.url = data.icon;
                icon.scaledSize = new google.maps.Size(50, 50);

                try {
                    var locationKey = data.storeId || data.driverInfo.driverId;//that.locationToKey(latlng);
                } catch (execp) {
                    var locationKey = "";
                    console.warn(execp);
                }
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: icon,
                    mapData: data
                });

                bounds.extend(latlng);


                var infowindow = new google.maps.InfoWindow({
                    content: data.title
                });

                marker.addListener('mouseover', function () {
                    infowindow.open(map, this);
                });

                marker.addListener('mouseout', function () {
                    infowindow.close();
                });

                marker.addListener('click', function () {
                    that.removeAllMarkerAnimation(markers);
                    this.setAnimation(google.maps.Animation.BOUNCE);
                    var marker = this;
                    console.log(marker);
                    if (marker.mapData) {
                        if (marker.mapData.type && marker.mapData.type === "Driver") {
                            $(document).trigger('maps.marker.clicked', marker.mapData);
                        }
                    }
                });

                markers[locationKey] = marker;

                if (data.type === "Store") {
                    //counter++;

                    storeMarkers[locationKey] = marker;
                    //console.log(index + " = " + counter + ' - ' + Object.keys(storeMarkers).length);
                    //console.log('\n');

                } else if (data.type === "Driver") {
                    driverMarkers[locationKey] = marker;
                }


            });

            map.fitBounds(bounds);

            //console.log(counter);
            //console.log(Object.keys(storeMarkers));
            //console.log(Object.keys(storeMarkers).length);

            $(document).trigger('maps.marker.added', [inMarkers]);
        },

        generateDirections: function (map, markers) {
            var that = this;
            var bounds = new google.maps.LatLngBounds();
            var driver, store, customer;
            var driverName, storeName, customerName;
            var driverlat, driverlng, storelat, storelng, customerlat, customerlng;
            var flightPlanCoordinates = [];
            var origin = null;
            var destination = null;
            var waypoint = null;
            var directionsService = new google.maps.DirectionsService;
            var orderId = that.getRouteOrderId(markers);
            var routeDetails = orderRoutes[orderId];


            if (!$.isEmptyObject(markers.driver)) {
                driver = markers.driver;
                driverlat = driver.lat;
                driverlng = driver.lng;
                driverName = driver.title;
                if (driverlat != "undefined" && driverlng != "undefined") {
                    flightPlanCoordinates.push({
                        lat: driverlat,
                        lng: driverlng
                    });
                    origin = driverlat + "," + driverlng;
                    bounds.extend(new google.maps.LatLng(driverlat, driverlng));
                }

            }

            if (!$.isEmptyObject(markers.store)) {
                store = markers.store;
                storelat = store.lat;
                storelng = store.lng;
                storeName = store.title;
                if (storelat != "undefined" && storelng != "undefined") {
                    flightPlanCoordinates.push({
                        lat: storelat,
                        lng: storelng
                    });
                    waypoint = {
                        location: storelat + "," + storelng,
                        stopover: false,
                    };
                    bounds.extend(new google.maps.LatLng(storelat, storelng));
                }

            }

            if (!$.isEmptyObject(markers.customer)) {
                customer = markers.customer;
                customerlat = customer.lat;
                customerlng = customer.lng;
                customerName = customer.title;
                if (!customerlat && !customerlng) {
                    if (!$.isEmptyObject(customer.area)) {
                        customerlat = customer.area.latitude || null;
                        customerlng = customer.area.longitude || null;
                    }
                }

                if (customerlat != "undefined" && customerlng != "undefined") {
                    flightPlanCoordinates.push({
                        lat: customerlat,
                        lng: customerlng
                    });
                    destination = customerlat + "," + customerlng;
                    bounds.extend(new google.maps.LatLng(customerlat, customerlng));
                }

            }

            if (routeDetails) {
                map.fitBounds(bounds);
                return;
            }

            that.setMarkers(map, [driver, store, customer]);

            if (destination === "0,0") {
                destination = null;
            } else if (origin === "0,0") {
                origin = null;
            } else if (waypoint.location == "0,0") {
                waypoint = null;
            }

            try {
                directionsService.route({
                    origin: origin || waypoint.location,
                    destination: destination || waypoint.location,
                    waypoints: [waypoint],
                    optimizeWaypoints: true,
                    travelMode: 'DRIVING'
                }, function (response, status) {
                    if (status === 'OK') {
                        var myRoute = response.routes[0].legs[0];
                        var points = [];
                        if (status == google.maps.DirectionsStatus.OK) {
                            for (var i = 0; i < myRoute.steps.length; i++) {
                                for (var j = 0; j < myRoute.steps[i].lat_lngs.length; j++) {
                                    points.push(myRoute.steps[i].lat_lngs[j]);
                                }
                            }
                        }

                        var routLine = new google.maps.Polyline({
                            path: points,
                            strokeColor: "#338FFF",
                            strokeOpacity: 0.5,
                            strokeWeight: 10,
                            orderId: orderId
                        });

                        var infowindow = new google.maps.InfoWindow({
                            content: "Order Id: <strong>" + orderId + "</strong><Br>" +
                            "Driver: " + (driverName || "Not Available") + "<br>" +
                            "Store: " + (storeName || "Not Available") + "<br>" +
                            "Customer: " + (customerName || "Not Available"),

                            pixelOffset: new google.maps.Size(0, -10)
                        });

                        google.maps.event.addListener(routLine, 'mouseover', function (e) {
                            infowindow.open(map, this);
                            var point = e.latLng;
                            infowindow.setPosition(point);
                        });

                        google.maps.event.addListener(routLine, 'rightclick', function (e) {
                            var thisOrderId = this.orderId;
                            var allMarkers = that.getMarkers();
                            $.each(allMarkers, function (index, marker) {
                                if (marker.mapData.orderId) {
                                    if (marker.mapData.orderId === thisOrderId) {
                                        marker.setMap(null);
                                    }
                                }
                            });
                            this.setMap(null);
                            delete orderRoutes[this.orderId];

                        });

                        routLine.setMap(map);

                        orderRoutes[orderId] = routLine;


                    } else {
                        if (status === "ZERO_RESULTS" || status === "NOT_FOUND") {
                            Main.popDialog('Error', 'No directions available for these locations.', null, 'error');
                        } else {
                            $('#closeLinkedMapsBtn').click();
                            Main.popDialog('Error', 'Directions request failed due to ' + status, null, 'error');
                        }
                    }
                });

            } catch (e) {
                $('.loader').remove();
                Main.popDialog('Error', "Can't fetch the required location data.", null, 'error');
            }


        },

        removeAllMarkerAnimation: function (customMarkers) {
            if (customMarkers !== undefined) markers = customMarkers;
            for (var i in markers) {
                if (markers.hasOwnProperty(i)) {
                    markers[i].setAnimation(null);
                }
            }
        },

        locationToKey: function (location) {
            var latitude = location.latitude === undefined ? location.lat() : location.latitude;
            var longitude = location.longitude === undefined ? location.lng() : location.longitude;
            latitude = Number(latitude).toFixed(7);
            longitude = Number(longitude).toFixed(7);
            return (latitude + '_' + longitude).replace(/[.-]/g, '_');
        },

        getMarkers: function (type) {
            if (type === "Store") {
                return storeMarkers;
            } else if (type === "Driver") {
                return driverMarkers;
            } else if (type === "Customer") {
                return customerMarkers;
            } else {
                return markers;
            }
        },

        getRoutes: function () {
            return orderRoutes;
        },

        getMarkerType: function (markers) {
            var type = "";
            $.each(markers, function (index, data) {
                if (data) {
                    if (!$.isEmptyObject(data.type)) {
                        type = data.type;
                        return false;
                    }
                }
            });
            return type;
        },

        removeMarkersbyType: function (markers) {
            if (!markers) {
                return false;
            }

            $.each(markers, function (index, data) {
                data.setMap(null); //remove marker from map
            });

            $(document).trigger('maps.marker.remove');
        },

        getRouteOrderId: function (markers) {
            var orderId = "";
            $.each(markers, function (index, data) {
                if (data) {
                    if (!$.isEmptyObject(data.orderId)) {
                        orderId = data.orderId;
                        return false;
                    }
                }

            });
            return orderId;
        }
    };
})();