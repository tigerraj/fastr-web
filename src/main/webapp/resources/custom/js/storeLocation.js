/**
 * Created by Pratik on 7/5/2016.
 */

var storeLocationModule = (function () {
    var storeSettingsObj = {};
    var storeLocation = {
        ui: {
            locationBoxWrapper: $('#storeLocationWrapper'),
            brandLogo: $('.brand-logo'),
            brandTitle: $('.brand-title'),
            storeCount: $('.store-count'),
            storeStatus: $('.store-status'),
            servingAreaModal: $('.serving-area-modal'),
            servingAreaWrapper: $('#servingAreaWrapper'),
            selectAllGod: $('input[name="selectAllGod"]'),
            selectAllBox: $('input[name="selectAllBox"]'),
            saveChangesBtn: $('#saveChangesBtn'),
            driverListWrapper: $('#driverListWrapper'),
            selectAllDrivers: $('#selectAllDriverCheckbox'),
            driverListModal: $('#blackListedModal'),
            saveBlackListedBtn: $('#saveBLDriverChangesBtn'),
            ratePlanLink: $('a[data-action="ratePlan"]'),
            ratePlanModal: $('#ratePlanModal'),
            changeStatusLink: $('.trigger_status'),
            changeBrandStatusLink: $('.trigger_brand_status'),
            verifyStoreLink: $('[data-action="verifyStore"]'),
            storeSettingsLink: $('[data-action="storeSettings"]'),

            verityOptionModal: $('#verityOptionModal'),
            allowStoresDrivers: $('#allowStoresDrivers'),
            allowUnknownArea: $('#allowUnknownArea'),
            confirmVerifyStore: $('#confirmVerifyStore')


        },

        init: function () {
            this.getStoreDetail();
            this.events();
        },

        events: function () {
            var that = this;

            this.ui.locationBoxWrapper.on('click', this.ui.ratePlanLink.selector, function (e) {
                var link = $(e.target);
                var storeid = link.attr('data-storeid');
                var isdistancebasedRateplan = link.attr('data-isdistancebaserateplan');
                if (storeid != "undefined") {
                    if (link.attr('data-action') == "ratePlan") {
                        //alert(isdistancebasedRateplan);
                        that.getRatingPlans(storeid, isdistancebasedRateplan);

                    }
                }

            }).on('click', this.ui.changeStatusLink.selector, function (e) {
                var link = $(e.target);
                var storeId = link.attr('data-storeid');
                var status = link.attr('data-status');

                if (status != "undefined" && storeId != "undefined") {
                    that.changeStoreStatus(storeId, status);
                }

            }).on('click', this.ui.verifyStoreLink.selector, function (e) {
                var link = $(e.target);
                var storeId = link.attr('data-storeid');

                if (storeId != "undefined") {
                    //that.verifyStore(storeId);

                    that.ui.verityOptionModal.modal('show');
                    that.ui.verityOptionModal.attr('data-storeid', storeId).attr('data-isedit', false);
                }
            }).on('click', this.ui.storeSettingsLink.selector, function (e) {
                var link = $(e.target);
                var storeId = link.attr('data-storeid');

                if (storeId != "undefined") {
                    //that.verifyStore(storeId);

                    that.ui.verityOptionModal.modal('show');
                    that.ui.verityOptionModal.attr('data-storeid', storeId).attr('data-isedit', true);

                    var storeSettings = storeSettingsObj[storeId];
                    //console.log(storeSettings);

                    if (storeSettings.allowStoresDriver) {
                        that.ui.allowStoresDrivers.prop('checked', true);
                    }

                    if (storeSettings.allowUnknownAreaServing) {
                        that.ui.allowUnknownArea.prop('checked', true);
                    }

                }
            });

            $('body').on('click', $('.trigger_brand_status').selector, function (e) {
                var link = $(e.target);
                var storeBrandId = link.attr('data-storebrandid');
                var status = link.attr('data-status');

                if (status != "undefined" && storeBrandId != "undefined") {
                    that.changeStoreBrandStatus(storeBrandId, status);
                }
            });


            this.ui.ratePlanModal.on('show.bs.modal', function (event) {

            }).on('hide.bs.modal', function (event) {
                $(this).find('#WServiceArea').html('');
                $(this).find('#WOServiceArea').html('');
            });


            this.ui.servingAreaModal.on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var storeId = button.data('storeid');

                /*  var modal = $(this);
                 alert(storeId);*/
                $(this).attr('data-storeid', storeId);
                that.getAvailableAreas();
            }).on('shown.bs.modal', function (event) {
                $('.grid').masonry({
                    // options
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item',
                    percentPosition: true
                });

            }).on('hidden.bs.modal', function (event) {
                $('.grid').masonry('destroy');

                $(this).attr('data-storeid', "");

            });


            this.ui.driverListModal.on('shown.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var storeId = button.data('storeid');
                $(this).attr('data-storeid', storeId);
                if (that.ui.driverListWrapper.children().length) {
                    that.getBlacklistedDrivers(storeId);
                } else {
                    that.getAnyorderDrivers(storeId);
                }
            }).on('hidden.bs.modal', function (event) {
                $(this).attr('data-storeid', "");
                $(this).find('input[name="blackListedDriver"]').prop('checked', false);
            });

            this.ui.selectAllGod.change(function (e) {
                if ($(this).is(':checked')) {
                    that.ui.servingAreaWrapper.find('input[type="checkbox"]').prop('checked', true);
                } else {
                    that.ui.servingAreaWrapper.find('input[type="checkbox"]').prop('checked', false);
                }

            });

            this.ui.selectAllDrivers.change(function (e) {
                if ($(this).is(':checked')) {
                    that.ui.driverListWrapper.find('input[type="checkbox"]').prop('checked', true);
                } else {
                    that.ui.driverListWrapper.find('input[type="checkbox"]').prop('checked', false);
                }

            });


            this.ui.servingAreaWrapper.on('change', that.ui.selectAllBox, function (e) {
                var target = e.target;
                if (target.checked) {
                    $("div[data-parentid='" + target.id + "'] input[type='checkbox']").prop('checked', true);
                } else {
                    $("div[data-parentid='" + target.id + "'] input[type='checkbox']").prop('checked', false);
                }

            });


            this.ui.saveChangesBtn.click(function (e) {
                var storeId = that.ui.servingAreaModal.data('storeid');

                var checkedInputs = that.ui.servingAreaWrapper.find('input[name="subAreas"]:checked');
                var checkedInputIds = [];
                if (checkedInputs.length > 0) {
                    $.each(checkedInputs, function (index, input) {
                        checkedInputIds.push(input.id);
                    });

                    that.saveServingAreas(storeId, checkedInputIds);
                }

            });

            this.ui.saveBlackListedBtn.click(function (e) {
                var storeId = that.ui.driverListModal.data('storeid');

                var checkedInputs = that.ui.driverListWrapper.find('input[name="blackListedDriver"]:checked');
                var checkedInputIds = [];
                //if (checkedInputs.length > 0) {
                $.each(checkedInputs, function (index, input) {
                    checkedInputIds.push(input.id);
                });

                that.saveBlacklistedDrivers(storeId, checkedInputIds);
                //}

            });

            this.ui.verityOptionModal.on('hidden.bs.modal', function () {
                $(this).find('form').trigger('reset');
                $(this).removeAttr('data-storeid').removeAttr('data-isedit');
            });

            this.ui.confirmVerifyStore.on('click', function () {
                var storeId = that.ui.verityOptionModal.attr('data-storeid');
                var isEdit = that.ui.verityOptionModal.attr('data-isEdit');
                if (storeId) {
                    that.verifyStore(storeId, isEdit);
                }

            });
        },

        saveServingAreas: function (storeId, servingAreas) {
            var that = this;
            var url = pageContext + "merchant/update_store_serving_area";
            var callback = function (status, data) {
                if (data.success) {
                    that.ui.servingAreaModal.modal('hide');
                    var button1 = function () {
                        //window.location = Main.modifyURL(document.URL);
                        //window.location.reload();

                        $('#popDialog').modal('hide');

                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = [];

            $.each(servingAreas, function (index, areaId) {
                data.push({
                    "areaId": areaId
                });
            });

            Main.request(url, data, callback, {storeid: storeId, merchantid: merchantId});
        },


        getAvailableAreas: function () {
            var that = this;
            var url = pageContext + "smanager/get_active_area_list";
            var callback = function (status, data) {
                if (data.success) {
                    // console.log(data);
                    var availableAreaTempl = "";

                    $.each(data.params.areas, function (index, mainArea) {
                        //console.log(mainArea);
                        if (typeof(mainArea.areaName) != "undefined") {
                            if (mainArea.child && mainArea.child.length) {
                                availableAreaTempl += '<div class="grid-item"> ' +
                                    '<div class="s-area-box box-me">' +
                                    '<div class="s-area-box-header pt5 ph10"> ' +
                                    '<div class="pull-right">' +
                                    '<input id="' + mainArea.areaId + '" type="checkbox" name="selectAllBox" class="with-font form-control">' +
                                    '<label for="' + mainArea.areaId + '" class="control-label fontnormal">Select All</label>' +
                                    '</div>' +
                                    '<span class="fontbold font16">' + mainArea.areaName + '</span>' +
                                    '</div>';

                                $.each(mainArea.child, function (index, subArea) {
                                    availableAreaTempl += '<div class="pt5 ph10" data-parentid="' + mainArea.areaId + '"> ' +
                                        '<input id="' + subArea.areaId + '" type="checkbox" name="subAreas" class="with-font form-control" value="' + subArea.areaId + '"/> ' +
                                        '<label for="' + subArea.areaId + '" class="control-label fontnormal">' + subArea.areaName + '</label> </div> ';
                                });
                                availableAreaTempl += '</div></div>';
                            }
                        }
                    });

                    that.ui.servingAreaWrapper.html(availableAreaTempl);

                    that.getCheckedAreas();


                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        getCheckedAreas: function () {
            var that = this;
            var storeId = this.ui.servingAreaModal.data('storeid');
            var url = pageContext + "merchant/get_store_serving_area";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var servingAreas = data.params.servingArea;
                    $.each(servingAreas, function (index, servingArea) {
                        if (typeof(servingArea.child) != "undefined") {
                            if (servingArea.child && servingArea.child.length) {
                                $.each(servingArea.child, function (index, servingChild) {
                                    //console.log(servingChild.areaName);
                                    if (servingChild.selected) {
                                        $('input[id="' + servingChild.areaId + '"]').prop('checked', true);
                                    }

                                });
                            }

                        }
                    });


                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {storeid: storeId, merchantid: merchantId});
        },


        getStoreDetail: function () {
            var that = this;
            var url = pageContext + "merchant/get_brand_detail";
            var callback = function (status, data) {
                //console.log(data);
                if (data.success) {
                    var storeDetail = data.params.storeBrandDetail;
                    var stores = storeDetail.stores;

                    var storeLogo;
                    if (typeof(storeDetail.brandLogo) !== "undefined") {
                        storeLogo = storeDetail.brandLogo;
                    } else {
                        storeLogo = "http://placehold.it/175x175&text=SLogo";
                    }

                    var brandStatus = "";
                    if (storeDetail.status === "ACTIVE") {
                        brandStatus = '<a data-storebrandid="' + storeDetail.storeBrandId + '" data-status="INACTIVE" class="trigger_brand_status cursorpointer">Deactivate</a>';
                    } else {
                        brandStatus = '<a data-storebrandid="' + storeDetail.storeBrandId + '" data-status="ACTIVE" class="trigger_brand_status cursorpointer">Activate</a>';
                    }

                    that.ui.brandLogo.attr('src', storeDetail.brandLogo);
                    that.ui.brandTitle.html(storeDetail.brandName);
                    that.ui.storeCount.html(stores.length + ' Store/s');
                    that.ui.storeStatus.html(brandStatus);


                    var storelocs = "";
                    var isAdmin = (Main.getFromLocalStorage('userRole') === "ROLE_ADMIN");
                    var isManager = (Main.getFromLocalStorage('userRole') === "ROLE_MANAGER");
                    $.each(stores, function (index, location) {

                        storeSettingsObj[location.storeId] = {
                            allowStoresDriver: location.allowStoresDriver,
                            allowUnknownAreaServing: location.allowUnknownAreaServing
                        };


                        var status = "";
                        if (location.status === "ACTIVE") {
                            status = '<a href="#" data-storeid="' + location.storeId + '" data-status="INACTIVE" class="trigger_status col-xs-4 text-right positionabsolute top0 right0">Deactivate</a>';
                        } else {
                            status = '<a href="#" data-storeid="' + location.storeId + '" data-status="ACTIVE" class="trigger_status col-xs-4 text-right positionabsolute top0 right0">Activate</a>';
                        }

                        var verificationStatusLink = "";
                        if (isAdmin || isManager) {
                            if (!location.isVerified) {
                                verificationStatusLink = '<div class="col-xs-6 mb10"><a data-storeid="' + location.storeId + '" data-action="verifyStore" class="cursorpointer">Verify Store</a> </div>';
                            } else {
                                verificationStatusLink = '<div class="col-xs-6 mb10"><a data-storeid="' + location.storeId + '" data-action="storeSettings" class="cursorpointer">Store Settings</a> </div>';
                            }
                        }

                        storelocs += '<div class="col-sm-6 col-lg-3"> ' +
                            '<div class="store-address-box mv15"> ' +
                            '<div class="row positionrelative">' +
                            '<p class="store-address text-muted col-xs-8">' + location.givenLocation1 + ', ' + location.givenLocation2 + ' ' + '</p>  ' + status +
                            '</div>' +
                            '<div class="row">' +
                            '<div class="col-xs-6 mb10"><a href="#" data-toggle="modal" data-storeid="' + location.storeId + '" data-target="#servingAreaModal" class="">Serving Areas</a> </div>' +
                            '<div class="col-xs-6 mb10"><a href="#" class="" data-storeid="' + location.storeId + '" data-isdistancebaserateplan="' + (location.isDistanceBaseRatePlan || false) + '" data-action="ratePlan">Rate Plans</a> </div>' +
                            '<div class="col-xs-6 mb10"><a href="' + pageContext + 'merchant/store_manager?&storeId=' + location.storeId + '" class="">Store Managers</a> </div>' +
                                //'<div class="col-xs-6 mb10"><a href="' + pageContext + 'merchant/driver_list?&storeId=' + location.storeId + '" class="">Drivers</a> </div>' +
                            '<div class="col' +
                            '-xs-6 mb10"><a href="#" data-toggle="modal" data-storeid="' + location.storeId + '" data-target="#blackListedModal" class="">Blacklisted Drivers</a> </div>' +
                            verificationStatusLink +
                            '</div>' +
                            '<div class="clearfix">' +
                            '</div> ' +
                            '</div> ' +
                            '</div>';

                    });

                    that.ui.locationBoxWrapper.html(storelocs);


                    //console.log(storeSettingsObj);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {brandId: storeBrandId});
        },


        getAnyorderDrivers: function (storeId) {
            if (!storeId) {
                return;
            }
            var that = this;
            var url = pageContext + "smanager/get_any_order_drivers";
            var callback = function (status, data) {
                if (data.success) {
                    var anyOrderDrivers = data.params.drivers;
                    var driversUI = "<div class='row m0'>";
                    $.each(anyOrderDrivers, function (index, data) {
                        driversUI += '<div class="col-sm-6"><div class="pt5 ph10" data-parentid="' + data.driverId + '">' +
                            '<input id="' + data.driverId + '" type="checkbox" name="blackListedDriver" class="with-font form-control" value="' + data.driverId + '"/> ' +
                            '<label for="' + data.driverId + '" class="control-label fontnormal cursorpointer">' +
                            '<img src="' + data.user.profileImage + '" class="w40 mr5" alt=""> ' +
                                data.user.firstName + ' ' + data.user.lastName +
                            '</label> ' +
                            '</div> </div>';
                    });
                    //console.log(data);
                    driversUI += "</div>";
                    that.ui.driverListWrapper.html(driversUI);
                    that.getBlacklistedDrivers(storeId);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        getBlacklistedDrivers: function (storeId) {
            if (!storeId) {
                return;
            }
            var that = this;
            var url = pageContext + "smanager/get_driver_not_stores";
            var callback = function (status, data) {
                if (data.success) {
                    console.log(data);
                    var restrictedDrivers = data.params.drivers;
                    $.each(restrictedDrivers, function (index, data) {
                        $('input[id="' + data.driverId + '"]').prop('checked', true);
                    });
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {storeid: storeId});
        },

        saveBlacklistedDrivers: function (storeId, drivers) {
            var that = this;
            var url = pageContext + "smanager/update_stores_not_driver";
            var callback = function (status, data) {
                if (data.success) {
                    $('#blackListedModal').modal('hide');
                    var button1 = function () {
                        //window.location = Main.modifyURL(document.URL);
                        //window.location.reload();
                        $('#popDialog').modal('hide');
                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = [];

            $.each(drivers, function (index, driverId) {
                data.push({
                    "driverId": driverId
                });
            });

            Main.request(url, data, callback, {storeid: storeId, merchantid: merchantId});
        },

        getRatingPlans: function (storeId, isDistanceBasedRP) {
            var that = this;
            var userRole = Main.getFromLocalStorage('userRole');
            if (userRole === null) {
                Main.popDialog('Error', "You can't access right now.", null, 'error');
            } else if (userRole == "ROLE_MERCHANT") {
                that.ui.ratePlanModal.modal('show');
                var callback = function (status, data) {
                    var tmpl = "";
                    if (data.success) {
                        var ratePlans = data.params.ratePlans;
                        if (!$.isEmptyObject(ratePlans.unknownAreaRatePlans)) {
                            $.each(ratePlans.unknownAreaRatePlans, function (index, ratePlan) {
                                tmpl += '<div class="item-box "> ' +
                                    '<div class="row"> ' +
                                    '<div class="col-sm-4 text-center"> ' +
                                    '<p class="fontbold mb0 km-range">' + ratePlan.startDistance + '-' + ratePlan.endDistance + '</p> ' +
                                    '<p class="text-uppercase text-muted mb0">KM Range</p> ' +
                                    '</div> ' +
                                    '<div class="col-sm-4 text-center"> ' +
                                    '<p class="fontbold mb0 fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + ratePlan.baseCharge + '</p> ' +
                                    '<p class="text-uppercase text-muted mb0">base fare</p> ' +
                                    '</div> ' +
                                    '<div class="col-sm-4 text-center"> ' +
                                    '<p class="fontbold mb0 extra-fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + ratePlan.additionalChargePerKM + '</p> ' +
                                    '<p class="text-uppercase text-muted mb0">extra charge/KM</p> ' +
                                    '</div> ' +
                                    '</div> ' +
                                    '</div>';

                            });
                        } else if (!$.isEmptyObject(ratePlans.ratePlans)) {
                            $.each(ratePlans.ratePlans, function (index, ratePlan) {
                                tmpl += '<div class="item-box "> ' +
                                    '<div class="row"> ' +
                                    '<div class="col-sm-5 text-center"> ' +
                                    '<p class="fontbold mb0 order-range">' + ratePlan.orderCount + '</p> ' +
                                    '<p class="text-uppercase text-muted mb0">number of orders</p> ' +
                                    '</div> ' +
                                    '<div class="col-sm-5 col-sm-offset-2 text-center"> ' +
                                    '<p class="fontbold mb0 charge-amount">' + Main.getFromLocalStorage('currency') + ' ' + ratePlan.amount + '</p> ' +
                                    '<p class="text-uppercase text-muted mb0">charge/month</p> ' +
                                    '</div> ' +
                                    '</div> <hr class="hr-mod"> ' +
                                    '<div class="row"> ' +
                                    '<div class="col-sm-5 "> ' +
                                    '<p class=" text-muted">Per extra order above ' + ratePlan.orderCount + '</p> ' +
                                    '</div> ' +
                                    '<div class="col-sm-5 col-sm-offset-2 text-center"> ' +
                                    '<p class="fontbold mb0 extra-charge-amount">' + Main.getFromLocalStorage('currency') + ' ' + ratePlan.additionalChargePerOrder + '</p> ' +
                                    '</div> ' +
                                    '</div> ' +
                                    '</div>';
                            });
                        }

                        that.ui.ratePlanModal.find('#WServiceArea').html((tmpl === "") ? "No Rate Plans Available" : tmpl);
                    } else {
                        Main.popDialog('Error', data.message, null, 'error');
                    }
                };

                callback.requestType = "GET";
                Main.request(pageContext + "merchant/get_rate_plan", {}, callback, {storeid: storeId});


                //for unknown-area rate plans
                var callback2 = function (status, data) {
                    var tmpl = "";
                    if (data.success) {
                        $.each(data.params.ratePlans, function (index, ratePlan) {
                            tmpl += '<div class="item-box "> ' +
                                '<div class="row"> ' +
                                '<div class="col-sm-4 text-center"> ' +
                                '<p class="fontbold mb0 km-range">' + ratePlan.startDistance + '-' + ratePlan.endDistance + '</p> ' +
                                '<p class="text-uppercase text-muted mb0">KM Range</p> ' +
                                '</div> ' +
                                '<div class="col-sm-4 text-center"> ' +
                                '<p class="fontbold mb0 fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + ratePlan.baseCharge + '</p> ' +
                                '<p class="text-uppercase text-muted mb0">base fare</p> ' +
                                '</div> ' +
                                '<div class="col-sm-4 text-center"> ' +
                                '<p class="fontbold mb0 extra-fare-amount">' + Main.getFromLocalStorage('currency') + ' ' + ratePlan.additionalChargePerKM + '</p> ' +
                                '<p class="text-uppercase text-muted mb0">extra charge/KM</p> ' +
                                '</div> ' +
                                '</div> ' +
                                '</div>';
                        });
                        that.ui.ratePlanModal.find('#WOServiceArea').html((tmpl === "") ? "No Rate Plans Available" : tmpl);
                    } else {
                        Main.popDialog('Error', data.message, null, 'error');
                    }
                };

                callback2.requestType = "GET";

                Main.request(pageContext + "merchant/get_unknown_area_rate_plan", {}, callback2, {storeid: storeId});
            } else {
                window.location.href = pageContext + 'organizer/rate_list?&storeId=' + storeId + "&isDistancebasedRP=" + isDistanceBasedRP;
            }
        },

        changeStoreStatus: function (storeId, status) {
            var that = this;
            var url = pageContext + "merchant/change_store_status";
            var callback = function (status, data) {
                if (data.success) {
                    var btn = function () {
                        //window.location.reload();
                        $('#popDialog').modal('hide');
                        that.getStoreDetail();
                    };
                    btn.text = "Close";
                    var button = [btn];
                    Main.popDialog('Success', data.message, button, 'success', true);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = {
                "store": {
                    "storeId": storeId,
                    "status": status
                }
            };
            Main.request(url, data, callback, {});
        },

        changeStoreBrandStatus: function (storeBrandId, status) {
            var that = this;
            var url = pageContext + "merchant/change_store_brand_status";
            var callback = function (status, data) {
                if (data.success) {
                    var btn = function () {
                        //window.location.reload();
                        that.getStoreDetail();
                        $('#popDialog').modal('hide');
                    };
                    btn.text = "Close";
                    var button = [btn];
                    Main.popDialog('Success', data.message, button, 'success', true);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";
            var data = {
                "storeBrand": {
                    "storeBrandId": storeBrandId,
                    "status": status
                }
            };
            Main.request(url, data, callback, {});
        },

        verifyStore: function (storeId, isEdit) {

            var that = this;
            var url = pageContext + "admin/activate_store";
            if (isEdit == "true") {
                url = pageContext + "admin/update_store_setting";
            }
            var callback = function (status, data) {
                that.ui.verityOptionModal.modal('hide');
                if (data.success) {
                    var btn = function () {
                        window.location.reload();
                    };
                    btn.text = "Close";
                    var button = [btn];
                    Main.popDialog('Success', data.message, button, 'success', true);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "POST";
            var data = {
                "allowStoresDriver": that.ui.allowStoresDrivers.is(':checked'),
                "allowUnknownAreaServing": that.ui.allowUnknownArea.is(':checked')
            };
            Main.request(url, data, callback, {storeid: storeId});
        }

    };

    return {
        init: function () {
            storeLocation.init();
        }
    };
})();