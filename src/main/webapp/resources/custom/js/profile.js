/**
 * Created by Lunek on 6/8/2016.
 */

var profileModule = (function () {
    "use strict";

    var profile = {
        init: function () {
            this.getProfile(this.populateProfile);
            this.events();

            if (Main.getFromLocalStorage('userRole') == "ROLE_ADMIN") {
                this.ui.changePasswordLink.remove();
            }
        },
        ui: {
            form: $('#profileUpdateForm'),
            businessTitle: $('#businessTitle'),
            username: $('#username'),
            firstName: $('#firstName'),
            lastName: $('#lastName'),
            mobileNumber: $('#mobileNumber'),
            password: $('#curPassword'),
            submitBtn: $('#profileUpdateForm button[type="submit"]'),
            passwordFormWrapper: $('#passwordFormWrapper'),
            changePasswordLink: $('#changePasswordLink'),
            cancelPasswordChange: $('#cancelPasswordChange')
        },
        events: function () {
            var that = this;
            this.infoValidation();

            this.ui.changePasswordLink.on('click', function (e) {
                that.ui.passwordFormWrapper.fadeIn();
            });

            this.ui.cancelPasswordChange.on('click', function (e) {
                that.ui.passwordFormWrapper.fadeOut();
            });


        },
        infoValidation: function () {
            var profileObj = this;
            this.ui.form.validate({
                rules: {
                    businessTitle: {
                        // required: true
                    },
                    username: {
                        required: true
                    },
                    firstName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    mobileNumber: {
                        required: true,
                        number: true,
                        minlength: 10
                    },
                    curPassword: {
                        required: true,
                        minlength: 6
                    }
                },
                submitHandler: function () {
                    var merchantObj = {},
                        userObj = {},
                        headerObj = {},
                        data = {};
                    userObj.firstName = profileObj.ui.firstName.val();
                    userObj.lastName = profileObj.ui.lastName.val();
                    userObj.username = profileObj.ui.username.val();
                    userObj.mobileNumber = profileObj.ui.mobileNumber.val();
                    headerObj.password = profileObj.ui.password.val();
                    merchantObj.merchantId = profileObj.ui.merchantId;
                    // merchantObj.businessTitle = profileObj.ui.businessTitle.val();
                    merchantObj.user = userObj;
                    data.merchant = merchantObj;

                    var callback = function (status, data) {
                        if (data.success) {
                            profileObj.ui.password.val('');
                            Main.popDialog('Success', data.message, null, 'success');
                        } else {
                            Main.popDialog('Error', data.message, null, 'error');
                        }
                        profileObj.ui.submitBtn.prop('disable', false);
                    };

                    profileObj.updateProfile(data, callback, headerObj);
                    return false;
                }
            });
        },
        updateProfile: function (data, then, header) {
            var url = '/merchant/update_merchant';
            this.ui.submitBtn.prop('disable', true);
            Main.request(url, data, then, header);
        },
        populateProfile: function (status, data) {
            if (data.success) {
                var details = data.params.merchantDetail;
                var user = details.user || {};
                profile.ui.businessTitle.val(details.businessTitle || "").focus().blur();
                profile.ui.username.val(user.username || "").focus().blur();
                profile.ui.firstName.val(user.firstName || "").focus().blur();
                profile.ui.lastName.val(user.lastName || "").focus().blur();
                profile.ui.mobileNumber.val(user.mobileNumber || "").focus().blur();
                profile.ui.merchantId = details.merchantId;
            } else {
                Main.popDialog('Error', data.message, null, 'error');
            }
        },
        getProfile: function (then) {
            var url = '/merchant/get_merchant_detail';
            then.requestType = 'GET';
            var merchantId = Main.getNamedParameter('merchantId');
            //console.log(merchantId);
            if (merchantId == null) {
                merchantId = Main.getFromLocalStorage('merchantId');
            }
            Main.request(url, {}, then, {merchantId: Main.globalReplace(merchantId, '#', '')});
        }
    };

    var changePW = {

        init: function () {
            this.ui.userId = Main.getFromLocalStorage('userId');
            this.events();
        },
        ui: {
            form: $('#userChangePWForm'),
            curPW1: $("#curPassword1"),
            newPW: $('#newPassword'),
            confirmPW: $('#confirmPassword'),
            submitBtn: $('#userChangePWForm button[type="submit"]')
        },
        events: function () {
            this.validation();
        },
        validation: function () {
            var changePWObj = this;
            jQuery.validator.addMethod("notEqualTo", function (value, element, param) {
                return this.optional(element) || value != $(param).val();
            }, "New Password can't be same as old password");
            this.ui.form.validate({
                rules: {
                    curPassword1: {
                        required: true,
                        minlength: 6
                    },
                    newPassword: {
                        required: true,
                        minlength: 6,
                        notEqualTo: "#curPassword1"
                    },
                    confirmPassword: {
                        required: true,
                        minlength: 6,
                        equalTo: "#newPassword",
                    },
                },
                messages: {
                    confirmPassword: {
                        equalTo: "New Password do not match"
                    }
                },
                submitHandler: function () {
                    var data = {};
                    data.password = changePWObj.ui.curPW1.val();
                    data.newPassword = changePWObj.ui.confirmPW.val();
                    data.id = changePWObj.ui.userId;

                    var callback = function (status, data) {
                        if (data.success) {
                            Main.popDialog('Success', data.message, null, 'success');
                            changePWObj.ui.form[0].reset();
                        } else {
                            Main.popDialog('Error', data.message, null, 'error');
                        }
                        changePWObj.ui.submitBtn.prop('disable', false);
                    };

                    changePWObj.changePassword(data, callback);
                    return false;
                }
            });
        },
        changePassword: function (data, then) {
            var url = 'anon/change_password';
            this.ui.submitBtn.prop('disable', true);
            Main.request(url, {}, then, data);
        }
    };

    return {
        init: function () {
            profile.init();
            changePW.init();
        }
    };
})();

