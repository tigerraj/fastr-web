/**
 * Created by Pratik on 7/25/2016.
 */

var billingModule = (function () {

    "use strict";

    var listBills = {
        init: function () {
            this.getList();
            this.events();
            this.validation();
        },
        ui: {
            listTable: $('#listBillsTable'),
            approveBtn: $('.js-approve-bill'),
            statementModal: $('#statementModal'),
            statementModalForm: $('#statementModalForm'),

            merchantName: $('#merchantName'),
            statementPeriod: $('#statementPeriod'),

            dborders: $('#dborders'),
            dbAmount: $('#dbAmount'),

            ecOrders: $('#ecOrders'),
            ecAmount: $('#ecAmount'),

            orOrders: $('#orOrders'),
            orAmount: $('#orAmount'),

            otherCharges: $('#otherCharges'),
            totalAmount: $('#totalAmount'),

            generateStatementBtn: $('.generateStatementBtn'),

            listDT: {}
        },
        events: function () {
            var that = this;
            this.ui.listTable.on('click', this.ui.approveBtn.selector, function (event) {
                event.preventDefault();
                var sid = $(this).attr('data-id');
                var button1 = function () {
                    that.approveStatement(sid);
                    $('.modal').modal('hide');
                };
                button1.text = "Yes";
                var button2 = function () {
                    $('.modal').modal('hide');
                };
                button2.text = "No";
                var button = [button1, button2];
                Main.popDialog('Confirm', 'Are you sure you want to approve statement?', button);
            });

            that.ui.statementModal.on('hidden.bs.modal', function () {
                that.ui.statementModalForm.trigger('reset');
                that.ui.statementModalForm.removeAttr('data-id');
                that.ui.generateStatementBtn.removeAttr('data-id');
            }).on('show.bs.modal', function (e) {
                var statement_id = $(e.relatedTarget).attr('data-id');
                if (statement_id) {
                    that.getStatementById(statement_id);
                }
            });

            $(document).on('click', that.ui.generateStatementBtn.selector, function (e) {
                var statement_id = $(e.target).attr('data-id');
                if (statement_id) {
                    that.generateStatement(statement_id);
                }
            });
        },
        getList: function () {
            var that = this,
                user = {},
                params = {};

            var dataFilter = function (data, type) {
                if (data.success) {
                    var statements = data.params.statementList;
                    var responseRows = statements.numberOfRows;
                    var tableData = [];
                    var row = [],
                        merchantName = "",
                        statementPeriod = "",
                        ratePlanSubscription = "",
                        dborders = "-",
                        dbamt = "-",
                        ecorders = "",
                        ecamt = "",
                        ororders = "",
                        oramt = "",
                        otherCharges = "",
                        amount = "",
                        pdfLink = "",
                        paidDate = "",
                        action = "",
                        statementId = "";

                    $.each(statements.data, function (index, val) {
                        row = [];
                        merchantName = "";
                        statementPeriod = "";
                        ratePlanSubscription = "-";
                        dborders = "-";
                        dbamt = "-";
                        ecorders = "";
                        ecamt = "";
                        ororders = "";
                        oramt = "";
                        otherCharges = "0";
                        amount = "-";
                        pdfLink = "";
                        paidDate = "";
                        action = "";
                        statementId = "";
                        if (val.store) {
                            if (val.store.storeBrand) {
                                if (val.store.storeBrand.brandName) {
                                    merchantName = val.store.storeBrand.brandName || "";
                                    if (merchantName && val.store.givenLocation1) {
                                        merchantName += " ( " + val.store.givenLocation1 + " )";
                                    }
                                }
                            }
                        }
                        if (val.fromDate && val.toDate) {
                            statementPeriod = moment(val.fromDate).format("DD/MM/YY") + " - " + moment(val.toDate).format("DD/MM/YY");
                        }
                        if (val.orderPlan) {
                            //ratePlanSubscription = val.subscriptionRatePlan || "";
                            ratePlanSubscription = val.orderPlan || "";
                            ratePlanSubscription = Main.ucfirst(Main.globalReplace(ratePlanSubscription, '_', ' '));
                        }


                        if (val.distanceBasedOrderCount) {
                            dborders = val.distanceBasedOrderCount || "-";
                        }

                        if (val.distanceBasedOrderAmount) {
                            dbamt = val.distanceBasedOrderAmount || "-";
                        }


                        ecorders = val.extraOrderCount;
                        ecamt = val.extraOrderCharge;

                        ororders = val.outOfServingAreaOrderCount;
                        oramt = val.outOfServingAreaOrderCharge;


                        if (val.otherCharge) {
                            otherCharges = val.otherCharge || "";
                        }

                        if (val.totalPayableAmount) {
                            amount = Main.getFromLocalStorage('currency') + " " + Main.numberWithCommas(val.totalPayableAmount);
                        }


                        if (val.paidStatus && val.paidDate) {
                            paidDate = moment(val.paidDate).format('DD/MM/YY');
                        }

                        pdfLink = "<a href='#' class='generateStatementBtn' data-id='" + val.statementId + "'>Generate Statement</a>";

                        if (!val.paidStatus) {
                            //action = '<button type="button" class="btn btn-default js-approve-bill" data-id="' + val.statementId + '"> Pay </button>';
                            action = "<a href='#' data-toggle='modal' data-target='#statementModal' data-id='" + val.statementId + "'>Edit Statement</a> | " + pdfLink;
                        }

                        if (val.statementUrl) {
                            pdfLink = '<a href="' + val.statementUrl + '" target="_blank"> View Statement </a>';
                            action = pdfLink;
                        }

                        if (Main.getFromLocalStorage('userRole') !== "ROLE_ADMIN" && Main.getFromLocalStorage('userRole') !== "ROLE_MANAGER") {
                            action = '-';
                        }

                        /*console.log(Main.getFromLocalStorage('userRole') === "ROLE_MERCHANT");
                        console.log(val.paidStatus);*/
                        if (Main.getFromLocalStorage('userRole') === "ROLE_MERCHANT") {
                            if (val.statementUrl) {
                                action = pdfLink;
                            }
                        }


                        row = [index + 1, merchantName, statementPeriod, ratePlanSubscription, dborders, dbamt, ecorders, ecamt, ororders, oramt, otherCharges, amount, action];//amount, pdfLink, paidDate, action];
                        row = $.extend({}, row);
                        tableData.push(row);
                    });
                    var response = {};
                    response.data = tableData;
                    response.recordsTotal = responseRows;
                    response.recordsFiltered = responseRows;
                    return response;
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            dataFilter.requestType = "POST";
            dataFilter.url = pageContext + "smanager/get_statement";
            dataFilter.columns = [
                {"sortable": false},
                {"name": "store#storeBrand#merchant#user#firstName"},
                {"name": "fromDate"},
                {"name": "orderPlan"},
                {"name": "distanceBasedOrderCount"},
                {"name": "distanceBasedOrderAmount"},
                {"name": "extraOrderCount"},
                {"name": "extraOrderCharge"},
                {"name": "outOfServingAreaOrderCount"},
                {"name": "outOfServingAreaOrderCharge"},
                {"name": "otherCharge"},
                {"name": "totalPayableAmount"},
                {"sortable": false}
            ];
            user.userId = Main.getFromLocalStorage('userId');
            params.user = user;
            dataFilter.params = params;
            this.ui.listDT = Main.createDataTable(that.ui.listTable, dataFilter);
        },
        approveStatement: function (id) {
            var that = this,
                url = "",
                params = {},
                statement = {},
                callback;
            if (!id) {
                return;
            }

            url += 'organizer/approve_statement';
            statement.statementId = id;
            params.statement = statement;

            callback = function (status, data) {
                if (data.success) {
                    that.ui.listDT.ajax.reload(null, false);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "PUT";

            Main.request(url, params, callback, {});

        },

        getStatementById: function (statement_id) {
            var that = this;
            if (!statement_id) {
                return;
            }

            var callback = function (status, data) {
                if (data.success) {
                    var val = data.params.statement;

                    var merchantName = "";
                    var statementPeriod = "";
                    var ratePlanSubscription = "0";
                    var dborders = "0";
                    var dbamt = "0";
                    var ecorders = "0";
                    var ecamt = "0";
                    var ororders = "0";
                    var oramt = "0";
                    var otherCharges = "0";
                    var amount = "0";

                    if (val.store) {
                        if (val.store.storeBrand) {
                            if (val.store.storeBrand.brandName) {
                                merchantName = val.store.storeBrand.brandName || "";
                                if (merchantName && val.store.givenLocation1) {
                                    merchantName += " ( " + val.store.givenLocation1 + " )";
                                }
                            }
                        }
                    }
                    if (val.fromDate && val.toDate) {
                        statementPeriod = moment(val.fromDate).format("DD/MM/YY") + " - " + moment(val.toDate).format("DD/MM/YY");
                    }
                    if (val.subscriptionRatePlan) {
                        ratePlanSubscription = val.subscriptionRatePlan || "";
                    }
                    if (val.distanceBasedOrderCount) {
                        dborders = val.distanceBasedOrderCount || "-";
                    }
                    if (val.distanceBasedOrderAmount) {
                        dbamt = val.distanceBasedOrderAmount || "-";
                    }
                    if (val.otherCharge) {
                        otherCharges = val.otherCharge || "";
                    }
                    if (val.totalPayableAmount) {
                        amount = Main.getFromLocalStorage('currency') + " " + Main.numberWithCommas(val.totalPayableAmount);
                    }

                    if (val.statementUrl) {
                        that.ui.statementModalForm.find('.generateStatementBtn').addClass('hidden');
                    } else {
                        that.ui.statementModalForm.find('.generateStatementBtn').removeClass('hidden');
                    }

                    ecorders = val.extraOrderCount;
                    ecamt = val.extraOrderCharge;

                    ororders = val.outOfServingAreaOrderCount;
                    oramt = val.outOfServingAreaOrderCharge;

                    that.ui.merchantName.val(merchantName);
                    that.ui.statementPeriod.val(statementPeriod);
                    that.ui.dborders.val(dborders);
                    that.ui.dbAmount.val(dbamt);
                    that.ui.ecOrders.val(ecorders);
                    that.ui.ecAmount.val(ecamt);
                    that.ui.orOrders.val(ororders);
                    that.ui.orAmount.val(oramt);
                    that.ui.otherCharges.val(otherCharges);
                    that.ui.totalAmount.val(amount);
                    that.ui.statementModalForm.attr('data-id', statement_id);
                    that.ui.generateStatementBtn.attr('data-id', statement_id);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };

            callback.requestType = "GET";
            var url = pageContext + "smanager/get_statement?statementId=" + statement_id;
            Main.request(url, {}, callback, {});
        },

        validation: function () {
            var that = this;
            that.ui.statementModalForm.on('submit', function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    dborders: {
                        number: true
                    },
                    dbAmount: {
                        number: true
                    },
                    ecOrders: {
                        number: true
                    },
                    ecAmount: {
                        number: true
                    },
                    orOrders: {
                        number: true
                    },
                    orAmount: {
                        number: true
                    },
                    otherCharges: {
                        number: true
                    }
                },
                submitHandler: function (e) {
                    var formData = {
                        statement: {
                            statementId: that.ui.statementModalForm.data('id'),
                            extraOrderCount: that.ui.ecOrders.val(),
                            extraOrderCharge: that.ui.ecAmount.val(),
                            outOfServingAreaOrderCount: that.ui.orOrders.val(),
                            outOfServingAreaOrderCharge: that.ui.orAmount.val(),
                            distanceBasedOrderCount: that.ui.dborders.val(),
                            distanceBasedOrderAmount: that.ui.dbAmount.val(),
                            otherCharge: that.ui.otherCharges.val()
                        }
                    };

                    that.updateStatement(formData);
                }
            })
        },

        updateStatement: function (data) {
            var that = this;
            if (!data) {
                return;
            }

            var callback = function (status, data) {
                that.ui.statementModal.modal('hide');
                if (data.success) {
                    that.ui.listDT.ajax.reload();
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };

            callback.requestType = "POST";
            var url = pageContext + "smanager/update_statement";
            Main.request(url, data, callback, {});
        },

        generateStatement: function (statement_id) {
            var that = this;
            if (!statement_id) {
                return;
            }

            var callback = function (status, data) {
                if (data.success) {
                    Main.popDialog("Success", data.message, null, 'success');
                    that.ui.listDT.ajax.reload();
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };

            callback.requestType = "POST";
            var url = pageContext + "smanager/generate_statement";
            var data = {
                "statement": {
                    "statementId": statement_id
                }
            };
            Main.request(url, data, callback, {});
        }


    };

    return {
        list: function () {
            listBills.init();
        }
    };

})();

