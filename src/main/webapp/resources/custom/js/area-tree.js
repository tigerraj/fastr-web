/**
 * Created by Pratik on 5/3/2016.
 */

var addAreaModule = (function () {
    "use strict";
    var dropdownTemplate = "";
    var addArea = {
        ui: {
            addAreaForm: $('#addAreaForm'),
            areaName: $("#areaName"),
            streetName: $("#streetName"),
            parentArea: $("#parentArea"),
            latitude: $("#latitude"),
            longitude: $("#longitude"),
        },

        init: function () {
            this.validation();
            this.getAvailableAreas();
        },

        events: function () {

        },

        getAvailableAreas: function () {
            var that = this;
            var url = "/smanager/get_area_list";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data.params.areas)

                    var allAreas = data.params.areas;
                    that.multilevelAreas(allAreas, 20, true);

                    $("#parentArea").append(dropdownTemplate).selectpicker({
                        liveSearch: true,
                        title: "Parent Area",
                        style: "btn-ao-input"
                    });

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        multilevelAreas: function (areas, padding, bold) {
            var that = this;
            var fontw = "";
            if (bold) {
                fontw = "bold";
            }
            $.each(areas, function (index, data) {
                dropdownTemplate += "<option value='" + data.areaId + "' style='padding-left: " + padding + "px; font-weight: " + fontw + "'>" + data.areaName + "</option>";
                if (data.child.length > 0) {
                    that.multilevelAreas(data.child, padding + 15);
                }
            });
        },

        validation: function () {
            //console.log('validation');
            var that = this;
            this.ui.addAreaForm.validate({
                rules: {
                    areaName: {
                        required: true
                    },
                    streetName: {
                        required: true,
                    },
                    latitude: {
                        required: true,
                    },
                    longitude: {
                        required: true,
                    },
                },
                submitHandler: function () {
                    var callback = function (status, data) {
                        //console.log(data);
                        if (data.success) {
                            var button1 = function () {
                                window.location = Main.modifyURL(document.URL);
                            };
                            button1.text = "Close";
                            var button = [button1];
                            Main.popDialog("", data.message, button, 'success', true);
                            //that.ui.addAreaForm.trigger('reset');
                        } else {
                            Main.popDialog('Error', data.message, null, 'error');
                        }
                    };

                    callback["requestType"] = "PUT";

                    var data = {
                        "areaName": that.ui.areaName.val(),
                        "latitude": that.ui.latitude.val(),
                        "longitude": that.ui.longitude.val(),
                        "street": that.ui.streetName.val(),
                        "parent": {
                            "areaId": that.ui.parentArea.val()
                        }
                    };

                    //console.log(data);

                    Main.request('/smanager/save_area', data, callback);
                }
            });

        }
    };

    return {
        init: function () {
            addArea.init();
        }
    };

})();


var editAreaModule = (function () {
    "use strict";
    var dropdownTemplate = "";
    var editArea = {
        ui: {
            addAreaForm: $('#addAreaForm'),
            areaName: $("#areaName"),
            streetName: $("#streetName"),
            parentArea: $("#parentArea"),
            latitude: $("#latitude"),
            longitude: $("#longitude"),
            pageTitle: $('.content-title')
        },

        init: function () {
            this.validation();
            this.getAvailableAreas();
            this.getAreaDetails(areaId);
            //this.ui.addAreaForm.find("button[type='submit']").text('Save Changes');
            this.ui.pageTitle.text("Edit Area");
        },

        events: function () {

        },

        getAreaDetails: function (id) {
            //console.log(id);
            var that = this;
            var url = "/smanager/get_area";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data.params.area)
                    that.setAreaDetailsToFields(data.params.area);
                    var locations = [{lat: Number(data.params.area.latitude), lng: Number(data.params.area.longitude)}];
                    //console.log(locations);
                    mapsModule.addMarkers(locations);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {id: id});
        },

        setAreaDetailsToFields: function (data) {
            //console.log(data);
            var that = this;
            this.ui.areaName.val(data.areaName).focus().blur();
            this.ui.streetName.val(data.street).focus().blur();
            this.ui.longitude.val(data.longitude).focus().blur();
            this.ui.latitude.val(data.latitude).focus().blur();


            if (typeof(data.parent) !== 'undefined') {
                setTimeout(function () {
                    that.ui.parentArea.selectpicker('val', data.parent.areaId + "");
                }, 500);
            }

        },

        getAvailableAreas: function () {
            var that = this;
            var url = "/smanager/get_area_list";
            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data.params.areas)

                    var allAreas = data.params.areas;
                    that.multilevelAreas(allAreas, 20, true);

                    $("#parentArea").append(dropdownTemplate).selectpicker({
                        liveSearch: true,
                        title: "Parent Area",
                        style: "btn-ao-input"
                    });

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        multilevelAreas: function (areas, padding, bold) {
            var that = this;
            var fontw = "";
            if (bold) {
                fontw = "bold";
            }
            $.each(areas, function (index, data) {
                dropdownTemplate += "<option value='" + data.areaId + "' style='padding-left: " + padding + "px; font-weight: " + fontw + "'>" + data.areaName + "</option>";
                if (data.child.length > 0) {
                    that.multilevelAreas(data.child, padding + 15);
                }
            });
        },

        validation: function () {
            //console.log('validation');
            var that = this;
            this.ui.addAreaForm.validate({
                rules: {
                    areaName: {
                        required: true
                    },
                    streetName: {
                        required: true,
                    },
                    latitude: {
                        required: true,
                    },
                    longitude: {
                        required: true,
                    },
                },
                submitHandler: function () {
                    var callback = function (status, data) {
                        //console.log(data);
                        if (data.success) {
                            var button1 = function () {
                                window.location = Main.modifyURL(document.URL);
                            };
                            button1.text = "Close";
                            var button = [button1];
                            Main.popDialog("", data.message, button, 'success', true);
                            //that.ui.addAreaForm.trigger('reset');
                        } else {
                            Main.popDialog('Error', data.message, null, 'error');
                        }
                    };

                    callback["requestType"] = "PUT";

                    var data = {
                        "areaName": that.ui.areaName.val(),
                        "latitude": that.ui.latitude.val(),
                        "longitude": that.ui.longitude.val(),
                        "street": that.ui.streetName.val(),
                        "parent": {
                            "areaId": that.ui.parentArea.val()
                        }
                    };

                    console.log(data);

                    Main.request('/organizer/update_area', data, callback, {id: areaId});
                }
            });

        }
    };

    return {
        init: function () {
            editArea.init();
        }
    };

})();


var listAreaModule = (function () {
    "use strict";
    var listTemplate = "<ul>";
    var allAvailableAreas;
    var listArea = {
        ui: {
            areaViewWrapper: $('#areaViewWrapper'),
            areaFormWrapper: $('#areaFormWrapper'),
            categoryHomeBtn: $('#categoryHomeBtn'),
            areaForm: $('#areaDetailForm'),
            editAreaBtn: $('#editAreaBtn'),
            addRootAreaBtn: $('#addRootAreaBtn'),
            formlatlng: $('#formlatlng'),
            areaNameInput: $('#areaName'),
            latitudeInput: $('#latitude'),
            longitudeInput: $('#longitude'),
            areaStatusInput: $('#areaStatus'),
            addSubAreaBtn: $('#addSubAreaBtn'),
            mapsWrapper: $('#googleMapsWrapper'),
            submitBtn: $('#submitButton'),
            treeWrapper: $('#tree'),
        },

        init: function () {
            this.events();
            this.getAvailableAreas();
            this.validation();
            this.ui.areaViewWrapper.removeClass('hide');
        },

        googleMapsChange: function (option) {
            if (option == "show") {
                this.ui.mapsWrapper.removeClass('hide');
            } else if (option == "hide") {
                this.ui.mapsWrapper.addClass('hide');
            }

            setTimeout(function () {
                google.maps.event.trigger(map, 'resize')
            }, 200);

        },

        areaViewWrapperChange: function (option) {
            if (option == "show") {
                this.ui.areaViewWrapper.removeClass('hide');
            } else if (option == "hide") {
                this.ui.areaViewWrapper.addClass('hide');
            }
        },

        areaFormWrapperChange: function (option) {
            if (option == "show") {
                this.ui.areaFormWrapper.removeClass('hide');
            } else if (option == "hide") {
                this.ui.areaFormWrapper.addClass('hide');
            }
        },

        formElementsChange: function(option){
            if (option == "enable") {
                this.ui.areaForm.find('input, select').removeAttr('disabled');
            } else if (option == "disable") {
                this.ui.areaForm.find('input, select').attr('disabled', 'disabled');
            }
        },

        events: function () {
            var that = this;

            $(document).on('click', '.jstree-anchor', function (e) {
                var selectedNode = $('#tree').jstree('get_selected');
                var selectedNodeId = $("#" + selectedNode).attr('id');
                var selectedAreaId = $("#" + selectedNodeId).data('area-id')
                //console.log(selectedAreaId);

                that.getAreaDetails(selectedAreaId);
            });

            this.ui.categoryHomeBtn.click(function (e) {
                that.areaFormWrapperChange('hide');
                that.areaViewWrapperChange('show')
            });

            this.ui.addRootAreaBtn.click(function (e) {
                that.addRootArea();
            });

            this.ui.addSubAreaBtn.click(function (e) {
                that.addSubArea();
            });

            this.ui.editAreaBtn.click(function (e) {
                that.ui.submitBtn.html("Save Changes").removeClass('hide');
                that.formElementsChange('enable');
            });
        },

        validation: function () {
            var that = this;
            this.ui.areaForm.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    areaName: {
                        required: true
                    },
                    areaStatus: {
                        required: true,
                    },
                },
                submitHandler: function () {

                    var formEdit = (that.ui.submitBtn.text() == "Save Changes") ? true : false;

                    var callback = function (status, data) {
                        console.log(data);
                        if (data.success) {
                            var button1 = function () {
                                window.location = Main.modifyURL(document.URL);
                            };
                            button1.text = "Close";
                            var button = [button1];
                            Main.popDialog("", data.message, button, 'success', true);
                        } else {
                            Main.popDialog('Error', data.message, null, 'error');
                        }
                    };

                    callback["requestType"] = "PUT";

                    var data = {
                        "areaName": that.ui.areaNameInput.val(),
                        "latitude": that.ui.latitudeInput.val(), //that.ui.latitude.val(),
                        "longitude": that.ui.longitudeInput.val(), //that.ui.longitude.val(),
                        "street": "", //that.ui.streetName.val(),
                        "status": that.ui.areaStatusInput.val(), //that.ui.streetName.val(),
                        "parent": {
                            "areaId": that.ui.addSubAreaBtn.data('parent-id')//that.ui.parentArea.val()
                        }
                    };

                    console.log(data);

                    if (formEdit) {
                        data.parent.areaId = "";
                        Main.request(pageContext + 'organizer/update_area', data, callback, {id: that.ui.areaForm.data('area-id')});
                    } else {
                        Main.request(pageContext + 'organizer/save_area', data, callback);
                    }


                }
            });
        },

        addRootArea: function () {
            this.googleMapsChange('hide');

            this.formElementsChange('enable');


            this.areaViewWrapperChange('hide');
            this.areaFormWrapperChange('show');

            this.ui.editAreaBtn.addClass('hide');

            this.ui.submitBtn.html("Add Root Area").removeClass('hide');

            this.ui.addSubAreaBtn.addClass('hide');
            this.ui.areaForm.attr('data-area-id', "add-new");
            this.ui.formlatlng.addClass('hide');

            this.ui.areaForm.find('input, select').val("").focus().blur();
            this.ui.areaStatusInput.val("").focus().blur();

        },

        addSubArea: function () {
            //initGoogleMaps();

            this.googleMapsChange('show');
            mapsModule.clearMarkers();

            this.formElementsChange('enable');

            this.areaViewWrapperChange('hide');
            this.areaFormWrapperChange('show');

            this.ui.editAreaBtn.addClass('hide');

            this.ui.submitBtn.html("Add Sub Area").removeClass('hide');

            this.ui.addSubAreaBtn.addClass('hide');
            this.ui.areaForm.attr('data-area-id', "add-new-sub");
            this.ui.formlatlng.removeClass('hide');
            this.ui.areaForm.find('input, select').val("").focus().blur();
            this.ui.areaStatusInput.val("").focus().blur();
        },

        getAreaDetails: function (id) {
            var that = this;
            var url = pageContext + "smanager/get_area";
            var callback = function (status, data) {
                if (data.success) {
                    that.showAreaFormWithData(data.params.area);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {id: id});
        },

        showAreaFormWithData: function (areaDetails) {
            //console.log(areaDetails);

            this.formElementsChange('disable');

            if (areaDetails.latitude != "" && areaDetails.latitude != "0" && typeof(areaDetails.latitude) !== "undefined") {
                this.ui.formlatlng.removeClass('hide');
                //initGoogleMaps();
                this.googleMapsChange('show');
                var locations = [{lat: Number(areaDetails.latitude), lng: Number(areaDetails.longitude)}];
                //setTimeout(function(){
                mapsModule.addMarkers(locations);
                //},500);
            } else {
                this.ui.formlatlng.addClass('hide');
                this.googleMapsChange('hide');
            }


            this.areaViewWrapperChange('hide');
            this.areaFormWrapperChange('show');


            this.ui.addSubAreaBtn.removeClass('hide');
            this.ui.submitBtn.addClass('hide');
            this.ui.areaForm.attr('data-area-id', areaDetails.areaId);
            this.ui.addSubAreaBtn.attr('data-parent-id', areaDetails.areaId);

            this.ui.areaNameInput.val(areaDetails.areaName).focus().blur();
            this.ui.latitudeInput.val(areaDetails.latitude).focus().blur();
            this.ui.longitudeInput.val(areaDetails.longitude).focus().blur();
            this.ui.areaStatusInput.val(areaDetails.status).focus().blur();
        },

        multilevelAreas: function (areas) {
            var that = this;
            $.each(areas, function (index, data) {
                listTemplate += "<li data-area-id='" + data.areaId + "'><a>" + data.areaName + "<!--<span class='ml15 mr10 btn-link' data-action='edit'><i class='fa fa-pencil'></i></span><span class='btn-link' data-action='delete'><i class='fa fa-remove'></i></span>--></a>";
                if (data.child.length > 0) {
                    listTemplate += "<ul>";
                    that.multilevelAreas(data.child);
                    listTemplate += "</ul></li>";
                }
            });
        },


        getAvailableAreas: function () {
            var that = this;
            var url = pageContext + "smanager/get_area_list";
            var callback = function (status, data) {
                if (data.success) {
                    var allAreas = data.params.areas;
                    console.log(data);
                    allAvailableAreas = allAreas;
                    that.multilevelAreas(allAreas);
                    listTemplate += "</ul>";

                    that.ui.treeWrapper.append(listTemplate).jstree({
                        'core': {
                            'themes': {
                                'icons': false
                            }
                        },
                    });

                    that.generateAreaBox(allAreas);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            Main.request(url, {}, callback, {});
        },

        generateAreaBox: function (areas) {
            var that = this;
            $.each(areas, function (index, data) {
                that.ui.areaViewWrapper.append('' +
                    '<div class="col-md-4">' +
                    '<div class="box-me p25 mb25">' +
                    '<p class="fontbold">' + data.areaName + '</p>' +
                    '</div>' +
                    '</div>');
            });
        }

    };

    return {
        init: function () {
            listArea.init();
        }
    };

})();