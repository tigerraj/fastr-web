var origin = location.host;
var protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
var conn = new WebSocket(protocol + '//' + origin + '/socket');
conn.onopen = function () {
    console.log("Connected to the server");
};
//when we got a message from a signaling server
conn.onmessage = function (msg) {
    var data = JSON.parse(msg.data);
    switch (data.type) {
        case "message":
            console.log(data);
            orderMain.update(data);
            break;
        default:
            break;
    }
};
conn.onerror = function (err) {
    console.log("Got error", err);
};

var orderMain = (function () {
    'use strict';
    var orderList = {
        ui: {
            orderListWaitingCount: $('#orderListWaitingCount'),
            orderListRunningCount: $('#orderListRunningCount'),
            orderPlacedCount: $('#orderPlacedCount'),
            orderStartedCount: $('#orderStartedCount'),
            inRouteTopickUpCount: $('#inRouteTopickUpCount'),
            atStoreCount: $('#atStoreCount'),
            inRouteToDeliveryCount: $('#inRouteToDeliveryCount'),
            successfulCount: $('#pastOrderWrapper .counterHere'),
            cancelledCount: $('#cancelledOrderWrapper .counterHere'),
            header: {
                countUnassigned: $('#countUnassignedOrder'),
                countAssigned: $('#countAssignedOrder')
            },
            sidebar: {
                sidebarCountWaiting: $('#sidebarCountWaiting'),
                sidebarCountRunning: $('#sidebarCountRunning'),
                sidebarCountSuccess: $('#sidebarCountSuccess'),
                sidebarCountCancelled: $('#sidebarCountCancelled')
            }
        },
        count: {
            assigned: 0,
            unassigned: 0,
            waiting: 0,
            running: 0,
            successful: 0,
            cancelled: 0
        },
        addOrder: function (data) {
        },
        changeOrderStatus: function (data) {
            if (data.messageType !== 'ORDER_STATUS') {
                if (data.messageType !== 'NEW_ORDER') {
                    return;
                }
            }
            orderList.count.assigned = parseInt(orderList.ui.orderStartedCount) + parseInt(orderList.ui.inRouteTopickUpCount) + parseInt(orderList.ui.atStoreCount) + parseInt(orderList.ui.inRouteToDeliveryCount);
            orderList.count.unassigned = parseInt(orderList.ui.orderPlacedCount);
            orderList.count.waiting = parseInt(orderList.ui.orderPlacedCount) + parseInt(orderList.ui.orderStartedCount);

            var statusInfo = JSON.parse(data.message);
            console.log(statusInfo);
            var orderId = statusInfo.orderId;
            var driverId = statusInfo.driverId;
            switch (statusInfo.orderStatus) {
                case "ORDER_PLACED":
                    orderList.ui.orderPlacedCount.text(parseInt(orderList.ui.orderPlacedCount.text()) + 1);
                    orderList.ui.orderListWaitingCount.text(parseInt(orderList.ui.orderListWaitingCount.text()) + 1);
                    console.log("Increase ORDER_PLACED To: " + orderList.ui.orderPlacedCount.text());
                    break;
                case "ORDER_STARTED":
                    orderList.ui.orderStartedCount.text(parseInt(orderList.ui.orderStartedCount.text()) + 1);
                    console.log("Increase ORDER_STARTED To: " + orderList.ui.orderStartedCount.text());
                    break;
                case "EN_ROUTE_TO_PICK_UP":
                    // Increase
                    orderList.ui.inRouteTopickUpCount.text(parseInt(orderList.ui.inRouteTopickUpCount.text()) + 1);
                    orderList.ui.orderListRunningCount.text(parseInt(orderList.ui.orderListRunningCount.text()) + 1);
                    console.log("Increase EN_ROUTE_TO_PICK_UP To: " + orderList.ui.inRouteTopickUpCount.text());
                    console.log("Increase Running Count To: " + orderList.ui.inRouteTopickUpCount.text());

                    // Decrease
                    orderList.ui.orderStartedCount.text(parseInt(orderList.ui.orderStartedCount.text()) - 1);
                    orderList.ui.orderListWaitingCount.text(parseInt(orderList.ui.orderListWaitingCount.text()) - 1);
                    console.log('Decrease ORDER_STARTED: ' + orderList.ui.orderStartedCount.text());
                    console.log('Decrease Order Waiting Count: ' + orderList.ui.orderStartedCount.text());

                    break;
                case "AT_STORE":
                    orderList.ui.atStoreCount.text(parseInt(orderList.ui.atStoreCount.text()) + 1);
                    console.log("Increase AT_STORE To: " + orderList.ui.atStoreCount.text());

                    orderList.ui.inRouteTopickUpCount.text(parseInt(orderList.ui.inRouteTopickUpCount.text()) - 1);
                    console.log('Decrease EN_ROUTE_TO_PICK_UP: ' + orderList.ui.inRouteTopickUpCount.text());

                    break;
                case "EN_ROUTE_TO_DELIVER":
                    orderList.ui.inRouteToDeliveryCount.text(parseInt(orderList.ui.inRouteToDeliveryCount.text()) + 1);
                    console.log("Increase EN_ROUTE_TO_DELIVER To: " + orderList.ui.inRouteToDeliveryCount.text());

                    orderList.ui.atStoreCount.text(parseInt(orderList.ui.atStoreCount.text()) - 1);
                    console.log('Decrease AT_STORE: ' + orderList.ui.atStoreCount.text());

                    break;
                case "DELIVERED":
                    orderList.ui.successfulCount.text(parseInt(orderList.ui.successfulCount.text()) + 1);
                    console.log("Increase DELIVERED To: " + orderList.ui.successfulCount.text());

                    orderList.ui.inRouteToDeliveryCount.text(parseInt(orderList.ui.inRouteToDeliveryCount.text()) - 1);
                    orderList.ui.orderListRunningCount.text(parseInt(orderList.ui.orderListRunningCount.text()) - 1);
                    console.log('Decrease AT_CUSTOMER: ' + orderList.ui.inRouteToDeliveryCount.text());

                    break;
                case "CANCELLED":
                    orderList.ui.cancelledCount.text(parseInt(orderList.ui.cancelledCount.text()) + 1);
                    console.log("Increase CANCELLED To: " + orderList.ui.cancelledCount.text());
                    break;
            }
            var orderListItem = $('li[data-id="' + orderId + '"]');
            if (!orderListItem.length) {
                var activeTab = $('#ordersTabs').find('li.active');
                var activeTabAnchor = activeTab.find('a[data-toggle="tab"]');
                if (activeTabAnchor.attr('data-status') === statusInfo.orderStatus) {
                    fromSocket = true;
                    activeTab.removeClass('active');
                    activeTabAnchor.tab('show');
                }
            } else {
                var oldOrderStatus = orderListItem.closest('ul').attr('data-type');
                orderListItem.animate({ opacity: 0 },  300, function () {
                    orderListItem.remove();
                });
            }

            adminModule.orderCount();

            if (driverId) {
                var driverListItem = $('a[data-driver="' + driverId + '"]');
                if (driverListItem.length) {
                    var count = parseInt(driverListItem.find('strong').text());
                    if (statusInfo.orderStatus === 'ORDER_STARTED') {
                        if (count >= 0) {
                            driverListItem.find('strong').text(count + 1);
                            console.log('Increased Order Count: ' + count + 1);
                        }
                    }

                    if (statusInfo.orderStatus === 'CANCELLED' || statusInfo.orderStatus === 'DELIVERED') {
                        if (count !== 0) {
                            driverListItem.find('strong').text(count - 1);
                            console.log('Decreased Order Count: ' + (count - 1));
                        }
                    }
                }
            }
            return;
        }
    };

    var driverList = {
        changeDriverStatus: function (data) {
            if (data.messageType !== 'DRIVER_AVAILABILITY_STATUS') {
                return;
            }
        }
    };

    return {
        update: function (data) {
            if (!data && !data.messageType) {
                return;
            }
            switch (data.messageType) {
                case 'ORDER_STATUS':
                case 'NEW_ORDER':
                    orderList.changeOrderStatus(data);
                    break;
                case 'DRIVER_LOCATION_UPDATE':
                    break;
                case 'DRIVER_AVAILABILITY_STATUS':
                    driverList.changeDriverStatus(data);
                    break;
            }
        }
    };

})();

