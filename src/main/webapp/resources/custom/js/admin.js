/**
 * Created by Lunek on 6/21/2016.
 */

var adminModule = (function () {
    "use strict";

    var notificationList = {
        init: function () {
            this.getNotifications();
            this.events();
        },
        ui: {
            count: $('#notificationCount'),
            list: $('#notificationList'),
            listSidebar: $('#notificationListSidebar'),
            read: $('.js-read-notification')
        },
        events: function () {
            var that = this;
            this.ui.list.on('click', this.ui.read.selector, function (event) {
                event.preventDefault();
                that.updateNotification([$(this).attr('data-id')]);
            });
            $(document).on('scroll', function () {
                var listSidebar = that.ui.listSidebar;
                if (listSidebar.hasClass('in')) {
                    if (listSidebar.offset().top > 87) {
                        if (parseInt(listSidebar.css('top')) !== 0) {
                            listSidebar.addClass('scrolled');
                        }
                    } else {
                        listSidebar.removeClass('scrolled');
                    }
                }
            });
        },
        getNotifications: function () {
            var that = this,
                url = "",
                header = {},
                callback;
            url = "/merchant/get_notification";
            callback = function (status, data) {
                if (data.success) {
                    var details = data.params.notifications;
                    that.showNotificationList(details);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            callback.requestType = "GET";
            header.userId = Main.getFromLocalStorage('userId');
            Main.request(url, {}, callback, header);
        },
        showNotificationCount: function (data) {
            if (data) {
                this.ui.count.text(data);
            }
        },
        showNotificationList: function (data) {
            this.showNotificationCount(data.notViewedCount || "");
            if (data.notifications.length) {
                var notifications = "",
                    notificationCategory = "";
                $.each(data.notifications, function (index, val) {
                    var rightNow = moment(val.createdDate).calendar(null, {
                        sameDay: '[Today]',
                        lastDay: '[Yesterday]',
                        lastWeek: '[Last] dddd',
                        sameElse: '[Older]'
                    });
                    if (notificationCategory !== rightNow && index !== 0) {
                        notifications += '</ul>';
                    }
                    if (notificationCategory !== rightNow) {
                        notificationCategory = rightNow;
                        notifications += '<p class="text-center text-uppercase p5 notification-category">' + notificationCategory + '</p>';
                        notifications += '<ul class="notification-list list-unstyled mb0">';
                    }
                    if (notificationCategory === rightNow) {
                        notifications += '<li class="p20 notification"> <div class="media">';
                        notifications += '<div class="media-body">';
                        if (val.viewed) {
                            notifications += '<span>' + val.content + '</span>';
                        } else {
                            notifications += '<strong>' + val.content + '</strong>';
                        }
                        notifications += '<small class="ml20">' + moment(val.createdDate).fromNow() + '</small>';
                        notifications += '</div>';
                        if (!val.viewed) {
                            notifications += '<div class="media-right media-middle">';
                            notifications += '<button type="button" class="btn c-b-trans p0 js-read-notification" data-id="' + val.notificationId + '"> <i class="fa fa-circle" aria-hidden="true"></i> </button>';
                            notifications += '</div>';
                        }
                        notifications += '</div> </li>';
                    }
                });
                this.ui.list.html(notifications);
            }
        },
        updateNotification: function (idArr) {
            if (!idArr.length) {
                return;
            }
            var that = this,
                url = "",
                params = {},
                header = {},
                callback;

            url = "/merchant/update_viewed_notification";
            params = idArr;
            header.userId = Main.getFromLocalStorage('userId');
            callback = function (status, data) {
                if (data.success) {
                    that.updateNotificationList(idArr);
                } else {
                    Main.popDialog('Error', data.message, null, 'error');
                }
            };
            Main.request(url, params, callback, header);
        },
        updateNotificationList: function (idArr) {
            var that = this;
            if (idArr.length) {
                $.each(idArr, function (index, val) {
                    $(that.ui.read.selector + '[data-id="' + val + '"]').closest('.notification').addClass('read');
                });
            }
        }
    };

    var orderCount = {
        init: function () {
            this.requestCount(this.displayCount);
            this.events();
            this.ui.header.rangeSelector.find('li').eq(0).children('a').trigger('click');
        },
        ui: {
            orderCountWaiting: $('.js-order-count-waiting'),
            orderCountRunning: $('.js-order-count-running'),
            orderCountDelivered: $('.js-order-count-delivered'),
            orderCountCancelled: $('.js-order-count-cancelled'),
            header: {
                rangeSelector: $('#countRange'),
                countTotalOrder: $('#countTotalOrder'),
                countUnassignedOrder: $('#countUnassignedOrder'),
                countAssignedOrder: $('#countAssignedOrder')
            }
        },
        events: function() {
            var that = this;

            this.ui.header.rangeSelector.on('click', 'a', function(event) {
                event.preventDefault();
                that.requestHeaderCount($(this).text());
            });
        },
        count: {
            orderPlaced: 0,
            orderStarted: 0,
            inRouteToPickUp: 0,
            atStore: 0,
            inRouteToDeliver: 0,
            atCustomer: 0,
            delivered: 0,
            cancelled: 0,
            waiting: 0,
            running: 0,
            assigned: 0,
            unassigned: 0,
            total: 0,
        },
        requestCount: function (dataCallback) {
            var url = "/smanager/get_order_num_by_status",
                params = {},
                user = {},
                that = this,
                callback;

            callback = function (status, data) {
                if (data.success) {
                    var countDetail = data.params.numberOfOrderByStatus;
                    if (dataCallback && typeof dataCallback === 'function') {
                        that.setCount(countDetail, dataCallback);
                    } else {
                        that.setCount(countDetail);
                    }
                } else {
                    console.error(data.message);
                }
            };

            user.userId = Main.getFromLocalStorage('userId') || null;
            if (!user.userId) {
                return;
            }
            params.user = user;

            Main.request(url, params, callback, {});

        },
        setCount: function (data, afterSetCallback) {
            if ($.isEmptyObject(data)) {
                return;
            }

            this.count.orderPlaced = data.ORDER_PLACED || 0;
            this.count.orderStarted = data.ORDER_STARTED || 0;
            this.count.inRouteToPickUp = data.EN_ROUTE_TO_PICK_UP || 0;
            this.count.atStore = data.AT_STORE || 0;
            this.count.inRouteToDeliver = data.EN_ROUTE_TO_DELIVER || 0;
            this.count.atCustomer = data.AT_CUSTOMER || 0;
            this.count.delivered = data.DELIVERED || 0;
            this.count.cancelled = data.CANCELLED || 0;
            this.count.waiting = this.count.orderPlaced + this.count.orderStarted;
            this.count.running = this.count.inRouteToPickUp + this.count.atStore + this.count.inRouteToDeliver;

            this.count.assigned = this.count.orderStarted + this.count.inRouteToPickUp + this.count.atStore + this.count.inRouteToDeliver + this.count.atCustomer;
            this.count.unassigned = this.count.orderPlaced;
            this.count.total = this.count.orderPlaced + this.count.orderStarted + this.count.inRouteToPickUp + this.count.atStore + this.count.inRouteToDeliver + this.count.atCustomer + this.count.delivered + this.count.cancelled;

            if (afterSetCallback && typeof afterSetCallback === 'function') {
                afterSetCallback();
            }
        },
        displayCount: function () {
            var that = orderCount;
            that.ui.orderCountWaiting.text(that.count.waiting);
            that.ui.orderCountRunning.text(that.count.running);
            that.ui.orderCountDelivered.text(that.count.delivered);
            that.ui.orderCountCancelled.text(that.count.cancelled);
        },
        getCount: function () {
            return orderCount.count;
        },
        getRequestObject: function(type) {
            if (!type) {
                return {};
            }

            var requestObj = {},
                fromTime = 'T00:00:00',
                toTime = "T23:59:59",
                fromDate, toDate, fromDateTime, toDateTime;

            switch(type) {
                case 'Today':
                    fromDate = toDate = moment().format('YYYY-MM-DD');
                    fromDateTime = fromDate + fromTime;
                    toDateTime = toDate + toTime;
                    requestObj.fromDate = fromDateTime;
                    requestObj.toDate = toDateTime;
                    break;

                case 'Yesterday':
                    fromDate = toDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
                    fromDateTime = fromDate + fromTime;
                    toDateTime = toDate + toTime;
                    requestObj.fromDate = fromDateTime;
                    requestObj.toDate = toDateTime;
                    break;

                case 'Current Month':
                    fromDate =  moment().set('date', 1).format('YYYY-MM-DD');
                    toDate = moment().format('YYYY-MM-DD');
                    fromDateTime = fromDate + fromTime;
                    toDateTime = toDate + toTime;
                    requestObj.fromDate = fromDateTime;
                    requestObj.toDate = toDateTime;
                    break;

                case 'All':
                default:
                    requestObj = {};
            };

            return requestObj;
        },
        requestHeaderCount: function (requestType, dataCallback) {
            var url = "/smanager/order_count_by_date",
                params = {},
                user = {},
                that = this,
                callback;

            if (!requestType) {
                console.error('Could not find the request date range');
                return
            }

            var requestDateObj = this.getRequestObject(requestType);

            callback = function (status, data) {
                if (data.success) {
                    var countDetail = data.params.orderCountByDate;
                    if (dataCallback && typeof dataCallback === 'function') {
                        that.displayHeaderCount(countDetail, dataCallback);
                    } else {
                        that.displayHeaderCount(countDetail);
                    }
                } else {
                    console.error(data.message);
                }
            };

            callback.loaderDiv = null;

            user.userId = Main.getFromLocalStorage('userId') || null;
            if (!user.userId) {
                return;
            }
            params.user = user;
            if (requestDateObj.fromDate && requestDateObj.toDate) {
                params.fromDate = requestDateObj.fromDate;
                params.toDate = requestDateObj.toDate;
            }

            Main.request(url, params, callback, {});

        },
        displayHeaderCount: function(filteredCount, afterSetCallback) {
            if (!filteredCount) {
                return;
            }

            var that = orderCount;
            that.ui.header.countAssignedOrder.text(filteredCount.assignedOrderCount || 0);
            that.ui.header.countUnassignedOrder.text(filteredCount.unassignedOrderCount || 0);
            that.ui.header.countTotalOrder.text(filteredCount.totalOrderCount || 0);

            if (afterSetCallback && typeof afterSetCallback === 'function') {
                afterSetCallback();
            }
        }
    };

    return {
        notification: function () {
            notificationList.init();
        },
        orderCount: function () {
            orderCount.init();
        },
        getCount: function () {
            return orderCount.getCount();
        }
    };

})();

// var expressOrderModule = (function () {
//     return {
//         ui: {
//             expressModal: $('#expressModal'),
//             expressForm: $('#expressForm'),
//             expressStoreList: $('#expressStoreList'),
//             expressMainArea: $('#expressMainArea'),
//             expressSubArea: $('#expressSubArea'),

//         },

//         init: function () {
//             this.events();
//         },

//         events: function () {
//             var that = this;
//             this.ui.expressModal.on('show.modal.bs', function () {
//                 that.getStores();
//             });
//         },


//         getStores: function () {
//             var url = "/smanager/get_store_list",
//                 params = {},
//                 headers = {userId: Main.getFromLocalStorage('userId')},
//                 callback,
//                 that = this;

//             callback = function (status, data) {
//                 if (data.success) {
//                     var storeList = data.params.stores;
//                     var storeListTmpl = "";
//                     $.each(storeList, function (index, data) {
//                         storeListTmpl += "<div><input class='with-font form-control' type='radio' name='stores' id='" + data.storeId + "'><label for='" + data.storeId + "'>" + data.addressNote + "</label></div>"
//                     });
//                     that.ui.expressStoreList.html(storeListTmpl);
//                 } else {
//                     Main.popDialog('Error', data.message, null, 'error');
//                 }
//             };
//             callback.requestType = "GET";
//             Main.request(url, params, callback, headers);
//         },

//         getStoresArea: function (storeId) {
//             if (!storeId) {
//                 return;
//             }
//             var that = this,
//                 url = "",
//                 header = {},
//                 callback;

//             url = "/smanager/get_store_serving_area_list";
//             header.storeId = storeId;
//             callback = function (status, data) {
//                 if (data.success) {
//                     var storeAreaList = data.params.areas;
//                     console.log(storeAreaList);
//                 } else {
//                     Main.popDialog('Error', data.message, null, 'error');
//                 }
//             };
//             callback.requestType = 'GET';
//             Main.request(url, {}, callback, header);
//         }
//     }
// })();