/**
 * Created by Pratik on 6/30/2016.
 */


var sManagerModule = (function () {
    var storeId = "";
    var allStoreManagers = {};
    var sManager = {
        ui: {
            storeManagerModal: $('#storeManagerModal'),
            storeManagerWrapper: $('#storeManagerBoxWrapper'),

            storeManagerForm: $('#storeManagerForm'),
            firstNameInput: $('#firstName'),
            lastNameInput: $('#lastName'),
            emailInput: $('#emailAddress'),
            mobileInput: $('#mobileNumber'),
            statusInput: $('#sManagerStatus'),
            submitBtn: $('#storeManagerForm').find('button[type="submit"]'),

        },
        init: function () {
            storeId = Main.getNamedParameter('storeId');
            if (storeId === null) {
                var button1 = function () {
                    window.location = Main.modifyURL(pageContext);
                };
                button1.text = "Close";
                var button = [button1];
                Main.popDialog('Error', 'Invalid store Id', button, null);
                return;
            }


            this.events();
            this.validation();
            this.getAllStoreManagers();
        },
        events: function () {
            var that = this;
            this.ui.storeManagerModal.on('show.modal.bs', function (e) {
                var btn = $(e.relatedTarget);
                var smanagerid = btn.data('smanagerid');
                if (typeof(smanagerid) !== "undefined") {
                    $(this).attr('data-smanagerid', smanagerid);
                    $(this).find('.modal-title').html('Edit Store Manager');
                    that.ui.submitBtn.text('Save Changes');
                    that.setFormData(smanagerid);
                } else {
                    $(this).find('.modal-title').html('Add Store Manager');
                    that.ui.submitBtn.text('Add');
                }
            }).on('hide.modal.bs', function (e) {
                $(this).attr('data-smanagerid', '');
                that.ui.submitBtn.text('Add');
                that.ui.storeManagerModal.find('.modal-title').text('Add Store Manager');
                that.ui.storeManagerForm.find('input').val('');
                that.ui.emailInput.removeAttr('disabled');
            });

            this.ui.storeManagerWrapper.on('click', $('.change-status'), function (e) {
                var thisItem = $(e.target);
                var smanagerId = thisItem.attr('data-smanagerid');
                var status = thisItem.attr('data-status');
                console.log(status);
                if (typeof(status) != "undefined") {
                    that.changeManagerStatus(smanagerId, status);
                }
            });
        },

        setFormData: function (storeManagerId) {
            var curManager = allStoreManagers[storeManagerId];
            //console.log(curManager);
            if (typeof(curManager) != "undefined") {
                this.ui.firstNameInput.val(curManager.user.firstName);
                this.ui.lastNameInput.val(curManager.user.lastName);
                this.ui.emailInput.val(curManager.user.emailAddress).attr('disabled', 'disabled');
                this.ui.mobileInput.val(curManager.user.mobileNumber);
            }
        },

        changeManagerStatus: function (managerId, newStatus) {
            var that = this;

            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var button1 = function () {

                        //window.location = Main.modifyURL(document.URL.replace('#', ''));
                        $('#popDialog').modal('hide');
                        that.getAllStoreManagers();

                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };


            callback.requestType = "PUT";
            var data = {
                "storeManager": {
                    "storeManagerId": managerId,
                    "user": {
                        "userStatus": newStatus
                    }
                }
            };
            var url = pageContext + "merchant/change_store_manager_status ";
            Main.request(url, data, callback, {});

        },

        getAllStoreManagers: function () {
            var that = this;

            var callback = function (status, data) {
                if (data.success) {
                    //console.log(data);
                    var allStoreManagersUI = "";
                    $.each(data.params.storeManagers, function (index, data) {
                        allStoreManagers[data.storeManagerId] = data;
                        allStoreManagersUI += that.getStoreManagerUI(data);
                    });
                    that.ui.storeManagerWrapper.html(allStoreManagersUI);
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };


            callback.requestType = "GET";
            var url = pageContext + "merchant/get_store_managers";
            Main.request(url, {}, callback, {storeid: Main.globalReplace(storeId, '#', '')});

        },
        getStoreManagerUI: function (data) {
            //console.log(data);
            var statusLink = (data.user.userStatus === "ACTIVE") ?
            '<a data-smanagerid="' + data.storeManagerId + '" data-status="INACTIVE" class="change-status cursorpointer">Deactivate</a> ' :
            '<a data-smanagerid="' + data.storeManagerId + '" data-status="ACTIVE" class="change-status cursorpointer">Activate</a> ';
            statusLink = (data.user.userStatus === "UNVERIFIED") ? '':statusLink;

            var tmpl = '<div class="col-sm-3 mb25"> ' +
                '<div class="box-me p25"> ' +
                '<p class="full-name fontbold">' + data.user.firstName + ' ' + data.user.lastName + ' (' + data.user.userStatus + ')</p> ' +
                '<p class="email-address">' + data.user.emailAddress + '</p> ' +
                '<p class="contact-number">' + data.user.mobileNumber + '</p> ' +
                '<div class="pull-right"> ' +
                '<a href="#" data-smanagerid="' + data.storeManagerId + '" data-toggle="modal" data-target="#storeManagerModal" class="mr15">Edit</a>' +
                statusLink +
                '</div> ' +
                '<div class="clearfix"></div> ' +
                '</div> ' +
                '</div>';
            return tmpl;
        },

        addEditStoreManager: function () {
            var that = this;

            var callback = function (status, data) {
                if (data.success) {
                    that.ui.storeManagerModal.modal('hide');
                    var button1 = function () {
                        //window.location = Main.modifyURL(document.URL);
                        $('#popDialog').modal('hide');
                        that.getAllStoreManagers();
                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("", data.message, button, 'success', true);

                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                }
            };

            var data = {
                "storeManager": {
                    "user": {
                        "username": that.ui.emailInput.val(),
                        "firstName": that.ui.firstNameInput.val(),
                        "lastName": that.ui.lastNameInput.val(),
                        "emailAddress": that.ui.emailInput.val(),
                        "mobileNumber": that.ui.mobileInput.val()
                    },
                    "store": {
                        "storeId": Main.globalReplace(storeId, '#', '')
                    }
                }
            };

            var smanagerid = that.ui.storeManagerModal.attr('data-smanagerid');
            var url = "";
            if (smanagerid === "" || typeof(smanagerid) == "undefined") {
                callback.requestType = "POST";
                url = pageContext + "merchant/save_smanager ";
                Main.request(url, data, callback, {});
            } else {
                callback.requestType = "POST";
                data.storeManager.storeManagerId = smanagerid;
                url = pageContext + "smanager/update_store_manager ";
                Main.request(url, data, callback, {});
            }
        },

        validation: function () {
            var that = this;
            this.ui.storeManagerForm.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    firstName: {
                        required: true,
                    },
                    lastName: {
                        required: true,
                    },
                    emailAddress: {
                        required: true,
                    },
                    mobileNumber: {
                        required: true,
                        number: true,
                        minlength: 9,
                        maxlength: 10
                    }
                },
                submitHandler: function () {
                    that.addEditStoreManager();
                }
            });
        },
    };

    return {
        init: function () {
            sManager.init();
        }
    };
})();