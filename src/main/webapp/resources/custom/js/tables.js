/**
 * Created by Pratik on 6/15/2016.
 */

var tablesModule = (function () {

    "use strict";

    var superFT = [],
        superTable = [],
        tables = {
        ui: {
            tableWrapper: $('.table-ao-wrapper'),
            tableOptions: $('.table-options'),
            tableSearchInput: $('#tableSearch'),
            tableSearchBtn: $('#tableSearchBtn'),
            tableLimitInput: $('#tableLimit'),
            tableFilterInput: $('#tableFilter'),
            tableItself: $('.table-ao')
        },

        init: function (table, columnsConfig) {
            superTable[table.selector] = table;
            superTable[table.selector].config = columnsConfig;
            superFT = FooTable.init(table, {
                columns: columnsConfig
            });
            this.events();
        },

        events: function () {
            var that = this;
            this.ui.tableLimitInput.on('change', function () {
                var newSize = $(this).val();
                console.log(newSize);
                //that.getAvailableDrivers(newSize);
                superFT.pageSize(Number(newSize));
            });

            this.ui.tableItself.on('before.ft.sorting', function (e, ft, sorter) {
                //console.log(sorter);
                that.sortRows(sorter.column.name, sorter.direction);
            });
            this.ui.tableSearchBtn.on('click', function(event) {
                var searchKey = that.ui.tableSearchInput.val();
                if (searchKey !== "") {
                    that.filterSearchRows(searchKey);
                }
            });
            this.ui.tableSearchInput.on('keyup', function () {
                var searchKey = $(this).val();
                if (searchKey !== "") {
                    that.filterSearchRows(searchKey);
                }
            });

        },

        addRows: function (rows) {
            $.each(rows, function (index, row) {
                superFT.rows.add(row);
            });
        },

        removeAllRow: function () {
            //alert('remove rows');
            this.ui.tableItself.find('tbody').html('');
        },

        filterSearchRows: function (searchKey) {
            //send request with search key
            //this.removeAllRow();
            var that = this;
            var thisTable = "#" + that.ui.tableLimitInput.closest(that.ui.tableWrapper.selector).find('table').attr('id');
            var data = {
                "page": {
                    "pageNumber": 1,
                    "pageSize": that.ui.tableLimitInput.val(),
                    "searchFor": searchKey
                }
            };
            that.generateTableData(superTable[thisTable].url, data, superTable[thisTable].callback);
        },

        sortRows: function (colName, direction) {
            //send request with colname and sort direction
        },

        generateTableData: function (table, url, data, callback) {
            superTable[table.selector].request = url;
            superTable[table.selector].data = data;
            superTable[table.selector].callback = callback;
            Main.request(url, data, callback);
        }
    };

    return {
        generateTable: function (table, columnsConfig) {
            tables.init(table, columnsConfig);
        },

        generateTableRows: function (table, url, data, callback) {
            tables.generateTableData(table, url, data, callback);
        },

        showRows: function (rows) {
            tables.addRows(rows);
        }
    };
})();
