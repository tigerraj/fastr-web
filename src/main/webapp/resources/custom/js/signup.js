/**
 * Created by Pratik on 5/4/2016.
 */

var signUpModule = (function () {
    "use strict";

    var signUp = {
        ui: {
            datePicker: $("[data-action='datePicker']"),
            registrationForm: $("#registrationForm"),
            paymentForm: $("#paymentForm"),
            signupButton : $("#btnNext"),



            //signup form UIs
            businessTitle : $("#businessTitle"),
            firstName : $("#firstName"),
            lastName : $("#lastName"),
            email : $("#email"),
            password : $("#password"),
            contactNumber : $("#contact"),

            //card form UIs
            cardHolderName: $("#cardHolderName"),
            cardNumber: $("#cardNumber"),
            cvc: $("#cvc"),
            expiryDate: $("#expiryDate"),

        },

        init: function () {
            this.events();

            this.ui.paymentForm.hide();
        },

        events: function () {
            var that = this;
            this.ui.datePicker.datetimepicker({
                format: "MMMM, YYYY",
                minDate: moment(),
            });

            this.ui.registrationForm.validate({
                submitHandler: function (form) {
                    var submitData = {
                        "merchant": {
                            "businessTitle": that.ui.businessTitle.val(),
                            "user": {
                                "username": that.ui.email.val(),
                                "firstName": that.ui.firstName.val(),
                                "lastName": that.ui.lastName.val(),
                                "emailAddress": that.ui.email.val(),
                                "mobileNumber": that.ui.contactNumber.val()
                            }
                        },
                        "password": that.ui.password.val()
                    };

                    console.log(submitData);
                    //return;


                    that.doSignup(submitData);


                    /*that.ui.registrationForm.fadeOut(200, function(){
                     that.ui.paymentForm.fadeIn(200);
                     });*/

                }
            });

            this.ui.paymentForm.validate({
                submitHandler: function (form) {
                    alert("form valid");
                }
            });
        },

        doSignup: function(data) {
            var that = this;
            this.ui.signupButton.prop("disabled", true);

            var callback = function(status, data) {

                if (data.success) {
                    var button1 = function() {
                        window.location = Main.modifyURL("/");
                    };
                    button1.text = "Close";
                    var button = [button1];
                    Main.popDialog("Congratulations", data.message, button, 'success', true);
                    that.ui.signupButton.prop("disabled", false);
                    that.ui.registrationForm.trigger("reset");
                } else {
                    Main.popDialog("Error", data.message, null, 'error');
                    that.ui.signupButton.prop("disabled", false);
                }
            };

            Main.request('/anon/save_merchant', data, callback);
        }
    };

    return {
        init: function () {
            signUp.init();
        }
    }

})();