package com.yetistep.anyorder.service;

import com.yetistep.anyorder.dao.inf.*;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.*;
import com.yetistep.anyorder.service.impl.StoreManagerMobileServiceImpl;
import com.yetistep.anyorder.service.inf.StoreManagerMobileService;
import com.yetistep.anyorder.util.RandomValueFieldPopulator;
import org.apache.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/19/16
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class StoreManagerMobileServiceTest {

    private static final Logger log = Logger.getLogger(StoreManagerMobileServiceTest.class);

    @Mock
    UserDao userDaoMock;

    @Mock
    AreaDao areaDaoMock;

    @Mock
    OrderDao orderDaoMock;

    @Mock
    CustomerDao customerDaoMock;

    @Mock
    CustomersAreaDao customersAreaDaoMock;

    @Mock
    StoreManagerDao storeManagerDaoMock;

    @Mock
    UserDeviceDao userDeviceDaoMock;

    @Mock
    DriverDao driverDaoMock;


    @InjectMocks
    StoreManagerMobileService storeManagerMobileServiceMock = new StoreManagerMobileServiceImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testStoreManagerLogin() throws Exception {

        UserEntity user = new UserEntity();
        user.setPassword("$2a$10$8eZdSJo9.XWUjN5vpWHisee1zbcjdb4kaoCsYxKxgkyh8U3xRevlq");
        //RandomValueFieldPopulator.populate(user, "password", "fullName");
        UserDeviceEntity userDevice = new UserDeviceEntity();
        userDevice.setDeviceUuid("34455-346");
        List<UserDeviceEntity> userDevices = new ArrayList<>();
        userDevices.add(userDevice);
        user.setUserDevices(userDevices);
        HeaderDto headerDto = new HeaderDto();
        headerDto.setUsername("test@yopmail.com");
        headerDto.setPassword("password");

        when(userDaoMock.findByUserName(headerDto.getUsername())).thenReturn(user);

        UserDeviceEntity inUserDevice = new UserDeviceEntity();
        userDevice.setDeviceUuid("3445d455-346");
        UserEntity userEntity = storeManagerMobileServiceMock.storeManagerLogin(headerDto, inUserDevice);

        Assert.assertThat(userEntity.getAccessToken(), CoreMatchers.instanceOf(String.class));
        Assert.assertEquals(user.getUserDevices().size(), 2);
        Assert.assertEquals(user.getUserDevices().get(0).getAccessToken(), null);
        Assert.assertNotNull(user.getUserDevices().get(1).getAccessToken());
        verify(userDaoMock, timeout(1)).findByUserName(headerDto.getUsername());
        verify(userDaoMock, timeout(1)).update(user);
    }

    @Test
    public void testCreateOrder() throws Exception {

        OrderEntity order = new OrderEntity();
        RandomValueFieldPopulator.populate(order, "orderDescription", "deliveryDate", "orderTotal", "customersNoteToDriver", "deliveryFee");
        CustomerEntity customerEntity = new CustomerEntity();
        CustomersAreaEntity customersAreaEntity = new CustomersAreaEntity();
        String customerId =  "3edffghfgj";
        customerEntity.setCustomerId(customerId);
        String customerAreaId = "34fglkfhgp";
        customersAreaEntity.setCustomersAreaId(customerAreaId);

        List<CustomersAreaEntity> customersAreaEntities = new ArrayList<>();
        customersAreaEntities.add(customersAreaEntity);

        customerEntity.setCustomersArea(customersAreaEntities);
        order.setCustomer(customerEntity);

        HeaderDto headerDto = new HeaderDto();
        headerDto.setAccessToken("dfgkldhf;hkfgph");
        headerDto.setStoreManagerId("dfmngjkndg83u4j435nm3j4k");
        CustomerEntity returnCustomer = new CustomerEntity();
        RandomValueFieldPopulator.populate(returnCustomer);

        StoreManagerEntity returnStoreManager = new StoreManagerEntity();
        returnStoreManager.setStoreManagerId(headerDto.getStoreManagerId());
        returnStoreManager.setStore(new StoreEntity());

        UserDeviceEntity returnUserDevice = new UserDeviceEntity();
        UserEntity returnUser = new UserEntity();
        returnUser.setStoreManager(returnStoreManager);
        returnUserDevice.setUser(returnUser);

        CustomersAreaEntity returnCustomersAreaEntity = new CustomersAreaEntity();
        RandomValueFieldPopulator.populate(returnCustomersAreaEntity);

        when(userDeviceDaoMock.findByAccessToken(anyString())).thenReturn(returnUserDevice);
        when(orderDaoMock.save(order)).thenReturn(true);
        when(customerDaoMock.findByCustomerId(customerId)).thenReturn(returnCustomer);
        when(customersAreaDaoMock.findByCustomersAreaId(customerAreaId)).thenReturn(returnCustomersAreaEntity);

        RequestJsonDto requestJsonDto = new RequestJsonDto();
        requestJsonDto.setOrder(order);
        requestJsonDto.setSaveAndAssign(false);
        storeManagerMobileServiceMock.createOrder(headerDto, requestJsonDto);

        verify(userDeviceDaoMock, times(1)).findByAccessToken(anyString());
        verify(orderDaoMock, times(1)).save(order);
        verify(customerDaoMock, times(1)).findByCustomerId(customerId);
        verify(customersAreaDaoMock, times(1)).findByCustomersAreaId(customerAreaId);
        verifyNoMoreInteractions(orderDaoMock);
        verifyNoMoreInteractions(customerDaoMock);
        verifyNoMoreInteractions(customersAreaDaoMock);
        verifyNoMoreInteractions(storeManagerDaoMock);

    }

    @Test
    public void testCreateOrderWIthNewCustomer() throws Exception {

        OrderEntity order = new OrderEntity();
        RandomValueFieldPopulator.populate(order, "orderDescription", "deliveryDate", "orderTotal", "customersNoteToDriver", "deliveryFee");
        CustomerEntity customerEntity = new CustomerEntity();
        CustomersAreaEntity customersAreaEntity = new CustomersAreaEntity();
        AreaEntity area = new AreaEntity();
        String areaId =  "3edffghfgj";
        area.setAreaId(areaId);
        customersAreaEntity.setArea(area);
        customersAreaEntity.setStreet("street");
        UserEntity user = new UserEntity();
        RandomValueFieldPopulator.populate(user, "firstName", "lastName", "mobileNumber");
        customerEntity.setUser(user);

        List<CustomersAreaEntity> customersAreaEntities = new ArrayList<>();
        customersAreaEntities.add(customersAreaEntity);

        customerEntity.setCustomersArea(customersAreaEntities);
        order.setCustomer(customerEntity);

        HeaderDto headerDto = new HeaderDto();
        headerDto.setAccessToken("dfgkldhf;hkfgph");
        headerDto.setStoreManagerId("dfmngjkndg83u4j435nm3j4k");

        CustomerEntity returnCustomer = new CustomerEntity();
        RandomValueFieldPopulator.populate(returnCustomer);

        StoreManagerEntity returnStoreManager = new StoreManagerEntity();
        returnStoreManager.setStoreManagerId(headerDto.getStoreManagerId());
        returnStoreManager.setStore(new StoreEntity());

        UserDeviceEntity returnUserDevice = new UserDeviceEntity();
        UserEntity returnUser = new UserEntity();
        returnUser.setStoreManager(returnStoreManager);
        returnUserDevice.setUser(returnUser);

        CustomersAreaEntity returnCustomersAreaEntity = new CustomersAreaEntity();
        RandomValueFieldPopulator.populate(returnCustomersAreaEntity);

        AreaEntity returnArea = new AreaEntity();
        RandomValueFieldPopulator.populate(returnArea);
        returnArea.setAreaId(areaId);

        when(userDeviceDaoMock.findByAccessToken(anyString())).thenReturn(returnUserDevice);
        when(areaDaoMock.findByAreaId(areaId)).thenReturn(returnArea);
        when(orderDaoMock.save(order)).thenReturn(true);

        RequestJsonDto requestJsonDto = new RequestJsonDto();
        requestJsonDto.setSaveAndAssign(false);
        requestJsonDto.setOrder(order);
        storeManagerMobileServiceMock.createOrder(headerDto, requestJsonDto);

        verify(userDeviceDaoMock, times(1)).findByAccessToken(anyString());
        verify(areaDaoMock, times(1)).findByAreaId(areaId);
        verify(orderDaoMock, times(1)).save(order);
        verifyNoMoreInteractions(orderDaoMock);
        verifyNoMoreInteractions(storeManagerDaoMock);

    }

    //currently not used
    @Test
    public void getCustomerByMobileNumberTest() throws Exception {
        String mobileNumber = "9849540028";
        UserEntity user = new UserEntity();
        user.setMobileNumber(mobileNumber);
        CustomerEntity customer = new CustomerEntity();
        RandomValueFieldPopulator.populate(user);
        RandomValueFieldPopulator.populate(customer);
        user.setCustomer(customer);
        HeaderDto headerDto = new HeaderDto();
        RequestJsonDto requestJsonDto = new RequestJsonDto();
        requestJsonDto.setMobileNumber(mobileNumber);
        List<UserEntity> users = new ArrayList<>();
        users.add(user);
        when(userDaoMock.findCustomersByMobileNumber(mobileNumber)).thenReturn(users);

        List<UserEntity> returnUsers = storeManagerMobileServiceMock.getCustomersByMobileNumber(headerDto, requestJsonDto);
        verify(userDaoMock, times(1)).findCustomersByMobileNumber(mobileNumber);
        verifyNoMoreInteractions(userDaoMock);

        for (UserEntity returnUser: returnUsers){
            Assert.assertNotNull(returnUser.getFirstName());
            Assert.assertNotNull(returnUser.getLastName());
            Assert.assertNotNull(returnUser.getMobileNumber());
            Assert.assertNotNull(returnUser.getCustomer().getCustomerId());
        }
    }

    @Test
    public void getCustomerInfoTest() throws Exception {
        String customerId = "34dfgjki34j09smkd394i539404";
        UserEntity user = new UserEntity();
        RandomValueFieldPopulator.populate(user);
        CustomerEntity customer = new CustomerEntity();
        RandomValueFieldPopulator.populate(customer);
        customer.setCustomerId(customerId);
        CustomersAreaEntity customersArea1 = new CustomersAreaEntity();
        RandomValueFieldPopulator.populate(customersArea1);
        CustomersAreaEntity customersArea2 = new CustomersAreaEntity();
        RandomValueFieldPopulator.populate(customersArea2);
        HeaderDto headerDto = new HeaderDto();
        headerDto.setId(customerId);
        List<CustomersAreaEntity> customersAreas = new ArrayList<>();
        AreaEntity area1 = new AreaEntity();
        AreaEntity area2 = new AreaEntity();
        AreaEntity area3 = new AreaEntity();
        RandomValueFieldPopulator.populate(area1);
        RandomValueFieldPopulator.populate(area2);
        RandomValueFieldPopulator.populate(area3);
        area2.setParent(area1);
        customersArea1.setArea(area2);
        customersArea2.setArea(area3);
        customersAreas.add(customersArea1);
        customersAreas.add(customersArea2);
        customer.setCustomersArea(customersAreas);
        user.setCustomer(customer);
        customer.setUser(user);
        String mobileNumber = "9849540028";
        RequestJsonDto requestJsonDto = new RequestJsonDto();
        requestJsonDto.setMobileNumber(mobileNumber);

        when(userDaoMock.findByMobileNumberAndRole(any(String.class), any(UserRole.class))).thenReturn(user);

        CustomerEntity customerEntity = storeManagerMobileServiceMock.getCustomersInfo(headerDto, requestJsonDto);
        verify(userDaoMock, times(1)).findByMobileNumberAndRole(any(String.class), any(UserRole.class));
        verifyNoMoreInteractions(customerDaoMock);

        Assert.assertNotNull(customerEntity.getCustomerId());
        Assert.assertNotNull(customerEntity.getUser().getFirstName());
        Assert.assertNotNull(customerEntity.getUser().getFirstName());

        List<CustomersAreaEntity> customersAreaEntities = customerEntity.getCustomersArea();
        for (CustomersAreaEntity returnCustomerArea: customersAreaEntities){
            Assert.assertNotNull(returnCustomerArea.getCustomersAreaId());
            Assert.assertNotNull(returnCustomerArea.getStreet());
            Assert.assertNotNull(returnCustomerArea.getGivenLocation());
            AreaEntity testCustomersArea = returnCustomerArea.getArea();
            while (testCustomersArea.getParent()!=null && testCustomersArea.getParent().getAreaId()!=null){
                Assert.assertNotNull(testCustomersArea.getAreaId());
                Assert.assertNotNull(testCustomersArea.getAreaName());
                testCustomersArea = testCustomersArea.getParent();
            }
        }
    }

    @Test
    public void testGetStoresDrivers() throws Exception {
        HeaderDto headerDto = new HeaderDto();
        headerDto.setAccessToken("dfgkldhf;hkfgph");
        UserDeviceEntity userDeviceEntity = new UserDeviceEntity();
        UserEntity user = new UserEntity();
        StoreManagerEntity storeManager = new StoreManagerEntity();
        StoreEntity store = new StoreEntity();
        List<DriverEntity> drivers = new ArrayList<>();
        DriverEntity driver = new DriverEntity();
        DriverEntity driver1 = new DriverEntity();
        RandomValueFieldPopulator.populate(driver);
        RandomValueFieldPopulator.populate(driver1);
        UserEntity driverUser = new UserEntity();
        RandomValueFieldPopulator.populate(driverUser);
        UserEntity driverUser1 = new UserEntity();
        RandomValueFieldPopulator.populate(driverUser1);
        driver.setUser(driverUser);
        driver1.setUser(driverUser1);
        drivers.add(driver);
        drivers.add(driver1);
        store.setDrivers(drivers);
        storeManager.setStore(store);
        user.setStoreManager(storeManager);
        userDeviceEntity.setUser(user);
        when(userDeviceDaoMock.findByAccessToken(anyString())).thenReturn(userDeviceEntity);
        when(driverDaoMock.getDriversOrderCount(anyString())).thenReturn(2);

        List<DriverEntity> driversList = storeManagerMobileServiceMock.getStoresDrivers(headerDto);

        for (DriverEntity driverEntity: driversList) {
            Assert.assertNotNull(driverEntity.getUser().getFirstName());
            Assert.assertNotNull(driverEntity.getUser().getLastName());
            Assert.assertNotNull(driverEntity.getDriverId());
            Assert.assertSame(driverEntity.getPreviousOrderCount(), 2);
        }
    }


}
