package com.yetistep.anyorder.service;

import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.model.CustomerEntity;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.service.impl.UserServiceImpl;
import com.yetistep.anyorder.service.inf.UserService;
import com.yetistep.anyorder.util.RandomValueFieldPopulator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 4/29/16
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    UserDao userDaoMock;

    @InjectMocks
    UserService userServiceMock = new UserServiceImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testSaveUser() throws Exception {
        UserEntity user = new UserEntity();
        CustomerEntity customer = new CustomerEntity();
        /*user.setFullName("User Name");
        user.setEmailAddress("email@yopmail.com");
        user.setMobileNumber("9849540028");*/
        RandomValueFieldPopulator.populate(user);
        RandomValueFieldPopulator.populate(customer);
        user.setCustomer(customer);
        userServiceMock.saveUser(user);

        verify(userDaoMock, times(1)).save(user);

        verifyNoMoreInteractions(userDaoMock);
    }

    @Test
    public void testChangePassword() throws Exception {

        HeaderDto headerDto = new HeaderDto();
        headerDto.setId("3446fgtr756");
        headerDto.setPassword("password");
        UserEntity user = new UserEntity();
        RandomValueFieldPopulator.populate(user);
        user.setPassword("$2a$10$/ZnJcIU0uHB6YV6HdinBOOVwjo3o1ZLJ8c0.oOXq4UpwsAXcycpv6");
        when(userDaoMock.findByUserId(headerDto.getId())).thenReturn(user);

        userServiceMock.changePassword(headerDto);

        verify(userDaoMock, times(1)).findByUserId(headerDto.getId());
        verify(userDaoMock, times(1)).update(user);
        verifyNoMoreInteractions(userDaoMock);


    }


}
