package com.yetistep.anyorder.service;

import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.dao.inf.UserDeviceDao;
import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.UserDeviceEntity;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.service.impl.DriverMobileServiceImpl;
import com.yetistep.anyorder.service.impl.DriverServiceImpl;
import com.yetistep.anyorder.service.inf.DriverMobileService;
import com.yetistep.anyorder.service.inf.DriverService;
import org.apache.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:51 AM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class DriverServiceTest {

    private static final Logger log = Logger.getLogger(DriverServiceTest.class);

    @Mock
    UserDao userDaoMock;

    @Mock
    UserDeviceDao userDeviceDaoMock;

    @InjectMocks
    DriverService driverServiceMock = new DriverServiceImpl();

    @InjectMocks
    DriverMobileService driverMobileServiceMock = new DriverMobileServiceImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDriverLogin() throws Exception {

        UserEntity user = new UserEntity();
        user.setPassword("$2a$10$8eZdSJo9.XWUjN5vpWHisee1zbcjdb4kaoCsYxKxgkyh8U3xRevlq");
        //RandomValueFieldPopulator.populate(user, "password", "fullName");
        UserDeviceEntity userDevice = new UserDeviceEntity();
        userDevice.setDeviceUuid("34455-346");
        List<UserDeviceEntity> userDevices = new ArrayList<>();
        userDevices.add(userDevice);
        user.setUserDevices(userDevices);
        HeaderDto headerDto = new HeaderDto();
        headerDto.setUsername("test@yopmail.com");
        headerDto.setPassword("password");

        when(userDaoMock.findByUsernameMobile(headerDto.getUsername())).thenReturn(user);

        UserDeviceEntity inUserDevice = new UserDeviceEntity();
        userDevice.setDeviceUuid("3445d455-346");
        DriverEntity driver = driverMobileServiceMock.driverLogin(headerDto, inUserDevice);

        Assert.assertThat(driver.getAccessToken(), CoreMatchers.instanceOf(String.class));
        Assert.assertEquals(user.getUserDevices().size(), 2);
        Assert.assertEquals(user.getUserDevices().get(0).getAccessToken(), null);
        Assert.assertNotNull(user.getUserDevices().get(1).getAccessToken());
        verify(userDaoMock, timeout(1)).findByUsernameMobile(headerDto.getUsername());
        verify(userDaoMock, timeout(1)).update(user);
    }

    @Test
    public void testGetDriver() throws Exception {




        //UserEntity user = driverServiceMock.getDriver();
    }

}
