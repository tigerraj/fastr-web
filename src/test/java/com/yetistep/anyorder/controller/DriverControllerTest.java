package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.model.DriverEntity;
import com.yetistep.anyorder.model.UserDeviceEntity;
import com.yetistep.anyorder.service.inf.DriverMobileService;
import com.yetistep.anyorder.service.inf.DriverService;
import com.yetistep.anyorder.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/16/16
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class DriverControllerTest {

    private MockMvc mockMvc;

    @Mock
    private DriverService driverServiceMock;

    @Mock
    private DriverMobileService driverMobileServiceMock;

    @InjectMocks
    private DriverController controllerUnderTest;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();
    }

    /*@Test
     public void testDriverLogin() throws Exception {

        HeaderDto headerDto = new HeaderDto();
        headerDto.setUsername("test@yopmail.com");
        headerDto.setPassword("password");
        DriverEntity driver = new DriverEntity();
        driver.setAccessToken("645567568-4mdgdfg=343457");

        when(driverMobileServiceMock.driverLogin(headerDto, new UserDeviceEntity())).thenReturn(driver);

        mockMvc.perform(post("/driver/login_driver").header("username", "test@yopmail.com").header("password", "password").contentType(MediaType.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(new UserDeviceEntity())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.params.accessToken", is("645567568-4mdgdfg=343457")));

        verify(driverMobileServiceMock, times(1)).driverLogin(headerDto, new UserDeviceEntity());

        verifyNoMoreInteractions(driverMobileServiceMock);

    }*/

    /*@Test
    public void testGetDriver() throws Exception {
        HeaderDto headerDto = new HeaderDto();
        headerDto.setId("436487867863454");
        headerDto.setAccessToken("dfgkj3kl46j346j90sgdoifgjek43");
        DriverEntity driver = new DriverEntity();
        driver.setId(1);
        driver.setUserId("436487867863454");
        driver.setFirstName("Anyorder");
        driver.setLastName("Driver");

        when(driverServiceMock.getDriver()).thenReturn(driver);

        mockMvc.perform(get("/get_driver").header("id", "436487867863454").header("accessToken", "dfgkj3kl46j346j90sgdoifgjek43"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.params.driver", isA(UserEntity.class)));

        verify(driverServiceMock, times(1)).getDriver(headerDto);

        verifyNoMoreInteractions(driverServiceMock);

    }*/

}
