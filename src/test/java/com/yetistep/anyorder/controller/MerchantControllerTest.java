package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.HeaderDto;
import com.yetistep.anyorder.model.MerchantEntity;
import com.yetistep.anyorder.model.UserEntity;
import com.yetistep.anyorder.service.inf.MerchantService;
import com.yetistep.anyorder.service.inf.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

/*
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 2/1/16
 * Time: 12:40 PM
 * To change this template use File | Settings | File Templates.
 */

@RunWith(MockitoJUnitRunner.class)
public class MerchantControllerTest {

    private MockMvc mockMvc;

    @Mock
    private UserService userServiceMock;

    @Mock
    private MerchantService merchantService;

    @InjectMocks
    private MerchantController controllerUnderTest;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();
    }

    @Test
    public void testTest() {
        Assert.assertEquals("1", "1");
    }

   /* @Test
    public void getMerchantDetailTest() throws Exception {
        HeaderDto headerDto = new HeaderDto();
        headerDto.setMerchantId("1");
        MerchantEntity merchantEntity = new MerchantEntity();
        UserEntity user = new UserEntity();
        user.setId(1);
        user.setFirstName("Merchant First Name");
        user.setLastName("Merchant Last Name");
        merchantEntity.setId(1);
        merchantEntity.setUser(user);
        when(merchantService.getMerchantDetail(headerDto)).thenReturn(merchantEntity);

        MvcResult mvcResult = mockMvc.perform(get("/get_merchant_detail").header("merchantId", "1").contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();

        mvcResult.getResponse().getContentAsString();
        verify(merchantService, times(1)).getMerchantDetail(headerDto);
        verifyNoMoreInteractions(merchantService);
    }*/
}
