package com.yetistep.anyorder.controller;

import com.yetistep.anyorder.dto.RequestJsonDto;
import com.yetistep.anyorder.model.OrderEntity;
import com.yetistep.anyorder.service.inf.StoreManagerMobileService;
import com.yetistep.anyorder.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/20/16
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class StoreManagerMobileControllerTest {

    private MockMvc mockMvc;

    @Mock
    private StoreManagerMobileService storeManagerMobileServiceMock;

    @InjectMocks
    private StoreManagerMobileController controllerUnderTest;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();
    }


   /* @Test
    public void testCreateOrder() throws Exception {

        OrderEntity order = new OrderEntity();
        RequestJsonDto requestJsonDto = new RequestJsonDto();
        requestJsonDto.setOrder(order);

        mockMvc.perform(post("/msmanager/create_order").contentType(MediaType.APPLICATION_JSON_UTF8).header("accessToken", UUID.randomUUID().toString()).content(TestUtil.convertObjectToJsonBytes(requestJsonDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success", is(true)));


    }*/


}
