package com.yetistep.anyorder.dao;

import com.yetistep.anyorder.dao.impl.UserDaoImpl;
import com.yetistep.anyorder.dao.inf.UserDao;
import com.yetistep.anyorder.enums.UserRole;
import com.yetistep.anyorder.model.UserEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: Chandra Prakash Panday
 * Date: 5/2/16
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDaoTest {

    @Mock
    SessionFactory sessionFactoryMock;

    @InjectMocks
    UserDao userDaoTest = new UserDaoImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCheckIfEmailExistsWithSameRole() {

        Criteria criteria = Mockito.mock(Criteria.class);
        Session session = Mockito.mock(Session.class);
        when(sessionFactoryMock.getCurrentSession()).thenReturn(session);
        when(sessionFactoryMock.getCurrentSession().createCriteria(UserEntity.class)).thenReturn(criteria);

        userDaoTest.checkIfEmailExistsWithSameRole("test@yopmail.com", UserRole.ROLE_CUSTOMER);

        verify(sessionFactoryMock, times(2)).getCurrentSession();
        verifyNoMoreInteractions(sessionFactoryMock);

    }

}
